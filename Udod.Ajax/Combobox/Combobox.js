﻿(function ($) {
    $.widget("ui.combobox", {
        _create: function () {
            var self = this;
            var select = this.element,
            //theWidth = select.width(),
                  selected = select.children(":selected"),
                  value = selected.val() ? selected.text() : "";
            select.hide();
            var input = $('<input type="text" class="ui-autocomplete-box" />')
                  .insertAfter(select).val(value)
                  .autocomplete({
                      delay: 0,
                      minLength: 3,
                      source: function (request, response) {
                          var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                          response(select.children("option").map(function () {
                              var text = $(this).text();
                              if (this.value && (!request.term || matcher.test(text)))
                                  return {
                                      label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")", "gi"), "$1"),
                                      value: text,
                                      option: this
                                  };
                          }));
                      },
                      select: function (event, ui) {
                          ui.item.option.selected = true;
                          //select.val( ui.item.option.value );
                          self._trigger("selected", event, {
                              item: ui.item.option
                          });
                      },
                      change: function (event, ui) {
                          if (!ui.item) {
                              var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i"),
                              valid = false;
                              select.children("option").each(function () {
                                  if (this.value.match(matcher)) {
                                      this.selected = valid = true;
                                      return false;
                                  }
                              });
                              if (!valid) {
                                  // remove invalid value, as it didn't match anything
                                  $(this).val("");
                                  select.val("");
                                  self._trigger("selected", event, null);
                                  return false;
                              }
                          }
                      }
                  })
            //.addClass("ui-widget ui-widget-content");
            input.focus(function (e) { if (!input.val()) input.autocomplete('search'); });
            $('<button class="ui-autocomplete-button" />').insertAfter(input)
                .click(function (e) {
                    // close if already visible
                    if (input.autocomplete("widget").is(":visible")) {
                        input.autocomplete("close");
                        return false;
                    }
                    // pass empty string as value to search for, displaying all results
                    input.autocomplete("search", "");
                    input.focus();
                    return false;
                });
        }
    });
})(jQuery);

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using AjaxControlToolkit;
using Udod.Ajax;

[assembly: System.Web.UI.WebResource("Udod.Ajax.Combobox.Combobox.js", "text/javascript")]

namespace Udod.Ajax
{
    [ClientScriptResource("Udod.Ajax.Combobox", "Udod.Ajax.Combobox.Combobox.js")]
    [RequiredScript(typeof(jQueryScript))]
    [RequiredScript(typeof(jUIall))]
    public static class ComboBox 
    {

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AjaxControlToolkit;
using System.Web.UI;

[assembly: System.Web.UI.WebResource("Udod.Ajax.Test.js", "text/javascript")]

namespace Udod.Ajax
{
    [ClientScriptResource("Udod.Ajax", "Udod.Ajax.Test.js")]
    [RequiredScript(typeof(jQueryScript))]
    [RequiredScript(typeof(jUIall))]
    [TargetControlType(typeof(Control))]
    public class Test : ExtenderControlBase
    {

    }
}
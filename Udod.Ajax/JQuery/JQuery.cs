﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AjaxControlToolkit;

[assembly: System.Web.UI.WebResource("Udod.Ajax.jquery.js", "text/javascript")]
[assembly: System.Web.UI.WebResource("Udod.Ajax.Common.jBgIframe.js", "text/javascript")]
[assembly: System.Web.UI.WebResource("Udod.Ajax.Common.minmax.js", "text/javascript")]
[assembly: System.Web.UI.WebResource("Udod.Ajax.jAutocomplete.js", "text/javascript")]

namespace Udod.Ajax
{
    /// <summary>
    /// This class just exists as a type to get jquery script loaded. 
    /// </summary>
    [ClientScriptResource(null, "Udod.Ajax.jquery.js")]
    public static class jQueryScript 
    {
    }
}
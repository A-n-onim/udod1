﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AjaxControlToolkit;

[assembly: System.Web.UI.WebResource("Udod.Ajax.jMsAjax.js", "text/javascript")]

namespace Udod.Ajax
{
    /// <summary>
    /// This class just exists as a type to get jquery script loaded. 
    /// </summary>
    [ClientScriptResource(null, "Udod.Ajax.jMsAjax.js")]
    public static class jMsAjax
    {
    }
}
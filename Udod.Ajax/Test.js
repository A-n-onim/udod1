﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Udod.Ajax");

Udod.Ajax.Test = function() {

    Udod.Ajax.Test.initializeBase(this);

    // initialize fields
    this._list = null;
    this._container = null;
    this._region = null;
    this._rayon = null;
    this._city = null;
    this._naspunkt = null;
    this._street = null;
    this._housenumber = null;
    this._fraction = null;
    this._housing = null;
    this._building = null;
    this._flat = null;
    this._service = '../WebServices/kladr.asmx';
}

Udod.Ajax.Test.prototype =
{
    initialize: function () {
        Udod.Ajax.Test.callBaseMethod(this, 'initialize');

        this._list = $(this.get_element());
        this._region = $("#region");
        this._rayon = $("#rayon");
        this._city = $("#city");
        //this._naspunkt = $("#");
        this._street = $("#street");
        this._housenumber = $("#HouseNumber");
        this._fraction = $("#Fraction");
        this._housing = $("#Housing");
        this._building = $("#Building");
        this._flat = $("#Flat");


        this.InitCombobox();
    },

    dispose: function () {
        //Add custom dispose actions here
        Udod.Ajax.Test.callBaseMethod(this, 'dispose');
        this._list = null;
        this._container = null;
        this._region = null;
        this._rayon = null;
        this._city = null;
        this._naspunkt = null;
        this._street = null;
        this._housenumber = null;
        this._fraction = null;
        this._housing = null;
        this._building = null;
        this._flat = null;
        
    },

    InitCombobox: function () {

        var self = this;
        $.jmsajax({
            url: self._service,
            method: 'GetRegion',
            data: {},
            error: function (request, status, error) {
            },
            success: function (json) {
                if (!json) {
                    return;
                }
                $(json).each(function () {
                    self._region.append('<option value="' + this.Code + '">' + this.Name + '</option>');
                });
                self._region.combobox();
            }
        });


        return true;
    }
}

Udod.Ajax.Test.registerClass('Udod.Ajax.Test', Sys.UI.Behavior);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
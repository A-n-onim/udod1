﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Udod.Dal
{
    public class TransferDb : BaseDb
    {
        public TransferDb() {}

        public List<ExtendedTransferStatus> GetTransferStatuses()
        {
            List<ExtendedTransferStatus> list = new List<ExtendedTransferStatus>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetTransferStatus", Connection) { CommandType = CommandType.StoredProcedure };
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedTransferStatus item = new ExtendedTransferStatus();
                    item.TransferStatusId = (int)reader["TransferStatusId"];
                    item.TransferStatusName = (string)reader["TransferStatusName"];
                    list.Add(item);
                }
            }

            return list;
        }

        public void TransferDeSelect(long udodId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Transfer_DeSelect", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.ExecuteNonQuery();
            }
        }

        public void TransferCheckAgeGroupPupil(long ageGroupPupilId, bool check)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UPDATE AgeGroup_Pupil SET TransferIsSelected = @check WHERE AgeGroupPupilId=@ageGroupPupilId", Connection) { CommandType = CommandType.Text };
                comm.Parameters.Add(new SqlParameter("check", check ? 1 : 0));
                comm.Parameters.Add(new SqlParameter("AgeGroupPupilId", ageGroupPupilId));
                comm.ExecuteNonQuery();
            }
        }

        public List<ExtendedTransferPupil> GetTransferPupilPaging(long udodId, long? childUnionId, int? numYear, string pupilLastName, string pupilFirstName, string pupilMiddleName, int? status, long? agegroupId, int? PagerIndex, int? PagerLength)
        {
            List<ExtendedTransferPupil> list = new List<ExtendedTransferPupil>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Transfer_GetTransferPupilPaging", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("udodId", udodId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("childUnionId", childUnionId.Value));
                if (numYear.HasValue) comm.Parameters.Add(new SqlParameter("numYear", numYear.Value));
                if (agegroupId.HasValue) comm.Parameters.Add(new SqlParameter("agegroupId", agegroupId.Value));
                if (!string.IsNullOrEmpty(pupilLastName)) comm.Parameters.Add(new SqlParameter("pupilLastName", pupilLastName));
                if (!string.IsNullOrEmpty(pupilFirstName)) comm.Parameters.Add(new SqlParameter("pupilFirstName", pupilFirstName));
                if (!string.IsNullOrEmpty(pupilMiddleName)) comm.Parameters.Add(new SqlParameter("pupilMiddleName", pupilMiddleName));
                if (status.HasValue) comm.Parameters.Add(new SqlParameter("status", status.Value));
                if (PagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", PagerIndex));
                if (PagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", PagerLength));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedTransferPupil item = new ExtendedTransferPupil();
                    item.AgeGroupPupilId = (long)reader["AgeGroupPupilId"];
                    item.PupilFio = (string)reader["PupilFio"];
                    item.PupilBirthday = (DateTime)reader["PupilBirthday"];
                    item.ChildUnion = new ExtendedChildUnion
                    {
                        ChildUnionId = (long)reader["ChildUnionId"],
                        Name = (string)reader["ChildUnionName"]
                    };
                    item.CurrentNumYear = (int)reader["CurrentNumYear"];
                    item.TotalNumYear = (int)reader["TotalNumYear"];
                    item.PupilYearsOnSeptFirst = (int)reader["PupilYearsOnSeptFirst"];
                    item.ChildUnionAges = (string)reader["ChildUnionAges"];
                    item.DictTypeBudgetId = (int)reader["DictTypeBudgetId"];
                    item.StatusDO = (int) reader["StatusDO"];
                    item.Statusgroup = (int)reader["StatusGroup"];
                    item.NameGroup = (string) reader["GroupName"];
                    item.StartDate = (DateTime) reader["StartDate"];
                    item.EndDate = (DateTime)reader["EndDate"];
                    item.TransferStatus = new ExtendedTransferStatus
                    {
                        TransferStatusId = (int)reader["TransferStatusId"],
                        TransferStatusName = (string)reader["TransferStatusName"]
                    };
                    item.TransferIsSelected = (bool)reader["TransferIsSelected"];

                    list.Add(item);
                }
            }

            return list;
        }

        public int GetTransferPupilCount(long udodId, long? childUnionId, int? numYear, string pupilLastName, string pupilFirstName, string pupilMiddleName, int? status, long? agegroupId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Transfer_GetTransferPupilCount", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("udodId", udodId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("childUnionId", childUnionId.Value));
                if (numYear.HasValue) comm.Parameters.Add(new SqlParameter("numYear", numYear.Value));
                if (agegroupId.HasValue) comm.Parameters.Add(new SqlParameter("agegroupId", agegroupId.Value));
                if (!string.IsNullOrEmpty(pupilLastName)) comm.Parameters.Add(new SqlParameter("pupilLastName", pupilLastName));
                if (!string.IsNullOrEmpty(pupilFirstName)) comm.Parameters.Add(new SqlParameter("pupilFirstName", pupilFirstName));
                if (!string.IsNullOrEmpty(pupilMiddleName)) comm.Parameters.Add(new SqlParameter("pupilMiddleName", pupilMiddleName));
                if (status.HasValue) comm.Parameters.Add(new SqlParameter("status", status.Value));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }

            return 0;
        }

        public void ChangeStatusInSelected(long udodId, int newStatus)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Transfer_ChangeStatusInSelected", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("NewStatus", newStatus));
                comm.ExecuteNonQuery();
            }
        }

        public void RecalculateUdod(long udodId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Transfer_RecalculateUdod", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.ExecuteNonQuery();
            }
        }

        public int GetSelectedPupilCount(long udodId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Transfer_GetSelectedPupilCount", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("udodId", udodId));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }

            return 0;
        }

        public List<ExtendedPupil> TransferPupilInNewDO(long udodId, long groupId, int numYear)
        {
            List<ExtendedPupil> list=new List<ExtendedPupil>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Transfer_TransferInNewDO", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("groupId", groupId));
                comm.Parameters.Add(new SqlParameter("NewNumyear", numYear));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedPupil item = new ExtendedPupil();
                    item.UserId = (Guid)reader["UserId"];
                    string message = (string)reader["LastName"]
                    + " " + (string)reader["FirstName"]
                    + " " + (string)reader["MiddleName"]
                        // это ошибка по которой не зачислился ребенок
                    + ": " + (string)reader["ErrorMessage"];
                    item.ErrorMessage = message;
                    list.Add(item);
                }
            }
            return list;
        }


    }
}

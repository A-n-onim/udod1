﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.DataSetExtensions;
using System.Configuration;

namespace Udod.Dal
{
    public class PupilsDb :BaseDb
    {
        public List<ExtendedPupil> GetPupilsUdod(Guid pupilId, long childUnionId)
        {
            List<ExtendedPupil> list = new List<ExtendedPupil>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Pupil_GetAgeGroupPupils", conn) {CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("PupilId", pupilId));
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["ReestrCode"]!=DBNull.Value)
                    {
                        ExtendedPupil item = new ExtendedPupil();

                        item.ChildUnions = new List<ExtendedChildUnion>();
                        item.ChildUnions.Add(new ExtendedChildUnion()
                        {
                            ReestrPupilAgeGroupId = (string)reader["ReestrCode"]
                        });

                        list.Add(item);
                    }
                }
            }

            return list;
        }

        public List<ExtendedPupil> GetPupils(long udodId, long? childUnionId, string pupilLastName, string pupilFirstName, string pupilMiddleName, Guid? teacherId)
        {
            List<ExtendedPupil> list = new List<ExtendedPupil>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm= new SqlCommand("Udod_GetPupils", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (!string.IsNullOrEmpty(pupilLastName)) comm.Parameters.Add(new SqlParameter("PupilLastName", pupilLastName));
                if (!string.IsNullOrEmpty(pupilFirstName)) comm.Parameters.Add(new SqlParameter("PupilFirstName", pupilFirstName));
                if (!string.IsNullOrEmpty(pupilMiddleName)) comm.Parameters.Add(new SqlParameter("PupilMiddleName", pupilMiddleName));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Guid _pupilId = (Guid)reader["PupilUserId"];
                    long? _childUnionId = null;
                    if (reader["ChildUnionId"] != DBNull.Value) _childUnionId = (long)reader["ChildUnionId"];

                    Guid? _parentId = null;
                    if (reader["ParentUserId"] != DBNull.Value) _parentId = (Guid)reader["parentUserId"];

                    long? _addressId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long)reader["AddressId"];

                    ExtendedPupil last = list.LastOrDefault(i => i.UserId == _pupilId);

                    if (null != last)
                    {
                        if (_childUnionId.HasValue && last.ChildUnions.Count(i => i.ChildUnionId == _childUnionId) == 0)
                        {
                            var itemChildUnion = new ExtendedChildUnion();
                            itemChildUnion.ChildUnionId = (long)reader["ChildUnionId"];
                            itemChildUnion.Name = (string)reader["ChildUnionName"];
                            (last.ChildUnions as List<ExtendedChildUnion>).Add(itemChildUnion);
                        }

                        if (_parentId.HasValue && last.Parents.Count(i => i.UserId == _parentId) == 0)
                        {
                            var parent = new ExtendedUser()
                            {
                                UserId = (Guid)reader["ParentUserId"],
                                LastName = (string)reader["ParentLastName"],
                                FirstName = (string)reader["ParentFirstName"],
                                MiddleName = (string)reader["ParentMiddleName"],
                                PhoneNumber = (string)reader["PhoneNumber"]
                            };
                            (last.Parents as List<ExtendedUser>).Add(parent);
                        }

                        if (_addressId.HasValue && last.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var address = new ExtendedAddress();
                            address.AddressId = (long)reader["AddressId"];
                            address.AddressType = new ExtendedAddressType()
                            {
                                AddressTypeId = (int)reader["AddressTypeId"]
                            };
                            address.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) address.BtiCode = (int)reader["BtiCode"];
                            address.KladrCode = (string)reader["KladrCode"];
                            (last.Addresses as List<ExtendedAddress>).Add(address);
                        }
                    }
                    else
                    {
                        ExtendedPupil item = new ExtendedPupil();
                        item.UserId = (Guid)reader["PupilUserId"];
                        item.LastName = (string)reader["PupilLastName"];
                        item.FirstName = (string)reader["PupilFirstName"];
                        item.MiddleName = (string)reader["PupilMiddleName"];
                        item.Birthday = (DateTime)reader["Birthday"];
                        item.Sex = Convert.ToInt32((bool)reader["Sex"]);
                        item.AddressStr = (string)reader["AddressStr"];
                        item.SchoolName = (string)reader["SchoolName"];
                        item.ClassName = (string)reader["ClassName"];

                        item.Parents = new List<ExtendedUser>();

                        if (_parentId.HasValue)
                        {
                            var parent = new ExtendedUser()
                            {
                                UserId = (Guid)reader["ParentUserId"],
                                LastName = (string)reader["ParentLastName"],
                                FirstName = (string)reader["ParentFirstName"],
                                MiddleName = (string)reader["ParentMiddleName"],
                                PhoneNumber = (string)reader["PhoneNumber"]
                            };
                            (item.Parents as List<ExtendedUser>).Add(parent);
                        };

                        item.ChildUnions = new List<ExtendedChildUnion>();

                        if (_childUnionId.HasValue)
                        {
                            var itemChildUnion = new ExtendedChildUnion();
                            itemChildUnion.ChildUnionId = (long)reader["ChildUnionId"];
                            itemChildUnion.Name = (string) reader["ChildUnionName"];
                            (item.ChildUnions as List<ExtendedChildUnion>).Add(itemChildUnion);
                        }

                        item.Addresses = new List<ExtendedAddress>();

                        if (_addressId.HasValue)
                        {
                            var address = new ExtendedAddress();
                            address.AddressId = (long)reader["AddressId"];
                            address.AddressType = new ExtendedAddressType()
                            {
                                AddressTypeId = (int)reader["AddressTypeId"]
                            };
                            address.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) address.BtiCode = (int)reader["BtiCode"];
                            address.KladrCode = (string)reader["KladrCode"];
                            (item.Addresses as List<ExtendedAddress>).Add(address);
                        }

                        list.Add(item);
                    }
                }
            }

            return list.OrderBy(i=>i.Fio).ToList();
        }

        public List<ExtendedPupil> GetGroupPupilsPaging(int? cityId, long? udodId, long? childUnionId, string pupilLastName, string pupilFirstName, string pupilMiddleName, int? PagerIndex, int? PagerLength, Guid? teacherId, int? pupilNumYear, int? budgetTypeId, long? groupId)
        {
            //Stopwatch sw= new Stopwatch();
            //sw.Start();
            List<ExtendedPupil> list = new List<ExtendedPupil>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetGroupPupilPaging", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                if (!string.IsNullOrEmpty(pupilLastName)) comm.Parameters.Add(new SqlParameter("PupilLastName", pupilLastName));
                if (!string.IsNullOrEmpty(pupilFirstName)) comm.Parameters.Add(new SqlParameter("PupilFirstName", pupilFirstName));
                if (!string.IsNullOrEmpty(pupilMiddleName)) comm.Parameters.Add(new SqlParameter("PupilMiddleName", pupilMiddleName));
                if (PagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", PagerIndex));
                if (PagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", PagerLength));
                if (pupilNumYear.HasValue) comm.Parameters.Add(new SqlParameter("NumYear", pupilNumYear));
                if (budgetTypeId.HasValue) comm.Parameters.Add(new SqlParameter("BudgetTypeId", budgetTypeId));
                if (groupId.HasValue) comm.Parameters.Add(new SqlParameter("GroupId", groupId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Guid _pupilId = (Guid)reader["PupilUserId"];
                    long? _childUnionId = null;
                    if (reader["ChildUnionId"] != DBNull.Value) _childUnionId = (long)reader["ChildUnionId"];

                    
                    ExtendedPupil item = new ExtendedPupil();
                    item.UserId = (Guid)reader["PupilUserId"];
                    item.LastName = (string)reader["PupilLastName"];
                    item.FirstName = (string)reader["PupilFirstName"];
                    item.MiddleName = (string)reader["PupilMiddleName"];
                    item.Birthday = (DateTime)reader["Birthday"];
                    item.AgeGroupPupilId = (long) reader["AgeGroupPupilId"];
                    item.IsSelect = (bool) reader["isSelect"];    
                    item.ChildUnions = new List<ExtendedChildUnion>();
                    
                    if (_childUnionId.HasValue)
                    {
                        var itemChildUnion = new ExtendedChildUnion();
                        itemChildUnion.ChildUnionId = (long)reader["ChildUnionId"];
                        itemChildUnion.PupilNumYear = (int) reader["NumYear"];
                        itemChildUnion.PupilBudgetType = new ExtendedTypeBudget()
                                                        {
                                                            DictTypeBudgetId = (int) reader["DictTypeBudgetId"]
                                                        };
                        itemChildUnion.ReestrPupilAgeGroupId = (string) reader["ReestrCodeZach"];
                        (item.ChildUnions as List<ExtendedChildUnion>).Add(itemChildUnion);
                    }

                    item.Group=new ExtendedGroup()
                    {
                        UdodAgeGroupId = (long)reader["UdodAgeGroupId"],
                        Name = (string)reader["GroupName"],
                        ChildUnionName = (string)reader["ChildUnionName"]
                    };

                    list.Add(item);
                    
                }
            }
            //sw.Stop();
            //long i1 = sw.ElapsedMilliseconds;
            return list;
        }

        public int GetGroupPupilsCount(int? cityId, long? udodId, long? childUnionId, string pupilLastName, string pupilFirstName, string pupilMiddleName, Guid? teacherId, int? pupilNumYear, int? budgetTypeId, long? groupId)
        {
            List<ExtendedPupil> list = new List<ExtendedPupil>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetGroupPupilCount", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                if (!string.IsNullOrEmpty(pupilLastName)) comm.Parameters.Add(new SqlParameter("PupilLastName", pupilLastName));
                if (!string.IsNullOrEmpty(pupilFirstName)) comm.Parameters.Add(new SqlParameter("PupilFirstName", pupilFirstName));
                if (!string.IsNullOrEmpty(pupilMiddleName)) comm.Parameters.Add(new SqlParameter("PupilMiddleName", pupilMiddleName));
                if (pupilNumYear.HasValue) comm.Parameters.Add(new SqlParameter("NumYear", pupilNumYear));
                if (budgetTypeId.HasValue) comm.Parameters.Add(new SqlParameter("BudgetTypeId", budgetTypeId));
                if (groupId.HasValue) comm.Parameters.Add(new SqlParameter("GroupId", groupId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }

            return 0;
        }

        public void SelectGroupPupil(long ageGroupPupilId, bool selectvalue)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GroupSelectPupil", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("AgeGroupPupilId", ageGroupPupilId));
                comm.Parameters.Add(new SqlParameter("Selectvalue", selectvalue));
                comm.ExecuteNonQuery();
            }
        }

        public void ClearSelectGroupPupil(long udodId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GroupClearSelectPupil", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.ExecuteNonQuery();
            }
        }

        public List<ExtendedPupil> TransferPupil(long childUnionOldId, long childunionId, long udodAgeGroupId, int? numYear, int? typeBudgetId, long? groupfilterId)
        {
            List<ExtendedPupil> list = new List<ExtendedPupil>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GroupTransferPupil", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                //comm.Parameters.Add(new SqlParameter("ChildUnionOldId", childUnionOldId));
                comm.Parameters.Add(new SqlParameter("ChildunionId", childUnionOldId));
                comm.Parameters.Add(new SqlParameter("UdodAgeGroupId", udodAgeGroupId));
                if (typeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                if (numYear.HasValue) comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                if (groupfilterId.HasValue) comm.Parameters.Add(new SqlParameter("GroupfilterId", groupfilterId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedPupil item = new ExtendedPupil();
                    item.UserId = (Guid) reader["UserId"];
                    string message = (string) reader["LastName"]
                    +" "+(string) reader["FirstName"]
                    + " " + (string)reader["MiddleName"]
                    // это ошибка по которой не зачислился ребенок
                    + ": " + (string)reader["ErrorMessage"];
                    item.ErrorMessage = message;
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedPupil> ReestrGetAgeGroupPupils(int? cityId, long? udodId, long? childUnionId, string pupilLastName, string pupilFirstName, string pupilMiddleName, int? PagerIndex, int? PagerLength, Guid? teacherId, int message, int? pupilNumYear, int? budgetTypeId, bool showDelete)
        {
            //Stopwatch sw= new Stopwatch();
            //sw.Start();
            List<ExtendedPupil> list = new List<ExtendedPupil>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Reestr_GetAgeGroupPupils", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                if (!string.IsNullOrEmpty(pupilLastName)) comm.Parameters.Add(new SqlParameter("PupilLastName", pupilLastName));
                if (!string.IsNullOrEmpty(pupilFirstName)) comm.Parameters.Add(new SqlParameter("PupilFirstName", pupilFirstName));
                if (!string.IsNullOrEmpty(pupilMiddleName)) comm.Parameters.Add(new SqlParameter("PupilMiddleName", pupilMiddleName));
                if (PagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", PagerIndex));
                if (PagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", PagerLength));
                comm.Parameters.Add(new SqlParameter("Message", message));
                if (pupilNumYear.HasValue) comm.Parameters.Add(new SqlParameter("PupilNumYear", pupilNumYear));
                if (budgetTypeId.HasValue) comm.Parameters.Add(new SqlParameter("BudgetTypeId", budgetTypeId));
                comm.Parameters.Add(new SqlParameter("ShowDelete", showDelete));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Guid _pupilId = (Guid)reader["PupilUserId"];
                    long? _childUnionId = null;
                    if (reader["ChildUnionId"] != DBNull.Value) _childUnionId = (long)reader["ChildUnionId"];

                    Guid? _parentId = null;
                    if (reader["ParentUserId"] != DBNull.Value) _parentId = (Guid)reader["parentUserId"];

                    long? _addressId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long)reader["AddressId"];

                    ExtendedPupil last = list.LastOrDefault(i => i.UserId == _pupilId);

                    if (null != last)
                    {
                        if (_parentId.HasValue && last.Parents.Count(i => i.UserId == _parentId) == 0)
                        {
                            var parent = new ExtendedUser()
                            {
                                UserId = (Guid)reader["ParentUserId"],
                                LastName = (string)reader["ParentLastName"],
                                FirstName = (string)reader["ParentFirstName"],
                                MiddleName = (string)reader["ParentMiddleName"],
                                PhoneNumber = (string)reader["ParentPhoneNumber"],
                                ExpressPhoneNumber = (string)reader["ParentExpressPhoneNumber"],
                                EMail = (string)reader["ParentEmail"],
                                Passport = new ExtendedPassport()
                                {
                                    PassportType = new ExtendedPassportType()
                                    {
                                        PassportTypeId = (int)reader["ParentPassportTypeId"],
                                        PassportTypeName = (string)reader["ParentPassportTypeName"]
                                    },
                                    UserPassportId = (long)reader["ParentPassportId"],
                                    Series = (string)reader["ParentSeries"],
                                    Number = (string)reader["ParentNumber"],
                                    IssueDate = (DateTime)reader["ParentIssueDate"]
                                },
                                UserType = (int)reader["ParentUserType"]
                            };

                            (last.Parents as List<ExtendedUser>).Add(parent);
                        }

                        if (_addressId.HasValue && last.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            ExtendedAddress address = new ExtendedAddress();
                            address.AddressId = (long)reader["AddressId"];
                            address.AddressStr = (string)reader["AddressStr"];
                            address.AddressEmail = (string)reader["AddressEmail"];
                            address.AddressWorkTime = (string)reader["AddressWorkTime"];
                            address.AddressPhone = (string)reader["AddressPhone"];
                            address.KladrCode = (string)reader["KladrCode"];
                            address.PostIndex = (string)reader["PostIndex"];
                            address.HouseNumber = (string)reader["HouseNumber"];
                            address.Fraction = (string)reader["Fraction"];
                            address.Housing = (string)reader["Housing"];
                            address.Building = (string)reader["Building"];
                            address.FlatNumber = (string)reader["Flat"];
                            address.AddressType = new ExtendedAddressType()
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["Name"]
                            };
                            address.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) address.BtiCode = (int)reader["BtiCode"];

                            address.BtiHouseCode = null;
                            if (reader["BtiHouseCode"] != DBNull.Value) address.BtiHouseCode = (int)reader["BtiHouseCode"];
                            
                            last.Addresses.Add(address);
                        }

                        if (_childUnionId.HasValue && last.ChildUnions.Count(i => i.ChildUnionId == _childUnionId) == 0)
                        {
                            ExtendedChildUnion childUnion = new ExtendedChildUnion();
                            childUnion.ChildUnionId = _childUnionId.Value;
                            childUnion.PupilNumYear = (int)reader["pupilNumYear"];
                            childUnion.PupilAgeGroupId = (long) reader["AgeGroupPupilId"];
                            if (reader["AgeGroupReestrCode"] != DBNull.Value) childUnion.ReestrPupilAgeGroupId = (string)reader["AgeGroupReestrCode"];
                            else childUnion.ReestrPupilAgeGroupId = "";
                            last.ChildUnions.Add(childUnion);
                        }
                    }
                    else
                    {
                        var item = new ExtendedPupil();
                        item.UserId = (Guid)reader["PupilId"];
                        item.LastName = (string)reader["PupilLastName"];
                        item.FirstName = (string)reader["PupilFirstName"];
                        item.MiddleName = (string)reader["PupilMiddleName"];
                        item.Birthday = (DateTime)reader["Birthday"];
                        item.PhoneNumber = (string)reader["PupilPhoneNumber"];
                        item.ExpressPhoneNumber = (string)reader["PupilExpressPhoneNumber"];
                        item.EMail = (string)reader["PupilEmail"];
                        item.Sex = Convert.ToInt32((bool)reader["PupilSex"]);
                        item.ReestrCode = "";
                        try
                        {
                            item.ReestrCode = ((Int64)reader["PupilReestrCode"]).ToString();
                        }
                        catch (Exception)
                        {
                        }
                        item.ReestrUdodCode = "";
                        try
                        {
                            item.ReestrUdodCode = ((Int64)reader["ReestrUdodCode"]).ToString();
                        }
                        catch (Exception)
                        {
                        }
                        item.ReestrGuid = null;
                        if (reader["ReestrGuid"] != DBNull.Value)
                        {
                            item.ReestrGuid = (Guid)reader["ReestrGuid"];
                        }

                        if (reader["PupilPassportId"] != DBNull.Value)
                            item.Passport = new ExtendedPassport()
                            {
                                PassportType = new ExtendedPassportType()
                                {
                                    PassportTypeId = (int)reader["PupilPassportTypeId"],
                                    PassportTypeName = (string)reader["PupilPassportTypeName"]
                                },
                                UserPassportId = (long)reader["PupilPassportId"],
                                Series = (string)reader["PupilSeries"],
                                Number = (string)reader["PupilNumber"],
                                IssueDate = (DateTime)reader["PupilIssueDate"]
                            };

                        item.Parents = new List<ExtendedUser>();

                        if (_parentId.HasValue)
                        {
                            var parent = new ExtendedUser()
                            {
                                UserId = (Guid)reader["ParentUserId"],
                                LastName = (string)reader["ParentLastName"],
                                FirstName = (string)reader["ParentFirstName"],
                                MiddleName = (string)reader["ParentMiddleName"],
                                PhoneNumber = (string)reader["ParentPhoneNumber"],
                                ExpressPhoneNumber = (string)reader["ParentExpressPhoneNumber"],
                                EMail = (string)reader["ParentEmail"],
                                Passport = new ExtendedPassport()
                                {
                                    PassportType = new ExtendedPassportType()
                                    {
                                        PassportTypeId = (int)reader["ParentPassportTypeId"],
                                        PassportTypeName = (string)reader["ParentPassportTypeName"]
                                    },
                                    UserPassportId = (long)reader["ParentPassportId"],
                                    Series = (string)reader["ParentSeries"],
                                    Number = (string)reader["ParentNumber"],
                                    IssueDate = (DateTime)reader["ParentIssueDate"]
                                },
                                UserType = (int)reader["ParentUserType"]
                            };

                            (item.Parents as List<ExtendedUser>).Add(parent);
                        }

                        item.Addresses = new List<ExtendedAddress>();
                        if (_addressId.HasValue)
                        {
                            ExtendedAddress address = new ExtendedAddress();
                            address.AddressId = (long)reader["AddressId"];
                            address.AddressStr = (string)reader["AddressStr"];
                            address.AddressEmail = (string)reader["AddressEmail"];
                            address.AddressWorkTime = (string)reader["AddressWorkTime"];
                            address.AddressPhone = (string)reader["AddressPhone"];
                            address.KladrCode = (string)reader["KladrCode"];
                            address.PostIndex = (string)reader["PostIndex"];
                            address.HouseNumber = (string)reader["HouseNumber"];
                            address.Fraction = (string)reader["Fraction"];
                            address.Housing = (string)reader["Housing"];
                            address.Building = (string)reader["Building"];
                            address.FlatNumber = (string)reader["Flat"];
                            address.AddressType = new ExtendedAddressType()
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["Name"]
                            };
                            address.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) address.BtiCode = (int)reader["BtiCode"];

                            address.BtiHouseCode = null;
                            if (reader["BtiHouseCode"] != DBNull.Value) address.BtiHouseCode = (int)reader["BtiHouseCode"];
                            
                            item.Addresses.Add(address);
                        }

                        item.ChildUnions = new List<ExtendedChildUnion>();
                        if (_childUnionId.HasValue)
                        {
                            ExtendedChildUnion childUnion = new ExtendedChildUnion();
                            childUnion.ChildUnionId = _childUnionId.Value;
                            childUnion.PupilNumYear = (int) reader["pupilNumYear"];
                            if (reader["AgeGroupReestrCode"] != DBNull.Value) childUnion.ReestrPupilAgeGroupId = (string)reader["AgeGroupReestrCode"];
                            else childUnion.ReestrPupilAgeGroupId = "";
                            childUnion.PupilAgeGroupId = (long)reader["AgeGroupPupilId"];
                            item.ChildUnions.Add(childUnion);
                        }

                        list.Add(item);
                    }
                }
            }
            //sw.Stop();
            //long i1 = sw.ElapsedMilliseconds;
            return list;
        }

        public List<ExtendedPupil> GetPupilsPaging(int? cityId, long? udodId, long? childUnionId, string pupilLastName, string pupilFirstName, string pupilMiddleName, int? PagerIndex, int? PagerLength, Guid? teacherId, int message, int? pupilNumYear, int? budgetTypeId, bool showDelete, long? groupId)
        {
            //Stopwatch sw= new Stopwatch();
            //sw.Start();
            List<ExtendedPupil> list = new List<ExtendedPupil>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetPupilsPaging", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                if (!string.IsNullOrEmpty(pupilLastName)) comm.Parameters.Add(new SqlParameter("PupilLastName", pupilLastName));
                if (!string.IsNullOrEmpty(pupilFirstName)) comm.Parameters.Add(new SqlParameter("PupilFirstName", pupilFirstName));
                if (!string.IsNullOrEmpty(pupilMiddleName)) comm.Parameters.Add(new SqlParameter("PupilMiddleName", pupilMiddleName));
                if (PagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", PagerIndex));
                if (PagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", PagerLength));
                comm.Parameters.Add(new SqlParameter("Message", message));
                if (pupilNumYear.HasValue) comm.Parameters.Add(new SqlParameter("PupilNumYear", pupilNumYear));
                if (budgetTypeId.HasValue) comm.Parameters.Add(new SqlParameter("BudgetTypeId", budgetTypeId));
                if (groupId.HasValue) comm.Parameters.Add(new SqlParameter("GroupId", groupId));
                comm.Parameters.Add(new SqlParameter("ShowDelete", showDelete));

                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Guid _pupilId = (Guid)reader["PupilUserId"];
                    long? _childUnionId = null;
                    if (reader["ChildUnionId"] != DBNull.Value) _childUnionId = (long)reader["ChildUnionId"];

                    Guid? _parentId = null;
                    if (reader["ParentUserId"] != DBNull.Value) _parentId = (Guid)reader["parentUserId"];

                    ExtendedPupil last = list.LastOrDefault(i => i.UserId == _pupilId);

                    if (null != last)
                    {
                        if (_childUnionId.HasValue && last.ChildUnions.Count(i => i.ChildUnionId == _childUnionId) == 0)
                        {
                            var itemChildUnion = new ExtendedChildUnion();
                            itemChildUnion.ChildUnionId = (long)reader["ChildUnionId"];
                            (last.ChildUnions as List<ExtendedChildUnion>).Add(itemChildUnion);
                        }

                        if (_parentId.HasValue && last.Parents.Count(i => i.UserId == _parentId) == 0)
                        {
                            var parent = new ExtendedUser()
                            {
                                UserId = (Guid)reader["ParentUserId"],
                                LastName = (string)reader["ParentLastName"],
                                FirstName = (string)reader["ParentFirstName"],
                                MiddleName = (string)reader["ParentMiddleName"],
                                PhoneNumber = (string)reader["PhoneNumber"],
                                ExpressPhoneNumber = (string)reader["ExpressPhoneNumber"]
                            };
                            (last.Parents as List<ExtendedUser>).Add(parent);
                        }
                    }
                    else
                    {
                        ExtendedPupil item = new ExtendedPupil();
                        item.UserId = (Guid)reader["PupilUserId"];
                        item.LastName = (string)reader["PupilLastName"];
                        item.FirstName = (string)reader["PupilFirstName"];
                        item.MiddleName = (string)reader["PupilMiddleName"];
                        item.Birthday = (DateTime)reader["Birthday"];
                        item.AddressStr = (string)reader["AddressStr"];
                        item.ErrorMessage = (string)reader["ErrorMessage"];
                        item.Parents = new List<ExtendedUser>();

                        if (_parentId.HasValue)
                        {
                            var parent = new ExtendedUser()
                            {
                                UserId = (Guid)reader["ParentUserId"],
                                LastName = (string)reader["ParentLastName"],
                                FirstName = (string)reader["ParentFirstName"],
                                MiddleName = (string)reader["ParentMiddleName"],
                                PhoneNumber = (string)reader["PhoneNumber"],
                                ExpressPhoneNumber = (string)reader["ExpressPhoneNumber"]
                            };
                            (item.Parents as List<ExtendedUser>).Add(parent);
                        };

                        item.ChildUnions = new List<ExtendedChildUnion>();

                        if (_childUnionId.HasValue)
                        {
                            var itemChildUnion = new ExtendedChildUnion();
                            itemChildUnion.ChildUnionId = (long)reader["ChildUnionId"];
                            (item.ChildUnions as List<ExtendedChildUnion>).Add(itemChildUnion);
                        }

                        list.Add(item);
                    }
                }
            }
            //sw.Stop();
            //long i1 = sw.ElapsedMilliseconds;
            return list;
        }

        public int GetPupilsCount(int? cityId, long? udodId, long? childUnionId, string pupilLastName, string pupilFirstName, string pupilMiddleName, Guid? teacherId, int message, int? pupilNumYear, int? budgetTypeId, bool showDelete, long? groupId)
        {
            List<ExtendedPupil> list = new List<ExtendedPupil>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetPupilsCount", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                if (!string.IsNullOrEmpty(pupilLastName)) comm.Parameters.Add(new SqlParameter("PupilLastName", pupilLastName));
                if (!string.IsNullOrEmpty(pupilFirstName)) comm.Parameters.Add(new SqlParameter("PupilFirstName", pupilFirstName));
                if (!string.IsNullOrEmpty(pupilMiddleName)) comm.Parameters.Add(new SqlParameter("PupilMiddleName", pupilMiddleName));
                comm.Parameters.Add(new SqlParameter("Message", message));
                if (pupilNumYear.HasValue) comm.Parameters.Add(new SqlParameter("PupilNumYear", pupilNumYear));
                if (budgetTypeId.HasValue) comm.Parameters.Add(new SqlParameter("BudgetTypeId", budgetTypeId));
                if (groupId.HasValue) comm.Parameters.Add(new SqlParameter("GroupId", groupId));
                comm.Parameters.Add(new SqlParameter("ShowDelete", showDelete));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }

            return 0;
        }

        public List<ExtendedPupil> GetPupilsToExport(long udodId, long? childUnionId, string pupilLastName, string pupilFirstName, string pupilMiddleName, Guid? teacherId, int? numYear, int? typeBudgetId, bool isDeleted, long? ageGroupId)
        {
            List<ExtendedPupil> list = new List<ExtendedPupil>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("PupilExport", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (!string.IsNullOrEmpty(pupilLastName)) comm.Parameters.Add(new SqlParameter("PupilLastName", pupilLastName));
                if (!string.IsNullOrEmpty(pupilFirstName)) comm.Parameters.Add(new SqlParameter("PupilFirstName", pupilFirstName));
                if (!string.IsNullOrEmpty(pupilMiddleName)) comm.Parameters.Add(new SqlParameter("PupilMiddleName", pupilMiddleName));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                if (numYear.HasValue) comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                if (typeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                if (isDeleted) comm.Parameters.Add(new SqlParameter("isDeleted", isDeleted));
                if (ageGroupId.HasValue) comm.Parameters.Add(new SqlParameter("AgeGroupId", ageGroupId));
                SqlDataReader reader = comm.ExecuteReader();
                int ordDateOtch = reader.GetOrdinal("DateOtch");
                while (reader.Read())
                {
                    Guid _pupilId = (Guid)reader["PupilUserId"];
                    long? _childUnionId = null;
                    if (reader["ChildUnionId"] != DBNull.Value) _childUnionId = (long)reader["ChildUnionId"];

                    Guid? _parentId = null;
                    if (reader["ParentUserId"] != DBNull.Value) _parentId = (Guid)reader["parentUserId"];

                    long? _addressId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long)reader["AddressId"];

                    ExtendedPupil last = list.LastOrDefault(i => i.UserId == _pupilId);

                    if (null != last)
                    {
                        if (_childUnionId.HasValue && last.ChildUnions.Count(i => i.ChildUnionId == _childUnionId) == 0)
                        {
                            var itemChildUnion = new ExtendedChildUnion();
                            itemChildUnion.ChildUnionId = (long)reader["ChildUnionId"];
                            itemChildUnion.Name = (string)reader["ChildUnionName"];
                            itemChildUnion.UdodId = (long)reader["UdodId"];
                            itemChildUnion.UdodShortName = (string)reader["UdodShortName"];
                            itemChildUnion.Profile = new ExtendedProfile()
                            {
                                Name = (string)reader["ProfileName"]
                            };
                            itemChildUnion.Program = new ExtendedProgram()
                            {
                                Name = (string)reader["ProgramName"]
                            };
                            itemChildUnion.PupilBudgetType = new ExtendedTypeBudget()
                            {
                                TypeBudgetName = (string)reader["TypeBudgetName"]
                            };
                            itemChildUnion.PupilHours = (double)reader["Hours"];
                            itemChildUnion.PupilNumYear = (int)reader["NumYear"];
                            itemChildUnion.PupilAgeGroupId = (long)reader["AgeGroupPupilId"];
                            itemChildUnion.AgeGroups = new List<ExtendedAgeGroup>();
                            Object dateOtch = ordDateOtch < 0 || reader.IsDBNull(ordDateOtch)
                                                  ? null
                                                  : reader["DateOtch"];
                            itemChildUnion.AgeGroups.Add(new ExtendedAgeGroup()
                            {
                                Name = (string)reader["GroupName"],
                                DateBegin = (DateTime)reader["DateZach"],
                                DateEnd = (DateTime?)dateOtch
                            });
                            /*itemChildUnion.DateStart = (DateTime)reader["DateBegin"];
                            itemChildUnion.DateEnd1 = null;
                            if (reader["DateEnd"] != DBNull.Value)
                                itemChildUnion.DateEnd1 = (DateTime)reader["DateEnd"];
                            */
                            (last.ChildUnions as List<ExtendedChildUnion>).Add(itemChildUnion);
                        }

                        if (_parentId.HasValue && last.Parents.Count(i => i.UserId == _parentId) == 0)
                        {
                            var parent = new ExtendedUser()
                            {
                                UserId = (Guid)reader["ParentUserId"],
                                LastName = (string)reader["ParentLastName"],
                                FirstName = (string)reader["ParentFirstName"],
                                MiddleName = (string)reader["ParentMiddleName"],
                                PhoneNumber = (string)reader["PhoneNumber"]
                            };
                            (last.Parents as List<ExtendedUser>).Add(parent);
                        }

                        if (_addressId.HasValue && last.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var address = new ExtendedAddress();
                            address.AddressId = (long)reader["AddressId"];
                            address.AddressType = new ExtendedAddressType()
                            {
                                AddressTypeId = (int)reader["AddressTypeId"]
                            };
                            address.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) address.BtiCode = (int)reader["BtiCode"];
                            address.KladrCode = (string)reader["KladrCode"];
                            (last.Addresses as List<ExtendedAddress>).Add(address);
                        }
                    }
                    else
                    {
                        ExtendedPupil item = new ExtendedPupil();
                        item.UserId = (Guid)reader["PupilUserId"];
                        item.LastName = (string)reader["PupilLastName"];
                        item.FirstName = (string)reader["PupilFirstName"];
                        item.MiddleName = (string)reader["PupilMiddleName"];
                        item.Birthday = (DateTime)reader["Birthday"];
                        item.Sex = Convert.ToInt32((bool)reader["Sex"]);
                        item.AddressStr = (string)reader["AddressStr"];
                        item.SchoolName = (string)reader["SchoolName"];
                        item.ClassName = (string)reader["ClassName"];
                        item.Parents = new List<ExtendedUser>();

                        if (_parentId.HasValue)
                        {
                            var parent = new ExtendedUser()
                            {
                                UserId = (Guid)reader["ParentUserId"],
                                LastName = (string)reader["ParentLastName"],
                                FirstName = (string)reader["ParentFirstName"],
                                MiddleName = (string)reader["ParentMiddleName"],
                                PhoneNumber = (string)reader["PhoneNumber"]
                            };
                            (item.Parents as List<ExtendedUser>).Add(parent);
                        };

                        item.ChildUnions = new List<ExtendedChildUnion>();

                        if (_childUnionId.HasValue)
                        {
                            var itemChildUnion = new ExtendedChildUnion();
                            itemChildUnion.ChildUnionId = (long)reader["ChildUnionId"];
                            itemChildUnion.Name = (string)reader["ChildUnionName"];
                            itemChildUnion.UdodId = (long)reader["UdodId"];
                            itemChildUnion.UdodShortName = (string)reader["UdodShortName"];
                            itemChildUnion.Profile = new ExtendedProfile()
                            {
                                Name = (string)reader["ProfileName"]
                            };
                            itemChildUnion.Program = new ExtendedProgram()
                            {
                                Name = (string)reader["ProgramName"]
                            };
                            itemChildUnion.PupilBudgetType = new ExtendedTypeBudget()
                            {
                                TypeBudgetName = (string)reader["TypeBudgetName"]
                            };
                            itemChildUnion.PupilHours = (double)reader["Hours"];
                            itemChildUnion.PupilNumYear = (int)reader["NumYear"];
                            itemChildUnion.PupilAgeGroupId = (long)reader["AgeGroupPupilId"];
                            /*itemChildUnion.DateStart = (DateTime) reader["DateBegin"];
                            itemChildUnion.DateEnd1 = null;
                            if (reader["DateEnd"]!=DBNull.Value)
                            itemChildUnion.DateEnd1 = (DateTime)reader["DateEnd"];*/
                            itemChildUnion.AgeGroups = new List<ExtendedAgeGroup>();
                            Object dateOtch = ordDateOtch < 0 || reader.IsDBNull(ordDateOtch)
                                                  ? null
                                                  : reader["DateOtch"];
                            itemChildUnion.AgeGroups.Add(new ExtendedAgeGroup()
                            {
                                Name = (string)reader["GroupName"],
                                DateBegin = (DateTime)reader["DateZach"],
                                DateEnd = (DateTime?)dateOtch
                            });
                            

                            (item.ChildUnions as List<ExtendedChildUnion>).Add(itemChildUnion);
                        }

                        item.Addresses = new List<ExtendedAddress>();

                        if (_addressId.HasValue)
                        {
                            var address = new ExtendedAddress();
                            address.AddressId = (long)reader["AddressId"];
                            address.AddressType = new ExtendedAddressType()
                            {
                                AddressTypeId = (int)reader["AddressTypeId"]
                            };
                            address.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) address.BtiCode = (int)reader["BtiCode"];
                            address.KladrCode = (string)reader["KladrCode"];
                            (item.Addresses as List<ExtendedAddress>).Add(address);
                        }

                        list.Add(item);
                    }
                }
            }
            return list.OrderBy(i => i.Fio).ToList();
        }

        public ExtendedPupil GetPupil(Guid pupilId)
        {
            ExtendedPupil item = null;

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Pupil_GetPupil",conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("PupilId", pupilId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Guid? _parentId = null;
                    if (reader["ParentUserId"] != DBNull.Value) _parentId = (Guid)reader["ParentUserId"];

                    if (null != item)
                    {
                        if (_parentId.HasValue && item.Parents.Count(i => i.UserId == _parentId) == 0)
                        {
                            var parent = new ExtendedUser()
                            {
                                UserId = (Guid)reader["ParentUserId"],
                                LastName = (string)reader["ParentLastName"],
                                FirstName = (string)reader["ParentFirstName"],
                                MiddleName = (string)reader["ParentMiddleName"],
                                PhoneNumber = (string)reader["ParentPhoneNumber"],
                                ExpressPhoneNumber = (string)reader["ParentExpressPhoneNumber"],
                                EMail = (string)reader["ParentEmail"],
                                Passport = new ExtendedPassport()
                                {
                                    PassportType = new ExtendedPassportType()
                                    {
                                        PassportTypeId = (int)reader["ParentPassportTypeId"],
                                        PassportTypeName = (string)reader["ParentPassportTypeName"]
                                    },
                                    UserPassportId = (long)reader["ParentPassportId"],
                                    Series = (string)reader["ParentSeries"],
                                    Number = (string)reader["ParentNumber"],
                                    IssueDate = (DateTime)reader["ParentIssueDate"]
                                },
                                UserType = (int)reader["ParentUserType"]
                            };

                            (item.Parents as List<ExtendedUser>).Add(parent);
                        }
                    }
                    else
                    {
                        item = new ExtendedPupil();
                        item.UserId = (Guid)reader["PupilId"];
                        item.LastName = (string)reader["PupilLastName"];
                        item.FirstName = (string)reader["PupilFirstName"];
                        item.MiddleName = (string)reader["PupilMiddleName"];
                        item.Birthday = (DateTime)reader["Birthday"];
                        item.PhoneNumber = (string)reader["PupilPhoneNumber"];
                        item.ExpressPhoneNumber = (string)reader["PupilExpressPhoneNumber"];
                        item.EMail = (string)reader["PupilEmail"];
                        item.Sex = Convert.ToInt32((bool)reader["PupilSex"]);
                        item.SchoolName = (string)reader["SchoolName"];
                        item.ClassName = (string)reader["ClassName"];
                        item.Hours = (double)reader["PupilHours"];
                        item.ReestrCode = "";
                        item.DictSchoolId = null;
                        try
                        {
                            item.DictSchoolId = ((Int32)reader["DictSchoolId"]);
                        }
                        catch (Exception)
                        {
                        }
                        item.DictSchoolTypeId = 0;
                        try
                        {
                            item.DictSchoolTypeId = ((Int32)reader["TypeSchoolId"]);
                        }
                        catch (Exception)
                        {
                            item.DictSchoolTypeId = 0;
                        }
                        try
                        {
                            item.ReestrCode=((Int64)reader["ReestrCode"]).ToString();
                        }
                        catch (Exception)
                        {
                        }
                        item.ReestrUdodCode = "";
                        try
                        {
                            item.ReestrUdodCode = ((Int64)reader["ReestrUdodCode"]).ToString();
                        }
                        catch (Exception)
                        {
                        }
                        item.ReestrGuid = null;
                        if (reader["ReestrGuid"]!=DBNull.Value)
                        {
                            item.ReestrGuid = (Guid) reader["ReestrGuid"];
                        }

                        if (reader["PupilPassportId"] != DBNull.Value)
                            item.Passport = new ExtendedPassport()
                            {
                                PassportType = new ExtendedPassportType()
                                {
                                    PassportTypeId = (int)reader["PupilPassportTypeId"],
                                    PassportTypeName = (string)reader["PupilPassportTypeName"]
                                },
                                UserPassportId = (long)reader["PupilPassportId"],
                                Series = (string)reader["PupilSeries"],
                                Number = (string)reader["PupilNumber"],
                                IssueDate = (DateTime)reader["PupilIssueDate"]
                            };

                        item.Parents = new List<ExtendedUser>();

                        if (_parentId.HasValue)
                        {
                            var parent = new ExtendedUser()
                            {
                                UserId = (Guid)reader["ParentUserId"],
                                LastName = (string)reader["ParentLastName"],
                                FirstName = (string)reader["ParentFirstName"],
                                MiddleName = (string)reader["ParentMiddleName"],
                                PhoneNumber = (string)reader["ParentPhoneNumber"],
                                ExpressPhoneNumber = (string)reader["ParentExpressPhoneNumber"],
                                EMail = (string)reader["ParentEmail"],
                                Passport = new ExtendedPassport()
                                {
                                    PassportType = new ExtendedPassportType()
                                    {
                                        PassportTypeId = (int)reader["ParentPassportTypeId"],
                                        PassportTypeName = (string)reader["ParentPassportTypeName"]
                                    },
                                    UserPassportId = (long)reader["ParentPassportId"],
                                    Series = (string)reader["ParentSeries"],
                                    Number = (string)reader["ParentNumber"],
                                    IssueDate = (DateTime)reader["ParentIssueDate"]
                                },
                                UserType = (int)reader["ParentUserType"]
                            };

                            (item.Parents as List<ExtendedUser>).Add(parent);
                        }
                    }
                }
            }

            return item;
        }

        public List<ExtendedPupil> ReestrGetPupils(Guid? pupilId, int? cityId, long? udodId, string lastName, string firstName, string middleName)
        {
            List<ExtendedPupil> list = new List<ExtendedPupil>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Reestr_GetPupils", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (pupilId.HasValue) comm.Parameters.Add(new SqlParameter("PupilId", pupilId));
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (!string.IsNullOrEmpty(lastName)) comm.Parameters.Add(new SqlParameter("LastName", lastName));
                if (!string.IsNullOrEmpty(firstName)) comm.Parameters.Add(new SqlParameter("FirstName", firstName));
                if (!string.IsNullOrEmpty(middleName)) comm.Parameters.Add(new SqlParameter("MiddleName", middleName));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Guid _pupilId = (Guid)reader["PupilId"];

                    Guid? _parentId = null;
                    if (reader["ParentUserId"] != DBNull.Value) _parentId = (Guid)reader["ParentUserId"];

                    long? _addressId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long)reader["AddressId"];


                    ExtendedPupil last = list.LastOrDefault(i => i.UserId == _pupilId);

                    if (null != last)
                    {
                        if (_addressId.HasValue && last.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            ExtendedAddress address = new ExtendedAddress();
                            address.AddressId = (long)reader["AddressId"];
                            address.AddressStr = (string)reader["AddressStr"];
                            address.AddressEmail = (string)reader["AddressEmail"];
                            address.AddressWorkTime = (string)reader["AddressWorkTime"];
                            address.AddressPhone = (string)reader["AddressPhone"];
                            address.KladrCode = (string)reader["KladrCode"];
                            address.PostIndex = (string)reader["PostIndex"];
                            address.HouseNumber = (string)reader["HouseNumber"];
                            address.Fraction = (string)reader["Fraction"];
                            address.Housing = (string)reader["Housing"];
                            address.Building = (string)reader["Building"];
                            address.FlatNumber = (string)reader["Flat"];
                            address.AddressType = new ExtendedAddressType()
                            {
                                AddressTypeId = 5
                            };
                            address.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) address.BtiCode = (int)reader["BtiCode"];

                            address.BtiHouseCode = null;
                            if (reader["BtiHouseCode"] != DBNull.Value) address.BtiHouseCode = (int)reader["BtiHouseCode"];

                            last.Addresses.Add(address);
                        }

                        if (_parentId.HasValue && last.Parents.Count(i => i.UserId == _parentId) == 0)
                        {
                            var parent = new ExtendedUser()
                            {
                                UserId = (Guid)reader["ParentUserId"],
                                LastName = (string)reader["ParentLastName"],
                                FirstName = (string)reader["ParentFirstName"],
                                MiddleName = (string)reader["ParentMiddleName"],
                                PhoneNumber = (string)reader["ParentPhoneNumber"],
                                ExpressPhoneNumber = (string)reader["ParentExpressPhoneNumber"],
                                EMail = (string)reader["ParentEmail"],
                                Passport = new ExtendedPassport()
                                {
                                    PassportType = new ExtendedPassportType()
                                    {
                                        PassportTypeId = (int)reader["ParentPassportTypeId"],
                                        PassportTypeName = (string)reader["ParentPassportTypeName"]
                                    },
                                    UserPassportId = (long)reader["ParentPassportId"],
                                    Series = (string)reader["ParentSeries"],
                                    Number = (string)reader["ParentNumber"],
                                    IssueDate = (DateTime)reader["ParentIssueDate"]
                                },
                                UserType = (int)reader["ParentUserType"]
                            };

                            (last.Parents as List<ExtendedUser>).Add(parent);
                        }
                    }
                    else
                    {
                        ExtendedPupil item = new ExtendedPupil();
                        item.UserId = (Guid)reader["PupilId"];
                        item.LastName = (string)reader["PupilLastName"];
                        item.FirstName = (string)reader["PupilFirstName"];
                        item.MiddleName = (string)reader["PupilMiddleName"];
                        item.Birthday = (DateTime)reader["Birthday"];
                        item.PhoneNumber = (string)reader["PupilPhoneNumber"];
                        item.ExpressPhoneNumber = (string)reader["PupilExpressPhoneNumber"];
                        item.EMail = (string)reader["PupilEmail"];
                        item.Sex = Convert.ToInt32((bool)reader["PupilSex"]);
                        item.SchoolName = (string)reader["SchoolName"];
                        item.ClassName = (string)reader["ClassName"];
                        item.ReestrCode = "";
                        try
                        {
                            item.ReestrCode = ((Int64)reader["ReestrCode"]).ToString();
                        }
                        catch (Exception)
                        {
                        }
                        item.ReestrUdodCode = "";
                        try
                        {
                            item.ReestrUdodCode = ((Int64)reader["ReestrUdodCode"]).ToString();
                        }
                        catch (Exception)
                        {
                        }
                        item.ReestrGuid = null;
                        if (reader["ReestrGuid"] != DBNull.Value)
                        {
                            item.ReestrGuid = (Guid)reader["ReestrGuid"];
                        }

                        if (reader["PupilPassportId"] != DBNull.Value)
                            item.Passport = new ExtendedPassport()
                            {
                                PassportType = new ExtendedPassportType()
                                {
                                    PassportTypeId = (int)reader["PupilPassportTypeId"],
                                    PassportTypeName = (string)reader["PupilPassportTypeName"]
                                },
                                UserPassportId = (long)reader["PupilPassportId"],
                                Series = (string)reader["PupilSeries"],
                                Number = (string)reader["PupilNumber"],
                                IssueDate = (DateTime)reader["PupilIssueDate"]
                            };

                        item.Parents = new List<ExtendedUser>();

                        if (_parentId.HasValue)
                        {
                            var parent = new ExtendedUser()
                            {
                                UserId = (Guid)reader["ParentUserId"],
                                LastName = (string)reader["ParentLastName"],
                                FirstName = (string)reader["ParentFirstName"],
                                MiddleName = (string)reader["ParentMiddleName"],
                                PhoneNumber = (string)reader["ParentPhoneNumber"],
                                ExpressPhoneNumber = (string)reader["ParentExpressPhoneNumber"],
                                EMail = (string)reader["ParentEmail"],
                                Passport = new ExtendedPassport()
                                {
                                    PassportType = new ExtendedPassportType()
                                    {
                                        PassportTypeId = (int)reader["ParentPassportTypeId"],
                                        PassportTypeName = (string)reader["ParentPassportTypeName"]
                                    },
                                    UserPassportId = (long)reader["ParentPassportId"],
                                    Series = (string)reader["ParentSeries"],
                                    Number = (string)reader["ParentNumber"],
                                    IssueDate = (DateTime)reader["ParentIssueDate"]
                                },
                                UserType = (int)reader["ParentUserType"]
                            };

                            (item.Parents as List<ExtendedUser>).Add(parent);
                        }

                        item.Addresses = new List<ExtendedAddress>();
                        if (_addressId.HasValue)
                        {
                            ExtendedAddress address = new ExtendedAddress();
                            address.AddressId = (long)reader["AddressId"];
                            address.AddressStr = (string)reader["AddressStr"];
                            address.AddressEmail = (string)reader["AddressEmail"];
                            address.AddressWorkTime = (string)reader["AddressWorkTime"];
                            address.AddressPhone = (string)reader["AddressPhone"];
                            address.KladrCode = (string)reader["KladrCode"];
                            address.PostIndex = (string)reader["PostIndex"];
                            address.HouseNumber = (string)reader["HouseNumber"];
                            address.Fraction = (string)reader["Fraction"];
                            address.Housing = (string)reader["Housing"];
                            address.Building = (string)reader["Building"];
                            address.FlatNumber = (string)reader["Flat"];
                            address.AddressType = new ExtendedAddressType()
                            {
                                AddressTypeId = 5
                            };
                            address.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) address.BtiCode = (int)reader["BtiCode"];

                            address.BtiHouseCode = null;
                            if (reader["BtiHouseCode"] != DBNull.Value) address.BtiHouseCode = (int)reader["BtiHouseCode"];
                            
                            item.Addresses.Add(address);
                        }

                        list.Add(item);
                    }
                }
            }

            return list;
        }

        public List<ExtendedChildUnion> GetChildUnion(Guid pupilId)
        {
            List<ExtendedChildUnion> list = new List<ExtendedChildUnion>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Pupil_GetUdods", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("PupilId", pupilId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedChildUnion item = new ExtendedChildUnion();
                    item.ChildUnionId = (long)reader["ChildUnionId"];
                    item.AgeStart = (int)reader["AgeStart"];
                    item.AgeEnd = (int)reader["AgeEnd"];
                    item.Name = (string)reader["ChildUnionName"];
                    item.UdodId = (long) reader["UdodId"];
                    item.UdodShortName = (string)reader["UdodShortName"];
                    item.UdodPhone = (string)reader["UdodPhone"];
                    item.NumYears = (int)reader["NumYears"];
                    item.PupilAgeGroupId = (long)reader["AgeGroupPupilId"];
                    item.AgeGroups = new List<ExtendedAgeGroup>();
                    item.AgeGroups.Add(new ExtendedAgeGroup()
                    {
                        UdodAgeGroupId = (long)reader["UdodAgeGroupId"],
                        Age = (int)reader["Age"],
                        LessonLength = (int)reader["LessonLength"],
                        BreakLength = (int) reader["BreakLength"],
                        LessonsInWeek = (int) reader["LessonsInWeek"],
                        Name = (string)reader["UdodAgeGroupName"]
                    });
                    item.Section = new ExtendedSection()
                    {
                        SectionId = (int)reader["SectionId"],
                        Name = (string)reader["SectionName"]
                    };
                    item.Profile = new ExtendedProfile()
                    {
                        Name = (string)reader["ProfileName"]
                    };
                    /*item.YearOfStudy = new ExtendedYearOfStudy()
                    {
                        YearOfStudy = (int)reader["YearOfStudy"],
                        UdodYearOfStudyId = (long)reader["UdodYearOfStudyId"]
                    };*/
                    /*item.EducationType= new ExtendedEducationType()
                    {
                        
                        EducationTypeId = (int)reader["EducationTypeId"],
                        EducationTypeName = (string)reader["EducationTypeName"]
                    };*/
                    /*item.TypeBudget = new ExtendedTypeBudget()
                    {
                        DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                        TypeBudgetName = (string)reader["TypeBudgetName"]
                    };*/
                    //item.TeacherFio = (string) reader["TeacherFio"];
                    //item.TeacherId = (Guid) reader["TeacherId"];
                    item.Program = new ExtendedProgram()
                    {
                        ProgramId = (int)reader["ProgramId"],
                        Name = (string)reader["ProgramName"]
                    };
                    item.UdodId = (long) reader["UdodId"];

                    item.PupilBudgetType = new ExtendedTypeBudget()
                    {
                        TypeBudgetName = (string)reader["TypeBudgetName"]
                    };

                    item.PupilNumYear = (int)reader["PupilNumYear"];

                    item.PupilHours = (double)reader["Hours"];

                    list.Add(item);
                }
            }

            return list;
        }
        public int UpdateChildUnion(Guid pupilId, long childUnionOld, long childUnionNew, long groupId, int numYear)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Pupil_UpdateChildUnion", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("PupilId", pupilId));
                comm.Parameters.Add(new SqlParameter("ChildUnionIdOld", childUnionOld));
                comm.Parameters.Add(new SqlParameter("ChildUnionIdNew", childUnionNew));
                comm.Parameters.Add(new SqlParameter("GroupId", groupId));
                comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                //comm.ExecuteNonQuery();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }

            return 0;
        }

        public void UpdatePupil(Guid pupilId, ExtendedPupil pupildata)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Pupil_UpdatePupilProfile", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("PupilId", pupilId));
                comm.Parameters.Add(new SqlParameter("PupilLastName", pupildata.LastName));
                comm.Parameters.Add(new SqlParameter("PupilFirstName", pupildata.FirstName));
                comm.Parameters.Add(new SqlParameter("PupilMiddleName", pupildata.MiddleName));
                comm.Parameters.Add(new SqlParameter("Birthday", pupildata.Birthday));
                comm.Parameters.Add(new SqlParameter("PupilPhoneNumber", pupildata.PhoneNumber));
                if (!string.IsNullOrEmpty(pupildata.EMail)) comm.Parameters.Add(new SqlParameter("PupilEmail", pupildata.EMail));
                comm.Parameters.Add(new SqlParameter("PupilExpressPhoneNumber", pupildata.ExpressPhoneNumber));
                comm.Parameters.Add(new SqlParameter("PupilSex", pupildata.Sex));
                comm.Parameters.Add(new SqlParameter("PupilPassportTypeId", pupildata.Passport.PassportType.PassportTypeId));
                comm.Parameters.Add(new SqlParameter("PupilSeries", pupildata.Passport.Series));
                comm.Parameters.Add(new SqlParameter("PupilNumber", pupildata.Passport.Number));
                comm.Parameters.Add(new SqlParameter("PupilSchool", pupildata.SchoolName));
                comm.Parameters.Add(new SqlParameter("PupilClass", pupildata.ClassName));
                comm.Parameters.Add(new SqlParameter("SchoolTypeId", pupildata.DictSchoolTypeId));
                if (!string.IsNullOrEmpty(pupildata.SchoolCode)) comm.Parameters.Add(new SqlParameter("PupilSchoolCode", pupildata.SchoolCode));
                comm.Parameters.Add(new SqlParameter("ReestrCode", pupildata.ReestrCode));
                if (pupildata.ReestrGuid.HasValue) comm.Parameters.Add(new SqlParameter("ReestrGuidCode", pupildata.ReestrGuid.Value.ToString()));
                if (pupildata.Passport.IssueDate.HasValue) comm.Parameters.Add(new SqlParameter("PupilIssueDate", pupildata.Passport.IssueDate));

                comm.ExecuteNonQuery();
            }
        }

        public void UpdateParent(ExtendedUser parentData)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Pupil_UpdateParentProfile", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ParentId", parentData.UserId));
                comm.Parameters.Add(new SqlParameter("ParentLastName", parentData.LastName));
                comm.Parameters.Add(new SqlParameter("ParentFirstName", parentData.FirstName));
                comm.Parameters.Add(new SqlParameter("ParentMiddleName", parentData.MiddleName));
                comm.Parameters.Add(new SqlParameter("ParentPhoneNumber", parentData.PhoneNumber));
                comm.Parameters.Add(new SqlParameter("ParentExpressPhoneNumber", parentData.ExpressPhoneNumber));
                comm.Parameters.Add(new SqlParameter("ParentEmail", parentData.EMail));
                comm.Parameters.Add(new SqlParameter("ParentPassportTypeId", parentData.Passport.PassportType.PassportTypeId));
                comm.Parameters.Add(new SqlParameter("ParentSeries", parentData.Passport.Series));
                comm.Parameters.Add(new SqlParameter("ParentNumber", parentData.Passport.Number));
                comm.Parameters.Add(new SqlParameter("ParentIssueDate", parentData.Passport.IssueDate));
                comm.Parameters.Add(new SqlParameter("ParentUserType", parentData.UserType));

                comm.ExecuteNonQuery();
            }
        }

        public List<long> DeletePupil(Guid pupilId, long childUnionId, string reason)
        {
            List<long> list= new List<long>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Pupil_DeleteFromChildUnion", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("PupilId", pupilId));
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("Reason", reason));
                SqlDataReader reader= comm.ExecuteReader();
                while (reader.Read())
                {
                    if (reader[0] != DBNull.Value)
                    {
                        long reestrCode = (long) reader[0];
                        list.Add(reestrCode);
                    }
                }
            }
            return list;
        }

        public bool GetIsDismissed(long? claimId, Guid? pupilId, long? childUnionId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Pupil_GetIsDismissed", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (claimId.HasValue) comm.Parameters.Add(new SqlParameter("ClaimId", claimId));
                if (pupilId.HasValue) comm.Parameters.Add(new SqlParameter("pupilId", pupilId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("UdodChildUnionId", childUnionId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    if ((int)reader[0] > 0)
                        return false;
                    else
                        return true;
                }
            }
            return false;
        }

        public ExtendedLimit GetLimit(Guid pupilId, bool? onlyEnrolled)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Pupil_GetLimit", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("PupilId", pupilId));
                if (onlyEnrolled.HasValue) comm.Parameters.Add(new SqlParameter("onlyEnrolled", onlyEnrolled.Value));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedLimit item = new ExtendedLimit()
                    {
                        CountUdod = (int)reader["CountUdod"],
                        CountChildUnion = (int)reader["CountChildUnion"],
                        Hours = (double)reader["Hours"],
                        PaidHours = (double)reader["PaidHours"],
                        NormHours = (double)reader["NormHours"],
                        ClaimHours = (double)reader["ClaimHours"]
                    };
                    return item;
                }
            }
            return new ExtendedLimit(){CountUdod = 0, CountChildUnion = 0, Hours = 0};
        }

        public List<ExtendedUdod> GetLimitUdods(Guid pupilId)
        {
            List<ExtendedUdod> list = new List<ExtendedUdod>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Pupil_GetLimitUdods", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("PupilId", pupilId));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedUdod item = new ExtendedUdod()
                    {
                        UdodId = (long)reader["UdodId"]
                    };
                    list.Add(item);

                }
            }
            return list;
        }

        public List<ExtendedImportedPupil> GetImportedPupilsPaging(long udodId, string pupilLastName, string pupilFirstName, string pupilMiddleName, string childUnionName, int? numYear, long? childUnionId, int? dictTypeBudgetId, bool showDeleted, int? PagerIndex, int? PagerLength)
        {
            List<ExtendedImportedPupil> list = new List<ExtendedImportedPupil>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Imported_Udod_GetPupilsPaging", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (!string.IsNullOrEmpty(pupilLastName)) comm.Parameters.Add(new SqlParameter("PupilLastName", pupilLastName));
                if (!string.IsNullOrEmpty(pupilFirstName)) comm.Parameters.Add(new SqlParameter("PupilFirstName", pupilFirstName));
                if (!string.IsNullOrEmpty(pupilMiddleName)) comm.Parameters.Add(new SqlParameter("PupilMiddleName", pupilMiddleName));
                if (!string.IsNullOrEmpty(childUnionName)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnionName));
                if (numYear.HasValue) comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (dictTypeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("DictTypeBudgetId", dictTypeBudgetId));
                comm.Parameters.Add(new SqlParameter("ShowDeleted", showDeleted));
                if (PagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", PagerIndex));
                if (PagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", PagerLength));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedImportedPupil item = new ExtendedImportedPupil();
                    item.UserId = (Guid)reader["ImportedUserId"];
                    item.LastName = (string)reader["LastName"];
                    item.FirstName = (string)reader["FirstName"];
                    item.MiddleName = (string)reader["MiddleName"];
                    item.Birthday = (DateTime)reader["Birthday"];
                    item.AddressStr = (string)reader["AddressStr"];
                    item.ErrorMessage = (string)reader["ErrorMessage"];
                    item.Sex = Convert.ToInt32((bool)reader["Sex"]);
                    item.Checked = (bool)reader["IsSelected"];
                    item.IsEdit = (bool)reader["IsEdit"];
                    item.Passport = new ExtendedPassport()
                                        {
                                            Series = (string)reader["Series"],
                                            Number = (string)reader["Number"],
                                            IssueDate = (DateTime)reader["IssueDate"],
                                            PassportType = new ExtendedPassportType() { PassportTypeId = (int)reader["DocumentTypeId"] },
                                        };
                    item.Addresses = new List<ExtendedAddress>();
                    if (reader["RealAddressStr"]!=DBNull.Value)
                    {
                        ExtendedAddress address = new ExtendedAddress();
                        address.AddressStr = (string) reader["RealAddressStr"];
                        item.Addresses.Add(address);
                    }
                    else
                    {
                        ExtendedAddress address = new ExtendedAddress();
                        address.AddressStr = "";
                        item.Addresses.Add(address);
                    }
                    item.ChildUnions = new List<ExtendedChildUnion>();
                    item.ChildUnions.Add(new ExtendedChildUnion()
                        {
                            Name = (string)reader["ChildUnionName"],
                            ChildUnionId = reader["ChildUnionId"]!=DBNull.Value?(long)reader["ChildUnionId"]:0
                        });
                    item.NumYear = reader["NumYear"] != DBNull.Value ? (int) reader["NumYear"] : 0;
                    item.AddedChildUnionName = (string) reader["AddedChildUnionName"];
                    if (reader["DictTypeBudgetId"] != DBNull.Value)
                    {
                        item.TypeBudget = new ExtendedTypeBudget() { DictTypeBudgetId = (int)reader["DictTypeBudgetId"] };
                    }
                    else item.TypeBudget = new ExtendedTypeBudget();

                    
                    list.Add(item);
                    
                }
            }

            return list;
        }

        public ExtendedImportedPupil GetImportedPupil(Guid importedUserId)
        {
            ExtendedImportedPupil item = new ExtendedImportedPupil();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Imported_Udod_GetPupil", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UserId", importedUserId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    item.UserId = (Guid)reader["ImportedUserId"];
                    item.LastName = (string)reader["LastName"];
                    item.FirstName = (string)reader["FirstName"];
                    item.MiddleName = (string)reader["MiddleName"];
                    item.Birthday = (DateTime)reader["Birthday"];
                    item.AddressStr = (string)reader["AddressStr"];
                    item.Sex = Convert.ToInt32((bool)reader["Sex"]);
                    item.ErrorMessage = (string) reader["ErrorMessage"];
                    item.IsEdit = (bool)reader["IsEdit"];
                    item.ClassName = (string)reader["PupilClass"];
                    item.SchoolName = (string)reader["PupilSchool"];
                    item.EMail = (string)reader["EMail"];
                    item.PhoneNumber = (string)reader["PhoneNumber"];
                    item.Passport = new ExtendedPassport()
                    {
                        Series = (string)reader["Series"],
                        Number = (string)reader["Number"],
                        IssueDate = (DateTime)reader["IssueDate"],
                        PassportType = new ExtendedPassportType() { PassportTypeId = (int)reader["DocumentTypeId"] },
                    };
                    item.Addresses = new List<ExtendedAddress>();
                    
                    if (reader["AddressId"] != DBNull.Value)
                    {
                        ExtendedAddress address = new ExtendedAddress();
                        address.AddressId = (long) reader["AddressId"];
                        address.AddressStr = (string)reader["RealAddressStr"];
                        address.KladrCode = (string) reader["KladrCode"];
                        address.PostIndex = (string) reader["PostIndex"];
                        address.HouseNumber = (string) reader["HouseNumber"];
                        address.Fraction = (string) reader["Fraction"];
                        address.Housing = (string) reader["Housing"];
                        address.Building = (string) reader["Building"];
                        address.FlatNumber = (string) reader["Flat"];
                        address.AddressType = new ExtendedAddressType()
                            {
                                AddressTypeId = (int) reader["AddressTypeId"],
                                /*Name = (string) reader["Name"]*/
                            };
                        address.BtiCode = null;
                        if (reader["BtiCode"] != DBNull.Value) address.BtiCode = (int) reader["BtiCode"];

                        item.Addresses.Add(address);
                    }
                    item.ChildUnions = new List<ExtendedChildUnion>();
                    item.ChildUnions.Add(new ExtendedChildUnion()
                    {
                        Name = (string)reader["ChildUnionName"]
                    });
                    item.NumYear = reader["NumYear"] != DBNull.Value ? (int)reader["NumYear"] : 0;
                    item.AddedChildUnionName = (string)reader["AddedChildUnionName"];
                    if (reader["DictTypeBudgetId"] != DBNull.Value)
                    {
                        item.TypeBudget = new ExtendedTypeBudget() { DictTypeBudgetId = (int)reader["DictTypeBudgetId"] };
                    }
                    else item.TypeBudget = new ExtendedTypeBudget();

                    item.Parents= new List<ExtendedUser>();

                    var parent = new ExtendedUser()
                    {
                        UserId = (Guid)reader["ParentUserId"],
                        LastName = (string)reader["ParentLastName"],
                        FirstName = (string)reader["ParentFirstName"],
                        MiddleName = (string)reader["ParentMiddleName"],
                        PhoneNumber = (string)reader["ParentPhoneNumber"],
                        EMail = (string)reader["ParentEmail"],
                        Passport = new ExtendedPassport()
                        {
                            PassportType = new ExtendedPassportType()
                            {
                                PassportTypeId = (int)reader["ParentPassportTypeId"],
                                //PassportTypeName = (string)reader["ParentPassportTypeName"]
                            },
                            Series = (string)reader["ParentSeries"],
                            Number = (string)reader["ParentNumber"],
                            IssueDate = (DateTime)reader["ParentIssueDate"]
                        },
                    };

                    item.Parents.Add(parent);
                }
            }

            return item;
        }

        public int GetImportedPupilsCount(long udodId, string pupilLastName, string pupilFirstName, string pupilMiddleName, string childUnionName, int? numYear, long? childUnionId, int? dictTypeBudgetId, bool showDeleted, int? PagerIndex, int? PagerLength)
        {
            int countrecord = 0;
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Imported_Udod_GetPupilsCount", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (!string.IsNullOrEmpty(pupilLastName)) comm.Parameters.Add(new SqlParameter("PupilLastName", pupilLastName));
                if (!string.IsNullOrEmpty(pupilFirstName)) comm.Parameters.Add(new SqlParameter("PupilFirstName", pupilFirstName));
                if (!string.IsNullOrEmpty(pupilMiddleName)) comm.Parameters.Add(new SqlParameter("PupilMiddleName", pupilMiddleName));
                if (!string.IsNullOrEmpty(childUnionName)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnionName));
                if (numYear.HasValue) comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (dictTypeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("DictTypeBudgetId", dictTypeBudgetId));
                comm.Parameters.Add(new SqlParameter("ShowDeleted", showDeleted));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    countrecord = (int) reader[0];
                }
            }

            return countrecord;
        }

        public Guid ImportedAddPupil(Guid? userId, Guid parentId, long udodId, string lastName, string firstName, string middleName, DateTime birthDay, bool sex, int documentType, string series, string number, DateTime issueDate, string code, string postIndex, string houseNumber, string fraction, string housing, string building, string flatNumber, int addressTypeId, int? typeAddressCode, string addressStr, float coord_x, float coord_y, string eMail, string phoneNumber, string pupilschool, int? pupilSchoolCode, string pupilclass, string childUnionName, int? numYear, int? dicttypeBudgetId, long? childUnionId, bool? isEdit)
        {
            Guid PupilId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Imported_AddPupil", conn) { CommandType = CommandType.StoredProcedure };
                if (userId.HasValue) comm.Parameters.Add(new SqlParameter("ImportedUserId", userId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("ParentId", parentId));
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("LastName", lastName));
                comm.Parameters.Add(new SqlParameter("FirstName", firstName));
                comm.Parameters.Add(new SqlParameter("MiddleName", middleName));
                comm.Parameters.Add(new SqlParameter("Birthday", birthDay));
                comm.Parameters.Add(new SqlParameter("Sex", sex));
                comm.Parameters.Add(new SqlParameter("DocumentType", documentType));
                comm.Parameters.Add(new SqlParameter("Series", series.Trim()));
                comm.Parameters.Add(new SqlParameter("Number", number.Trim()));
                comm.Parameters.Add(new SqlParameter("IssueDate", issueDate));
                comm.Parameters.Add(new SqlParameter("EMail", eMail));
                if (dicttypeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("DictTypeBudgetId", dicttypeBudgetId));
                comm.Parameters.Add(new SqlParameter("PhoneNumber", phoneNumber));
                comm.Parameters.Add(new SqlParameter("AddressStr", addressStr));
                comm.Parameters.Add(new SqlParameter("KladrCode", code));
                if (!string.IsNullOrEmpty(code))
                {
                    comm.Parameters.Add(new SqlParameter("RealAddressStr", (new KladrDb()).GetAddress(code, houseNumber, fraction, housing, building, flatNumber, typeAddressCode).AddressStr));
                }
                else
                {
                    comm.Parameters.Add(new SqlParameter("RealAddressStr", ""));
                }

                if (!string.IsNullOrEmpty(pupilschool)) comm.Parameters.Add(new SqlParameter("PupilSchool", pupilschool));
                if (pupilSchoolCode.HasValue) comm.Parameters.Add(new SqlParameter("PupilSchoolCode", pupilSchoolCode));
                if (!string.IsNullOrEmpty(pupilclass)) comm.Parameters.Add(new SqlParameter("PupilClass", pupilclass));
                
                comm.Parameters.Add(new SqlParameter("PostIndex", postIndex));
                comm.Parameters.Add(new SqlParameter("HouseNumber", houseNumber));
                comm.Parameters.Add(new SqlParameter("Fraction", fraction));
                comm.Parameters.Add(new SqlParameter("Housing", housing));
                comm.Parameters.Add(new SqlParameter("Building", building));
                comm.Parameters.Add(new SqlParameter("Flat", flatNumber));
                comm.Parameters.Add(new SqlParameter("AddressTypeId", addressTypeId));

                comm.Parameters.Add(typeAddressCode.HasValue
                                        ? new SqlParameter("typeAddressCode", typeAddressCode.Value.ToString())
                                        : new SqlParameter("typeAddressCode", 1.ToString()));

                comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnionName));
                if (numYear.HasValue) comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                if (isEdit.HasValue) comm.Parameters.Add(new SqlParameter("IsEdit", isEdit));
                
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    PupilId = (Guid)reader[0];
                }
            }
            return PupilId;
        }

        public Guid ImportedAddParent(Guid? userId, string lastName, string firstName, string middleName, int documentType, string series, string number, DateTime issueDate, string eMail, string phoneNumber)
        {
            Guid parentId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Imported_AddParent", conn) { CommandType = CommandType.StoredProcedure };
                if (userId.HasValue) comm.Parameters.Add(new SqlParameter("ImportedUserId", userId));
                comm.Parameters.Add(new SqlParameter("LastName", lastName));
                comm.Parameters.Add(new SqlParameter("FirstName", firstName));
                comm.Parameters.Add(new SqlParameter("MiddleName", middleName));
                comm.Parameters.Add(new SqlParameter("DocumentType", documentType));
                comm.Parameters.Add(new SqlParameter("Series", series.Trim()));
                comm.Parameters.Add(new SqlParameter("Number", number.Trim()));
                comm.Parameters.Add(new SqlParameter("IssueDate", issueDate));
                comm.Parameters.Add(new SqlParameter("EMail", eMail));
                comm.Parameters.Add(new SqlParameter("PhoneNumber", phoneNumber));
                
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    parentId = (Guid)reader[0];
                }
            }
            return parentId;
        }

        public void DeleteImportedPupil(Guid userId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Imported_DeletePupil", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ImportedUserId", userId));
                comm.ExecuteNonQuery();
                
            }
        }
        public void ImportedCheckPupil(Guid userId, bool checkValue)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Imported_CheckPupil", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ImportedUserId", userId));
                comm.Parameters.Add(new SqlParameter("CheckValue", checkValue));
                comm.ExecuteNonQuery();

            }
        }
        public void ImportedIsEditPupil(Guid userId, bool checkValue)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Imported_SetEditPupil", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ImportedUserId", userId));
                comm.Parameters.Add(new SqlParameter("CheckValue", checkValue));
                comm.ExecuteNonQuery();

            }
        }
        public Guid ImportedAddPupilInAgegroup(Guid userId, long? childUnionId)
        {
            Guid returnvalue = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Imported_AddPupilInAgeGroup", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ImportedUserId", userId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    returnvalue = (Guid) reader[0];
                }
            }
            return returnvalue;
        }

        public List<string> ImportedGetChildUnions(long udodId)
        {
            List<string> list= new List<string>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Imported_GetChildUnions", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    list.Add((string)reader["ChildUnionName"]);
                }
            }
            return list;
        }

        public List<Guid> FindSimilarPupil(string LastName, string FirstName, int DocumentType, string Series, string Number, DateTime Birthday, Guid PupilId)
        {
            List<Guid> list = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Pupil_FindSimilarPupil", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("LastName", LastName));
                comm.Parameters.Add(new SqlParameter("FirstName", FirstName));
                comm.Parameters.Add(new SqlParameter("DocumentType", DocumentType));
                comm.Parameters.Add(new SqlParameter("Series", Series));
                comm.Parameters.Add(new SqlParameter("Number", Number));
                comm.Parameters.Add(new SqlParameter("Birthday", Birthday));
                comm.Parameters.Add(new SqlParameter("PupilId", PupilId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    list.Add((Guid)reader["UserId"]);
                }
            }
            return list;
        }

        public void MergePupils(Guid CurrentPupil, Guid ExistPupil)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Pupil_MergePupils", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("CurrentPupil", CurrentPupil));
                comm.Parameters.Add(new SqlParameter("ExistPupil", ExistPupil));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteParent(Guid parentId, Guid pupilId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Pupil_DeleteParent", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("parentId", parentId));
                comm.Parameters.Add(new SqlParameter("pupilId", pupilId));
                comm.ExecuteNonQuery();
            }
        }

        #region Функции школ

        public List<ExtendedImportedPupil> GetSchoolPupilsPaging(long? udodId, string pupilLastName, string pupilFirstName, string pupilMiddleName, string childUnionName, bool showDeleted, string className, int? isSchool, int? PagerIndex, int? PagerLength)
        {
            List<ExtendedImportedPupil> list = new List<ExtendedImportedPupil>();
            if (!udodId.HasValue) return list;
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("School_Udod_GetPupilsPaging", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (!string.IsNullOrEmpty(pupilLastName)) comm.Parameters.Add(new SqlParameter("PupilLastName", pupilLastName));
                if (!string.IsNullOrEmpty(pupilFirstName)) comm.Parameters.Add(new SqlParameter("PupilFirstName", pupilFirstName));
                if (!string.IsNullOrEmpty(pupilMiddleName)) comm.Parameters.Add(new SqlParameter("PupilMiddleName", pupilMiddleName));
                if (!string.IsNullOrEmpty(childUnionName)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnionName));
                if (!string.IsNullOrEmpty(className)) comm.Parameters.Add(new SqlParameter("Class", className));
                comm.Parameters.Add(new SqlParameter("ShowDeleted", showDeleted));
                if (isSchool.HasValue) comm.Parameters.Add(new SqlParameter("IsSchool", isSchool));
                if (PagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", PagerIndex));
                if (PagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", PagerLength));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Guid _pupilId = (Guid)reader["SchoolUserId"];
                    long? _childUnionId = null;
                    if (reader["ChildUnionId"] != DBNull.Value) _childUnionId = (long)reader["ChildUnionId"];

                    ExtendedPupil last = list.LastOrDefault(i => i.UserId == _pupilId);

                    if (null != last)
                    {
                        if (_childUnionId.HasValue && last.ChildUnions.Count(i => i.ChildUnionId == _childUnionId) == 0)
                        {
                            var itemChildUnion = new ExtendedChildUnion();
                            itemChildUnion.ChildUnionId = (long)reader["ChildUnionId"];
                            itemChildUnion.Name = (string)reader["ChildUnionName"];
                            last.ChildUnions.Add(itemChildUnion);
                        }
                    }
                    else
                    {
                        ExtendedImportedPupil item = new ExtendedImportedPupil();
                        item.UserId = (Guid) reader["SchoolUserId"];
                        item.LastName = (string) reader["LastName"];
                        item.FirstName = (string) reader["FirstName"];
                        item.MiddleName = (string) reader["MiddleName"];
                        item.Birthday = (DateTime) reader["Birthday"];
                        item.AddressStr = (string) reader["AddressStr"];
                        item.ErrorMessage = (string) reader["ErrorMessage"];
                        item.Sex = Convert.ToInt32((bool) reader["Sex"]);
                        item.Checked = (bool) reader["IsSelected"];
                        item.IsEdit = (bool) reader["IsEdit"];
                        item.Passport = new ExtendedPassport()
                                            {
                                                Series = (string) reader["Series"],
                                                Number = (string) reader["Number"],
                                                IssueDate = (DateTime) reader["IssueDate"],
                                                PassportType =
                                                    new ExtendedPassportType()
                                                        {PassportTypeId = (int) reader["DocumentTypeId"]},
                                            };
                        item.Addresses = new List<ExtendedAddress>();
                        if (reader["RealAddressStr"] != DBNull.Value)
                        {
                            ExtendedAddress address = new ExtendedAddress();
                            address.AddressStr = (string) reader["RealAddressStr"];
                            item.Addresses.Add(address);
                        }
                        else
                        {
                            ExtendedAddress address = new ExtendedAddress();
                            address.AddressStr = "";
                            item.Addresses.Add(address);
                        }
                        item.ChildUnions = new List<ExtendedChildUnion>();

                        if (_childUnionId.HasValue)
                        {
                            var childunion = new ExtendedChildUnion();
                            childunion.ChildUnionId = _childUnionId.Value;
                            childunion.Name = (string) reader["ChildUnionName"];
                            item.ChildUnions.Add(childunion);
                        }

                        item.NumYear = reader["NumYear"] != DBNull.Value ? (int) reader["NumYear"] : 0;
                        if (reader["DictTypeBudgetId"] != DBNull.Value)
                        {
                            item.TypeBudget = new ExtendedTypeBudget()
                                                  {DictTypeBudgetId = (int) reader["DictTypeBudgetId"]};
                        }
                        else item.TypeBudget = new ExtendedTypeBudget();


                        list.Add(item);
                    }
                }
            }

            return list;
        }

        public int GetSchoolPupilsCount(long udodId, string pupilLastName, string pupilFirstName, string pupilMiddleName, string childUnionName, bool showDeleted, string className, int? isSchool, int? PagerIndex, int? PagerLength)
        {
            int countrecord = 0;
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("School_Udod_GetPupilsCount", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (!string.IsNullOrEmpty(pupilLastName)) comm.Parameters.Add(new SqlParameter("PupilLastName", pupilLastName));
                if (!string.IsNullOrEmpty(pupilFirstName)) comm.Parameters.Add(new SqlParameter("PupilFirstName", pupilFirstName));
                if (!string.IsNullOrEmpty(pupilMiddleName)) comm.Parameters.Add(new SqlParameter("PupilMiddleName", pupilMiddleName));
                if (!string.IsNullOrEmpty(childUnionName)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnionName));
                if (!string.IsNullOrEmpty(className)) comm.Parameters.Add(new SqlParameter("Class", className));
                comm.Parameters.Add(new SqlParameter("ShowDeleted", showDeleted));
                if (isSchool.HasValue) comm.Parameters.Add(new SqlParameter("IsSchool", isSchool));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    countrecord = (int)reader[0];
                }
            }

            return countrecord;
        }

        public ExtendedImportedPupil GetSchoolPupil(Guid importedUserId)
        {
            ExtendedImportedPupil item = new ExtendedImportedPupil();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("School_Udod_GetPupil", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UserId", importedUserId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    item.UserId = (Guid)reader["SchoolUserId"];
                    item.LastName = (string)reader["LastName"];
                    item.FirstName = (string)reader["FirstName"];
                    item.MiddleName = (string)reader["MiddleName"];
                    item.Birthday = (DateTime)reader["Birthday"];
                    item.AddressStr = (string)reader["AddressStr"];
                    item.Sex = Convert.ToInt32((bool)reader["Sex"]);
                    item.ErrorMessage = (string)reader["ErrorMessage"];
                    item.IsEdit = (bool)reader["IsEdit"];
                    item.ClassName = (string)reader["PupilClass"];
                    item.SchoolName = (string)reader["PupilSchool"];
                    item.EMail = (string)reader["EMail"];
                    item.PhoneNumber = (string)reader["PhoneNumber"];
                    if (reader["ReestrCode"]!=DBNull.Value) 
                        item.ReestrCode = (string) reader["ReestrCode"];
                    item.Passport = new ExtendedPassport()
                    {
                        Series = (string)reader["Series"],
                        Number = (string)reader["Number"],
                        IssueDate = (DateTime)reader["IssueDate"],
                        PassportType = new ExtendedPassportType() { PassportTypeId = (int)reader["DocumentTypeId"] },
                    };
                    item.Addresses = new List<ExtendedAddress>();

                    if (reader["AddressId"] != DBNull.Value)
                    {
                        ExtendedAddress address = new ExtendedAddress();
                        address.AddressId = (long)reader["AddressId"];
                        address.AddressStr = (string)reader["RealAddressStr"];
                        address.KladrCode = (string)reader["KladrCode"];
                        address.PostIndex = (string)reader["PostIndex"];
                        address.HouseNumber = (string)reader["HouseNumber"];
                        address.Fraction = (string)reader["Fraction"];
                        address.Housing = (string)reader["Housing"];
                        address.Building = (string)reader["Building"];
                        address.FlatNumber = (string)reader["Flat"];
                        address.AddressType = new ExtendedAddressType()
                        {
                            AddressTypeId = (int)reader["AddressTypeId"],
                            /*Name = (string) reader["Name"]*/
                        };
                        address.BtiCode = null;
                        if (reader["BtiCode"] != DBNull.Value) address.BtiCode = (int)reader["BtiCode"];

                        item.Addresses.Add(address);
                    }
                    item.ChildUnions = new List<ExtendedChildUnion>();
                    item.NumYear = reader["NumYear"] != DBNull.Value ? (int)reader["NumYear"] : 0;
                    if (reader["DictTypeBudgetId"] != DBNull.Value)
                    {
                        item.TypeBudget = new ExtendedTypeBudget() { DictTypeBudgetId = (int)reader["DictTypeBudgetId"] };
                    }
                    else item.TypeBudget = new ExtendedTypeBudget();

                    item.Parents = new List<ExtendedUser>();
                    /*
                    var parent = new ExtendedUser()
                    {
                        UserId = (Guid)reader["ParentUserId"],
                        LastName = (string)reader["ParentLastName"],
                        FirstName = (string)reader["ParentFirstName"],
                        MiddleName = (string)reader["ParentMiddleName"],
                        PhoneNumber = (string)reader["ParentPhoneNumber"],
                        EMail = (string)reader["ParentEmail"],
                        Passport = new ExtendedPassport()
                        {
                            PassportType = new ExtendedPassportType()
                            {
                                PassportTypeId = (int)reader["ParentPassportTypeId"],
                                //PassportTypeName = (string)reader["ParentPassportTypeName"]
                            },
                            Series = (string)reader["ParentSeries"],
                            Number = (string)reader["ParentNumber"],
                            IssueDate = (DateTime)reader["ParentIssueDate"]
                        },
                    };

                    item.Parents.Add(parent);*/
                }
            }

            return item;
        }
        public Guid SchoolAddPupilInAgegroup(Guid userId, long childUnionId, int numYear, long udodAgeGroupId)
        {
            Guid returnvalue = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("School_AddPupilInAgeGroup", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("SchoolUserId", userId));
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                comm.Parameters.Add(new SqlParameter("GroupId", udodAgeGroupId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    returnvalue = (Guid)reader[0];
                }
            }
            return returnvalue;
        }

        public Guid SchoolAddPupil(Guid? userId, Guid parentId, long udodId, string lastName, string firstName, string middleName, DateTime birthDay, bool sex, int documentType, string series, string number, DateTime issueDate, string code, string postIndex, string houseNumber, string fraction, string housing, string building, string flatNumber, int addressTypeId, int? typeAddressCode, string addressStr, float coord_x, float coord_y, string eMail, string phoneNumber, string pupilschool, int? pupilSchoolCode, string pupilclass, string childUnionName, int numYear, int? dicttypeBudgetId, bool? isEdit, bool isSchool, string reestrCode, Guid? reestrGuidCode)
        {
            Guid PupilId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("School_AddPupil", conn) { CommandType = CommandType.StoredProcedure };
                if (userId.HasValue) comm.Parameters.Add(new SqlParameter("SchoolUserId", userId));
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("LastName", lastName));
                comm.Parameters.Add(new SqlParameter("FirstName", firstName));
                comm.Parameters.Add(new SqlParameter("MiddleName", middleName));
                comm.Parameters.Add(new SqlParameter("Birthday", birthDay));
                comm.Parameters.Add(new SqlParameter("Sex", sex));
                comm.Parameters.Add(new SqlParameter("DocumentType", documentType));
                comm.Parameters.Add(new SqlParameter("Series", series.Trim()));
                comm.Parameters.Add(new SqlParameter("Number", number.Trim()));
                comm.Parameters.Add(new SqlParameter("IssueDate", issueDate));
                comm.Parameters.Add(new SqlParameter("EMail", eMail));
                if (dicttypeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("DictTypeBudgetId", dicttypeBudgetId));
                comm.Parameters.Add(new SqlParameter("PhoneNumber", phoneNumber));
                comm.Parameters.Add(new SqlParameter("AddressStr", addressStr));
                comm.Parameters.Add(new SqlParameter("KladrCode", code));
                if (!string.IsNullOrEmpty(code))
                {
                    comm.Parameters.Add(new SqlParameter("RealAddressStr", (new KladrDb()).GetAddress(code, houseNumber, fraction, housing, building, flatNumber, typeAddressCode).AddressStr));
                }
                else
                {
                    comm.Parameters.Add(new SqlParameter("RealAddressStr", ""));
                }

                if (!string.IsNullOrEmpty(pupilschool)) comm.Parameters.Add(new SqlParameter("PupilSchool", pupilschool));
                if (pupilSchoolCode.HasValue) comm.Parameters.Add(new SqlParameter("PupilSchoolCode", pupilSchoolCode));
                if (!string.IsNullOrEmpty(pupilclass)) comm.Parameters.Add(new SqlParameter("PupilClass", pupilclass));

                comm.Parameters.Add(new SqlParameter("PostIndex", postIndex));
                comm.Parameters.Add(new SqlParameter("HouseNumber", houseNumber));
                comm.Parameters.Add(new SqlParameter("Fraction", fraction));
                comm.Parameters.Add(new SqlParameter("Housing", housing));
                comm.Parameters.Add(new SqlParameter("Building", building));
                comm.Parameters.Add(new SqlParameter("Flat", flatNumber));
                comm.Parameters.Add(new SqlParameter("AddressTypeId", addressTypeId));

                comm.Parameters.Add(typeAddressCode.HasValue
                                        ? new SqlParameter("typeAddressCode", typeAddressCode.Value.ToString())
                                        : new SqlParameter("typeAddressCode", 1.ToString()));

                comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                comm.Parameters.Add(new SqlParameter("IsSchool", isSchool));
                comm.Parameters.Add(new SqlParameter("ReestrCode", reestrCode));
                if (reestrGuidCode.HasValue) comm.Parameters.Add(new SqlParameter("ReestrGuidCode", reestrGuidCode.Value));
                if (isEdit.HasValue) comm.Parameters.Add(new SqlParameter("IsEdit", isEdit));

                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    PupilId = (Guid)reader[0];
                }
            }
            return PupilId;
        }

        public void SchoolCheckPupil(Guid userId, bool checkValue)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("School_CheckPupil", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("SchoolUserId", userId));
                comm.Parameters.Add(new SqlParameter("CheckValue", checkValue));
                comm.ExecuteNonQuery();

            }
        }

        public void SchoolSetDeleted(long udodId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("School_SetDeleted", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.ExecuteNonQuery();

            }
        }

        public void SchoolDeletePupils(long udodId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("School_DeletePupils", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.ExecuteNonQuery();

            }
        }

        public void DeleteSchoolPupil(Guid userId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("School_DeletePupil", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("SchoolUserId", userId));
                comm.ExecuteNonQuery();

            }
        }

        public void SchoolDeSelect(long udodId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("School_DeSelect", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.ExecuteNonQuery();

            }
        }

        public List<string> SchoolGetClasses(long udodId)
        {
            List<string> list = new List<string>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("School_GetClasses", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    list.Add((string)reader[0]);
                }

            }

            return list;
        }

        public List<ExtendedPupil> GetNotReestrCodePupils(int? cityId, long? udodId)
        {
            List<ExtendedPupil> list = new List<ExtendedPupil>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetNotReestrCodePupil", conn) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId.Value));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId.Value));
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedPupil item = new ExtendedPupil();
                    item.UserId = (Guid)reader["PupilUserId"];
                    item.LastName = (string)reader["PupilLastName"];
                    item.FirstName = (string)reader["PupilFirstName"];
                    item.MiddleName = (string)reader["PupilMiddleName"];
                    item.ErrorMessage = (string) reader["ErrorMessage"];
                    if (reader["PupilPassportId"] != DBNull.Value)
                    {
                        item.Passport = new ExtendedPassport()
                                            {
                                                PassportType = new ExtendedPassportType()
                                                                   {
                                                                       PassportTypeId =
                                                                           (int) reader["PupilPassportTypeId"],
                                                                       PassportTypeName =
                                                                           (string) reader["PupilPassportTypeName"]
                                                                   },
                                                UserPassportId = (long) reader["PupilPassportId"],
                                                Series = (string) reader["PupilSeries"],
                                                Number = (string) reader["PupilNumber"],
                                                IssueDate = (DateTime) reader["PupilIssueDate"]
                                            };
                    }

                    list.Add(item);
                }

            }

            return list;
        }

        public void UpdatePupilReestrCode(Guid userId, long? reestrCode, long? reestrUdodCode, Guid? reestrGuid, string errorMessage, int? ekisId, string nameSchool, string nameKlass)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("UpdatePupilReestrCode", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UserId", userId));
                if (reestrCode.HasValue) comm.Parameters.Add(new SqlParameter("ReestrCode", reestrCode));
                if (reestrUdodCode.HasValue) comm.Parameters.Add(new SqlParameter("ReestrUdodCode", reestrUdodCode));
                if (reestrGuid.HasValue) comm.Parameters.Add(new SqlParameter("ReestrGuid", reestrGuid));
                if (ekisId.HasValue) comm.Parameters.Add(new SqlParameter("EkisId", ekisId));
                if (!string.IsNullOrEmpty(errorMessage)) comm.Parameters.Add(new SqlParameter("UpdateError", errorMessage));
                if (!string.IsNullOrEmpty(nameKlass)) comm.Parameters.Add(new SqlParameter("NameKlass", nameKlass));
                if (!string.IsNullOrEmpty(nameSchool)) comm.Parameters.Add(new SqlParameter("NameSchool", nameSchool));
                comm.ExecuteNonQuery();

            }
        }

        public void UpdatePupilZachReestrCode(long pupilAgeGroupId, long reestrCode)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("UpdatePupilZachReestrCode", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("PupilAgeGroupId", pupilAgeGroupId));
                comm.Parameters.Add(new SqlParameter("ReestrCode", reestrCode));
                comm.ExecuteNonQuery();

            }
        }
        #endregion
    }
}

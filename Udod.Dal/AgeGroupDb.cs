﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Udod.Dal
{
    public class AgeGroupDb : BaseDb
    {
        public int GetGroupsCountForTeacher(long childUnionId, long childUnionTeacherId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_GetAgeGroupCountForTeacher", conn) {CommandType = CommandType.StoredProcedure};
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("ChildUnionTeacherId", childUnionTeacherId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }

            }
            return 0;
        }

        public List<ExtendedAgeGroup> GetAgeGroups(int? cityId, long? udodId, int? sectionId, long? childUnionId)
        {
            List<ExtendedAgeGroup> list = new List<ExtendedAgeGroup>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_GetAgeGroup", conn) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));

                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedAgeGroup item = new ExtendedAgeGroup();
                    item.Age = (int) reader["Age"];
                    item.ChildUnionName = (string)reader["Name"];
                    item.Section = new ExtendedSection()
                    {
                        Name = (string)reader["SectionName"],
                        SectionId = (int)reader["SectionId"],
                        ProfileId = (int)reader["ProfileId"]
                    };
                    
                    item.UdodAgeGroupId = (long) reader["UdodAgeGroupId"];
                    item.UdodId = (long)reader["UdodId"];
                    item.ChildUnionId = (long)reader["ChildUnionId"];
                    item.MaxCountPupil = (int)reader["MaxCountPupil"];

                    item.NumYearStart = (int)reader["NumYearStart"];
                    item.NumYearEnd = (int)reader["NumYearEnd"];
                    item.AgeStart = (int)reader["AgeStart"];
                    item.AgeEnd = (int)reader["AgeEnd"];
                    
                    item.LessonLength = (int)reader["LessonLength"];
                    item.BreakLength = (int)reader["BreakLength"];
                    item.LessonsInWeek = (int)reader["LessonsInWeek"];

                    item.IsActive = (bool)reader["IsActive"];
                    /*item.YearOfStudy = new ExtendedYearOfStudy()
                    {
                        YearOfStudy = (int)reader["YearOfStudy"],
                        UdodYearOfStudyId = (long)reader["UdodYearOfStudyId"],
                        UdodId = (long)reader["UdodId"]
                    };
                    */
                    item.Program = new ExtendedProgram()
                    {
                        ProgramId = (int)reader["ProgramId"],
                        Name = (string)reader["ProgramName"]
                    };
                    //item.TeacherFio = (string) reader["Fio"];
                    //item.TeacherId = (Guid)reader["TeacherId"];
                    list.Add(item);
                }

            }

            return list;
        }

        public List<ExtendedGroup> GetGroupsWithoutFirstYear(long? udodId, string nameGroup, long? childunionId, Guid? teacherId, int? numYear, int? dictTypeBudgetId, string orderBy, int pagerIndex, int pagerLength, bool? isReadyGroup)
        {
            List<ExtendedGroup> list = new List<ExtendedGroup>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_GetGroupsWithoutFirstYear", conn) { CommandType = CommandType.StoredProcedure };
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (childunionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childunionId));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                if (numYear.HasValue) comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                if (dictTypeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("dictTypeBudgetId", dictTypeBudgetId));
                if (!string.IsNullOrEmpty(nameGroup)) comm.Parameters.Add(new SqlParameter("NameGroup", nameGroup));
                if (!string.IsNullOrEmpty(orderBy)) comm.Parameters.Add(new SqlParameter("OrderBy", orderBy));
                comm.Parameters.Add(new SqlParameter("pagerIndex", pagerIndex));
                comm.Parameters.Add(new SqlParameter("pagerLength", pagerLength));
                if (isReadyGroup.HasValue) comm.Parameters.Add(new SqlParameter("IsReadyGroup", isReadyGroup));

                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long _udodGroupId = (long)reader["UdodAgeGroupId"];
                    long? _addressId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long)reader["AddressId"];
                    Guid? _teacherId = null;
                    if (reader["TeacherId"] != DBNull.Value) _teacherId = (Guid)reader["TeacherId"];
                    int? _numYear = null;
                    if (reader["NumYear"] != DBNull.Value) _numYear = (int)reader["NumYear"];

                    ExtendedGroup last = list.LastOrDefault(i => i.UdodAgeGroupId == _udodGroupId);
                    if (last != null)
                    {
                        if (_addressId.HasValue && last.Adresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var address = new ExtendedAddress();
                            address.AddressId = _addressId.Value;
                            address.AddressStr = (string)reader["AddressStr"];
                            last.Adresses.Add(address);
                        }
                        if (_teacherId.HasValue && last.Teachers.Count(i => i.UserId == _teacherId) == 0)
                        {
                            var teacher = new ExtendedTeacher();
                            teacher.UserId = _teacherId.Value;
                            teacher.LastName = (string)reader["LastName"];
                            teacher.FirstName = (string)reader["FirstName"];
                            teacher.MiddleName = (string)reader["MiddleName"];
                            last.Teachers.Add(teacher);
                        }
                        if (_numYear.HasValue && last.NumYears.Count(i => i == _numYear) == 0)
                        {
                            last.NumYears.Add(_numYear.Value);
                        }
                    }
                    else
                    {
                        ExtendedGroup item = new ExtendedGroup();
                        item.Name = (string)reader["GroupName"];
                        item.AgeStart = (int)reader["AgeStart"];
                        item.AgeEnd = (int)reader["AgeEnd"];
                        item.UdodAgeGroupId = _udodGroupId;
                        item.TypeBudget = new ExtendedTypeBudget()
                        {
                            DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                            TypeBudgetName = (string)reader["TypeBudgetName"]
                        };
                        item.EducationType = new ExtendedEducationType()
                        {
                            EducationTypeId = (int)reader["EducationTypeId"],
                            EducationTypeName = (string)reader["EducationTypeName"]
                        };

                        item.UdodId = (long)reader["UdodId"];
                        item.Comment = (string)reader["Comment"];
                        item.ChildUnionName = (string)reader["ChildUnionName"];
                        item.ChildUnionId = (long)reader["ChildUnionId"];
                        item.CurrentCountPupil = (int)reader["CountPupils"];
                        item.MaxCountPupil = (int)reader["MaxCountPupil"];

                        item.NumYears = new List<int>();
                        if (_numYear.HasValue)
                        {
                            item.NumYears.Add(_numYear.Value);
                        }


                        item.Teachers = new List<ExtendedTeacher>();
                        if (_teacherId.HasValue)
                        {
                            var teacher = new ExtendedTeacher()
                            {
                                LastName = (string)reader["LastName"],
                                FirstName = (string)reader["FirstName"],
                                MiddleName = (string)reader["MiddleName"],
                                UserId = _teacherId.Value
                            };
                            item.Teachers.Add(teacher);
                        }
                        item.Adresses = new List<ExtendedAddress>();
                        if (_addressId.HasValue)
                        {
                            var address = new ExtendedAddress()
                            {
                                AddressId = _addressId.Value,
                                AddressStr = (string)reader["AddressStr"]
                            };
                            item.Adresses.Add(address);
                        }
                        list.Add(item);
                    }
                }

            }
            return list;
        }

        public List<ExtendedGroup> GetGroupsPaging(long? udodId, string nameGroup, long? childunionId, Guid? teacherId, int? numYear, int? dictTypeBudgetId, string orderBy, int pagerIndex, int pagerLength, bool? isReadyGroup)
        {
            List<ExtendedGroup> list= new List<ExtendedGroup>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_GetGroupsPaging", conn) { CommandType = CommandType.StoredProcedure };
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (childunionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childunionId));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                if (numYear.HasValue) comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                if (dictTypeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("dictTypeBudgetId", dictTypeBudgetId));
                if (!string.IsNullOrEmpty(nameGroup)) comm.Parameters.Add(new SqlParameter("NameGroup", nameGroup));
                if (!string.IsNullOrEmpty(orderBy)) comm.Parameters.Add(new SqlParameter("OrderBy", orderBy));
                comm.Parameters.Add(new SqlParameter("pagerIndex", pagerIndex));
                comm.Parameters.Add(new SqlParameter("pagerLength", pagerLength));
                if (isReadyGroup.HasValue) comm.Parameters.Add(new SqlParameter("IsReadyGroup", isReadyGroup));

                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long _udodGroupId = (long)reader["UdodAgeGroupId"];
                    long? _addressId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long)reader["AddressId"];
                    Guid? _teacherId = null;
                    if (reader["TeacherId"] != DBNull.Value) _teacherId = (Guid)reader["TeacherId"];
                    int? _numYear = null;
                    if (reader["NumYear"] != DBNull.Value) _numYear = (int)reader["NumYear"];

                    ExtendedGroup last = list.LastOrDefault(i => i.UdodAgeGroupId == _udodGroupId);
                    if (last!=null)
                    {
                        if (_addressId.HasValue && last.Adresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var address = new ExtendedAddress();
                            address.AddressId = _addressId.Value;
                            address.AddressStr = (string) reader["AddressStr"];
                            last.Adresses.Add(address);
                        }
                        if(_teacherId.HasValue && last.Teachers.Count(i => i.UserId == _teacherId) == 0)
                        {
                            var teacher = new ExtendedTeacher();
                            teacher.UserId = _teacherId.Value;
                            teacher.LastName = (string) reader["LastName"];
                            teacher.FirstName = (string) reader["FirstName"];
                            teacher.MiddleName = (string) reader["MiddleName"];
                            last.Teachers.Add(teacher);
                        }
                        if (_numYear.HasValue && last.NumYears.Count(i=>i==_numYear)==0)
                        {
                            last.NumYears.Add(_numYear.Value);
                        }
                    }
                    else
                    {
                        ExtendedGroup item = new ExtendedGroup();
                        item.Name = (string) reader["GroupName"];
                        item.AgeStart = (int)reader["AgeStart"];
                        item.AgeEnd = (int)reader["AgeEnd"];
                        item.UdodAgeGroupId = _udodGroupId;
                        item.TypeBudget = new ExtendedTypeBudget()
                        {
                            DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                            TypeBudgetName = (string)reader["TypeBudgetName"]
                        };
                        item.EducationType = new ExtendedEducationType()
                        {
                            EducationTypeId = (int)reader["EducationTypeId"],
                            EducationTypeName = (string)reader["EducationTypeName"]
                        };

                        item.UdodId = (long) reader["UdodId"];
                        item.Comment = (string)reader["Comment"];
                        item.ChildUnionName = (string)reader["ChildUnionName"];
                        item.ChildUnionId = (long)reader["ChildUnionId"];
                        item.CurrentCountPupil = (int)reader["CountPupils"];
                        item.MaxCountPupil = (int)reader["MaxCountPupil"];

                        if (reader["StartLesson"] != DBNull.Value)
                            item.StartLesson = (DateTime)reader["StartLesson"];
                        
                        if (reader["StartClaim"] != DBNull.Value)
                            item.StartClaim = (DateTime) reader["StartClaim"];
                        if (reader["EndClaim"] != DBNull.Value)
                            item.EndClaim = (DateTime)reader["EndClaim"];
                        item.NumYears = new List<int>();
                        if (_numYear.HasValue)
                        {
                            item.NumYears.Add(_numYear.Value);
                        }
                        

                        item.Teachers = new List<ExtendedTeacher>();
                        if (_teacherId.HasValue)
                        {
                            var teacher = new ExtendedTeacher()
                                              {
                                                  LastName = (string) reader["LastName"],
                                                  FirstName = (string) reader["FirstName"],
                                                  MiddleName = (string) reader["MiddleName"],
                                                  UserId = _teacherId.Value
                                              };
                            item.Teachers.Add(teacher);
                        }
                        item.Adresses= new List<ExtendedAddress>();
                        if (_addressId.HasValue)
                        {
                            var address = new ExtendedAddress()
                                              {
                                                  AddressId = _addressId.Value,
                                                  AddressStr = (string)reader["AddressStr"]
                                              };
                            item.Adresses.Add(address);
                        }
                        list.Add(item);
                    }
                }

            }
            return list;
        }

        public int GetGroupsCount(long? udodId, string nameGroup, long? childunionId, Guid? teacherId, int? numYear, int? dictTypeBudgetId, string orderBy, int pagerIndex, int pagerLength)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_GetGroupsCount", conn) { CommandType = CommandType.StoredProcedure };
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (childunionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childunionId));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                if (numYear.HasValue) comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                if (dictTypeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("dictTypeBudgetId", dictTypeBudgetId));
                if (!string.IsNullOrEmpty(nameGroup)) comm.Parameters.Add(new SqlParameter("NameGroup", nameGroup));
                
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }

            }
            return 0;
        }

        public ExtendedGroup GetGroup(long groupId)
        {
            List<ExtendedGroup> list = new List<ExtendedGroup>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_GetGroupsPaging", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("GroupId", groupId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long _udodGroupId = (long)reader["UdodAgeGroupId"];
                    long? _addressId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long)reader["AddressId"];
                    Guid? _teacherId = null;
                    if (reader["TeacherId"] != DBNull.Value) _teacherId = (Guid)reader["TeacherId"];
                    int? _numYear = null;
                    if (reader["NumYear"] != DBNull.Value) _numYear = (int)reader["NumYear"];

                    ExtendedGroup last = list.LastOrDefault(i => i.UdodAgeGroupId == _udodGroupId);
                    if (last != null)
                    {
                        if (_addressId.HasValue && last.Adresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var address = new ExtendedAddress();
                            address.AddressId = _addressId.Value;
                            address.AddressStr = (string)reader["AddressStr"];
                            last.Adresses.Add(address);
                        }
                        if (_teacherId.HasValue && last.Teachers.Count(i => i.UserId == _teacherId) == 0)
                        {
                            var teacher = new ExtendedTeacher();
                            teacher.UserId = _teacherId.Value;
                            teacher.LastName = (string)reader["LastName"];
                            teacher.FirstName = (string)reader["FirstName"];
                            teacher.MiddleName = (string)reader["MiddleName"];
                            last.Teachers.Add(teacher);
                        }
                        if (_numYear.HasValue && last.NumYears.Count(i => i == _numYear) == 0)
                        {
                            last.NumYears.Add(_numYear.Value);
                        }
                    }
                    else
                    {
                        ExtendedGroup item = new ExtendedGroup();
                        item.Name = (string)reader["GroupName"];
                        item.AgeStart = (int)reader["AgeStart"];
                        item.AgeEnd = (int)reader["AgeEnd"];
                        item.UdodAgeGroupId = _udodGroupId;
                        item.TypeBudget = new ExtendedTypeBudget()
                        {
                            DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                            TypeBudgetName = (string)reader["TypeBudgetName"]
                        };
                        item.EducationType = new ExtendedEducationType()
                        {
                            EducationTypeId = (int)reader["EducationTypeId"],
                            EducationTypeName = (string)reader["EducationTypeName"]
                        };

                        item.UdodId = (long)reader["UdodId"];
                        item.Comment = (string)reader["Comment"];
                        item.Place = (string)reader["Place"];
                        item.ChildUnionName = (string)reader["ChildUnionName"];
                        item.ChildUnionId = (long)reader["ChildUnionId"];
                        item.CurrentCountPupil = (int)reader["CountPupils"];
                        item.MaxCountPupil = (int)reader["MaxCountPupil"];
                        item.StartLesson = null;
                        if (reader["StartLesson"] != DBNull.Value) item.StartLesson = (DateTime) reader["StartLesson"];
                        item.StartClaim = null;
                        if (reader["StartClaim"] != DBNull.Value) item.StartClaim = (DateTime)reader["StartClaim"];
                        item.EndClaim = null;
                        if (reader["EndClaim"] != DBNull.Value) item.EndClaim = (DateTime)reader["EndClaim"];

                        item.NumYears = new List<int>();
                        if (_numYear.HasValue)
                        {
                            item.NumYears.Add(_numYear.Value);
                        }


                        item.Teachers = new List<ExtendedTeacher>();
                        if (_teacherId.HasValue)
                        {
                            var teacher = new ExtendedTeacher()
                            {
                                LastName = (string)reader["LastName"],
                                FirstName = (string)reader["FirstName"],
                                MiddleName = (string)reader["MiddleName"],
                                UserId = _teacherId.Value
                            };
                            item.Teachers.Add(teacher);
                        }
                        item.Adresses = new List<ExtendedAddress>();
                        if (_addressId.HasValue)
                        {
                            var address = new ExtendedAddress()
                            {
                                AddressId = _addressId.Value,
                                AddressStr = (string)reader["AddressStr"]
                            };
                            item.Adresses.Add(address);
                        }
                        list.Add(item);
                    }
                }

            }

            return list.FirstOrDefault();
        }

        public ExtendedGroup GetGroup(long childUnionId, Guid pupilId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_GetGroupByPupilId", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("PupilId", pupilId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long _udodGroupId = (long)reader["UdodAgeGroupId"];
                    return GetGroup(_udodGroupId);
                }

            }

            return (ExtendedGroup)null;
        }

        public int DeleteGroup(long groupId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_DeleteGroup", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("GroupId", groupId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }
            return 0;
        }

        public long AddGroup(string name, long childUnionId, int ageStart, int ageEnd, int typeBudgetId, int educationTypeId, string comment, string place, DateTime startLesson, DateTime startClaim, DateTime endClaim, int maxCountPupil)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_AddGroup", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Name", name));
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("AgeStart", ageStart));
                comm.Parameters.Add(new SqlParameter("AgeEnd", ageEnd));
                comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                comm.Parameters.Add(new SqlParameter("EducationTypeId", educationTypeId));
                if (!string.IsNullOrEmpty(comment)) comm.Parameters.Add(new SqlParameter("Comment", comment));
                if (!string.IsNullOrEmpty(place)) comm.Parameters.Add(new SqlParameter("Place", place));
                comm.Parameters.Add(new SqlParameter("StartLesson", startLesson));
                comm.Parameters.Add(new SqlParameter("StartClaim", startClaim));
                comm.Parameters.Add(new SqlParameter("EndClaim", endClaim));
                comm.Parameters.Add(new SqlParameter("MaxCountPupil", maxCountPupil));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (long)reader[0];
                }
            }
            return -1;    
        }

        public void UpdateGroup(long groupId, string name, long childUnionId, int ageStart, int ageEnd, int typeBudgetId, int educationTypeId, string comment, string place, DateTime startLesson, DateTime startClaim, DateTime endClaim, int maxCountPupil)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_UpdateGroup", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("GroupId", groupId));
                comm.Parameters.Add(new SqlParameter("Name", name));
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("AgeStart", ageStart));
                comm.Parameters.Add(new SqlParameter("AgeEnd", ageEnd));
                comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                comm.Parameters.Add(new SqlParameter("EducationTypeId", educationTypeId));
                if (!string.IsNullOrEmpty(comment)) comm.Parameters.Add(new SqlParameter("Comment", comment));
                if (!string.IsNullOrEmpty(place)) comm.Parameters.Add(new SqlParameter("Place", place));
                comm.Parameters.Add(new SqlParameter("StartLesson", startLesson));
                comm.Parameters.Add(new SqlParameter("StartClaim", startClaim));
                comm.Parameters.Add(new SqlParameter("EndClaim", endClaim));
                comm.Parameters.Add(new SqlParameter("MaxCountPupil", maxCountPupil));
                comm.ExecuteNonQuery();
            }
            
        }

        public void AddGroupAddress(long groupId, long addressId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_AddGroupAddress", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UdodGroupId", groupId));
                comm.Parameters.Add(new SqlParameter("AddressId", addressId));
                comm.ExecuteNonQuery();
            }
        }

        public void AddGroupTeacher(long groupId, Guid teacherId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_AddGroupTeacher", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UdodGroupId", groupId));
                comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                comm.ExecuteNonQuery();
            }
        }

        public void AddGroupYear(long groupId, int year)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_AddGroupYear", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UdodGroupId", groupId));
                comm.Parameters.Add(new SqlParameter("Numyear", year));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteGroupAddress(long groupId, long addressId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_DeleteGroupAddress", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UdodGroupId", groupId));
                comm.Parameters.Add(new SqlParameter("AddressId", addressId));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteGroupTeacher(long groupId, Guid teacherId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_DeleteGroupTeacher", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UdodGroupId", groupId));
                comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteGroupYear(long groupId, int year)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("Udod_DeleteGroupYear", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UdodGroupId", groupId));
                comm.Parameters.Add(new SqlParameter("Numyear", year));
                comm.ExecuteNonQuery();
            }
        }

        public ExtendedAgeGroup GetAgeGroup(long udodAgeGroupId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Udod_GetAgeGroup", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("udodAgeGroupId", udodAgeGroupId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedAgeGroup item = new ExtendedAgeGroup();
                    item.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                    item.UdodId = (long)reader["UdodId"];
                    item.ChildUnionId = (long)reader["ChildUnionId"];
                    item.ChildUnionName = (string)reader["Name"];
                    item.Name = (string)reader["Name"];
                    item.Section = new ExtendedSection()
                    {
                        Name = (string)reader["SectionName"],
                        SectionId = (int)reader["SectionId"],
                        ProfileId = (int)reader["ProfileId"]
                    };
                    item.Profile = new ExtendedProfile()
                    {
                        ProfileId = (int)reader["ProfileId"],
                        Name = (string)reader["ProfileName"],
                        ProgramId = (int)reader["ProgramId"]
                    };
                    item.Program = new ExtendedProgram()
                    {
                        ProgramId = (int)reader["ProgramId"],
                        Name = (string)reader["ProgramName"]
                    };
                    item.Age = (int)reader["Age"];
                    
                    //item.TeacherFio = (string)reader["Fio"];
                    //item.TeacherId = (Guid)reader["TeacherId"];

                    item.NumYearStart = (int)reader["NumYearStart"];
                    item.NumYearEnd = (int)reader["NumYearEnd"];
                    item.AgeStart = (int)reader["AgeStart"];
                    item.AgeEnd = (int)reader["AgeEnd"];
                    
                    item.LessonLength = (int)reader["LessonLength"];
                    item.BreakLength = (int)reader["BreakLength"];
                    item.LessonsInWeek = (int)reader["LessonsInWeek"];

                    item.IsActive = (bool)reader["IsActive"];
                    /*item.YearOfStudy = new ExtendedYearOfStudy()
                    {
                        YearOfStudy = (int)reader["YearOfStudy"],
                        UdodYearOfStudyId = (long)reader["UdodYearOfStudyId"],
                        UdodId = (long)reader["UdodId"]
                    };*/
                    item.Comment = (string)reader["Comment"];
                    return item;
                }
            }
            return new ExtendedAgeGroup();
        }

        public void UpdateAgeGroup(long udodAgeGroupId, int numYearStart, int numYearEnd, int ageStart, int ageEnd, int lessonlength, int breakLength, int lessonsInWeek, bool isActive)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_UpdateAgeGroup", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodAgeGroupId", udodAgeGroupId));
                comm.Parameters.Add(new SqlParameter("NumYearStart", numYearStart));
                comm.Parameters.Add(new SqlParameter("NumYearEnd", numYearEnd));
                comm.Parameters.Add(new SqlParameter("AgeStart", ageStart));
                comm.Parameters.Add(new SqlParameter("AgeEnd", ageEnd));
                comm.Parameters.Add(new SqlParameter("LessonLength", lessonlength));
                comm.Parameters.Add(new SqlParameter("BreakLength", breakLength));
                comm.Parameters.Add(new SqlParameter("LessonsInWeek", lessonsInWeek));
                comm.Parameters.Add(new SqlParameter("IsActive", isActive));
                comm.ExecuteNonQuery();
            }
        }

        public void AddAgeGroup(long udodId, long childUnionId, int age, int numYearStart, int numYearEnd, int ageStart, int ageEnd, int lessonlength, int breakLength, int lessonsInWeek, int maxCountPupil)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_AddAgeGroup", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("Age", age));
                comm.Parameters.Add(new SqlParameter("MaxCountPupil", maxCountPupil));
                comm.Parameters.Add(new SqlParameter("NumYearStart", numYearStart));
                comm.Parameters.Add(new SqlParameter("NumYearEnd", numYearEnd));
                comm.Parameters.Add(new SqlParameter("AgeStart", ageStart));
                comm.Parameters.Add(new SqlParameter("AgeEnd", ageEnd));
                comm.Parameters.Add(new SqlParameter("LessonLength", lessonlength));
                comm.Parameters.Add(new SqlParameter("BreakLength", breakLength));
                comm.Parameters.Add(new SqlParameter("LessonsInWeek", lessonsInWeek));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteAgeGroups(long childUnionId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("ChildUnion_DeleteAgeGroups", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteAgeGroup(long ageGroupId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("AgeGroup_DeleteAgeGroup", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ageGroupId", ageGroupId));
                comm.ExecuteNonQuery();
            }
        }

        public void OpenCloseChildUnion(long? ageGroupId, long? childUnionId, bool open)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("AgeGroup_OpenCloseChildUnion", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (ageGroupId.HasValue) comm.Parameters.Add(new SqlParameter("AgeGroupId", ageGroupId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("childUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("open", open));
                comm.ExecuteNonQuery();
            }
        }

        public int PupilsCount(long childUnionId, int numYear)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("AgeGroup_GetPupils", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("childUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("numYear", numYear));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }
            return 0;
        }

        public int PupilsCountInFirstYear(long childUnionId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("AgeGroup_GetPupilsInFirstYear", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("childUnionId", childUnionId));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }
            return 0;
        }

        public ExtendedAgeGroup GetPupilAgeGroup(Guid PupilId, long childUnionId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("AgeGroup_GetPupilAgeGroup", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("PupilId", PupilId));
                comm.Parameters.Add(new SqlParameter("childUnionId", childUnionId));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedAgeGroup item = new ExtendedAgeGroup();
                    item.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];

                    item.NumYearStart = (int)reader["NumYearStart"];
                    item.NumYearEnd = (int)reader["NumYearEnd"];
                    item.AgeStart = (int)reader["AgeStart"];
                    item.AgeEnd = (int)reader["AgeEnd"];

                    item.LessonLength = (int)reader["LessonLength"];
                    item.BreakLength = (int)reader["BreakLength"];
                    item.LessonsInWeek = (int)reader["LessonsInWeek"];
                    return item;
                }
            }
            return new ExtendedAgeGroup();
        }
    }
}

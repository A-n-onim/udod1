﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Udod.Dal.Enum;

namespace Udod.Dal
{
    #region Основные структуры
    [Serializable]
    public class ExtendedUdod
    {
        public long UdodId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string JurAddress { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public string FioDirector { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string EMail { get; set; }
        public string SiteUrl { get; set; }
        public int UdodTypeId { get; set; }
        public int RayonId { get; set; }
        public string RayonName { get; set; }
        public List<ExtendedAddress> Addresses { get; set; }
        public List<ExtendedMetro> Metros { get; set; }
        public List<ExtendedSection> Sections { get; set; }
        public List<ExtendedProgram> Programs { get; set; }
        public int EkisId { get; set; }
        public int? ReestrId { get; set; }
        public string ReestrGuid { get; set; }
        public int UdodStatusId { get; set; }
        public bool IsDOEnabled { get; set; }
        public bool IsInvalid { get; set; }
        public bool IsContingentEnabled { get; set; }
        public string UdodNumber { get; set; }
        public string ParentName { get; set; }
        public int ParentId { get; set; }
        public ExtendedUdodDopInfo DopInfo { get; set; }
        public ExtendedUdodLicense License { get; set; }
        public ExtendedUdodLicense Accreditation { get; set; }
    }
    [Serializable]
    public struct ExtendedUdodDopInfo
    {
        public long DopInfoId { get; set; }
        public string DopName { get; set; }
        public int Shift { get; set; }
        public string INN { get; set; }
        public string KPP { get; set; }
        public string OGRN { get; set; }
        public bool Is_Individual { get; set; }
        public bool Is_Main { get; set; }
        public string code_okato { get; set; }
        public int ownership { get; set; }
        public string sector { get; set; }
        public int type_UDOD { get; set; }
        public string legalform { get; set; }
        public int study_form { get; set; }
        public int maxPupils { get; set; }
        public string OKOGU { get; set; }
        public string CityzenShip { get; set; }
        public int Status { get; set; }
        public string Activity { get; set; }
        public Boolean IsFilial { get; set; }
        public int CountSmen { get; set; }
    }

    [Serializable]
    public struct ExtendedUdodLicense
    {
        public long UdodLicenseId { get; set; }
        public string Series { get; set; }
        public string Number { get; set; }
        public string ReestrNumber { get; set; }
        public DateTime ReestrDate { get; set; }
        public DateTime ReestrValidity { get; set; }
    }

    [Serializable]
    public struct ExtendedFond
    {
        public int UdodFondId { get; set; }
        public string Number { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public Guid TeacherId { get; set; }
        public List<ExtendedFondProperty> Properties { get; set; }
    }

    [Serializable]
    public struct ExtendedFondProperty
    {
        public int UdodFondPropertyId { get; set; }
        public string Name { get; set; }
        public string PropertyStatus { get; set; }
    }

    [Serializable]
    public struct ExtendedUdodStatus
    {
        public int UdodStatusId { get; set; }
        public string UdodStatusName { get; set; }
    }
    [Serializable]
    public struct ExtendedProgram
    {
        public int ProgramId { get; set; }
        public string Name { get; set; }
        public List<ExtendedProfile> Profiles { get; set; }
        public bool isExistInCurrentUdod { get; set; }
        public bool isEnabledToChange { get; set; }
        public int ChildUnionCount { get; set; }
    }
    [Serializable]
    public struct ExtendedSection
    {
        public int SectionId { get; set; }
        public string  Name { get; set; }
        public string ShortName { get; set; }
        public int LimitAge { get; set; }
        public int ProfileId { get; set; }
        public bool isExistInCurrentUdod { get; set; }
        public bool isEnabledToChange { get; set; }
        public int ChildUnionCount { get; set; }
        public int EszSectionId { get; set; }
    }
    [Serializable]
    public struct ExtendedProfile
    {
        public int ProfileId { get; set; }
        public string Name { get; set; }
        public int ProgramId { get; set; }
        public List<ExtendedSection> Sections { get; set; } 
    }
    [Serializable]
    public struct ExtendedCity
    {
        public int CityId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string OkatoId { get; set; }
    }
    [Serializable]
    public struct ExtendedMetro
    {
        public int MetroId { get; set; }
        public string Name { get; set; }
    }
    [Serializable]
    public struct ExtendedRayon
    {
        public int RayonId { get; set; }
        public string Name { get; set; }
        public int CityId { get; set; }
    }
    [Serializable]
    public struct ExtendedRole
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
    [Serializable]
    public struct ExtendedAddressType
    {
        public int AddressTypeId { get; set; }
        public string Name { get; set; }
    }

    [Serializable]
    public struct ExtendedMegaReport
    {
        public string CityName { get; set; }
        public string ParentName { get; set; }
        public string UdodName { get; set; }
        public int CountDO { get; set; }
        public int IsRec { get; set; }
        public int isPublic { get; set; }
        public int del_pupil { get; set; }
        public int del_zach { get; set; }
        public int zach_pupil { get; set; }
        public int zach_zach { get; set; }
        public int count_pupils { get; set; }
        public int count_not_ou { get; set; }
        public int all_pupils { get; set; }
    }
    [Serializable]
    public struct ExtendedReport
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public long UdodId { get; set; }
        public string UdodName { get; set; }
        public string UdodRayon { get; set; }
        public long ChildUnionId { get; set; }
        public string ChildUnionName { get; set; }
        public string ChildUnionSection { get; set; }
        public string ChildUnionProfile { get; set; }
        public string ChildUnionProgram { get; set; }
        public int ChildUnionClaimNum { get; set; }
    }
    [Serializable]
    public class ExtendedAgeReport
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public long UdodId { get; set; }
        public string UdodName { get; set; }
        public int UdodTypeId { get; set; }
        public int CountUdod { get; set; }
        public int CountUdodIsDo { get; set; }
        public int[] CountPupils { get; set; }
    }

    [Serializable]
    public class ExtendedAgeSexReport
    {
        public int ProgramId { get; set; }
        public string ProgramName { get; set; }
        public int TypeBudgetId { get; set; }
        public string TypeBudgetName { get; set; }
        public int[] CountPupils { get; set; }
    }

    public struct ExtendedDataSetValue
    {
        public string StrValue { get; set; }
        public bool IsInt { get; set; }
    }

    [Serializable]
    public class ExtendedClaimReport
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public long UdodId { get; set; }
        public string UdodName { get; set; }
        public int UdodTypeId { get; set; }
        public int CountUdod { get; set; }
        public int CountUdodIsDo { get; set; }
        public int SumDO { get; set; }
        public int CountDOPublic { get; set; }
        public int CountDONotPublic { get; set; }
        public int SumClaim { get; set; }
        public int CountClaimPGU { get; set; }
        public int CountClaimUdod { get; set; }
        public int CountClaimStatus1 { get; set; }
        public int CountClaimStatus7 { get; set; }
        public int CountClaimStatus4 { get; set; }
        public int CountClaimStatus5 { get; set; }
        public int CountClaimStatus6 { get; set; }
        public int CountClaimAnnul0 { get; set; }
        public int CountClaimAnnul1 { get; set; }
        public int CountClaimAnnul2 { get; set; }
        public int CountClaimAnnul3 { get; set; }

    }
    [Serializable]
    public struct ExtendedAddress
    {
        public long AddressId { get; set; }
        public string KladrCode { get; set; }
        public int? BtiCode { get; set; }
        public int? OmkCode { get; set; }
        public string PostIndex { get; set; }
        public string AddressStr { get; set; }
        public string HouseNumber { get; set; }
        public string Fraction { get; set; }
        public string Housing { get; set; }
        public string Building { get; set; }
        public string FlatNumber { get; set; }
        public float CoordinateX { get; set; }
        public float CoordinateY { get; set; }
        public string AddressWorkTime { get; set; }
        public string AddressPhone { get; set; }
        public string AddressEmail { get; set; }
        public ExtendedAddressType AddressType { get; set; }
        public string AddressUdodName { get; set; }
        public bool IsPrimary { get; set; }
        public int? BtiHouseCode { get; set; }
    }

    [Serializable]
    public struct ExtendedUdodAddress
    {
        public long UdodId { get; set; }
        public string UdodName { get; set; }
        public string AddressStr { get; set; }
        public long AddressId { get; set; }
        public long EoId { get; set; }
    }
    [Serializable]
    public class ExtendedUser
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Fio { get { return LastName + ' ' + FirstName + ' ' + MiddleName; }}
        public string EMail { get; set; }
        public string PhoneNumber { get; set; }
        public string ExpressPhoneNumber { get; set; }
        public int Sex { get; set; }
        public ExtendedPassport Passport { get; set; }
        public long? UdodId { get; set; }
        public int? CityId { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public List<ExtendedAddress> Addresses { get; set; }
        public List<ExtendedRole> Roles { get; set; }
        public string SchoolName { get; set; }
        public string ClassName { get; set; }
        public int UserType { get; set; }
        public string ReestrCode { get; set; }
        public string ReestrUdodCode { get; set; }
        public Guid? ReestrGuid { get; set; }
    }
    [Serializable]
    public class ExtendedUserType
    {
        public int UserTypeId { get; set; }
        public string UserTypeName { get; set; }
    }
    [Serializable]
    public class ExtendedTeacher : ExtendedUser
    {
        public List<ExtendedSection> Sections { get; set; } 
    }

    [Serializable]
    public struct ExtendedYearOfStudy
    {
        public long UdodYearOfStudyId { get; set; }
        public int YearOfStudy { get; set; }
        public long UdodId { get; set; }
    }
    [Serializable]
    public class ExtendedUdodSection 
    {
        public long UdodSectionId { get; set; }
        public ExtendedProfile Profile { get; set; }
        public ExtendedSection Section { get; set; }
        public long UdodId { get; set; }
        public ExtendedProgram Program { get; set; }
    }

    [Serializable]
    public class ExtendedSimpleSchedule
    {
        public long ChildUnionId { get; set; }
        public int DayOfWeek { get; set; }
        public int TimeDayId { get; set; }
        public string TimeDayName { get; set; }
    }

    [Serializable]
    public class ExtendedChildUnion
    {
        public long ChildUnionId { get; set; }
        public string Name { get; set; }
        public long UdodSectionId { get; set; }
        public int MaxCountPupil { get; set; }
        public int MinCountPupil { get; set; }
        public string Comment { get; set; }
        public long UdodId { get; set; }
        public string UdodShortName { get; set; }
        public string UdodPhone { get; set; }
        public int AgeStart { get; set; }
        public int AgeEnd { get; set; }
        public int NumYears { get; set; } //предполоогаемый срок обучения
        public bool isDeleted { get;set;}
        public bool IsActive { get; set; }
        public bool IsWorking { get; set; }
        public ExtendedProgramCategory ProgramCategory { get; set; }
        public ExtendedProfile Profile { get; set; }
        public ExtendedSection Section { get; set; }
        public ExtendedProgram Program { get; set; }
        public List<ExtendedAgeGroup> AgeGroups { get; set; }
        public List<ExtendedAddress> Addresses { get; set; }
        public List<ExtendedTypeBudget> BudgetTypes { get; set; }
        public List<ExtendedEducationType> EducationTypes { get; set; }
        public List<ExtendedTeacher> Teachers { get; set; }
        public ExtendedTypeBudget PupilBudgetType { get; set; }
        public double PupilHours { get; set; }
        public int PupilNumYear { get; set; }
        public long PupilAgeGroupId { get; set; }
        public long ChildUnionDescriptionId { get; set; }
        public string ProgramService { get; set; }
        public string RuleService { get; set; }
        public int DocsDeadline { get; set; }
        public bool TestService { get; set; }
        public int ToursNumber { get; set; }
        public int TypeFinanceId { get; set; }
        public decimal Finansing { get; set; }
        public decimal SubFinance { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public DateTime? DateEnd1 { get; set; }
        public int SexId { get; set; }
        public int StatusId { get; set; }
        public int FormId { get; set; }
        public int OfficeId { get; set; }
        public List<ExtendedAddress> Offices { get; set; }
        public string ESZId { get; set; }
        public Guid? MessageId { get; set; }
        public string ReestrCode { get; set; }
        public string ReestrPupilAgeGroupId { get; set; }
        public string GuidOU { get; set; }
        public bool isDopService { get; set; }
        public int TypeValueServiceId { get; set; }
        public bool isPublicPortal { get; set; }
        public bool isReception { get; set; }
    }
    [Serializable]
    public struct ExtendedAgeGroup
    {
        public long UdodAgeGroupId { get; set; }
        public string Name { get; set; }
        public string ChildUnionName { get; set; }
        public int Age { get; set; }
        public ExtendedProfile Profile { get; set; }
        public ExtendedYearOfStudy YearOfStudy { get; set; }
        public ExtendedSection Section { get; set; }
        public long UdodId { get; set; }
        public long ChildUnionId { get; set; }
        //public ExtendedTypeBudget TypeBudget { get; set; }
        //public ExtendedEducationType EducationType { get; set; }
        public int NumYearStart { get; set; }
        public int NumYearEnd { get; set; }
        public int AgeStart { get; set; }
        public int AgeEnd { get; set; }
        public int LessonLength { get; set; }
        public int BreakLength { get; set; }
        public int LessonsInWeek { get; set; }
        public bool IsActive { get; set; }
        public ExtendedProgram Program { get; set; }
        //public string TeacherFio { get; set; }
        //public Guid TeacherId { get; set; }
        public string Comment { get; set; }
        public int MaxCountPupil { get; set; }
        public List<ExtendedAddress> Adresses { get; set; }
        public DateTime DateBegin { get; set; }
        public DateTime? DateEnd { get; set; }
    }
    [Serializable]
    public class ExtendedGroup
    {
        public long UdodAgeGroupId { get; set; }
        public string Name { get; set; }
        public string ChildUnionName { get; set; }
        public long UdodId { get; set; }
        public long ChildUnionId { get; set; }
        public ExtendedTypeBudget TypeBudget { get; set; }
        public ExtendedEducationType EducationType { get; set; }
        public int AgeStart { get; set; }
        public int AgeEnd { get; set; }
        public List<Int32> NumYears { get; set; }
        public List<ExtendedTeacher> Teachers { get; set; } 
        public string Comment { get; set; }
        public string Place { get; set; }
        public int CurrentCountPupil { get; set; }
        public int MaxCountPupil { get; set; }
        public List<ExtendedAddress> Adresses { get; set; }
        public DateTime? StartClaim { get; set; }
        public DateTime? EndClaim { get; set; }
        public DateTime? StartLesson { get; set; }
    }
    [Serializable]
    public class ExtendedScheduleLesson
    {
        public long ScheduleLessonId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public DateTime EndRecess { get; set; }
        public string Fio { get; set; }
        public Guid TeacherId { get; set; }
        public int SectionId { get; set; }
        public string SectionName { get; set; }
        public long ChildUnionId { get; set; }
        public long GroupId { get; set; }
        public string Comment { get; set; }
        public int Weekday { get; set; }
    }

    [Serializable]
    public class ExtendedScheduleHistory : ExtendedScheduleLesson
    {
        public DateTime LessonDate { get; set; }
        public string ChildUnionName { get; set; }
        public string GroupName { get; set; }
        public long ScheduleLessonHistoryId { get; set; }
    }
    [Serializable]
    public class ExtendedSchedule
    {
        public long ScheduleGroupId { get; set; }
        public long GroupId { get; set; }
        public string GroupName { get; set; }
        public long ChildUnionId { get; set; }
        public string ChildUnionName { get; set; }
        public int MaxLessonsInDay { get; set; }
        public List<ExtendedScheduleLesson> WeekDays { get; set; }
    }
    [Serializable]
    public struct ExtendedClaimStatus
    {
        public int ClaimStatusId { get; set; }
        public string ClaimStatusName { get; set; }
    }
    [Serializable]
    public struct ExtendedTypeBudget
    {
        public int DictTypeBudgetId { get; set; }
        public string TypeBudgetName { get; set; }
    }
    [Serializable]
    public struct ExtendedEducationType
    {
        public int EducationTypeId { get; set; }
        public string EducationTypeName { get; set; }
    }
    [Serializable]
    public struct ExtendedProgramCategory
    {
        public int ProgramCategoryId { get; set; }
        public string ProgramCategoryName { get; set; }
    }
    [Serializable]
    public struct ExtendedMessage
    {
        public Guid MessageId { get; set; }
        public long ClaimId { get; set; }
        public long? ESZClaimId { get; set; }
        public string Comment { get; set; }
    }
    [Serializable]
    public struct ExtendedPublicMessage
    {
        public long MessageUserId { get; set; }
        public long MessageAdminId { get; set; }
        public string Message { get; set; }
        public Guid UserId { get; set; }
        public DateTime? DateRead { get; set; }
        public DateTime CreatedDate { get; set; }
    }
    [Serializable]
    public struct ExtendedMessageOutput
    {
        public Guid MessageOutputId { get; set; }
        public string MessageData { get; set; }
        public int Count { get; set; }
    }
    [Serializable]
    public struct ExtendedPassportType
    {
        public int PassportTypeId { get; set; }
        public string PassportTypeName { get; set; }
    }
    [Serializable]
    public struct ExtendedPassport
    {
        public long UserPassportId { get; set; }
        public string Series { get; set; }
        public string Number { get; set; }
        public DateTime? IssueDate { get; set; }
        public ExtendedPassportType PassportType { get; set; }
    }

    [Serializable]
    public struct ExtendedUdodSectionProfileProgram
    {
        public long UdodId { get; set; }
        public List<ExtendedProgram> Programs { get; set; }
        public List<ExtendedProfile> Profiles { get; set; }
        public List<ExtendedSection> Sections { get; set; }
    }
    
    [Serializable]
    public class ExtendedPupil : ExtendedUser
    {
        public List<ExtendedUser> Parents { get; set; }
        public string AddressStr { get; set; }
        public List<ExtendedChildUnion> ChildUnions { get; set; }
        public string ErrorMessage { get; set; }
        public string SchoolCode { get; set; }
        public string EkisCode { get; set; }
        public ExtendedGroup Group { get; set; }
        public long AgeGroupPupilId { get; set; }
        public bool IsSelect { get; set; }
        public bool IsReestrDelete { get; set; }
        public double Hours { get; set; }
        public int DictSchoolTypeId { get; set; }
        public int? DictSchoolId { get; set; }
    }
    [Serializable]
    public struct ExtendedUdodMetro
    {
        public long UdodMetroId { get; set; }
        public string NameMetro { get; set; }

    }
    [Serializable]
    public struct ExtendedChildUnionTeacher
    {
        public long ChildUnionTeacherId { get; set; }
        public Guid UserId { get; set; }
        public string TeacherName { get; set; }
    }

    [Serializable]
    public struct ExtendedHistoryEvent
    {
        public int Number {get;set;}
        public int FirstEkisId { get; set; }
        public int SecondEkisId { get; set; }
        public string TypeEvents { get; set; }
    }

    #endregion

    #region Временные
    [Serializable]
    public struct ExtendedTmp
    {
        public Guid SchoolGuid { get; set; }
        public Guid UserId { get; set; }
        public int EkisCode { get; set; }
    }
    #endregion

    #region Кладр и ОКАТО
    [Serializable]
    public struct ExtendedKladr
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    [Serializable]
    public struct ExtendedOkato
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
    #endregion

    #region Вспомогательные структуры

    public struct ExtendedMenuItem
    {
        public string Url { get; set; }
        public string Tooltip { get; set; }
        public string Text { get; set; }
    }

    public struct ExtendedLimit
    {
        public int CountUdod { get; set; }
        public int CountChildUnion { get; set; }
        public double Hours { get; set; }
        public double PaidHours { get; set; }
        public double NormHours { get; set; }
        public double ClaimHours { get; set; }
    }

    public class ExtendedUdodPassportMessage
    {
        public long UdodId { get; set; }
        public string ShortName { get; set; }
        public string PhoneNumber { get; set; }
        public List<ExtendedClaim> Claims { get; set; }
    }

    public class ExtendedUserPassportMessage
    {
        
        public Guid ChildId { get; set; }
        public Guid ParentId { get; set; }
        public string ChildFio { get; set; }
        public string ParentFio { get; set; }
        public List<ExtendedUdodPassportMessage> Udods { get; set; }
    }

    public class ExtendedDocument
    {
        public long DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentPath { get; set; }
    }

    public struct ExtendedPassword
    {
        public string UdodName { get; set; }
        public long UdodId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }

    public class ExtendedRequests
    {
        public Guid RequestId { get; set; }
        public long? ClaimId { get; set; }
        public long? ChildUnionId { get; set; }
        public long? UdodAgeGroupId { get; set; }
        public DateTime DateCreated { get; set; }
        public string FileName { get; set; }
        public bool IsError { get; set; }
        public int TypeRequest { get; set; }
    }

    public struct ExtendedEventType
    {
        public int EventTypeId { get; set; }
        public string Name { get; set; }
    }

    public struct ExtendedErrorESZ
    {
        public Guid MessageId { get; set; }
        public string Error { get; set; }
        public long ESZClaimId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ParentFio { get; set; }
        public string PupilFio { get; set; }
        public DateTime PupilBirthday { get; set; }
        public string PupilPassportNumber { get; set; }
        public string PupilPassportSeries { get; set; }
        public DateTime PupilPassportIssueDate { get; set; }
        public string ParentEMail { get; set; }
        public string ParentPhoneNumber { get; set; }
        public long ChildUnionId { get; set; }
    }

    public struct ExtendedEvent
    {
        public Int64 EventId { get; set; }
        public DateTime EventDate { get; set; }
        public ExtendedEventType EventType { get; set; }
        public ExtendedUser User { get; set; }
        public string RemoteIP { get; set; }
        public string Message { get; set; }
        public Guid PupilId { get; set; }
        public ExtendedClaim Claim { get; set; }
        public ExtendedUdod Udod { get; set; }
        public ExtendedChildUnion ChildUnion { get; set; }
    }

    public class ExtendedSchoolType
    {
        public int DictSchoolTypeId { get; set;}
        public string DictSchoolTypeName { get; set; }
    }

    public class ExtendedSchool
    {
        public int DictSchoolId { get; set; }
        public string Name { get; set; }
    }

    public class ExtendedESZDict
    {
        public int DictId { get; set; }
        public string Name { get; set; }
    }

    public class ExtendedTransferStatus
    {
        public int TransferStatusId { get; set; }
        public string TransferStatusName { get; set; }
    }

    #endregion

    #region Репорт
    public struct ExtendedReportClaim
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int RayonId { get; set; }
        public string RayonName { get; set; }
        public int UdodId { get; set; }
        public string UdodName { get; set; }
        public string UdodShortName { get; set; }
        public int TypeMessage { get; set; }
        public int Count1 { get; set; }
        public int Count2 { get; set; }
        public int Count3 { get; set; }
        public int Count4 { get; set; }
        public int Count5 { get; set; }
        public int Count6 { get; set; }
        public int Count7 { get; set; }
        public int Count8 { get; set; }
        public int Count9 { get; set; }
        public int Count10 { get; set; }
        public int Count11 { get; set; }
        public int Count12 { get; set; }
        public int CountAll { get
        {
            return Count1 + Count2 + Count3 + Count4 + Count5 + Count6 + Count7 + Count8 + Count9 + Count10 + Count11 +
                   Count12;
        }  }
    }
    public struct ExtendedExportReportClaim
    {
        public string CityName { get; set; }
        public string RayonName { get; set; }
        public string UdodName { get; set; }
        public string UdodShortName { get; set; }
        public int CountType1 { get; set; } // - на рассмотрении
        public int CountType2 { get; set; } // - поступило через осип
        public int CountType3 { get; set; } // - поступило через УДО
        public int CountType4 { get; set; } // - зачислено
        public int CountType5 { get; set; } // - аннулировано
        public int CountType6 { get; set; } // - в резерве
        public int CountType7 { get; set; } // - поданы документы
        public int CountType8 { get; set; } // - заявителей обратилось
        public int CountType9 { get; set; } // - заявителей зачисленны
        public int CountType0 { get; set; } // - поступило через портал
        public int CountType10 { get; set; } // - всего ОСИП+ПОРТАЛ+УДО
    }

    public struct ExtendedReportDep
    {
        public string Parametr { get; set; }
        public int Count { get; set; }
        public string Procent { get; set; }
    }

    public struct ExtendedReportYearEnroll
    {
        public int Year { get; set; }
        public int FreeEnrollCount { get; set; }
        public int FeeEnrollCount { get; set; }
        public int AllEnrollCount { get { return FreeEnrollCount + FeeEnrollCount; } }
    }

    public struct ExtendedReportDepFromCity
    {
        public int CityId { get; set; }
        public long UdodId { get; set; }
        public string NameCity { get; set; }
        public string NameUdod { get; set; }
        public string IsDOEnabled { get; set; }
        public int[] Counts { get; set; }
        public List<ExtendedReportYearEnroll> Years { get; set; }
        public int SumFee { get { return Years.Sum(i => i.FeeEnrollCount); } }
        public int SumFree { get { return Years.Sum(i => i.FreeEnrollCount); } }
        public int Sum { get { return Years.Sum(i => i.AllEnrollCount); } }
    }

    public struct ExtendedReportLesson
    {
        public int CityId { get; set; }
        public long UdodId { get; set; }
        public string CityName { get; set; }
        public string UdodName { get; set; }
        public string ChildUnionName { get; set; }
        public int LessonLength { get; set; }
        public int LessonsInWeek { get; set; }
    }

    public class ExtendedReportTransfer
    {
        public long UdodId { get; set; }
        public string ShortName { get; set; }
        public string IsDOEnabled { get; set; }
        public string UdodNumber { get; set; }
        public string CityName { get; set; }
        public int EnrollSum { get { return EndOfStudy + WillBeTransfer + WillBeDismissed + AgeGroupNotExist + TooMuchHours+Repear; } }
        public int EndOfStudy { get; set; }
        public int WillBeTransfer { get; set; }
        public int WillBeDismissed { get; set; }
        public int AgeGroupNotExist { get; set; }
        public int TooMuchHours { get; set; }
        public int Repear { get; set; }
    }

    public struct ExtendedReportUdodProgram
    {
        public Int64 UdodId { get; set; }
        public string UdodNumber { get; set; }
        public int ProgramId { get; set; }
        public string UdodName { get; set; }
        public string ProgramName { get; set; }
        public string CityName { get; set; }
        public int CountChildUnion { get; set; }
        public bool isDO { get; set; }
        public bool isGos { get; set; }
    }

    public struct ExtendedReportDepEvalCity
    {
        public string Name { get; set; }
        
        public int[] Counts { get; set;}
        public float[] Procents { get; set;}
    }

    public class ExtendedImportedPupil : ExtendedPupil
    {
        public int NumYear { get; set; }
        public string ErrorMessage { get; set; }
        public bool Checked { get; set; }
        public string AddedChildUnionName { get; set; }
        public bool IsEdit { get; set; }
        public ExtendedTypeBudget TypeBudget { get; set; }
    }

    public struct ExtendedImportedPupilUdod
    {
        public long UdodId { get; set; }
        public string UdodName { get; set; }
        public string UdodPhoneNumber { get; set; }
        public Guid UserId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public DateTime? DateEnd { get; set; }
        public int? DictClaimStatusId { get; set; } 
    }
    #endregion

    #region Заявка
    public struct ExtendedClaim
    {
        public long ClaimId { get; set; }
        public long Number { get; set; }
        public int NumYear { get; set; }
        public ExtendedUser Teacher { get; set; }
        public ExtendedAgeGroup AgeGroup { get; set; }
        public Guid UserId { get; set; }
        public ExtendedUser Pupil { get; set; }
        public ExtendedUser Parent { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime BeginDate { get; set; }
        public ExtendedClaimStatus ClaimStatus { get; set; }
        public ExtendedUdod Udod { get; set; }
        public ExtendedChildUnion ChildUnion { get; set; }
        public int CreatorRoleId { get; set; }
        public long RelationId { get; set; }
        public bool? IsReestr { get; set; }
        public DateTime UpdateStatusDate { get; set; }
    }
    #endregion

    #region Зачисление
    public class ExtendedTransferPupil
    {
        public long AgeGroupPupilId { get; set; }
        public string PupilFio { get; set; }
        public DateTime PupilBirthday { get; set; }
        public ExtendedChildUnion ChildUnion { get; set; }
        public long AgeGroupId { get; set; }
        public long NewAgeGroupId { get; set; }
        public int PupilYearsOnSeptFirst { get; set; }
        public string ChildUnionAges { get; set; }
        public int CurrentNumYear { get; set; }
        public int TotalNumYear { get; set; }
        public ExtendedTransferStatus TransferStatus { get; set; }
        public bool TransferIsSelected { get; set; }
        public int DictTypeBudgetId { get; set; }
        public int StatusDO { get; set; }
        public int Statusgroup { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string NameGroup { get; set; }
    }
    #endregion
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.DataSetExtensions;
using System.Configuration;

namespace Udod.Dal
{
    public class DictCityDb : BaseDb
    {
        private int CitiesCount;

        public DictCityDb(){}

        public List<ExtendedCity> GetCities()
        {
            List<ExtendedCity> list = new List<ExtendedCity>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Dict_GetCity", Connection){CommandType = CommandType.StoredProcedure};
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while(reader.Read())
                {
                    ExtendedCity item = new ExtendedCity();
                    item.CityId = (int)reader["CityId"];
                    item.Name = (string) reader["Name"];
                    item.ShortName = (string) reader["ShortName"];
                    item.OkatoId = (string)reader["OkatoId"];
                    list.Add(item);
                }
            }

            CitiesCount = list.Count;
            return list;
        }

        public ExtendedCity? GetCity(int cityId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetCity", Connection){CommandType = CommandType.StoredProcedure};
                comm.Parameters.Add(new SqlParameter("CityId", cityId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedCity item = new ExtendedCity();
                    item.CityId = cityId;
                    item.Name = (string)reader["Name"];
                    item.ShortName = (string)reader["ShortName"];
                    return item;
                }
            }
            return null;
        }

        public int GetCitiesCount(int? cityId)
        {
            return CitiesCount;
        }
    }
}

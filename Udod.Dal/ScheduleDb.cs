﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.DataSetExtensions;
using System.Configuration;

namespace Udod.Dal
{
    public class ScheduleDb : BaseDb
    {
        public List<ExtendedPupil> ScheduleLessonGetPupils(long scheduleLessonId)
        {
            List<ExtendedPupil> list=new List<ExtendedPupil>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Schedule_GetLessonPupil", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("@ScheduleLessonId", scheduleLessonId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedPupil item = new ExtendedPupil();
                    item.UserId = (Guid) reader["PupilId"];
                    list.Add(item);
                }
            }

            return list;
        }

        public void AddScheduleLessonPupil(long scheduleLessonId, Guid pupilId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Schedule_AddLessonPupil", conn)
                                      {CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("@ScheduleLessonId", scheduleLessonId));
                comm.Parameters.Add(new SqlParameter("@PupilId", pupilId));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteScheduleLessonPupil(long scheduleLessonId, Guid pupilId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Schedule_DeleteLessonPupil", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("@ScheduleLessonId", scheduleLessonId));
                comm.Parameters.Add(new SqlParameter("@PupilId", pupilId));
                comm.ExecuteNonQuery();
            }
        }


        public List<ExtendedScheduleHistory> GetScheduleHistoryPaging(long? udodId, long? childUnionId, Guid? teacherId, long? groupId, int? numYear, int? dictTypeBudgetId, int? dictTypeEducation, DateTime? startDate, DateTime? endDate, int? pageIndex, int? pageLength)
        {
            List<ExtendedScheduleHistory> list = new List<ExtendedScheduleHistory>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Schedule_GetScheduleHistory", conn) {CommandType = CommandType.StoredProcedure};
                conn.Open();
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (groupId.HasValue) comm.Parameters.Add(new SqlParameter("GroupId", groupId));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (numYear.HasValue) comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                if (dictTypeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("DictTypeBudgetId", dictTypeBudgetId));
                if (dictTypeEducation.HasValue) comm.Parameters.Add(new SqlParameter("DictTypeEducation", dictTypeEducation));
                if (startDate.HasValue) comm.Parameters.Add(new SqlParameter("StartDate", startDate));
                if (endDate.HasValue) comm.Parameters.Add(new SqlParameter("EndDate", endDate));
                if (pageIndex.HasValue) comm.Parameters.Add(new SqlParameter("PageIndex", pageIndex));
                if (pageLength.HasValue) comm.Parameters.Add(new SqlParameter("PageLength", pageLength));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    var item = new ExtendedScheduleHistory
                    {
                        SectionId = (int) reader["SectionId"],
                        SectionName = (string) reader["SectionName"],
                        StartTime = (DateTime) reader["StartTime"],
                        EndTime = (DateTime) reader["EndTime"],
                        EndRecess = (DateTime) reader["EndRecess"],
                        ScheduleLessonHistoryId = (long) reader["ScheduleLessonHistoryId"],
                        TeacherId = (Guid) reader["TeacherId"],
                        Fio = (string) reader["Fio"],
                        Weekday = (int) reader["WeekDay"],
                        ChildUnionId = (long) reader["ChildUnionId"],
                        GroupId = (long) reader["UdodAgeGroupId"],
                        Comment = (string) reader["Comment"],
                        LessonDate = (DateTime) reader["LessonDate"],
                        ChildUnionName = (string)reader["ChildUnionName"],
                        GroupName = (string)reader["GroupName"],
                        ScheduleLessonId = (long)reader["ScheduleLessonId"]
                    };

                    list.Add(item);
                }
            }
            return list;
        }

        public void DeleteSheduleLesson(long scheduleLessonId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Schedule_DeleteScheduleLesson", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ScheduleLessonId", scheduleLessonId));
                comm.ExecuteNonQuery();
            }
        }

        public ExtendedScheduleLesson GetSheduleLesson(long scheduleLessonId)
        {
            ExtendedScheduleLesson lesson= new ExtendedScheduleLesson();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Schedule_GetScheduleLesson", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ScheduleLessonId", scheduleLessonId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    lesson.SectionId = (int)reader["SectionId"];
                    lesson.SectionName = (string)reader["SectionName"];
                    lesson.StartTime = (DateTime)reader["StartTime"];
                    lesson.EndTime = (DateTime)reader["EndTime"];
                    lesson.EndRecess = (DateTime)reader["EndRecess"];
                    lesson.ScheduleLessonId = (long)reader["ScheduleLessonId"];
                    lesson.TeacherId = (Guid)reader["TeacherId"];
                    lesson.Fio = (string)reader["Fio"];
                    lesson.Weekday = (int)reader["WeekDay"];
                    lesson.ChildUnionId = (long) reader["ChildUnionId"];
                    lesson.GroupId = (long)reader["UdodAgeGroupId"];
                    lesson.Comment = (string)reader["Comment"];
                }
            }
            return lesson;
        }

        public ExtendedScheduleHistory GetSheduleLessonHistory(long scheduleLessonHistoryId)
        {
            ExtendedScheduleHistory lesson = new ExtendedScheduleHistory();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Schedule_GetScheduleLessonHistory", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ScheduleLessonHistoryId", scheduleLessonHistoryId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    lesson.SectionId = (int)reader["SectionId"];
                    lesson.SectionName = (string)reader["SectionName"];
                    lesson.StartTime = (DateTime)reader["StartTime"];
                    lesson.EndTime = (DateTime)reader["EndTime"];
                    lesson.EndRecess = (DateTime)reader["EndRecess"];
                    lesson.ScheduleLessonId = (long)reader["ScheduleLessonId"];
                    lesson.ScheduleLessonHistoryId = (long)reader["ScheduleLessonHistoryId"];
                    lesson.TeacherId = (Guid)reader["TeacherId"];
                    lesson.Fio = (string)reader["Fio"];
                    lesson.Weekday = (int)reader["WeekDay"];
                    lesson.ChildUnionId = (long)reader["ChildUnionId"];
                    lesson.GroupId = (long)reader["UdodAgeGroupId"];
                    lesson.Comment = (string)reader["Comment"];
                    lesson.LessonDate = (DateTime)reader["LessonDate"];
                }
            }
            return lesson;
        }

        public List<ExtendedSchedule> GetShedules(long? childUnionId, long? groupId, Guid? teacherId, int? programId, bool? isRecess)
        {
            List<ExtendedSchedule> list= new List<ExtendedSchedule>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Schedule_GetSchedules", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (groupId.HasValue) comm.Parameters.Add(new SqlParameter("GroupId", groupId));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                if (programId.HasValue) comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                if (isRecess.HasValue) comm.Parameters.Add(new SqlParameter("IsRecess", isRecess));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    /*Guid? _teacherId = null;
                    if (reader["TeacherId"]!=DBNull.Value) _teacherId=(Guid)reader["TeacherId"];
                    int? _sectionId = null;
                    if (reader["SectionId"] != DBNull.Value) _sectionId = (int) reader["SectionId"];
                    */
                    long _scheduleId = (long) reader["ScheduleId"];
                    long? _scheduleLessonId = null;
                    if (reader["ScheduleLessonId"]!=DBNull.Value) _scheduleLessonId=(long)reader["ScheduleLessonId"];

                    int? _weekday = null;
                    if (reader["WeekDay"] != DBNull.Value) _weekday = (int)reader["WeekDay"];

                    var last = list.FirstOrDefault(i => i.ScheduleGroupId == _scheduleId && _weekday.HasValue && i.WeekDays.Count(k => k.Weekday == _weekday) == 0);
                    /*int _weekday = (int) reader["WeekDay"];
                    var firstgroup = list.FirstOrDefault(i => i.GroupId == _groupId && i.MaxLessonsInDay<maxCountPupil && i.WeekDays.Count(k=>k.Weekday==_weekday)>0);
                    if (firstgroup.GroupId!=0)
                    {
                        // тут расставить значения для merge
                    }
                    */

                    if (last!=null)
                    {
                        if (_scheduleLessonId.HasValue && last.WeekDays.Count(i => i.ScheduleLessonId == _scheduleLessonId) == 0)
                        {
                            ExtendedScheduleLesson lesson = new ExtendedScheduleLesson();
                            lesson.SectionId = (int)reader["SectionId"];
                            lesson.SectionName = (string)reader["SectionName"];
                            lesson.StartTime = (DateTime)reader["StartTime"];
                            lesson.EndTime = (DateTime)reader["EndTime"];
                            lesson.EndRecess = (DateTime)reader["EndRecess"];
                            lesson.ScheduleLessonId = (long)reader["ScheduleLessonId"];
                            lesson.TeacherId = (Guid)reader["TeacherId"];
                            lesson.Fio = (string)reader["Fio"];
                            lesson.Weekday = (int)reader["WeekDay"];
                            lesson.Comment = (string)reader["Comment"];
                            last.WeekDays.Add(lesson);
                        }
                    }
                    else
                    {
                        //maxCountPupil = 1;
                        ExtendedSchedule item = new ExtendedSchedule();
                        item.ScheduleGroupId = (long) reader["ScheduleId"];
                        item.ChildUnionId = (long) reader["ChildUnionId"];
                        item.ChildUnionName = (string) reader["ChildunionName"];
                        item.GroupId = (long) reader["UdodAgeGroupId"];
                        item.GroupName = (string) reader["GroupName"];
                        item.WeekDays=  new List<ExtendedScheduleLesson>();
                        item.MaxLessonsInDay = 1;
                        var firstitem = list.FirstOrDefault(i => i.ScheduleGroupId == _scheduleId);
                        if (firstitem!=null)
                        {
                            firstitem.MaxLessonsInDay++;
                            item.MaxLessonsInDay = 0;
                        }
                        if (_scheduleLessonId.HasValue)
                        {
                            ExtendedScheduleLesson lesson = new ExtendedScheduleLesson();
                            lesson.SectionId = (int) reader["SectionId"];
                            lesson.SectionName = (string) reader["SectionName"];
                            lesson.StartTime = (DateTime) reader["StartTime"];
                            lesson.EndTime = (DateTime) reader["EndTime"];
                            lesson.EndRecess = (DateTime)reader["EndRecess"];
                            lesson.ScheduleLessonId = (long) reader["ScheduleLessonId"];
                            lesson.TeacherId = (Guid) reader["TeacherId"];
                            lesson.Fio = (string) reader["Fio"];
                            lesson.Weekday = (int) reader["WeekDay"];
                            lesson.Comment = (string)reader["Comment"];
                            item.WeekDays.Add(lesson);
                        }
                        list.Add(item);
                    }

                }
            }

            return list;
        }

        public long AddSheduleLesson(long childUnionId, long groupId, Guid teacherId, int sectionId, int weekday, DateTime startTime, DateTime endTime, DateTime endRecess, string comment)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Schedule_AddScheduleLesson", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                //comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("UdodAgeGroupId", groupId));
                comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                comm.Parameters.Add(new SqlParameter("DayOfWeek", weekday));
                comm.Parameters.Add(new SqlParameter("StartTime", startTime));
                comm.Parameters.Add(new SqlParameter("EndTime", endTime));
                comm.Parameters.Add(new SqlParameter("EndRecess", endRecess));
                comm.Parameters.Add(new SqlParameter("Comment", comment));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (long) reader[0];
                }

            }
            return -1;
        }
        public long UpdateSheduleLesson(long scheduleLessonid, long childUnionId, long groupId, Guid teacherId, int sectionId, int weekday, DateTime startTime, DateTime endTime, DateTime endRecess, string comment)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Schedule_UpdateScheduleLesson", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                //comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("ScheduleLessonId", scheduleLessonid));
                comm.Parameters.Add(new SqlParameter("UdodAgeGroupId", groupId));
                comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                comm.Parameters.Add(new SqlParameter("DayOfWeek", weekday));
                comm.Parameters.Add(new SqlParameter("StartTime", startTime));
                comm.Parameters.Add(new SqlParameter("EndTime", endTime));
                comm.Parameters.Add(new SqlParameter("EndRecess", endRecess));
                comm.Parameters.Add(new SqlParameter("Comment", comment));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (long)reader[0];
                }

            }
            return -1;
        }

        public int UpdateSheduleLessonHistory(long scheduleLessonHistoryId, DateTime lessonDate, Guid teacherId, DateTime startTime, DateTime endTime, DateTime endRecess, string comment)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Schedule_UpdateScheduleHistory", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                //comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("ScheduleLessonHistoryId", scheduleLessonHistoryId));
                comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                comm.Parameters.Add(new SqlParameter("LessonDate", lessonDate));
                comm.Parameters.Add(new SqlParameter("StartTime", startTime));
                comm.Parameters.Add(new SqlParameter("EndTime", endTime));
                comm.Parameters.Add(new SqlParameter("EndRecess", endRecess));
                comm.Parameters.Add(new SqlParameter("Comment", comment));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }
            return -1;
        }

        public void DeleteSheduleLessonHistory(long scheduleLessonHistoryId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Schedule_DeleteScheduleHistory", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                //comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("ScheduleLessonHistoryId", scheduleLessonHistoryId));
                comm.ExecuteNonQuery();
            }
        }
    }
}

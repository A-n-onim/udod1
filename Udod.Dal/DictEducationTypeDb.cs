﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.DataSetExtensions;
using System.Configuration;

namespace Udod.Dal
{
    public class DictEducationTypeDb : BaseDb
    {
        public DictEducationTypeDb() {}

        public int EducationTypeCount;

        public List<ExtendedEducationType> GetEducationTypes()
        {
            List<ExtendedEducationType> list = new List<ExtendedEducationType>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetEducationType", Connection) { CommandType = CommandType.StoredProcedure };
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedEducationType item = new ExtendedEducationType();
                    item.EducationTypeId = (int)reader["EducationTypeId"];
                    item.EducationTypeName = (string)reader["EducationTypeName"];
                    list.Add(item);
                }
            }

            EducationTypeCount = list.Count;
            return list;
        }

        public void AddEducationType(string Name)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("AddDictEducationType", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Name", Name));
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateEducationType(string Name, int EducationTypeId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateDictEducationType", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Name", Name));
                comm.Parameters.Add(new SqlParameter("EducationTypeId", EducationTypeId));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteEducationType(int EducationTypeId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("DeleteDictEducationType", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("EducationTypeId", EducationTypeId));
                comm.ExecuteNonQuery();
            }
        }

        public ExtendedEducationType? GetEducationType(int EducationTypeId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetEducationType", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("EducationTypeId", EducationTypeId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedEducationType item = new ExtendedEducationType();
                    item.EducationTypeId = (int)reader["EducationTypeId"];
                    item.EducationTypeName = (string)reader["EducationTypeName"];
                    return item;
                }
            }
            return null;
        }

        public int GetEducationTypeCount()
        {
            return EducationTypeCount;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Udod.Dal
{
    public abstract class BaseDb : IDisposable
    {
        protected BaseDb()
        {
            
        }

        public void Dispose()
        {
            
        }
    }
}

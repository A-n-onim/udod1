﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Udod.Dal
{
    public class DictSchoolDb : BaseDb
    {
        public DictSchoolDb() { }
        public List<ExtendedSchool> GetSchools(string code, string name, int count)
        {
            List<ExtendedSchool> list = new List<ExtendedSchool>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetSchool", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("name", name));
                comm.Parameters.Add(new SqlParameter("Count", count));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedSchool item = new ExtendedSchool();
                    item.DictSchoolId = (int)reader["DictSchoolId"];
                    item.Name = (string)reader["Name"];
                    list.Add(item);
                }
            }

            
            return list;
        }
        public ExtendedSchool GetSchool(string code)
        {
            ExtendedSchool item = new ExtendedSchool();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetSchoolById", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Code", code));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    
                    item.DictSchoolId = (int)reader["DictSchoolId"];
                    item.Name = (string)reader["Name"];
                    
                }
            }


            return item;
        }

        public List<ExtendedSchoolType> GetSchoolType()
        {
            List<ExtendedSchoolType> list = new List<ExtendedSchoolType>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("select * from dict_schooltype", Connection) { CommandType = CommandType.Text };
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedSchoolType item = new ExtendedSchoolType();
                    item.DictSchoolTypeId = (int)reader["DictSchoolTypeId"];
                    item.DictSchoolTypeName = (string)reader["SchoolTypeName"];
                    list.Add(item);
                }
            }


            return list;
        }
        public void UpdateDictSchool(int ekisId, string schoolName, long? reestrCode)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateDictSchool", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ekisId", ekisId));
                comm.Parameters.Add(new SqlParameter("SchoolName", schoolName));
                if (reestrCode.HasValue) comm.Parameters.Add(new SqlParameter("reestrCode", reestrCode));
                comm.ExecuteNonQuery();


            }
        }
    }
}

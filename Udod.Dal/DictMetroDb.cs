﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.DataSetExtensions;
using System.Configuration;

namespace Udod.Dal
{
    public class DictMetroDb : BaseDb
    {
        private int MetrosCount;

        public DictMetroDb(){}

        public List<ExtendedMetro> GetMetros()
        {
            List<ExtendedMetro> list = new List<ExtendedMetro>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Dict_GetMetro", Connection){CommandType = CommandType.StoredProcedure};
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while(reader.Read())
                {
                    ExtendedMetro item = new ExtendedMetro();
                    item.MetroId = (int)reader["MetroId"];
                    item.Name = (string) reader["Name"];
                    list.Add(item);
                }
            }

            MetrosCount = list.Count;
            return list;
        }

        public ExtendedMetro? GetMetro(int metroId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("GetMetro", Connection){CommandType = CommandType.StoredProcedure};
                comm.Parameters.Add(new SqlParameter("MetroId", metroId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedMetro item = new ExtendedMetro();
                    item.MetroId = metroId;
                    item.Name = (string)reader["Name"];
                    return item;
                }
            }
            return null;
        }

        public int GetMetrosCount()
        {
            return MetrosCount;
        }
    }
}

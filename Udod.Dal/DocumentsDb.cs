﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Udod.Dal
{
    public class DocumentsDb : BaseDb
    {
        public List<ExtendedDocument> GetDocuments()
        {
            List<ExtendedDocument> list = new List<ExtendedDocument>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("SELECT * FROM Documents", conn) { CommandType = CommandType.Text };
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedDocument item = new ExtendedDocument();
                    item.DocumentId = (long)reader["DocumentId"];
                    item.DocumentName = (string)reader["DocumentName"];
                    item.DocumentPath = (string)reader["DocumentPath"];
                    list.Add(item);
                }
            }

            return list;
        }

        public void AddDocument(string name, string path)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("INSERT INTO Documents(DocumentName, DocumentPath) VALUES (@DocumentName, @DocumentPath)", Connection) { CommandType = CommandType.Text };
                comm.Parameters.Add(new SqlParameter("DocumentName", name));
                comm.Parameters.Add(new SqlParameter("DocumentPath", path));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteDocument(long documentId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("DELETE FROM Documents WHERE DocumentId = @DocumentId", Connection) { CommandType = CommandType.Text };
                comm.Parameters.Add(new SqlParameter("DocumentId", documentId));
                comm.ExecuteNonQuery();
            }
        }
    }
}

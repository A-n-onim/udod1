﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Udod.Dal
{
    public class KladrDb :BaseDb
    {
        public List<ExtendedKladr> GetRegion()
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm= new SqlCommand("Kladr_GetRegion", Connection){CommandType = CommandType.StoredProcedure};
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedKladr item = new ExtendedKladr()
                                             {
                                                 Code = (string)reader["Code"],
                                                 Name = (string)reader["Name"]
                                             };
                    list.Add(item);
                }
            }
            return list;
        }
        public List<ExtendedKladr> GetRayon(string code)
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Kladr_GetRayon", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("code", code));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedKladr item = new ExtendedKladr()
                    {
                        Code = (string)reader["Code"],
                        Name = (string)reader["Name"]
                    };
                    list.Add(item);
                }
            }
            return list;
        }
        public List<ExtendedKladr> GetCity(string code)
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Kladr_GetCity", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("code", code));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedKladr item = new ExtendedKladr()
                    {
                        Code = (string)reader["Code"],
                        Name = (string)reader["Name"]
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedKladr> GetNasPunkt(string code)
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Kladr_GetNasPunkt", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("code", code));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedKladr item = new ExtendedKladr()
                    {
                        Code = (string)reader["Code"],
                        Name = (string)reader["Name"]
                    };
                    list.Add(item);
                }
            }
            return list;
        }
        public List<ExtendedKladr> GetStreet(string code)
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Kladr_GetStreet", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("code", code));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedKladr item = new ExtendedKladr()
                    {
                        Code = (string)reader["Code"],
                        Name = (string)reader["Name"]
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        public string GetStreetName(string code, bool isBti)
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Kladr_GetStreetName", Connection) { CommandType = CommandType.StoredProcedure };
                if (isBti)
                    comm.CommandText = "Kladr_GetStreetNameByBti";
                comm.Parameters.Add(new SqlParameter("code", code));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {

                    return (string) reader["Name"];

                }
            }
            return "";
        }

        public List<ExtendedKladr> GetStreet(string code, string name, int count)
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Kladr_GetStreetNew", Connection) { CommandType = CommandType.StoredProcedure };
                if (code.Substring(0, 2) == "77" && code.Length > 10)
                    comm.CommandText = "Kladr_GetStreetNew1";
                comm.Parameters.Add(new SqlParameter("code", code));
                comm.Parameters.Add(new SqlParameter("count", count));
                if (!string.IsNullOrEmpty(name)) comm.Parameters.Add(new SqlParameter("Name", name));
                
                
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedKladr item = new ExtendedKladr()
                    {
                        Code = (string)reader["Code"],
                        Name = (string)reader["Name"]
                    };
                    list.Add(item);
                }
            }
            if (list.Count==0 && string.IsNullOrEmpty(name))
            {
                list.Add(new ExtendedKladr(){Code = code, Name = "Нет   "});
            }
            return list;
        }
        public List<ExtendedKladr> GetHouse(string code, string name, int count)
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Kladr_GetHouseNew", Connection) { CommandType = CommandType.StoredProcedure };
                if (code.Length < 10)
                    comm.CommandText = "Kladr_GetHouseByBti";
                comm.Parameters.Add(new SqlParameter("code", code));
                comm.Parameters.Add(new SqlParameter("count", count));
                if (!string.IsNullOrEmpty(name)) comm.Parameters.Add(new SqlParameter("Name", name));
                
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    string houses = (string) reader["Name"];
                    string[] values = houses.Split(',');
                    foreach (string value in values)
                    {
                        ExtendedKladr item = new ExtendedKladr();
                        string newname = value.Replace("стр", ", стр.");
                        newname = newname.Replace("к", ", корп.");

                        item.Name = newname;
                        if (!string.IsNullOrEmpty(name))
                        {
                            if (item.Name.Contains(name))
                                list.Add(item);
                        }
                        else
                        {
                            list.Add(item);
                        }
                    }
                }
            }
            return list.Distinct().Take(count).ToList();
        }

        public string GetStreetCode(string code, string street)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Kladr_GetStreetCode", Connection) { CommandType = CommandType.StoredProcedure };
                if (code.Substring(0, 2) == "77" && code.Length > 10)
                    comm.CommandText = "Kladr_GetStreetCodeByBti";
                comm.Parameters.Add(new SqlParameter("code", code));
                comm.Parameters.Add(new SqlParameter("street", street));
                
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (string) reader[0];
                }
            }
            return "";
        }

        public int GetHouseCode(int streetCode, string house, string krt, string strt)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Kladr_GetHouseCodebyBti", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("StreetCode", streetCode));
                comm.Parameters.Add(new SqlParameter("House", house));
                if (!string.IsNullOrEmpty(krt)) comm.Parameters.Add(new SqlParameter("Krt", krt));
                if (!string.IsNullOrEmpty(strt)) comm.Parameters.Add(new SqlParameter("Strt", strt));

                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return Convert.ToInt32(reader[0]);
                }
            }
            return 0;
        }

        public List<ExtendedKladr> GetHouse(string code)
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Kladr_GetHouse", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("code", code));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedKladr item = new ExtendedKladr()
                    {
                        Code = (string)reader["Code"],
                        Name = (string)reader["Name"]
                    };
                    list.Add(item);
                }
            }
            return list;
        }


        public void DeleteAddress(long addressId, Guid? userId, long? udodId, long? sectionId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("DeleteAddress", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("AddressId", addressId));
                if (userId.HasValue) comm.Parameters.Add(new SqlParameter("UserId", userId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateAddress(string code, string postIndex, string houseNumber, string fraction, string housing, string building, string flatNumber, int AddressTypeId, float coord_x, float coord_y, Guid? userId, long? udodId, string addressWorkTime, string addressPhone, string addressEmail, long addressId, int? typeAddressCode, string addressUdodName)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Udod_UpdateAddress", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("KladrCode", code));
                using (KladrDb db = new KladrDb())
                {
                    comm.Parameters.Add(new SqlParameter("AddressStr", db.GetAddress(code, houseNumber, fraction, housing, building, flatNumber, typeAddressCode).AddressStr));
                }
                comm.Parameters.Add(new SqlParameter("PostIndex", ""));
                if (!string.IsNullOrEmpty(houseNumber)) comm.Parameters.Add(new SqlParameter("HouseNumber", houseNumber));
                if (!string.IsNullOrEmpty(fraction)) comm.Parameters.Add(new SqlParameter("Fraction", fraction));
                if (!string.IsNullOrEmpty(housing)) comm.Parameters.Add(new SqlParameter("Housing", housing));
                if (!string.IsNullOrEmpty(building)) comm.Parameters.Add(new SqlParameter("Building", building));
                comm.Parameters.Add(new SqlParameter("Flat", flatNumber));
                comm.Parameters.Add(new SqlParameter("AddressTypeId", AddressTypeId));
                comm.Parameters.Add(new SqlParameter("Coordinate_x", coord_x));
                comm.Parameters.Add(new SqlParameter("Coordinate_y", coord_y));
                comm.Parameters.Add(new SqlParameter("AddressWorkTime", addressWorkTime));
                comm.Parameters.Add(new SqlParameter("AddressPhone", addressPhone));
                comm.Parameters.Add(new SqlParameter("AddressEmail", addressEmail));
                comm.Parameters.Add(new SqlParameter("AddressUdodName", addressUdodName));
                if (userId.HasValue) comm.Parameters.Add(new SqlParameter("UserId", userId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("AddressId", addressId));
                comm.Parameters.Add(typeAddressCode.HasValue
                                        ? new SqlParameter("typeAddressCode", typeAddressCode.Value.ToString())
                                        : new SqlParameter("typeAddressCode", 1.ToString()));
                comm.ExecuteNonQuery();
            }
        }

        public void AddAddress(string code, string postIndex, string houseNumber, string fraction, string housing, string building, string flatNumber, int AddressTypeId, float coord_x, float coord_y, Guid? userId, long? udodId, string addressWorkTime, string addressPhone, string addressEmail, int? typeAddressCode, string addressUdodName)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("AddAddress", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("KladrCode", code));
                using (KladrDb db = new KladrDb())
                {
                    comm.Parameters.Add(new SqlParameter("AddressStr", db.GetAddress(code, houseNumber, fraction, housing, building, flatNumber, typeAddressCode).AddressStr));
                }
                comm.Parameters.Add(new SqlParameter("PostIndex", ""));
                comm.Parameters.Add(new SqlParameter("HouseNumber", houseNumber));
                comm.Parameters.Add(new SqlParameter("Fraction", fraction));
                comm.Parameters.Add(new SqlParameter("Housing", housing));
                comm.Parameters.Add(new SqlParameter("Building", building));
                comm.Parameters.Add(new SqlParameter("Flat", flatNumber));
                comm.Parameters.Add(new SqlParameter("AddressTypeId", AddressTypeId));
                comm.Parameters.Add(new SqlParameter("Coordinate_x", coord_x));
                comm.Parameters.Add(new SqlParameter("Coordinate_y", coord_y));
                comm.Parameters.Add(new SqlParameter("AddressWorkTime", addressWorkTime));
                comm.Parameters.Add(new SqlParameter("AddressPhone", addressPhone));
                comm.Parameters.Add(new SqlParameter("AddressEmail", addressEmail));
                comm.Parameters.Add(new SqlParameter("AddressUdodName", addressUdodName));
                if (userId.HasValue) comm.Parameters.Add(new SqlParameter("UserId", userId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(typeAddressCode.HasValue
                                        ? new SqlParameter("typeAddressCode", typeAddressCode.Value.ToString())
                                        : new SqlParameter("typeAddressCode", 1.ToString()));
                comm.ExecuteNonQuery();
            }
        }

        public ExtendedAddress GetAddress(string code, string houseNumber, string fraction, string housing, string building, string flatNumber, int? typeAddressCode)
        {
            ExtendedAddress address = new ExtendedAddress();

            address.KladrCode = code;
            address.HouseNumber = houseNumber;
            address.Fraction = fraction;
            address.Housing = housing;
            address.Building = building;
            address.FlatNumber = flatNumber;
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm= new SqlCommand("Kladr_GetAddress", Connection){CommandType = CommandType.StoredProcedure};
                if (!typeAddressCode.HasValue)
                    comm.Parameters.Add(new SqlParameter("code", code));
                else
                {
                    comm.Parameters.Add(new SqlParameter("code", ""));
                    comm.Parameters.Add(new SqlParameter("BtiCode", Convert.ToInt32(code)));

                }
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    address.AddressStr = (string) reader["AddressStr"];
                }

            }
            address.AddressStr += string.IsNullOrEmpty(houseNumber)?"":", д." + houseNumber;
            address.AddressStr += string.IsNullOrEmpty(fraction)?"": "/" + fraction;
            address.AddressStr += string.IsNullOrEmpty(housing)?"":", к." + housing;
            address.AddressStr += string.IsNullOrEmpty(building)?"":", стр." + building;
            address.AddressStr += string.IsNullOrEmpty(flatNumber)?"":", кв." + flatNumber;
            return address;
        }

        public List<ExtendedAddressType> GetAddressTypes()
        {
            List<ExtendedAddressType> list = new List<ExtendedAddressType>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Dict_GetAddressTypes", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedAddressType item = new ExtendedAddressType()
                                                   {
                                                       AddressTypeId = (int)reader["AddressTypeId"],
                                                       Name = (string)reader["Name"]
                                                   };
                    list.Add(item);
                }
            }

            return list;
        }

        public void EmptySelect()
        {
        }
        public List<ExtendedAddress> GetAddressesById(Guid? userId, long? udodId, long? сhildUnionId, long? udodAgeGroup)
        {
            List<ExtendedAddress> list = new List<ExtendedAddress>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetAddresses", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (userId.HasValue) comm.Parameters.Add(new SqlParameter("UserId", userId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (сhildUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", сhildUnionId));
                if (udodAgeGroup.HasValue) comm.Parameters.Add(new SqlParameter("UdodAgeGroup", udodAgeGroup));

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedAddress item = new ExtendedAddress();
                    item.AddressId = (long) reader["AddressId"];
                    item.AddressStr = (string)reader["AddressStr"];
                    item.AddressEmail = (string)reader["AddressEmail"];
                    item.AddressWorkTime = (string)reader["AddressWorkTime"];
                    item.AddressPhone = (string)reader["AddressPhone"];
                    item.KladrCode = (string)reader["KladrCode"];
                    item.PostIndex = (string)reader["PostIndex"];
                    item.HouseNumber = (string)reader["HouseNumber"];
                    item.Fraction = (string)reader["Fraction"];
                    item.Housing = (string)reader["Housing"];
                    item.Building = (string)reader["Building"];
                    item.FlatNumber = (string)reader["Flat"];
                    item.AddressType = new ExtendedAddressType()
                    {
                        AddressTypeId = (int)reader["AddressTypeId"],
                        Name = (string)reader["Name"]
                    };
                    item.BtiCode = null;
                    if (reader["BtiCode"] != DBNull.Value) item.BtiCode = (int)reader["BtiCode"];

                    item.BtiHouseCode = null;
                    if (reader["BtiHouseCode"] != DBNull.Value) item.BtiHouseCode = (int)reader["BtiHouseCode"];
                    

                    list.Add(item);
                }
            }

            return list;
        }

        public List<ExtendedOkato> GetOkatoAO(string code)
        {
            List<ExtendedOkato> list = new List<ExtendedOkato>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Okato_GetAO",conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                if (!string.IsNullOrEmpty(code)) comm.Parameters.Add(new SqlParameter("Code", code));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedOkato item = new ExtendedOkato();
                    item.Code = (string)reader["Code"];
                    item.Name = (string)reader["name_raw"];
                    list.Add(item);
                }

            }

            return list;
        }

        public List<ExtendedOkato> GetOkatoRayon(string code)
        {
            List<ExtendedOkato> list = new List<ExtendedOkato>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Okato_GetRayon", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (!string.IsNullOrEmpty(code)) comm.Parameters.Add(new SqlParameter("Code", code));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedOkato item = new ExtendedOkato();
                    item.Code = (string)reader["Code"];
                    item.Name = (string)reader["name_raw"];
                    list.Add(item);
                }

            }

            return list;
        }
        public List<ExtendedOkato> GetOkatoRayon(int cityId)
        {
            List<ExtendedOkato> list = new List<ExtendedOkato>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Okato_GetRayon", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("CityId", cityId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedOkato item = new ExtendedOkato();
                    item.Code = (string)reader["Code"];
                    item.Name = (string)reader["name_raw"];
                    list.Add(item);
                }

            }

            return list;
        }

        public string GetCodeAddress(string region, string rayon, string city, string street)
        {
            string code = "";
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Kladr_GetCodeAddress", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("Region", region));
                comm.Parameters.Add(new SqlParameter("Rayon", rayon));
                comm.Parameters.Add(new SqlParameter("City", city));
                comm.Parameters.Add(new SqlParameter("Street", street));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    code = (string)reader["Code"];
                }

            }
            return code;
        }

        public int GetStreetCodeByHouseCode(long houseCode)
        {
            int code=0;
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Kladr_GetStreetCodeByHouseCode", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("HouseCode", houseCode));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    code = (int)reader[0];
                }

            }
            return code;
        }

        public List<ExtendedUdodAddress> UdodGetUdodAddress()
        {
            List<ExtendedUdodAddress> list= new List<ExtendedUdodAddress>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetUdodAddress", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedUdodAddress item = new ExtendedUdodAddress
                    {
                        EoId = (long) reader["Eo_Id"],
                        UdodId = (long) reader["UdodId"],
                        UdodName = (string) reader["Name"],
                        AddressId = (long) reader["AddressId"],
                        AddressStr = (string) reader["AddressStr"]
                    };

                    list.Add(item);
                }

            }
            return list;
        }

        public void UpdateDict(long codeDict, int typeFile, List<string> updateParams)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Kladr_UpdateDict", conn)
                                      {CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("TypeFile", typeFile));
                comm.Parameters.Add(new SqlParameter("CodeDict", codeDict));
                int i = 1;
                foreach(var param in updateParams)
                {
                    comm.Parameters.Add(new SqlParameter(string.Format("param{0}",i), param));
                    i++;
                }
                comm.ExecuteNonQuery();
            }
        }

    }
}

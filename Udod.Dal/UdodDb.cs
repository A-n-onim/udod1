﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
//using System.Data.Sql.;
using System.Configuration;

namespace Udod.Dal
{
    public class UdodDb : BaseDb
    {
        private int UdodsCount;

        public UdodDb(){}

        public void ChangeEkisCode(long udodId, int ekisId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("UPDATE Udod SET eo_id = @EkisId, ReestrId = NULL WHERE (UdodId = @udodId)", Connection)
                                      {CommandType = CommandType.Text};
                comm.Parameters.Add(new SqlParameter("EkisId", ekisId));
                comm.Parameters.Add(new SqlParameter("udodId", udodId));
                Connection.Open();
                comm.ExecuteNonQuery();
            }
        }

        public void ChangeParent(long udodId, string parentName)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_ChangeParent", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ParentName", parentName));
                comm.Parameters.Add(new SqlParameter("udodId", udodId));
                Connection.Open();
                comm.ExecuteNonQuery();
            }
        }

        public List<ExtendedUdod> GetUdodsForReport(string cityId, string udodName, string typeUdodId)
        {
            List<ExtendedUdod> list= new List<ExtendedUdod>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetUdodForReport", Connection) {CommandType = CommandType.StoredProcedure};
                if (!string.IsNullOrEmpty(udodName)) comm.Parameters.Add(new SqlParameter("UdodName", udodName));
                if (!string.IsNullOrEmpty(cityId)) comm.Parameters.Add(new SqlParameter("StrCityType", cityId));
                if (!string.IsNullOrEmpty(typeUdodId)) comm.Parameters.Add(new SqlParameter("StrUdodType", typeUdodId));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while(reader.Read())
                {
                    ExtendedUdod item = new ExtendedUdod();

                    item.UdodId = (long) reader["UdodId"];
                    item.ShortName = (string)reader["ShortName"];
                    list.Add(item);
                }
            }

            return list;
        }
        public List<ExtendedUdod> GetUdods(int? cityId, string cityCode, string rayonCode, int? metroId, int? programId, int? profileId, int? sectionId, string udodName, int? typeBudgetId, string sectionName, string profileName, string programName, string childUnionName, int? rayonId, int? ShowIsDOEnabled)
        {
            List<ExtendedUdod> list= new List<ExtendedUdod>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetUdod", Connection){CommandType = CommandType.StoredProcedure};
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId.Value));
                if (metroId.HasValue) comm.Parameters.Add(new SqlParameter("MetroId", metroId.Value));
                if (rayonId.HasValue) comm.Parameters.Add(new SqlParameter("RayonId", rayonId.Value));
                if (!string.IsNullOrEmpty(cityCode)) comm.Parameters.Add(new SqlParameter("CityCode", cityCode));
                if (!string.IsNullOrEmpty(rayonCode)) comm.Parameters.Add(new SqlParameter("RayonCode", rayonCode));
                if (!string.IsNullOrEmpty(udodName)) comm.Parameters.Add(new SqlParameter("UdodName", udodName));
                if (!string.IsNullOrEmpty(programName)) comm.Parameters.Add(new SqlParameter("ProgramName", programName));
                if (!string.IsNullOrEmpty(profileName)) comm.Parameters.Add(new SqlParameter("ProfileName", profileName));
                if (!string.IsNullOrEmpty(sectionName)) comm.Parameters.Add(new SqlParameter("SectionName", sectionName));
                if (!string.IsNullOrEmpty(childUnionName)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnionName));
                if (programId.HasValue) comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                if (profileId.HasValue) comm.Parameters.Add(new SqlParameter("ProfileId", profileId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                if (typeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                comm.Parameters.Add(new SqlParameter("OnlyActive", 1));
                if (ShowIsDOEnabled.HasValue) comm.Parameters.Add(new SqlParameter("ShowIsDOEnabled", ShowIsDOEnabled));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while(reader.Read())
                {
                    long _udodId = (long)reader["UdodId"];
                    long? _addressId = null;
                    int? _metroId = null;
                    if (reader["AddressId"]!=DBNull.Value) _addressId= (long)reader["AddressId"];
                    if (reader["MetroId"] != DBNull.Value) _metroId = (int)reader["MetroId"];

                    ExtendedUdod last = list.LastOrDefault(i => i.UdodId == _udodId);

                    if (null != last)
                    {
                        if (_addressId.HasValue && last.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                                        {
                                            AddressTypeId = (int)reader["AddressTypeId"],
                                            Name = (string)reader["AddressTypeName"]
                                        };
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int) reader["BtiCode"];
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.AddressWorkTime = (string)reader["AddressWorkTime"];
                            itemAddress.AddressPhone = (string)reader["AddressPhone"];
                            itemAddress.AddressEmail = (string)reader["AddressEmail"];

                            (last.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        if (_metroId.HasValue && last.Metros.Count(i => i.MetroId == _metroId) == 0)
                        {
                            var itemMetro = new ExtendedMetro();
                            itemMetro.MetroId = (int)reader["MetroId"];
                            itemMetro.Name = (string)reader["MetroName"];
                            (last.Metros as List<ExtendedMetro>).Add(itemMetro);
                        }
                    }
                    else
                    {
                        ExtendedUdod item = new ExtendedUdod();
                        item.UdodId = (long)reader["UdodId"];
                        item.Name = (string)reader["Name"];
                        item.ShortName = (string)reader["ShortName"];
                        item.JurAddress = (string)reader["JurAddress"];
                        item.CityId = (int)reader["CityId"];
                        item.CityName = (string)reader["CityName"];
                        item.FioDirector = (string)reader["FioDirector"];
                        item.PhoneNumber = (string)reader["PhoneNumber"];
                        item.FaxNumber = (string)reader["FaxNumber"];
                        item.EMail = (string)reader["EMail"];
                        item.SiteUrl = (string)reader["SiteUrl"];
                        item.UdodTypeId = (int)reader["UdodTypeId"];
                        item.UdodStatusId = (int)reader["UdodStatusId"];
                        if (reader["IsContingentEnabled"] == DBNull.Value) item.IsContingentEnabled = false;
                        else
                        item.IsContingentEnabled = (bool)reader["IsContingentEnabled"];

                        item.Addresses = new List<ExtendedAddress>();

                        if (_addressId.HasValue)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];
                            
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.AddressWorkTime = (string)reader["AddressWorkTime"];
                            itemAddress.AddressPhone = (string)reader["AddressPhone"];
                            itemAddress.AddressEmail = (string)reader["AddressEmail"];

                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        item.Metros = new List<ExtendedMetro>();

                        if (_metroId.HasValue)
                        {
                            var itemMetro = new ExtendedMetro();
                            itemMetro.MetroId = (int)reader["MetroId"];
                            itemMetro.Name = (string)reader["MetroName"];
                            (item.Metros as List<ExtendedMetro>).Add(itemMetro);
                        }
                        list.Add(item);
                    }
                }
            }

            UdodsCount = list.Count;
            return list;
        }

        public List<ExtendedUdod> GetUdods(string code, string name, int count)
        {
            List<ExtendedUdod> list = new List<ExtendedUdod>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetAddressUdodName", Connection) { CommandType = CommandType.StoredProcedure };
                if (!string.IsNullOrEmpty(code)) comm.Parameters.Add(new SqlParameter("code", Convert.ToInt32(code)));
                comm.Parameters.Add(new SqlParameter("count", count));
                comm.Parameters.Add(new SqlParameter("name", name));

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedUdod item = new ExtendedUdod();
                    item.UdodId = (long)reader["UdodId"];
                    item.ShortName = (string)reader["ShortName"];

                    list.Add(item);
                }
            }

            //SectionsCount = list.Count;
            return list;
        }

        public List<ExtendedUdod> GetUdodsPaging(int? cityId, string cityCode, string rayonCode, int? metroId, int? programId, int? profileId, int? sectionId, string udodName, int? typeBudgetId, string sectionName, string profileName, string programName, string childUnionName, int? rayonId, int? PagerIndex, int? PagerLength, int? UdodStatusId, bool? isContingent, bool? IsOsip, string number)
        {
            List<ExtendedUdod> list = new List<ExtendedUdod>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetUdodPaging", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId.Value));
                if (metroId.HasValue) comm.Parameters.Add(new SqlParameter("MetroId", metroId.Value));
                if (rayonId.HasValue) comm.Parameters.Add(new SqlParameter("RayonId", rayonId.Value));
                if (!string.IsNullOrEmpty(cityCode)) comm.Parameters.Add(new SqlParameter("CityCode", cityCode));
                if (!string.IsNullOrEmpty(rayonCode)) comm.Parameters.Add(new SqlParameter("RayonCode", rayonCode));
                if (!string.IsNullOrEmpty(udodName)) comm.Parameters.Add(new SqlParameter("UdodName", udodName));
                if (!string.IsNullOrEmpty(programName)) comm.Parameters.Add(new SqlParameter("ProgramName", programName));
                if (!string.IsNullOrEmpty(profileName)) comm.Parameters.Add(new SqlParameter("ProfileName", profileName));
                if (!string.IsNullOrEmpty(sectionName)) comm.Parameters.Add(new SqlParameter("SectionName", sectionName));
                if (!string.IsNullOrEmpty(childUnionName)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnionName));
                if (programId.HasValue) comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                if (profileId.HasValue) comm.Parameters.Add(new SqlParameter("ProfileId", profileId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                if (typeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                if (PagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", PagerIndex));
                if (PagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", PagerLength));
                if (UdodStatusId.HasValue) comm.Parameters.Add(new SqlParameter("UdodStatusId", UdodStatusId));
                if (isContingent.HasValue) comm.Parameters.Add(new SqlParameter("IsContingent", isContingent));
                if (IsOsip.HasValue) comm.Parameters.Add(new SqlParameter("IsOsip", IsOsip));
                if (!string.IsNullOrEmpty(number)) comm.Parameters.Add(new SqlParameter("Number", number));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long _udodId = (long)reader["UdodId"];
                    long? _addressId = null;
                    int? _metroId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long)reader["AddressId"];
                    if (reader["MetroId"] != DBNull.Value) _metroId = (int)reader["MetroId"];

                    ExtendedUdod last = list.LastOrDefault(i => i.UdodId == _udodId);

                    if (null != last)
                    {
                        if (_addressId.HasValue && last.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.AddressWorkTime = (string)reader["AddressWorkTime"];
                            itemAddress.AddressPhone = (string)reader["AddressPhone"];
                            itemAddress.AddressEmail = (string)reader["AddressEmail"];

                            (last.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        if (_metroId.HasValue && last.Metros.Count(i => i.MetroId == _metroId) == 0)
                        {
                            var itemMetro = new ExtendedMetro();
                            itemMetro.MetroId = (int)reader["MetroId"];
                            itemMetro.Name = (string)reader["MetroName"];
                            (last.Metros as List<ExtendedMetro>).Add(itemMetro);
                        }
                    }
                    else
                    {
                        ExtendedUdod item = new ExtendedUdod();
                        item.UdodId = (long)reader["UdodId"];
                        item.Name = (string)reader["Name"];
                        item.ShortName = (string)reader["ShortName"];
                        item.JurAddress = (string)reader["JurAddress"];
                        item.CityId = (int)reader["CityId"];
                        item.CityName = (string)reader["CityName"];
                        item.FioDirector = (string)reader["FioDirector"];
                        item.PhoneNumber = (string)reader["PhoneNumber"];
                        item.FaxNumber = (string)reader["FaxNumber"];
                        item.EMail = (string)reader["EMail"];
                        item.SiteUrl = (string)reader["SiteUrl"];
                        item.UdodTypeId = (int)reader["UdodTypeId"];
                        item.UdodStatusId = (int)reader["UdodStatusId"];
                        item.IsContingentEnabled = (bool)reader["IsContingentEnabled"];
                        item.IsDOEnabled = (bool)reader["IsDOEnabled"];
                        item.IsInvalid = (bool)reader["IsInvalid"];

                        item.Addresses = new List<ExtendedAddress>();

                        if (_addressId.HasValue)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];

                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.AddressWorkTime = (string)reader["AddressWorkTime"];
                            itemAddress.AddressPhone = (string)reader["AddressPhone"];
                            itemAddress.AddressEmail = (string)reader["AddressEmail"];

                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        item.Metros = new List<ExtendedMetro>();

                        if (_metroId.HasValue)
                        {
                            var itemMetro = new ExtendedMetro();
                            itemMetro.MetroId = (int)reader["MetroId"];
                            itemMetro.Name = (string)reader["MetroName"];
                            (item.Metros as List<ExtendedMetro>).Add(itemMetro);
                        }
                        list.Add(item);
                    }
                }
            }

            return list;
        }

        public int GetUdodsCount(int? cityId, string cityCode, string rayonCode, int? metroId, int? programId, int? profileId, int? sectionId, string udodName, int? typeBudgetId, string sectionName, string profileName, string programName, string childUnionName, int? rayonId, int? UdodStatusId, bool? isContingent, bool? IsOsip, string number)
        {
            List<ExtendedUdod> list = new List<ExtendedUdod>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetUdodCount", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId.Value));
                if (metroId.HasValue) comm.Parameters.Add(new SqlParameter("MetroId", metroId.Value));
                if (rayonId.HasValue) comm.Parameters.Add(new SqlParameter("RayonId", rayonId.Value));
                if (!string.IsNullOrEmpty(cityCode)) comm.Parameters.Add(new SqlParameter("CityCode", cityCode));
                if (!string.IsNullOrEmpty(rayonCode)) comm.Parameters.Add(new SqlParameter("RayonCode", rayonCode));
                if (!string.IsNullOrEmpty(udodName)) comm.Parameters.Add(new SqlParameter("UdodName", udodName));
                if (!string.IsNullOrEmpty(programName)) comm.Parameters.Add(new SqlParameter("ProgramName", programName));
                if (!string.IsNullOrEmpty(profileName)) comm.Parameters.Add(new SqlParameter("ProfileName", profileName));
                if (!string.IsNullOrEmpty(sectionName)) comm.Parameters.Add(new SqlParameter("SectionName", sectionName));
                if (!string.IsNullOrEmpty(childUnionName)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnionName));
                if (programId.HasValue) comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                if (profileId.HasValue) comm.Parameters.Add(new SqlParameter("ProfileId", profileId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                if (typeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                if (UdodStatusId.HasValue) comm.Parameters.Add(new SqlParameter("UdodStatusId", UdodStatusId));
                if (isContingent.HasValue) comm.Parameters.Add(new SqlParameter("IsContingent", isContingent));
                if (IsOsip.HasValue) comm.Parameters.Add(new SqlParameter("IsOsip", IsOsip));
                if (!string.IsNullOrEmpty(number)) comm.Parameters.Add(new SqlParameter("Number", number));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }
            return 0;
        }

        public List<ExtendedUdod> GetUdodsForService(int? cityId, string cityCode, string rayonCode, int? metroId, int? programId, int? profileId, int? sectionId, string udodName, int? typeBudgetId, string sectionName, int? rayonId)
        {
            List<ExtendedUdod> list = new List<ExtendedUdod>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetUdodForService", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId.Value));
                if (metroId.HasValue) comm.Parameters.Add(new SqlParameter("MetroId", metroId.Value));
                if (rayonId.HasValue) comm.Parameters.Add(new SqlParameter("RayonId", rayonId.Value));
                if (!string.IsNullOrEmpty(cityCode)) comm.Parameters.Add(new SqlParameter("CityCode", cityCode));
                if (!string.IsNullOrEmpty(rayonCode)) comm.Parameters.Add(new SqlParameter("RayonCode", rayonCode));
                if (!string.IsNullOrEmpty(udodName)) comm.Parameters.Add(new SqlParameter("UdodName", udodName));
                if (!string.IsNullOrEmpty(sectionName)) comm.Parameters.Add(new SqlParameter("SectionName", sectionName));
                if (programId.HasValue) comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                if (profileId.HasValue) comm.Parameters.Add(new SqlParameter("ProfileId", profileId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                if (typeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long _udodId = (long)reader["UdodId"];
                    long? _addressId = null;
                    int? _metroId = null;
                    int? _sectionId = null;
                    int? _programId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long)reader["AddressId"];
                    if (reader["MetroId"] != DBNull.Value) _metroId = (int)reader["MetroId"];
                    if (reader["SectionId"] != DBNull.Value) _sectionId = (int)reader["SectionId"];
                    if (reader["ProgramId"] != DBNull.Value) _programId = (int)reader["ProgramId"];

                    ExtendedUdod last = list.LastOrDefault(i => i.UdodId == _udodId);

                    if (null != last)
                    {
                        if (_programId.HasValue && last.Programs.Count(i => i.ProgramId == _programId) == 0)
                        {
                            ExtendedProgram program = new ExtendedProgram()
                            {
                                Name = (string)reader["ProgramName"],
                                ProgramId = _programId.Value
                                
                            };
                            last.Programs.Add(program);
                        }

                        if (_sectionId.HasValue && last.Sections.Count(i => i.SectionId == _sectionId) == 0)
                        {
                            ExtendedSection section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                                SectionId = _sectionId.Value
                            };
                            last.Sections.Add(section);
                        }
                        if (_addressId.HasValue && last.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.AddressWorkTime = (string)reader["AddressWorkTime"];
                            itemAddress.AddressPhone = (string)reader["AddressPhone"];
                            itemAddress.AddressEmail = (string)reader["AddressEmail"];

                            (last.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        if (_metroId.HasValue && last.Metros.Count(i => i.MetroId == _metroId) == 0)
                        {
                            var itemMetro = new ExtendedMetro();
                            itemMetro.MetroId = (int)reader["MetroId"];
                            itemMetro.Name = (string)reader["MetroName"];
                            (last.Metros as List<ExtendedMetro>).Add(itemMetro);
                        }
                    }
                    else
                    {
                        ExtendedUdod item = new ExtendedUdod();
                        item.UdodId = (long)reader["UdodId"];
                        item.Name = (string)reader["Name"];
                        item.ShortName = (string)reader["ShortName"];
                        item.JurAddress = (string)reader["JurAddress"];
                        item.CityId = (int)reader["CityId"];
                        item.CityName = (string)reader["CityName"];
                        item.FioDirector = (string)reader["FioDirector"];
                        item.PhoneNumber = (string)reader["PhoneNumber"];
                        item.FaxNumber = (string)reader["FaxNumber"];
                        item.EMail = (string)reader["EMail"];
                        item.SiteUrl = (string)reader["SiteUrl"];
                        item.UdodTypeId = (int)reader["UdodTypeId"];

                        item.Addresses = new List<ExtendedAddress>();

                        if (_addressId.HasValue)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];

                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.AddressWorkTime = (string)reader["AddressWorkTime"];
                            itemAddress.AddressPhone = (string)reader["AddressPhone"];
                            itemAddress.AddressEmail = (string)reader["AddressEmail"];

                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        item.Metros = new List<ExtendedMetro>();

                        if (_metroId.HasValue)
                        {
                            var itemMetro = new ExtendedMetro();
                            itemMetro.MetroId = (int)reader["MetroId"];
                            itemMetro.Name = (string)reader["MetroName"];
                            (item.Metros as List<ExtendedMetro>).Add(itemMetro);
                        }

                        item.Sections=new List<ExtendedSection>();
                        if (_sectionId.HasValue)
                        {
                            ExtendedSection section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                                SectionId = _sectionId.Value
                            };
                            item.Sections.Add(section);
                        }

                        item.Programs = new List<ExtendedProgram>();
                        if (_programId.HasValue)
                        {
                            ExtendedProgram program = new ExtendedProgram()
                            {
                                Name = (string)reader["ProgramName"],
                                ProgramId = _programId.Value
                                
                            };
                            item.Programs.Add(program);
                        }

                        list.Add(item);
                    }
                }
            }

            UdodsCount = list.Count;
            return list;
        }

        public long AddUdod(string name, string shortName, string jurAddress, int cityId, string fioDirector, string phoneNumber, string faxNumber, string email, string siteUrl, int udodTypeId, string udodNumber)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("AddUdod", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Name", name));
                comm.Parameters.Add(new SqlParameter("ShortName", shortName));
                comm.Parameters.Add(new SqlParameter("JurAddress", jurAddress));
                comm.Parameters.Add(new SqlParameter("CityId", cityId));
                comm.Parameters.Add(new SqlParameter("FioDirector", fioDirector));
                comm.Parameters.Add(new SqlParameter("PhoneNumber", phoneNumber));
                comm.Parameters.Add(new SqlParameter("FaxNumber", faxNumber));
                comm.Parameters.Add(new SqlParameter("EMail", email));
                comm.Parameters.Add(new SqlParameter("SiteUrl", siteUrl));
                comm.Parameters.Add(new SqlParameter("UdodTypeId", udodTypeId));
                comm.Parameters.Add(new SqlParameter("UdodNumber", udodNumber));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    return (long)reader["UdodId"];
                }
            }
            return -1;
        }

        public void UpdateUdod(string name, string shortName, string jurAddress, int cityId, string fioDirector, string phoneNumber, string faxNumber, string email, string siteUrl, int udodTypeId, bool isDOEnabled, string udodNumber, long udodId, bool isInvalid)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateUdod", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Name", name));
                comm.Parameters.Add(new SqlParameter("ShortName", shortName));
                comm.Parameters.Add(new SqlParameter("JurAddress", jurAddress));
                comm.Parameters.Add(new SqlParameter("CityId", cityId));
                comm.Parameters.Add(new SqlParameter("FioDirector", fioDirector));
                comm.Parameters.Add(new SqlParameter("PhoneNumber", phoneNumber));
                comm.Parameters.Add(new SqlParameter("FaxNumber", faxNumber));
                comm.Parameters.Add(new SqlParameter("EMail", email));
                comm.Parameters.Add(new SqlParameter("SiteUrl", siteUrl));
                comm.Parameters.Add(new SqlParameter("UdodTypeId", udodTypeId));
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("IsDOEnabled", isDOEnabled));
                comm.Parameters.Add(new SqlParameter("IsInvalid", isInvalid));
                comm.Parameters.Add(new SqlParameter("UdodNumber", udodNumber));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteUdod(long UdodId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("DeleteUdod", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UdodId", UdodId));
                comm.ExecuteNonQuery();
            }
        }

        public ExtendedUdod GetUdod(long udodId)
        {
            ExtendedUdod item = null;
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("GetUdod", Connection){CommandType = CommandType.StoredProcedure};
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                SqlDataReader reader = comm.ExecuteReader();
                
                while (reader.Read())
                {
                    long? _addressId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long)reader["AddressId"];
                    int? _metroId = null;
                    if (reader["MetroId"] != DBNull.Value) _metroId = (int)reader["MetroId"];

                    if (null != item)
                    {
                        if (_addressId.HasValue && item.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];

                            itemAddress.BtiHouseCode = null;
                            if (reader["BtiHouseCode"] != DBNull.Value) itemAddress.BtiHouseCode = (int)reader["BtiHouseCode"];
                            
                            itemAddress.AddressWorkTime = (string)reader["AddressWorkTime"];
                            itemAddress.AddressPhone = (string)reader["AddressPhone"];
                            itemAddress.AddressEmail = (string)reader["AddressEmail"];
                            itemAddress.AddressUdodName = (string)reader["AddressUdodName"];
                            itemAddress.IsPrimary = (bool)reader["IsPrimary"];

                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            //var fullAddress = new ExtendedAddress();
                            //using (KladrDb db = new KladrDb())
                            //{
                            //    fullAddress = db.GetAddress(itemAddress.KladrCode, itemAddress.HouseNumber, itemAddress.Fraction, itemAddress.Housing, itemAddress.Building, "НУЛ");
                            //}
                            //fullAddress.AddressId = itemAddress.AddressId;
                            //fullAddress.AddressType = itemAddress.AddressType;
                            //fullAddress.AddressWorkTime = itemAddress.AddressWorkTime;
                            //fullAddress.AddressPhone = itemAddress.AddressPhone;
                            //fullAddress.AddressEmail = itemAddress.AddressEmail;

                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        if (_metroId.HasValue && item.Metros.Count(i => i.MetroId == _metroId) == 0)
                        {
                            var itemMetro = new ExtendedMetro();
                            itemMetro.MetroId = (int)reader["MetroId"];
                            itemMetro.Name = (string)reader["MetroName"];
                            (item.Metros as List<ExtendedMetro>).Add(itemMetro);
                        }
                    }
                    else
                    {
                        item = new ExtendedUdod();
                        item.UdodId = (long)reader["UdodId"];
                        item.Name = (string)reader["Name"];
                        item.ShortName = (string)reader["ShortName"];
                        item.JurAddress = (string)reader["JurAddress"];
                        item.CityId = (int)reader["CityId"];
                        item.CityName = (string)reader["CityName"];
                        item.FioDirector = (string)reader["FioDirector"];
                        item.PhoneNumber = (string)reader["PhoneNumber"];
                        item.FaxNumber = (string)reader["FaxNumber"];
                        item.EMail = (string)reader["EMail"];
                        item.SiteUrl = (string)reader["SiteUrl"];
                        item.UdodTypeId = (int)reader["UdodTypeId"];
                        item.UdodStatusId = (int)reader["UdodStatusId"];
                        item.EkisId = (int)reader["EkisId"];
                        item.ReestrId = null;
                        if (reader["ReestrId"] != DBNull.Value) item.ReestrId = (int)reader["ReestrId"];
                        item.IsInvalid = (bool)reader["IsInvalid"];
                        item.IsDOEnabled = (bool)reader["IsDOEnabled"];
                        if (reader["IsContingentEnabled"] == DBNull.Value) item.IsContingentEnabled = false;
                        else
                            item.IsContingentEnabled = (bool)reader["IsContingentEnabled"];
                        item.UdodNumber = (string)reader["UdodNumber"];

                        item.Addresses = new List<ExtendedAddress>();

                        if (_addressId.HasValue)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];
                            
                            itemAddress.BtiHouseCode = null;
                            if (reader["BtiHouseCode"] != DBNull.Value) itemAddress.BtiHouseCode = (int)reader["BtiHouseCode"];
                            
                            itemAddress.AddressWorkTime = (string)reader["AddressWorkTime"];
                            itemAddress.AddressPhone = (string)reader["AddressPhone"];
                            itemAddress.AddressEmail = (string)reader["AddressEmail"];
                            itemAddress.AddressUdodName = (string)reader["AddressUdodName"];
                            itemAddress.IsPrimary = (bool)reader["IsPrimary"];

                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            /*var fullAddress = new ExtendedAddress();
                            using (KladrDb db = new KladrDb())
                            {
                                fullAddress = db.GetAddress(itemAddress.KladrCode, itemAddress.HouseNumber, itemAddress.Fraction, itemAddress.Housing, itemAddress.Building, "НУЛ");
                            }
                            fullAddress.AddressId = itemAddress.AddressId;
                            fullAddress.AddressType = itemAddress.AddressType;
                            fullAddress.AddressWorkTime = itemAddress.AddressWorkTime;
                            fullAddress.AddressPhone = itemAddress.AddressPhone;
                            fullAddress.AddressEmail = itemAddress.AddressEmail;*/

                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        item.Metros = new List<ExtendedMetro>();

                        if (_metroId.HasValue)
                        {
                            var itemMetro = new ExtendedMetro();
                            itemMetro.MetroId = (int)reader["MetroId"];
                            itemMetro.Name = (string)reader["MetroName"];

                            (item.Metros as List<ExtendedMetro>).Add(itemMetro);
                        }
                    }
                }
            }
            return item;
        }

        public ExtendedUdod GetUdodByEkis(int ekisId)
        {
            ExtendedUdod item = null;
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("GetUdodByEkis", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("EkisId", ekisId));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    long? _addressId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long)reader["AddressId"];
                    int? _metroId = null;
                    if (reader["MetroId"] != DBNull.Value) _metroId = (int)reader["MetroId"];

                    if (null != item)
                    {
                        if (_addressId.HasValue && item.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];

                            itemAddress.BtiHouseCode = null;
                            if (reader["BtiHouseCode"] != DBNull.Value) itemAddress.BtiHouseCode = (int)reader["BtiHouseCode"];

                            itemAddress.AddressWorkTime = (string)reader["AddressWorkTime"];
                            itemAddress.AddressPhone = (string)reader["AddressPhone"];
                            itemAddress.AddressEmail = (string)reader["AddressEmail"];
                            itemAddress.AddressUdodName = (string)reader["AddressUdodName"];
                            itemAddress.IsPrimary = (bool)reader["IsPrimary"];

                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            //var fullAddress = new ExtendedAddress();
                            //using (KladrDb db = new KladrDb())
                            //{
                            //    fullAddress = db.GetAddress(itemAddress.KladrCode, itemAddress.HouseNumber, itemAddress.Fraction, itemAddress.Housing, itemAddress.Building, "НУЛ");
                            //}
                            //fullAddress.AddressId = itemAddress.AddressId;
                            //fullAddress.AddressType = itemAddress.AddressType;
                            //fullAddress.AddressWorkTime = itemAddress.AddressWorkTime;
                            //fullAddress.AddressPhone = itemAddress.AddressPhone;
                            //fullAddress.AddressEmail = itemAddress.AddressEmail;

                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        if (_metroId.HasValue && item.Metros.Count(i => i.MetroId == _metroId) == 0)
                        {
                            var itemMetro = new ExtendedMetro();
                            itemMetro.MetroId = (int)reader["MetroId"];
                            itemMetro.Name = (string)reader["MetroName"];
                            (item.Metros as List<ExtendedMetro>).Add(itemMetro);
                        }
                    }
                    else
                    {
                        item = new ExtendedUdod();
                        item.UdodId = (long)reader["UdodId"];
                        item.Name = (string)reader["Name"];
                        item.ShortName = (string)reader["ShortName"];
                        item.JurAddress = (string)reader["JurAddress"];
                        item.CityId = (int)reader["CityId"];
                        item.CityName = (string)reader["CityName"];
                        item.FioDirector = (string)reader["FioDirector"];
                        item.PhoneNumber = (string)reader["PhoneNumber"];
                        item.FaxNumber = (string)reader["FaxNumber"];
                        item.EMail = (string)reader["EMail"];
                        item.SiteUrl = (string)reader["SiteUrl"];
                        item.UdodTypeId = (int)reader["UdodTypeId"];
                        item.UdodStatusId = (int)reader["UdodStatusId"];
                        item.EkisId = (int)reader["EkisId"];
                        item.ReestrId = null;
                        if (reader["ReestrId"] != DBNull.Value) item.ReestrId = (int)reader["ReestrId"];
                        item.IsInvalid = (bool)reader["IsInvalid"];
                        item.IsDOEnabled = (bool)reader["IsDOEnabled"];
                        if (reader["IsContingentEnabled"] == DBNull.Value) item.IsContingentEnabled = false;
                        else
                            item.IsContingentEnabled = (bool)reader["IsContingentEnabled"];
                        item.UdodNumber = (string)reader["UdodNumber"];

                        item.Addresses = new List<ExtendedAddress>();

                        if (_addressId.HasValue)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];

                            itemAddress.BtiHouseCode = null;
                            if (reader["BtiHouseCode"] != DBNull.Value) itemAddress.BtiHouseCode = (int)reader["BtiHouseCode"];

                            itemAddress.AddressWorkTime = (string)reader["AddressWorkTime"];
                            itemAddress.AddressPhone = (string)reader["AddressPhone"];
                            itemAddress.AddressEmail = (string)reader["AddressEmail"];
                            itemAddress.AddressUdodName = (string)reader["AddressUdodName"];
                            itemAddress.IsPrimary = (bool)reader["IsPrimary"];

                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            /*var fullAddress = new ExtendedAddress();
                            using (KladrDb db = new KladrDb())
                            {
                                fullAddress = db.GetAddress(itemAddress.KladrCode, itemAddress.HouseNumber, itemAddress.Fraction, itemAddress.Housing, itemAddress.Building, "НУЛ");
                            }
                            fullAddress.AddressId = itemAddress.AddressId;
                            fullAddress.AddressType = itemAddress.AddressType;
                            fullAddress.AddressWorkTime = itemAddress.AddressWorkTime;
                            fullAddress.AddressPhone = itemAddress.AddressPhone;
                            fullAddress.AddressEmail = itemAddress.AddressEmail;*/

                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        item.Metros = new List<ExtendedMetro>();

                        if (_metroId.HasValue)
                        {
                            var itemMetro = new ExtendedMetro();
                            itemMetro.MetroId = (int)reader["MetroId"];
                            itemMetro.Name = (string)reader["MetroName"];

                            (item.Metros as List<ExtendedMetro>).Add(itemMetro);
                        }
                    }
                }
            }
            return item;
        }

        public void MergeOrAttachUdods(long FirstUdodId, long SecondUdodId, string Name, string ShortName)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Udod_Reorganisation_Merge", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("FirstUdodId", FirstUdodId));
                comm.Parameters.Add(new SqlParameter("SecondUdodId", SecondUdodId));
                if (Name != "") comm.Parameters.Add(new SqlParameter("Name", Name));
                if (ShortName != "") comm.Parameters.Add(new SqlParameter("ShortName", ShortName));
                comm.ExecuteNonQuery();
            }
        }

        public void DetachOrDivideUdods(long UdodId, string SecondAddressIds, string SecondTeacherIds, string SecondMetroIds,
            ExtendedUdod firstPassport, ExtendedUdod secondPassport)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Udod_Reorganisation_Detach", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UdodId", UdodId));
                comm.Parameters.Add(new SqlParameter("SecondUdodAddressIds", SecondAddressIds));
                comm.Parameters.Add(new SqlParameter("SecondUdodTeacherIds", SecondTeacherIds));
                comm.Parameters.Add(new SqlParameter("SecondUdodMetroIds", SecondMetroIds));
                if (firstPassport != null)
                {
                    comm.Parameters.Add(new SqlParameter("FirstName", firstPassport.Name));
                    comm.Parameters.Add(new SqlParameter("FirstShortName", firstPassport.ShortName));
                    comm.Parameters.Add(new SqlParameter("FirstJurAddress", firstPassport.JurAddress));
                    comm.Parameters.Add(new SqlParameter("FirstCityId", firstPassport.CityId));
                    comm.Parameters.Add(new SqlParameter("FirstFioDirector", firstPassport.FioDirector));
                    comm.Parameters.Add(new SqlParameter("FirstPhoneNumber", firstPassport.PhoneNumber)); 
                    comm.Parameters.Add(new SqlParameter("FirstFaxNumber", firstPassport.FaxNumber));
                    comm.Parameters.Add(new SqlParameter("FirstEmail", firstPassport.EMail));
                    comm.Parameters.Add(new SqlParameter("FirstSiteUrl", firstPassport.SiteUrl));
                }
                comm.Parameters.Add(new SqlParameter("SecondName", secondPassport.Name));
                comm.Parameters.Add(new SqlParameter("SecondShortName", secondPassport.ShortName));
                comm.Parameters.Add(new SqlParameter("SecondJurAddress", secondPassport.JurAddress));
                comm.Parameters.Add(new SqlParameter("SecondCityId", secondPassport.CityId));
                comm.Parameters.Add(new SqlParameter("SecondFioDirector", secondPassport.FioDirector));
                comm.Parameters.Add(new SqlParameter("SecondPhoneNumber", secondPassport.PhoneNumber));
                comm.Parameters.Add(new SqlParameter("SecondFaxNumber", secondPassport.FaxNumber));
                comm.Parameters.Add(new SqlParameter("SecondEmail", secondPassport.EMail));
                comm.Parameters.Add(new SqlParameter("SecondSiteUrl", secondPassport.SiteUrl));
                
                comm.ExecuteNonQuery();
            }
        }

        public ExtendedUdodSectionProfileProgram GetUdodSections(long udodId, int? programId, int? profileId)
        {
            ExtendedUdodSectionProfileProgram item = new ExtendedUdodSectionProfileProgram();
            item.Profiles = new List<ExtendedProfile>();
            item.Programs = new List<ExtendedProgram>();
            item.Sections = new List<ExtendedSection>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetUdodSections", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (programId.HasValue) comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                if (profileId.HasValue) comm.Parameters.Add(new SqlParameter("ProfileId", profileId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    int _programId = (int) reader["ProgramId"];
                    int _profileId = (int) reader["ProfileId"];
                    int _sectionId = (int) reader["SectionId"];

                    if (item.Programs.Count(i=>i.ProgramId==_programId)==0)
                    {
                        (item.Programs as List<ExtendedProgram>).Add(new ExtendedProgram(){ProgramId = _programId, Name = (string)reader["ProgramName"]});
                    }
                    if (item.Profiles.Count(i => i.ProfileId == _profileId) == 0)
                    {
                        (item.Profiles as List<ExtendedProfile>).Add(new ExtendedProfile() { ProfileId = _profileId, Name = (string)reader["ProfileName"] });
                    }
                    if (item.Sections.Count(i => i.SectionId == _sectionId) == 0)
                    {
                        (item.Sections as List<ExtendedSection>).Add(new ExtendedSection() { SectionId = _sectionId, Name = (string)reader["SectionName"] });
                    }
                }
            }

            return item;
        }

        public List<ExtendedUdodMetro> GetMetro(long udodId)
        {
            List<ExtendedUdodMetro> list =new List<ExtendedUdodMetro>();

            using (SqlConnection conn =new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetMetro", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedUdodMetro item = new ExtendedUdodMetro();
                    item.UdodMetroId = (long) reader["UdodMetroId"];
                    item.NameMetro = (string) reader["Name"];
                    list.Add(item);
                }
            }

            return list;
        }
        public void AddMetro(long udodId, int metroId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_AddMetro", conn) {CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("MetroId", metroId));
                comm.ExecuteNonQuery();
            }
        }
        public void DeleteMetro(long udodMetroId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_DeleteMetro", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodMetroId", udodMetroId));
                comm.ExecuteNonQuery();
            }
        }
        public int[] GetReestrDogmId(long udodId)
        {
            int[] _id = new int[2];
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("SELECT Cast(eo_id as int) as eo_id, ReestrId From Udod WHERE (Udod.UdodId = @UdodId)", conn) { CommandType = CommandType.Text };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    _id[0] = (int)reader["eo_id"];
                    _id[1] = reader["ReestrId"]!=DBNull.Value?(int)reader["ReestrId"]:0;
                }
            }

            return _id;
        }
        public void SetReestrId(long udodId, int reestrId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("update Udod set ReestrId=@ReestrId WHERE (Udod.UdodId = @UdodId)", conn) { CommandType = CommandType.Text };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("ReestrId", reestrId));
                SqlDataReader reader = comm.ExecuteReader();
                
            }
        }

        public List<ExtendedUdodStatus> GetUdodStatuses()
        {
            List<ExtendedUdodStatus> list = new List<ExtendedUdodStatus>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("SELECT DictUdodStatusId, DictUdodStatusName FROM Dict_UdodStatus", conn) { CommandType = CommandType.Text };
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedUdodStatus item = new ExtendedUdodStatus();
                    item.UdodStatusId = (int)reader["DictUdodStatusId"];
                    item.UdodStatusName = (string)reader["DictUdodStatusName"];
                    list.Add(item);
                }
            }

            return list;
        }

        public void UpdateUdodStatus(long udodId, int newStatus)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UPDATE Udod SET UdodStatusId = @newStatus WHERE UdodId=@UdodId", Connection) { CommandType = CommandType.Text };
                comm.Parameters.Add(new SqlParameter("newStatus", newStatus));
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.ExecuteNonQuery();
            }
        }

        public bool UpdateUdodPrimaryAddress(long addressId, long udodId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Udod_UpdatePrimaryAddress", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("AddressId", addressId));
                var reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (bool) reader[0];
                }
            }
            return false;
        }

        public void SetContingentEnabled(Int64 udodId, bool value)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UPDATE Udod SET IsContingentEnabled = @value WHERE UdodId=@UdodId", Connection) { CommandType = CommandType.Text };
                comm.Parameters.Add(new SqlParameter("value", value));
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.ExecuteNonQuery();
            }
        }

        public ExtendedUdodDopInfo GetUdodDopInfo(long udodId)
        {
            ExtendedUdodDopInfo item = new ExtendedUdodDopInfo();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetUdodDopInfo", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    item.Activity = (string)reader["Activity"];
                    item.CityzenShip = (string)reader["CityzenShip"];
                    item.code_okato = (string)reader["code_okato"];
                    item.DopInfoId = (long)reader["DopInfoId"];
                    item.DopName = (string)reader["DopName"];
                    item.INN = (string)reader["INN"];
                    item.Is_Individual = (bool)reader["Is_Individual"];
                    item.Is_Main = (bool)reader["Is_Main"];
                    item.KPP = (string)reader["KPP"];
                    item.legalform = (string)reader["legalform"];
                    item.maxPupils = (int)reader["maxPupils"];
                    item.OGRN = (string)reader["OGRN"];
                    item.OKOGU = (string)reader["OKOGU"];
                    item.ownership = (int)reader["ownership"];
                    item.sector = (string)reader["sector"];
                    item.Shift = (int)reader["Shift"];
                    item.Status = (int)reader["Status"];
                    item.study_form = (int)reader["study_form"];
                    item.type_UDOD = (int)reader["type_UDOD"];
                    item.IsFilial = (Boolean)reader["IsFilial"];
                    item.CountSmen = (int)reader["CountSmen"];
                }
            }

            return item;
        }

        public void UpdateUdodDopInfo(long udodId, ExtendedUdodDopInfo udodDopInfo)
        {
            //ExtendedUdodDopInfo item = new ExtendedUdodDopInfo();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_UpdateUdodDopInfo", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("Activity", udodDopInfo.Activity));
                comm.Parameters.Add(new SqlParameter("CityzenShip", udodDopInfo.CityzenShip));
                comm.Parameters.Add(new SqlParameter("code_okato", udodDopInfo.code_okato));
                comm.Parameters.Add(new SqlParameter("DopInfoId", udodDopInfo.DopInfoId));
                comm.Parameters.Add(new SqlParameter("DopName", udodDopInfo.DopName));
                comm.Parameters.Add(new SqlParameter("INN", udodDopInfo.INN));
                comm.Parameters.Add(new SqlParameter("Is_Individual", udodDopInfo.Is_Individual));
                comm.Parameters.Add(new SqlParameter("Is_Main", udodDopInfo.Is_Main));
                comm.Parameters.Add(new SqlParameter("KPP", udodDopInfo.KPP));
                comm.Parameters.Add(new SqlParameter("legalform", udodDopInfo.legalform));
                comm.Parameters.Add(new SqlParameter("OGRN", udodDopInfo.OGRN));
                comm.Parameters.Add(new SqlParameter("OKOGU", udodDopInfo.OKOGU));
                comm.Parameters.Add(new SqlParameter("ownership", udodDopInfo.ownership));
                comm.Parameters.Add(new SqlParameter("sector", udodDopInfo.sector));
                comm.Parameters.Add(new SqlParameter("Shift", udodDopInfo.Shift));
                comm.Parameters.Add(new SqlParameter("maxPupils", udodDopInfo.maxPupils));
                comm.Parameters.Add(new SqlParameter("Status", udodDopInfo.Status));
                comm.Parameters.Add(new SqlParameter("study_form", udodDopInfo.study_form));
                comm.Parameters.Add(new SqlParameter("type_UDOD", udodDopInfo.type_UDOD));
                comm.Parameters.Add(new SqlParameter("IsFilial", udodDopInfo.IsFilial));
                comm.Parameters.Add(new SqlParameter("CountSmen", udodDopInfo.CountSmen));
                comm.ExecuteNonQuery();
            }

            //return item;
        }

        // 1 это лицензия, 2 это аккредитация
        public ExtendedUdodLicense GetUdodLicense(long udodId, int typeLicense)
        {
            ExtendedUdodLicense item = new ExtendedUdodLicense();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetUdodLicense", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                // 1 это лицензия, 2 это аккредитация
                comm.Parameters.Add(new SqlParameter("TypeLicense", typeLicense));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    item.Series = (string)reader["Series"];
                    item.Number = (string)reader["Number"];
                    item.ReestrDate = (DateTime)reader["ReestrDate"];
                    item.ReestrNumber = (string)reader["ReestrNumber"];
                    item.ReestrValidity = (DateTime)reader["ReestrValidity"];
                    item.UdodLicenseId = (long)reader["UdodLicenseId"];

                }
            }

            return item;
        }

        public void UpdateUdodLicense(long udodId, ExtendedUdodLicense udodLicense, int typeLicense)
        {
            //ExtendedUdodDopInfo item = new ExtendedUdodDopInfo();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_UpdateUdodLicense", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("TypeLicense", typeLicense));

                comm.Parameters.Add(new SqlParameter("Number", udodLicense.Number));
                comm.Parameters.Add(new SqlParameter("ReestrDate", udodLicense.ReestrDate));
                comm.Parameters.Add(new SqlParameter("UdodLicenseId", udodLicense.UdodLicenseId));
                comm.Parameters.Add(new SqlParameter("Series", udodLicense.Series));
                comm.Parameters.Add(new SqlParameter("ReestrValidity", udodLicense.ReestrValidity));
                comm.Parameters.Add(new SqlParameter("ReestrNumber", udodLicense.ReestrNumber));
                
                comm.ExecuteNonQuery();
            }

            //return item;
        }

        public List<ExtendedUdod> GetMainUdods()
        {
            List<ExtendedUdod> list = new List<ExtendedUdod>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("SELECT Udod.UdodId, Udod.ShortName FROM Udod_DopInfo INNER JOIN Udod ON Udod_DopInfo.UdodId = Udod.UdodId WHERE (Udod_DopInfo.IsFilial = 0)", Connection) { CommandType = CommandType.Text };
                
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedUdod item = new ExtendedUdod();
                    item.UdodId = (long)reader["UdodId"];
                    item.ShortName = (string)reader["ShortName"];

                    list.Add(item);
                }
            }

            return list;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Udod.Dal
{
    public class DictProgramDb : BaseDb
    {
        private int ProgramsCount;

        public DictProgramDb() {}

        public List<ExtendedProgram> GetPrograms(long? udodId)
        {
            List<ExtendedProgram> list = new List<ExtendedProgram>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetProgram", Connection) { CommandType = CommandType.StoredProcedure };
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedProgram item = new ExtendedProgram();
                    item.ProgramId = (int)reader["ProgramId"];
                    item.Name = (string)reader["Name"];
                    list.Add(item);
                }
            }

            ProgramsCount = list.Count;
            return list;
        }

        public List<ExtendedProgram> GetUdodPrograms(long udodId)
        {
            List<ExtendedProgram> list = new List<ExtendedProgram>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetUdodProgram", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedProgram item = new ExtendedProgram();
                    item.ProgramId = (int)reader["ProgramId"];
                    item.Name = (string)reader["Name"];
                    item.isExistInCurrentUdod = !(reader["UdodProgramId"] == DBNull.Value);
                    item.isEnabledToChange = ((int)reader["CUCount"] == 0);
                    item.ChildUnionCount = (int)reader["CUCount"];
                    list.Add(item);
                }
            }

            return list;
        }

        public void UpdateUdodProgram(long udodId, int programId, bool isChecked)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateUdodProgram", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("udodId", udodId));
                comm.Parameters.Add(new SqlParameter("programId", programId));
                comm.Parameters.Add(new SqlParameter("isDeleted", !isChecked));
                comm.ExecuteNonQuery();
            }
        }

        public List<ExtendedProgram> GetPrograms(string code, string name, int count)
        {
            List<ExtendedProgram> list = new List<ExtendedProgram>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetProgramNew", Connection) { CommandType = CommandType.StoredProcedure };
                
                if (!string.IsNullOrEmpty(code)) comm.Parameters.Add(new SqlParameter("code", Convert.ToInt32(code)));
                comm.Parameters.Add(new SqlParameter("count", count));
                comm.Parameters.Add(new SqlParameter("name", name));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedProgram item = new ExtendedProgram();
                    item.ProgramId = (int)reader["ProgramId"];
                    item.Name = (string)reader["Name"];
                    list.Add(item);
                }
            }

            //ProgramsCount = list.Count;
            return list;
        }

        public void AddProgram(string Name)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("AddDictProgram", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Name", Name));
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateProgram(string Name, long ProgramId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateDictProgram", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Name", Name));
                comm.Parameters.Add(new SqlParameter("ProgramId", ProgramId));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteProgram(long ProgramId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("DeleteDictProgram", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ProgramId", ProgramId));
                comm.ExecuteNonQuery();
            }
        }

        public ExtendedProgram? GetProgram(long? programId, string programName)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetProgram", Connection) {CommandType = CommandType.StoredProcedure };
                if (programId.HasValue)comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                if (!string.IsNullOrEmpty(programName)) comm.Parameters.Add(new SqlParameter("ProgramName", programName));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedProgram item = new ExtendedProgram();
                    item.ProgramId = (int)reader["ProgramId"];
                    item.Name = (string)reader["Name"];
                    return item;
                }
            }
            return null;
        }

        public int GetProgramsCount()
        {
            return ProgramsCount;
        }
    }
}

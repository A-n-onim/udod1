﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Udod.Dal
{
    public class ReportDb : BaseDb
    {
        public List<ExtendedDataSetValue[]> GetDataSet(int typeDataSet)
        {
            List<ExtendedDataSetValue[]> list = new List<ExtendedDataSetValue[]>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("DataSet_Get"+typeDataSet.ToString(), Connection)
                                      {CommandType = CommandType.StoredProcedure};
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedDataSetValue[] item = new ExtendedDataSetValue[reader.FieldCount];
                    
                    for (int i=0;i<reader.FieldCount;i++)
                    {
                        if (((Type)reader.GetFieldType(i)).Name =="DateTime")
                        {
                            item[i].StrValue = reader.GetDateTime(i).ToShortDateString();
                            item[i].IsInt = false;
                        }
                        else if (((Type)reader.GetFieldType(i)).Name == "Int32")
                        {
                            item[i].StrValue = reader.GetInt32(i).ToString();
                            item[i].IsInt = true;
                        }
                        else
                        {
                            item[i].StrValue = reader.GetString(i);
                            item[i].IsInt = false;
                        }
                        
                    }
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedAgeSexReport> GetAgeSexReport(string strUdodType, string strCityType, long? udodId, bool checkDistinct)
        {
            List<ExtendedAgeSexReport> list = new List<ExtendedAgeSexReport>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetAgeSex", Connection) { CommandType = CommandType.StoredProcedure };
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("udodId", udodId.Value));
                if (!string.IsNullOrEmpty(strUdodType)) comm.Parameters.Add(new SqlParameter("StrUdodType", strUdodType));
                if (!string.IsNullOrEmpty(strCityType)) comm.Parameters.Add(new SqlParameter("StrCityType", strCityType));
                comm.Parameters.Add(new SqlParameter("CheckDistinct", checkDistinct));
                int _oldprogramId = -1;
                int _oldtypeBudgetId = -1;
                string oldprogramName = "";
                string oldTypeBudgetName = "";
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    int _programId = (int)reader["ProgramId"];
                    int _typeBudgetId = (int)reader["DictTypeBudgetId"];
                    
                    var last = list.FirstOrDefault(i => i.ProgramId == _programId && i.TypeBudgetId == _typeBudgetId );

                    if (last != null)
                    {
                        if (reader["AgePupil"] != DBNull.Value)
                        {
                            int num = (int)reader["AgePupil"] - 4;
                            if ((bool)reader["Sex"])
                            {
                                last.CountPupils[num * 3 + 1] = (int)reader["CountPupils"];
                                last.CountPupils[52] += (int)reader["CountPupils"];
                            }
                            else
                            {
                                last.CountPupils[num * 3] = (int)reader["CountPupils"];
                                last.CountPupils[51] += (int)reader["CountPupils"];
                            }
                            last.CountPupils[num * 3 + 2] += (int)reader["CountPupils"];
                            last.CountPupils[53] += (int)reader["CountPupils"];
                        }
                    }
                    else
                    {
                        ExtendedAgeSexReport item = new ExtendedAgeSexReport();
                        item.ProgramId = (int)reader["ProgramId"];
                        item.ProgramName = (string)reader["ProgramName"];
                        item.TypeBudgetId = (int)reader["DictTypeBudgetId"];
                        item.TypeBudgetName = (string)reader["TypeBudgetName"];
                        item.CountPupils = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                        if (reader["AgePupil"] != DBNull.Value)
                        {
                            int num = (int)reader["AgePupil"] - 4;
                            if ((bool)reader["Sex"])
                            {
                                item.CountPupils[num*3+1] = (int)reader["CountPupils"];
                                item.CountPupils[52] = (int)reader["CountPupils"];
                            }
                            else
                            {
                                item.CountPupils[num*3] = (int)reader["CountPupils"];
                                item.CountPupils[51] = (int)reader["CountPupils"];
                            }
                            item.CountPupils[num*3 + 2] += (int) reader["CountPupils"];
                            item.CountPupils[53] = (int)reader["CountPupils"];
                            ExtendedAgeSexReport item1 = new ExtendedAgeSexReport();
                            item1.TypeBudgetId = _oldtypeBudgetId == 2 ? 1 : 2;
                            /*if (_oldprogramId != -1 && _oldprogramId != _programId && item.TypeBudgetId != item1.TypeBudgetId && list.Count(i => i.ProgramId == _oldprogramId) == 1)
                            {
                                item1.ProgramId = _oldprogramId;
                                item1.ProgramName = oldprogramName;
                                item1.TypeBudgetName = _oldtypeBudgetId == 2 ? "Платно" : "Бесплатно";
                                item1.CountPupils = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                                list.Add(item1);
                            }*/
                            list.Add(item);
                            
                            _oldprogramId = _programId;
                            _oldtypeBudgetId = _typeBudgetId;
                            oldprogramName = item.ProgramName;
                            oldTypeBudgetName = item.TypeBudgetName;
                        }
                    }
                    
                }
                ExtendedAgeSexReport item2 = new ExtendedAgeSexReport();
                item2.TypeBudgetId = _oldtypeBudgetId == 2 ? 1 : 2;
                if (_oldprogramId != -1 && list.Count(i => i.ProgramId == _oldprogramId) == 1)
                {
                    item2.ProgramId = _oldprogramId;
                    item2.ProgramName = oldprogramName;
                    item2.TypeBudgetName = _oldtypeBudgetId == 2 ? "Платно" : "Бесплатно";
                    item2.CountPupils = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                    list.Add(item2);
                }
            }
            return list;
        }

        public List<ExtendedClaimReport> GetClaimCountReport(int typeReport, string strUdodType, string strCityType, long? udodId, DateTime? dateBegin, DateTime? dateEnd)
        {
            List<ExtendedClaimReport> list = new List<ExtendedClaimReport>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetClaimCount", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("TypeReport", typeReport));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("udodId", udodId.Value));
                if (dateBegin.HasValue) comm.Parameters.Add(new SqlParameter("DateBegin", dateBegin.Value));
                if (dateEnd.HasValue) comm.Parameters.Add(new SqlParameter("DateEnd", dateEnd.Value));
                if (!string.IsNullOrEmpty(strUdodType)) comm.Parameters.Add(new SqlParameter("StrUdodType", strUdodType));
                if (!string.IsNullOrEmpty(strCityType)) comm.Parameters.Add(new SqlParameter("StrCityType", strCityType));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    int _cityId = (int)reader["CityId"];
                    long _udodId = (long)reader["UdodId"];
                    int _udodTypeId = (int)reader["UdodTypeId"];

                    ExtendedClaimReport last=null;
                    if (typeReport==0)
                    {
                        if (!string.IsNullOrEmpty(strUdodType))
                            last = list.FirstOrDefault(i=>i.UdodTypeId==_udodTypeId);
                        else
                            last = list.FirstOrDefault();
                    }
                    else if (typeReport == 1)
                    {
                        if (!string.IsNullOrEmpty(strUdodType))
                            last = list.FirstOrDefault(i => i.CityId == _cityId && i.UdodTypeId==_udodTypeId);
                        else
                            last = list.FirstOrDefault(i => i.CityId == _cityId);
                    } 
                    if (last != null)
                    {
                        last.CountDOPublic += (int)reader["CountIsPublic"];
                        last.CountDONotPublic += (int)reader["CountIsNotPublic"];
                        last.SumDO += (int)reader["CountIsPublic"] + (int)reader["CountIsNotPublic"];

                        last.CountClaimPGU += (int)reader["ClaimPGU"];
                        last.CountClaimUdod += (int)reader["ClaimNotPGU"];

                        last.SumClaim += (int)reader["ClaimPGU"] + (int)reader["ClaimNotPGU"];

                        last.CountClaimStatus1 += (int)reader["ClaimStatus1"];
                        last.CountClaimStatus7 += (int)reader["ClaimStatus7"];
                        last.CountClaimStatus4 += (int)reader["ClaimStatus4"];
                        last.CountClaimStatus5 += (int)reader["ClaimStatus5"];
                        last.CountClaimStatus6 += (int)reader["ClaimStatus6"];

                        last.CountClaimAnnul0 += (int)reader["ClaimStatus5_1"];
                        last.CountClaimAnnul1 += (int)reader["ClaimStatus5_2"];
                        last.CountClaimAnnul2 += (int)reader["ClaimStatus5_3"];
                        last.CountClaimAnnul3 += (int)reader["ClaimStatus5_4"];
                    }
                    else
                    {
                        ExtendedClaimReport item = new ExtendedClaimReport();
                        if (typeReport != 0)
                        {
                            item.CityId = (int) reader["CityId"];
                            item.CityName = (string) reader["CityName"];
                        }
                        if (typeReport == 2)
                        {
                            item.UdodId = (long) reader["UdodId"];
                            item.UdodName = (string) reader["UdodName"];
                        }
                        if ((strUdodType != "-1" && !string.IsNullOrEmpty(strUdodType)) || (typeReport==2))
                            item.UdodTypeId = (int)reader["UdodTypeId"];
                        else item.UdodTypeId = -1;
                        item.CountUdod = 0;

                        item.CountDOPublic = (int)reader["CountIsPublic"];
                        item.CountDONotPublic = (int)reader["CountIsNotPublic"];
                        item.SumDO += item.CountDOPublic + item.CountDONotPublic;

                        item.CountClaimPGU = (int)reader["ClaimPGU"];
                        item.CountClaimUdod = (int)reader["ClaimNotPGU"];

                        item.SumClaim += item.CountClaimPGU + item.CountClaimUdod;

                        item.CountClaimStatus1 = (int)reader["ClaimStatus1"];
                        item.CountClaimStatus7 = (int)reader["ClaimStatus7"];
                        item.CountClaimStatus4 = (int)reader["ClaimStatus4"];
                        item.CountClaimStatus5 = (int)reader["ClaimStatus5"];
                        item.CountClaimStatus6 = (int)reader["ClaimStatus6"];

                        item.CountClaimAnnul0 = (int) reader["ClaimStatus5_1"];
                        item.CountClaimAnnul1 = (int) reader["ClaimStatus5_2"];
                        item.CountClaimAnnul2 = (int) reader["ClaimStatus5_3"];
                        item.CountClaimAnnul3 = (int) reader["ClaimStatus5_4"];

                        list.Add(item);
                    }
                }
            }
            return list;
        }

        public List<ExtendedAgeReport> GetUdodCountReport(int typeReport, string strUdodType, string strCityType, long? udodId)
        {
            List<ExtendedAgeReport> list = new List<ExtendedAgeReport>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetUdodCount", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("TypeReport", typeReport));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("udodId", udodId.Value));
                if (!string.IsNullOrEmpty(strUdodType)) comm.Parameters.Add(new SqlParameter("StrUdodType", strUdodType));
                if (!string.IsNullOrEmpty(strCityType)) comm.Parameters.Add(new SqlParameter("StrCityType", strCityType));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedAgeReport item = new ExtendedAgeReport();
                    item.CityId = (int)reader["CityId"];
                    item.CityName = (string)reader["CityName"];
                    item.UdodId = (long)reader["UdodId"];
                    item.UdodName = (string)reader["UdodName"];
                    item.UdodTypeId = (int)reader["UdodTypeId"];
                    item.CountUdod = (int)reader["CountUdods"];
                    item.CountUdodIsDo = (int)reader["CountUdodsIsDo"];
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedAgeReport> GetAgeReport(int typeReport, string strUdodType, string strCityType, long? udodId, DateTime? date)
        {
            List<ExtendedAgeReport> list = new List<ExtendedAgeReport>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetUniquePupils", Connection)
                                      {CommandType = CommandType.StoredProcedure};
                comm.Parameters.Add(new SqlParameter("TypeReport", typeReport));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("udodId", udodId.Value));
                if (date.HasValue) comm.Parameters.Add(new SqlParameter("Date", date.Value));
                if (!string.IsNullOrEmpty(strUdodType)) comm.Parameters.Add(new SqlParameter("StrUdodType", strUdodType));
                if (!string.IsNullOrEmpty(strCityType)) comm.Parameters.Add(new SqlParameter("StrCityType", strCityType));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    int _cityId=(int) reader["CityId"];
                    long _udodId=(long) reader["UdodId"];
                    int _udodTypeId = (int) reader["UdodTypeId"];

                    var last=list.FirstOrDefault(i=>i.CityId==_cityId && i.UdodId==_udodId && i.UdodTypeId==_udodTypeId);

                    if (last!=null)
                    {
                        if (reader["AgePupil"] != DBNull.Value)
                        {
                            int num = 3*((int) reader["AgePupil"] - 4);
                            if ((bool)reader["Sex"])
                            {
                                last.CountPupils[num+1] = (int) reader["CountPupils"];
                                last.CountPupils[49] += (int)reader["CountPupils"];
                            }
                            else
                            {
                                last.CountPupils[num] = (int)reader["CountPupils"];
                                last.CountPupils[48] += (int)reader["CountPupils"];
                            }
                            last.CountPupils[num + 2] += (int)reader["CountPupils"];
                            last.CountPupils[50] += (int) reader["CountPupils"];
                        }
                    }
                    else
                    {
                        ExtendedAgeReport item = new ExtendedAgeReport();
                        item.CityId = (int) reader["CityId"];
                        item.CityName = (string) reader["CityName"];
                        item.UdodId = (long) reader["UdodId"];
                        item.UdodName = (string) reader["UdodName"];
                        item.UdodTypeId = (int) reader["UdodTypeId"];
                        item.CountPupils = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                        if (reader["AgePupil"] != DBNull.Value)
                        {
                            int num = 3*((int) reader["AgePupil"] - 4);
                            if ((bool)reader["Sex"])
                            {
                                item.CountPupils[num+1] = (int) reader["CountPupils"];
                                item.CountPupils[49] = (int)reader["CountPupils"];
                            }
                            else
                            {
                                item.CountPupils[num] = (int)reader["CountPupils"];
                                item.CountPupils[48] = (int)reader["CountPupils"];
                            }
                            item.CountPupils[num + 2] += (int)reader["CountPupils"];
                            item.CountPupils[50] = (int) reader["CountPupils"];
                            item.CountUdod = 0;
                            item.CountUdodIsDo = 0;
                            list.Add(item);
                        }
                    }
                }
            }
            return list;
        }

        public List<ExtendedReport> GetChildUnionClaimCount(int? cityId, long? udodId, int type)
        {
            List<ExtendedReport> list = new List<ExtendedReport>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetChildUnionClaimCount", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("cityId", cityId.Value));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("udodId", udodId.Value));
                comm.Parameters.Add(new SqlParameter("topOrNo", type));
                SqlDataReader reader = comm.ExecuteReader();

                long lastChildUnionId = -1;
                while (reader.Read())
                {
                    long _childUnionId = (long)reader["ChildUnionId"];

                    if (lastChildUnionId != _childUnionId)
                    {
                        ExtendedReport item = new ExtendedReport();
                        //item.CityId = (int)reader["CityId"];
                        //item.CityName = (string)reader["CityName"];
                        //item.UdodId = (long)reader["UdodId"];
                        //item.UdodName = (string)reader["UdodName"];
                        //if (reader["UdodRayon"] != DBNull.Value) item.UdodRayon = (string)reader["UdodRayon"];
                        //else item.UdodRayon = "Район не указан";
                        //item.ChildUnionId = (long)reader["ChildUnionId"];
                        item.ChildUnionName = (string)reader["ChildUnionName"];
                        //item.ChildUnionSection = (string)reader["ChildUnionSection"];
                        //item.ChildUnionProfile = (string)reader["ChildUnionProfile"];
                        //item.ChildUnionProgram = (string)reader["ChildUnionProgram"];
                        item.ChildUnionClaimNum = (int)reader["ChildUnionClaimNum"];
                        lastChildUnionId = item.ChildUnionId;
                        list.Add(item);
                    }
                }
            }
            return list;
        }

        public List<ExtendedReport> GetCityClaimCount(int? cityId)
        {
            List<ExtendedReport> list = new List<ExtendedReport>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetCityClaimCount", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("cityId", cityId.Value));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedReport item = new ExtendedReport();
                    item.CityId = (int)reader["CityId"];
                    item.CityName = (string)reader["CityName"];
                    item.ChildUnionClaimNum = (int)reader["ChildUnionClaimNum"];
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedReport> GetRayonClaimCount(int? cityId)
        {
            List<ExtendedReport> list = new List<ExtendedReport>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetRayonClaimCount", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("cityId", cityId.Value));
                SqlDataReader reader = comm.ExecuteReader();

                //long lastUdodId = -1;
                while (reader.Read())
                {
                    //long _udodId = (long)reader["UdodId"];

                    //if (lastUdodId != _udodId)
                    //{
                        ExtendedReport item = new ExtendedReport();
                        item.CityId = (int)reader["CityId"];
                        item.CityName = (string)reader["CityName"];
                        //item.UdodId = (long)reader["UdodId"];
                        //item.UdodName = (string)reader["UdodName"];
                        if (reader["UdodRayon"] != DBNull.Value) item.UdodRayon = (string)reader["UdodRayon"];
                        else item.UdodRayon = "Район не указан";
                        item.ChildUnionClaimNum = (int)reader["ChildUnionClaimNum"];
                        //lastUdodId = item.UdodId;
                        list.Add(item);
                    //}
                }
            }
            return list;
        }

        public List<ExtendedReport> GetSectionClaimCount(int? cityId, long? udodId, int type)
        {
            List<ExtendedReport> list = new List<ExtendedReport>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetSectionClaimCount", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("cityId", cityId.Value));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("udodId", udodId.Value));
                comm.Parameters.Add(new SqlParameter("topOrNo", type));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedReport item = new ExtendedReport();
                    //item.CityId = (int)reader["CityId"];
                    //item.CityName = (string)reader["CityName"];
                    //item.UdodId = (long)reader["UdodId"];
                    //item.UdodName = (string)reader["UdodName"];
                    item.ChildUnionSection = (string)reader["ChildUnionSection"];
                    //item.ChildUnionProfile = (string)reader["ChildUnionProfile"];
                    //item.ChildUnionProgram = (string)reader["ChildUnionProgram"];
                    item.ChildUnionClaimNum = (int)reader["ChildUnionClaimNum"];
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedReport> GetProfileClaimCount(int? cityId, long? udodId, int type)
        {
            List<ExtendedReport> list = new List<ExtendedReport>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetProfileClaimCount", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("cityId", cityId.Value));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("udodId", udodId.Value));
                comm.Parameters.Add(new SqlParameter("topOrNo", type));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedReport item = new ExtendedReport();
                    //item.CityId = (int)reader["CityId"];
                    //item.CityName = (string)reader["CityName"];
                    //item.UdodId = (long)reader["UdodId"];
                    //item.UdodName = (string)reader["UdodName"];
                    //item.ChildUnionSection = (string)reader["ChildUnionSection"];
                    item.ChildUnionProfile = (string)reader["ChildUnionProfile"];
                    item.ChildUnionClaimNum = (int)reader["ChildUnionClaimNum"];
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedReport> GetProgramClaimCount(int? cityId, long? udodId, int type)
        {
            List<ExtendedReport> list = new List<ExtendedReport>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetProgramClaimCount", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("cityId", cityId.Value));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("udodId", udodId.Value));
                comm.Parameters.Add(new SqlParameter("topOrNo", type));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedReport item = new ExtendedReport();
                    //item.CityId = (int)reader["CityId"];
                    //item.CityName = (string)reader["CityName"];
                    //item.UdodId = (long)reader["UdodId"];
                    //item.UdodName = (string)reader["UdodName"];
                    //item.ChildUnionSection = (string)reader["ChildUnionSection"];
                    //item.ChildUnionProfile = (string)reader["ChildUnionProfile"];
                    item.ChildUnionProgram = (string)reader["ChildUnionProgram"];
                    item.ChildUnionClaimNum = (int)reader["ChildUnionClaimNum"];
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedReportClaim> GetReportClaimCount()
        {
            List<ExtendedReportClaim> list = new List<ExtendedReportClaim>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetStatReg", Connection) { CommandType = CommandType.StoredProcedure };
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedReportClaim item = new ExtendedReportClaim();
                    item.CityId = (int) reader["CityId"];
                    item.CityName = (string) reader["CityName"];
                    item.RayonId = (int)reader["RayonId"];
                    item.RayonName = (string)reader["RayonName"];
                    item.UdodId = (int)reader["UdodId"];
                    item.UdodName = (string)reader["Name"];
                    item.UdodShortName = (string)reader["ShortName"];
                    item.TypeMessage = (int) reader["TypeMessage"];
                    item.Count1 = (int)reader["Count_1"];
                    item.Count2 = (int)reader["Count_2"];
                    item.Count3 = (int)reader["Count_3"];
                    item.Count4 = (int)reader["Count_4"];
                    item.Count5 = (int)reader["Count_5"];
                    item.Count6 = (int)reader["Count_6"];
                    item.Count7 = (int)reader["Count_7"];
                    item.Count8 = (int)reader["Count_8"];
                    item.Count9 = (int)reader["Count_9"];
                    item.Count10 = (int)reader["Count_10"];
                    item.Count11 = (int)reader["Count_11"];
                    item.Count12 = (int)reader["Count_12"];
                    list.Add(item);
                }
            }
            /*var cities = list.Select(i => i.CityId).Distinct().ToList();
            foreach (int city in cities)
            {
                for (int k = 0; k < 3; k++)
                {
                    ExtendedReportClaim item = new ExtendedReportClaim();
                    item.CityId = city;
                    item.CityName = list.Where(i => i.CityId == city).FirstOrDefault().CityName;
                    item.TypeMessage = k;
                    item.Count1 = list.Where(i => i.CityId == city && i.TypeMessage == k).Sum(i => i.Count1);
                    item.Count2 = list.Where(i => i.CityId == city && i.TypeMessage == k).Sum(i => i.Count2);
                    item.Count3 = list.Where(i => i.CityId == city && i.TypeMessage == k).Sum(i => i.Count3);
                    item.Count4 = list.Where(i => i.CityId == city && i.TypeMessage == k).Sum(i => i.Count4);
                    item.Count5 = list.Where(i => i.CityId == city && i.TypeMessage == k).Sum(i => i.Count5);
                    item.Count6 = list.Where(i => i.CityId == city && i.TypeMessage == k).Sum(i => i.Count6);
                    item.Count7 = list.Where(i => i.CityId == city && i.TypeMessage == k).Sum(i => i.Count7);
                    item.Count8 = list.Where(i => i.CityId == city && i.TypeMessage == k).Sum(i => i.Count8);
                    item.Count9 = list.Where(i => i.CityId == city && i.TypeMessage == k).Sum(i => i.Count9);
                    item.Count10 = list.Where(i => i.CityId == city && i.TypeMessage == k).Sum(i => i.Count10);
                    item.Count11 = list.Where(i => i.CityId == city && i.TypeMessage == k).Sum(i => i.Count11);
                    item.Count12 = list.Where(i => i.CityId == city && i.TypeMessage == k).Sum(i => i.Count12);
                    list.Insert(0,item);
                }
            }*/

            return list;
        }

        public List<ExtendedReportDep> GetReportDep(int udodTypeId, int? transferStatus)
        {
            List<ExtendedReportDep> list = new List<ExtendedReportDep>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetReportForDep", Connection)
                                      {CommandType = CommandType.StoredProcedure};
                comm.Parameters.Add(new SqlParameter("UdodTypeId", udodTypeId));
                if(transferStatus.HasValue) comm.Parameters.Add(new SqlParameter("TransferStatus", transferStatus));
                SqlDataReader reader = comm.ExecuteReader();
                int i = 0;
                while (reader.Read())
                {
                    ExtendedReportDep item = new ExtendedReportDep();
                    item.Count = (int) reader[0];
                    switch (i)
                    {
                        case 0:
                            item.Parametr = "<b>Общее кол-во учреждений</b>";
                            break;
                        case 1:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;через МПГУ";
                            break;
                        case 2:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;через ОСИП";
                            break;
                        case 3:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;через учреждение";
                            break;
                        case 4: item.Parametr = "<b>Кол-во будущих воспитанников по заявлениям</b>";
                            break;
                        case 5:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Родитель/Законный представитель";
                            break;
                        case 6:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Ребенок, старше 14 лет";
                            break;
                        case 7:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;в 1 детское объед.";
                            break;
                        case 8:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;в 2 детских объед.";
                            break;
                        case 9:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;в 3 детских объед.";
                            break;
                        case 10:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;в 4 и более детских объед.";
                            break;
                        case 11:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;зарег. в г. Москва";
                            break;
                        case 12:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;зарег. в др. рег.";
                            break;
                        case 13:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;до 6 лет";
                            break;
                        case 14:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;6-10 лет";
                            break;
                        case 15:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;11-18 лет";
                            break;
                        case 16:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;старше 18 лет";
                            break;
                        case 17:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;бесплатно";
                            break;
                        case 18:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;платно";
                            break;
                        case 19:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;бесплатно";
                            break;
                        case 20:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;платно";
                            break;
                        case 21:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;1 год обучения";
                            break;
                        case 22:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;2 год обучения";
                            break;
                        case 23:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;3 год обучения";
                            break;
                        case 24:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;4 год обучения";
                            break;
                        case 25:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;5 год обучения";
                            break;
                        case 26:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Больше 5 года обучения";
                            break;
                        case 27:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;1 год обучения";
                            break;
                        case 28:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;2 год обучения";
                            break;
                        case 29:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;3 год обучения";
                            break;
                        case 30:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;4 год обучения";
                            break;
                        case 31:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;5 год обучения";
                            break;
                        case 32:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Больше 5 года обучения";
                            break;
                        case 33:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Естественнонаучная направленность";
                            break;
                        case 34:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Иное";
                            break;
                        case 35:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Социально-педагогическая направленность";
                            break;
                        case 36:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Техническая направленность";
                            break;
                        case 37:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Туристско-краеведческая направленность";
                            break;
                        case 38:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Физкультурно-спортивная направленность";
                            break;
                        case 39:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Художественная направленность";
                            break;
                        case 40:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Спортивно-техническая";
                            break;
                        case 41:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Туристско-краеведческая";
                            break;
                        case 42:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Физкультурно-спортивная";
                            break;
                        case 43:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Художественно-эстетическая";
                            break;
                        case 44:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;Эколого-биологическая";
                            break;
                        case 45:
                            item.Parametr = "&nbsp;&nbsp;&nbsp;в том числе имеющих блок ДО";
                            break;
                        default:
                        break;
                    }
                    list.Add(item);
                    i++;
                }
            }
            return list;
        }

        public List<ExtendedReportDepFromCity> GetReportDepFromCity(int udodTypeId, int? transferStatus)
        {
            List<ExtendedReportDepFromCity> list = new List<ExtendedReportDepFromCity>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_getreportForDepFromCity", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UdodTypeId", udodTypeId));
                if (transferStatus.HasValue) comm.Parameters.Add(new SqlParameter("TransferStatus", transferStatus));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedReportDepFromCity item = new ExtendedReportDepFromCity();
                    item.CityId = (int) reader[0];
                    item.NameCity = (string) reader[1];
                    item.Counts= new int[56];
                    item.Counts[0] = (int)reader[2];

                    item.Counts[1] = (int)reader[3] + (int)reader[4] + (int)reader[5];
                    item.Counts[2] = (int)reader[3];
                    item.Counts[3] = (int)reader[4];
                    item.Counts[4] = (int)reader[5];

                    item.Counts[5] = (int)reader[6];

                    item.Counts[6] = (int)reader[7] + (int)reader[8];
                    item.Counts[7] = (int)reader[7];
                    item.Counts[8] = (int)reader[8];

                    item.Counts[9] = (int)reader[9] + (int)reader[10] + (int)reader[11]+ (int)reader[12];
                    item.Counts[10] = (int)reader[9];
                    item.Counts[11] = (int)reader[10];
                    item.Counts[12] = (int)reader[11];
                    item.Counts[13] = (int)reader[12];

                    item.Counts[14] = (int)reader[13] + (int)reader[14] ;
                    item.Counts[15] = (int)reader[13];
                    item.Counts[16] = (int)reader[14];

                    item.Counts[17] = (int)reader[15] + (int)reader[16] + (int)reader[17] + (int)reader[18];
                    item.Counts[18] = (int)reader[15];
                    item.Counts[19] = (int)reader[16];
                    item.Counts[20] = (int)reader[17];
                    item.Counts[21] = (int)reader[18];

                    item.Counts[22] = (int)reader[19] + (int)reader[20];
                    item.Counts[23] = (int)reader[19];
                    item.Counts[24] = (int)reader[20];

                    item.Counts[25] = (int)reader[21] + (int)reader[22];
                    item.Counts[26] = (int)reader[21];
                    item.Counts[27] = (int)reader[22];

                    item.Counts[28] = (int)reader[23] + (int)reader[24] + (int)reader[25] + (int)reader[26] + (int)reader[27] + (int)reader[28];
                    item.Counts[29] = (int)reader[23];
                    item.Counts[30] = (int)reader[24];
                    item.Counts[31] = (int)reader[25];
                    item.Counts[32] = (int)reader[26];
                    item.Counts[33] = (int)reader[27];
                    item.Counts[34] = (int)reader[28];

                    item.Counts[35] = (int)reader[29] + (int)reader[30] + (int)reader[31] + (int)reader[32] + (int)reader[33] + (int)reader[34];
                    item.Counts[36] = (int)reader[29];
                    item.Counts[37] = (int)reader[30];
                    item.Counts[38] = (int)reader[31];
                    item.Counts[39] = (int)reader[32];
                    item.Counts[40] = (int)reader[33];
                    item.Counts[41] = (int)reader[34];

                    item.Counts[42] = (int)reader[35] + (int)reader[36] + (int)reader[37] + (int)reader[38] + (int)reader[39] + (int)reader[40] + (int)reader[41] + (int)reader[42] + (int)reader[43] + (int)reader[44] + (int)reader[45] + (int)reader[46];
                    item.Counts[43] = (int)reader[35];
                    item.Counts[44] = (int)reader[36];
                    item.Counts[45] = (int)reader[37];
                    item.Counts[46] = (int)reader[38];
                    item.Counts[47] = (int)reader[39];
                    item.Counts[48] = (int)reader[40];
                    item.Counts[49] = (int)reader[41];
                    item.Counts[50] = (int)reader[42];
                    item.Counts[51] = (int)reader[43];
                    item.Counts[52] = (int)reader[44];
                    item.Counts[53] = (int)reader[45];
                    item.Counts[54] = (int)reader[46];

                    item.Counts[55] = (int)reader[47];

                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedReportDepFromCity> GetReportDepFromUdod(int udodTypeId, int? transferStatus)
        {
            List<ExtendedReportDepFromCity> list = new List<ExtendedReportDepFromCity>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_getreportForDepFromUdod", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UdodTypeId", udodTypeId));
                if (transferStatus.HasValue) comm.Parameters.Add(new SqlParameter("TransferStatus", transferStatus));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedReportDepFromCity item = new ExtendedReportDepFromCity();
                    item.CityId = (int)reader[0];
                    item.NameCity = (string)reader[1];
                    item.NameUdod = (string)reader[2];
                    item.Counts = new int[46];
                    item.Counts[0] = (int)reader[3];
                    item.Counts[1] = (int)reader[4];
                    item.Counts[2] = (int)reader[5];
                    item.Counts[3] = (int)reader[6];
                    item.Counts[4] = (int)reader[7];
                    item.Counts[5] = (int)reader[8];
                    item.Counts[6] = (int)reader[5] + (int)reader[6] + (int)reader[7] + (int)reader[8];
                    
                    item.Counts[7] = (int)reader[9];
                    item.Counts[8] = (int)reader[10];
                    item.Counts[9] = (int)reader[9] + (int)reader[10];
                    
                    item.Counts[10] = (int)reader[11];
                    item.Counts[11] = (int)reader[12];
                    item.Counts[12] = (int)reader[13];
                    item.Counts[13] = (int)reader[14];
                    item.Counts[14] = (int)reader[11] + (int)reader[12] + (int)reader[13] + (int)reader[14];
                    
                    item.Counts[15] = (int)reader[15];
                    item.Counts[16] = (int)reader[16];

                    item.Counts[17] = (int)reader[17];
                    item.Counts[18] = (int)reader[18];

                    item.Counts[19] = (int)reader[19];
                    item.Counts[20] = (int)reader[20];
                    item.Counts[21] = (int)reader[21];
                    item.Counts[22] = (int)reader[22];
                    item.Counts[23] = (int)reader[23];
                    item.Counts[24] = (int)reader[24];
                    item.Counts[25] = (int)reader[24] + (int)reader[23] + (int)reader[22] + (int)reader[21] + (int)reader[20] + (int)reader[19];

                    item.Counts[26] = (int)reader[25];
                    item.Counts[27] = (int)reader[26];
                    item.Counts[28] = (int)reader[27];
                    item.Counts[29] = (int)reader[28];
                    item.Counts[30] = (int)reader[29];
                    item.Counts[31] = (int)reader[30];
                    item.Counts[32] = (int)reader[25] + (int)reader[26] + (int)reader[27] + (int)reader[28] + (int)reader[29] + (int)reader[30];

                    item.Counts[33] = (int)reader[31];
                    item.Counts[34] = (int)reader[32];
                    item.Counts[35] = (int)reader[33];
                    item.Counts[36] = (int)reader[34];
                    item.Counts[37] = (int)reader[35];
                    item.Counts[38] = (int)reader[36];
                    item.Counts[39] = (int)reader[37];
                    item.Counts[40] = (int)reader[38];
                    item.Counts[41] = (int)reader[39];
                    item.Counts[42] = (int)reader[40];
                    item.Counts[43] = (int)reader[41];
                    item.Counts[44] = (int)reader[42];
                    item.Counts[45] = (int)reader[31] + (int)reader[32] + (int)reader[33] + (int)reader[34] + (int)reader[35] + (int)reader[36] + (int)reader[37] + (int)reader[38] + (int)reader[39] + (int)reader[40] + (int)reader[41] + (int)reader[42];


                    item.IsDOEnabled = (bool)reader[43] ? "Да" : "Нет";

                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedReportDepFromCity> GetReportForOsip(int? cityId, int? udodTypeId, long? udodId)
        {
            List<ExtendedReportDepFromCity> list = new List<ExtendedReportDepFromCity>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_CountPupilForOsip", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodTypeId.HasValue) comm.Parameters.Add(new SqlParameter("UdodTypeId", udodTypeId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long? _udodId = (long)reader["UdodId"];
                    int? _numYear = (int)reader["NumYear"];

                    ExtendedReportDepFromCity last = list.LastOrDefault(i => i.UdodId == _udodId.Value);

                    if (last.UdodId!=0)
                    {
                        ExtendedReportYearEnroll last_year = last.Years.LastOrDefault(i => i.Year == _numYear.Value);

                        if (last_year.Year != 0)
                        {
                            last.Years.Remove(last_year);

                            if ((int)reader["BudgetType"] == 1) last_year.FeeEnrollCount = (int)reader["CountPupils"];
                            else last_year.FreeEnrollCount = (int)reader["CountPupils"];

                            last.Years.Insert(last_year.Year - 1, last_year);
                        }
                        else
                        {
                            if (_numYear.HasValue)
                            {
                                ExtendedReportYearEnroll year = new ExtendedReportYearEnroll();
                                year.Year = (int)reader["NumYear"];
                                if ((int)reader["BudgetType"] == 1) year.FeeEnrollCount = (int)reader["CountPupils"];
                                else year.FreeEnrollCount = (int)reader["CountPupils"];

                                (last.Years as List<ExtendedReportYearEnroll>).Add(year);
                            }
                        }
                    }
                    else
                    {
                        ExtendedReportDepFromCity item = new ExtendedReportDepFromCity();
                        item.UdodId = _udodId.Value;
                        item.NameUdod = (string) reader["ShortName"];
                        item.IsDOEnabled = ((bool)reader["IsDOEnabled"]) ? "Да" : "Нет";
                        item.Years = new List<ExtendedReportYearEnroll>();
                        for (int i = 1; i <= 11; i++)
                        {
                            ExtendedReportYearEnroll year = new ExtendedReportYearEnroll();
                            year.Year = i;
                            (item.Years as List<ExtendedReportYearEnroll>).Add(year);
                        }

                        if (_numYear.HasValue)
                        {
                            ExtendedReportYearEnroll last_year = item.Years.LastOrDefault(i => i.Year == _numYear.Value);

                            item.Years.Remove(last_year);

                            if ((int)reader["BudgetType"] == 1) last_year.FeeEnrollCount = (int)reader["CountPupils"];
                            else last_year.FreeEnrollCount = (int)reader["CountPupils"];

                            item.Years.Insert(last_year.Year-1, last_year);
                        }
                        list.Add(item);
                    }
                }
            }
            return list;
        }

        public List<ExtendedReportLesson> GetReportLesson(int? cityId, int? LLfrom, int? LLto, int? LIWfrom, int? LIWto, long? udodId)
        {
            List<ExtendedReportLesson> list = new List<ExtendedReportLesson>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetLessons", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (LLfrom.HasValue) comm.Parameters.Add(new SqlParameter("LLfrom", LLfrom));
                if (LLto.HasValue) comm.Parameters.Add(new SqlParameter("LLto", LLto));
                if (LIWfrom.HasValue) comm.Parameters.Add(new SqlParameter("LIWfrom", LIWfrom));
                if (LIWto.HasValue) comm.Parameters.Add(new SqlParameter("LIWto", LIWto));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedReportLesson item = new ExtendedReportLesson();
                    item.CityId = (int)reader["CityId"];
                    item.CityName = (string)reader["CityName"];
                    item.UdodId = (long)reader["UdodId"];
                    item.UdodName = (string)reader["UdodName"];
                    item.ChildUnionName = (string)reader["ChildUnionName"];
                    item.LessonLength = (int)reader["LessonLength"];
                    item.LessonsInWeek = (int)reader["LessonsInWeek"];

                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedReportTransfer> GetReportTransfer(int? cityId)
        {
            List<ExtendedReportTransfer> list = new List<ExtendedReportTransfer>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetTransfer", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long? _udodId = (long)reader["UdodId"];

                    ExtendedReportTransfer last = list.LastOrDefault(i => i.UdodId == _udodId.Value);

                    if (last!=null)
                    {
                        switch ((int)reader["TransferStatusId"])
                        {
                            case 1:
                                last.EndOfStudy = (int)reader["AgeGroupPupilCount"];
                                break;
                            case 2:
                                last.WillBeTransfer = (int)reader["AgeGroupPupilCount"];
                                break;
                            case 3:
                                last.WillBeDismissed = (int)reader["AgeGroupPupilCount"];
                                break;
                            case 4:
                                last.AgeGroupNotExist = (int)reader["AgeGroupPupilCount"];
                                break;
                            case 5:
                                last.TooMuchHours = (int)reader["AgeGroupPupilCount"];
                                break;
                            case 6:
                                last.Repear = (int)reader["AgeGroupPupilCount"];
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        ExtendedReportTransfer item = new ExtendedReportTransfer();
                        item.UdodId = (long)reader["UdodId"];
                        item.ShortName = (string)reader["ShortName"];
                        item.CityName = (string)reader["CityName"];
                        item.IsDOEnabled = (bool)reader["IsDOEnabled"] ? "Да" : "Нет";
                        item.UdodNumber = (string)reader["UdodNumber"];
                        item.EndOfStudy = item.WillBeTransfer = item.WillBeDismissed = item.AgeGroupNotExist = item.TooMuchHours = item.Repear = 0;

                        switch ((int)reader["TransferStatusId"])
                        {
                            case 1:
                                item.EndOfStudy = (int)reader["AgeGroupPupilCount"];
                                break;
                            case 2:
                                item.WillBeTransfer = (int)reader["AgeGroupPupilCount"];
                                break;
                            case 3:
                                item.WillBeDismissed = (int)reader["AgeGroupPupilCount"];
                                break;
                            case 4:
                                item.AgeGroupNotExist = (int)reader["AgeGroupPupilCount"];
                                break;
                            case 5:
                                item.TooMuchHours = (int)reader["AgeGroupPupilCount"];
                                break;
                            case 6:
                                item.Repear = (int)reader["AgeGroupPupilCount"];
                                break;
                            default:
                                break;
                        }

                        list.Add(item);
                    }
                }
            }
            return list;
        }

        public List<ExtendedReportUdodProgram> GetReportUdodProgram(int? cityId)
        {
            List<ExtendedReportUdodProgram> list = new List<ExtendedReportUdodProgram>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_GetUdodProgram", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedReportUdodProgram item = new ExtendedReportUdodProgram();
                    item.UdodId = (long)reader["UdodId"];
                    item.UdodNumber = (string)reader["UdodNumber"];
                    item.UdodName = (string)reader["ShortName"];
                    item.ProgramName = (string)reader["ProgramName"];
                    item.CityName = (string)reader["CityName"];
                    item.CountChildUnion = (int)reader["CountChildUnion"];
                    item.ProgramId = (int)reader["ProgramId"];
                    item.isDO = (bool)reader["IsDOEnabled"];
                    item.isGos = (bool)reader["IsInvalid"];
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedMegaReport> GetMegaReport()
        {
            List<ExtendedMegaReport> list = new List<ExtendedMegaReport>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Report_MegaReport", Connection) { CommandType = CommandType.StoredProcedure };
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedMegaReport item = new ExtendedMegaReport();
                    
                    item.CityName = (string)reader["CityName"];
                    item.ParentName = (string)reader["ParentName"];
                    item.UdodName = (string)reader["UdodName"];
                    item.all_pupils = (int)reader["all_pupils"];
                    item.count_not_ou = (int)reader["count_not_ou"];
                    item.count_pupils = (int)reader["count_pupils"];
                    item.del_pupil = (int)reader["del_pupil"];
                    item.del_zach = (int)reader["del_zach"];
                    item.isPublic = (int)reader["isPublic"];
                    item.zach_pupil = (int)reader["zach_pupil"];
                    item.zach_zach = (int)reader["zach_zach"];
                    item.zach_zach = (int)reader["zach_zach"];
                    item.IsRec = (int)reader["IsRec"];
                    item.CountDO = (int)reader["Count_do"];
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedMegaReport> GetMegaReport2()
        {
            List<ExtendedMegaReport> list = new List<ExtendedMegaReport>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("ReportMegaReport2", Connection) { CommandType = CommandType.StoredProcedure };
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedMegaReport item = new ExtendedMegaReport();

                    item.CityName = (string)reader["CityName"];
                    item.ParentName = (string)reader["ParentName"];
                    item.UdodName = (string)reader["UdodName"];
                    item.all_pupils = (int)reader["CountPupils"];
                    list.Add(item);
                }
            }
            return list;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Udod.Dal
{
    public class CommonDb :BaseDb
    {
        public List<ExtendedTmp> GetSchoolsTmp()
        {
            List<ExtendedTmp> list = new List<ExtendedTmp>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("SELECT        userid, ReestrGuid FROM            tmpreestr3", conn) { CommandType = CommandType.Text };
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedTmp item = new ExtendedTmp()
                    {
                        SchoolGuid = (Guid)reader["ReestrGuid"],
                        UserId = (Guid)reader["userid"]
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        public void UpdateSchoolsTmp(Guid guidReestr, int ekisCode)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("UPDATE       tmpreeestr1 SET                ekis = @EkisId WHERE        (guidreeestr = @guidReestr1)", conn) { CommandType = CommandType.Text };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("guidReestr1", guidReestr));
                comm.Parameters.Add(new SqlParameter("EkisId", ekisCode));
                comm.ExecuteNonQuery();
            }
            
        }

        public List<ExtendedHistoryEvent> GetHistoryEvents()
        {
            List<ExtendedHistoryEvent> list = new List<ExtendedHistoryEvent>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetHistoryEvents", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedHistoryEvent item = new ExtendedHistoryEvent()
                    {
                        SecondEkisId = (int)reader["SecondEkisId"],
                        FirstEkisId = (int)reader["FirstEkisId"],
                        TypeEvents = (string)reader["TypeEvent"],
                        Number = (int)reader["Number"]
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedPassportType> GetPasportType()
        {
            List<ExtendedPassportType> list = new List<ExtendedPassportType>();

            using (SqlConnection conn= new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Dict_GetPassportTypes", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedPassportType item = new ExtendedPassportType()
                                                    {
                                                        PassportTypeId = (int) reader["PassportTypeId"],
                                                        PassportTypeName = (string) reader["Name"]
                                                    };
                    list.Add(item);
                }
            }
            return list;
        }

        public List<DateTime> GetTimerUpdateDatetimes()
        {
            List<DateTime> list = new List<DateTime>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("SELECT StartUpdateDatetime, EndUpdateDatetime FROM Const_TimerDatetime", conn) { CommandType = CommandType.Text };
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    list.Add((DateTime)reader["StartUpdateDatetime"]);
                    list.Add((DateTime)reader["EndUpdateDatetime"]);
                }
            }
            return list;
        }

        public void UpdateTimerDatetimes(DateTime startTime, DateTime endTime)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("UPDATE Const_TimerDatetime SET StartUpdateDatetime = @StartTime, EndUpdateDatetime = @EndTime", conn) { CommandType = CommandType.Text };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("StartTime", startTime));
                comm.Parameters.Add(new SqlParameter("EndTime", endTime));
                comm.ExecuteNonQuery();
            }
        }

        public List<ExtendedTypeBudget> GetReasonAnnuls(long claimId)
        {
            List<ExtendedTypeBudget> list = new List<ExtendedTypeBudget>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Dict_GetReasonAnnul", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ClaimId", claimId));
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedTypeBudget item = new ExtendedTypeBudget()
                                                  {
                                                      DictTypeBudgetId = (int)reader["DictReasonAnnulId"],
                                                      TypeBudgetName = (string)reader["Name"]
                                                  };

                    list.Add(item);
                }
            }

            return list;
        }
    }
}

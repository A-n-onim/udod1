﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Udod.Dal
{
    public class ESZDb :BaseDb
    {
        public List<ExtendedESZDict> GetForms()
        {
            List<ExtendedESZDict> list = new List<ExtendedESZDict>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("ESZ_GetForms", Connection) { CommandType = CommandType.StoredProcedure };
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedESZDict item = new ExtendedESZDict();
                    item.DictId = (int)reader["ESZFormId"];
                    item.Name = (string)reader["Name"];
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedESZDict> GetSexs()
        {
            List<ExtendedESZDict> list = new List<ExtendedESZDict>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("ESZ_GetSexs", Connection) { CommandType = CommandType.StoredProcedure };
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedESZDict item = new ExtendedESZDict();
                    item.DictId = (int)reader["ESZSexId"];
                    item.Name = (string)reader["Name"];
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedESZDict> GetStatuses()
        {
            List<ExtendedESZDict> list = new List<ExtendedESZDict>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("ESZ_GetStatuses", Connection) { CommandType = CommandType.StoredProcedure };
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedESZDict item = new ExtendedESZDict();
                    item.DictId = (int)reader["ESZStatusId"];
                    item.Name = (string)reader["Name"];
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedESZDict> GetTypeFinances()
        {
            List<ExtendedESZDict> list = new List<ExtendedESZDict>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("ESZ_GetTypeFinances", Connection) { CommandType = CommandType.StoredProcedure };
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedESZDict item = new ExtendedESZDict();
                    item.DictId = (int)reader["ESZTypeFinanceId"];
                    item.Name = (string)reader["Name"];
                    list.Add(item);
                }
            }
            return list;
        }
        public List<ExtendedESZDict> GetTypeValueService()
        {
            List<ExtendedESZDict> list = new List<ExtendedESZDict>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("ESZ_GetTypeValueService", Connection) { CommandType = CommandType.StoredProcedure };
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedESZDict item = new ExtendedESZDict();
                    item.DictId = (int)reader["ESZTypeValueServiceId"];
                    item.Name = (string)reader["Name"];
                    list.Add(item);
                }
            }
            return list;
        }
        public List<ExtendedESZDict> GetTimeDays()
        {
            List<ExtendedESZDict> list = new List<ExtendedESZDict>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("ESZ_GetTimeDay", Connection) { CommandType = CommandType.StoredProcedure };
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedESZDict item = new ExtendedESZDict();
                    item.DictId = (int)reader["ESZTimeDayId"];
                    item.Name = (string)reader["Name"];
                    list.Add(item);
                }
            }
            return list;
        }
    }
}

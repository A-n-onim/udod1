﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.DataSetExtensions;
using System.Configuration;

namespace Udod.Dal
{
    public class SectionDb : BaseDb
    {
        //private int SectionsCount;

        public SectionDb(){}

        /*public List<ExtendedUdodSection> GetSections(long udodId, int? sectionId)
        {
            List<ExtendedUdodSection> list = new List<ExtendedUdodSection>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetSection", Connection){CommandType = CommandType.StoredProcedure};
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while(reader.Read())
                {
                    long _sectionId = (long)reader["UdodSectionId"];
                    long? _ageGroupId=null;
                    if (reader["UdodAgeGroupId"] != DBNull.Value) _ageGroupId = (long?)reader["UdodAgeGroupId"];
                    
                    long? _addressId=null;
                    if (reader["AddressId"]!=DBNull.Value) _addressId = (long?)reader["AddressId"];

                    ExtendedUdodSection last = list.LastOrDefault(i => i.UdodSectionId == _sectionId);

                    if (null != last)
                    {
                        if (_ageGroupId.HasValue && last.AgeGroups.Count(i => i.UdodAgeGroupId == _ageGroupId) == 0)
                        {
                            var itemAgeGroup = new ExtendedAgeGroup();
                            itemAgeGroup.AgeEnd = (int)reader["AgeEnd"];
                            itemAgeGroup.AgeStart = (int)reader["AgeStart"];
                            itemAgeGroup.Section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                                SectionId = (int)reader["SectionId"],
                                ProfileId = (int)reader["ProfileId"]
                            };
                            itemAgeGroup.TypeBudget = new ExtendedTypeBudget()
                            {
                                DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                                TypeBudgetName = (string)reader["TypeBudgetName"]
                            };
                            itemAgeGroup.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                            itemAgeGroup.UdodId = udodId;
                            itemAgeGroup.NumYears = (int)reader["NumYears"];
                            itemAgeGroup.EducationType = new ExtendedEducationType()
                            {
                                EducationTypeId = (int)reader["EducationTypeId"],
                                EducationTypeName = (string)reader["EducationTypeName"]
                            };
                            itemAgeGroup.LessonLength = (int)reader["LessonLength"];
                            itemAgeGroup.BreakLength = (int)reader["BreakLength"];
                            itemAgeGroup.LessonsInWeek = (int)reader["LessonsInWeek"];
                            itemAgeGroup.Program = new ExtendedProgram()
                            {
                                ProgramId = (int)reader["ProgramId"],
                                Name = (string)reader["ProgramName"]
                            };
                            itemAgeGroup.TeacherFio = (string)reader["Fio"];

                            (last.AgeGroups as List<ExtendedAgeGroup>).Add(itemAgeGroup);
                        }

                        if (_addressId.HasValue && last.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];

                            (last.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }
                    }
                    else
                    {
                        ExtendedUdodSection item = new ExtendedUdodSection();
                        item.UdodSectionId = (long)reader["UdodSectionId"];
                        item.Name = (string)reader["Name"];
                        item.Section = new ExtendedSection()
                        {
                            SectionId = (int)reader["SectionId"],
                            Name = (string)reader["SectionName"]
                        };
                        item.Profile = new ExtendedProfile()
                        {
                            ProgramId = (int)reader["ProfileId"],
                            Name = (string)reader["ProfileName"]
                        };
                        item.Program = new ExtendedProgram()
                        {
                            ProgramId = (int)reader["ProgramId"],
                            Name = (string)reader["ProgramName"]
                        };
                        item.UdodId = udodId;
                        item.Comment = (string)reader["Comment"];

                        item.AgeGroups = new List<ExtendedAgeGroup>();
                        item.Addresses = new List<ExtendedAddress>();
                        if (_ageGroupId.HasValue)
                        {
                            var itemAgeGroup = new ExtendedAgeGroup();
                            itemAgeGroup.AgeEnd = (int)reader["AgeEnd"];
                            itemAgeGroup.AgeStart = (int)reader["AgeStart"];
                            itemAgeGroup.Section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                                SectionId = (int)reader["SectionId"],
                                ProfileId = (int)reader["ProfileId"]
                            };
                            itemAgeGroup.TypeBudget = new ExtendedTypeBudget()
                            {
                                DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                                TypeBudgetName = (string)reader["TypeBudgetName"]
                            };
                            itemAgeGroup.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                            itemAgeGroup.UdodId = udodId;
                            itemAgeGroup.NumYears = (int)reader["NumYears"];
                            itemAgeGroup.EducationType = new ExtendedEducationType()
                            {
                                EducationTypeId = (int)reader["EducationTypeId"],
                                EducationTypeName = (string)reader["EducationTypeName"]
                            };
                            itemAgeGroup.LessonLength = (int)reader["LessonLength"];
                            itemAgeGroup.BreakLength = (int)reader["BreakLength"];
                            itemAgeGroup.LessonsInWeek = (int)reader["LessonsInWeek"];
                            itemAgeGroup.Program = new ExtendedProgram()
                            {
                                ProgramId = (int)reader["ProgramId"],
                                Name = (string)reader["ProgramName"]
                            };
                            itemAgeGroup.TeacherFio = (string)reader["Fio"];

                            (item.AgeGroups as List<ExtendedAgeGroup>).Add(itemAgeGroup);
                        }

                        if (_addressId.HasValue)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];

                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        list.Add(item);
                    }
                }
            }

            SectionsCount = list.Count;
            return list;
        }

        public void UpdateUdodSection(ExtendedUdodSection eus)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateUdodSection", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Name", eus.Name));
                comm.Parameters.Add(new SqlParameter("Comment", eus.Comment));
                comm.Parameters.Add(new SqlParameter("sectionId", eus.Section.SectionId));
                comm.Parameters.Add(new SqlParameter("UdodSectionId", eus.UdodSectionId));
                comm.ExecuteNonQuery();

                foreach (int id in eus.AddressesCheckedIds)
                {
                    SqlCommand comm2 = new SqlCommand("UpdateUdodSectionAddress", Connection) { CommandType = CommandType.StoredProcedure };
                    comm2.Parameters.Add(new SqlParameter("AddressId", id));
                    comm2.Parameters.Add(new SqlParameter("SectionId", eus.UdodSectionId));
                    comm2.Parameters.Add(new SqlParameter("checked", true));
                    comm2.ExecuteNonQuery();
                }

                foreach (int id in eus.AddressesUnCheckedIds)
                {
                    SqlCommand comm3 = new SqlCommand("UpdateUdodSectionAddress", Connection) { CommandType = CommandType.StoredProcedure };
                    comm3.Parameters.Add(new SqlParameter("AddressId", id));
                    comm3.Parameters.Add(new SqlParameter("SectionId", eus.UdodSectionId));
                    comm3.Parameters.Add(new SqlParameter("checked", false));
                    comm3.ExecuteNonQuery();
                }
            }
        }

        public ExtendedUdodSection GetSection(int sectionId)
        {
            ExtendedUdodSection item=null;

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetSection", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long? _ageGroupId = null;
                    if (reader["UdodAgeGroupId"] != DBNull.Value) _ageGroupId = (long?)reader["UdodAgeGroupId"];

                    long? _addressId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long?)reader["AddressId"];

                    if (null != item)
                    {
                        if (_ageGroupId.HasValue && item.AgeGroups.Count(i => i.UdodAgeGroupId == _ageGroupId) == 0)
                        {
                            var itemAgeGroup = new ExtendedAgeGroup();
                            itemAgeGroup.AgeEnd = (int)reader["AgeEnd"];
                            itemAgeGroup.AgeStart = (int)reader["AgeStart"];
                            itemAgeGroup.Section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                                SectionId = (int)reader["SectionId"],
                                ProfileId = (int)reader["ProfileId"]
                            };
                            itemAgeGroup.TypeBudget = new ExtendedTypeBudget()
                            {
                                DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                                TypeBudgetName = (string)reader["TypeBudgetName"]
                            };
                            itemAgeGroup.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                            itemAgeGroup.UdodId = (long)reader["UdodId"];
                            itemAgeGroup.NumYears = (int)reader["NumYears"];
                            itemAgeGroup.EducationType = new ExtendedEducationType()
                            {
                                EducationTypeId = (int)reader["EducationTypeId"],
                                EducationTypeName = (string)reader["EducationTypeName"]
                            };
                            itemAgeGroup.LessonLength = (int)reader["LessonLength"];
                            itemAgeGroup.BreakLength = (int)reader["BreakLength"];
                            itemAgeGroup.LessonsInWeek = (int)reader["LessonsInWeek"];
                            itemAgeGroup.Program = new ExtendedProgram()
                            {
                                ProgramId = (int)reader["ProgramId"],
                                Name = (string)reader["ProgramName"]
                            };
                            itemAgeGroup.TeacherFio = (string)reader["Fio"];

                            (item.AgeGroups as List<ExtendedAgeGroup>).Add(itemAgeGroup);
                        }

                        if (_addressId.HasValue && item.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];

                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }
                    }
                    else
                    {
                        item = new ExtendedUdodSection();
                        item.UdodSectionId = (long)reader["UdodSectionId"];
                        item.Name = (string)reader["Name"];
                        item.Section = new ExtendedSection()
                        {
                            SectionId = (int)reader["SectionId"],
                            Name = (string)reader["SectionName"]
                        };
                        item.Profile = new ExtendedProfile()
                        {
                            ProgramId = (int)reader["ProfileId"],
                            Name = (string)reader["ProfileName"]
                        };
                        item.Program = new ExtendedProgram()
                        {
                            ProgramId = (int)reader["ProgramId"],
                            Name = (string)reader["ProgramName"]
                        };
                        item.UdodId = (long)reader["UdodId"];
                        item.Comment = (string)reader["Comment"];

                        item.AgeGroups = new List<ExtendedAgeGroup>();
                        item.Addresses = new List<ExtendedAddress>();

                        if (_ageGroupId.HasValue)
                        {
                            var itemAgeGroup = new ExtendedAgeGroup();
                            itemAgeGroup.AgeEnd = (int)reader["AgeEnd"];
                            itemAgeGroup.AgeStart = (int)reader["AgeStart"];
                            itemAgeGroup.Section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                                SectionId = (int)reader["SectionId"],
                                ProfileId = (int)reader["ProfileId"]
                            };
                            itemAgeGroup.TypeBudget = new ExtendedTypeBudget()
                            {
                                DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                                TypeBudgetName = (string)reader["TypeBudgetName"]
                            };
                            itemAgeGroup.LessonLength = (int)reader["LessonLength"];
                            itemAgeGroup.BreakLength = (int)reader["BreakLength"];
                            itemAgeGroup.LessonsInWeek = (int)reader["LessonsInWeek"];
                            itemAgeGroup.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                            itemAgeGroup.UdodId = (long)reader["UdodId"];
                            itemAgeGroup.NumYears = (int)reader["NumYears"];
                            itemAgeGroup.EducationType = new ExtendedEducationType()
                            {
                                EducationTypeId = (int)reader["EducationTypeId"],
                                EducationTypeName = (string)reader["EducationTypeName"]
                            };
                            itemAgeGroup.Program = new ExtendedProgram()
                            {
                                ProgramId = (int)reader["ProgramId"],
                                Name = (string)reader["ProgramName"]
                            };
                            itemAgeGroup.TeacherFio = (string)reader["Fio"];

                            (item.AgeGroups as List<ExtendedAgeGroup>).Add(itemAgeGroup);
                        }

                        if (_addressId.HasValue)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];

                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }
                    }
                }
            }
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm2 = new SqlCommand("GetUdodSectionAddress", Connection) { CommandType = CommandType.StoredProcedure };
                comm2.Parameters.Add(new SqlParameter("SectionId", sectionId));
                Connection.Open();
                SqlDataReader reader2 = comm2.ExecuteReader();
                item.AddressesCheckedIds = new List<long>();
                while (reader2.Read())
                {
                    (item.AddressesCheckedIds as List<long>).Add((long)reader2["AddressId"]);
                }
            }
            return item;
        }*/

        public List<ExtendedUdodSection> GetUdodSections(long udodId)
        {
            List<ExtendedUdodSection> list = new List<ExtendedUdodSection>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("GetUdodSections" , Connection) {CommandType = CommandType.StoredProcedure};
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedUdodSection item = new ExtendedUdodSection();
                    item.UdodId = (long)reader["UdodId"];
                    item.Section = new ExtendedSection
                    {
                        SectionId = (int)reader["SectionId"],
                        Name = (string)reader["Name"]
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        public ExtendedUdodSection GetUdodSection(long udodId, int sectionId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("GetUdodSection", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedUdodSection item = new ExtendedUdodSection();
                    item.UdodSectionId = (long)reader["UdodSectionId"];
                    return item;
                }
            }
            return null;
        }

        public void UpdateUdodSection(long udodId, int sectionId, bool isChecked)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateUdodSection", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("udodId", udodId));
                comm.Parameters.Add(new SqlParameter("sectionId", sectionId));
                comm.Parameters.Add(new SqlParameter("isDeleted", !isChecked));
                comm.ExecuteNonQuery();
            }
        }
    }
}

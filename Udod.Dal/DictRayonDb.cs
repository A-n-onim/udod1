﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.DataSetExtensions;
using System.Configuration;

namespace Udod.Dal
{
    public class DictRayonDb : BaseDb
    {
        private int RayonsCount;

        public DictRayonDb(){}

        public List<ExtendedRayon> GetRayons(string city)
        {
            List<ExtendedRayon> list = new List<ExtendedRayon>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Dict_GetRayon", Connection) { CommandType = CommandType.StoredProcedure };
                if (!string.IsNullOrEmpty(city)) comm.Parameters.Add(new SqlParameter("City", city));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedRayon item = new ExtendedRayon();
                    //item.RayonId = (int)reader["DictRayonId"];
                    item.Name = (string)reader["RayonName"];
                    //item.CityId = (int)reader["CityId"];
                    list.Add(item);
                }
            }

            RayonsCount = list.Count;
            return list;
        }

        public List<ExtendedRayon> GetRayons(int? cityId)
        {
            List<ExtendedRayon> list = new List<ExtendedRayon>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Dict_GetRayon", Connection){CommandType = CommandType.StoredProcedure};
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId.Value));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while(reader.Read())
                {
                    ExtendedRayon item = new ExtendedRayon();
                    item.RayonId = (int)reader["DictRayonId"];
                    item.Name = (string) reader["RayonName"];
                    item.CityId = (int)reader["CityId"];
                    list.Add(item);
                }
            }

            RayonsCount = list.Count;
            return list;
        }

        public ExtendedRayon? GetRayon(int RayonId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetRayon", Connection){CommandType = CommandType.StoredProcedure};
                comm.Parameters.Add(new SqlParameter("RayonId", RayonId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedRayon item = new ExtendedRayon();
                    item.RayonId = RayonId;
                    item.Name = (string)reader["RayonName"];
                    item.CityId = (int)reader["CityId"];
                    return item;
                }
            }
            return null;
        }

        public ExtendedRayon? GetRayon(string codeRayon)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetRayon", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("CodeRayon", codeRayon));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedRayon item = new ExtendedRayon();
                    item.RayonId = (int)reader["DictRayonId"]; ;
                    item.Name = (string)reader["RayonName"];
                    item.CityId = (int)reader["CityId"];
                    return item;
                }
            }
            return null;
        }

        public int GetRayonsCount()
        {
            return RayonsCount;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Udod.Dal
{
    public class MessageDb : BaseDb
    {
        private int MessagesCount;

        public MessageDb() {}

        public List<ExtendedPublicMessage> GetUserMessage(Guid userId, bool viewAll)
        {
            List<ExtendedPublicMessage> list = new List<ExtendedPublicMessage>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Message_GetUserMessage", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UserId", userId));
                comm.Parameters.Add(new SqlParameter("ViewAll", viewAll));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedPublicMessage item = new ExtendedPublicMessage();
                    item.MessageUserId = (long)reader["MessageUserId"];
                    item.UserId = (Guid)reader["UserId"];
                    item.MessageAdminId = (int)reader["MessageAdminId"];
                    item.DateRead = null;
                    if (reader["DateRead"]!=DBNull.Value) item.DateRead = (DateTime)reader["DateRead"];
                    item.CreatedDate = (DateTime) reader["CreatedDate"];
                    item.Message = (string) reader["Message"];
                    list.Add(item);
                }
            }

            return list;
        }

        public void AddAdminMessage(string message)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("AddMessageAdmin", Connection)
                                      {CommandType = CommandType.StoredProcedure};
                comm.Parameters.Add(new SqlParameter("Message", message));
                comm.ExecuteNonQuery();
            }
        }

        public void MessageRead(Guid userId, int messageAdminId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Message_UserRead", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("MessageAdminId", messageAdminId));
                comm.Parameters.Add(new SqlParameter("UserId", userId));
                comm.ExecuteNonQuery();
            }
        }

        public List<ExtendedMessage> GetMessages(long? claimId, long?ESZclaimId)
        {
            List<ExtendedMessage> list = new List<ExtendedMessage>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("GetMessage", Connection) { CommandType = CommandType.StoredProcedure };
                if (claimId.HasValue) comm.Parameters.Add(new SqlParameter("ClaimId", claimId));
                if (ESZclaimId.HasValue) comm.Parameters.Add(new SqlParameter("ESZClaimId", ESZclaimId));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedMessage item = new ExtendedMessage();
                    item.MessageId = (Guid)reader["MessageId"];
                    item.ClaimId = (long)reader["ClaimId"];
                    if (reader["ESZClaimId"] != DBNull.Value)
                        item.ESZClaimId = (long)reader["ESZClaimId"];
                    else item.ESZClaimId = null;
                    item.Comment = "";
                    if (reader["Name"]!=DBNull.Value)
                    {
                        item.Comment = (string) reader["Name"];
                    }

                    list.Add(item);
                }
            }

            MessagesCount = list.Count;
            return list;
        }

        public void AddMessage(Guid messageId, long claimId, long? ESZclaimId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("AddMessage", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("MessageId", messageId));
                comm.Parameters.Add(new SqlParameter("ClaimId", claimId));
                if (ESZclaimId.HasValue) comm.Parameters.Add(new SqlParameter("ESZClaimId", ESZclaimId));
                comm.ExecuteNonQuery();
            }
        }

        public List<ExtendedErrorESZ> GetErrorsESZ(DateTime? dateStart, DateTime? dateEnd, long? eszClaimId)
        {
            List<ExtendedErrorESZ> list = new List<ExtendedErrorESZ>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Message_GetErrors", Connection)
                                      {CommandType = CommandType.StoredProcedure};
                if (dateStart.HasValue) comm.Parameters.Add(new SqlParameter("DateStart", dateStart.Value));
                if (dateEnd.HasValue) comm.Parameters.Add(new SqlParameter("DateEnd", dateEnd.Value));
                if (eszClaimId.HasValue) comm.Parameters.Add(new SqlParameter("ESZClaimId", eszClaimId.Value));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedErrorESZ item = new ExtendedErrorESZ();
                    item.MessageId = (Guid) reader["MessageId"];
                    item.Error = (string)reader["Error"];
                    item.ESZClaimId = (long)reader["ESZClaimId"];
                    item.CreatedDate = (DateTime)reader["CreatedDate"];
                    if (reader["ParentFio"] != DBNull.Value) item.ParentFio = (string)reader["ParentFio"];
                    if (reader["PupilFio"] != DBNull.Value) item.PupilFio = (string)reader["PupilFio"];
                    if (reader["PupilBirthday"] != DBNull.Value) item.PupilBirthday = (DateTime)reader["PupilBirthday"];
                    if (reader["PupilPassportNumber"] != DBNull.Value) item.PupilPassportNumber = (string)reader["PupilPassportNumber"];
                    if (reader["PupilPassportSeries"] != DBNull.Value) item.PupilPassportSeries = (string)reader["PupilPassportSeries"];
                    if (reader["PupilPassportIssueDate"] != DBNull.Value) item.PupilPassportIssueDate = (DateTime)reader["PupilPassportIssueDate"];
                    if (reader["ParentEMail"] != DBNull.Value) item.ParentEMail = (string)reader["ParentEMail"];
                    if (reader["ParentPhoneNumber"] != DBNull.Value) item.ParentPhoneNumber = (string)reader["ParentPhoneNumber"];
                    if (reader["ChildUnionId"] != DBNull.Value) item.ChildUnionId = (long)reader["ChildUnionId"];

                    list.Add(item);
                }
            }

            return list;
        }

        public void AddMessageError(Guid messageId, string error, long? ESZclaimId, ExtendedUser pupil, ExtendedUser parent, long childunionId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Message_AddError", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("MessageId", messageId));
                comm.Parameters.Add(new SqlParameter("Error", error));
                comm.Parameters.Add(new SqlParameter("ParentFio", parent.Fio));
                comm.Parameters.Add(new SqlParameter("PupilFio", pupil.Fio));
                comm.Parameters.Add(new SqlParameter("PupilBirthday", pupil.Birthday));
                comm.Parameters.Add(new SqlParameter("PupilPassportSeries", pupil.Passport.Series));
                comm.Parameters.Add(new SqlParameter("PupilPassportNumber", pupil.Passport.Number));
                comm.Parameters.Add(new SqlParameter("PupilPassportIssueDate", pupil.Passport.IssueDate));
                comm.Parameters.Add(new SqlParameter("ParentEMail", parent.EMail));
                comm.Parameters.Add(new SqlParameter("ParentPhoneNumber", parent.PhoneNumber));
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childunionId));

                if (ESZclaimId.HasValue) comm.Parameters.Add(new SqlParameter("ESZClaimId", ESZclaimId));
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateMessage(long claimId, Guid messageId, long? ESZclaimId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateMessage", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ClaimId", claimId));
                comm.Parameters.Add(new SqlParameter("MessageId", messageId));
                if (ESZclaimId.HasValue) comm.Parameters.Add(new SqlParameter("ESZClaimId", ESZclaimId));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteMessage(Guid messageId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("DeleteMessage", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("MessageId", messageId));
                comm.ExecuteNonQuery();
            }
        }

        public ExtendedMessage? GetMessage(Guid messageId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("GetMessage", Connection) {CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("MessageId", messageId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedMessage item = new ExtendedMessage();
                    item.MessageId = (Guid)reader["MessageId"];
                    item.ClaimId = (long)reader["ClaimId"];
                    return item;
                }
            }
            return null;
        }

        public int GetMessagesCount()
        {
            return MessagesCount;
        }
    }
}

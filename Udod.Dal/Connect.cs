﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Udod.Dal
{
    public static class Connect
    {
        public static string SqlConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["UdodConnectionString"].ConnectionString;
            }
        }
    }
}

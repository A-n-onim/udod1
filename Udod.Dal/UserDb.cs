﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Udod.Dal
{
    public class UserDb : BaseDb
    {
        public long GetRelation(Guid pupilId, Guid parentId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("select RelationId from Relation where ChildId=@pupilId and ParentId=@parentId", Connection) {CommandType = CommandType.Text};
                comm.Parameters.Add(new SqlParameter("pupilId", pupilId));
                comm.Parameters.Add(new SqlParameter("parentId", parentId));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return Convert.ToInt64(reader[0]);
                }
            }
            return -1;
        }

        public ExtendedUser GetUserByCert(string cert)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetUserByCert", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Cert", cert));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedUser user = new ExtendedUser();
                    user.UserId = (Guid)reader["UserId"];
                    user.UserName = (string)reader["Username"];
                    user.LastName = (string)reader["Lastname"];
                    user.FirstName = (string)reader["Firstname"];
                    user.MiddleName = (string)reader["Middlename"];
                    user.PhoneNumber = (string)reader["Phonenumber"];
                    user.ExpressPhoneNumber = (string)reader["ExpressPhonenumber"];
                    user.EMail = (string)reader["EMail"];
                    user.Birthday = (DateTime)reader["Birthday"];
                    user.CreateDate = (DateTime)reader["CreatedDate"];
                    user.LastModifiedDate = (DateTime)reader["LastModifiedDate"];
                    if (reader["UdodId"] != DBNull.Value) user.UdodId = (long)reader["UdodId"];
                    if (reader["CityId"] != DBNull.Value) user.CityId = (int)reader["CityId"];
                    return user;
                }
            }

            return new ExtendedUser();
        }

        public ExtendedUser GetUser(string userName)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetUser", Connection){CommandType = CommandType.StoredProcedure};
                comm.Parameters.Add(new SqlParameter("UserName", userName));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedUser user= new ExtendedUser();
                    user.UserId = (Guid) reader["UserId"];
                    user.UserName = (string) reader["Username"];
                    user.LastName = (string) reader["Lastname"];
                    user.FirstName = (string) reader["Firstname"];
                    user.MiddleName = (string) reader["Middlename"];
                    user.PhoneNumber = (string) reader["Phonenumber"];
                    user.ExpressPhoneNumber = (string) reader["ExpressPhonenumber"];
                    user.EMail = (string) reader["EMail"];
                    user.Birthday = (DateTime) reader["Birthday"];
                    user.CreateDate = (DateTime)reader["CreatedDate"];
                    user.LastModifiedDate = (DateTime)reader["LastModifiedDate"];
                    if (reader["UdodId"]!=DBNull.Value) user.UdodId= (long)reader["UdodId"];
                    if (reader["CityId"]!=DBNull.Value) user.CityId= (int)reader["CityId"];
                    return user;
                }
            }

            return new ExtendedUser();
        }

        public ExtendedUser GetUserAddresses(Guid userId)
        {
            ExtendedUser item = null;
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetUserAddress", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UserId", userId));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long? _addressId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long)reader["AddressId"];

                    if (null != item)
                    {
                        if (_addressId.HasValue && item.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];
                            
                            itemAddress.AddressStr = (string)reader["AddressStr"];

                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }
                    }
                    else
                    {
                        item = new ExtendedUser();
                        item.UserId = (Guid)reader["UserId"];
                        item.UserName = (string)reader["Username"];

                        item.Addresses = new List<ExtendedAddress>();

                        if (_addressId.HasValue)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };

                            itemAddress.AddressStr = (string)reader["AddressStr"];

                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }
                    }
                }
                return item;
            }
        }

        public void SaveProfile(List<SqlParameter> parameters)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                /*
                SqlCommand comm= new SqlCommand("User_SaveUserProfile", Connection){CommandType = CommandType.StoredProcedure};
                Connection.Open();
                foreach (SqlParameter sqlParameter in parameters)
                {
                    comm.Parameters.Add(sqlParameter);
                }
                comm.ExecuteNonQuery();*/
            }
        }

        public bool CheckExistsingPassport(string lastName, string firstName, int documentType, string series, string number, Guid? userId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("User_CheckExistPassport", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("LastName", lastName));
                comm.Parameters.Add(new SqlParameter("FirstName", firstName));
                comm.Parameters.Add(new SqlParameter("DocumentType", documentType));
                comm.Parameters.Add(new SqlParameter("Series", series.Trim()));
                comm.Parameters.Add(new SqlParameter("Number", number.Trim()));
                if (userId.HasValue) comm.Parameters.Add(new SqlParameter("UserId", userId.Value));

                conn.Open();
                return Convert.ToBoolean(comm.ExecuteScalar());
            }
        }

        public List<ExtendedUserPassportMessage> FindUdodByPassport(int documentType, string series, string number)
        {
            List<ExtendedUserPassportMessage> list = new List<ExtendedUserPassportMessage>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("User_FindUdodByPassport", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("DocumentType", documentType));
                comm.Parameters.Add(new SqlParameter("Series", series.Trim()));
                comm.Parameters.Add(new SqlParameter("Number", number.Trim()));

                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while(reader.Read())
                {
                    Guid _childId = (Guid)reader["childId"];
                    Guid _parentId = (Guid)reader["parentId"];

                    long? _udodId = null;
                    if (reader["UdodId"] != DBNull.Value) _udodId = (long)reader["UdodId"];

                    long? _claimId = null;
                    if (reader["ClaimId"] != DBNull.Value) _claimId = (long)reader["ClaimId"];


                    ExtendedUserPassportMessage last = list.LastOrDefault(i => i.ChildId == _childId && i.ParentId == _parentId);

                    if (null != last)
                    {
                        if (_udodId.HasValue)
                        {
                            ExtendedUdodPassportMessage last2 = last.Udods.LastOrDefault(i => i.UdodId == _udodId);

                            if (null != last2)
                            {
                                if (_claimId.HasValue && last2.Claims.Count(i => i.ClaimId == _claimId) == 0)
                                {
                                    ExtendedClaim claim = new ExtendedClaim();
                                    claim.ClaimId = _claimId.Value;
                                    claim.Number = (long)reader["ClaimNumber"];
                                    (last2.Claims as List<ExtendedClaim>).Add(claim);
                                }
                            }
                            else
                            {
                                ExtendedUdodPassportMessage udodpm = new ExtendedUdodPassportMessage();
                                udodpm.UdodId = _udodId.Value;
                                udodpm.ShortName = (string)reader["ShortName"];
                                udodpm.PhoneNumber = (string)reader["PhoneNumber"];

                                udodpm.Claims = new List<ExtendedClaim>();
                                if (_claimId.HasValue)
                                {
                                    ExtendedClaim claim = new ExtendedClaim();
                                    claim.ClaimId = _claimId.Value;
                                    claim.Number = (long)reader["ClaimNumber"];
                                    (udodpm.Claims as List<ExtendedClaim>).Add(claim);
                                }

                                (last.Udods as List<ExtendedUdodPassportMessage>).Add(udodpm);
                            }
                        }
                        if (string.IsNullOrEmpty(last.ParentFio) && !string.IsNullOrEmpty((string)reader["ParentFio"]))
                        {
                            last.ParentFio = (string) reader["ParentFio"];
                        }
                    }
                    else
                    {
                        ExtendedUserPassportMessage item = new ExtendedUserPassportMessage();
                        item.ChildFio = (string)reader["ChildFio"];
                        item.ChildId = (Guid)reader["ChildId"];
                        item.ParentFio = (string)reader["ParentFio"];
                        item.ParentId = (Guid)reader["ParentId"];

                        item.Udods = new List<ExtendedUdodPassportMessage>();
                        if (_udodId.HasValue)
                        {
                            ExtendedUdodPassportMessage udodpm = new ExtendedUdodPassportMessage();
                            udodpm.UdodId = _udodId.Value;
                            udodpm.ShortName = (string)reader["ShortName"];
                            udodpm.PhoneNumber = (string)reader["PhoneNumber"];

                            udodpm.Claims = new List<ExtendedClaim>();
                            if (_claimId.HasValue)
                            {
                                ExtendedClaim claim = new ExtendedClaim();
                                claim.ClaimId = _claimId.Value;
                                claim.Number = (long)reader["ClaimNumber"];
                                (udodpm.Claims as List<ExtendedClaim>).Add(claim);
                            }

                            (item.Udods as List<ExtendedUdodPassportMessage>).Add(udodpm);
                        }

                        list.Add(item);
                    }
                }
            }
            return list;
        }

        public Guid CreateParent(string lastName, string firstName, string middleName, DateTime birthDay, bool sex, string phoneNumber, string eMail, bool isPupil, int documentType, string series, string number, DateTime issueDate, int userType, string expressPhoneNumber)
        {
            Guid userId = Guid.Empty;

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("User_CreateParent", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("LastName", lastName));
                comm.Parameters.Add(new SqlParameter("FirstName", firstName));
                comm.Parameters.Add(new SqlParameter("MiddleName", middleName));
                comm.Parameters.Add(new SqlParameter("Birthday", birthDay));
                comm.Parameters.Add(new SqlParameter("Sex", sex));
                comm.Parameters.Add(new SqlParameter("PhoneNumber", phoneNumber));
                comm.Parameters.Add(new SqlParameter("ExpressPhoneNumber", expressPhoneNumber));
                comm.Parameters.Add(new SqlParameter("EMail", eMail));
                comm.Parameters.Add(new SqlParameter("IsPupil", isPupil));
                comm.Parameters.Add(new SqlParameter("DocumentType", documentType));
                comm.Parameters.Add(new SqlParameter("Series", series.Trim()));
                comm.Parameters.Add(new SqlParameter("Number", number.Trim()));
                comm.Parameters.Add(new SqlParameter("IssueDate", issueDate));
                comm.Parameters.Add(new SqlParameter("UserType", userType));
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    userId = (Guid)reader[0];
                }
            }

            return userId;
        }
        public Guid CreatePupil(Guid userId, string lastName, string firstName, string middleName, DateTime birthDay, bool sex, bool isPupil, string pupilschool, string pupilSchoolCode, string pupilclass, int documentType, string series, string number, DateTime issueDate, string reestrCode, string reestrGuidCode)
        {
            Guid PupilId = Guid.Empty;

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("User_CreatePupil", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("userId", userId));
                comm.Parameters.Add(new SqlParameter("LastName", lastName));
                comm.Parameters.Add(new SqlParameter("FirstName", firstName));
                comm.Parameters.Add(new SqlParameter("MiddleName", middleName));
                comm.Parameters.Add(new SqlParameter("Birthday", birthDay));
                comm.Parameters.Add(new SqlParameter("Sex", sex));
                comm.Parameters.Add(new SqlParameter("IsPupil", isPupil));
                if (!string.IsNullOrEmpty(pupilschool)) comm.Parameters.Add(new SqlParameter("PupilSchool", pupilschool));
                if (!string.IsNullOrEmpty(pupilSchoolCode)) comm.Parameters.Add(new SqlParameter("PupilSchoolCode", pupilSchoolCode));
                if (!string.IsNullOrEmpty(pupilclass)) comm.Parameters.Add(new SqlParameter("PupilClass", pupilclass));
                comm.Parameters.Add(new SqlParameter("DocumentType", documentType));
                comm.Parameters.Add(new SqlParameter("Series", series.Trim()));
                comm.Parameters.Add(new SqlParameter("Number", number.Trim()));
                comm.Parameters.Add(new SqlParameter("IssueDate", issueDate));
                if (!string.IsNullOrEmpty(reestrCode)) comm.Parameters.Add(new SqlParameter("ReestrCode", reestrCode));
                if (!string.IsNullOrEmpty(reestrGuidCode)) comm.Parameters.Add(new SqlParameter("ReestrGuidCode", reestrGuidCode));
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    PupilId = (Guid)reader[0];
                }
            }

            return PupilId;
        }

        public List<ExtendedUser> GetUsers(long? udodId)
        {
            List<ExtendedUser> list = new List<ExtendedUser>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetUsers", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    
                    Guid _userId = (Guid) reader["UserId"];
                    int? _roleId=null;
                    if (reader["RoleId"] != DBNull.Value) _roleId = (int) reader["RoleId"];
                    var last = list.Where(i => i.UserId == _userId).LastOrDefault();
                    if (last !=null)
                    {
                        if (_roleId.HasValue && last.Roles.Count(i=>i.RoleId==_roleId.Value)==0)
                        {
                            var role = new ExtendedRole()
                            {
                                RoleId = _roleId.Value,
                                Name = (string)reader["RoleName"],
                                ShortName = (string)reader["RoleShortName"]
                            };
                            last.Roles.Add(role);
                        }
                    }
                    else
                    {
                        ExtendedUser item = new ExtendedUser();
                        item.UserId = (Guid) reader["UserId"];
                        
                        item.UserName = (string) reader["UserName"];
                        item.LastName = (string) reader["LastName"];
                        item.FirstName = (string) reader["FirstName"];
                        item.MiddleName = (string) reader["MiddleName"];

                        item.Roles = new List<ExtendedRole>();
                        if (_roleId.HasValue)
                        {
                            var role = new ExtendedRole()
                            {
                                RoleId = _roleId.Value,
                                Name = (string) reader["RoleName"],
                                ShortName = (string) reader["RoleShortName"]
                            };
                            item.Roles.Add(role);
                        }
                        list.Add(item);
                    }
                    
                }
            }

            return list;
        }

        public List<ExtendedUser> GetUsersPaging(long? udodId, string Login, string LastName, string FirstName, string MiddleName, int? PagerIndex, int? PagerLength)
        {
            List<ExtendedUser> list = new List<ExtendedUser>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetUsersPaging", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (!string.IsNullOrEmpty(Login)) comm.Parameters.Add(new SqlParameter("Login", Login));
                if (!string.IsNullOrEmpty(LastName)) comm.Parameters.Add(new SqlParameter("LastName", LastName));
                if (!string.IsNullOrEmpty(FirstName)) comm.Parameters.Add(new SqlParameter("FirstName", FirstName));
                if (!string.IsNullOrEmpty(MiddleName)) comm.Parameters.Add(new SqlParameter("MiddleName", MiddleName));
                if (PagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", PagerIndex));
                if (PagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", PagerLength));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {

                    Guid _userId = (Guid)reader["UserId"];
                    int? _roleId = null;
                    if (reader["RoleId"] != DBNull.Value) _roleId = (int)reader["RoleId"];
                    var last = list.Where(i => i.UserId == _userId).LastOrDefault();
                    if (last != null)
                    {
                        if (_roleId.HasValue && last.Roles.Count(i => i.RoleId == _roleId.Value) == 0)
                        {
                            var role = new ExtendedRole()
                            {
                                RoleId = _roleId.Value,
                                Name = (string)reader["RoleName"],
                                ShortName = (string)reader["RoleShortName"]
                            };
                            last.Roles.Add(role);
                        }
                    }
                    else
                    {
                        ExtendedUser item = new ExtendedUser();
                        item.UserId = (Guid)reader["UserId"];

                        item.UserName = (string)reader["UserName"];
                        item.LastName = (string)reader["LastName"];
                        item.FirstName = (string)reader["FirstName"];
                        item.MiddleName = (string)reader["MiddleName"];

                        item.Roles = new List<ExtendedRole>();
                        if (_roleId.HasValue)
                        {
                            var role = new ExtendedRole()
                            {
                                RoleId = _roleId.Value,
                                Name = (string)reader["RoleName"],
                                ShortName = (string)reader["RoleShortName"]
                            };
                            item.Roles.Add(role);
                        }
                        list.Add(item);
                    }

                }
            }

            return list;
        }

        public int GetUsersCount(long? udodId, string Login, string LastName, string FirstName, string MiddleName)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetUsersCount", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (!string.IsNullOrEmpty(Login)) comm.Parameters.Add(new SqlParameter("Login", Login));
                if (!string.IsNullOrEmpty(LastName)) comm.Parameters.Add(new SqlParameter("LastName", LastName));
                if (!string.IsNullOrEmpty(FirstName)) comm.Parameters.Add(new SqlParameter("FirstName", FirstName));
                if (!string.IsNullOrEmpty(MiddleName)) comm.Parameters.Add(new SqlParameter("MiddleName", MiddleName));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }

            return 0;
        }

        public ExtendedUser GetUserToUpdate(Guid userId)
        {
            ExtendedUser user = null;
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetUserToUpdate", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UserId", userId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    int? _roleId = null;
                    if (reader["RoleId"] != DBNull.Value) _roleId = (int)reader["RoleId"];

                    if (user != null)
                    {
                        if (_roleId.HasValue && user.Roles.Count(i => i.RoleId == _roleId.Value) == 0)
                        {
                            var role = new ExtendedRole()
                            {
                                RoleId = _roleId.Value,
                                Name = (string)reader["RoleName"],
                                ShortName = (string)reader["RoleShortName"]
                            };
                            user.Roles.Add(role);
                        }
                    }
                    else
                    {
                        user = new ExtendedUser();
                        user.UserId = (Guid)reader["UserId"];
                        user.UserName = (string)reader["Username"];
                        user.LastName = (string)reader["Lastname"];
                        user.FirstName = (string)reader["Firstname"];
                        user.MiddleName = (string)reader["Middlename"];
                        user.PhoneNumber = (string)reader["Phonenumber"];
                        user.ExpressPhoneNumber = (string)reader["ExpressPhonenumber"];
                        user.EMail = (string)reader["EMail"];
                        user.Birthday = (DateTime)reader["Birthday"];
                        user.CreateDate = (DateTime)reader["CreatedDate"];
                        user.LastModifiedDate = (DateTime)reader["LastModifiedDate"];
                        if (reader["UdodId"] != DBNull.Value) user.UdodId = (long)reader["UdodId"];
                        if (reader["CityId"] != DBNull.Value) user.CityId = (int)reader["CityId"];

                        user.Roles = new List<ExtendedRole>();
                        if (_roleId.HasValue)
                        {
                            var role = new ExtendedRole()
                            {
                                RoleId = _roleId.Value,
                                Name = (string)reader["RoleName"],
                                ShortName = (string)reader["RoleShortName"]
                            };
                            user.Roles.Add(role);
                        }
                    }
                }
            }
            return user;
        }

        public void DeleteUser(Guid userId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_DeleteUser", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UserId", userId));
                comm.ExecuteNonQuery();
            }
        }

        public bool CheckLoginIsExist(string userName)
        {
            bool isExist = false; 
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("User_CheckExistLogin", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("@UserName", userName));
                isExist = Convert.ToBoolean(comm.ExecuteScalar());
            }
            return isExist;
        }

        public Guid CreateUser(string userName, string password, string eMail)
        {
            Guid userId = Guid.Empty;
            DateTime createDate = DateTime.Now;
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("User_CreateUser", conn) {CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("@UserName", userName));
                comm.Parameters.Add(new SqlParameter("@Password", password));
                comm.Parameters.Add(new SqlParameter("@EMail", eMail));
                comm.Parameters.Add("@CreationDate", SqlDbType.DateTime).Value = createDate;
                comm.Parameters.Add("@LastPasswordChangedDate", SqlDbType.DateTime).Value = createDate;
                comm.Parameters.Add("@LastActivityDate", SqlDbType.DateTime).Value = createDate;
                comm.Parameters.Add("@IsLockedOut", SqlDbType.Bit).Value = false;
                comm.Parameters.Add("@LastLockedOutDate", SqlDbType.DateTime).Value = createDate;
                comm.Parameters.Add("@FailedPasswordAttemptCount", SqlDbType.Int).Value = 0;
                comm.Parameters.Add("@FailedPasswordAttemptWindowStart", SqlDbType.DateTime).Value = createDate;
                comm.Parameters.Add("@FailedPasswordAnswerAttemptCount", SqlDbType.Int).Value = 0;
                comm.Parameters.Add("@FailedPasswordAnswerAttemptWindowStart", SqlDbType.DateTime).Value = createDate;
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    userId = (Guid)reader[0];
                }
            }
            return userId;
        }
        public void SaveProfile(Guid userId, string userName, string lastName, string firstName, string middleName, DateTime birthday, string eMail, string phonenumber, string expressPhoneNumber, string pupilSchool, string pupilClass)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("User_SaveUserProfile", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("@UserId", userId));
                comm.Parameters.Add(new SqlParameter("@UserName", userName));
                comm.Parameters.Add(new SqlParameter("@LastName", lastName));
                comm.Parameters.Add(new SqlParameter("@FirstName", firstName));
                comm.Parameters.Add(new SqlParameter("@MiddleName", middleName));
                comm.Parameters.Add(new SqlParameter("@EMail", eMail));
                comm.Parameters.Add(new SqlParameter("@Birthday", birthday));
                comm.Parameters.Add(new SqlParameter("@PhoneNumber", phonenumber));
                comm.Parameters.Add(new SqlParameter("@ExpressPhoneNumber", expressPhoneNumber));
                if (!string.IsNullOrEmpty(pupilSchool)) comm.Parameters.Add(new SqlParameter("@PupilSchool", pupilSchool));
                if (!string.IsNullOrEmpty(pupilClass)) comm.Parameters.Add(new SqlParameter("@PupilClass", pupilClass));
                comm.ExecuteNonQuery();
                
            }
        }
        public ExtendedUser GetUser(Guid userId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetUser", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UserId", userId));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedUser user = new ExtendedUser();
                    user.UserId = (Guid)reader["UserId"];
                    user.UserName = (string)reader["Username"];
                    user.LastName = (string)reader["Lastname"];
                    user.FirstName = (string)reader["Firstname"];
                    user.MiddleName = (string)reader["Middlename"];
                    user.PhoneNumber = (string)reader["Phonenumber"];
                    user.ExpressPhoneNumber = (string)reader["ExpressPhonenumber"];
                    user.EMail = (string)reader["EMail"];
                    user.Birthday = (DateTime)reader["Birthday"];
                    user.CreateDate = (DateTime)reader["CreatedDate"];
                    user.LastModifiedDate = (DateTime)reader["LastModifiedDate"];
                    if (reader["UdodId"] != DBNull.Value) user.UdodId = (long)reader["UdodId"];
                    if (reader["CityId"] != DBNull.Value) user.CityId = (int)reader["CityId"];
                    return user;
                }
            }

            return new ExtendedUser();
        }
        public void AddUserRole(Guid userId, int roleId, int? cityId, long? udodId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("User_AddRole",conn){CommandType = CommandType.StoredProcedure};
                comm.Parameters.Add(new SqlParameter("UserId", userId));
                comm.Parameters.Add(new SqlParameter("RoleId", roleId));
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.ExecuteNonQuery();
            }
        }
        public void DeleteUserRole(Guid userId, int roleId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("User_DeleteRole", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UserId", userId));
                comm.Parameters.Add(new SqlParameter("RoleId", roleId));
                comm.ExecuteNonQuery();
            }
        }

        public List<ExtendedUserType> GetUserTypes()
        {
            List<ExtendedUserType> list = new List<ExtendedUserType>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("User_GetUserTypes", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedUserType userType = new ExtendedUserType();
                    userType.UserTypeId = (int)reader["DictUserTypeId"];
                    userType.UserTypeName = (string)reader["TypeName"];
                    list.Add(userType);
                }
            }
            return list;
        }

        public List<ExtendedImportedPupilUdod> ImportedGetPupilUdods(int documentType, string series, string number)
        {
            List<ExtendedImportedPupilUdod> list=new List<ExtendedImportedPupilUdod>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("User_GetUsersByPassport", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("DocumentType", documentType));
                comm.Parameters.Add(new SqlParameter("Series", series.Trim()));
                comm.Parameters.Add(new SqlParameter("Number", number.Trim()));

                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedImportedPupilUdod item = new ExtendedImportedPupilUdod();
                    item.UdodId = (long)reader["UdodId"];
                    item.UdodName = (string)reader["UdodName"];
                    item.UdodPhoneNumber = (string)reader["UdodPhoneNumber"];
                    item.UserId = (Guid)reader["UserId"];
                    item.LastName = (string)reader["Lastname"];
                    item.FirstName = (string)reader["Firstname"];
                    item.MiddleName = (string)reader["Middlename"];
                    item.DateEnd = null;
                    if (reader["DateEnd"] != DBNull.Value) item.DateEnd = (DateTime)reader["DateEnd"];
                    item.DictClaimStatusId = null;
                    if (reader["DictClaimStatusId"] != DBNull.Value) item.DictClaimStatusId = (int)reader["DictClaimStatusId"];
                    list.Add(item);
                }
            }
            return list;
        }

        public void UpdatePersonalDeed(Guid userId, string errorMessage)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand("User_UpdatePersonalDeed", conn) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("UserId", userId));
                comm.Parameters.Add(new SqlParameter("ErrorMessage", errorMessage));
                comm.ExecuteReader();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Udod.Dal
{
    public class MessageOutputDb : BaseDb
    {
        private int MessagesOutputCount;

        public MessageOutputDb() {}

        public List<ExtendedMessageOutput> GetMessagesOutput()
        {
            List<ExtendedMessageOutput> list = new List<ExtendedMessageOutput>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("GetMessageOutput", Connection) { CommandType = CommandType.StoredProcedure };
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedMessageOutput item = new ExtendedMessageOutput();
                    item.MessageOutputId = (Guid)reader["MessageOutputId"];
                    item.MessageData = (string)reader["MessageData"];
                    item.Count = (int)reader["Count"];
                    list.Add(item);
                }
            }

            MessagesOutputCount = list.Count;
            return list;
        }

        public void AddMessageOutput(Guid messageOutputId, string messageData, int? count)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("AddMessageOutput", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("MessageOutputId", messageOutputId));
                comm.Parameters.Add(new SqlParameter("MessageData", messageData));
                comm.Parameters.Add(new SqlParameter("Count", count));
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateMessageOutput(int Count, Guid messageOutputId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateMessageOutput", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Count", Count));
                comm.Parameters.Add(new SqlParameter("MessageOutputId", messageOutputId));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteMessageOutput(Guid messageOutputId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("DeleteMessageOutput", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("MessageOutputId", messageOutputId));
                comm.ExecuteNonQuery();
            }
        }

        public int GetMessagesOutputCount()
        {
            return MessagesOutputCount;
        }
    }
}

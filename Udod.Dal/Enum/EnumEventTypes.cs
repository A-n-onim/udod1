﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Udod.Dal.Enum
{
    public enum EnumEventTypes
    {
        ApplicationError = 1,
        ClaimCreated = 2,
        ClaimEdited = 3,
        ClaimStatusChanged = 4,
        ClaimDeleted = 5,
        UdodCreated = 6,
        UdodUpdated = 7,
        UdodDeleted = 8,
        UdodMerged = 9,
        UdodAttached = 10,
        UdodDetached = 11,
        UdodDivided = 12,
        PupilUpdate = 13,
        PupilUdodUpdate = 14,
        PupilUdodDelete = 15,
        SecondYearZach = 16,
        SchoolZach = 17,
        UdodStatusUpdate = 18,
        TransferNewYear = 19,
        AddScheduleLesson = 22,
        UpdateScheduleLesson = 20,
        DeleteScheduleLesson = 21,
        Login=23
    }
}

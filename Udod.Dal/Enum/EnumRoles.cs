﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Udod.Dal.Enum
{
    public enum EnumRoles
    {
        Administrator=1,
        Osip=2,
        UdodEmployee=3,
        Department=4,
        User=5,
        Pupil=6
    }

}

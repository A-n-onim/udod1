﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.DataSetExtensions;
using System.Configuration;

namespace Udod.Dal
{
    public class ClaimDb : BaseDb
    {
        public List<ExtendedClaim> GetExportClaim(Guid? userId, long? udodId, int? cityId, long? childUnionId, int? sectionId, int? claimStatusId, string numberStr, string lastName, string firstName, string middleName, string udodName, string childUnion, DateTime? startDate, DateTime? endDate, string sectionName, DateTime? birthdayBegin, DateTime? birthdayEnd, int? birthdayFindType, Guid? teacherId)
        {
            long? number;
            try
            {
                if (numberStr == null) throw new Exception();
                number = Convert.ToInt64(numberStr);
            }
            catch
            {
                number = null;
            }

            DateTime minDate = new DateTime(1900, 1, 1);
            if (startDate < minDate) startDate = minDate;
            if (endDate < minDate) endDate = minDate;
            if (birthdayBegin < minDate) birthdayBegin = minDate;
            if (birthdayEnd < minDate) birthdayEnd = minDate;

            List<ExtendedClaim> list = new List<ExtendedClaim>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Claim_GetClaimExport", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (userId.HasValue) comm.Parameters.Add(new SqlParameter("UserId", userId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                if (claimStatusId.HasValue) comm.Parameters.Add(new SqlParameter("ClaimStatusId", claimStatusId));
                if (number.HasValue) comm.Parameters.Add(new SqlParameter("Number", number));
                if (startDate.HasValue) comm.Parameters.Add(new SqlParameter("StartDate", startDate));
                if (endDate.HasValue) comm.Parameters.Add(new SqlParameter("EndDate", endDate));
                if (!string.IsNullOrEmpty(lastName)) comm.Parameters.Add(new SqlParameter("LastName", lastName.Trim()));
                if (!string.IsNullOrEmpty(firstName))
                    comm.Parameters.Add(new SqlParameter("FirstName", firstName.Trim()));
                if (!string.IsNullOrEmpty(middleName))
                    comm.Parameters.Add(new SqlParameter("MiddleName", middleName.Trim()));
                if (!string.IsNullOrEmpty(udodName)) comm.Parameters.Add(new SqlParameter("UdodName", udodName.Trim()));
                if (!string.IsNullOrEmpty(childUnion))
                    comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnion));
                if (!string.IsNullOrEmpty(sectionName))
                    comm.Parameters.Add(new SqlParameter("SectionName", sectionName));
                if (birthdayBegin.HasValue) comm.Parameters.Add(new SqlParameter("BirthdayBegin", birthdayBegin));
                if (birthdayEnd.HasValue) comm.Parameters.Add(new SqlParameter("BirthdayEnd", birthdayEnd));
                if (birthdayFindType.HasValue)
                    comm.Parameters.Add(new SqlParameter("BirthdayFindType", Convert.ToBoolean(birthdayFindType)));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                SqlDataReader reader = comm.ExecuteReader();
                while(reader.Read())
                {
                    long? _ClaimId = (long) reader["ClaimId"];

                    var last = list.FirstOrDefault(i => i.ClaimId == _ClaimId);

                    if (last.ClaimId!=0)
                    {
                        if (reader["TeacherId"] != DBNull.Value)
                        last.ChildUnion.Teachers.Add(new ExtendedTeacher()
                                                         {
                                                             UserId = (Guid)reader["TeacherId"],
                                                             LastName = (string)reader["TeacherLastName"],
                                                             FirstName = (string)reader["TeacherFirstName"],
                                                             MiddleName = (string)reader["TeacherMiddleName"]
                                                         });
                    }
                    else
                    {
                        ExtendedClaim item = new ExtendedClaim();
                        item.ClaimId = (long) reader["ClaimId"];
                        item.Number = (long) reader["Number"];
                        item.BeginDate = (DateTime) reader["BeginDate"];
                        item.CreatedDate = (DateTime) reader["CreatedDate"];
                        item.NumYear = (int) reader["PupilNumYear"];
                        item.UpdateStatusDate = (DateTime)reader["UpdateStatusDate"];
                        item.ClaimStatus = new ExtendedClaimStatus
                                               {
                                                   ClaimStatusId = (int) reader["DictClaimStatusId"],
                                                   ClaimStatusName = (string) reader["ClaimStatusName"]
                                               };
                        item.Udod = new ExtendedUdod()
                                        {
                                            UdodId = (long) reader["UdodId"],
                                            Name = (string) reader["UdodName"]
                                        };
                        item.ChildUnion = new ExtendedChildUnion()
                                              {
                                                  ChildUnionId = (long) reader["ChildUnionId"],
                                                  Name = (string) reader["ChildUnionName"],
                                                  isDeleted = reader["ChildUnionDateEnd"] != DBNull.Value,
                                                  Program = new ExtendedProgram()
                                                                {
                                                                    ProgramId = (int) reader["ProgramId"],
                                                                    Name = (string) reader["ProgramName"]
                                                                },
                                                  Profile = new ExtendedProfile()
                                                                {
                                                                    ProfileId = (int) reader["ProfileId"],
                                                                    Name = (string) reader["ProfileName"]
                                                                },
                                                  Section = new ExtendedSection()
                                                                {
                                                                    SectionId = (int) reader["SectionId"],
                                                                    Name = (string) reader["SectionName"]
                                                                }
                                              };
                        item.ChildUnion.Teachers = new List<ExtendedTeacher>();
                        if (reader["TeacherId"]!=DBNull.Value)
                        item.ChildUnion.Teachers.Add(new ExtendedTeacher()
                                                         {
                                                             UserId = (Guid)reader["TeacherId"],
                                                             LastName = (string)reader["TeacherLastName"],
                                                             FirstName = (string)reader["TeacherFirstName"],
                                                             MiddleName = (string)reader["TeacherMiddleName"]
                                                         });


                        item.Parent = new ExtendedUser
                                          {
                                              UserId = (Guid) reader["ParentId"],
                                              LastName = (string) reader["ParentLastName"],
                                              FirstName = (string) reader["ParentFirstName"],
                                              MiddleName = (string) reader["ParentMiddleName"],
                                              EMail = (string) reader["ParentEMail"],
                                              PhoneNumber = (string) reader["ParentPhoneNumber"],
                                              ExpressPhoneNumber = (string)reader["ExpressParentPhoneNumber"]
                                          };
                        item.Pupil = new ExtendedUser
                                         {
                                             UserId = (Guid) reader["PupilId"],
                                             LastName = (string) reader["PupilLastName"],
                                             FirstName = (string) reader["PupilFirstName"],
                                             MiddleName = (string) reader["PupilMiddleName"],
                                             Sex = Convert.ToInt32((bool) reader["PupilSex"]),
                                             Birthday = (DateTime) reader["PupilBirthday"],
                                             Addresses = new List<ExtendedAddress>()
                                         };
                        if (reader["AddressId"] != DBNull.Value)
                        {
                            var address = new ExtendedAddress()
                            {
                                AddressId = (long)reader["AddressId"],
                                AddressStr = (string)reader["AddressStr"]
                            };
                            address.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) address.BtiCode = (int)reader["BtiCode"];
                            item.Pupil.Addresses.Add(address);
                        }
                        
                        if (reader["CreatorRoleId"] != DBNull.Value)
                            item.CreatorRoleId = (int) reader["CreatorRoleId"];

                        list.Add(item);
                    }
                }
            }

            return list;
        }

        public List<long> GetClaimsNotinESZ()
        {
            List<long> list = new List<long>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("SELECT        Claim.ClaimId FROM            Claim INNER JOIN Relation ON Claim.RelationId = Relation.RelationId WHERE        (Claim.CreatedDate > CONVERT(DATETIME, '2013-08-15 00:00:00', 102)) AND (Relation.ParentId = Relation.ChildId) ORDER BY Claim.CreatedDate DESC", conn) {CommandType = CommandType.Text};
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long id = (long) reader[0];
                    list.Add(id);
                }
            }

            return list;
        }

        public List<ExtendedClaim> GetClaims(Guid? userId, long? udodId, int? cityId, long? childUnionId, int? sectionId, int? claimStatusId, string numberStr, string lastName, string firstName, string middleName, string udodName, string childUnion, DateTime? startDate, DateTime? endDate, string sectionName, DateTime? birthdayBegin, DateTime? birthdayEnd, int? birthdayFindType, int? pagerIndex, int? pagerLength, Guid? teacherId)
        {
            long? number;
            try
            {
                if (numberStr == null) throw new Exception();
                number = Convert.ToInt64(numberStr);
            }
            catch
            {
                number = null;
            }

            DateTime minDate = new DateTime(1900, 1, 1);
            if (startDate < minDate) startDate = minDate;
            if (endDate < minDate) endDate = minDate;
            if (birthdayBegin < minDate) birthdayBegin = minDate;
            if (birthdayEnd < minDate) birthdayEnd = minDate;

            List<ExtendedClaim> list = new List<ExtendedClaim>();
            using (SqlConnection conn= new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Claim_GetClaim", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                if (userId.HasValue) comm.Parameters.Add(new SqlParameter("UserId", userId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                if (claimStatusId.HasValue) comm.Parameters.Add(new SqlParameter("ClaimStatusId", claimStatusId));
                if (number.HasValue) comm.Parameters.Add(new SqlParameter("Number", number));
                if (startDate.HasValue) comm.Parameters.Add(new SqlParameter("StartDate", startDate));
                if (endDate.HasValue) comm.Parameters.Add(new SqlParameter("EndDate", endDate));
                if (!string.IsNullOrEmpty(lastName)) comm.Parameters.Add(new SqlParameter("LastName", lastName.Trim()));
                if (!string.IsNullOrEmpty(firstName)) comm.Parameters.Add(new SqlParameter("FirstName", firstName.Trim()));
                if (!string.IsNullOrEmpty(middleName)) comm.Parameters.Add(new SqlParameter("MiddleName", middleName.Trim()));
                if (!string.IsNullOrEmpty(udodName)) comm.Parameters.Add(new SqlParameter("UdodName", udodName.Trim()));
                if (!string.IsNullOrEmpty(childUnion)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnion));
                if (!string.IsNullOrEmpty(sectionName)) comm.Parameters.Add(new SqlParameter("SectionName", sectionName));
                if (birthdayBegin.HasValue) comm.Parameters.Add(new SqlParameter("BirthdayBegin", birthdayBegin));
                if (birthdayEnd.HasValue) comm.Parameters.Add(new SqlParameter("BirthdayEnd", birthdayEnd));
                if (birthdayFindType.HasValue) comm.Parameters.Add(new SqlParameter("BirthdayFindType", Convert.ToBoolean(birthdayFindType)));
                if (pagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", pagerIndex));
                if (pagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", pagerLength));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedClaim item= new ExtendedClaim();
                    item.ClaimId = (long) reader["ClaimId"];
                    item.Number = (long) reader["Number"];
                    item.BeginDate = (DateTime)reader["BeginDate"];
                    item.CreatedDate = (DateTime) reader["CreatedDate"];
                    item.NumYear = (int) reader["PupilNumYear"];
                    item.RelationId = (long)reader["RelationId"];
                    item.ClaimStatus = new ExtendedClaimStatus
                    {
                        ClaimStatusId = (int) reader["DictClaimStatusId"],
                        ClaimStatusName = (string) reader["ClaimStatusName"]
                    };
                    item.Udod= new ExtendedUdod()
                    {
                        UdodId = (long)reader["UdodId"],
                        Name = (string)reader["UdodName"]
                    };
                    item.ChildUnion= new ExtendedChildUnion()
                    {
                        ChildUnionId = (long)reader["ChildUnionId"],
                        Name = (string)reader["ChildUnionName"],
                        AgeStart = (int)reader["AgeStart"],
                        AgeEnd = (int)reader["AgeEnd"],
                        NumYears = (int)reader["NumYears"],
                        isDeleted = reader["ChildUnionDateEnd"] != DBNull.Value,
                        Program = new ExtendedProgram()
                        {
                            ProgramId = (int)reader["ProgramId"],
                            Name = (string)reader["ProgramName"]
                        },
                        Profile = new ExtendedProfile()
                        {
                            ProfileId = (int)reader["ProfileId"],
                            Name = (string)reader["ProfileName"]
                        },
                        Section = new ExtendedSection()
                        {
                            SectionId = (int)reader["SectionId"],
                            Name = (string)reader["SectionName"]
                        }
                    };
                    
                    item.Parent = new ExtendedUser
                    {
                        UserId = (Guid)reader["ParentId"],
                        LastName = (string)reader["ParentLastName"],
                        FirstName = (string)reader["ParentFirstName"],
                        MiddleName = (string)reader["ParentMiddleName"],
                        Passport = new ExtendedPassport
                        {
                            UserPassportId = (long)reader["UserPassportId"],
                            Series = (string)reader["PassportSeries"],
                            Number = (string)reader["PassportNumber"],
                            IssueDate = (DateTime)reader["PassportIssueDate"],
                            PassportType = new ExtendedPassportType
                            {
                                PassportTypeId = (int)reader["PassportTypeId"],
                            }
                        },
                        EMail = (string)reader["ParentEMail"],
                        PhoneNumber = (string)reader["ParentPhoneNumber"]
                    };
                    item.Pupil = new ExtendedUser
                    {
                        UserId = (Guid)reader["PupilId"],
                        LastName = (string)reader["PupilLastName"],
                        FirstName = (string)reader["PupilFirstName"],
                        MiddleName = (string)reader["PupilMiddleName"],
                        Sex = Convert.ToInt32((bool)reader["PupilSex"]),
                        Birthday = (DateTime)reader["PupilBirthday"],
                        Addresses = new List<ExtendedAddress>() 
                    };
                    if (reader["AddressId"]!=DBNull.Value)
                    item.Pupil.Addresses.Add(new ExtendedAddress()
                        {
                            AddressId = (long) reader["AddressId"],
                            AddressStr = (string)reader["AddressStr"]
                        });
                    /*item.AgeGroup = new ExtendedAgeGroup()
                    {
                        UdodAgeGroupId = (long) reader["UdodAgeGroupId"],
                        Age = (int) reader["Age"],
                        UdodId = (long) reader["UdodId"],
                        Program = new ExtendedProgram()
                        {
                            ProgramId = (int)reader["ProgramId"],
                            Name = (string)reader["ProgramName"]
                        },
                        Profile = new ExtendedProfile()
                        {
                            ProfileId = (int)reader["ProfileId"],
                            Name = (string)reader["ProfileName"]
                        },
                        Section = new ExtendedSection()
                        {
                            SectionId = (int)reader["SectionId"],
                            Name = (string)reader["SectionName"]
                        }

                    };*/
                    if (reader["CreatorRoleId"] != DBNull.Value)
                        item.CreatorRoleId = (int)reader["CreatorRoleId"];

                    list.Add(item);
                }
            }
            return list;
        }

        public int GetClaimsCount(Guid? userId, long? udodId, int? cityId, long? childUnionId, int? sectionId, int? claimStatusId, string numberStr, string lastName, string firstName, string middleName, string udodName, string childUnion, DateTime? startDate, DateTime? endDate, string sectionName, DateTime? birthdayBegin, DateTime? birthdayEnd, int? birthdayFindType, int? pagerIndex, int? pagerLength, Guid? teacherId)
        {
            long? number;
            try
            {
                if (numberStr == null) throw new Exception();
                number = Convert.ToInt64(numberStr);
            }
            catch
            {
                number = null;
            }
            
            DateTime minDate = new DateTime(1900,1,1);
            if (startDate < minDate) startDate = minDate;
            if (endDate < minDate) endDate = minDate;
            if (birthdayBegin < minDate) birthdayBegin = minDate;
            if (birthdayEnd < minDate) birthdayEnd = minDate;

            List<ExtendedClaim> list = new List<ExtendedClaim>();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Claim_GetClaimCount", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (userId.HasValue) comm.Parameters.Add(new SqlParameter("UserId", userId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                if (claimStatusId.HasValue) comm.Parameters.Add(new SqlParameter("ClaimStatusId", claimStatusId));
                if (number.HasValue) comm.Parameters.Add(new SqlParameter("Number", number));
                if (startDate.HasValue) comm.Parameters.Add(new SqlParameter("StartDate", startDate));
                if (endDate.HasValue) comm.Parameters.Add(new SqlParameter("EndDate", endDate));
                if (!string.IsNullOrEmpty(lastName)) comm.Parameters.Add(new SqlParameter("LastName", lastName.Trim()));
                if (!string.IsNullOrEmpty(firstName)) comm.Parameters.Add(new SqlParameter("FirstName", firstName.Trim()));
                if (!string.IsNullOrEmpty(middleName)) comm.Parameters.Add(new SqlParameter("MiddleName", middleName.Trim()));
                if (!string.IsNullOrEmpty(udodName)) comm.Parameters.Add(new SqlParameter("UdodName", udodName.Trim()));
                if (!string.IsNullOrEmpty(childUnion)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnion));
                if (!string.IsNullOrEmpty(sectionName)) comm.Parameters.Add(new SqlParameter("SectionName", sectionName));
                if (birthdayBegin.HasValue) comm.Parameters.Add(new SqlParameter("BirthdayBegin", birthdayBegin));
                if (birthdayEnd.HasValue) comm.Parameters.Add(new SqlParameter("BirthdayEnd", birthdayEnd));
                if (birthdayFindType.HasValue) comm.Parameters.Add(new SqlParameter("BirthdayFindType", Convert.ToBoolean(birthdayFindType)));
                if (pagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", pagerIndex));
                if (pagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", pagerLength));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }
            return 0;
        }

        public long? AddClaim(Guid pupilId, DateTime beginDate, long childUnionId, int numYear,  int creatorRoleId, Guid ParentId, bool isReestr)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {

                SqlCommand comm = new SqlCommand("Claim_AddClaim", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("PupilId", pupilId));
                comm.Parameters.Add(new SqlParameter("BeginDate", beginDate));
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                comm.Parameters.Add(new SqlParameter("CreatorRoleId", creatorRoleId));
                comm.Parameters.Add(new SqlParameter("ParentId", ParentId));
                comm.Parameters.Add(new SqlParameter("IsReestr", isReestr));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (long) reader[0];
                }
            }
            return null;
        }

        public long? AddClaim(Guid pupilId, long childUnionId, Guid ParentId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {

                SqlCommand comm = new SqlCommand("Claim_AddESZClaim", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("PupilId", pupilId));
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("ParentId", ParentId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (long)reader[0];
                }
            }
            return null;
        }

        public long? UpdateClaim(long claimId, Guid pupilId, DateTime beginDate, long childUnionId, int claimStatusId, int numYear, bool isReestr)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Claim_UpdateClaim", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ClaimId", claimId));
                comm.Parameters.Add(new SqlParameter("PupilId", pupilId));
                comm.Parameters.Add(new SqlParameter("BeginDate", beginDate));
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("DictClaimStatusId", claimStatusId));
                comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                comm.Parameters.Add(new SqlParameter("IsReestr", isReestr));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (long)reader[0];
                }
            }
            return null;
        }

        public void UpdateClaimTmp(long claimId, long relationId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("update Claim set RelationId=@RelationId where claimId=@ClaimId", conn) { CommandType = CommandType.Text };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ClaimId", claimId));
                comm.Parameters.Add(new SqlParameter("RelationId", relationId));

                comm.ExecuteNonQuery();
                
            }
        }

        public ExtendedClaim GetClaim(long claimId)
        {
            ExtendedClaim item = new ExtendedClaim();
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Claim_GetClaim", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ClaimId", claimId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    item.ClaimId = (long)reader["ClaimId"];
                    item.Number = (long)reader["Number"];
                    item.BeginDate = (DateTime)reader["BeginDate"];
                    item.CreatedDate = (DateTime)reader["CreatedDate"];
                    item.NumYear = (int)reader["PupilNumYear"];
                    item.RelationId = (long) reader["RelationId"];
                    if (reader["IsReestr"] != DBNull.Value)
                        item.IsReestr = (bool)reader["IsReestr"];
                    else item.IsReestr = null;
                    item.ClaimStatus = new ExtendedClaimStatus
                    {
                        ClaimStatusId = (int)reader["DictClaimStatusId"],
                        ClaimStatusName = (string)reader["ClaimStatusName"]
                    };
                    item.Udod = new ExtendedUdod()
                    {
                        UdodId = (long)reader["UdodId"],
                        Name = (string)reader["UdodName"]
                    };
                    item.ChildUnion = new ExtendedChildUnion()
                    {
                        ChildUnionId = (long)reader["ChildUnionId"],
                        Name = (string)reader["ChildUnionName"]
                    };
                    
                    item.Parent = new ExtendedUser
                    {
                        UserId = (Guid)reader["ParentId"],
                        LastName = (string)reader["ParentLastName"],
                        FirstName = (string)reader["ParentFirstName"],
                        MiddleName = (string)reader["ParentMiddleName"],
                        Passport = new ExtendedPassport
                        {
                            UserPassportId = (long)reader["UserPassportId"],
                            Series = (string)reader["PassportSeries"],
                            Number = (string)reader["PassportNumber"],
                            IssueDate = (DateTime)reader["PassportIssueDate"],
                            PassportType = new ExtendedPassportType
                            {
                                PassportTypeId = (int)reader["PassportTypeId"],
                            }
                        },
                        EMail = (string)reader["ParentEMail"],
                        PhoneNumber = (string)reader["ParentPhoneNumber"],
                        ExpressPhoneNumber = (string)reader["ParentExpressPhoneNumber"]
                    };
                    if (reader["DictUserTypeId"] != DBNull.Value)
                        item.Parent.UserType = (int)reader["DictUserTypeId"];

                    item.Pupil = new ExtendedUser
                    {
                        UserId = (Guid)reader["PupilId"],
                        LastName = (string)reader["PupilLastName"],
                        FirstName = (string)reader["PupilFirstName"],
                        MiddleName = (string)reader["PupilMiddleName"],
                        Sex = Convert.ToInt32((bool)reader["PupilSex"]),
                        Birthday = (DateTime)reader["PupilBirthday"],
                        Addresses = new List<ExtendedAddress>(),
                        SchoolName = (string)reader["SchoolName"],
                        ClassName = (string)reader["ClassName"],
                        
                        /*Passport = new ExtendedPassport
                        {
                            UserPassportId = (long)reader["PupilPassportId"],
                            Series = (string)reader["PupilPassportSeries"],
                            Number = (string)reader["PupilPassportNumber"],
                            IssueDate = (DateTime)reader["PupilPassportIssueDate"],
                            PassportType = new ExtendedPassportType
                            {
                                PassportTypeId = (int)reader["PupilPassportTypeId"],
                            }
                        }*/
                    };
                    try
                    {
                        item.Pupil.ReestrCode = ((Int64)reader["ReestrCode"]).ToString();
                    }
                    catch (Exception)
                    {
                        item.Pupil.ReestrCode = "";
                    }
                    
                    if (reader["PupilPassportId"] != DBNull.Value)
                    {
                        item.Pupil.Passport = new ExtendedPassport()
                        {
                            UserPassportId = (long)reader["PupilPassportId"],
                            Series = (string)reader["PupilPassportSeries"],
                            Number = (string)reader["PupilPassportNumber"],
                            IssueDate = (DateTime)reader["PupilPassportIssueDate"],
                            PassportType = new ExtendedPassportType
                            {
                                PassportTypeId = (int)reader["PupilPassportTypeId"],
                            }
                        };
                    }
                    if (reader["AddressId"]!=DBNull.Value)
                    item.Pupil.Addresses.Add(new ExtendedAddress()
                    {
                        AddressId = (long)reader["AddressId"],
                        AddressStr = (string)reader["AddressStr"]
                    });
                    /*item.AgeGroup = new ExtendedAgeGroup()
                    {
                        UdodAgeGroupId = (long)reader["UdodAgeGroupId"],
                        Age = (int)reader["Age"],
                        UdodId = (long)reader["UdodId"],
                        
                        Program = new ExtendedProgram()
                        {
                            ProgramId = (int)reader["ProgramId"],
                            Name = (string)reader["ProgramName"]
                        },
                        Profile = new ExtendedProfile()
                        {
                            ProfileId = (int)reader["ProfileId"],
                            Name = (string)reader["ProfileName"]
                        },
                        Section = new ExtendedSection()
                        {
                            SectionId = (int)reader["SectionId"],
                            Name = (string)reader["SectionName"]
                        }

                    };*/
                    item.ChildUnion = new ExtendedChildUnion()
                    {
                        ChildUnionId = (long)reader["ChildUnionId"],
                        Name = (string)reader["ChildUnionName"],
                        AgeStart = (int)reader["AgeStart"],
                        AgeEnd = (int)reader["AgeEnd"],
                        NumYears = (int)reader["NumYears"],
                        isDeleted = reader["ChildUnionDateEnd"]!=DBNull.Value,
                        Program = new ExtendedProgram()
                        {
                            ProgramId = (int)reader["ProgramId"],
                            Name = (string)reader["ProgramName"]
                        },
                        Profile = new ExtendedProfile()
                        {
                            ProfileId = (int)reader["ProfileId"],
                            Name = (string)reader["ProfileName"]
                        },
                        Section = new ExtendedSection()
                        {
                            SectionId = (int)reader["SectionId"],
                            Name = (string)reader["SectionName"]
                        }
                    };
                    if (reader["CreatorRoleId"] != DBNull.Value)
                        item.CreatorRoleId = (int)reader["CreatorRoleId"];
                }
            }
            return item;
        }

        public List<ExtendedClaimStatus> GetClaimStatuses()
        {
            List<ExtendedClaimStatus> list= new List<ExtendedClaimStatus>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Claim_GetStatuses", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedClaimStatus item= new ExtendedClaimStatus()
                    {
                        ClaimStatusId = (int)reader["DictClaimStatusId"],
                        ClaimStatusName = (string)reader["ClaimStatusName"]
                    };
                    list.Add(item);
                }
            }

            return list;
        }

        public void UpdateClaimStatus(long claimId, int statusId, int? dictTypeBudgetId, int? reasonAnnulId, long? udodGroupId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Claim_UpdateStatus", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ClaimId", claimId));
                comm.Parameters.Add(new SqlParameter("@DictClaimStatusId", statusId));
                //if (dictTypeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("@DictTypeBudgetId", dictTypeBudgetId));
                if (reasonAnnulId.HasValue) comm.Parameters.Add(new SqlParameter("ReasonAnnulId", reasonAnnulId));
                if (udodGroupId.HasValue) comm.Parameters.Add(new SqlParameter("UdodGroupId", udodGroupId));
                comm.ExecuteNonQuery();
            }
        }
    }
}

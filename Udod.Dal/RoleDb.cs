﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Udod.Dal
{
    public class RoleDb :BaseDb
    {
        public List<ExtendedRole> GetRoles()
        {
            List<ExtendedRole> list = new List<ExtendedRole>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetRole", Connection) { CommandType = CommandType.StoredProcedure };
                Connection.Open();
                
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedRole item = new ExtendedRole();
                    item.RoleId = (int)reader["RoleId"];
                    item.Name = (string)reader["Name"];
                    item.ShortName = (string)reader["ShortName"];
                    list.Add(item);
                }

            }

            return list;
        }
        public List<ExtendedRole> GetRolesByUserId(Guid? userId)
        {
            List<ExtendedRole> list = new List<ExtendedRole>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetRole", Connection){CommandType = CommandType.StoredProcedure};
                Connection.Open();
                if (userId.HasValue) comm.Parameters.Add(new SqlParameter("UserId", userId.Value));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedRole item = new ExtendedRole();
                    item.RoleId = (int)reader["RoleId"];
                    item.Name = (string)reader["Name"];
                    item.ShortName = (string)reader["ShortName"];
                    list.Add(item);
                }

            }

            return list;
        }
        public ExtendedRole? GetRole(int roleId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetRole", Connection) { CommandType = CommandType.StoredProcedure }; 
                Connection.Open();
                comm.Parameters.Add(new SqlParameter("RoleId", roleId));
                SqlDataReader reader = comm.ExecuteReader();
                while(reader.Read())
                {
                    ExtendedRole role=  new ExtendedRole();
                    role.RoleId = roleId;
                    role.Name = (string) reader["Name"];
                    role.ShortName = (string) reader["ShortName"];
                    return role;
                }
            }
            return null;
        }
        
    }
}

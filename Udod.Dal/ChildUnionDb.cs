﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.DataSetExtensions;
using System.Configuration;

namespace Udod.Dal
{
    public class ChildUnionDb : BaseDb
    {
        private int ChildUnionsCount;

        public ChildUnionDb() { }

        public List<ExtendedChildUnion> GetChildUnions(int? cityId, long? udodId, int? programId, int? profileId, int? sectionId, int? typeBudgetId, string sectionName, string profileName, string programName, string childUnionName, string cityCode, string rayonCode, int? metroId, string udodName, int? rayonId)
        {
            List<ExtendedChildUnion> list = new List<ExtendedChildUnion>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetChildUnionList", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (programId.HasValue) comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                if (profileId.HasValue) comm.Parameters.Add(new SqlParameter("ProfileId", profileId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                if (typeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                if (!string.IsNullOrEmpty(programName)) comm.Parameters.Add(new SqlParameter("ProgramName", programName));
                if (!string.IsNullOrEmpty(profileName)) comm.Parameters.Add(new SqlParameter("ProfileName", profileName));
                if (!string.IsNullOrEmpty(sectionName)) comm.Parameters.Add(new SqlParameter("SectionName", sectionName));
                if (!string.IsNullOrEmpty(childUnionName)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnionName));
                if (!string.IsNullOrEmpty(cityCode)) comm.Parameters.Add(new SqlParameter("CityCode", cityCode));
                if (!string.IsNullOrEmpty(rayonCode)) comm.Parameters.Add(new SqlParameter("RayonCode", rayonCode));
                if (rayonId.HasValue) comm.Parameters.Add(new SqlParameter("RayonId", rayonId));
                if (metroId.HasValue) comm.Parameters.Add(new SqlParameter("MetroId", metroId));
                if (!string.IsNullOrEmpty(udodName)) comm.Parameters.Add(new SqlParameter("UdodName", udodName));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long _childUnionId = (long)reader["ChildUnionId"];
                    long? _ageGroupId = null;
                    if (reader["UdodAgeGroupId"] != DBNull.Value) _ageGroupId = (long?)reader["UdodAgeGroupId"];

                    long? _addressId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long?)reader["AddressId"];

                    int? _budgetTypeId = null;
                    if (reader["DictTypeBudgetId"] != DBNull.Value) _budgetTypeId = (int?)reader["DictTypeBudgetId"];

                    int? _educationTypeId = null;
                    if (reader["EducationTypeId"] != DBNull.Value) _educationTypeId = (int?)reader["EducationTypeId"];

                    ExtendedChildUnion last = list.LastOrDefault(i => i.ChildUnionId == _childUnionId);

                    if (null != last)
                    {
                        if (_ageGroupId.HasValue && last.AgeGroups.Count(i => i.UdodAgeGroupId == _ageGroupId) == 0)
                        {
                            var itemAgeGroup = new ExtendedAgeGroup();
                            //itemAgeGroup.AgeEnd = (int)reader["AgeEnd"];
                            itemAgeGroup.Age = (int)reader["Age"];
                            itemAgeGroup.Section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                                SectionId = (int)reader["SectionId"],
                                ProfileId = (int)reader["ProfileId"]
                            };
                            //itemAgeGroup.TypeBudget = new ExtendedTypeBudget()
                            //{
                            //    DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                            //    TypeBudgetName = (string)reader["TypeBudgetName"]
                            //};
                            itemAgeGroup.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                            itemAgeGroup.UdodId = (long)reader["UdodId"];
                            //itemAgeGroup.EducationType = new ExtendedEducationType()
                            //{
                            //    EducationTypeId = (int)reader["EducationTypeId"],
                            //    EducationTypeName = (string)reader["EducationTypeName"]
                            //};
                            itemAgeGroup.LessonLength = (int)reader["LessonLength"];
                            itemAgeGroup.BreakLength = (int)reader["BreakLength"];
                            itemAgeGroup.LessonsInWeek = (int)reader["LessonsInWeek"];
                            itemAgeGroup.Program = new ExtendedProgram()
                            {
                                ProgramId = (int)reader["ProgramId"],
                                Name = (string)reader["ProgramName"]
                            };
                            //itemAgeGroup.TeacherFio = (string)reader["Fio"];

                            (last.AgeGroups as List<ExtendedAgeGroup>).Add(itemAgeGroup);
                        }

                        if (_addressId.HasValue && last.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];
                            (last.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        if (_budgetTypeId.HasValue && last.BudgetTypes.Count(i => i.DictTypeBudgetId == _budgetTypeId) == 0)
                        {
                            var itemBudgetType = new ExtendedTypeBudget();
                            itemBudgetType.DictTypeBudgetId = (int)reader["DictTypeBudgetId"];
                            itemBudgetType.TypeBudgetName = (string)reader["TypeBudgetName"];

                            (last.BudgetTypes as List<ExtendedTypeBudget>).Add(itemBudgetType);
                        }

                        if (_educationTypeId.HasValue && last.EducationTypes.Count(i => i.EducationTypeId == _educationTypeId) == 0)
                        {
                            var itemEducationType = new ExtendedEducationType();
                            itemEducationType.EducationTypeId = (int)reader["EducationTypeId"];
                            itemEducationType.EducationTypeName = (string)reader["EducationTypeName"];

                            (last.EducationTypes as List<ExtendedEducationType>).Add(itemEducationType);
                        }
                    }
                    else
                    {
                        ExtendedChildUnion item = new ExtendedChildUnion();
                        item.ChildUnionId = (long)reader["ChildUnionId"];
                        item.Name = (string)reader["Name"];
                        item.AgeStart = (int) reader["AgeStart"];
                        item.AgeEnd = (int) reader["AgeEnd"];
                        item.NumYears = (int) reader["NumYears"];
                        item.UdodId = (long)reader["UdodId"];
                        item.Section = new ExtendedSection()
                        {
                            SectionId = (int)reader["SectionId"],
                            Name = (string)reader["SectionName"]
                        };
                        item.Profile = new ExtendedProfile()
                        {
                            ProgramId = (int)reader["ProfileId"],
                            Name = (string)reader["ProfileName"]
                        };
                        item.Program = new ExtendedProgram()
                        {
                            ProgramId = (int)reader["ProgramId"],
                            Name = (string)reader["ProgramName"]
                        };
                        item.ProgramCategory = new ExtendedProgramCategory()
                        {
                            ProgramCategoryId = (int)reader["ProgramCategoryId"],
                            ProgramCategoryName = (string)reader["ProgramCategoryName"]
                        };
                        item.UdodSectionId = (long)reader["UdodSectionId"];
                        item.Comment = (string)reader["Comment"];
                        item.NumYears = (int) reader["NumYears"];
                        item.IsActive = (bool) reader["IsActive"];
                        item.AgeGroups = new List<ExtendedAgeGroup>();
                        item.Addresses = new List<ExtendedAddress>();
                        item.BudgetTypes = new List<ExtendedTypeBudget>();
                        item.EducationTypes = new List<ExtendedEducationType>();
                        if (_ageGroupId.HasValue)
                        {
                            var itemAgeGroup = new ExtendedAgeGroup();
                            //itemAgeGroup.AgeEnd = (int)reader["AgeEnd"];
                            itemAgeGroup.Age = (int)reader["AgeStart"];
                            itemAgeGroup.Section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                                SectionId = (int)reader["SectionId"],
                                ProfileId = (int)reader["ProfileId"]
                            };
                            //itemAgeGroup.TypeBudget = new ExtendedTypeBudget()
                            //{
                            //    DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                            //    TypeBudgetName = (string)reader["TypeBudgetName"]
                            //};
                            itemAgeGroup.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                            itemAgeGroup.UdodId = (long)reader["UdodId"];
                            //itemAgeGroup.EducationType = new ExtendedEducationType()
                            //{
                            //    EducationTypeId = (int)reader["EducationTypeId"],
                            //    EducationTypeName = (string)reader["EducationTypeName"]
                            //};
                            itemAgeGroup.LessonLength = (int)reader["LessonLength"];
                            itemAgeGroup.BreakLength = (int)reader["BreakLength"];
                            itemAgeGroup.LessonsInWeek = (int)reader["LessonsInWeek"];
                            itemAgeGroup.Program = new ExtendedProgram()
                            {
                                ProgramId = (int)reader["ProgramId"],
                                Name = (string)reader["ProgramName"]
                            };
                            //itemAgeGroup.TeacherFio = (string)reader["Fio"];

                            (item.AgeGroups as List<ExtendedAgeGroup>).Add(itemAgeGroup);

                            itemAgeGroup = new ExtendedAgeGroup();
                            //itemAgeGroup.AgeEnd = (int)reader["AgeEnd"];
                            itemAgeGroup.Age = (int)reader["AgeEnd"];
                            itemAgeGroup.Section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                                SectionId = (int)reader["SectionId"],
                                ProfileId = (int)reader["ProfileId"]
                            };
                            //itemAgeGroup.TypeBudget = new ExtendedTypeBudget()
                            //{
                            //    DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                            //    TypeBudgetName = (string)reader["TypeBudgetName"]
                            //};
                            itemAgeGroup.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                            itemAgeGroup.UdodId = (long)reader["UdodId"];
                            //itemAgeGroup.EducationType = new ExtendedEducationType()
                            //{
                            //    EducationTypeId = (int)reader["EducationTypeId"],
                            //    EducationTypeName = (string)reader["EducationTypeName"]
                            //};
                            itemAgeGroup.LessonLength = (int)reader["LessonLength"];
                            itemAgeGroup.BreakLength = (int)reader["BreakLength"];
                            itemAgeGroup.LessonsInWeek = (int)reader["LessonsInWeek"];
                            itemAgeGroup.Program = new ExtendedProgram()
                            {
                                ProgramId = (int)reader["ProgramId"],
                                Name = (string)reader["ProgramName"]
                            };
                            //itemAgeGroup.TeacherFio = (string)reader["Fio"];

                            (item.AgeGroups as List<ExtendedAgeGroup>).Add(itemAgeGroup);
                        }

                        if (_addressId.HasValue)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];
                            
                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        if (_budgetTypeId.HasValue)
                        {
                            var itemBudgetType = new ExtendedTypeBudget();
                            itemBudgetType.DictTypeBudgetId = (int)reader["DictTypeBudgetId"];
                            itemBudgetType.TypeBudgetName = (string)reader["TypeBudgetName"];

                            (item.BudgetTypes as List<ExtendedTypeBudget>).Add(itemBudgetType);
                        }

                        if (_educationTypeId.HasValue)
                        {
                            var itemEducationType = new ExtendedEducationType();
                            itemEducationType.EducationTypeId = (int)reader["EducationTypeId"];
                            itemEducationType.EducationTypeName = (string)reader["EducationTypeName"];

                            (item.EducationTypes as List<ExtendedEducationType>).Add(itemEducationType);
                        }

                        list.Add(item);
                    }
                }
            }

            ChildUnionsCount = list.Count;
            return list;
        }

        public List<ExtendedChildUnion> GetChildUnionsForExport(long? udodId)
        {
            List<ExtendedChildUnion> list = new List<ExtendedChildUnion>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetChildUnionsForExport", Connection) { CommandType = CommandType.StoredProcedure };
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long _childUnionId = (long)reader["ChildUnionId"];
                    long? _ageGroupId = null;
                    if (reader["UdodAgeGroupId"] != DBNull.Value) _ageGroupId = (long?)reader["UdodAgeGroupId"];

                    Guid? _teacherId = null;
                    if (reader["TeacherId"] != DBNull.Value) _teacherId = (Guid?)reader["TeacherId"];

                    int? _budgetTypeId = null;
                    if (reader["DictTypeBudgetId"] != DBNull.Value) _budgetTypeId = (int?)reader["DictTypeBudgetId"];

                    int? _educationTypeId = null;
                    if (reader["EducationTypeId"] != DBNull.Value) _educationTypeId = (int?)reader["EducationTypeId"];

                    ExtendedChildUnion last = list.LastOrDefault(i => i.ChildUnionId == _childUnionId);

                    if (null != last)
                    {
                        if (_ageGroupId.HasValue && last.AgeGroups.Count(i => i.UdodAgeGroupId == _ageGroupId) == 0)
                        {
                            var itemAgeGroup = new ExtendedAgeGroup();
                            //itemAgeGroup.AgeEnd = (int)reader["AgeEnd"];
                            //itemAgeGroup.Age = (int)reader["Age"];
                            itemAgeGroup.AgeStart = (int)reader["AgeGroupAgeStart"];
                            itemAgeGroup.AgeEnd = (int)reader["AgeGroupAgeEnd"];
                            itemAgeGroup.NumYearStart = (int)reader["NumYearStart"];
                            itemAgeGroup.NumYearEnd = (int)reader["NumYearEnd"];
                            itemAgeGroup.Section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                            };
                            //itemAgeGroup.TypeBudget = new ExtendedTypeBudget()
                            //{
                            //    DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                            //    TypeBudgetName = (string)reader["TypeBudgetName"]
                            //};
                            itemAgeGroup.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                            itemAgeGroup.UdodId = (long)reader["UdodId"];
                            //itemAgeGroup.EducationType = new ExtendedEducationType()
                            //{
                            //    EducationTypeId = (int)reader["EducationTypeId"],
                            //    EducationTypeName = (string)reader["EducationTypeName"]
                            //};
                            itemAgeGroup.LessonLength = (int)reader["LessonLength"];
                            itemAgeGroup.BreakLength = (int)reader["BreakLength"];
                            itemAgeGroup.LessonsInWeek = (int)reader["LessonsInWeek"];
                            itemAgeGroup.Program = new ExtendedProgram()
                            {
                                Name = (string)reader["ProgramName"]
                            };
                            //itemAgeGroup.TeacherFio = (string)reader["Fio"];

                            (last.AgeGroups as List<ExtendedAgeGroup>).Add(itemAgeGroup);
                        }

                        if (_teacherId.HasValue && last.Teachers.Count(i => i.UserId == _teacherId) == 0)
                        {
                            var itemTeacher = new ExtendedTeacher();
                            itemTeacher.LastName = (string)reader["LastName"];
                            itemTeacher.FirstName = (string)reader["FirstName"];
                            itemTeacher.MiddleName = (string)reader["MiddleName"];
                            itemTeacher.UserId = _teacherId.Value;
                            
                            (last.Teachers).Add(itemTeacher);
                        }

                        if (_budgetTypeId.HasValue && last.BudgetTypes.Count(i => i.DictTypeBudgetId == _budgetTypeId) == 0)
                        {
                            var itemBudgetType = new ExtendedTypeBudget();
                            itemBudgetType.DictTypeBudgetId = (int)reader["DictTypeBudgetId"];
                            itemBudgetType.TypeBudgetName = (string)reader["TypeBudgetName"];

                            (last.BudgetTypes as List<ExtendedTypeBudget>).Add(itemBudgetType);
                        }

                        if (_educationTypeId.HasValue && last.EducationTypes.Count(i => i.EducationTypeId == _educationTypeId) == 0)
                        {
                            var itemEducationType = new ExtendedEducationType();
                            itemEducationType.EducationTypeId = (int)reader["EducationTypeId"];
                            itemEducationType.EducationTypeName = (string)reader["EducationTypeName"];

                            (last.EducationTypes as List<ExtendedEducationType>).Add(itemEducationType);
                        }
                    }
                    else
                    {
                        ExtendedChildUnion item = new ExtendedChildUnion();
                        item.ChildUnionId = (long)reader["ChildUnionId"];
                        item.Name = (string)reader["Name"];
                        item.AgeStart = (int)reader["AgeStart"];
                        item.AgeEnd = (int)reader["AgeEnd"];
                        item.NumYears = (int)reader["NumYears"];
                        item.UdodId = (long)reader["UdodId"];
                        item.Section = new ExtendedSection()
                        {
                            Name = (string)reader["SectionName"]
                        };
                        item.Profile = new ExtendedProfile()
                        {
                            Name = (string)reader["ProfileName"]
                        };
                        item.Program = new ExtendedProgram()
                        {
                            Name = (string)reader["ProgramName"]
                        };
                        item.ProgramCategory = new ExtendedProgramCategory()
                        {
                            ProgramCategoryName = (string)reader["ProgramCategoryName"]
                        };
                        item.UdodSectionId = (long)reader["UdodSectionId"];
                        item.NumYears = (int)reader["NumYears"];
                        if (reader["IsPublicPortal"] != DBNull.Value)
                            item.isPublicPortal = (bool)reader["IsPublicPortal"];
                        else item.isPublicPortal = false;
                        if (reader["IsReception"] != DBNull.Value)
                            item.isReception = (bool)reader["IsReception"];
                        else item.isReception = false;
                        item.AgeGroups = new List<ExtendedAgeGroup>();
                        item.Teachers = new List<ExtendedTeacher>();
                        item.BudgetTypes = new List<ExtendedTypeBudget>();
                        item.EducationTypes = new List<ExtendedEducationType>();
                        if (_ageGroupId.HasValue)
                        {
                            var itemAgeGroup = new ExtendedAgeGroup();
                            //itemAgeGroup.AgeEnd = (int)reader["AgeEnd"];
                            itemAgeGroup.AgeStart = (int)reader["AgeGroupAgeStart"];
                            itemAgeGroup.AgeEnd = (int)reader["AgeGroupAgeEnd"];
                            itemAgeGroup.NumYearStart = (int)reader["NumYearStart"];
                            itemAgeGroup.NumYearEnd = (int)reader["NumYearEnd"];
                            itemAgeGroup.Section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                            };
                            //itemAgeGroup.TypeBudget = new ExtendedTypeBudget()
                            //{
                            //    DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                            //    TypeBudgetName = (string)reader["TypeBudgetName"]
                            //};
                            itemAgeGroup.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                            itemAgeGroup.UdodId = (long)reader["UdodId"];
                            //itemAgeGroup.EducationType = new ExtendedEducationType()
                            //{
                            //    EducationTypeId = (int)reader["EducationTypeId"],
                            //    EducationTypeName = (string)reader["EducationTypeName"]
                            //};
                            itemAgeGroup.LessonLength = (int)reader["LessonLength"];
                            itemAgeGroup.BreakLength = (int)reader["BreakLength"];
                            itemAgeGroup.LessonsInWeek = (int)reader["LessonsInWeek"];
                            itemAgeGroup.Program = new ExtendedProgram()
                            {
                                Name = (string)reader["ProgramName"]
                            };
                            //itemAgeGroup.TeacherFio = (string)reader["Fio"];

                            (item.AgeGroups as List<ExtendedAgeGroup>).Add(itemAgeGroup);
                        }

                        if (_teacherId.HasValue)
                        {
                            var itemTeacher = new ExtendedTeacher();
                            itemTeacher.LastName = (string)reader["LastName"];
                            itemTeacher.FirstName = (string)reader["FirstName"];
                            itemTeacher.MiddleName = (string)reader["MiddleName"];
                            itemTeacher.UserId = _teacherId.Value;
                            
                            (item.Teachers).Add(itemTeacher);
                        }

                        if (_budgetTypeId.HasValue)
                        {
                            var itemBudgetType = new ExtendedTypeBudget();
                            itemBudgetType.DictTypeBudgetId = (int)reader["DictTypeBudgetId"];
                            itemBudgetType.TypeBudgetName = (string)reader["TypeBudgetName"];

                            (item.BudgetTypes as List<ExtendedTypeBudget>).Add(itemBudgetType);
                        }

                        if (_educationTypeId.HasValue)
                        {
                            var itemEducationType = new ExtendedEducationType();
                            itemEducationType.EducationTypeId = (int)reader["EducationTypeId"];
                            itemEducationType.EducationTypeName = (string)reader["EducationTypeName"];

                            (item.EducationTypes as List<ExtendedEducationType>).Add(itemEducationType);
                        }

                        list.Add(item);
                    }
                }
            }

            return list;
        }

        public List<ExtendedChildUnion> GetDeletedChildUnions(DateTime deletedDate)
        {
            List<ExtendedChildUnion> list = new List<ExtendedChildUnion>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("ChildUnion_GetDeletedChildUnions", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("DeletedDate", deletedDate));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long _childUnionId = (long)reader["ChildUnionId"];
                    ExtendedChildUnion item= new ExtendedChildUnion(){ChildUnionId = _childUnionId};
                    list.Add(item);
                }
            }

            ChildUnionsCount = list.Count;
            return list;
        }

        public List<ExtendedChildUnion> GetChildUnionsShort(int? cityId, long? udodId, int? programId, int? profileId, int? sectionId, int? typeBudgetId, string sectionName, string profileName, string programName, string childUnionName, string cityCode, string rayonCode, int? metroId, string udodName, int? rayonId, Guid? teacherId, bool isReception)
        {
            List<ExtendedChildUnion> list = new List<ExtendedChildUnion>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetChildUnionShort", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (programId.HasValue) comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                if (profileId.HasValue) comm.Parameters.Add(new SqlParameter("ProfileId", profileId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                if (typeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                if (!string.IsNullOrEmpty(programName)) comm.Parameters.Add(new SqlParameter("ProgramName", programName));
                if (!string.IsNullOrEmpty(profileName)) comm.Parameters.Add(new SqlParameter("ProfileName", profileName));
                if (!string.IsNullOrEmpty(sectionName)) comm.Parameters.Add(new SqlParameter("SectionName", sectionName));
                if (!string.IsNullOrEmpty(childUnionName)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnionName));
                if (!string.IsNullOrEmpty(cityCode)) comm.Parameters.Add(new SqlParameter("CityCode", cityCode));
                if (!string.IsNullOrEmpty(rayonCode)) comm.Parameters.Add(new SqlParameter("RayonCode", rayonCode));
                if (metroId.HasValue) comm.Parameters.Add(new SqlParameter("MetroId", metroId));
                if (rayonId.HasValue) comm.Parameters.Add(new SqlParameter("RayonId", rayonId));
                if (teacherId.HasValue) comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                if (!string.IsNullOrEmpty(udodName)) comm.Parameters.Add(new SqlParameter("UdodName", udodName));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long _childUnionId = (long)reader["ChildUnionId"];
                    Guid? _teacherid = null;
                    if (reader["TeacherId"] != DBNull.Value) _teacherid = (Guid?)reader["TeacherId"];

                    int? _budgetTypeId = null;
                    if (reader["DictTypeBudgetId"] != DBNull.Value) _budgetTypeId = (int?)reader["DictTypeBudgetId"];

                    ExtendedChildUnion last = list.LastOrDefault(i => i.ChildUnionId == _childUnionId);
                    if (null != last)
                    {
                        if (_teacherid.HasValue && last.Teachers.Count(i => i.UserId == _teacherid) == 0)
                        {
                            last.Teachers.Add(new ExtendedTeacher(){
                                UserId = _teacherid.Value, 
                                FirstName = (string)reader["TeacherFirstName"],
                                LastName = (string)reader["TeacherLastName"],
                                MiddleName = (string)reader["TeacherMiddleName"]
                            });
                        }

                        if (_budgetTypeId.HasValue && last.BudgetTypes.Count(i => i.DictTypeBudgetId == _budgetTypeId) == 0)
                        {
                            last.BudgetTypes.Add(new ExtendedTypeBudget()
                                                {
                                                    DictTypeBudgetId= _budgetTypeId.Value,
                                                    TypeBudgetName = (string)reader["TypeBudgetName"]
                                                });
                        }
                    }
                    else
                    {
                        ExtendedChildUnion item = new ExtendedChildUnion();
                        item.ChildUnionId = (long) reader["ChildUnionId"];
                        item.Name = (string) reader["Name"];
                        item.AgeStart = (int) reader["AgeStart"];
                        item.AgeEnd = (int) reader["AgeEnd"];
                        item.NumYears = (int) reader["NumYears"];
                        item.UdodId = (long) reader["UdodId"];
                        item.IsActive = (bool) reader["IsActive"];
                        item.Section = new ExtendedSection()
                                           {
                                               SectionId = (int) reader["SectionId"],
                                               Name = (string) reader["SectionName"]
                                           };
                        item.Profile = new ExtendedProfile()
                                           {
                                               ProgramId = (int) reader["ProfileId"],
                                               Name = (string) reader["ProfileName"]
                                           };
                        item.Program = new ExtendedProgram()
                                           {
                                               ProgramId = (int) reader["ProgramId"],
                                               Name = (string) reader["ProgramName"]
                                           };
                        item.ProgramCategory = new ExtendedProgramCategory()
                                                   {
                                                       ProgramCategoryId = (int) reader["ProgramCategoryId"],
                                                       ProgramCategoryName = (string) reader["ProgramCategoryName"]
                                                   };
                        item.UdodSectionId = (long) reader["UdodSectionId"];
                        item.Comment = (string) reader["Comment"];
                        item.NumYears = (int) reader["NumYears"];
                        item.Addresses = new List<ExtendedAddress>();
                        if (reader["AddressStr"] != DBNull.Value)
                            item.Addresses.Add(new ExtendedAddress()
                                                   {
                                                       AddressStr = (string) reader["AddressStr"]
                                                   });
                        item.Teachers = new List<ExtendedTeacher>();
                        if (_teacherid.HasValue)
                        {
                            item.Teachers.Add(new ExtendedTeacher()
                            {
                                UserId = _teacherid.Value,
                                FirstName = (string)reader["TeacherFirstName"],
                                LastName = (string)reader["TeacherLastName"],
                                MiddleName = (string)reader["TeacherMiddleName"]
                            });
                        }
                        item.BudgetTypes = new List<ExtendedTypeBudget>();
                        if (_budgetTypeId.HasValue)
                        {
                            item.BudgetTypes.Add(new ExtendedTypeBudget()
                            {
                                DictTypeBudgetId = _budgetTypeId.Value,
                                TypeBudgetName = (string)reader["TypeBudgetName"]
                            });
                        }
                        item.isReception = (bool) reader["IsReception"];
                        if ((isReception && item.isReception) || (!isReception))
                        list.Add(item);
                    }
                }
            }

            ChildUnionsCount = list.Count;
            return list;
        }

        public List<ExtendedChildUnion> GetChildUnionsShortForService(int? cityId, long? udodId, int? programId, int? profileId, int? sectionId, int? typeBudgetId, string sectionName, string profileName, string programName, string childUnionName, string cityCode, string rayonCode, int? metroId, string udodName, int? rayonId)
        {
            List<ExtendedChildUnion> list = new List<ExtendedChildUnion>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetChildUnionShortForService", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (programId.HasValue) comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                if (profileId.HasValue) comm.Parameters.Add(new SqlParameter("ProfileId", profileId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                if (typeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                if (!string.IsNullOrEmpty(programName)) comm.Parameters.Add(new SqlParameter("ProgramName", programName));
                if (!string.IsNullOrEmpty(profileName)) comm.Parameters.Add(new SqlParameter("ProfileName", profileName));
                if (!string.IsNullOrEmpty(sectionName)) comm.Parameters.Add(new SqlParameter("SectionName", sectionName));
                if (!string.IsNullOrEmpty(childUnionName)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnionName));
                if (!string.IsNullOrEmpty(cityCode)) comm.Parameters.Add(new SqlParameter("CityCode", cityCode));
                if (!string.IsNullOrEmpty(rayonCode)) comm.Parameters.Add(new SqlParameter("RayonCode", rayonCode));
                if (metroId.HasValue) comm.Parameters.Add(new SqlParameter("MetroId", metroId));
                if (rayonId.HasValue) comm.Parameters.Add(new SqlParameter("RayonId", rayonId));
                if (!string.IsNullOrEmpty(udodName)) comm.Parameters.Add(new SqlParameter("UdodName", udodName));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long _childUnionId = (long)reader["ChildUnionId"];
                    Guid? _teacherid = null;
                    if (reader["TeacherId"] != DBNull.Value) _teacherid = (Guid?)reader["TeacherId"];

                    int? _budgetTypeId = null;
                    if (reader["DictTypeBudgetId"] != DBNull.Value) _budgetTypeId = (int?)reader["DictTypeBudgetId"];

                    ExtendedChildUnion last = list.LastOrDefault(i => i.ChildUnionId == _childUnionId);
                    if (null != last)
                    {
                        if (_teacherid.HasValue && last.Teachers.Count(i => i.UserId == _teacherid) == 0)
                        {
                            last.Teachers.Add(new ExtendedTeacher()
                            {
                                UserId = _teacherid.Value,
                                FirstName = (string)reader["TeacherFirstName"],
                                LastName = (string)reader["TeacherLastName"],
                                MiddleName = (string)reader["TeacherMiddleName"]
                            });
                        }

                        if (_budgetTypeId.HasValue && last.BudgetTypes.Count(i => i.DictTypeBudgetId == _budgetTypeId) == 0)
                        {
                            last.BudgetTypes.Add(new ExtendedTypeBudget()
                            {
                                DictTypeBudgetId = _budgetTypeId.Value,
                                TypeBudgetName = (string)reader["TypeBudgetName"]
                            });
                        }
                    }
                    else
                    {
                        ExtendedChildUnion item = new ExtendedChildUnion();
                        item.ChildUnionId = (long)reader["ChildUnionId"];
                        item.Name = (string)reader["Name"];
                        item.AgeStart = (int)reader["AgeStart"];
                        item.AgeEnd = (int)reader["AgeEnd"];
                        item.NumYears = (int)reader["NumYears"];
                        item.UdodId = (long)reader["UdodId"];
                        item.IsActive = (bool)reader["IsActive"];
                        item.Section = new ExtendedSection()
                        {
                            SectionId = (int)reader["SectionId"],
                            Name = (string)reader["SectionName"]
                        };
                        item.Profile = new ExtendedProfile()
                        {
                            ProgramId = (int)reader["ProfileId"],
                            Name = (string)reader["ProfileName"]
                        };
                        item.Program = new ExtendedProgram()
                        {
                            ProgramId = (int)reader["ProgramId"],
                            Name = (string)reader["ProgramName"]
                        };
                        item.ProgramCategory = new ExtendedProgramCategory()
                        {
                            ProgramCategoryId = (int)reader["ProgramCategoryId"],
                            ProgramCategoryName = (string)reader["ProgramCategoryName"]
                        };
                        item.UdodSectionId = (long)reader["UdodSectionId"];
                        item.Comment = (string)reader["Comment"];
                        item.NumYears = (int)reader["NumYears"];
                        item.Addresses = new List<ExtendedAddress>();
                        if (reader["AddressStr"] != DBNull.Value)
                            item.Addresses.Add(new ExtendedAddress()
                            {
                                AddressStr = (string)reader["AddressStr"]
                            });
                        item.Teachers = new List<ExtendedTeacher>();
                        if (_teacherid.HasValue)
                        {
                            item.Teachers.Add(new ExtendedTeacher()
                            {
                                UserId = _teacherid.Value,
                                FirstName = (string)reader["TeacherFirstName"],
                                LastName = (string)reader["TeacherLastName"],
                                MiddleName = (string)reader["TeacherMiddleName"]
                            });
                        }
                        item.BudgetTypes = new List<ExtendedTypeBudget>();
                        if (_budgetTypeId.HasValue)
                        {
                            item.BudgetTypes.Add(new ExtendedTypeBudget()
                            {
                                DictTypeBudgetId = _budgetTypeId.Value,
                                TypeBudgetName = (string)reader["TypeBudgetName"]
                            });
                        }

                        list.Add(item);
                    }
                }
            }

            ChildUnionsCount = list.Count;
            return list;
        }

        public int GetChildUnionsShortCountForService(int? cityId, long? udodId, int? programId, int? profileId, int? sectionId, int? typeBudgetId, string sectionName, string profileName, string programName, string childUnionName, string cityCode, string rayonCode, int? metroId, string udodName, int? rayonId)
        {
            int countChildUnion = 0;
            
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetChildUnionShortCountForService", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (programId.HasValue) comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                if (profileId.HasValue) comm.Parameters.Add(new SqlParameter("ProfileId", profileId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                if (typeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                if (!string.IsNullOrEmpty(programName)) comm.Parameters.Add(new SqlParameter("ProgramName", programName));
                if (!string.IsNullOrEmpty(profileName)) comm.Parameters.Add(new SqlParameter("ProfileName", profileName));
                if (!string.IsNullOrEmpty(sectionName)) comm.Parameters.Add(new SqlParameter("SectionName", sectionName));
                if (!string.IsNullOrEmpty(childUnionName)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnionName));
                if (!string.IsNullOrEmpty(cityCode)) comm.Parameters.Add(new SqlParameter("CityCode", cityCode));
                if (!string.IsNullOrEmpty(rayonCode)) comm.Parameters.Add(new SqlParameter("RayonCode", rayonCode));
                if (metroId.HasValue) comm.Parameters.Add(new SqlParameter("MetroId", metroId));
                if (rayonId.HasValue) comm.Parameters.Add(new SqlParameter("RayonId", rayonId));
                if (!string.IsNullOrEmpty(udodName)) comm.Parameters.Add(new SqlParameter("UdodName", udodName));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    countChildUnion = (int) reader[0];
                }
            }
            
            
            return countChildUnion;
        }

        public List<ExtendedChildUnion> GetChildUnionsPaging(int? cityId, long? udodId, int? programId, int? profileId, int? sectionId, int? typeBudgetId, string sectionName, string profileName, string programName, string childUnionName, string cityCode, string rayonCode, int? metroId, string udodName, int? rayonId, int? pagerIndex, int? pagerLength, string orderBy, int? ShowIsDOEnabled, bool? isActive, bool isPublicPortal, bool isReception, long? childUnionId)
        {
            List<ExtendedChildUnion> list = new List<ExtendedChildUnion>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetChildUnionPaging", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (programId.HasValue) comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                if (profileId.HasValue) comm.Parameters.Add(new SqlParameter("ProfileId", profileId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                if (typeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                if (!string.IsNullOrEmpty(programName)) comm.Parameters.Add(new SqlParameter("ProgramName", programName));
                if (!string.IsNullOrEmpty(profileName)) comm.Parameters.Add(new SqlParameter("ProfileName", profileName));
                if (!string.IsNullOrEmpty(sectionName)) comm.Parameters.Add(new SqlParameter("SectionName", sectionName));
                if (!string.IsNullOrEmpty(childUnionName)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnionName));
                if (!string.IsNullOrEmpty(cityCode)) comm.Parameters.Add(new SqlParameter("CityCode", cityCode));
                if (!string.IsNullOrEmpty(rayonCode)) comm.Parameters.Add(new SqlParameter("RayonCode", rayonCode));
                if (metroId.HasValue) comm.Parameters.Add(new SqlParameter("MetroId", metroId));
                if (rayonId.HasValue) comm.Parameters.Add(new SqlParameter("RayonId", rayonId));
                if (!string.IsNullOrEmpty(udodName)) comm.Parameters.Add(new SqlParameter("UdodName", udodName));
                if (pagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", pagerIndex));
                if (pagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", pagerLength));
                if (ShowIsDOEnabled.HasValue) comm.Parameters.Add(new SqlParameter("ShowIsDOEnabled", ShowIsDOEnabled));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId.Value));
                if (isActive.HasValue) comm.Parameters.Add(new SqlParameter("IsActive", isActive));
                comm.Parameters.Add(new SqlParameter("IsPublicPortal", isPublicPortal));
                comm.Parameters.Add(new SqlParameter("IsReception", isReception));
                if (!string.IsNullOrEmpty(orderBy)) comm.Parameters.Add(new SqlParameter("OrderBy", orderBy));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long _childUnionId = (long)reader["ChildUnionId"];
                    Guid? _teacherid = null;
                    if (reader["TeacherId"] != DBNull.Value) _teacherid = (Guid?)reader["TeacherId"];

                    int? _budgetTypeId = null;
                    if (reader["DictTypeBudgetId"] != DBNull.Value) _budgetTypeId = (int?)reader["DictTypeBudgetId"];

                    ExtendedChildUnion last = list.LastOrDefault(i => i.ChildUnionId == _childUnionId);
                    if (null != last)
                    {
                        if (_teacherid.HasValue && last.Teachers.Count(i => i.UserId == _teacherid) == 0)
                        {
                            last.Teachers.Add(new ExtendedTeacher()
                            {
                                UserId = _teacherid.Value,
                                FirstName = (string)reader["TeacherFirstName"],
                                LastName = (string)reader["TeacherLastName"],
                                MiddleName = (string)reader["TeacherMiddleName"]
                            });
                        }

                        if (_budgetTypeId.HasValue && last.BudgetTypes.Count(i => i.DictTypeBudgetId == _budgetTypeId) == 0)
                        {
                            last.BudgetTypes.Add(new ExtendedTypeBudget()
                            {
                                DictTypeBudgetId = _budgetTypeId.Value,
                                TypeBudgetName = (string)reader["TypeBudgetName"]
                            });
                        }
                    }
                    else
                    {
                        ExtendedChildUnion item = new ExtendedChildUnion();
                        item.ChildUnionId = (long)reader["ChildUnionId"];
                        item.Name = (string)reader["Name"];
                        item.AgeStart = (int)reader["AgeStart"];
                        item.AgeEnd = (int)reader["AgeEnd"];
                        item.NumYears = (int)reader["NumYears"];
                        item.UdodId = (long)reader["UdodId"];
                        item.IsActive = (bool)reader["IsActive"];
                        item.Section = new ExtendedSection()
                        {
                            SectionId = (int)reader["SectionId"],
                            Name = (string)reader["SectionName"]
                        };
                        item.Profile = new ExtendedProfile()
                        {
                            ProgramId = (int)reader["ProfileId"],
                            Name = (string)reader["ProfileName"]
                        };
                        item.Program = new ExtendedProgram()
                        {
                            ProgramId = (int)reader["ProgramId"],
                            Name = (string)reader["ProgramName"]
                        };
                        item.ProgramCategory = new ExtendedProgramCategory()
                        {
                            ProgramCategoryId = (int)reader["ProgramCategoryId"],
                            ProgramCategoryName = (string)reader["ProgramCategoryName"]
                        };
                        item.UdodSectionId = (long)reader["UdodSectionId"];
                        item.Comment = (string)reader["Comment"];
                        item.NumYears = (int)reader["NumYears"];
                        item.Addresses = new List<ExtendedAddress>();
                        if (reader["AddressStr"] != DBNull.Value)
                            item.Addresses.Add(new ExtendedAddress()
                            {
                                AddressStr = (string)reader["AddressStr"]
                            });
                        item.Teachers = new List<ExtendedTeacher>();
                        if (_teacherid.HasValue)
                        {
                            item.Teachers.Add(new ExtendedTeacher()
                            {
                                UserId = _teacherid.Value,
                                FirstName = (string)reader["TeacherFirstName"],
                                LastName = (string)reader["TeacherLastName"],
                                MiddleName = (string)reader["TeacherMiddleName"]
                            });
                        }
                        item.BudgetTypes = new List<ExtendedTypeBudget>();
                        if (_budgetTypeId.HasValue)
                        {
                            item.BudgetTypes.Add(new ExtendedTypeBudget()
                            {
                                DictTypeBudgetId = _budgetTypeId.Value,
                                TypeBudgetName = (string)reader["TypeBudgetName"]
                            });
                        }
                        item.isPublicPortal = (bool)reader["IsPublicPortal"];
                        item.isReception = (bool)reader["IsReception"];

                        //if ((isPublicPortal && item.isPublicPortal && item.isReception) || (!isPublicPortal))
                        list.Add(item);
                    }
                }
            }

            ChildUnionsCount = list.Count;
            return list;
        }

        public int GetChildUnionsCount(int? cityId, long? udodId, int? programId, int? profileId, int? sectionId, int? typeBudgetId, string sectionName, string profileName, string programName, string childUnionName, string cityCode, string rayonCode, int? metroId, string udodName, int? rayonId, int? pagerIndex, int? pagerLength, int? ShowIsDOEnabled, bool? isActive, bool isPublicPortal, bool isReception, long? childUnionId)
        {
            List<ExtendedChildUnion> list = new List<ExtendedChildUnion>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetChildUnionCount", Connection) { CommandType = CommandType.StoredProcedure };
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (programId.HasValue) comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                if (profileId.HasValue) comm.Parameters.Add(new SqlParameter("ProfileId", profileId));
                if (sectionId.HasValue) comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                if (typeBudgetId.HasValue) comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                if (!string.IsNullOrEmpty(programName)) comm.Parameters.Add(new SqlParameter("ProgramName", programName));
                if (!string.IsNullOrEmpty(profileName)) comm.Parameters.Add(new SqlParameter("ProfileName", profileName));
                if (!string.IsNullOrEmpty(sectionName)) comm.Parameters.Add(new SqlParameter("SectionName", sectionName));
                if (!string.IsNullOrEmpty(childUnionName)) comm.Parameters.Add(new SqlParameter("ChildUnionName", childUnionName));
                if (!string.IsNullOrEmpty(cityCode)) comm.Parameters.Add(new SqlParameter("CityCode", cityCode));
                if (!string.IsNullOrEmpty(rayonCode)) comm.Parameters.Add(new SqlParameter("RayonCode", rayonCode));
                if (metroId.HasValue) comm.Parameters.Add(new SqlParameter("MetroId", metroId));
                if (rayonId.HasValue) comm.Parameters.Add(new SqlParameter("RayonId", rayonId));
                if (!string.IsNullOrEmpty(udodName)) comm.Parameters.Add(new SqlParameter("UdodName", udodName));
                if (pagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", pagerIndex));
                if (pagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", pagerLength));
                if (ShowIsDOEnabled.HasValue) comm.Parameters.Add(new SqlParameter("ShowIsDOEnabled", ShowIsDOEnabled));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (isActive.HasValue) comm.Parameters.Add(new SqlParameter("IsActive", isActive));
                comm.Parameters.Add(new SqlParameter("IsPublicPortal", isPublicPortal));
                comm.Parameters.Add(new SqlParameter("IsReception", isReception));
                
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }
            return 0;
        }

        public long AddChildUnion(ExtendedChildUnion ecu)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("AddChildUnion", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Name", ecu.Name));
                comm.Parameters.Add(new SqlParameter("AgeStart", ecu.AgeStart));
                comm.Parameters.Add(new SqlParameter("AgeEnd", ecu.AgeEnd));
                comm.Parameters.Add(new SqlParameter("NumYears", ecu.NumYears));
                comm.Parameters.Add(new SqlParameter("Comment", ecu.Comment));
                comm.Parameters.Add(new SqlParameter("IsActive", ecu.IsActive));
                comm.Parameters.Add(new SqlParameter("UdodSectionId", ecu.UdodSectionId));
                comm.Parameters.Add(new SqlParameter("ProgramCategoryId", ecu.ProgramCategory.ProgramCategoryId));
                comm.Parameters.Add(new SqlParameter("MinCountPupil", ecu.MinCountPupil));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (long)reader["ChildUnionId"];
                }
            }
            return -1;
        }

        public void DeleteChildUnion(long ChildUnionId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("DeleteChildUnion", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ChildUnionId", ChildUnionId));
                comm.ExecuteNonQuery();
            }
        }

        public void RealDeleteChildUnion(long ChildUnionId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Udod_RealDeleteChildUnion", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ChildUnionId", ChildUnionId));
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateChildUnion(ExtendedChildUnion ecu)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateChildUnion", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Name", ecu.Name));
                comm.Parameters.Add(new SqlParameter("AgeStart", ecu.AgeStart));
                comm.Parameters.Add(new SqlParameter("AgeEnd", ecu.AgeEnd));
                comm.Parameters.Add(new SqlParameter("NumYears", ecu.NumYears));
                comm.Parameters.Add(new SqlParameter("Comment", ecu.Comment));
                comm.Parameters.Add(new SqlParameter("IsActive", ecu.IsActive));
                comm.Parameters.Add(new SqlParameter("UdodSectionId", ecu.UdodSectionId));
                comm.Parameters.Add(new SqlParameter("ChildUnionId", ecu.ChildUnionId));
                comm.Parameters.Add(new SqlParameter("ProgramCategoryId", ecu.ProgramCategory.ProgramCategoryId));
                comm.Parameters.Add(new SqlParameter("MinCountPupil", ecu.MinCountPupil));
                comm.Parameters.Add(new SqlParameter("MaxCountPupil", ecu.MaxCountPupil));
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateChildUnionDescription(ExtendedChildUnion ecu)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateChildUnionDescription", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ChildUnionId", ecu.ChildUnionId));
                comm.Parameters.Add(new SqlParameter("ProgramService", ecu.ProgramService));
                comm.Parameters.Add(new SqlParameter("RuleService", ecu.RuleService));
                comm.Parameters.Add(new SqlParameter("DocsDeadline", ecu.DocsDeadline));
                comm.Parameters.Add(new SqlParameter("TestService", ecu.TestService));
                comm.Parameters.Add(new SqlParameter("ToursNumber", ecu.ToursNumber));
                comm.Parameters.Add(new SqlParameter("TypeFinanceId", ecu.TypeFinanceId));
                comm.Parameters.Add(new SqlParameter("Finansing", ecu.Finansing));
                comm.Parameters.Add(new SqlParameter("SubFinance", ecu.SubFinance));
                comm.Parameters.Add(new SqlParameter("DateStart", ecu.DateStart));
                comm.Parameters.Add(new SqlParameter("DateEnd", ecu.DateEnd));
                comm.Parameters.Add(new SqlParameter("SexId", ecu.SexId));
                comm.Parameters.Add(new SqlParameter("StatusId", ecu.StatusId));
                comm.Parameters.Add(new SqlParameter("FormId", ecu.FormId));
                comm.Parameters.Add(new SqlParameter("TypeValueServiceId", ecu.TypeValueServiceId));
                comm.Parameters.Add(new SqlParameter("IsDopService", ecu.isDopService));
                if (ecu.MessageId.HasValue) comm.Parameters.Add(new SqlParameter("MessageId", ecu.MessageId));
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateChildUnionIsPortal(ExtendedChildUnion ecu)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateChildUnionIsPortal", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ChildUnionId", ecu.ChildUnionId));
                comm.Parameters.Add(new SqlParameter("IsPublicPortal", ecu.isPublicPortal));
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateChildUnionIsReception(ExtendedChildUnion ecu)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateChildUnionIsReception", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ChildUnionId", ecu.ChildUnionId));
                comm.Parameters.Add(new SqlParameter("IsReception", ecu.isReception));
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateChildUnionEszId(Guid messageId, string codeId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateChildUnionEszCode", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("messageId", messageId));
                comm.Parameters.Add(new SqlParameter("codeId", codeId));
                comm.ExecuteNonQuery();
            }
        }

        public List<ExtendedSimpleSchedule> GetChildUnionSimpleSchedule(long childUnionId)
        {
            List<ExtendedSimpleSchedule> list = new List<ExtendedSimpleSchedule>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetChildUnionSimpleSchedule", Connection)
                                      {CommandType = CommandType.StoredProcedure};
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedSimpleSchedule item = new ExtendedSimpleSchedule();
                    item.ChildUnionId = childUnionId;
                    item.DayOfWeek = (int) reader["DayOfWeek"];
                    item.TimeDayId = (int) reader["TimeDayId"];
                    item.TimeDayName = (string) reader["TimeDayName"];
                    
                    list.Add(item);
                }
            }
            return list;
        }

        public void UpdateChildUnionSimpleSchedule(long childUnionId, int dayOfWeek, int timeDayId, int value)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_UpdateChildUnionSimpleSchedule", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("DayOfWeek", dayOfWeek));
                comm.Parameters.Add(new SqlParameter("TimeDayId", timeDayId));
                comm.Parameters.Add(new SqlParameter("Value", value));
                Connection.Open();
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateChildUnionReestrCode(long childUnionId, long reestrCode)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_UpdateChildUnionReestrCode", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("ReestrCode", reestrCode));
                Connection.Open();
                comm.ExecuteNonQuery();
            }
        }

        public int[] GetCountGroups(long childUnionId, int numYear)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("ChildUnion_GetCountGroups", Connection)
                                      {CommandType = CommandType.StoredProcedure};
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("NumYear", numYear));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return new int[] { (int)reader[0], (int)reader[1], (int)reader[2] };
                }
            }
            return new int[] {0, 0, 0};
        }


        public ExtendedChildUnion GetChildUnion(long? childUnionId, long? ageGroupId)
        {
            ExtendedChildUnion item = null;

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetChildUnion", Connection) { CommandType = CommandType.StoredProcedure };
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId.Value));
                if (ageGroupId.HasValue) comm.Parameters.Add(new SqlParameter("AgeGroupId", ageGroupId.Value));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long? _ageGroupId = null;
                    if (reader["UdodAgeGroupId"] != DBNull.Value) _ageGroupId = (long?)reader["UdodAgeGroupId"];

                    long? _addressId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long?)reader["AddressId"];

                    long? _officeAddressId = null;
                    if (reader["OfficeAddressId"] != DBNull.Value) _officeAddressId = (long?)reader["OfficeAddressId"];

                    int? _budgetTypeId = null;
                    if (reader["DictTypeBudgetId"] != DBNull.Value) _budgetTypeId = (int?)reader["DictTypeBudgetId"];

                    int? _educationTypeId = null;
                    if (reader["EducationTypeId"] != DBNull.Value) _educationTypeId = (int?)reader["EducationTypeId"];

                    if (null != item)
                    {
                        if (_ageGroupId.HasValue && item.AgeGroups.Count(i => i.UdodAgeGroupId == _ageGroupId) == 0)
                        {
                            var itemAgeGroup = new ExtendedAgeGroup();
                            //itemAgeGroup.AgeEnd = (int)reader["AgeEnd"];
                            itemAgeGroup.Age = (int)reader["Age"];
                            itemAgeGroup.Section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                                SectionId = (int)reader["SectionId"],
                                ProfileId = (int)reader["ProfileId"],
                                EszSectionId = (int)reader["ESZSectionId"]
                            };
                            //itemAgeGroup.TypeBudget = new ExtendedTypeBudget()
                            //{
                            //    DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                            //    TypeBudgetName = (string)reader["TypeBudgetName"]
                            //};
                            itemAgeGroup.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                            itemAgeGroup.UdodId = (long)reader["UdodId"];
                            //itemAgeGroup.EducationType = new ExtendedEducationType()
                            //{
                            //    EducationTypeId = (int)reader["EducationTypeId"],
                            //    EducationTypeName = (string)reader["EducationTypeName"]
                            //};
                            itemAgeGroup.NumYearStart = (int)reader["NumYearStart"];
                            itemAgeGroup.NumYearEnd = (int)reader["NumYearEnd"];
                            itemAgeGroup.AgeStart = (int)reader["AgeGroupAgeStart"];
                            itemAgeGroup.AgeEnd = (int)reader["AgeGroupAgeEnd"];

                            itemAgeGroup.LessonLength = (int)reader["LessonLength"];
                            itemAgeGroup.BreakLength = (int)reader["BreakLength"];
                            itemAgeGroup.LessonsInWeek = (int)reader["LessonsInWeek"];
                            itemAgeGroup.IsActive = (bool)reader["AgeGroupIsActive"];
                            itemAgeGroup.Program = new ExtendedProgram()
                            {
                                ProgramId = (int)reader["ProgramId"],
                                Name = (string)reader["ProgramName"]
                            };
                            //itemAgeGroup.TeacherFio = (string)reader["Fio"];

                            (item.AgeGroups as List<ExtendedAgeGroup>).Add(itemAgeGroup);
                        }

                        if (_officeAddressId.HasValue && item.Offices.Count(i => i.AddressId == _officeAddressId) == 0)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["OfficeAddressId"];
                            item.Offices.Add(itemAddress);
                        }

                        if (_addressId.HasValue && item.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];

                            itemAddress.BtiHouseCode = null;
                            if (reader["BtiHouseCode"] != DBNull.Value) itemAddress.BtiHouseCode = (int)reader["BtiHouseCode"];
                            

                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        if (_budgetTypeId.HasValue && item.BudgetTypes.Count(i => i.DictTypeBudgetId == _budgetTypeId) == 0)
                        {
                            var itemBudgetType = new ExtendedTypeBudget();
                            itemBudgetType.DictTypeBudgetId = (int)reader["DictTypeBudgetId"];
                            itemBudgetType.TypeBudgetName = (string)reader["TypeBudgetName"];

                            (item.BudgetTypes as List<ExtendedTypeBudget>).Add(itemBudgetType);
                        }

                        if (_educationTypeId.HasValue && item.EducationTypes.Count(i => i.EducationTypeId == _educationTypeId) == 0)
                        {
                            var itemEducationType = new ExtendedEducationType();
                            itemEducationType.EducationTypeId = (int)reader["EducationTypeId"];
                            itemEducationType.EducationTypeName = (string)reader["EducationTypeName"];

                            (item.EducationTypes as List<ExtendedEducationType>).Add(itemEducationType);
                        }
                    }
                    else
                    {
                        item = new ExtendedChildUnion();
                        item.ChildUnionId = (long)reader["ChildUnionId"];
                        item.Name = (string)reader["Name"];
                        item.Section = new ExtendedSection()
                        {
                            SectionId = (int)reader["SectionId"],
                            Name = (string)reader["SectionName"],
                            EszSectionId = (int)reader["ESZSectionId"]
                        };
                        item.Profile = new ExtendedProfile()
                        {
                            ProfileId = (int)reader["ProfileId"],
                            Name = (string)reader["ProfileName"]
                        };
                        item.Program = new ExtendedProgram()
                        {
                            ProgramId = (int)reader["ProgramId"],
                            Name = (string)reader["ProgramName"]
                        };
                        item.ProgramCategory = new ExtendedProgramCategory()
                        {
                            ProgramCategoryId = (int)reader["ProgramCategoryId"],
                            ProgramCategoryName = (string)reader["ProgramCategoryName"]
                        };
                        item.UdodId = (long)reader["UdodId"];
                        item.UdodSectionId = (long)reader["UdodSectionId"];
                        item.Comment = (string)reader["Comment"];
                        item.NumYears = (int) reader["NumYears"];
                        item.IsActive = (bool) reader["IsActive"];
                        //item.IsWorking = (bool) reader["IsWorking"];
                        item.AgeGroups = new List<ExtendedAgeGroup>();
                        item.Addresses = new List<ExtendedAddress>();
                        item.BudgetTypes = new List<ExtendedTypeBudget>();
                        item.EducationTypes = new List<ExtendedEducationType>();
                        item.AgeStart = (int) reader["AgeStart"];
                        item.AgeEnd = (int) reader["AgeEnd"];
                        item.MinCountPupil = (int) reader["MinCountPupil"];
                        item.MaxCountPupil = (int) reader["MaxCountPupil"];
                        if (reader["ReestrCode"] != DBNull.Value)
                        {
                            item.ReestrCode = (string) reader["ReestrCode"];
                        }
                        if (reader["GuidOU"] != DBNull.Value)
                        {
                            item.GuidOU = (string)reader["GuidOU"];
                        }
                        if (reader["MessageId"] != DBNull.Value)
                        {
                            item.MessageId = (Guid) reader["MessageId"];
                        }
                        else
                            item.MessageId = null;
                        if (reader["ChildUnionDescriptionId"] != DBNull.Value)
                        {
                            item.ChildUnionDescriptionId = (long) reader["ChildUnionDescriptionId"];
                            item.ProgramService = (string) reader["ProgramService"];
                            item.RuleService = (string) reader["RuleService"];
                            item.DocsDeadline = (int) reader["DocsDeadline"];
                            item.TestService = (bool) reader["TestService"];
                            item.ToursNumber = (int) reader["ToursNumber"];
                            item.TypeFinanceId = (int) reader["TypeFinanceId"];
                            item.Finansing = Convert.ToDecimal(reader["Finansing"], CultureInfo.GetCultureInfo("en-us"));
                            item.SubFinance = Convert.ToDecimal(reader["SubFinance"], CultureInfo.GetCultureInfo("en-us")); 
                            item.DateStart = (DateTime) reader["DateStart"];
                            item.DateEnd = (DateTime) reader["DateEnd"];
                            item.SexId = (int) reader["SexId"];
                            item.StatusId = (int) reader["StatusId"];
                            item.FormId = (int) reader["FormId"];
                            item.ESZId = (string) reader["ESZId"];
                            item.isDopService = (bool) reader["IsDopService"];
                            item.TypeValueServiceId = (int)reader["TypeValueServiceId"];
                            item.isPublicPortal = (bool) reader["IsPublicPortal"];
                            item.isReception = (bool) reader["IsReception"];
                        }
                        item.Offices = new List<ExtendedAddress>();

                        if (_officeAddressId.HasValue)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["OfficeAddressId"];
                            item.Offices.Add(itemAddress);
                        }

                        if (_ageGroupId.HasValue)
                        {
                            var itemAgeGroup = new ExtendedAgeGroup();
                            //itemAgeGroup.AgeEnd = (int)reader["AgeEnd"];
                            itemAgeGroup.Age = (int)reader["Age"];
                            itemAgeGroup.Section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                                SectionId = (int)reader["SectionId"],
                                ProfileId = (int)reader["ProfileId"],
                                EszSectionId = (int)reader["ESZSectionId"]
                            };
                            //itemAgeGroup.TypeBudget = new ExtendedTypeBudget()
                            //{
                            //    DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                            //    TypeBudgetName = (string)reader["TypeBudgetName"]
                            //};

                            itemAgeGroup.NumYearStart = (int)reader["NumYearStart"];
                            itemAgeGroup.NumYearEnd = (int)reader["NumYearEnd"];
                            itemAgeGroup.AgeStart = (int)reader["AgeGroupAgeStart"];
                            itemAgeGroup.AgeEnd = (int)reader["AgeGroupAgeEnd"];

                            itemAgeGroup.LessonLength = (int)reader["LessonLength"];
                            itemAgeGroup.BreakLength = (int)reader["BreakLength"];
                            itemAgeGroup.LessonsInWeek = (int)reader["LessonsInWeek"];
                            itemAgeGroup.IsActive = (bool)reader["AgeGroupIsActive"];
                            itemAgeGroup.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                            itemAgeGroup.UdodId = (long)reader["UdodId"];
                            //itemAgeGroup.EducationType = new ExtendedEducationType()
                            //{
                            //    EducationTypeId = (int)reader["EducationTypeId"],
                            //    EducationTypeName = (string)reader["EducationTypeName"]
                            //};
                            itemAgeGroup.Program = new ExtendedProgram()
                            {
                                ProgramId = (int)reader["ProgramId"],
                                Name = (string)reader["ProgramName"]
                            };
                            //itemAgeGroup.TeacherFio = (string)reader["Fio"];

                            (item.AgeGroups as List<ExtendedAgeGroup>).Add(itemAgeGroup);
                        }

                        if (_addressId.HasValue)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];

                            itemAddress.BtiHouseCode= null;
                            if (reader["BtiHouseCode"] != DBNull.Value) itemAddress.BtiHouseCode = (int)reader["BtiHouseCode"];
                            

                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        if (_budgetTypeId.HasValue)
                        {
                            var itemBudgetType = new ExtendedTypeBudget();
                            itemBudgetType.DictTypeBudgetId = (int)reader["DictTypeBudgetId"];
                            itemBudgetType.TypeBudgetName = (string)reader["TypeBudgetName"];

                            (item.BudgetTypes as List<ExtendedTypeBudget>).Add(itemBudgetType);
                        }

                        if (_educationTypeId.HasValue)
                        {
                            var itemEducationType = new ExtendedEducationType();
                            itemEducationType.EducationTypeId = (int)reader["EducationTypeId"];
                            itemEducationType.EducationTypeName = (string)reader["EducationTypeName"];

                            (item.EducationTypes as List<ExtendedEducationType>).Add(itemEducationType);
                        }
                    }
                }
            }
            return item;
        }

        public ExtendedChildUnion GetDeletedChildUnion(long? childUnionId, long? ageGroupId)
        {
            ExtendedChildUnion item = null;

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetChildUnionTmp", Connection) { CommandType = CommandType.StoredProcedure };
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId.Value));
                if (ageGroupId.HasValue) comm.Parameters.Add(new SqlParameter("AgeGroupId", ageGroupId.Value));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    long? _ageGroupId = null;
                    if (reader["UdodAgeGroupId"] != DBNull.Value) _ageGroupId = (long?)reader["UdodAgeGroupId"];

                    long? _addressId = null;
                    if (reader["AddressId"] != DBNull.Value) _addressId = (long?)reader["AddressId"];

                    long? _officeAddressId = null;
                    if (reader["OfficeAddressId"] != DBNull.Value) _officeAddressId = (long?)reader["OfficeAddressId"];

                    int? _budgetTypeId = null;
                    if (reader["DictTypeBudgetId"] != DBNull.Value) _budgetTypeId = (int?)reader["DictTypeBudgetId"];

                    int? _educationTypeId = null;
                    if (reader["EducationTypeId"] != DBNull.Value) _educationTypeId = (int?)reader["EducationTypeId"];

                    if (null != item)
                    {
                        if (_ageGroupId.HasValue && item.AgeGroups.Count(i => i.UdodAgeGroupId == _ageGroupId) == 0)
                        {
                            var itemAgeGroup = new ExtendedAgeGroup();
                            //itemAgeGroup.AgeEnd = (int)reader["AgeEnd"];
                            itemAgeGroup.Age = (int)reader["Age"];
                            itemAgeGroup.Section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                                SectionId = (int)reader["SectionId"],
                                ProfileId = (int)reader["ProfileId"],
                                EszSectionId = (int)reader["ESZSectionId"]
                            };
                            //itemAgeGroup.TypeBudget = new ExtendedTypeBudget()
                            //{
                            //    DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                            //    TypeBudgetName = (string)reader["TypeBudgetName"]
                            //};
                            itemAgeGroup.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                            itemAgeGroup.UdodId = (long)reader["UdodId"];
                            //itemAgeGroup.EducationType = new ExtendedEducationType()
                            //{
                            //    EducationTypeId = (int)reader["EducationTypeId"],
                            //    EducationTypeName = (string)reader["EducationTypeName"]
                            //};
                            itemAgeGroup.NumYearStart = (int)reader["NumYearStart"];
                            itemAgeGroup.NumYearEnd = (int)reader["NumYearEnd"];
                            itemAgeGroup.AgeStart = (int)reader["AgeGroupAgeStart"];
                            itemAgeGroup.AgeEnd = (int)reader["AgeGroupAgeEnd"];

                            itemAgeGroup.LessonLength = (int)reader["LessonLength"];
                            itemAgeGroup.BreakLength = (int)reader["BreakLength"];
                            itemAgeGroup.LessonsInWeek = (int)reader["LessonsInWeek"];
                            itemAgeGroup.IsActive = (bool)reader["AgeGroupIsActive"];
                            itemAgeGroup.Program = new ExtendedProgram()
                            {
                                ProgramId = (int)reader["ProgramId"],
                                Name = (string)reader["ProgramName"]
                            };
                            //itemAgeGroup.TeacherFio = (string)reader["Fio"];

                            (item.AgeGroups as List<ExtendedAgeGroup>).Add(itemAgeGroup);
                        }

                        if (_officeAddressId.HasValue && item.Offices.Count(i => i.AddressId == _officeAddressId) == 0)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["OfficeAddressId"];
                            item.Offices.Add(itemAddress);
                        }

                        if (_addressId.HasValue && item.Addresses.Count(i => i.AddressId == _addressId) == 0)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];

                            itemAddress.BtiHouseCode = null;
                            if (reader["BtiHouseCode"] != DBNull.Value) itemAddress.BtiHouseCode = (int)reader["BtiHouseCode"];


                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        if (_budgetTypeId.HasValue && item.BudgetTypes.Count(i => i.DictTypeBudgetId == _budgetTypeId) == 0)
                        {
                            var itemBudgetType = new ExtendedTypeBudget();
                            itemBudgetType.DictTypeBudgetId = (int)reader["DictTypeBudgetId"];
                            itemBudgetType.TypeBudgetName = (string)reader["TypeBudgetName"];

                            (item.BudgetTypes as List<ExtendedTypeBudget>).Add(itemBudgetType);
                        }

                        if (_educationTypeId.HasValue && item.EducationTypes.Count(i => i.EducationTypeId == _educationTypeId) == 0)
                        {
                            var itemEducationType = new ExtendedEducationType();
                            itemEducationType.EducationTypeId = (int)reader["EducationTypeId"];
                            itemEducationType.EducationTypeName = (string)reader["EducationTypeName"];

                            (item.EducationTypes as List<ExtendedEducationType>).Add(itemEducationType);
                        }
                    }
                    else
                    {
                        item = new ExtendedChildUnion();
                        item.ChildUnionId = (long)reader["ChildUnionId"];
                        item.Name = (string)reader["Name"];
                        item.Section = new ExtendedSection()
                        {
                            SectionId = (int)reader["SectionId"],
                            Name = (string)reader["SectionName"],
                            EszSectionId = (int)reader["ESZSectionId"]
                        };
                        item.Profile = new ExtendedProfile()
                        {
                            ProfileId = (int)reader["ProfileId"],
                            Name = (string)reader["ProfileName"]
                        };
                        item.Program = new ExtendedProgram()
                        {
                            ProgramId = (int)reader["ProgramId"],
                            Name = (string)reader["ProgramName"]
                        };
                        item.ProgramCategory = new ExtendedProgramCategory()
                        {
                            ProgramCategoryId = (int)reader["ProgramCategoryId"],
                            ProgramCategoryName = (string)reader["ProgramCategoryName"]
                        };
                        item.UdodId = (long)reader["UdodId"];
                        item.UdodSectionId = (long)reader["UdodSectionId"];
                        item.Comment = (string)reader["Comment"];
                        item.NumYears = (int)reader["NumYears"];
                        item.IsActive = (bool)reader["IsActive"];
                        //item.IsWorking = (bool) reader["IsWorking"];
                        item.AgeGroups = new List<ExtendedAgeGroup>();
                        item.Addresses = new List<ExtendedAddress>();
                        item.BudgetTypes = new List<ExtendedTypeBudget>();
                        item.EducationTypes = new List<ExtendedEducationType>();
                        item.AgeStart = (int)reader["AgeStart"];
                        item.AgeEnd = (int)reader["AgeEnd"];
                        if (reader["ReestrCode"] != DBNull.Value)
                        {
                            item.ReestrCode = (string)reader["ReestrCode"];
                        }
                        if (reader["GuidOU"] != DBNull.Value)
                        {
                            item.GuidOU = (string)reader["GuidOU"];
                        }
                        if (reader["MessageId"] != DBNull.Value)
                        {
                            item.MessageId = (Guid)reader["MessageId"];
                        }
                        else
                            item.MessageId = null;
                        if (reader["ChildUnionDescriptionId"] != DBNull.Value)
                        {
                            item.ChildUnionDescriptionId = (long)reader["ChildUnionDescriptionId"];
                            item.ProgramService = (string)reader["ProgramService"];
                            item.RuleService = (string)reader["RuleService"];
                            item.DocsDeadline = (int)reader["DocsDeadline"];
                            item.TestService = (bool)reader["TestService"];
                            item.ToursNumber = (int)reader["ToursNumber"];
                            item.TypeFinanceId = (int)reader["TypeFinanceId"];
                            item.Finansing = Convert.ToDecimal(reader["Finansing"], CultureInfo.GetCultureInfo("en-us"));
                            item.SubFinance = Convert.ToDecimal(reader["SubFinance"], CultureInfo.GetCultureInfo("en-us"));
                            item.DateStart = (DateTime)reader["DateStart"];
                            item.DateEnd = (DateTime)reader["DateEnd"];
                            item.SexId = (int)reader["SexId"];
                            item.StatusId = (int)reader["StatusId"];
                            item.FormId = (int)reader["FormId"];
                            item.ESZId = (string)reader["ESZId"];
                            item.isDopService = (bool)reader["IsDopService"];
                            item.TypeValueServiceId = (int)reader["TypeValueServiceId"];
                        }
                        item.Offices = new List<ExtendedAddress>();

                        if (_officeAddressId.HasValue)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["OfficeAddressId"];
                            item.Offices.Add(itemAddress);
                        }

                        if (_ageGroupId.HasValue)
                        {
                            var itemAgeGroup = new ExtendedAgeGroup();
                            //itemAgeGroup.AgeEnd = (int)reader["AgeEnd"];
                            itemAgeGroup.Age = (int)reader["Age"];
                            itemAgeGroup.Section = new ExtendedSection()
                            {
                                Name = (string)reader["SectionName"],
                                SectionId = (int)reader["SectionId"],
                                ProfileId = (int)reader["ProfileId"],
                                EszSectionId = (int)reader["ESZSectionId"]
                            };
                            //itemAgeGroup.TypeBudget = new ExtendedTypeBudget()
                            //{
                            //    DictTypeBudgetId = (int)reader["DictTypeBudgetId"],
                            //    TypeBudgetName = (string)reader["TypeBudgetName"]
                            //};

                            itemAgeGroup.NumYearStart = (int)reader["NumYearStart"];
                            itemAgeGroup.NumYearEnd = (int)reader["NumYearEnd"];
                            itemAgeGroup.AgeStart = (int)reader["AgeGroupAgeStart"];
                            itemAgeGroup.AgeEnd = (int)reader["AgeGroupAgeEnd"];

                            itemAgeGroup.LessonLength = (int)reader["LessonLength"];
                            itemAgeGroup.BreakLength = (int)reader["BreakLength"];
                            itemAgeGroup.LessonsInWeek = (int)reader["LessonsInWeek"];
                            itemAgeGroup.IsActive = (bool)reader["AgeGroupIsActive"];
                            itemAgeGroup.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                            itemAgeGroup.UdodId = (long)reader["UdodId"];
                            //itemAgeGroup.EducationType = new ExtendedEducationType()
                            //{
                            //    EducationTypeId = (int)reader["EducationTypeId"],
                            //    EducationTypeName = (string)reader["EducationTypeName"]
                            //};
                            itemAgeGroup.Program = new ExtendedProgram()
                            {
                                ProgramId = (int)reader["ProgramId"],
                                Name = (string)reader["ProgramName"]
                            };
                            //itemAgeGroup.TeacherFio = (string)reader["Fio"];

                            (item.AgeGroups as List<ExtendedAgeGroup>).Add(itemAgeGroup);
                        }

                        if (_addressId.HasValue)
                        {
                            var itemAddress = new ExtendedAddress();
                            itemAddress.AddressId = (long)reader["AddressId"];
                            itemAddress.KladrCode = (string)reader["KladrCode"];
                            itemAddress.AddressStr = (string)reader["AddressStr"];
                            itemAddress.HouseNumber = (string)reader["HouseNumber"];
                            itemAddress.Fraction = (string)reader["Fraction"];
                            itemAddress.Housing = (string)reader["Housing"];
                            itemAddress.Building = (string)reader["Building"];
                            itemAddress.FlatNumber = (string)reader["Flat"];
                            itemAddress.AddressType = new ExtendedAddressType
                            {
                                AddressTypeId = (int)reader["AddressTypeId"],
                                Name = (string)reader["AddressTypeName"]
                            };
                            //itemAddress.CoordinateX = (float)reader["Coordinate_x"];
                            //itemAddress.CoordinateY = (float)reader["Coordinate_y"];
                            itemAddress.BtiCode = null;
                            if (reader["BtiCode"] != DBNull.Value) itemAddress.BtiCode = (int)reader["BtiCode"];

                            itemAddress.BtiHouseCode = null;
                            if (reader["BtiHouseCode"] != DBNull.Value) itemAddress.BtiHouseCode = (int)reader["BtiHouseCode"];


                            (item.Addresses as List<ExtendedAddress>).Add(itemAddress);
                        }

                        if (_budgetTypeId.HasValue)
                        {
                            var itemBudgetType = new ExtendedTypeBudget();
                            itemBudgetType.DictTypeBudgetId = (int)reader["DictTypeBudgetId"];
                            itemBudgetType.TypeBudgetName = (string)reader["TypeBudgetName"];

                            (item.BudgetTypes as List<ExtendedTypeBudget>).Add(itemBudgetType);
                        }

                        if (_educationTypeId.HasValue)
                        {
                            var itemEducationType = new ExtendedEducationType();
                            itemEducationType.EducationTypeId = (int)reader["EducationTypeId"];
                            itemEducationType.EducationTypeName = (string)reader["EducationTypeName"];

                            (item.EducationTypes as List<ExtendedEducationType>).Add(itemEducationType);
                        }
                    }
                }
            }
            return item;
        }

        public List<ExtendedChildUnionTeacher> GetTeacher(long childUnionId)
        {
            List<ExtendedChildUnionTeacher> list = new List<ExtendedChildUnionTeacher>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("ChildUnion_GetTeacher", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedChildUnionTeacher item = new ExtendedChildUnionTeacher();
                    item.ChildUnionTeacherId = (long)reader["ChildUnionTeacherId"];
                    item.UserId = (Guid)reader["UserId"];
                    item.TeacherName = (string)reader["TeacherName"];
                    list.Add(item);
                }
            }

            return list;
        }
        public void AddTeacher(long childUnionId, Guid teacherId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("ChildUnion_AddTeacher", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("TeacherId", teacherId));
                comm.ExecuteNonQuery();
            }
        }
        public void DeleteTeacher(long childUnionTeacherId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("ChildUnion_DeleteTeacher", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ChildUnionTeacherId", childUnionTeacherId));
                comm.ExecuteNonQuery();
            }
        }

        public void AddTypeBudget(long childUnionId, int typeBudgetId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("ChildUnion_AddTypeBudget", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                comm.ExecuteNonQuery();
            }
        }
        public void DeleteTypeBudget(long childUnionId, int typeBudgetId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("ChildUnion_DeleteTypeBudget", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("TypeBudgetId", typeBudgetId));
                comm.ExecuteNonQuery();
            }
        }
        public void AddEducationType(long childUnionId, int educationTypeId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("ChildUnion_AddEducationType", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("EducationTypeId", educationTypeId));
                comm.ExecuteNonQuery();
            }
        }
        public void DeleteEducationType(long childUnionId, int educationTypeId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("ChildUnion_DeleteEducationType", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("EducationTypeId", educationTypeId));
                comm.ExecuteNonQuery();
            }
        }

        public void AddAddress(long childUnionId, long addressId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("ChildUnion_AddAddress", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("AddressId", addressId));
                comm.ExecuteNonQuery();
            }
        }
        public void DeleteAddress(long childUnionId, long addressId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("ChildUnion_DeleteAddress", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                comm.Parameters.Add(new SqlParameter("AddressId", addressId));
                comm.ExecuteNonQuery();
            }
        }
        public ExtendedUdod GetUdod(long childUnionId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("ChildUnion_GetUdod", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return new ExtendedUdod
                    {
                        UdodId = (long)reader["UdodId"],
                        Name = (string)reader["Name"],
                        ShortName = (string)reader["ShortName"],
                        JurAddress = (string)reader["JurAddress"],
                        CityId = (int)reader["CityId"],
                        FioDirector = (string)reader["FIODirector"],
                        PhoneNumber = (string)reader["PhoneNumber"],
                        FaxNumber = (string)reader["FaxNumber"],
                        EMail = (string)reader["EMail"],
                        SiteUrl = (string)reader["SiteUrl"],
                        UdodTypeId = (int)reader["UdodTypeId"]
                    };
                }
            }
            return null;
        }

    }
}

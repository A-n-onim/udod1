﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Udod.Dal.Enum;

namespace Udod.Dal
{
    public class EventsDb : BaseDb
    {
        public void AddRequest(Guid requestId, string fileName, long? claimId, long? udodAgeGroupid, long? childUnionId, int typeRequest)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("AddRequest", Connection)
                                      {CommandType = CommandType.StoredProcedure};
                
                comm.Parameters.Add(new SqlParameter("RequestId", requestId));
                comm.Parameters.Add(new SqlParameter("FileName", fileName));
                comm.Parameters.Add(new SqlParameter("TypeRequest", typeRequest));
                if (claimId.HasValue) comm.Parameters.Add(new SqlParameter("ClaimId", claimId.Value));
                if (udodAgeGroupid.HasValue) comm.Parameters.Add(new SqlParameter("UdodAgeGroupId", udodAgeGroupid.Value));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId.Value));
                Connection.Open();
                comm.ExecuteNonQuery();
            }
        }

        public void ReplyRequest(Guid requestId, bool isError, int typeRequest)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("ReplyRequest", Connection) { CommandType = CommandType.StoredProcedure };

                comm.Parameters.Add(new SqlParameter("RequestId", requestId));
                comm.Parameters.Add(new SqlParameter("IsError", isError));
                comm.Parameters.Add(new SqlParameter("TypeRequest", typeRequest));
                Connection.Open();
                comm.ExecuteNonQuery();
            }
        }
        /*
        public void LogRequest(Guid guid, Guid requestId, int typelog, string filetxt)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("AddLogRequest", Connection) { CommandType = CommandType.StoredProcedure };

                comm.Parameters.Add(new SqlParameter("guid", guid));
                comm.Parameters.Add(new SqlParameter("RequestId", requestId));
                comm.Parameters.Add(new SqlParameter("typerequest", typelog));
                comm.Parameters.Add(new SqlParameter("filetxt", filetxt));
                Connection.Open();
                comm.ExecuteNonQuery();
            }
        }*/

        public List<ExtendedRequests> GetRequests(bool? isError, Guid requestId)
        {
            List<ExtendedRequests> list = new List<ExtendedRequests>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetRequest", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("RequestId", requestId));
                comm.Parameters.Add(new SqlParameter("IsError", isError));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    var item = new ExtendedRequests();

                    item.RequestId = (Guid) reader["RequestId"];
                    item.FileName = (string) reader["FileName"];
                    item.ChildUnionId = null;
                    if (reader["ChildUnionId"] != DBNull.Value)
                        item.ChildUnionId = (long) reader["ChildUnionId"];
                    item.ClaimId = null;
                    if (reader["ClaimId"] != DBNull.Value)
                        item.ClaimId = (long)reader["ClaimId"];
                    item.UdodAgeGroupId = null;
                    if (reader["UdodAgeGroupId"] != DBNull.Value)
                        item.UdodAgeGroupId = (long)reader["UdodAgeGroupId"];
                    item.DateCreated = (DateTime) reader["DateCreated"];

                    item.IsError = (bool) reader["IsError"];
                    
                    list.Add(item);
                }
            }
            return list;
        }

        public List<ExtendedEventType> GetEventTypes()
        {
            List<ExtendedEventType> list = new List<ExtendedEventType>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetEventTypes", Connection) { CommandType = CommandType.StoredProcedure };
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    list.Add(new ExtendedEventType
                    {
                        EventTypeId = (int)reader["EventTypeId"],
                        Name = (string)reader["Name"]
                    });
                }
            }
            return list;
        }

        public List<ExtendedEvent> GetEvents(int? eventTypeId, DateTime? dateStart, DateTime? dateEnd, Guid? PupilId, long? UdodId, long? ChildUnionId)
        {
            List<ExtendedEvent> list = new List<ExtendedEvent>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetEvents", Connection) { CommandType = CommandType.StoredProcedure };
                if (eventTypeId.HasValue) comm.Parameters.Add(new SqlParameter("EventTypeId", eventTypeId.Value));
                if (dateStart.HasValue) comm.Parameters.Add(new SqlParameter("DateStart", dateStart.Value));
                if (dateEnd.HasValue) comm.Parameters.Add(new SqlParameter("DateEnd", dateEnd.Value));
                if (PupilId.HasValue) comm.Parameters.Add(new SqlParameter("PupilId", PupilId.Value));
                if (UdodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", UdodId.Value));
                if (ChildUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", ChildUnionId.Value));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    list.Add(new ExtendedEvent
                    {
                        EventId = (Int64)reader["EventId"],
                        EventDate = (DateTime)reader["EventDate"],
                        EventType = new ExtendedEventType { EventTypeId = (int)reader["EventTypeId"], Name = (string)reader["EventTypeName"] },
                        User = new ExtendedUser
                        {
                            UserId = (Guid)reader["UserId"],
                            UserName = (string)reader["UserName"],
                            FirstName = (string)reader["FirstName"],
                            LastName = (string)reader["LastName"],
                            MiddleName = (string)reader["MiddleName"]
                        },
                        RemoteIP = (string)reader["RemoteIP"],
                        Message = (string)reader["Message"],
                        
                        PupilId = (Guid)reader["PupilId"],
                        
                        Claim = new ExtendedClaim
                        {
                            ClaimId = (long)reader["ClaimId"],
                            Number = (long)reader["Number"],
                        },

                        Udod = new ExtendedUdod
                        {
                            UdodId = (long)reader["UdodId"],
                            ShortName = (string)reader["UdodName"],
                        },
                        ChildUnion = new ExtendedChildUnion
                        {
                            ChildUnionId = (long)reader["ChildUnionId"],
                            Name = (string)reader["ChildUnionName"]
                        }
                    });
                }
            }
            return list;
        }

        public List<ExtendedEvent> GetEventsPaging(int? eventTypeId, DateTime? dateStart, DateTime? dateEnd, Guid? PupilId, long? UdodId, long? ChildUnionId, string login, string message, int? PagerIndex, int? PagerLength)
        {
            List<ExtendedEvent> list = new List<ExtendedEvent>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetEventsPaging", Connection) { CommandType = CommandType.StoredProcedure };
                if (eventTypeId.HasValue) comm.Parameters.Add(new SqlParameter("EventTypeId", eventTypeId.Value));
                if (dateStart.HasValue) comm.Parameters.Add(new SqlParameter("DateStart", dateStart.Value));
                if (dateEnd.HasValue) comm.Parameters.Add(new SqlParameter("DateEnd", dateEnd.Value));
                if (PupilId.HasValue) comm.Parameters.Add(new SqlParameter("PupilId", PupilId.Value));
                if (UdodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", UdodId.Value));
                if (ChildUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", ChildUnionId.Value));
                if (!string.IsNullOrEmpty(login)) comm.Parameters.Add(new SqlParameter("Login", login));
                if (!string.IsNullOrEmpty(message)) comm.Parameters.Add(new SqlParameter("Message", message));
                if (PagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", PagerIndex));
                if (PagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", PagerLength));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    list.Add(new ExtendedEvent
                    {
                        EventId = (Int64)reader["EventId"],
                        EventDate = (DateTime)reader["EventDate"],
                        EventType = new ExtendedEventType { EventTypeId = (int)reader["EventTypeId"], Name = (string)reader["EventTypeName"] },
                        User = new ExtendedUser
                        {
                            UserId = (Guid)reader["UserId"],
                            UserName = (string)reader["UserName"],
                            FirstName = (string)reader["FirstName"],
                            LastName = (string)reader["LastName"],
                            MiddleName = (string)reader["MiddleName"]
                        },
                        RemoteIP = (string)reader["RemoteIP"],
                        Message = (string)reader["Message"],

                        PupilId = (Guid)reader["PupilId"],

                        Claim = new ExtendedClaim
                        {
                            ClaimId = (long)reader["ClaimId"],
                            Number = (long)reader["Number"],
                        },

                        Udod = new ExtendedUdod
                        {
                            UdodId = (long)reader["UdodId"],
                            ShortName = (string)reader["UdodName"],
                        },
                        ChildUnion = new ExtendedChildUnion
                        {
                            ChildUnionId = (long)reader["ChildUnionId"],
                            Name = (string)reader["ChildUnionName"]
                        }
                    });
                }
            }
            return list;
        }

        public int GetEventsCount(int? eventTypeId, DateTime? dateStart, DateTime? dateEnd, Guid? PupilId, long? UdodId, long? ChildUnionId, string login, string message, int? PagerIndex, int? PagerLength)
        {
            List<ExtendedEvent> list = new List<ExtendedEvent>();
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("GetEventsCount", Connection) { CommandType = CommandType.StoredProcedure };
                if (eventTypeId.HasValue) comm.Parameters.Add(new SqlParameter("EventTypeId", eventTypeId.Value));
                if (dateStart.HasValue) comm.Parameters.Add(new SqlParameter("DateStart", dateStart.Value));
                if (dateEnd.HasValue) comm.Parameters.Add(new SqlParameter("DateEnd", dateEnd.Value));
                if (PupilId.HasValue) comm.Parameters.Add(new SqlParameter("PupilId", PupilId.Value));
                if (UdodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", UdodId.Value));
                if (ChildUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", ChildUnionId.Value));
                if (!string.IsNullOrEmpty(login)) comm.Parameters.Add(new SqlParameter("Login", login));
                if (!string.IsNullOrEmpty(message)) comm.Parameters.Add(new SqlParameter("Message", message));
                if (PagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", PagerIndex));
                if (PagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", PagerLength));
                Connection.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }
            return 0;
        }

        public void AddEvent(EnumEventTypes eventType, Guid? userId, string remoteIP, string message, Guid? PupilId, long? ClaimId, long? UdodId, long? ChildUnionId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("AddEvent", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("EventTypeId", (int)eventType));
                comm.Parameters.Add(new SqlParameter("UserId", userId));
                comm.Parameters.Add(new SqlParameter("RemoteIP", remoteIP));
                comm.Parameters.Add(new SqlParameter("Message", message));
                if (PupilId.HasValue) comm.Parameters.Add(new SqlParameter("PupilId", PupilId.Value));
                if (ClaimId.HasValue) comm.Parameters.Add(new SqlParameter("ClaimId", ClaimId.Value));
                if (UdodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", UdodId.Value));
                if (ChildUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", ChildUnionId.Value));
                Connection.Open();
                comm.ExecuteNonQuery();
            }
        }

        public void AddException(long childUnionId, int? typeException, string exceptionText)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("AddExceptionESZ", Connection) { CommandType = CommandType.StoredProcedure };

                comm.Parameters.Add(new SqlParameter("@ChildUnionId", childUnionId));
                if (typeException.HasValue) comm.Parameters.Add(new SqlParameter("@Type", typeException));
                comm.Parameters.Add(new SqlParameter("@Error", exceptionText));
                Connection.Open();
                comm.ExecuteNonQuery();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Udod.Dal
{
    public class DictProgramCategoryDb : BaseDb
    {
        public DictProgramCategoryDb() { }

        public List<ExtendedProgramCategory> GetProgramCategories()
        {
            List<ExtendedProgramCategory> list = new List<ExtendedProgramCategory>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetProgramCategory", Connection) { CommandType = CommandType.StoredProcedure };
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedProgramCategory item = new ExtendedProgramCategory();
                    item.ProgramCategoryId = (int)reader["ProgramCategoryId"];
                    item.ProgramCategoryName = (string)reader["ProgramCategoryName"];
                    list.Add(item);
                }
            }

            return list;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Udod.Dal
{
    public class DictSectionDb : BaseDb
    {
        private int SectionsCount;

        public DictSectionDb() { }

        public List<ExtendedSection> GetSections(int? profileId, int? programId, long? udodId, string OrderBy)
        {
            List<ExtendedSection> list = new List<ExtendedSection>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetSection", Connection) { CommandType = CommandType.StoredProcedure };
                if (profileId.HasValue) comm.Parameters.Add(new SqlParameter("ProfileId", profileId.Value));
                if (programId.HasValue) comm.Parameters.Add(new SqlParameter("ProgramId", programId.Value));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (!string.IsNullOrEmpty(OrderBy)) comm.Parameters.Add(new SqlParameter("OrderBy", OrderBy));  

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedSection item = new ExtendedSection();
                    item.SectionId = (int)reader["SectionId"];
                    item.Name = (string)reader["Name"];
                    
                    item.isExistInCurrentUdod = !(reader["UdodSectionId"]==DBNull.Value);
                    if (udodId.HasValue)
                    {
                        item.isEnabledToChange = ((int)reader["CUCount"] == 0);
                        item.ChildUnionCount = (int)reader["CUCount"];
                    }
                    list.Add(item);
                }
            }

            SectionsCount = list.Count;
            return list;
        }

        public List<ExtendedSection> GetSections(string code, string name, int count)
        {
            List<ExtendedSection> list = new List<ExtendedSection>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetSectionNew", Connection) { CommandType = CommandType.StoredProcedure };
                if (!string.IsNullOrEmpty(code)) comm.Parameters.Add(new SqlParameter("code", Convert.ToInt32(code)));                
                comm.Parameters.Add(new SqlParameter("count", count));
                comm.Parameters.Add(new SqlParameter("name", name));

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedSection item = new ExtendedSection();
                    item.SectionId = (int)reader["SectionId"];
                    item.Name = (string)reader["Name"];

                    list.Add(item);
                }
            }

            //SectionsCount = list.Count;
            return list;
        }

        public List<ExtendedSection> GetSectionsFind(int? profileId, int? programId, long? udodId)
        {
            List<ExtendedSection> list = new List<ExtendedSection>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetSection", Connection) { CommandType = CommandType.StoredProcedure };
                if (profileId.HasValue && profileId.Value != -1) comm.Parameters.Add(new SqlParameter("ProfileId", profileId.Value));
                if (programId.HasValue && programId.Value != -1) comm.Parameters.Add(new SqlParameter("ProgramId", programId.Value));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedSection item = new ExtendedSection();
                    //item.SectionId = (int)reader["SectionId"];
                    item.Name = (string)reader["Name"];

                    item.isExistInCurrentUdod = !(reader["UdodSectionId"] == DBNull.Value);
                    list.Add(item);
                }
            }

            SectionsCount = list.Count;
            return list.Distinct().ToList();
        }

        public void AddSection(string Name, string ShortName, int LimitAge, int profileId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("AddDictSection", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Name", Name));
                comm.Parameters.Add(new SqlParameter("ShortName", ShortName));
                comm.Parameters.Add(new SqlParameter("LimitAge", LimitAge));
                comm.Parameters.Add(new SqlParameter("ProfileId", profileId));
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateSection(string Name, string ShortName, int LimitAge, long ProfileId, long SectionId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateDictSection", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Name", Name));
                comm.Parameters.Add(new SqlParameter("ShortName", ShortName));
                comm.Parameters.Add(new SqlParameter("LimitAge", LimitAge));
                comm.Parameters.Add(new SqlParameter("ProfileId", ProfileId));
                comm.Parameters.Add(new SqlParameter("SectionId", SectionId));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteSection(long SectionId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("DeleteDictSection", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("SectionId", SectionId));
                comm.ExecuteNonQuery();
            }
        }

        public ExtendedSection GetSection(int sectionId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetSection", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("SectionId", sectionId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedSection item = new ExtendedSection();
                    item.SectionId = (int)reader["SectionId"];
                    item.Name = (string)reader["Name"];
                    item.ShortName = (string)reader["ShortName"];
                    item.LimitAge = (int)reader["LimitAge"];
                    item.ProfileId = (int)reader["ProfileId"];
                    item.EszSectionId = (int)reader["EszSectionId"];
                    return item;
                }
            }
            return new ExtendedSection();
        }

        public int GetSectionsCount(int profileId, int programId, int udodId)
        {
            return SectionsCount;
        }
    }
}

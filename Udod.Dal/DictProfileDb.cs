﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Udod.Dal
{
    public class DictProfileDb : BaseDb
    {
        private int ProfilesCount;

        public DictProfileDb() { }

        public List<ExtendedProfile> GetProfiles(int? programId, long? udodId)
        {
            List<ExtendedProfile> list = new List<ExtendedProfile>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetProfile", Connection) { CommandType = CommandType.StoredProcedure };
                if (programId.HasValue) comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedProfile item = new ExtendedProfile();
                    item.ProfileId = (int)reader["ProfileId"];
                    item.Name = (string)reader["Name"];
                    list.Add(item);
                }
            }

            ProfilesCount = list.Count;
            return list;
        }

        public List<ExtendedProfile> GetProfiles(string code, string name, int count)
        {
            List<ExtendedProfile> list = new List<ExtendedProfile>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetProfileNew", Connection) { CommandType = CommandType.StoredProcedure };
                if (!string.IsNullOrEmpty(code)) comm.Parameters.Add(new SqlParameter("code", Convert.ToInt32(code)));
                comm.Parameters.Add(new SqlParameter("count", count));
                comm.Parameters.Add(new SqlParameter("name", name));
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedProfile item = new ExtendedProfile();
                    item.ProfileId = (int)reader["ProfileId"];
                    item.Name = (string)reader["Name"];
                    list.Add(item);
                }
            }

            //ProfilesCount = list.Count;
            return list;
        }

        public void AddProfile(string Name, int programId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("AddDictProfile", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Name", Name));
                comm.Parameters.Add(new SqlParameter("ProgramId", programId));
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateProfile(string Name, long ProgramId, long ProfileId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("UpdateDictProfile", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("Name", Name));
                comm.Parameters.Add(new SqlParameter("ProgramId", ProgramId));
                comm.Parameters.Add(new SqlParameter("ProfileId", ProfileId));
                comm.ExecuteNonQuery();
            }
        }

        public void DeleteProfile(long ProfileId)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("DeleteDictProfile", Connection) { CommandType = CommandType.StoredProcedure };
                comm.Parameters.Add(new SqlParameter("ProfileId", ProfileId));
                comm.ExecuteNonQuery();
            }
        }

        public ExtendedProfile? GetProfile(long? profileId, string profileName)
        {
            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetProfile", Connection) { CommandType = CommandType.StoredProcedure };
                if (profileId.HasValue)comm.Parameters.Add(new SqlParameter("ProfileId", profileId));
                if (!string.IsNullOrEmpty(profileName)) comm.Parameters.Add(new SqlParameter("ProfileName", profileName));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedProfile item = new ExtendedProfile();
                    item.ProfileId = (int)reader["ProfileId"];
                    item.Name = (string)reader["Name"];
                    item.ProgramId = (int)reader["ProgramId"];
                    return item;
                }
            }
            return null;
        }

        public int GetProfilesCount(int? ProgramId, long? udodId)
        {
            return ProfilesCount;
        }
    }
}

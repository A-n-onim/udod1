﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Udod.Dal
{
    public class TeacherDb :BaseDb
    {
        public List<ExtendedTeacher> GetTeacher(int? cityId, long? udodId, long? childUnionId)
        {
            List<ExtendedTeacher> list = new List<ExtendedTeacher>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetTeachers", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));

                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Guid _userId = (Guid) reader["UserId"];
                    int? _sectionId=null;
                    if (reader["SectionId"]!=DBNull.Value)
                    {
                        _sectionId = (int)reader["SectionId"];
                    }
                    

                    ExtendedTeacher last = list.LastOrDefault(i => i.UserId == _userId);

                    if (null != last)
                    {
                        if (_sectionId.HasValue && last.Sections.Count(i=>i.SectionId==_sectionId)==0)
                        {
                            var itemSection = new ExtendedSection();
                            itemSection.Name = (string)reader["SectionName"];
                            itemSection.SectionId = (int)reader["SectionId"];

                            (last.Sections as List<ExtendedSection>).Add(itemSection);
                        }
                    }
                    else
                    {
                        ExtendedTeacher item = new ExtendedTeacher();
                        item.UserId = _userId;
                        item.LastName = (string) reader["LastName"];
                        item.FirstName = (string) reader["FirstName"];
                        item.MiddleName = (string) reader["MiddleName"];
                        item.PhoneNumber = (string) reader["PhoneNumber"];
                        item.Birthday = (DateTime)reader["Birthday"];
                        item.EMail = (string)reader["EMail"];

                        item.Sections = new List<ExtendedSection>();

                        if (_sectionId.HasValue)
                        {
                            var itemSection = new ExtendedSection();
                            itemSection.Name = (string)reader["SectionName"];
                            itemSection.SectionId = (int)reader["SectionId"];
                            (item.Sections as List<ExtendedSection>).Add(itemSection);
                        }
                        list.Add(item);
                    }
                }

            }

            return list;
        }

        public List<ExtendedTeacher> GetTeacherPaging(int? cityId, long? udodId, long? childUnionId, int? PagerIndex, int? PagerLength)
        {
            List<ExtendedTeacher> list = new List<ExtendedTeacher>();

            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetTeachersPaging", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                if (childUnionId.HasValue) comm.Parameters.Add(new SqlParameter("ChildUnionId", childUnionId));
                if (PagerIndex.HasValue) comm.Parameters.Add(new SqlParameter("PagerIndex", PagerIndex));
                if (PagerLength.HasValue) comm.Parameters.Add(new SqlParameter("PagerLength", PagerLength));

                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Guid _userId = (Guid)reader["UserId"];
                    int? _sectionId = null;
                    if (reader["SectionId"] != DBNull.Value)
                    {
                        _sectionId = (int)reader["SectionId"];
                    }


                    ExtendedTeacher last = list.LastOrDefault(i => i.UserId == _userId);

                    if (null != last)
                    {
                        if (_sectionId.HasValue && last.Sections.Count(i => i.SectionId == _sectionId) == 0)
                        {
                            var itemSection = new ExtendedSection();
                            itemSection.Name = (string)reader["SectionName"];
                            itemSection.SectionId = (int)reader["SectionId"];

                            (last.Sections as List<ExtendedSection>).Add(itemSection);
                        }
                    }
                    else
                    {
                        ExtendedTeacher item = new ExtendedTeacher();
                        item.UserId = _userId;
                        item.LastName = (string)reader["LastName"];
                        item.FirstName = (string)reader["FirstName"];
                        item.MiddleName = (string)reader["MiddleName"];
                        item.PhoneNumber = (string)reader["PhoneNumber"];
                        item.Birthday = (DateTime)reader["Birthday"];
                        item.EMail = (string)reader["EMail"];

                        item.Sections = new List<ExtendedSection>();

                        if (_sectionId.HasValue)
                        {
                            var itemSection = new ExtendedSection();
                            itemSection.Name = (string)reader["SectionName"];
                            itemSection.SectionId = (int)reader["SectionId"];
                            (item.Sections as List<ExtendedSection>).Add(itemSection);
                        }
                        list.Add(item);
                    }
                }

            }

            return list;
        }

        public int GetTeacherCount(int? cityId, long? udodId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_GetTeachersCount", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                if (cityId.HasValue) comm.Parameters.Add(new SqlParameter("CityId", cityId));
                if (udodId.HasValue) comm.Parameters.Add(new SqlParameter("UdodId", udodId));

                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (int)reader[0];
                }
            }

            return 0;
        }

        public bool IsTeacherInChildUnion(Guid userId, long udodId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Teacher_GetChildUnionCount", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UserId", userId));
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    int count = (int) reader[0];
                    return count != 0;
                }
            }
            return false;
        }

        public void DeleteTeacherFromUdod(Guid userId, long udodId)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_DeleteTeacher", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UserId", userId));
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.ExecuteNonQuery();
            }
        }

        public void UpdateTeacher(Guid userId, string lastName, string firstName, string middleName)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_UpdateTeacher", conn) { CommandType = CommandType.StoredProcedure };
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UserId", userId));
                comm.Parameters.Add(new SqlParameter("LastName", lastName));
                comm.Parameters.Add(new SqlParameter("FirstName", firstName));
                comm.Parameters.Add(new SqlParameter("MiddleName", middleName));
                comm.ExecuteNonQuery();
            }
        }

        public void SetTeacherSection(long udodId, int sectionId, Guid userId, bool value)
        {
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Teacher_SetSection", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("userId", userId));
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("sectionId", sectionId));
                comm.Parameters.Add(new SqlParameter("Value", value));
                comm.ExecuteNonQuery();
            }
        }

        public Guid CreateTeacher(long udodId, string lastName, string firstName, string middleName)
        {
            Guid userid = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(Connect.SqlConnectionString))
            {
                SqlCommand comm = new SqlCommand("Udod_CreateTeacher", conn){CommandType = CommandType.StoredProcedure};
                conn.Open();
                comm.Parameters.Add(new SqlParameter("UdodId", udodId));
                comm.Parameters.Add(new SqlParameter("LastName", lastName));
                comm.Parameters.Add(new SqlParameter("FirstName", firstName));
                comm.Parameters.Add(new SqlParameter("MiddleName", middleName));
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    return (Guid) reader[0];
                }
            }
            return userid;
        }
    }
}

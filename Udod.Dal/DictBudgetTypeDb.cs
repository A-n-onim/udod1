﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Udod.Dal
{
    public class DictBudgetTypeDb : BaseDb
    {
        public DictBudgetTypeDb() {}

        public List<ExtendedTypeBudget> GetBudgetTypes()
        {
            List<ExtendedTypeBudget> list = new List<ExtendedTypeBudget>();

            using (SqlConnection Connection = new SqlConnection(Connect.SqlConnectionString))
            {
                Connection.Open();
                SqlCommand comm = new SqlCommand("Dict_GetBudgetType", Connection) { CommandType = CommandType.StoredProcedure };
                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    ExtendedTypeBudget item = new ExtendedTypeBudget();
                    item.DictTypeBudgetId = (int)reader["DictTypeBudgetId"];
                    item.TypeBudgetName = (string)reader["TypeBudgetName"];
                    list.Add(item);
                }
            }

            return list;
        }
    }
}

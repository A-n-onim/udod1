﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;

public partial class Login : Page
{
    private string ReturnUrl
    {
        get
        {
            string url = Request.QueryString["ReturnUrl"] ?? "Claim/NewClaim.aspx";

            return string.Concat(url, url.Contains("?") ? "&" : "?", "action=login");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Form.DefaultButton = lbEnter.UniqueID;
    }

    protected void lbEnter_Click(object sender, EventArgs e)
    {
        lblInfo.Text = "";
        string txtUser = tbLogin.Text;
        string txtPassword = tbPassword.Text;
        try
        {
            /*using (UserDb db = new UserDb())
            {
                ExtendedUser user = db.GetUser(txtUser);
                if (user.UserName == null)
                    throw new Exception();
            }
            */
            if (Membership.ValidateUser(txtUser, txtPassword))
            {
                var ticket = new FormsAuthenticationTicket(2, txtUser, DateTime.Now, DateTime.Now.AddDays(10), false, "",
                                                           FormsAuthentication.FormsCookiePath);
                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName)
                                 {
                                     HttpOnly = true,
                                     Path = FormsAuthentication.FormsCookiePath,
                                     Secure = FormsAuthentication.RequireSSL,
                                     Value = FormsAuthentication.Encrypt(ticket)
                                 };
                Response.Cookies.Clear();
                Response.Cookies.Add(cookie);
                Response.Redirect(ReturnUrl, true);
            }
            else
            {
            }
        }
        catch (Exception)
        {
            lblInfo.Text = "Неверный логин или пароль";
        }
    }
}
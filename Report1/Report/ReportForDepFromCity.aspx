﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportForDepFromCity.aspx.cs" MasterPageFile="../Master/MasterPage.master" Inherits="Report_ReportForDepFromCity" %>
<%@ Import Namespace="System.Globalization" %>

<asp:Content runat="server" ID="contentReport" ContentPlaceHolderID="body">
    <%-- <div style="font-size: 24px;">Комплектование учреждений дополнительного образования в разрезе округов</div>--%>
    <asp:UpdatePanel ID="upTitle" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Label runat="server" Font-Size="24px" ID="lblTitle" Text="Комплектование контингента учреждений дополнительного образования детей в разрезе округов" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="upLnk">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        Выбор типа учреждения:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlUdodType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUdodType_OnSelectedIndexChanged">
                            <Items>
                                <asp:ListItem runat="server" Value="0" Selected="true">УДО</asp:ListItem>
                                <asp:ListItem runat="server" Value="1">ОУ</asp:ListItem>
                                <asp:ListItem runat="server" Value="2">ДОУ</asp:ListItem>
                                <asp:ListItem runat="server" Value="3">СПО</asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </td>
                    <td>
                        Учебный год:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlTransferStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTransferStatus_OnSelectedIndexChanged">
                            <Items>
                                <asp:ListItem runat="server" Value="" Selected="true">Текущий</asp:ListItem>
                                <asp:ListItem runat="server" Value="2">Следующий</asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
                
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:LinkButton runat="server" ID="lnkExportInExcel" OnClick="lnkExportInExcel_OnClick"><div class="btnBlue">Экспорт в Excel</div></asp:LinkButton>
    <asp:UpdatePanel runat="server" ID="upGridView" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Repeater runat="server" ID="repReport" OnItemDataBound="repReport_OnItemDataBound">
                <HeaderTemplate>
                    <table  class="ReportTable" cellspacing="0px" style="width: 1400px;">
                    <tr class="reportHeader">
                        <td class="ReportTable" rowspan="2">Параметр</td>
                        <td class="ReportTable" colspan="2" style="width: 8.9%;">ЦАО</td>
                        <td class="ReportTable" colspan="2" style="width: 8.9%;">САО</td>
                        <td class="ReportTable" colspan="2" style="width: 8.9%;">СВАО</td>
                        <td class="ReportTable" colspan="2" style="width: 8.9%;">ВАО</td>
                        <td class="ReportTable" colspan="2" style="width: 8.9%;">ЮВАО</td>
                        <td class="ReportTable" colspan="2" style="width: 8.9%;">ЮАО</td>
                        <td class="ReportTable" colspan="2" style="width: 8.9%;">ЮЗАО</td>
                        <td class="ReportTable" colspan="2" style="width: 8.9%;">ЗАО</td>
                        <td class="ReportTable" colspan="2" style="width: 8.9%;">СЗАО</td>
                        <td class="ReportTable" colspan="2" style="width: 8.9%;">ЗелАО</td>
                    </tr>
                    <tr class="reportHeader">
                        <td class="ReportTable">Кол-во</td>
                        <td class="ReportTable" style="width: 65px;">%</td>
                        <td class="ReportTable">Кол-во</td>
                        <td class="ReportTable" style="width: 65px;">%</td>
                        <td class="ReportTable">Кол-во</td>
                        <td class="ReportTable" style="width: 65px;">%</td>
                        <td class="ReportTable">Кол-во</td>
                        <td class="ReportTable" style="width: 65px;">%</td>
                        <td class="ReportTable">Кол-во</td>
                        <td class="ReportTable" style="width: 65px;">%</td>
                        <td class="ReportTable">Кол-во</td>
                        <td class="ReportTable" style="width: 65px;">%</td>
                        <td class="ReportTable">Кол-во</td>
                        <td class="ReportTable" style="width: 65px;">%</td>
                        <td class="ReportTable">Кол-во</td>
                        <td class="ReportTable" style="width: 65px;">%</td>
                        <td class="ReportTable">Кол-во</td>
                        <td class="ReportTable" style="width: 65px;">%</td>
                        <td class="ReportTable">Кол-во</td>
                        <td class="ReportTable" style="width: 65px;">%</td>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr runat="server" ID="trId">
                        <td class="ReportTable"><%#Eval("Name")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("Counts[0]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                        <td class="ReportTable"><asp:Label runat="server" ID="lblFirst1"><%#Eval("Procents[0]").ToString() != "0" ? Eval("Procents[0]", "{0,5:P1}") : ""%></asp:Label> </td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("Counts[1]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                        <td class="ReportTable"><asp:Label runat="server" ID="lblFirst2"><%#Eval("Procents[1]").ToString() != "0" ? Eval("Procents[1]", "{0,5:P1}") : ""%></asp:Label></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("Counts[2]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                        <td class="ReportTable"><asp:Label runat="server" ID="lblFirst3"><%#Eval("Procents[2]").ToString() != "0" ? Eval("Procents[2]", "{0,5:P1}") : ""%></asp:Label></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("Counts[3]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                        <td class="ReportTable"><asp:Label runat="server" ID="lblFirst4"><%#Eval("Procents[3]").ToString() != "0" ? Eval("Procents[3]", "{0,5:P1}") : ""%></asp:Label></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("Counts[4]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                        <td class="ReportTable"><asp:Label runat="server" ID="lblFirst5"><%#Eval("Procents[4]").ToString() != "0" ? Eval("Procents[4]", "{0,5:P1}") : ""%></asp:Label></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("Counts[5]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                        <td class="ReportTable"><asp:Label runat="server" ID="lblFirst6"><%#Eval("Procents[5]").ToString() != "0" ? Eval("Procents[5]", "{0,5:P1}") : ""%></asp:Label></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("Counts[6]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                        <td class="ReportTable"><asp:Label runat="server" ID="lblFirst7"><%#Eval("Procents[6]").ToString() != "0" ? Eval("Procents[6]", "{0,5:P1}") : ""%></asp:Label></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("Counts[7]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                        <td class="ReportTable"><asp:Label runat="server" ID="lblFirst8"><%#Eval("Procents[7]").ToString() != "0" ? Eval("Procents[7]", "{0,5:P1}") : ""%></asp:Label></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("Counts[8]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                        <td class="ReportTable"><asp:Label runat="server" ID="lblFirst9"><%#Eval("Procents[8]").ToString() != "0" ? Eval("Procents[8]", "{0,5:P1}") : ""%></asp:Label></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("Counts[9]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                        <td class="ReportTable"><asp:Label runat="server" ID="lblFirst10"><%#Eval("Procents[9]").ToString() != "0" ? Eval("Procents[9]", "{0,5:P1}") : ""%></asp:Label></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                </table>
                </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class Report_ReportClaim : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void cbTypeUdod_OnCheckedChanged(object sender, EventArgs e)
    {
        cblUdodType.Enabled = cbTypeUdod.Checked;
    }

    protected void cblCity_OnDataBound(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            foreach (ListItem item in cblCity.Items)
            {
                item.Selected = true;
            }
        }
    }

    protected void lnkFind_OnClick(object sender, EventArgs e)
    {
        using (var db = new ReportDb())
        {
            var sb = new MyStringBuilder(", ");

            if (cbTypeUdod.Checked)
            {
                foreach (ListItem item in cblUdodType.Items)
                {
                    if (item.Selected)
                    {
                        sb.Append(item.Value);
                    }
                }
            }
            var sb1 = new MyStringBuilder(", ");
            if (rbTypeReport.SelectedValue != "0")
            {
                foreach (ListItem item in cblCity.Items)
                {
                    if (item.Selected)
                    {
                        sb1.Append(item.Value);
                    }
                }
            }

            var dateBegin = new DateTime(2013, 08, 16);
            DateTime dateEnd = DateTime.Now;
            try
            {
                dateBegin = Convert.ToDateTime(tbDateStart.Text, CultureInfo.GetCultureInfo("ru-ru"));
            }
            catch (Exception)
            {
            }
            try
            {
                dateEnd = Convert.ToDateTime(tbDateEnd.Text, CultureInfo.GetCultureInfo("ru-ru"));
            }
            catch (Exception)
            {
            }

            long? udodid = null;
            try
            {
                udodid = Convert.ToInt64(ddlUdod.SelectedValue);
            }
            catch (Exception)
            {
            }

            if (string.IsNullOrEmpty(tbDateStart.Text)) tbDateStart.Text = new DateTime(2013, 08, 16).ToString("dd.MM.yyyy", CultureInfo.GetCultureInfo("ru-ru"));
            if (string.IsNullOrEmpty(tbDateEnd.Text)) tbDateEnd.Text = DateTime.Now.ToString("dd.MM.yyyy", CultureInfo.GetCultureInfo("ru-ru")); ;

            List<ExtendedClaimReport> list = db.GetClaimCountReport(Convert.ToInt32(rbTypeReport.SelectedValue),
                                                                    !cbTypeUdod.Checked
                                                                        ? ""
                                                                        : string.IsNullOrEmpty(sb.ToString())
                                                                              ? "-1"
                                                                              : sb.ToString(), sb1.ToString(), udodid,
                                                                    dateBegin, dateEnd);

            List<ExtendedAgeReport> udods = db.GetUdodCountReport(Convert.ToInt32(rbTypeReport.SelectedValue),
                                                                  !cbTypeUdod.Checked
                                                                      ? ""
                                                                      : string.IsNullOrEmpty(sb.ToString())
                                                                            ? "-1"
                                                                            : sb.ToString(), sb1.ToString(), udodid);

            var sumClaim = new ExtendedClaimReport();
            sumClaim.CityName = "ИТОГО";
            sumClaim.UdodTypeId = -1;
            foreach (ExtendedClaimReport report in list)
            {
                ExtendedAgeReport udod = udods.FirstOrDefault(
                    i => i.CityId == report.CityId && i.UdodId == report.UdodId && i.UdodTypeId == report.UdodTypeId);
                if (udod != null)
                {
                    report.CountUdod = udod.CountUdod;
                    report.CountUdodIsDo = udod.CountUdodIsDo;
                }

                sumClaim.CountClaimAnnul0 += report.CountClaimAnnul0;
                sumClaim.CountClaimAnnul1 += report.CountClaimAnnul1;
                sumClaim.CountClaimAnnul2 += report.CountClaimAnnul2;
                sumClaim.CountClaimAnnul3 += report.CountClaimAnnul3;
                sumClaim.CountClaimPGU += report.CountClaimPGU;
                sumClaim.CountClaimStatus1 += report.CountClaimStatus1;
                sumClaim.CountClaimStatus7 += report.CountClaimStatus7;
                sumClaim.CountClaimStatus4 += report.CountClaimStatus4;
                sumClaim.CountClaimStatus5 += report.CountClaimStatus5;
                sumClaim.CountClaimStatus6 += report.CountClaimStatus6;
                sumClaim.CountClaimUdod += report.CountClaimUdod;
                sumClaim.CountDONotPublic += report.CountDONotPublic;
                sumClaim.CountDOPublic += report.CountDOPublic;
                sumClaim.CountUdod += report.CountUdod;
                sumClaim.CountUdodIsDo += report.CountUdodIsDo;
                sumClaim.SumClaim += report.SumClaim;
                sumClaim.SumDO += report.SumDO;
            }

            if (rbTypeReport.SelectedIndex != 0)
            {
                list.Add(sumClaim);
            }

            repReport.DataSource = list;
                //db.GetAgeReport(Convert.ToInt32(rbTypeReport.SelectedValue), sb.ToString(), sb1.ToString(), null);
            repReport.DataBind();
        }
        upReport.Update();
    }

    protected void rbTypeReport_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        cblCity.Enabled = rbTypeReport.SelectedValue != "0";
        tbUdodFilter.Enabled = ddlUdod.Enabled = rbTypeReport.SelectedIndex == 2;
    }

    protected void tbUdodFilter_OnTextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(tbUdodFilter.Text))
        {
            using (var db = new UdodDb())
            {
                var sb = new MyStringBuilder(", ");

                if (cbTypeUdod.Checked)
                {
                    foreach (ListItem item in cblUdodType.Items)
                    {
                        if (item.Selected)
                        {
                            sb.Append(item.Value);
                        }
                    }
                }
                var sb1 = new MyStringBuilder(", ");
                if (rbTypeReport.SelectedIndex!=0)
                {
                    foreach (ListItem item in cblCity.Items)
                    {
                        if (item.Selected)
                        {
                            sb1.Append(item.Value);
                        }
                    }
                }

                List<ExtendedUdod> list = db.GetUdodsForReport(sb1.ToString(), tbUdodFilter.Text, sb.ToString());
                if (list.Count == 0) list.Add(new ExtendedUdod { UdodId = -1, ShortName = "Учреждение не найдено" });
                ddlUdod.DataSource = list;
                ddlUdod.DataBind();
            }
        }
        else
        {
            ddlUdod.Items.Clear();
        }
    }

    protected void cblCity_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        tbUdodFilter.Text = "";
        tbUdodFilter_OnTextChanged(tbUdodFilter, new EventArgs());
    }

    protected void cblUdodType_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        tbUdodFilter.Text = "";
        tbUdodFilter_OnTextChanged(tbUdodFilter, new EventArgs());
    }


    protected void lnkExport_OnClick(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("Report.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                               string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        var workbook = new Aspose.Cells.Workbook();
        workbook.Open(MapPath("../Templates/ReportClaim.xlsx"));
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];

        #region Стили

        Aspose.Cells.Style style_area = workbook.Styles[0];
        Aspose.Cells.Style style_header = workbook.Styles[1];
        style_area.ShrinkToFit = true;

        style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.Black;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.Black;
        style_header.Font.IsBold = true;
        style_header.Font.Size = 12;
        style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
        style_header.BackgroundColor = Color.Gray;
        style_header.Pattern = Aspose.Cells.BackgroundType.Solid;

        #endregion

        using (ReportDb db = new ReportDb())
        {
            var sb = new MyStringBuilder(", ");

            if (cbTypeUdod.Checked)
            {
                foreach (ListItem item in cblUdodType.Items)
                {
                    if (item.Selected)
                    {
                        sb.Append(item.Value);
                    }
                }
            }
            var sb1 = new MyStringBuilder(", ");
            if (rbTypeReport.SelectedValue != "0")
            {
                foreach (ListItem item in cblCity.Items)
                {
                    if (item.Selected)
                    {
                        sb1.Append(item.Value);
                    }
                }
            }

            var dateBegin = new DateTime(2013, 08, 16);
            DateTime dateEnd = DateTime.Now;
            try
            {
                dateBegin = Convert.ToDateTime(tbDateStart.Text, CultureInfo.GetCultureInfo("ru-ru"));
            }
            catch (Exception)
            {
            }
            try
            {
                dateEnd = Convert.ToDateTime(tbDateEnd.Text, CultureInfo.GetCultureInfo("ru-ru"));
            }
            catch (Exception)
            {
            }

            long? udodid = null;
            try
            {
                udodid = Convert.ToInt64(ddlUdod.SelectedValue);
            }
            catch (Exception)
            {
            }
            List<ExtendedClaimReport> list = db.GetClaimCountReport(Convert.ToInt32(rbTypeReport.SelectedValue),
                                                                    !cbTypeUdod.Checked
                                                                        ? ""
                                                                        : string.IsNullOrEmpty(sb.ToString())
                                                                              ? "-1"
                                                                              : sb.ToString(), sb1.ToString(), udodid,
                                                                    dateBegin, dateEnd);

            List<ExtendedAgeReport> udods = db.GetUdodCountReport(Convert.ToInt32(rbTypeReport.SelectedValue),
                                                                  !cbTypeUdod.Checked
                                                                      ? ""
                                                                      : string.IsNullOrEmpty(sb.ToString())
                                                                            ? "-1"
                                                                            : sb.ToString(), sb1.ToString(), udodid);

            var sumClaim = new ExtendedClaimReport();
            sumClaim.CityName = "ИТОГО";
            sumClaim.UdodTypeId = -1;
            foreach (ExtendedClaimReport report in list)
            {
                ExtendedAgeReport udod = udods.FirstOrDefault(
                    i => i.CityId == report.CityId && i.UdodId == report.UdodId && i.UdodTypeId == report.UdodTypeId);
                if (udod != null)
                {
                    report.CountUdod = udod.CountUdod;
                    report.CountUdodIsDo = udod.CountUdodIsDo;
                }

                sumClaim.CountClaimAnnul0 += report.CountClaimAnnul0;
                sumClaim.CountClaimAnnul1 += report.CountClaimAnnul1;
                sumClaim.CountClaimAnnul2 += report.CountClaimAnnul2;
                sumClaim.CountClaimAnnul3 += report.CountClaimAnnul3;
                sumClaim.CountClaimPGU += report.CountClaimPGU;
                sumClaim.CountClaimStatus1 += report.CountClaimStatus1;
                sumClaim.CountClaimStatus7 += report.CountClaimStatus7;
                sumClaim.CountClaimStatus4 += report.CountClaimStatus4;
                sumClaim.CountClaimStatus5 += report.CountClaimStatus5;
                sumClaim.CountClaimStatus6 += report.CountClaimStatus6;
                sumClaim.CountClaimUdod += report.CountClaimUdod;
                sumClaim.CountDONotPublic += report.CountDONotPublic;
                sumClaim.CountDOPublic += report.CountDOPublic;
                sumClaim.CountUdod += report.CountUdod;
                sumClaim.CountUdodIsDo += report.CountUdodIsDo;
                sumClaim.SumClaim += report.SumClaim;
                sumClaim.SumDO += report.SumDO;
            }

            if (rbTypeReport.SelectedIndex != 0)
            {
                list.Add(sumClaim);
            }

            int k = 3;
            foreach (var report in list)
            {
                sheet.Cells[k, 0].PutValue(report.CityName);
                sheet.Cells[k, 1].PutValue(report.CountUdod);
                sheet.Cells[k, 2].PutValue(report.CountUdodIsDo);
                sheet.Cells[k, 3].PutValue(report.UdodTypeId == 0 ? "УДО" : report.UdodTypeId == 1 ? "ОУ" : report.UdodTypeId == 2 ? "ДОУ" : report.UdodTypeId == 3 ? "СПО" : "");
                sheet.Cells[k, 4].PutValue(report.UdodName);
                sheet.Cells[k, 5].PutValue(report.SumDO);
                sheet.Cells[k, 6].PutValue(report.CountDOPublic);
                sheet.Cells[k, 7].PutValue(report.CountDONotPublic);
                sheet.Cells[k, 8].PutValue(report.SumClaim);
                sheet.Cells[k, 9].PutValue(report.CountClaimPGU);
                sheet.Cells[k, 10].PutValue(report.CountClaimUdod);
                sheet.Cells[k, 11].PutValue(report.CountClaimStatus1);
                sheet.Cells[k, 12].PutValue(report.CountClaimStatus7);
                sheet.Cells[k, 13].PutValue(report.CountClaimStatus6);
                sheet.Cells[k, 14].PutValue(report.CountClaimStatus4);
                sheet.Cells[k, 15].PutValue(report.CountClaimStatus5);
                sheet.Cells[k, 16].PutValue(report.CountClaimAnnul1);
                sheet.Cells[k, 17].PutValue(report.CountClaimAnnul2);
                sheet.Cells[k, 18].PutValue(report.CountClaimAnnul3);
                sheet.Cells[k, 19].PutValue(report.CountClaimAnnul0);
                k++;
            }
            sheet.Cells.CreateRange(3, 0, list.Count, 20).ApplyStyle(style_area, new Aspose.Cells.StyleFlag { All = true });
        }


        workbook.Save("ReportClaim.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }
}
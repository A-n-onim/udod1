﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportForDepFromUdod.aspx.cs" MasterPageFile="../Master/MasterPage.master" Inherits="Report_ReportForDepFromUdod" %>
<%@ Import Namespace="System.Globalization" %>

<asp:Content runat="server" ID="content" ContentPlaceHolderID="body">
    <link href="../Css/defaultTheme.css" rel="stylesheet" media="screen" />
    <link href="../css/myTheme.css" rel="stylesheet" media="screen" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"> </script>
    <script type="text/javascript" src="../Scripts/jquery.fixedheadertable.js"> </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#reportTableId').fixedHeaderTable({ altClass: 'odd', footer: true, fixedColumns: 3 });
            $('.unvisibleCell').each(function() {
                $(this).attr('display', 'none');
            });
        });

        function bindReady(sender, args) {
            //$('#reportTableId').fixedHeaderTable({ altClass:'odd', autoShow: false, footer: false, cloneHeadToFoot: false, fixedColumns: 2 });
        }
        //Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(bindReady);
    </script>
    <%-- <div style="font-size: 24px;">Комплектование учреждений дополнительного образования в разрезе учреждений</div>--%>
    <asp:UpdatePanel ID="upTitle" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Label runat="server" Font-Size="24px" ID="lblTitle" Text="Комплектование контингента учреждений дополнительного образования детей в разрезе учреждений" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="upLnk">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        Выбор типа учреждения:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlUdodType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUdodType_OnSelectedIndexChanged">
                            <Items>
                                <asp:ListItem runat="server" Value="0" Selected="true">УДО</asp:ListItem>
                                <asp:ListItem runat="server" Value="1">ОУ</asp:ListItem>
                                <asp:ListItem runat="server" Value="2">ДОУ</asp:ListItem>
                                <asp:ListItem runat="server" Value="3">СПО</asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </td>
                    <td>
                        Учебный год:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlTransferStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTransferStatus_OnSelectedIndexChanged">
                            <Items>
                                <asp:ListItem runat="server" Value="" Selected="true">Текущий</asp:ListItem>
                                <asp:ListItem runat="server" Value="2">Следующий</asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
                
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:LinkButton runat="server" ID="lnkExportInExcel" OnClick="lnkExportInExcel_OnClick"><div class="btnBlue">Экспорт в Excel</div></asp:LinkButton>
    <asp:UpdatePanel runat="server" ID="upGridView" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="grid_8 height600">
                <asp:Repeater runat="server" ID="repReport" >
                    <HeaderTemplate>
                        <table id="reportTableId" class="fancyTable" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr class="reportHeader">
                                <th rowspan="2" class="d1">Округ<div class="fht-cell"></div></th>
                                <th rowspan="2" class="d1">Наименование учреждения<div class="fht-cell"></div></th>
                                <th rowspan="2" class="d1">Есть блок ДО<div class="fht-cell"></div></th>
                                <th rowspan="2" class="d1">Кол-во человек (обучающихся) внесённых в Систему<div class="fht-cell"></div></th>
                                <th rowspan="2" class="d1">Кол-во человек, подавших заявления<div class="fht-cell"></div></th>
                                <th colspan="5" >Кол-во человек, зачисленных в детские объед.</th>
                                <th colspan="3" >Кол-во человек, зачисленных в детские объед.</th>
                                <th colspan="5" >Кол-во человек, зачисленных в детские объед.</th>
                                <th colspan="2" >Общее кол-во зачислений по виду обучения</th>
                                <th colspan="2" >Общее кол-во часов (для зачисленных в детские объед.)</th>
                                <th colspan="7" >Сумма зачислений по годам обучения</th>
                                <th colspan="7" >Кол-во воспитанников по годам обучения</th>
                                <th colspan="13" >Кол-во обучающихся по направленностям</th>
                            </tr>
                            <tr class="reportHeader">
                                <th class="d1">в 1 детское объед.<div class="fht-cell"></div></th>
                                <th class="d1">в 2 детских объед.<div class="fht-cell"></div></th>
                                <th class="d1">в 3 детских объед.<div class="fht-cell"></div></th>
                                <th class="d1">в 4 и более детских объед.<div class="fht-cell"></div></th>
                                <th class="d1">Итого<div class="fht-cell"></div></th>
                                <th class="d1">зарег. в г. Москва<div class="fht-cell"></div></th>
                                <th class="d1">зарег. в др. рег.<div class="fht-cell"></div></th>
                                <th class="d1">Итого<div class="fht-cell"></div></th>
                                <th class="d1">до 6 лет<div class="fht-cell"></div></th>
                                <th class="d1">6-10 лет<div class="fht-cell"></div></th>
                                <th class="d1">11-18 лет<div class="fht-cell"></div></th>
                                <th class="d1">старше 18 лет<div class="fht-cell"></div></th>
                                <th class="d1">Итого<div class="fht-cell"></div></th>
                                <th class="d1">бесплатно<div class="fht-cell"></div></th>
                                <th class="d1">платно<div class="fht-cell"></div></th>
                                <th class="d1">бесплатно<div class="fht-cell"></div></th>
                                <th class="d1">платно<div class="fht-cell"></div></th>
                                <th class="d1">1 год<div class="fht-cell"></div></th>
                                <th class="d1">2 год<div class="fht-cell"></div></th>
                                <th class="d1">3 год<div class="fht-cell"></div></th>
                                <th class="d1">4 год<div class="fht-cell"></div></th>
                                <th class="d1">5 год<div class="fht-cell"></div></th>
                                <th class="d1">6 и более<div class="fht-cell"></div></th>
                                <th class="d1">Итого<div class="fht-cell"></div></th>
                                <th class="d1">1 год<div class="fht-cell"></div></th>
                                <th class="d1">2 год<div class="fht-cell"></div></th>
                                <th class="d1">3 год<div class="fht-cell"></div></th>
                                <th class="d1">4 год<div class="fht-cell"></div></th>
                                <th class="d1">5 год<div class="fht-cell"></div></th>
                                <th class="d1">6 и более<div class="fht-cell"></div></th>
                                <th class="d1">Итого<div class="fht-cell"></div></th>
                                <th class="d1">Военно-патриотическая<div class="fht-cell"></div></th>
                                <th class="d1">Естественнонаучная<div class="fht-cell"></div></th>
                                <th class="d1">Иное<div class="fht-cell"></div></th>
                                <th class="d1">Культурологическая<div class="fht-cell"></div></th>
                                <th class="d1">Научно-техническая<div class="fht-cell"></div></th>
                                <th class="d1">Социально-педагогическая<div class="fht-cell"></div></th>
                                <th class="d1">Социально-экономическая<div class="fht-cell"></div></th>
                                <th class="d1">Спортивно-техническая<div class="fht-cell"></div></th>
                                <th class="d1">Туристско-краеведческая<div class="fht-cell"></div></th>
                                <th class="d1">Физкультурно-спортивная<div class="fht-cell"></div></th>
                                <th class="d1">Художественно-эстетическая<div class="fht-cell"></div></th>
                                <th class="d1">Эколого-биологическая<div class="fht-cell"></div></th>
                                <th class="d1">Итого<div class="fht-cell"></div></th>
                            </tr>
                        </thead>
                        <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr style="height: 60px;">
                            <td><%#Eval("NameCity")%></td>
                            <td><%#Eval("NameUdod")%></td>
                            <td><%#Eval("IsDOEnabled")%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[0]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[1]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[2]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[3]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[4]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[5]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[6]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[7]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[8]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[9]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[10]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[11]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[12]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[13]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[14]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[15]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[16]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[17]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[18]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[19]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[20]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[21]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[22]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[23]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[24]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[25]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[26]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[27]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[28]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[29]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[30]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[31]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[32]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[33]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[34]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[35]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[36]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[37]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[38]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[39]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[40]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[41]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[42]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[43]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[44]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                            <td ><%#Convert.ToInt32(Eval("Counts[45]")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                    </tbody>
                    </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportClaim.aspx.cs" MasterPageFile="../Master/MasterPage.master" Inherits="Report_ReportClaim" %>

<asp:Content runat="server" ID="content" ContentPlaceHolderID="body">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Label runat="server" Font-Size="24px" ID="lblTitle" Text="Состояние обработки заявлений на программы дополнительного образования детей" />
            <table class="ReportTable">
                <tr>
                    <td class="ReportTable">Выбор типа отчета</td>
                    <td class="ReportTable">
                        Выбрать отдельный округ
                    </td>
                    <td class="ReportTable">
                        <asp:CheckBox runat="server" ID="cbTypeUdod" Text="Разделить по типу ОУ" AutoPostBack="true" OnCheckedChanged="cbTypeUdod_OnCheckedChanged"/>
                    </td>
                    <td class="ReportTable">
                        Выбрать учреждение
                    </td>
                    <td colspan="2" class="ReportTable">Построить отчёт за период</td>
                </tr>
                <tr>
                    <td class="ReportTable">
                        <asp:RadioButtonList runat="server" ID="rbTypeReport" AutoPostBack="true" OnSelectedIndexChanged="rbTypeReport_OnSelectedIndexChanged">
                            <Items>
                                <asp:ListItem Value="0" Selected="true">По городу</asp:ListItem>
                                <asp:ListItem Value="1">По округам</asp:ListItem>
                                <asp:ListItem Value="2">По учреждениям</asp:ListItem>
                            </Items>
                        </asp:RadioButtonList>
                    </td>
                    <td class="ReportTable">
                        <div style="height: 150px; width: 200px; overflow: auto;">
                            <asp:CheckBoxList runat="server" ID="cblCity" AutoPostBack="true" Enabled="false" RepeatColumns="2" DataSourceID="dsCity" DataTextField="ShortName" DataValueField="CityId" OnDataBound="cblCity_OnDataBound" OnSelectedIndexChanged="cblCity_OnSelectedIndexChanged"/>
                        </div>
                    </td>
                    <td class="ReportTable">
                        Выбор типа ОУ 
                        <asp:CheckBoxList runat="server" ID="cblUdodType" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="cblUdodType_OnSelectedIndexChanged">
                            <Items>
                                <asp:ListItem Value="0" Selected="true">УДО</asp:ListItem>
                                <asp:ListItem Value="1" Selected="true">ОУ</asp:ListItem>
                                <asp:ListItem Value="2" Selected="true">ДОУ</asp:ListItem>
                                <asp:ListItem Value="3" Selected="true">СПО</asp:ListItem>
                            </Items>
                        </asp:CheckBoxList>
                    </td>
                    <td class="ReportTable">
                        <asp:TextBox runat="server" ID="tbUdodFilter" AutoPostBack="true" Enabled="false" OnTextChanged="tbUdodFilter_OnTextChanged"></asp:TextBox>
                        <br/>
                        <asp:DropDownList CssClass="input" runat="server" ID="ddlUdod" Enabled="false" DataTextField="ShortName" DataValueField="UdodId" />
                    </td>
                    <td class="ReportTable">
                        <asp:TextBox runat="server" ID="tbDateStart" CssClass="inputXShort" />
                        <ajaxToolkit:CalendarExtender runat="server" ID="calendarDateStart" TargetControlID="tbDateStart" Format="dd.MM.yyyy"/>
                    </td>
                    <td class="ReportTable">
                        <asp:TextBox runat="server" ID="tbDateEnd" CssClass="inputXShort" />
                        <ajaxToolkit:CalendarExtender runat="server" ID="calendarDateEnd" TargetControlID="tbDateEnd" Format="dd.MM.yyyy"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" class="ReportTable" align="right">
                        <asp:LinkButton runat="server" ID="lnkFind" OnClick="lnkFind_OnClick"><div class="btnBlue">Построить отчет</div></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
<asp:LinkButton runat="server" ID="lnkExport" OnClick="lnkExport_OnClick"><div class="btnBlue">Экспорт в Excel</div></asp:LinkButton>
    <asp:UpdatePanel runat="server" ID="upReport" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Repeater runat="server" ID="repReport" >
                <HeaderTemplate>
                    <table  class="ReportTable" cellspacing="0px" style="width: 950px; margin-right: 50px;">
                    <tr class="reportHeader">
                        <td class="ReportTable" rowspan="3">Округ</td>
                        <td class="ReportTable" rowspan="3">Кол-во ОУ</td>
                        <td class="ReportTable" rowspan="3">Кол-во ОУ, имеющих блок ДО</td>
                        <td class="ReportTable" rowspan="3">Тип ОУ</td>
                        <td class="ReportTable" rowspan="3">Наименование ОУ</td>
                        <td class="ReportTable" colspan="3">Кол-во ДО</td>
                        <td class="ReportTable" colspan="3">Кол-во принятых заявлений</td>
                        <td class="ReportTable" colspan="4">Кол-во заявлений в статусе:</td>
                        <td class="ReportTable" colspan="5">Кол-во аннулированных заявлений:</td>
                    </tr>
                    <tr class="reportHeader">
                        <td class="ReportTable" rowspan="2">Всего</td>
                        <td class="ReportTable" rowspan="2">Опубл. на ПГУ</td>
                        <td class="ReportTable" rowspan="2">Не опубл. на ПГУ</td>
                        <td class="ReportTable" rowspan="2">Всего</td>
                        <td class="ReportTable" rowspan="2">Через ПГУ</td>
                        <td class="ReportTable" rowspan="2">Через ведомств. систему</td>
                        <td class="ReportTable" rowspan="2">На рассм.</td>
                        <td class="ReportTable" rowspan="2">Док-ты поданы</td>
                        <td class="ReportTable" rowspan="2">В резерв</td>
                        <td class="ReportTable" rowspan="2">Зачислено</td>
                        <td class="ReportTable" rowspan="2">Всего</td>
                        <td class="ReportTable" colspan="3">оператором по причине:</td>
                        <td class="ReportTable" rowspan="2">в связи с неявкой заявителя в учреждение</td>
                    </tr>
                    <tr class="reportHeader">
                        <td class="ReportTable">предоставление недостоверных данных</td>
                        <td class="ReportTable">отсутствие мест И отказ от резерва и другого ДО</td>
                        <td class="ReportTable">предоставлено место в другом ДО</td>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="ReportTable"><%#Eval("CityName")%></td>
                        <td class="ReportTable"><%#Eval("CountUdod")%></td>
                        <td class="ReportTable"><%#Eval("CountUdodIsDo")%></td>
                        <td class="ReportTable"><%#Eval("UdodTypeId").ToString() == "0"
                                 ? "УДО"
                                 : Eval("UdodTypeId").ToString() == "1"
                                       ? "ОУ"
                                       : Eval("UdodTypeId").ToString() == "2"
                                             ? "ДОУ"
                                             : Eval("UdodTypeId").ToString() == "3" ? "СПО" : ""%></td>
                        <td class="ReportTable"><%#Eval("UdodName")%></td>
                        <td class="ReportTable"><%#Eval("SumDO")%></td>
                        <td class="ReportTable"><%#Eval("CountDOPublic")%></td>
                        <td class="ReportTable"><%#Eval("CountDONotPublic")%></td>
                        <td class="ReportTable"><%#Eval("SumClaim")%></td>
                        <td class="ReportTable"><%#Eval("CountClaimPGU")%></td>
                        <td class="ReportTable"><%#Eval("CountClaimUdod")%></td>
                        <td class="ReportTable"><%#Eval("CountClaimStatus1")%></td>
                        <td class="ReportTable"><%#Eval("CountClaimStatus7")%></td>
                        <td class="ReportTable"><%#Eval("CountClaimStatus6")%></td>
                        <td class="ReportTable"><%#Eval("CountClaimStatus4")%></td>
                        <td class="ReportTable"><%#Eval("CountClaimStatus5")%></td>
                        <td class="ReportTable"><%#Eval("CountClaimAnnul1")%></td>
                        <td class="ReportTable"><%#Eval("CountClaimAnnul2")%></td>
                        <td class="ReportTable"><%#Eval("CountClaimAnnul3")%></td>
                        <td class="ReportTable"><%#Eval("CountClaimAnnul0")%></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                </table>
                </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:ObjectDataSource runat="server" ID="dsCity" TypeName="Udod.Dal.DictCityDb" SelectMethod="GetCities"></asp:ObjectDataSource>
</asp:Content>
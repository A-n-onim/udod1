﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class Report_ReportForDepFromCity : Page
{
    private void Recalculate(ref List<ExtendedReportDepEvalCity> list)
    {
        for (int i = 0; i < 56; i++)
        {
            var item = new ExtendedReportDepEvalCity();
            item.Counts = new int[10];
            item.Procents = new float[10];
            switch (i)
            {
                case 0:
                    item.Name = "<b>Общее кол-во учреждений</b>";
                    break;
                case 1:
                    item.Name =
                        "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во человек (обучающихся) внесённых в Систему</b></div>";
                    break;
                case 2:
                    item.Name = "&nbsp;&nbsp;&nbsp;через МПГУ";
                    break;
                case 3:
                    item.Name = "&nbsp;&nbsp;&nbsp;через ОСИП";
                    break;
                case 4:
                    item.Name = "&nbsp;&nbsp;&nbsp;через учреждение";
                    break;
                case 5:
                    item.Name =
                        "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во будущих воспитанников по заявлениям</b></div>";
                    break;
                case 6:
                    item.Name = "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во заявителей</b></div>";
                    break;
                case 7:
                    item.Name = "&nbsp;&nbsp;&nbsp;Родитель/Законный представитель";
                    break;
                case 8:
                    item.Name = "&nbsp;&nbsp;&nbsp;Ребенок, старше 14 лет";
                    break;
                case 9:
                    item.Name =
                        "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во человек, зачисленных в детские объед.</b></div>";
                    break;
                case 10:
                    item.Name = "&nbsp;&nbsp;&nbsp;в 1 детское объед.";
                    break;
                case 11:
                    item.Name = "&nbsp;&nbsp;&nbsp;в 2 детских объед.";
                    break;
                case 12:
                    item.Name = "&nbsp;&nbsp;&nbsp;в 3 детских объед.";
                    break;
                case 13:
                    item.Name = "&nbsp;&nbsp;&nbsp;в 4 и более детских объед.";
                    break;
                case 14:
                    item.Name =
                        "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во человек, зачисленных в детские объед.</b></div>";
                    break;
                case 15:
                    item.Name = "&nbsp;&nbsp;&nbsp;зарег. в г. Москва";
                    break;
                case 16:
                    item.Name = "&nbsp;&nbsp;&nbsp;зарег. в др. рег.";
                    break;
                case 17:
                    item.Name =
                        "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во человек, зачисленных в детские объед.</b></div>";
                    break;
                case 18:
                    item.Name = "&nbsp;&nbsp;&nbsp;до 6 лет";
                    break;
                case 19:
                    item.Name = "&nbsp;&nbsp;&nbsp;6-10 лет";
                    break;
                case 20:
                    item.Name = "&nbsp;&nbsp;&nbsp;11-18 лет";
                    break;
                case 21:
                    item.Name = "&nbsp;&nbsp;&nbsp;старше 18 лет";
                    break;
                case 22:
                    item.Name =
                        "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Общее кол-во зачислений по виду обучения</b></div>";
                    break;
                case 23:
                    item.Name = "&nbsp;&nbsp;&nbsp;бесплатно";
                    break;
                case 24:
                    item.Name = "&nbsp;&nbsp;&nbsp;платно";
                    break;
                case 25:
                    item.Name =
                        "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Общее кол-во часов (для зачисленных в детские объед.)</b></div>";
                    break;
                case 26:
                    item.Name = "&nbsp;&nbsp;&nbsp;бесплатно";
                    break;
                case 27:
                    item.Name = "&nbsp;&nbsp;&nbsp;платно";
                    break;
                case 28:
                    item.Name =
                        "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Сумма зачислений по годам обучения</b></div>";
                    break;
                case 29:
                    item.Name = "&nbsp;&nbsp;&nbsp;1 год";
                    break;
                case 30:
                    item.Name = "&nbsp;&nbsp;&nbsp;2 год";
                    break;
                case 31:
                    item.Name = "&nbsp;&nbsp;&nbsp;3 год";
                    break;
                case 32:
                    item.Name = "&nbsp;&nbsp;&nbsp;4 год";
                    break;
                case 33:
                    item.Name = "&nbsp;&nbsp;&nbsp;5 год";
                    break;
                case 34:
                    item.Name = "&nbsp;&nbsp;&nbsp;6 и более";
                    break;
                case 35:
                    item.Name =
                        "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во воспитанников по годам обучения</b></div>";
                    break;
                case 36:
                    item.Name = "&nbsp;&nbsp;&nbsp;1 год";
                    break;
                case 37:
                    item.Name = "&nbsp;&nbsp;&nbsp;2 год";
                    break;
                case 38:
                    item.Name = "&nbsp;&nbsp;&nbsp;3 год";
                    break;
                case 39:
                    item.Name = "&nbsp;&nbsp;&nbsp;4 год";
                    break;
                case 40:
                    item.Name = "&nbsp;&nbsp;&nbsp;5 год";
                    break;
                case 41:
                    item.Name = "&nbsp;&nbsp;&nbsp;6 и более";
                    break;
                case 42:
                    item.Name =
                        "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во обучающихся по направленностям</b></div>";
                    break;
                case 43:
                    item.Name = "&nbsp;&nbsp;&nbsp;Военно-патриотическая";
                    break;
                case 44:
                    item.Name = "&nbsp;&nbsp;&nbsp;Естественнонаучная";
                    break;
                case 45:
                    item.Name = "&nbsp;&nbsp;&nbsp;Иное";
                    break;
                case 46:
                    item.Name = "&nbsp;&nbsp;&nbsp;Культурологическая";
                    break;
                case 47:
                    item.Name = "&nbsp;&nbsp;&nbsp;Научно-техническая";
                    break;
                case 48:
                    item.Name = "&nbsp;&nbsp;&nbsp;Социально-педагогическая";
                    break;
                case 49:
                    item.Name = "&nbsp;&nbsp;&nbsp;Социально-экономическая";
                    break;
                case 50:
                    item.Name = "&nbsp;&nbsp;&nbsp;Спортивно-техническая";
                    break;
                case 51:
                    item.Name = "&nbsp;&nbsp;&nbsp;Туристско-краеведческая";
                    break;
                case 52:
                    item.Name = "&nbsp;&nbsp;&nbsp;Физкультурно-спортивная";
                    break;
                case 53:
                    item.Name = "&nbsp;&nbsp;&nbsp;Художественно-эстетическая";
                    break;
                case 54:
                    item.Name = "&nbsp;&nbsp;&nbsp;Эколого-биологическая";
                    break;
                case 55:
                    item.Name = "&nbsp;&nbsp;&nbsp;в том числе имеющих блок ДО";
                    break;
                default:
                    break;
            }

            list.Add(item);
        }
        using (var db = new ReportDb())
        {
            int? transferStatus;
            try
            {
                transferStatus = Convert.ToInt32(ddlTransferStatus.SelectedValue);
            }
            catch
            {
                transferStatus = null;
            }
            List<ExtendedReportDepFromCity> source = db.GetReportDepFromCity(
                Convert.ToInt32(ddlUdodType.SelectedValue), transferStatus);
            int j = 0;
            for (int k = 0; k < 10; k++)
            {
                int i = 0;
                for (int l = 0; l < 56; l++)
                {
                    list[l].Counts[k] = source[k].Counts[l];
                    switch (i)
                    {
                        case 0:
                            list[l].Procents[k] = 1;
                            break;
                        case 1:
                            list[l].Procents[k] = 1;
                            break;
                        case 2:
                            if (source[k].Counts[l - 1] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 1];
                            else list[l].Procents[k] = 0;
                            break;
                        case 3:
                            if (source[k].Counts[l - 2] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 2];
                            else list[l].Procents[k] = 0;
                            break;
                        case 4:
                            if (source[k].Counts[l - 3] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 3];
                            else list[l].Procents[k] = 0;
                            break;
                        case 5:
                            list[l].Procents[k] = 1;
                            break;
                        case 6:
                            if (source[k].Counts[l - 1] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 1];
                            else list[l].Procents[k] = 0;
                            break;
                        case 7:
                            if (source[k].Counts[l - 2] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 2];
                            else list[l].Procents[k] = 0;
                            break;
                        case 8:
                            if (source[k].Counts[l - 3] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 3];
                            else list[l].Procents[k] = 0;
                            break;
                        case 9:
                            list[l].Procents[k] = 1;
                            break;
                        case 10:
                            if (source[k].Counts[l - 1] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 1];
                            else list[l].Procents[k] = 0;
                            break;
                        case 11:
                            if (source[k].Counts[l - 2] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 2];
                            else list[l].Procents[k] = 0;
                            break;
                        case 12:
                            if (source[k].Counts[l - 3] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 3];
                            else list[l].Procents[k] = 0;
                            break;
                        case 13:
                            if (source[k].Counts[l - 4] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 4];
                            else list[l].Procents[k] = 0;
                            break;
                        case 14:
                            list[l].Procents[k] = 1;
                            break;
                        case 15:
                            if (source[k].Counts[l - 1] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 1];
                            else list[l].Procents[k] = 0;
                            break;
                        case 16:
                            if (source[k].Counts[l - 2] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 2];
                            else list[l].Procents[k] = 0;
                            break;
                        case 17:
                            list[l].Procents[k] = 1;
                            break;
                        case 18:
                            if (source[k].Counts[l - 1] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 1];
                            else list[l].Procents[k] = 0;
                            break;
                        case 19:
                            if (source[k].Counts[l - 2] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 2];
                            else list[l].Procents[k] = 0;
                            break;
                        case 20:
                            if (source[k].Counts[l - 3] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 3];
                            else list[l].Procents[k] = 0;
                            break;
                        case 21:
                            if (source[k].Counts[l - 4] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 4];
                            else list[l].Procents[k] = 0;
                            break;
                        case 22:
                            list[l].Procents[k] = 1;
                            break;
                        case 23:
                            if (source[k].Counts[l - 1] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 1];
                            else list[l].Procents[k] = 0;
                            break;
                        case 24:
                            if (source[k].Counts[l - 2] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 2];
                            else list[l].Procents[k] = 0;
                            break;
                        case 25:
                            list[l].Procents[k] = 1;
                            break;
                        case 26:
                            if (source[k].Counts[l - 1] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 1];
                            else list[l].Procents[k] = 0;
                            break;
                        case 27:
                            if (source[k].Counts[l - 2] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 2];
                            else list[l].Procents[k] = 0;
                            break;
                        case 28:
                            list[l].Procents[k] = 1;
                            break;
                        case 29:
                            if (source[k].Counts[l - 1] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 1];
                            else list[l].Procents[k] = 0;
                            break;
                        case 30:
                            if (source[k].Counts[l - 2] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 2];
                            else list[l].Procents[k] = 0;
                            break;
                        case 31:
                            if (source[k].Counts[l - 3] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 3];
                            else list[l].Procents[k] = 0;
                            break;
                        case 32:
                            if (source[k].Counts[l - 4] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 4];
                            else list[l].Procents[k] = 0;
                            break;
                        case 33:
                            if (source[k].Counts[l - 5] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 5];
                            else list[l].Procents[k] = 0;
                            break;
                        case 34:
                            if (source[k].Counts[l - 6] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 6];
                            else list[l].Procents[k] = 0;
                            break;
                        case 35:
                            list[l].Procents[k] = 1;
                            break;
                        case 36:
                            if (source[k].Counts[l - 1] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 1];
                            else list[l].Procents[k] = 0;
                            break;
                        case 37:
                            if (source[k].Counts[l - 2] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 2];
                            else list[l].Procents[k] = 0;
                            break;
                        case 38:
                            if (source[k].Counts[l - 3] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 3];
                            else list[l].Procents[k] = 0;
                            break;
                        case 39:
                            if (source[k].Counts[l - 4] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 4];
                            else list[l].Procents[k] = 0;
                            break;
                        case 40:
                            if (source[k].Counts[l - 5] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 5];
                            else list[l].Procents[k] = 0;
                            break;
                        case 41:
                            if (source[k].Counts[l - 6] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 6];
                            else list[l].Procents[k] = 0;
                            break;
                        case 42:
                            list[l].Procents[k] = 1;
                            break;
                        case 43:
                            if (source[k].Counts[l - 1] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 1];
                            else list[l].Procents[k] = 0;
                            break;
                        case 44:
                            if (source[k].Counts[l - 2] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 2];
                            else list[l].Procents[k] = 0;
                            break;
                        case 45:
                            if (source[k].Counts[l - 3] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 3];
                            else list[l].Procents[k] = 0;
                            break;
                        case 46:
                            if (source[k].Counts[l - 4] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 4];
                            else list[l].Procents[k] = 0;
                            break;
                        case 47:
                            if (source[k].Counts[l - 5] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 5];
                            else list[l].Procents[k] = 0;
                            break;
                        case 48:
                            if (source[k].Counts[l - 6] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 6];
                            else list[l].Procents[k] = 0;
                            break;
                        case 49:
                            if (source[k].Counts[l - 7] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 7];
                            else list[l].Procents[k] = 0;
                            break;
                        case 50:
                            if (source[k].Counts[l - 8] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 8];
                            else list[l].Procents[k] = 0;
                            break;
                        case 51:
                            if (source[k].Counts[l - 9] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 9];
                            else list[l].Procents[k] = 0;
                            break;
                        case 52:
                            if (source[k].Counts[l - 10] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 10];
                            else list[l].Procents[k] = 0;
                            break;
                        case 53:
                            if (source[k].Counts[l - 11] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 11];
                            else list[l].Procents[k] = 0;
                            break;
                        case 54:
                            if (source[k].Counts[l - 12] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 12];
                            else list[l].Procents[k] = 0;
                            break;
                        case 55:
                            if (source[k].Counts[l - 55] != 0)
                                list[l].Procents[k] = source[k].Counts[l]/(float) source[k].Counts[l - 55];
                            else list[l].Procents[k] = 0;
                            break;
                        default:
                            break;
                    }
                    i++;
                }
                j++;
            }
        }

        list.Insert(1, list.Last()); // дополнительное поле БДО переносим из конца наверх
        list.RemoveAt(list.Count - 1);

        // убираем пункты Кол-во будущих воспитанников по заявлениям и Кол-во заявителей
        for (int i = 9; i >= 6; i--)
            list.RemoveAt(i);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ddlUdodType.DataBind();
        if (!Page.IsPostBack)
        {
            var list = new List<ExtendedReportDepEvalCity>();

            Recalculate(ref list);

            repReport.DataSource = list;
            repReport.DataBind();
        }
    }

    protected void repReport_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            if (e.Item.ItemIndex == 0 || e.Item.ItemIndex == 2 || e.Item.ItemIndex == 6 || e.Item.ItemIndex == 11 ||
                e.Item.ItemIndex == 14 || e.Item.ItemIndex == 19 || e.Item.ItemIndex == 22 || e.Item.ItemIndex == 25 ||
                e.Item.ItemIndex == 32 || e.Item.ItemIndex == 39)
            {
                (e.Item.FindControl("lblFirst1") as Label).Text =
                    (e.Item.FindControl("lblFirst2") as Label).Text =
                    (e.Item.FindControl("lblFirst3") as Label).Text =
                    (e.Item.FindControl("lblFirst4") as Label).Text =
                    (e.Item.FindControl("lblFirst5") as Label).Text =
                    (e.Item.FindControl("lblFirst6") as Label).Text =
                    (e.Item.FindControl("lblFirst7") as Label).Text =
                    (e.Item.FindControl("lblFirst8") as Label).Text =
                    (e.Item.FindControl("lblFirst9") as Label).Text =
                    (e.Item.FindControl("lblFirst10") as Label).Text = "";
            }
            if (e.Item.ItemIndex == 0 || e.Item.ItemIndex == 2 || e.Item.ItemIndex == 6 || e.Item.ItemIndex == 11 ||
                e.Item.ItemIndex == 14 || e.Item.ItemIndex == 19 || e.Item.ItemIndex == 22 || e.Item.ItemIndex == 25 ||
                e.Item.ItemIndex == 32 || e.Item.ItemIndex == 39)
            {
                foreach (HtmlTableCell cell in (e.Item.FindControl("trId") as HtmlTableRow).Cells)
                {
                    cell.Attributes.Add("class", "ReportRowTop");
                }
                //Style.Add("border-top", "2px solid black;");
            }
        }
    }

    protected void lnkExportInExcel_OnClick(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("Report.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                               string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        var workbook = new Aspose.Cells.Workbook();
        workbook.Open(MapPath("../Templates/ReportCity.xlsx"));
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];

        #region Стили

        Aspose.Cells.Style style_area = workbook.Styles[0];
        Aspose.Cells.Style style_header = workbook.Styles[1];
        style_area.ShrinkToFit = true;

        style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.Black;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.Black;
        style_header.Font.IsBold = true;
        style_header.Font.Size = 12;
        style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
        style_header.BackgroundColor = Color.Gray;
        style_header.Pattern = Aspose.Cells.BackgroundType.Solid;

        #endregion

        var list = new List<ExtendedReportDepEvalCity>();

        Recalculate(ref list);
        int i = 2;
        sheet.Cells.CreateRange(2, 0, list.Count, 21).ApplyStyle(style_area, new Aspose.Cells.StyleFlag {All = true});
        foreach (ExtendedReportDepEvalCity param in list)
        {
            string paramName = param.Name.Replace("&nbsp;", "");
            if (DeleteTag(ref paramName))
            {
                sheet.Cells[i, 0].Style.Font.IsBold = true;
            }
            sheet.Cells[i, 0].PutValue(paramName);
            sheet.Cells[i, 1].PutValue(param.Counts[0]);
            sheet.Cells[i, 2].PutValue(param.Procents[0].ToString("P1"));
            sheet.Cells[i, 3].PutValue(param.Counts[1]);
            sheet.Cells[i, 4].PutValue(param.Procents[1].ToString("P1"));
            sheet.Cells[i, 5].PutValue(param.Counts[2]);
            sheet.Cells[i, 6].PutValue(param.Procents[2].ToString("P1"));
            sheet.Cells[i, 7].PutValue(param.Counts[3]);
            sheet.Cells[i, 8].PutValue(param.Procents[3].ToString("P1"));
            sheet.Cells[i, 9].PutValue(param.Counts[4]);
            sheet.Cells[i, 10].PutValue(param.Procents[4].ToString("P1"));
            sheet.Cells[i, 11].PutValue(param.Counts[5]);
            sheet.Cells[i, 12].PutValue(param.Procents[5].ToString("P1"));
            sheet.Cells[i, 13].PutValue(param.Counts[6]);
            sheet.Cells[i, 14].PutValue(param.Procents[6].ToString("P1"));
            sheet.Cells[i, 15].PutValue(param.Counts[7]);
            sheet.Cells[i, 16].PutValue(param.Procents[7].ToString("P1"));
            sheet.Cells[i, 17].PutValue(param.Counts[8]);
            sheet.Cells[i, 18].PutValue(param.Procents[8].ToString("P1"));
            sheet.Cells[i, 19].PutValue(param.Counts[9]);
            sheet.Cells[i, 20].PutValue(param.Procents[9].ToString("P1"));

            i++;
        }


        workbook.Save("Report.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }

    private bool DeleteTag(ref string paramName)
    {
        var regex = new Regex(@">[^<]+<");
        MatchCollection matches = regex.Matches(paramName);
        if (matches.Count > 0)
        {
            paramName = matches[0].Value.Replace("<", "").Replace(">", "");
            return true;
        }
        else return false;
    }

    protected void ddlUdodType_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlUdodType.SelectedValue == "1")
        {
            lblTitle.Text =
                "Комплектование контингента блока дополнительного образования в общеобразовательных учреждениях в разрезе округов";
        }
        else
        {
            lblTitle.Text = "Комплектование контингента учреждений дополнительного образования детей в разрезе округов";
        }

        var list = new List<ExtendedReportDepEvalCity>();

        Recalculate(ref list);

        repReport.DataSource = list;
        repReport.DataBind();
        upGridView.Update();
    }

    protected void ddlTransferStatus_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        var list = new List<ExtendedReportDepEvalCity>();

        Recalculate(ref list);

        repReport.DataSource = list;
        repReport.DataBind();
        upGridView.Update();
    }
}
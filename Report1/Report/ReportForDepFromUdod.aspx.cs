﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web;
using System.Web.UI;
using Udod.Dal;

public partial class Report_ReportForDepFromUdod : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ddlUdodType.DataBind();
        if (!Page.IsPostBack)
        {
            using (var db = new ReportDb())
            {
                int? transferStatus;
                try
                {
                    transferStatus = Convert.ToInt32(ddlTransferStatus.SelectedValue);
                }
                catch
                {
                    transferStatus = null;
                }

                List<ExtendedReportDepFromCity> list =
                    db.GetReportDepFromUdod(Convert.ToInt32(ddlUdodType.SelectedValue), transferStatus);
                repReport.DataSource = list;
                repReport.DataBind();
            }
        }
    }

    protected void lnkExportInExcel_OnClick(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("Report.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                               string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        var workbook = new Aspose.Cells.Workbook();
        workbook.Open(MapPath("../Templates/ReportUdod.xlsx"));
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];

        #region Стили

        Aspose.Cells.Style style_area = workbook.Styles[0];
        Aspose.Cells.Style style_header = workbook.Styles[1];
        style_area.ShrinkToFit = true;

        style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.Black;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.Black;
        style_header.Font.IsBold = true;
        style_header.Font.Size = 12;
        style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
        style_header.BackgroundColor = Color.Gray;
        style_header.Pattern = Aspose.Cells.BackgroundType.Solid;

        #endregion

        using (var db = new ReportDb())
        {
            int? transferStatus;
            try
            {
                transferStatus = Convert.ToInt32(ddlTransferStatus.SelectedValue);
            }
            catch
            {
                transferStatus = null;
            }

            List<ExtendedReportDepFromCity> list = db.GetReportDepFromUdod(Convert.ToInt32(ddlUdodType.SelectedValue),
                                                                           transferStatus);
            int i = 2;
            foreach (ExtendedReportDepFromCity udod in list)
            {
                sheet.Cells[i, 0].PutValue(udod.NameCity);
                sheet.Cells[i, 1].PutValue(udod.NameUdod);
                sheet.Cells[i, 2].PutValue(udod.IsDOEnabled);
                sheet.Cells[i, 3].PutValue(udod.Counts[0]);
                sheet.Cells[i, 4].PutValue(udod.Counts[1]);
                sheet.Cells[i, 5].PutValue(udod.Counts[2]);
                sheet.Cells[i, 6].PutValue(udod.Counts[3]);
                sheet.Cells[i, 7].PutValue(udod.Counts[4]);
                sheet.Cells[i, 8].PutValue(udod.Counts[5]);
                sheet.Cells[i, 9].PutValue(udod.Counts[6]);
                sheet.Cells[i, 10].PutValue(udod.Counts[7]);
                sheet.Cells[i, 11].PutValue(udod.Counts[8]);
                sheet.Cells[i, 12].PutValue(udod.Counts[9]);
                sheet.Cells[i, 13].PutValue(udod.Counts[10]);
                sheet.Cells[i, 14].PutValue(udod.Counts[11]);
                sheet.Cells[i, 15].PutValue(udod.Counts[12]);
                sheet.Cells[i, 16].PutValue(udod.Counts[13]);
                sheet.Cells[i, 17].PutValue(udod.Counts[14]);
                sheet.Cells[i, 18].PutValue(udod.Counts[15]);
                sheet.Cells[i, 19].PutValue(udod.Counts[16]);
                sheet.Cells[i, 20].PutValue(udod.Counts[17]);
                sheet.Cells[i, 21].PutValue(udod.Counts[18]);
                sheet.Cells[i, 22].PutValue(udod.Counts[19]);
                sheet.Cells[i, 23].PutValue(udod.Counts[20]);
                sheet.Cells[i, 24].PutValue(udod.Counts[21]);
                sheet.Cells[i, 25].PutValue(udod.Counts[22]);
                sheet.Cells[i, 26].PutValue(udod.Counts[23]);
                sheet.Cells[i, 27].PutValue(udod.Counts[24]);
                sheet.Cells[i, 28].PutValue(udod.Counts[25]);
                sheet.Cells[i, 29].PutValue(udod.Counts[26]);
                sheet.Cells[i, 30].PutValue(udod.Counts[27]);
                sheet.Cells[i, 31].PutValue(udod.Counts[28]);
                sheet.Cells[i, 32].PutValue(udod.Counts[29]);
                sheet.Cells[i, 33].PutValue(udod.Counts[30]);
                sheet.Cells[i, 34].PutValue(udod.Counts[31]);
                sheet.Cells[i, 35].PutValue(udod.Counts[32]);
                sheet.Cells[i, 36].PutValue(udod.Counts[33]);
                sheet.Cells[i, 37].PutValue(udod.Counts[34]);
                sheet.Cells[i, 38].PutValue(udod.Counts[35]);
                sheet.Cells[i, 39].PutValue(udod.Counts[36]);
                sheet.Cells[i, 40].PutValue(udod.Counts[37]);
                sheet.Cells[i, 41].PutValue(udod.Counts[38]);
                sheet.Cells[i, 42].PutValue(udod.Counts[39]);
                sheet.Cells[i, 43].PutValue(udod.Counts[40]);
                sheet.Cells[i, 44].PutValue(udod.Counts[41]);
                sheet.Cells[i, 45].PutValue(udod.Counts[42]);
                sheet.Cells[i, 46].PutValue(udod.Counts[43]);
                sheet.Cells[i, 47].PutValue(udod.Counts[44]);
                sheet.Cells[i, 48].PutValue(udod.Counts[45]);
                i++;
            }
            sheet.Cells.CreateRange(2, 0, i - 2, 49).ApplyStyle(style_area, new Aspose.Cells.StyleFlag {All = true});
        }
        workbook.Save("Report.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }

    protected void ddlUdodType_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlUdodType.SelectedValue == "1")
        {
            lblTitle.Text =
                "Комплектование контингента блока дополнительного образования в общеобразовательных учреждениях в разрезе учреждений";
        }
        else
        {
            lblTitle.Text =
                "Комплектование контингента учреждений дополнительного образования детей в разрезе учреждений";
        }
        using (var db = new ReportDb())
        {
            int? transferStatus;
            try
            {
                transferStatus = Convert.ToInt32(ddlTransferStatus.SelectedValue);
            }
            catch
            {
                transferStatus = null;
            }

            List<ExtendedReportDepFromCity> list = db.GetReportDepFromUdod(Convert.ToInt32(ddlUdodType.SelectedValue),
                                                                           transferStatus);
            repReport.DataSource = list;
            repReport.DataBind();
            upGridView.Update();
        }
    }

    protected void ddlTransferStatus_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        using (var db = new ReportDb())
        {
            int? transferStatus;
            try
            {
                transferStatus = Convert.ToInt32(ddlTransferStatus.SelectedValue);
            }
            catch
            {
                transferStatus = null;
            }

            List<ExtendedReportDepFromCity> list = db.GetReportDepFromUdod(Convert.ToInt32(ddlUdodType.SelectedValue),
                                                                           transferStatus);
            repReport.DataSource = list;
            repReport.DataBind();
            upGridView.Update();
        }
    }
}
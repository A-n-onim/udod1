﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class Report_ReportLoad : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lnkLoadMegaReport_OnClick(object sender, EventArgs e)
    {
        using (ReportDb db = new ReportDb())
        {
            HttpContext.Current.Response.Clear();

            string filename = string.Format("Report.xls");

            HttpContext.Current.Response.AddHeader("content-disposition",
                                                   string.Format("attachment; filename={0}", filename));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            var workbook = new Aspose.Cells.Workbook();
            workbook.Open(MapPath("../Templates/ReportMegaReport.xlsx"));
            Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];

            #region Стили

            Aspose.Cells.Style style_area = workbook.Styles[0];
            Aspose.Cells.Style style_header = workbook.Styles[1];
            style_area.ShrinkToFit = true;

            style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
            style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.Black;
            style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
            style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.Black;
            style_header.Font.IsBold = true;
            style_header.Font.Size = 12;
            style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
            style_header.BackgroundColor = Color.Gray;
            style_header.Pattern = Aspose.Cells.BackgroundType.Solid;

            #endregion
            List<ExtendedMegaReport> list = db.GetMegaReport();
            int k = 2;
            foreach (var report in list)
            {
                sheet.Cells[k, 0].PutValue(k-1);
                sheet.Cells[k, 1].PutValue(report.CityName);
                sheet.Cells[k, 2].PutValue(report.ParentName);
                sheet.Cells[k, 3].PutValue(report.UdodName);
                sheet.Cells[k, 4].PutValue(report.CountDO);
                sheet.Cells[k, 5].PutValue(report.IsRec);
                sheet.Cells[k, 6].PutValue(report.isPublic);
                sheet.Cells[k, 7].PutValue(report.del_pupil);
                sheet.Cells[k, 8].PutValue(report.del_zach);
                sheet.Cells[k, 9].PutValue(report.zach_pupil);
                sheet.Cells[k, 10].PutValue(report.zach_zach);
                sheet.Cells[k, 11].PutValue(report.count_pupils);
                sheet.Cells[k, 12].PutValue(report.count_not_ou);
                sheet.Cells[k, 13].PutValue(report.all_pupils);

                k++;
            }

            sheet.Cells.CreateRange(3, 0, list.Count+3, 14).ApplyStyle(style_area, new Aspose.Cells.StyleFlag { All = true });
            workbook.Save("ReportMegaReport.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
            HttpContext.Current.Response.End();
        }
    }

    protected void lnkLoadMegaReport2_OnClick(object sender, EventArgs e)
    {
        using (ReportDb db = new ReportDb())
        {
            HttpContext.Current.Response.Clear();

            string filename = string.Format("Report.xls");

            HttpContext.Current.Response.AddHeader("content-disposition",
                                                   string.Format("attachment; filename={0}", filename));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            var workbook = new Aspose.Cells.Workbook();
            workbook.Open(MapPath("../Templates/ReportMegaReport2.xlsx"));
            Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];

            #region Стили

            Aspose.Cells.Style style_area = workbook.Styles[0];
            Aspose.Cells.Style style_header = workbook.Styles[1];
            style_area.ShrinkToFit = true;

            style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
            style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.Black;
            style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
            style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.Black;
            style_header.Font.IsBold = true;
            style_header.Font.Size = 12;
            style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
            style_header.BackgroundColor = Color.Gray;
            style_header.Pattern = Aspose.Cells.BackgroundType.Solid;

            #endregion

            List<ExtendedMegaReport> list = db.GetMegaReport2();
            int k = 1;
            foreach (var report in list)
            {
                sheet.Cells[k, 0].PutValue(k);
                sheet.Cells[k, 1].PutValue(report.CityName);
                sheet.Cells[k, 2].PutValue(report.ParentName);
                sheet.Cells[k, 3].PutValue(report.UdodName);
                sheet.Cells[k, 4].PutValue(report.all_pupils);

                k++;
            }

            sheet.Cells.CreateRange(3, 0, list.Count + 3, 5).ApplyStyle(style_area,
                                                                        new Aspose.Cells.StyleFlag {All = true});
            workbook.Save("ReportMegaReport2.xls", Aspose.Cells.FileFormatType.Excel2003,
                          Aspose.Cells.SaveType.OpenInExcel, Response);
            HttpContext.Current.Response.End();
        }
    }
}
﻿using System;
using System.Web.UI;

public partial class Master_MasterPage : MasterPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        //imgHeader.ImageUrl = SiteUtility.GetUrl("~/img/header.png");
        //imgFooter.ImageUrl = SiteUtility.GetUrl("~/img/Footer.png");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        aMain.HRef = SiteUtility.GetUrl("~/Default.aspx");
        aReport.HRef = SiteUtility.GetUrl("~/Report/ReportForDepFromCity.aspx");
        aReportUdod.HRef = SiteUtility.GetUrl("~/Report/ReportForDepFromUdod.aspx");
        aReportAge.HRef = SiteUtility.GetUrl("~/Report/ReportAge.aspx");
        aReportClaim.HRef = SiteUtility.GetUrl("~/Report/ReportClaim.aspx");
        aReportAgeSex.HRef = SiteUtility.GetUrl("~/Report/ReportAgeSex.aspx");
        aLoad.HRef = SiteUtility.GetUrl("~/Report/ReportLoad.aspx");
    }

    protected void lnkLogout_OnClick(object sender, EventArgs e)
    {
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Data.SqlClient;
using System.Reflection;
using System.Security.Permissions;
using System.Web;
using System.Web.Hosting;
using System.Web.Profile;
using Udod.Dal;

namespace ReportSite.Providers
{
    [AspNetHostingPermission(SecurityAction.LinkDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    [AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    public sealed class CustomProfileProvider : ProfileProvider
    {
        //
        // Global connection string, generic exception message, event log info.
        //

        private string connectionString;
        private string eventLog = "Application";
        private string eventSource = "CustomProfileProvider";
        private string exceptionMessage = "An exception occurred. Please check the event log.";


        //
        // System.Configuration.SettingsProvider.ApplicationName
        //

        private string pApplicationName;
        public bool WriteExceptionsToEventLog { get; set; }

        public override string ApplicationName
        {
            get { return pApplicationName; }
            set { pApplicationName = value; }
        }

        public override void Initialize(string name, NameValueCollection config)
        {
            //
            // Initialize values from web.config.
            //

            if (config == null)
                throw new ArgumentNullException("config");

            if (name == null || name.Length == 0)
                name = "CustomProfileProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Custom Profile provider");
            }

            // Initialize the abstract base class.
            base.Initialize(name, config);


            if (config["applicationName"] == null || config["applicationName"].Trim() == "")
            {
                pApplicationName = HostingEnvironment.ApplicationVirtualPath;
            }
            else
            {
                pApplicationName = config["applicationName"];
            }


            //
            // Initialize connection string.
            //

            ConnectionStringSettings pConnectionStringSettings = ConfigurationManager.
                ConnectionStrings[config["connectionStringName"]];

            if (pConnectionStringSettings == null ||
                pConnectionStringSettings.ConnectionString.Trim() == "")
            {
                throw new ProviderException("Connection string cannot be blank.");
            }

            connectionString = pConnectionStringSettings.ConnectionString;
        }

        //
        // SettingsProvider.GetPropertyValues
        //

        public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context,
                                                                          SettingsPropertyCollection ppc)
        {
            var username = (string) context["UserName"];

            // The serializeAs attribute is ignored in this provider implementation.

            var svc = new SettingsPropertyValueCollection();

            var profile = new ExtendedUser();
            using (var db = new UserDb())
            {
                profile = db.GetUser(username);
            }

            foreach (SettingsProperty prop in ppc)
            {
                var pv = new SettingsPropertyValue(prop);

                pv.PropertyValue = GetPropertyValue(profile, pv.Name);

                svc.Add(pv);
            }

            return svc;
        }


        //
        // SettingsProvider.SetPropertyValues
        //

        public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection ppvc)
        {
            // The serializeAs attribute is ignored in this provider implementation.

            var username = (string) context["UserName"];

            var list = new List<SqlParameter>();
            list.Add(new SqlParameter("UserName", username));

            foreach (SettingsPropertyValue pv in ppvc)
            {
                if (pv.Property.IsReadOnly) continue;

                list.Add(new SqlParameter(pv.Name, pv.PropertyValue));
            }

            using (var db = new UserDb())
            {
                db.SaveProfile(list);
            }
        }


        private object GetPropertyValue(ExtendedUser profile, string name)
        {
            PropertyInfo property = typeof (ExtendedUser).GetProperty(name);
            if (property == null) return null;
            return property.GetValue(profile, null);
        }


        public override int DeleteInactiveProfiles(ProfileAuthenticationOption authenticationOption,
                                                   DateTime userInactiveSinceDate)
        {
            throw new NotImplementedException();
        }

        public override int DeleteProfiles(string[] usernames)
        {
            throw new NotImplementedException();
        }

        public override int DeleteProfiles(ProfileInfoCollection profiles)
        {
            throw new NotImplementedException();
        }

        public override ProfileInfoCollection FindInactiveProfilesByUserName(
            ProfileAuthenticationOption authenticationOption, string usernameToMatch, DateTime userInactiveSinceDate,
            int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override ProfileInfoCollection FindProfilesByUserName(ProfileAuthenticationOption authenticationOption,
                                                                     string usernameToMatch, int pageIndex, int pageSize,
                                                                     out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override ProfileInfoCollection GetAllInactiveProfiles(ProfileAuthenticationOption authenticationOption,
                                                                     DateTime userInactiveSinceDate, int pageIndex,
                                                                     int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override ProfileInfoCollection GetAllProfiles(ProfileAuthenticationOption authenticationOption,
                                                             int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfInactiveProfiles(ProfileAuthenticationOption authenticationOption,
                                                        DateTime userInactiveSinceDate)
        {
            throw new NotImplementedException();
        }
    }
}
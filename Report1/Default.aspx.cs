﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class _Default : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ddlUdodType.DataBind();
        if (!Page.IsPostBack)
        {
            var list = new List<ExtendedReportDep>();
            Recalculate(ref list);
            gvReport.DataSource = list;
            gvReport.DataBind();
        }
    }

    private void Recalculate(ref List<ExtendedReportDep> list)
    {
        using (var db = new ReportDb())
        {
            int? transferStatus;
            try
            {
                transferStatus = Convert.ToInt32(ddlTransferStatus.SelectedValue);
            }
            catch
            {
                transferStatus = null;
            }

            list = db.GetReportDep(Convert.ToInt32(ddlUdodType.SelectedValue), transferStatus);

            if (list.Count > 3)
            {
                var item = new ExtendedReportDep();
                item.Parametr =
                    "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во человек (обучающихся) внесённых в Систему</b></div>";
                item.Count = list[1].Count + list[2].Count + list[3].Count;
                var item1 = new ExtendedReportDep();
                item1 = list[1];
                item1.Procent = string.Format("{0,5:P1}", list[1].Count/(float) item.Count);
                list[1] = item1;
                item1 = list[2];
                item1.Procent = string.Format("{0,5:P1}", list[2].Count/(float) item.Count);
                list[2] = item1;
                item1 = list[3];
                item1.Procent = string.Format("{0,5:P1}", list[3].Count/(float) item.Count);
                list[3] = item1;
                item.Procent = string.Format("{0,5:P1}", 1);
                list.Insert(1, item);
            }
            if (list.Count > 7)
            {
                var item = new ExtendedReportDep();
                item.Parametr =
                    "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во заявителей</b></div>";
                item.Count = list[1 + 5].Count + list[1 + 6].Count;
                var item1 = new ExtendedReportDep();
                item1 = list[1 + 5];
                item1.Procent = string.Format("{0,5:P1}", list[1 + 5].Count/(float) item.Count);
                list[1 + 5] = item1;
                item1 = list[1 + 6];
                item1.Procent = string.Format("{0,5:P1}", list[1 + 6].Count/(float) item.Count);
                list[1 + 6] = item1;
                item.Procent = string.Format("{0,5:P1}", 1);
                list.Insert(6, item);
            }
            if (list.Count > 12)
            {
                var item = new ExtendedReportDep();
                item.Parametr =
                    "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во человек, зачисленных в детские объед.</b></div>";
                item.Count = list[9].Count + list[10].Count + list[11].Count + list[12].Count;
                var item1 = new ExtendedReportDep();
                item1 = list[9];
                item1.Procent = string.Format("{0,5:P1}", list[9].Count/(float) item.Count);
                list[9] = item1;
                item1 = list[10];
                item1.Procent = string.Format("{0,5:P1}", list[10].Count/(float) item.Count);
                list[10] = item1;
                item1 = list[11];
                item1.Procent = string.Format("{0,5:P1}", list[11].Count/(float) item.Count);
                list[11] = item1;
                item1 = list[12];
                item1.Procent = string.Format("{0,5:P1}", list[12].Count/(float) item.Count);
                list[12] = item1;
                item.Procent = string.Format("{0,5:P1}", 1);
                list.Insert(9, item);
            }
            if (list.Count > 15)
            {
                var item = new ExtendedReportDep();
                item.Parametr =
                    "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во человек, зачисленных в детские объед.</b></div>";
                item.Count = list[14].Count + list[15].Count;
                var item1 = new ExtendedReportDep();
                item1 = list[14];
                item1.Procent = string.Format("{0,5:P1}", list[14].Count/(float) item.Count);
                list[14] = item1;
                item1 = list[15];
                item1.Procent = string.Format("{0,5:P1}", list[15].Count/(float) item.Count);
                list[15] = item1;
                item.Procent = string.Format("{0,5:P1}", 1);
                list.Insert(14, item);
            }
            if (list.Count > 20)
            {
                var item = new ExtendedReportDep();
                item.Parametr =
                    "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во человек, зачисленных в детские объед.</b></div>";
                item.Count = list[20].Count + list[19].Count + list[18].Count + list[17].Count;
                var item1 = new ExtendedReportDep();
                item1 = list[17];
                item1.Procent = string.Format("{0,5:P1}", list[17].Count/(float) item.Count);
                list[17] = item1;
                item1 = list[18];
                item1.Procent = string.Format("{0,5:P1}", list[18].Count/(float) item.Count);
                list[18] = item1;
                item1 = list[19];
                item1.Procent = string.Format("{0,5:P1}", list[19].Count/(float) item.Count);
                list[19] = item1;
                item1 = list[20];
                item1.Procent = string.Format("{0,5:P1}", list[20].Count/(float) item.Count);
                list[20] = item1;
                item.Procent = string.Format("{0,5:P1}", 1);
                list.Insert(17, item);
            }
            if (list.Count > 23)
            {
                var item = new ExtendedReportDep();
                item.Parametr =
                    "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Общее кол-во зачислений по виду обучения</b></div>";
                item.Count = list[23].Count + list[22].Count;
                var item1 = new ExtendedReportDep();
                item1 = list[23];
                item1.Procent = string.Format("{0,5:P1}", list[23].Count/(float) item.Count);
                list[23] = item1;
                item1 = list[22];
                item1.Procent = string.Format("{0,5:P1}", list[22].Count/(float) item.Count);
                list[22] = item1;
                item.Procent = string.Format("{0,5:P1}", 1);
                list.Insert(22, item);
            }
            if (list.Count > 26)
            {
                var item = new ExtendedReportDep();
                item.Parametr =
                    "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Общее кол-во часов (для зачисленных в детские объед.)</b></div>";
                item.Count = list[26].Count + list[25].Count;
                var item1 = new ExtendedReportDep();
                item1 = list[26];
                item1.Procent = string.Format("{0,5:P1}", list[26].Count/(float) item.Count);
                list[26] = item1;
                item1 = list[25];
                item1.Procent = string.Format("{0,5:P1}", list[25].Count/(float) item.Count);
                list[25] = item1;
                item.Procent = string.Format("{0,5:P1}", 1);
                list.Insert(25, item);
            }
            if (list.Count > 32)
            {
                var item = new ExtendedReportDep();
                item.Parametr =
                    "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Сумма зачислений по годам обучения</b></div>";
                item.Count = list[25 + 3].Count + list[26 + 3].Count + list[27 + 3].Count + list[28 + 3].Count +
                             list[29 + 3].Count + list[30 + 3].Count;
                var item1 = new ExtendedReportDep();
                item1 = list[25 + 3];
                item1.Procent = string.Format("{0,5:P1}", list[25 + 3].Count/(float) item.Count);
                list[25 + 3] = item1;
                item1 = list[26 + 3];
                item1.Procent = string.Format("{0,5:P1}", list[26 + 3].Count/(float) item.Count);
                list[26 + 3] = item1;
                item1 = list[27 + 3];
                item1.Procent = string.Format("{0,5:P1}", list[27 + 3].Count/(float) item.Count);
                list[27 + 3] = item1;
                item1 = list[28 + 3];
                item1.Procent = string.Format("{0,5:P1}", list[28 + 3].Count/(float) item.Count);
                list[28 + 3] = item1;
                item1 = list[29 + 3];
                item1.Procent = string.Format("{0,5:P1}", list[29 + 3].Count/(float) item.Count);
                list[29 + 3] = item1;
                item1 = list[30 + 3];
                item1.Procent = string.Format("{0,5:P1}", list[30 + 3].Count/(float) item.Count);
                list[30 + 3] = item1;
                item.Procent = string.Format("{0,5:P1}", 1);
                list.Insert(25 + 3, item);
            }
            if (list.Count > 40)
            {
                var item = new ExtendedReportDep();
                item.Parametr =
                    "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во воспитанников по годам обучения</b></div>";
                item.Count = list[32 + 3].Count + list[33 + 3].Count + list[34 + 3].Count + list[35 + 3].Count +
                             list[36 + 3].Count + list[37 + 3].Count;
                var item1 = new ExtendedReportDep();
                item1 = list[32 + 3];
                item1.Procent = string.Format("{0,5:P1}", list[32 + 3].Count/(float) item.Count);
                list[32 + 3] = item1;
                item1 = list[33 + 3];
                item1.Procent = string.Format("{0,5:P1}", list[33 + 3].Count/(float) item.Count);
                list[33 + 3] = item1;
                item1 = list[34 + 3];
                item1.Procent = string.Format("{0,5:P1}", list[34 + 3].Count/(float) item.Count);
                list[34 + 3] = item1;
                item1 = list[35 + 3];
                item1.Procent = string.Format("{0,5:P1}", list[35 + 3].Count/(float) item.Count);
                list[35 + 3] = item1;
                item1 = list[36 + 3];
                item1.Procent = string.Format("{0,5:P1}", list[36 + 3].Count/(float) item.Count);
                list[36 + 3] = item1;
                item1 = list[37 + 3];
                item1.Procent = string.Format("{0,5:P1}", list[37 + 3].Count/(float) item.Count);
                list[37 + 3] = item1;
                item.Procent = string.Format("{0,5:P1}", 1);
                list.Insert(32 + 3, item);
            }
            if (list.Count > 53)
            {
                var item = new ExtendedReportDep();
                item.Parametr =
                    "<div style='padding-top: 10px;padding-bottom: 10px;'><b>Кол-во обучающихся по направленностям</b></div>";
                item.Count = list[42].Count + list[43].Count + list[44].Count + list[45].Count +
                             list[46].Count + list[47].Count + list[48].Count + list[49].Count + list[50].Count +
                             list[51].Count + list[52].Count + list[53].Count;
                var item1 = new ExtendedReportDep();

                for (int i = 42; i <= 53; i++)
                {
                    item1 = list[i];
                    item1.Procent = string.Format("{0,5:P1}", list[i].Count/(float) item.Count);
                    list[i] = item1;
                }

                item.Procent = string.Format("{0,5:P1}", 1);
                list.Insert(42, item);
            }

            // дополнительное поле БДО переносим из конца наверх
            var item2 = new ExtendedReportDep();
            item2 = list[list.Count - 1];
            item2.Procent = string.Format("{0,5:P1}", list[list.Count - 1].Count/(float) list[0].Count);
            list[list.Count - 1] = item2;
            list.Insert(1, list.Last());
            list.RemoveAt(list.Count - 1);

            // убираем пункты Кол-во будущих воспитанников по заявлениям и Кол-во заявителей
            for (int i = 9; i >= 6; i--)
                list.RemoveAt(i);
        }
    }

    protected void lnkExportInExcel_OnClick(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("Report.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                               string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        var workbook = new Aspose.Cells.Workbook();
        workbook.Open(MapPath("Templates/ReportTemplate.xlsx"));
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];

        #region Стили

        Aspose.Cells.Style style_area = workbook.Styles[0];
        Aspose.Cells.Style style_header = workbook.Styles[1];
        style_area.ShrinkToFit = true;

        style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.Black;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.Black;
        style_header.Font.IsBold = true;
        style_header.Font.Size = 12;
        style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
        style_header.BackgroundColor = Color.Gray;
        style_header.Pattern = Aspose.Cells.BackgroundType.Solid;

        #endregion

        var list = new List<ExtendedReportDep>();

        Recalculate(ref list);
        int i = 1;
        sheet.Cells.CreateRange(1, 0, list.Count, 3).ApplyStyle(style_area, new Aspose.Cells.StyleFlag {All = true});
        foreach (ExtendedReportDep param in list)
        {
            string paramName = param.Parametr.Replace("&nbsp;", "");
            if (DeleteTag(ref paramName))
            {
                sheet.Cells[i, 0].Style.Font.IsBold = true;
            }
            sheet.Cells[i, 0].PutValue(paramName);
            sheet.Cells[i, 1].PutValue(param.Count);
            sheet.Cells[i, 2].PutValue(param.Procent);
            i++;
        }


        workbook.Save("Report.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }

    private bool DeleteTag(ref string paramName)
    {
        var regex = new Regex(@">[^<]+<");
        MatchCollection matches = regex.Matches(paramName);
        if (matches.Count > 0)
        {
            paramName = matches[0].Value.Replace("<", "").Replace(">", "");
            return true;
        }
        else return false;
    }

    protected void gvReport_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex == 0 || e.Row.RowIndex == 2 || e.Row.RowIndex == 6 || e.Row.RowIndex == 11 ||
                e.Row.RowIndex == 14 || e.Row.RowIndex == 19 || e.Row.RowIndex == 22 || e.Row.RowIndex == 25 ||
                e.Row.RowIndex == 32 || e.Row.RowIndex == 39)
            {
                e.Row.CssClass = "ReportRowTop";
            }
        }
    }

    protected void ddlUdodType_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlUdodType.SelectedValue == "1")
        {
            lblTitle.Text =
                "Комплектование контингента блока дополнительного образования в общеобразовательных учреждениях в разрезе города";
        }
        else
        {
            lblTitle.Text = "Комплектование контингента учреждений дополнительного образования детей в разрезе города";
        }
        var list = new List<ExtendedReportDep>();
        Recalculate(ref list);
        gvReport.DataSource = list;
        gvReport.DataBind();
        upGridView.Update();
    }

    protected void ddlTransferStatus_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        var list = new List<ExtendedReportDep>();
        Recalculate(ref list);
        gvReport.DataSource = list;
        gvReport.DataBind();
        upGridView.Update();
    }

    protected void lnkErrorOk_OnClick(object sender, EventArgs e)
    {
    }

    protected void lnkErrorCancel_OnClick(object sender, EventArgs e)
    {
    }
}
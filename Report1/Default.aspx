﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" MasterPageFile="Master/MasterPage.master" Inherits="_Default" %>
<%@ Import Namespace="System.Globalization" %>
<asp:Content runat="server" ID="content" ContentPlaceHolderID="body">
    <%-- <div style="font-size: 24px;">Комплектование учреждений дополнительного образования в разрезе города</div> --%>
    <asp:UpdatePanel ID="upTitle" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Label runat="server" Font-Size="24px" ID="lblTitle" Text="Комплектование контингента учреждений дополнительного образования детей в разрезе города" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="upLnk">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        Выбор типа учреждения:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlUdodType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUdodType_OnSelectedIndexChanged">
                            <Items>
                                <asp:ListItem runat="server" Value="0" Selected="true">УДО</asp:ListItem>
                                <asp:ListItem runat="server" Value="1">ОУ</asp:ListItem>
                                <asp:ListItem runat="server" Value="2">ДОУ</asp:ListItem>
                                <asp:ListItem runat="server" Value="3">СПО</asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </td>
                    <td>
                        Учебный год:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlTransferStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTransferStatus_OnSelectedIndexChanged">
                            <Items>
                                <asp:ListItem runat="server" Value="" Selected="true">Текущий</asp:ListItem>
                                <asp:ListItem runat="server" Value="2">Следующий</asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
                
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:LinkButton runat="server" ID="lnkExportInExcel" OnClick="lnkExportInExcel_OnClick"><div class="btnBlue">Экспорт в Excel</div></asp:LinkButton>
    <asp:UpdatePanel runat="server" ID="upGridView" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView runat="server" ID="gvReport" AutoGenerateColumns="false" OnRowDataBound="gvReport_OnRowDataBound" CssClass="ReportTable" >
                <Columns>
                    <asp:BoundField runat="server" HeaderText="Параметр" DataField="Parametr" HtmlEncode="false"/>
                    <asp:TemplateField runat="server" HeaderText="Кол-во" >
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lbl"><%#Convert.ToInt32(Eval("Count")).ToString("### ###", CultureInfo.InvariantCulture)%></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="62" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    <asp:BoundField runat="server" HeaderText="Процент" DataField="Procent" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

        
</asp:Content>
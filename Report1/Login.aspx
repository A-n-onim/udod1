﻿<%@ Page Language="C#" MasterPageFile="Master/MasterLogin.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content runat="server" ID="contentLogin" ContentPlaceHolderID="body">

    <table width="800px" style="height: 400px" >
        <tr align="center" valign="middle">
            <td>
                <asp:Panel ID="Panel1" runat="server" GroupingText="Авторизация" Width="240px" style="padding: 5px;">
                    <table border=0>
                        <tr>
                            <td class="tdx">
                                Логин:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input">
                                    <asp:TextBox runat="server" ID="tbLogin" CssClass="inputShort" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdx">
                                Пароль:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input">
                                    <asp:TextBox runat="server" ID="tbPassword" TextMode="Password" CssClass="inputShort" />
                                </div>
                            </td>
                            <td>
                                <asp:Button style="margin-top: 4px; margin-left: 4px" runat="server" ID="lbEnter" onclick="lbEnter_Click" Text="Вход >>"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblInfo"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>


</asp:Content>
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="NewClaim.aspx.cs" Inherits="Claim_NewClaim" %>
<%@ Register TagPrefix="uct" TagName="PupilEdit" Src="~/UserControl/Pupil/ucPupilEdit.ascx" %>
<%@ Register TagPrefix="uct" TagName="Document" Src="~/UserControl/User/ucDocument.ascx" %>
<%@ Register TagPrefix="uct" TagName="Parent" Src="~/UserControl/Pupil/ucParent.ascx" %>
<%@ Register TagPrefix="uct" TagName="Address" Src="~/UserControl/Address/ucNewAddress.ascx" %>
<%@ Register TagPrefix="uct" TagName="Udod" Src="~/UserControl/Udod/ucFindUdod.ascx" %>
<%@ Register TagPrefix="uct" TagName="ErrorPopup" Src="~/UserControl/Menu/ucErrorPopup.ascx" %>
<%@ Register src="../UserControl/Udod/ucDOPassport.ascx" tagName="DOPassport" tagPrefix="uct" %>

<asp:Content runat="server" ID="contentClaim" ContentPlaceHolderID="body">
<script type="text/javascript">
    //$get('<%=popupSelectUdod.ClientID %>').css('visibility','visible');
    //$get('<%=popupSelectUdod.ClientID %>').css('display', 'block');
    $('<%="#"+popupSelectUdod.ClientID %>').css('visibility', 'visible');
    $('<%="#"+popupSelectUdod.ClientID %>').css('display', 'block');
    function bind(sender, args) {
        if (window.location.href.toLowerCase().indexOf('claim/newclaim.aspx') >= 0) {
            $('.ajax__combobox_itemlist').each(function () {
                //$(this).removeClass('ajax__combobox_itemlist');
                $(this).addClass('ajax__combobox_itemlist1');
            });
        }
    }
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(bind);
    
    function pageLoad() {
        //$get('<%=popupSelectUdod.ClientID %>').style.visibility = "hidden";
        //$get('<%=popupSelectUdod.ClientID %>').style.display = "none";
        //$('<%="#"+popupSelectUdod.ClientID %>').css('visibility', 'hidden');
        //$('<%="#"+popupSelectUdod.ClientID %>').css('display', 'none');
        if (window.location.href.toLowerCase().indexOf('claim/newclaim.aspx')>=0) {
            $('.ajax__combobox_itemlist').each(function () {
                //$(this).removeClass('ajax__combobox_itemlist');
                $(this).addClass('ajax__combobox_itemlist1');
            });
        }
    }
    
    function ShowMessage() {
        alert('В связи с большой загруженостью реестра контингента города Москвы запрос на проверку может занять до 2 минут. Для начала загрузки нажмите кнопку ОК');
    }

</script>
            <table border="0px;" >
                <tr runat="server" ID="rowStatus" Visible="false">
                    <td colspan="2">
                        <table border=0>
                            <tr>
                                <td>
                                   <h4>Дата регистрации заявления</h4>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblCreatedDate"></asp:Label>
                                </td>
                                <td >
                                    <h4>Статус заявления</h4>
                                </td>
                                <td>
                                    <div class="input"  style="margin-top: 6px;">
                                    <asp:Label runat="server" ID="lblStatus"></asp:Label>
                                    </div>
                                </td>
                                <td >
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h4>Регистрационный номер заявления</h4>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblNumber"></asp:Label>
                                </td>
                                <td>
                                    <h4>Количество оставшихся дней</h4>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblCountDays"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        *Поля, помеченные звездочкой '*', обязательны для заполнения
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel runat="server" ID="upIsPupil" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:RadioButtonList runat="server" ID="rbnIsPupil" AutoPostBack="true" OnSelectedIndexChanged="rbnIsPupil_SelectedChanged" >
                                    <Items>
                                        <asp:ListItem Selected="true" Value = "False">Заявитель - родитель/законный представитель</asp:ListItem>
                                        <asp:ListItem Value = "True">Заявитель - будущий обучающийся (старше 14 лет)</asp:ListItem>
                                    </Items>
                                </asp:RadioButtonList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="f2f2f2">
                        <strong>Сведения о заявителе</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel runat="server" ID="upParent" UpdateMode="Conditional">
                            <ContentTemplate>
                                <uct:Parent runat="server" ID="parent"/>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="rbnIsPupil" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" ID="uplblParentError" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="lblPassportParentDataError" Text="Ваши паспортные данные совпадают с уже существующими в системе. Проверьте правильность ввода данных" Visible="false" ForeColor="Red" runat="server" />
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" bgcolor="f2f2f2">
                        <strong>Сведения о будущем обучающемся</strong>
                    </td>
                </tr>
                <tr>
                    <td> 
                        <asp:UpdatePanel runat="server" ID="upPupilEdit" UpdateMode="Conditional">
                        <ContentTemplate>
                            <uct:PupilEdit runat="server" ID="PupilEdit" style="width: 100%;" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="rbnIsPupil"/>
                        </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" ID="uplblError" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="lblPupilPassportDataError" Text="Ваши паспортные данные совпадают с уже существующими в системе. Проверьте правильность ввода данных" Visible="false" ForeColor="Red" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel runat="server" ID="upLoadReestr">
                            <ContentTemplate>
                                <asp:LinkButton runat="server" ID="lnkLoadFromReestr" OnClick="lnkLoadFromReestr_OnClick" ValidationGroup="10"><div class="btnBlueLong">Загрузить из реестра</div></asp:LinkButton>
                                <asp:Label runat="server" ID="lblErrorLoad" ForeColor="red"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel runat="server" ID="upAddresses" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    
                                    <tr>
                                        <td>
                                            <asp:Panel ID="Panel3" runat="server" GroupingText="Адрес регистрации" style="width: 300px;">
                                                <asp:Label runat="server" ID="lblReestrAddress"></asp:Label>
                                                <uct:Address runat="server" ID="RegAddress" AddressTypeId="5" />
                                            </asp:Panel>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblErrorReestrAddress" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                     <td colspan="4" bgcolor="f2f2f2">
                        <strong>Выбор желаемого УДО и детского объединения</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel runat="server" ID="upUdod" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table class="dgc">
                                <tr>
                                    <td colspan="2">
                                        <asp:LinkButton runat="server" ID="lnkSelectUdod" onclick="lnkSelectUdod_Click"><div class="btnBlueLong">Выбрать УДО и ДО</div></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table>
                                            <tr>
                                                <th>
                                                    Наименование детского объединения
                                                </th>
                                                <th>
                                                    Наименование образовательной программы
                                                </th>
                                                <th>
                                                    Основной вид деятельности (предмет)
                                                </th>
                                                
                                                <th>
                                                    Возраст обучающихся
                                                </th>
                                                <th>
                                                    Адрес фактического проведения занятий
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label runat="server" ID="lblChildUnion"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblProgram"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblSection"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblAge"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblAddress"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        
                                        <asp:LinkButton runat="server" ID="lnkDetail" Visible="false" OnClick="lnkDetail_OnClick"><div class="btnBlueLong">Детальный просмотр</div></asp:LinkButton>
                                        <asp:Label runat="server" ID="lblChildUnionDeleted" Visible="false" style="color:Red; font-size:16px">Внимание! Данное ДО больше не существует</asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td>
                                        Дата желаемого начала обучения*:
                                    </td>
                                    <td>
                                        <div class="inputShort"><asp:TextBox runat="server" ID="tbBeginDate" CssClass="inputShort" /></div>
                                        <ajaxToolkit:CalendarExtender runat="server" ID="calendar" Format="dd.MM.yyyy" TargetControlID="tbBeginDate" />
                                        <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbBeginDate" Mask="99/99/9999" MaskType="Date" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=2 align="right">
                                        <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="val1" ErrorMessage="Поле 'Дата желаемого начала обучения' не заполнено" ControlToValidate="tbBeginDate" SetFocusOnError="true" ValidationGroup="71"></asp:RequiredFieldValidator>
                                        <asp:RangeValidator ID="valrDate" runat="server" ControlToValidate="tbBeginDate" MinimumValue="01.08.2012" MaximumValue="01.08.3012" Type="Date" text="Дата не может быть меньше Сегодня" Display="Dynamic" ValidationGroup="71"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Год обучения в данном детском объединении:
                                    </td>
                                    <td>
                                        <div class="input"  style="margin-top: 6px;">
                                        <asp:DropDownList runat="server" ID="ddlYearOfStudy" CssClass="input"/>
                                        </div>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <asp:UpdatePanel runat="server" ID="upLnk" UpdateMode="Conditional">
            <ContentTemplate>
                <table>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkChangeStatusDocs" runat="server" OnClick="changeStatus_Click">
                            <div id="lnkChangeStatusDocsDiv" runat="server" class="btnBlue">Поданы документы</div>
                        </asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="lnkChangeStatusEnroll" runat="server" OnClick="lnkChangeStatusEnroll_OnClick">
                            <div id="lnkChangeStatusEnrollDiv" runat="server" class="btnBlue">Зачислить</div>
                        </asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="lnkChangeStatusNextYear" runat="server" OnClick="changeStatus_Click">
                            <div id="divChangeStatusNextYear" runat="server" class="btnBlueLong">Зачислен на след.уч.г.</div>
                        </asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="lnkChangeStatusAnnul" runat="server" OnClick="changeStatus_Click">
                            <div id="divChangeStatusAnnul" runat="server" class="btnRed">Аннулировать</div>
                        </asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="lnkCreateClaimFromCurrent" runat="server" OnClick="lnkCreateClaimFromCurrent_OnClick" ValidationGroup="71">
                            <div id="divCreateClaimFromCurrent" runat="server" class="btnBlue" >Зачислить в др. ДО</div>
                        </asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan=2>
                        <asp:LinkButton runat="server" ID="lnkSave" OnClick="lnkSave_Click" ValidationGroup="71"><div class="btnBlueLong">Сохранить заявление</div></asp:LinkButton>
                    </td>
                    <td colspan=2>
                        <asp:LinkButton runat="server" ID="lnkCancel" OnClick="lnkCancel_OnClick" ><div class="btnBlueLong">К списку заявлений</div></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan=2>
                        
                    </td>
                </tr>
                </table>
            </ContentTemplate>
            </asp:UpdatePanel>
            <asp:LinkButton runat="server" ID="lnkPrint" OnClick="lnkPrint_Click"><div class="btnBlueLong">Печать заявления</div></asp:LinkButton>
            <br />
            
    <asp:Panel runat="server" ID="popupSelectUdod" style="padding: 5px; border: 2px solid black" BackColor="White">
        <asp:Panel ID="pnlHeader" runat="server" >
            <asp:Label ID="litTitle" Text="<span class='headerCaptions'>Выбор ДО</span>" runat="server"/>
            <asp:LinkButton ID="lnkClose" runat="server" 
                OnClientClick="$find('SelectUdodPopup').hide(); return false;" />
        </asp:Panel>
        <div style="width: 900px;">
            <uct:Udod runat="server" ID="selectUdod" OnChange="btnSelectUdod_Click"/>
        </div>
        <div >
            <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false"><div class="btnRed">Отменить</div></asp:LinkButton>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupSelectExtender" PopupDragHandleControlID="pnlHeader"
        PopupControlID="popupSelectUdod" CancelControlID="btnCancel"
        TargetControlID="btnShowPopup" RepositionMode="None" X="200" Y="100"
        BehaviorID="SelectUdodPopup"/>
    <asp:Button ID="btnShowPopup" runat="server" style="display:none" />    


    <asp:Panel runat="server" ID="popupSaveClaim" style="padding: 5px; display: none; border: 2px solid black" BackColor="White">
        <asp:Panel ID="saveClaimHeader" runat="server" >
            <asp:Label ID="Label1" Text="<span class='headerCaptions'>Заявление сохранено</span>" runat="server"/>
            <asp:LinkButton ID="LinkButton1" runat="server" 
                OnClientClick="$find('SaveClaimPopup').hide(); return false;" />
        </asp:Panel>
        <asp:UpdatePanel ID="upClaimInfo" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div>
                    <asp:Label runat="server" ID="lblClaimInfo"></asp:Label>
                </div>
                <asp:Label runat="server" ID="lblDeclarant"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:LinkButton runat="server" ID="lnkPrint2" OnClick="lnkPrint_Click"><div class="btnBlueLong">Печать заявления</div></asp:LinkButton>
        <div style="text-align: left;">
            <asp:RadioButtonList runat="server" ID="select">
            <Items>
                <asp:ListItem Value="0">Создать ещё одну заявку для данного ребёнка</asp:ListItem>
                <asp:ListItem Value="1">Создать заявку для другого ребёнка данного заявителя</asp:ListItem>
                <asp:ListItem Value="2">Создать новое заявление</asp:ListItem>
                <asp:ListItem Value="3">Завершить ввод и перейти в Реестр заявлений</asp:ListItem>
                <asp:ListItem Selected="true" Value="4">Продолжить работу с ЭТИМ заявлением</asp:ListItem>
            </Items>
            </asp:RadioButtonList>
        </div>
        <div >
            <asp:Button ID="btnSaveClaim" runat="server" Text="Продолжить" CausesValidation="false" OnClick="btnSaveClaim_OnClick"/>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupSaveClaimExtender" PopupDragHandleControlID="saveClaimHeader"
        PopupControlID="popupSaveClaim" 
        TargetControlID="btnShow1" RepositionMode="None"
        BehaviorID="SaveClaimPopup"/>
    <asp:Button ID="btnShow1" runat="server" style="display:none" />  


    <asp:Panel runat="server" ID="popupSelectDO" style="padding: 5px; display: none; border: 2px solid black; width: auto;" BackColor="White">
        <asp:Panel ID="pnlSelectDOHeader" runat="server" >
            <asp:Label ID="Label2" Text="<span class='headerCaptions'>Паспорт детского объединения</span>" runat="server"/>
            <asp:LinkButton ID="LinkButton2" runat="server" 
                OnClientClick="$find('SelectUdodPopup').hide(); return false;" />
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upDO" UpdateMode="Conditional">
        <ContentTemplate>
            <div >
                <uct:DOPassport runat="server" ID="selectDO" Edit="false"/>
            </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div >
            <asp:LinkButton ID="btnCancelPopup" runat="server" Text="<div class='btnBlue'>Закрыть</div>" CausesValidation="false" />
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupSelectDOExtender" PopupDragHandleControlID="pnlSelectDOHeader"
        PopupControlID="popupSelectDO" CancelControlID="btnCancelPopup" 
        TargetControlID="btnShowDOPopup" RepositionMode="None" X="-150"
        BehaviorID="AddUdodPopup"/>
    <asp:Button ID="btnShowDOPopup" runat="server" style="display:none" />
      

    <asp:ObjectDataSource runat="server" ID="dsClaimStatus" TypeName="Udod.Dal.ClaimDb" SelectMethod="GetClaimStatuses">
    </asp:ObjectDataSource>


    <asp:Panel runat="server" ID="popupLimit" CssClass="modalPopup" style="display: none; border: 2px solid black; width: 400px;" BackColor="White">
        <asp:Panel ID="pLimitHeader" runat="server" >
           <asp:Label ID="Label4" Text="<span class='headerCaptions'>Внимание</span>" runat="server"/>
           <asp:LinkButton ID="LinkButton4" runat="server" 
               OnClientClick="$find('LimitPopup').hide(); return false;" />
        </asp:Panel>
        <div >
            <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="upLimit" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Label runat="server" ID="lblLimit"></asp:Label>
                </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div >
            <asp:LinkButton ID="btnCancelLimit" runat="server" CausesValidation="false"><div class='btnBlue'>OK</div></asp:LinkButton>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupLimitExtender" PopupDragHandleControlID="pLimitHeader"
        PopupControlID="popupLimit" CancelControlID="btnCancelLimit"
        TargetControlID="btnShowLimit" RepositionMode="None" BackgroundCssClass="modalBackground"
        BehaviorID="LimitPopup  "/>
    <asp:Button ID="btnShowLimit" runat="server" style="display:none" />

    <asp:Panel runat="server" ID="popupError" CssClass="modalPopup" style="display: none; border: 2px solid black; width: 400px;" BackColor="White">
        <asp:Panel ID="pErrorHeader" runat="server" >
           <asp:Label ID="Label3" Text="<span class='headerCaptions'>Ошибка</span>" runat="server"/>
           <asp:LinkButton ID="LinkButton3" runat="server" 
               OnClientClick="$find('ErrorPopup').hide(); return false;" />
        </asp:Panel>
        <div >
            <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label runat="server" ID="lblError">Во время сохранения заявки произошла ошибка. Проверьте правильность заполнения адреса и других полей</asp:Label>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div >
            <asp:LinkButton ID="btnCancelError" runat="server" CausesValidation="false"><div class='btnBlue'>OK</div></asp:LinkButton>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupErrorExtender" PopupDragHandleControlID="pErrorHeader"
        PopupControlID="popupError" CancelControlID="btnCancelError"
        TargetControlID="btnShowError" RepositionMode="None" BackgroundCssClass="modalBackground"
        BehaviorID="ErrorPopup"/>
    <asp:Button ID="btnShowError" runat="server" style="display:none" />

    <asp:Panel runat="server" ID="popupSchoolError" CssClass="modalPopup" style="display: none; border: 2px solid black; width: 400px;" BackColor="White">
        <asp:Panel ID="Panel2" runat="server" >
           <asp:Label ID="Label6" Text="<span class='headerCaptions'>Ошибка</span>" runat="server"/>
           <asp:LinkButton ID="LinkButton6" runat="server" 
               OnClientClick="$find('SchoolErrorPopup').hide(); return false;" />
        </asp:Panel>
        <div >
            <asp:UpdatePanel runat="server" ID="UpdatePanel3" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label runat="server" ID="Label7">Для сотрудников общеобразовательных учреждений предусмотрен специальный раздел для внесения обучающихся по направлениям дополнительного образования, поэтому возможность создания заявления ВРЕМЕННО отключена. До появления спец-раздела В ОБЯЗАТЕЛЬНОМ ПОРЯДКЕ приведите информацию по детским объединениям в актуальное состояние.</asp:Label>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div >
            <asp:LinkButton ID="lnkSchoolError" runat="server" CausesValidation="false" ><div class='btnBlue'>OK</div></asp:LinkButton>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupSchoolErrorExtender" PopupDragHandleControlID="pErrorHeader"
        PopupControlID="popupSchoolError"  CancelControlID="lnkSchoolError"
        TargetControlID="btnShowSchoolError" RepositionMode="None" BackgroundCssClass="modalBackground"
        BehaviorID="SchoolErrorPopup"/>
    <asp:Button ID="btnShowSchoolError" runat="server" style="display:none" />


    <asp:Panel runat="server" ID="popupTypeBudget" CssClass="modalPopup" style="display: none; border: 2px solid black; width: 400px;" BackColor="White">
        <asp:Panel ID="pTypeBudgetHeader" runat="server" >
           <asp:Label ID="Label5" Text="<span class='headerCaptions'>Выбор группы</span>" runat="server"/>
           <asp:LinkButton ID="LinkButton5" runat="server" 
               OnClientClick="$find('TypeBudgetPopup').hide(); return false;" />
        </asp:Panel>
        <div>
            <asp:UpdatePanel runat="server" ID="upGroups" UpdateMode="Always">
                <ContentTemplate>
                    <asp:DropDownList runat="server" ID="ddlGroups" AutoPostBack="true" CssClass="input" DataTextField="Name" DataValueField="UdodAgeGroupId" OnSelectedIndexChanged="ddlGroups_OnSelectedIndexChanged"/>
                    <asp:RequiredFieldValidator runat="server" ID="reqVal" ControlToValidate="ddlGroups" ErrorMessage="Выберите группу" SetFocusOnError="true" ValidationGroup="100"></asp:RequiredFieldValidator>
                    <br/>
                    <asp:Panel runat="server" ID="pGroupinfo" GroupingText="информация о группе">
                    <asp:Label runat="server" ID="lblGroupInfo"></asp:Label>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div >
            <table>
                <tr>
                    <td><asp:LinkButton ID="lnkGroupOk" runat="server" CausesValidation="true" ValidationGroup="100" OnClick="lnkTypeBudgetOk_OnClick"><div class='btnBlue'>OK</div></asp:LinkButton></td>
                    <td><asp:LinkButton ID="lnkGroupCancel" runat="server" CausesValidation="false"><div class='btnBlue'>Отмена</div></asp:LinkButton></td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupTypeBudgetExtender" PopupDragHandleControlID="pTypeBudgetHeader"
        PopupControlID="popupTypeBudget" CancelControlID="lnkGroupCancel"
        TargetControlID="btnShowTypeBudget" RepositionMode="None" BackgroundCssClass="modalBackground"
        BehaviorID="TypeBudgetPopup"/>
    <asp:Button ID="btnShowTypeBudget" runat="server" style="display:none" />

    <asp:ObjectDataSource runat="server" ID="dsTypeBudget" TypeName="Udod.Dal.DictBudgetTypeDb" SelectMethod="GetBudgetTypes"></asp:ObjectDataSource>
    <asp:ObjectDataSource runat="server" ID="dsYearOfStudy" TypeName="Udod.Dal.ChildUnionDb" ></asp:ObjectDataSource>
    <%-- Заголовок и шаблон тела письма пользователю после создания заявки --%>
    <asp:Literal ID="EmailSubject" Text="Уведомление" Visible="false" runat="server" />
    <asp:Literal ID="EmailBody" Text="Уважаемый(ая) #LastName# #FirstName# #MiddleName#,<br />Вами было зарегистрировано заявление на зачисление #PupilFio# в учреждение дополнительного образования #OrgName# в детское объединение #ChildUnionName#.<br />Номер заявления: <b>#ClaimNumber#</b><br />Вам необходимо явиться в указанное учреждение для оформления документов не позднее <b>#DeadlineDate#</b> в соответствии с режимом работы и по адресу указанным на сайте учреждения <a href='http://#OrgSite#'>#OrgSite#</a>. При себе иметь паспорт РФ или иной документ, удостоверяющий личность заявителя, свидетельство о рождении или паспорт РФ будущего обучающегося, медицинскую справку и иные документы, если того требует специфика образовательного учреждения." Visible="false" runat="server" />

    <asp:Panel runat="server" ID="pAnnul" CssClass="modalPopup" style="display: none; border: 2px solid black; width: 400px;" BackColor="White">
        <asp:Panel ID="pAnnulHeader" runat="server" >
           <asp:Label ID="Label8" Text="<span class='headerCaptions'>Выбор причины аннулирования</span>" runat="server"/>
           <asp:LinkButton ID="LinkButton7" runat="server" 
               OnClientClick="$find('TypeBudgetPopup').hide(); return false;" />
        </asp:Panel>
        <div>
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:RadioButtonList runat="server" ID="rbAnnul" DataTextField="TypeBudgetName" DataValueField="DictTypeBudgetId" />
                    <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="RequiredFieldValidator1" ErrorMessage="Не выбрана причина аннулирования" ControlToValidate="rbAnnul" SetFocusOnError="true" ValidationGroup="234"></asp:RequiredFieldValidator>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div >
            <table>
                <tr>
                    <td><asp:LinkButton ID="lnkAnnulConfirm" runat="server" CausesValidation="true" ValidationGroup="234" OnClick="lnkAnnulConfirm_OnClick"><div class='btnBlue'>OK</div></asp:LinkButton></td>
                    <td><asp:LinkButton ID="LinkButton8" runat="server" CausesValidation="false"><div class='btnBlue'>Отмена</div></asp:LinkButton></td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="modalPopupAnnul" PopupDragHandleControlID="pAnnulHeader"
        PopupControlID="pAnnul" CancelControlID="LinkButton8"
        TargetControlID="Button1" RepositionMode="None" BackgroundCssClass="modalBackground"
        BehaviorID="AnnulPopup"/>
    <asp:Button ID="Button1" runat="server" style="display:none" />
    

    <asp:Panel runat="server" ID="p1" CssClass="modalPopup" style="display: none; border: 2px solid black; width: 600px;" BackColor="White">
        <asp:Panel ID="Panel4" runat="server" >
           <asp:Label ID="Label9" Text="<span class='headerCaptions'>Внимание</span>" runat="server"/>
           <asp:LinkButton ID="LinkButton9" runat="server" 
               OnClientClick="$find('Status1Popup').hide(); return false;" />
        </asp:Panel>
        <div>
            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    Желаемая дата зачисления ещё не наступила. Считать данного ребёнка обучающимся в текущий период времени и выполнить зачисление сейчас? Или отметить его к зачислению на следующий учебный год?
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div >
            <table>
                <tr>
                    <td><asp:LinkButton ID="lnkzach" runat="server" OnClick="changeStatus_Click"><div class='btnBlue'>Зачислить</div></asp:LinkButton></td>
                    <td><asp:LinkButton ID="lnkzachnextyear" runat="server" OnClick="changeStatus_Click"><div class='btnBlue'>Зачислить на след.г.</div></asp:LinkButton></td>
                    <td><asp:LinkButton ID="lnkcancel1" runat="server" CausesValidation="false"><div class='btnBlue'>Отмена</div></asp:LinkButton></td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="ModalPopupStatus1" PopupDragHandleControlID="Panel4"
        PopupControlID="p1" CancelControlID="lnkcancel1"
        TargetControlID="Button2" RepositionMode="None" BackgroundCssClass="modalBackground"
        BehaviorID="Status1Popup"/>
    <asp:Button ID="Button2" runat="server" style="display:none" />
    

    <uct:ErrorPopup ID="errorReestr" runat="server" OnErrorOkClick="errorReestr_OnErrorOkClick" Width="450"></uct:ErrorPopup> 
</asp:Content>
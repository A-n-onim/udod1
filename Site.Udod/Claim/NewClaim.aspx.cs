﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Udod.Dal;
using System.Net.Mail;
using System.Configuration;
using Udod.Dal.Enum;

public partial class Claim_NewClaim : BasePage
{
    private long? basedOnClaimNumber
    {
        get
        {
            if (ViewState["basedOnClaimNumber"] == null) return null;
            return (long)ViewState["basedOnClaimNumber"];
        }
        set
        {
            ViewState["basedOnClaimNumber"] = value;
        }
    }

    private bool enrollRightNow
    {
        get
        {
            if (ViewState["enrollRightNow"] == null) return false;
            return (bool)ViewState["enrollRightNow"];
        }
        set
        {
            ViewState["enrollRightNow"] = value;
        }
    }

    private long? currentClaimNumber
    {
        get
        {
            return (long?)ViewState["currentClaimNumber"];
        }
        set
        {
            ViewState["currentClaimNumber"] = value;
        }
    }

    private int? DictTypeBudgetId
    {
        get { return (int?)ViewState["DictTypeBudgetId"]; }
        set { ViewState["DictTypeBudgetId"] = value; }
    }

    private bool edit
    {
        set
        {
            ViewState["edit"] = value;
        }
        get
        {
            return (bool)ViewState["edit"];
        }
    }

    private bool? editReestr
    {
        set
        {
            ViewState["EditReestr"] = value;
        }
        get
        {
            if (ViewState["EditReestr"] == null) return null;
            return (bool)ViewState["EditReestr"];
        }
    }


    private bool IsReestr
    {
        set { lnkSave.Visible = value; upLnk.Update(); }
        get { return lnkSave.Visible; }
    }
    private Int64? PupilReestrCode
    {
        set { ViewState["PupilReestrCode"] = value; }
        get
        {
            if (ViewState["PupilReestrCode"] == null) return null;
            return (Int64)ViewState["PupilReestrCode"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsOsip || UserContext.Roles.IsUdodEmployee || UserContext.Roles.IsDepartment);
        lblDeclarant.Visible = UserContext.Roles.IsAdministrator;
        if (UserContext.UdodTypeId == 1)
        {
            // показать сообщение об ошибке
            /*using (UdodDb db = new UdodDb())
            {
                ExtendedUdod udod = db.GetUdod(UserContext.UdodId.Value);

                if (udod.IsContingentEnabled)
                {
                    popupSchoolErrorExtender.Show();
                    return;
                }
            }*/
        }
        if (!Page.IsPostBack)
        {
            edit = true;
            if (UserContext.Roles.IsDepartment) edit = false;
            // закрываем на редактирование все кроме ФИО и паспорта у ученика 
            lnkLoadFromReestr.Visible = edit;
            lnkSave.Visible = false;
            upLnk.Update();
            lnkCreateClaimFromCurrent.Visible = lnkChangeStatusDocs.Visible = lnkChangeStatusEnroll.Visible = lnkChangeStatusAnnul.Visible = lnkChangeStatusNextYear.Visible =
                (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee || UserContext.Roles.RoleId == (int)EnumRoles.Administrator);

            if (UserContext.ClaimId.HasValue)
            {
                LoadClaim();
                rbnIsPupil.Enabled = false;
            }
            else
            {
                lnkPrint.Visible = false;
                rbnIsPupil.Enabled = true;
                if (Session["PupilIdForNewClaim"] == null)
                RegAddress.Databind();
                if (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee)
                {
                    lnkCreateClaimFromCurrent.Visible = lnkChangeStatusDocs.Visible = lnkChangeStatusEnroll.Visible = lnkChangeStatusAnnul.Visible = lnkChangeStatusNextYear.Visible = false;
                }
            }

            if (Session["PupilIdForNewClaim"]!=null)
            {
                // загрузить информацию по ребенку в необходимые поля
                LoadClaim((Guid)(Session["PupilIdForNewClaim"]));
                Session["PupilIdForNewClaim"] = null;
            }

            valrDate.MinimumValue = DateTime.Today.ToString().Substring(0, 10);

            if (Session["SelectAgeGroupUdodId"] != null)
            {
                selectUdod.UdodAgeGroupId = Convert.ToInt64(Session["SelectAgeGroupUdodId"]);
                btnSelectUdod_Click(selectUdod, new EventArgs());
                Session["SelectAgeGroupUdodId"] = null;
            }

            // сравниваем isNewClaimFromOld только если редактирование, а не новая заявка
            if (UserContext.ClaimId.HasValue)
            {
                // если создаем заявку на основе предыдущей то кнопка сохранить доступна
                if ((bool)Session["isNewClaimFromOld"])
                {
                    edit = true;
                    lnkPrint.Visible = false;
                    rowStatus.Visible = false;
                    lnkSave.Enabled = true;
                    lnkCreateClaimFromCurrent.Visible = lnkChangeStatusDocs.Visible = lnkChangeStatusEnroll.Visible = lnkChangeStatusAnnul.Visible = lnkChangeStatusNextYear.Visible = false;
                    using (ClaimDb db = new ClaimDb())
                    {
                        basedOnClaimNumber = db.GetClaim(UserContext.ClaimId.Value).Number;
                    }
                    UserContext.ClaimId = null;
                    Session["isNewClaimFromOld"] = null;
                }
            }

            parent.Enabled = PupilEdit.Enabled =
                RegAddress.Enabled =
                lnkSelectUdod.Visible =
                tbBeginDate.Enabled =
                ddlYearOfStudy.Enabled = edit;

            //ddlYearOfStudy.Enabled = false;

            PupilEdit.Enable14YearsValidation = Convert.ToBoolean(rbnIsPupil.SelectedValue);
            if (Convert.ToBoolean(rbnIsPupil.SelectedValue))
            {
                parent.RelativeEnabled = !Convert.ToBoolean(rbnIsPupil.SelectedValue);
                PupilEdit.FioEnabled = !Convert.ToBoolean(rbnIsPupil.SelectedValue);
                PupilEdit.PassportEnabled = !Convert.ToBoolean(rbnIsPupil.SelectedValue);
                PupilEdit.LastName = parent.LastName;
                PupilEdit.FirstName = parent.FirstName;
                PupilEdit.MiddleName = parent.MiddleName;
                upIsPupil.Update();
            }

            if (Session["needLimitAlert"] != null)
            {
                lblLimit.Text = Session["needLimitAlert"].ToString();
                upLimit.Update();
                popupLimitExtender.Show();
                Session["needLimitAlert"] = null;
            }

            // если загружено из реестра то закрывать на редактирование поля
            if (editReestr.HasValue && UserContext.ClaimId.HasValue && PupilEdit.Enabled && !UserContext.Roles.IsAdministrator)
            {
                if (rbnIsPupil.SelectedIndex != 0)
                {
                    parent.IsReestr = editReestr.Value;
                }
                else
                {
                    PupilEdit.IsReestr = editReestr.Value;
                }
            }

            parent.reestrValidatorsEnabled = false;
        }
    }

    private void LoadClaim()
    {
        ExtendedClaim claim;
        using (ClaimDb db = new ClaimDb())
        {
            claim = db.GetClaim(UserContext.ClaimId.Value);
        }
        editReestr = claim.IsReestr;
        currentClaimNumber = claim.Number;
        // редактирование или нет
        if (UserContext.Roles.RoleId == (int)EnumRoles.Osip)
        {
            if (claim.ClaimStatus.ClaimStatusId == 1)
                edit = true;
            else
                edit = false;
        }
        if (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee)
        {
            if (claim.ClaimStatus.ClaimStatusId != 4 && claim.ClaimStatus.ClaimStatusId != 5)
                edit = true;
            else
                edit = false;
        }
        if (UserContext.Roles.RoleId == (int)EnumRoles.Administrator) edit = true;
        //
        //---------------------------------------------------------------------
        // Шапка заявки
        lblCreatedDate.Text = claim.CreatedDate.ToShortDateString();
        lblNumber.Text = claim.Number.ToString();
        if (claim.ClaimStatus.ClaimStatusId == 1)
        {
            if ((claim.CreatedDate.AddDays(7) - DateTime.Now).Days > 0)
                lblCountDays.Text = (claim.CreatedDate.AddDays(7) - DateTime.Now).Days.ToString();
        }
        else
        {
            lblCountDays.Visible = false;
        }
        rowStatus.Visible = true;
        lblStatus.Text = claim.ClaimStatus.ClaimStatusName;
        //
        //---------------------------------------------------------------------
        // Заявитель - будущий обучающийся
        rbnIsPupil.SelectedIndex = rbnIsPupil.Items.IndexOf(rbnIsPupil.Items.FindByValue(Convert.ToString(claim.Parent.UserId == claim.Pupil.UserId)));
        //
        //---------------------------------------------------------------------
        // Заявитель
        parent.LastName = claim.Parent.LastName;
        parent.FirstName = claim.Parent.FirstName;
        parent.MiddleName = claim.Parent.MiddleName;
        parent.PassportType = claim.Parent.Passport.PassportType.PassportTypeId;
        parent.Series = claim.Parent.Passport.Series;
        parent.Number = claim.Parent.Passport.Number;
        parent.IssueDate = claim.Parent.Passport.IssueDate;
        parent.EMail = claim.Parent.EMail;
        parent.PhoneNumber = claim.Parent.PhoneNumber;
        parent.ExpressPhoneNumber = claim.Parent.ExpressPhoneNumber;
        parent.Relativies = (claim.Parent.UserType != 0) ? claim.Parent.UserType : 1;
        //
        //---------------------------------------------------------------------
        // Обучающийся
        PupilEdit.LastName = claim.Pupil.LastName;
        PupilEdit.FirstName = claim.Pupil.FirstName;
        PupilEdit.MiddleName = claim.Pupil.MiddleName;
        PupilEdit.PassportType = claim.Pupil.Passport.PassportType.PassportTypeId;
        PupilEdit.Series = claim.Pupil.Passport.Series;
        PupilEdit.Number = claim.Pupil.Passport.Number;
        PupilEdit.IssueDate = claim.Pupil.Passport.IssueDate;
        PupilEdit.BirthDay = claim.Pupil.Birthday;
        PupilEdit.Sex = claim.Pupil.Sex;
        PupilEdit.SchoolName = claim.Pupil.SchoolName;
        PupilEdit.ClassName = claim.Pupil.ClassName;
        try
        {
            PupilReestrCode = Convert.ToInt64(claim.Pupil.ReestrCode);
        }
        catch (Exception)
        {
            PupilReestrCode = null;
        }
        
        //
        //---------------------------------------------------------------------
        // Адрес
        using (KladrDb db = new KladrDb())
        {
            List<ExtendedAddress> addresses = db.GetAddressesById(claim.Pupil.UserId, null, null, null);

            var regAddress = addresses.Where(i => i.AddressType.AddressTypeId == 5).FirstOrDefault();
            if (addresses.Count > 0)
            {
                RegAddress.IsBti = string.IsNullOrEmpty(regAddress.KladrCode);
                RegAddress.Code = string.IsNullOrEmpty(regAddress.KladrCode)
                                      ? regAddress.BtiCode.Value.ToString()
                                      : regAddress.KladrCode;
                RegAddress.Databind();
                RegAddress.Index = regAddress.PostIndex;
                RegAddress.Housing = regAddress.Housing;
                RegAddress.NumberHouse = regAddress.HouseNumber;
                RegAddress.Fraction = regAddress.Fraction;
                RegAddress.Flat = regAddress.FlatNumber;
                RegAddress.Building = regAddress.Building;

                
            }
            else
            {
                lnkChangeStatusDocs.Visible = lnkChangeStatusEnroll.Visible = lnkChangeStatusNextYear.Visible = false;
                RegAddress.Databind();
            }
            upAddresses.Update();
        }
        //
        //---------------------------------------------------------------------
        // Выбор УДО и ДО
        lnkSelectUdod.Text = "<div class='btnBlueLong'>Изменить ДО</div>";
        selectUdod.UdodAgeGroupId = claim.ChildUnion.ChildUnionId;
        selectUdod.UdodSectionName = claim.ChildUnion.Name;
        lblChildUnion.Text = claim.ChildUnion.Name;
        lblProgram.Text = claim.ChildUnion.Program.Name;
        lblSection.Text = claim.ChildUnion.Section.Name;
        lblAge.Text = "от " + claim.ChildUnion.AgeStart.ToString() + " до " + claim.ChildUnion.AgeEnd.ToString();
        using (KladrDb db = new KladrDb())
        {
            var Addresses = db.GetAddressesById(null, null, selectUdod.UdodAgeGroupId, null);
            if (Addresses.Count() > 0) lblAddress.Text = Addresses.FirstOrDefault().AddressStr;
            var adrr = db.GetAddressesById(null, null, null, claim.AgeGroup.UdodAgeGroupId);        // ?
            if (adrr.Count() > 0) lblAddress.Text = adrr.FirstOrDefault().AddressStr;
        }
        lnkDetail.Visible = true;
        if (claim.ChildUnion.isDeleted)
        {
            lnkDetail.Visible = false;
            lblChildUnionDeleted.Visible = true;
            lnkChangeStatusEnroll.Visible = false;
        }
        else
        {
            lblChildUnionDeleted.Visible = false;
        }
        //
        //---------------------------------------------------------------------
        // Дата начала и Год обучения
        tbBeginDate.Text = claim.BeginDate.ToShortDateString();
        ddlYearOfStudy.Items.Clear();
        for (int i = 1; i <= claim.ChildUnion.NumYears; i++)
        {
            ddlYearOfStudy.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        if (claim.ChildUnion.NumYears >= claim.NumYear)
            ddlYearOfStudy.SelectedIndex = claim.NumYear - 1;
        else ddlYearOfStudy.SelectedIndex = -1;
        //
        //---------------------------------------------------------------------
        // Кнопки
        lnkCreateClaimFromCurrent.Visible = (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee) && ((claim.ClaimStatus.ClaimStatusId == 7) || (claim.ClaimStatus.ClaimStatusId == 6));
        StatusButtonsEnabling(claim.ClaimStatus.ClaimStatusId);
    }

    private void LoadClaim(Guid pupilId)
    {

        ExtendedPupil pupil;
        using (PupilsDb db = new PupilsDb())
        {
            pupil = db.GetPupil(pupilId);
        }
        editReestr = string.IsNullOrEmpty(pupil.ReestrCode) && pupil.ReestrCode!="0";
        // редактирование или нет
        if (UserContext.Roles.RoleId == (int)EnumRoles.Administrator) edit = true;
        //
        //---------------------------------------------------------------------
        // Шапка заявки
        lblCreatedDate.Text = DateTime.Now.ToShortDateString();
        lblNumber.Text = "";
        //
        //---------------------------------------------------------------------
        // Заявитель - будущий обучающийся
        ExtendedUser pupilparent = pupil.Parents.FirstOrDefault(i=>i.UserId!=pupil.UserId);
        rbnIsPupil.SelectedIndex = rbnIsPupil.Items.IndexOf(rbnIsPupil.Items.FindByValue(Convert.ToString((pupilparent!=null && pupilparent.UserId == pupil.UserId)||pupilparent==null)));
        //
        //---------------------------------------------------------------------
        // Заявитель
        if (new DateTime((DateTime.Now - pupil.Birthday).Ticks).Year < 14 && rbnIsPupil.SelectedIndex == 1)
        {
            rbnIsPupil.SelectedIndex = 0;
        }

        if (pupilparent != null)
        {

            parent.LastName = pupilparent.LastName;
            parent.FirstName = pupilparent.FirstName;
            parent.MiddleName = pupilparent.MiddleName;
            parent.PassportType = pupilparent.Passport.PassportType.PassportTypeId;
            parent.Series = pupilparent.Passport.Series;
            parent.Number = pupilparent.Passport.Number;
            parent.IssueDate = pupilparent.Passport.IssueDate;
            parent.EMail = pupilparent.EMail;
            parent.PhoneNumber = pupilparent.PhoneNumber;
            parent.ExpressPhoneNumber = pupilparent.ExpressPhoneNumber;
            parent.Relativies = (pupilparent.UserType != 0) ? pupilparent.UserType : 1;

        }
        else
            if (rbnIsPupil.SelectedIndex == 1)
            {
                parent.LastName = pupil.LastName;
                parent.FirstName = pupil.FirstName;
                parent.MiddleName = pupil.MiddleName;
                parent.PassportType = pupil.Passport.PassportType.PassportTypeId;
                parent.Series = pupil.Passport.Series;
                parent.Number = pupil.Passport.Number;
                parent.IssueDate = pupil.Passport.IssueDate;
                parent.EMail = pupil.EMail;
                parent.PhoneNumber = pupil.PhoneNumber;
                parent.ExpressPhoneNumber = pupil.ExpressPhoneNumber;
                parent.Relativies = (pupil.UserType != 0) ? pupil.UserType : 1;
            }
        //
        //---------------------------------------------------------------------
        // Обучающийся
        PupilEdit.LastName = pupil.LastName;
        PupilEdit.FirstName = pupil.FirstName;
        PupilEdit.MiddleName = pupil.MiddleName;
        PupilEdit.PassportType = pupil.Passport.PassportType.PassportTypeId;
        PupilEdit.Series = pupil.Passport.Series;
        PupilEdit.Number = pupil.Passport.Number;
        PupilEdit.IssueDate = pupil.Passport.IssueDate;
        PupilEdit.BirthDay = pupil.Birthday;
        PupilEdit.Sex = pupil.Sex;
        PupilEdit.SchoolName = pupil.SchoolName;
        PupilEdit.ClassName = pupil.ClassName;
        try
        {
            PupilReestrCode = Convert.ToInt64(pupil.ReestrCode);
        }
        catch (Exception)
        {
            PupilReestrCode = null;
        }

        //
        //---------------------------------------------------------------------
        // Адрес
        using (KladrDb db = new KladrDb())
        {
            List<ExtendedAddress> addresses = db.GetAddressesById(pupil.UserId, null, null, null);

            var regAddress = addresses.Where(i => i.AddressType.AddressTypeId == 5).FirstOrDefault();
            RegAddress.IsBti = string.IsNullOrEmpty(regAddress.KladrCode);
            RegAddress.Code = string.IsNullOrEmpty(regAddress.KladrCode) ? regAddress.BtiCode.Value.ToString() : regAddress.KladrCode;
            RegAddress.Databind();
            RegAddress.Index = regAddress.PostIndex;
            RegAddress.Housing = regAddress.Housing;
            RegAddress.NumberHouse = regAddress.HouseNumber;
            RegAddress.Fraction = regAddress.Fraction;
            RegAddress.Flat = regAddress.FlatNumber;
            RegAddress.Building = regAddress.Building;
            upAddresses.Update();
        }
        
    }

    protected void lnkPrint_Click(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("Claim.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                                string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
        workbook.Open(MapPath("../Templates/ClaimTemplate.xlsx"));
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];
        if (selectUdod.UdodAgeGroupId.HasValue)
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                sheet.Cells[0, 0].PutValue("Заявление №" + currentClaimNumber);
                var childunion = db.GetChildUnion(selectUdod.UdodAgeGroupId, null);
                using (UdodDb db2 = new UdodDb())
                {
                    ExtendedUdod udod = db2.GetUdod(childunion.UdodId);
                    sheet.Cells[0, 2].PutValue("Директору ГБОУ " + udod.FioDirector);
                }

                sheet.Cells[1, 2].PutValue("От " + parent.LastName + " " + parent.FirstName + " " + parent.MiddleName);
                sheet.Cells[3, 2].PutValue("Проживающий (ая) по адресу: " + RegAddress.AddressStr);
                sheet.Cells[4, 2].PutValue("Контактный телефон: " + parent.PhoneNumber + ", " + parent.ExpressPhoneNumber);
                sheet.Cells[6, 2].PutValue("Электронная почта: " + parent.EMail);
                sheet.Cells[8, 0].PutValue("Прошу принять моего сына (дочь) " + PupilEdit.LastName + " " +
                                           PupilEdit.FirstName + " " + PupilEdit.MiddleName);
                sheet.Cells[10, 0].PutValue("в коллектив (объединение): " + childunion.Name);
                sheet.Cells[13, 0].PutValue("Дата рождения " + PupilEdit.BirthDay.ToShortDateString());
                sheet.Cells[13, 1].PutValue("№ школы (Д/сада) " + PupilEdit.SchoolName);
                sheet.Cells[13, 3].PutValue("класс (группа) " + PupilEdit.ClassName);
            }
        }

        workbook.Save("Claim.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }

    protected bool SaveClaim(bool showSaveClaimPopup)
    {
        try
        {
            if (selectUdod.UdodAgeGroupId.HasValue)
            {
                Guid PupilId = Guid.Empty;
                Guid ParentId = Guid.Empty;
                ExtendedClaim claim;
                bool isPupil = Convert.ToBoolean(rbnIsPupil.SelectedValue);

                if (isPupil == false)
                {
                    isPupil = true;
                    if ((parent.FirstName != PupilEdit.FirstName) ||
                        (parent.LastName != PupilEdit.LastName) ||
                        (parent.MiddleName != PupilEdit.MiddleName) ||
                        (parent.PassportType != PupilEdit.PassportType) ||
                        (parent.Series != PupilEdit.Series) ||
                        (parent.Number != PupilEdit.Number) ||
                        (parent.IssueDate != PupilEdit.IssueDate)) isPupil = false;
                }
                //
                //---------------------------------------------------------------------
                // создание новой заявки или редактирование старой
                if (UserContext.ClaimId.HasValue)
                {
                    //
                    //---------------------------------------------------------------------
                    // Редактирование
                    using (ClaimDb db = new ClaimDb())
                    {
                        claim = db.GetClaim(UserContext.ClaimId.Value);
                        PupilId = claim.Pupil.UserId;
                        ParentId = claim.Parent.UserId;
                    }
                    #region перед тем как редактировать проверим новые паспорта
                    using (UserDb db = new UserDb())
                    {
                        lblPassportParentDataError.Visible = false;
                        /*if (db.CheckExistsingPassport(parent.LastName, parent.FirstName, parent.PassportType, parent.Series, parent.Number, ParentId))
                        {
                            StringBuilder sb = new StringBuilder();
                            List<ExtendedUserPassportMessage> list = db.FindUdodByPassport(parent.PassportType, parent.Series, parent.Number);

                            sb.Append("Введённые Вами данные документа удостоверяющего личность уже были использованы: <br/>");
                            foreach (ExtendedUserPassportMessage user in list)
                            {
                                if (user.ParentFio == "")
                                {
                                    sb.Append("- в документе обучающегося " + user.ChildFio);
                                }
                                else
                                {
                                    sb.Append("- в документе " + user.ParentFio + ", являющегося представителем " + user.ChildFio);
                                }

                                foreach (ExtendedUdodPassportMessage udod in user.Udods)
                                {
                                    if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                    {
                                        sb.Append(", в заявлении(-ях) №");

                                        foreach (var n in udod.Claims.Where(j => j.ClaimId != 0)) // Вывести список этих ClaimId
                                        {
                                            sb.Append(n.Number.ToString() + ", ");
                                        }
                                        sb.Remove(sb.Length - 2, 2);
                                    }
                                    if (udod.Claims.Count(j => j.ClaimId == 0) > 0)
                                    {
                                        if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                        {
                                            sb.Append(", также являющимся обучающимся ");
                                        }
                                        else
                                        {
                                            sb.Append(", являющимся обучающимся ");
                                        }
                                    }
                                    sb.Append(" в учреждении " + udod.ShortName + ", телефон " + udod.PhoneNumber + ", ");
                                    sb.Remove(sb.Length - 2, 2);
                                }
                                sb.Append(";<br/>");
                            }
                            sb.Remove(sb.Length - 6, 6);
                            sb.Append(". <br/>Вам необходимо связаться с указанным(-ыми) УДО, определиться с чьей стороны допущена ошибка и внести необходимые коррективы.");

                            lblPassportParentDataError.Text = sb.ToString();
                            lblPassportParentDataError.Visible = true;
                            uplblParentError.Update();
                            return false;
                        }*/
                        lblPupilPassportDataError.Visible = false;
                        if (db.CheckExistsingPassport(isPupil ? parent.LastName : PupilEdit.LastName, isPupil ? parent.FirstName : PupilEdit.FirstName, isPupil ? parent.PassportType : PupilEdit.PassportType, isPupil ? parent.Series : PupilEdit.Series, isPupil ? parent.Number : PupilEdit.Number, isPupil ? ParentId : PupilId))
                        {
                            StringBuilder sb = new StringBuilder();
                            List<ExtendedUserPassportMessage> list = db.FindUdodByPassport(isPupil ? parent.PassportType : PupilEdit.PassportType, isPupil ? parent.Series : PupilEdit.Series, isPupil ? parent.Number : PupilEdit.Number);

                            sb.Append("Введённые Вами данные документа удостоверяющего личность уже были использованы: <br/>");
                            foreach (ExtendedUserPassportMessage user in list)
                            {
                                if (user.ParentFio == "" || user.ChildId==user.ParentId)
                                {
                                    sb.Append("- в документе обучающегося " + user.ChildFio);
                                }
                                else
                                {
                                    sb.Append("- в документе " + user.ParentFio + ", являющегося представителем " + user.ChildFio);
                                }

                                foreach (ExtendedUdodPassportMessage udod in user.Udods)
                                {
                                    if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                    {
                                        sb.Append(", в заявлении(-ях) №");

                                        foreach (var n in udod.Claims.Where(j => j.ClaimId != 0)) // Вывести список этих ClaimId
                                        {
                                            sb.Append(n.Number.ToString() + ", ");
                                        }
                                        sb.Remove(sb.Length - 2, 2);
                                    }
                                    if (udod.Claims.Count(j => j.ClaimId == 0) > 0)
                                    {
                                        if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                        {
                                            sb.Append(", также являющимся обучающимся ");
                                        }
                                        else
                                        {
                                            sb.Append(", являющимся обучающимся ");
                                        }
                                    }
                                    sb.Append(" в учреждении " + udod.ShortName + ", телефон " + udod.PhoneNumber + ", ");
                                    sb.Remove(sb.Length - 2, 2);
                                }
                                sb.Append(";<br/>");
                            }
                            sb.Remove(sb.Length - 2, 2);
                            sb.Append(". <br/>Вам необходимо связаться с указанным(-ыми) УДО, определиться с чьей стороны допущена ошибка и внести необходимые коррективы.");

                            //if (!isPupil)
                            {
                                lblPupilPassportDataError.Text = sb.ToString();
                                lblPupilPassportDataError.Visible = true;
                                
                            }
                            uplblError.Update();
                            return false;
                        }
                        uplblError.Update();
                    }
                    #endregion

                    // обновление сведений о заявителе и обучающемся
                    #region
                    using (PupilsDb db = new PupilsDb())
                    {
                        ExtendedPupil oldPupil = db.GetPupil(claim.Pupil.UserId);
                        ExtendedPupil newPupil = new ExtendedPupil();
                        newPupil.UserId = claim.Pupil.UserId;
                        newPupil.LastName = isPupil ? parent.LastName : PupilEdit.LastName;
                        newPupil.FirstName = isPupil ? parent.FirstName : PupilEdit.FirstName;
                        newPupil.MiddleName = isPupil ? parent.MiddleName : PupilEdit.MiddleName;
                        newPupil.Birthday = Convert.ToDateTime(PupilEdit.BirthDay);
                        newPupil.Sex = Convert.ToInt32(PupilEdit.Sex);
                        newPupil.PhoneNumber = isPupil ? parent.PhoneNumber : "";
                        newPupil.ExpressPhoneNumber = isPupil ? parent.ExpressPhoneNumber : "";
                        newPupil.EMail = isPupil ? parent.EMail : "";
                        newPupil.Passport = new ExtendedPassport()
                        {
                            PassportType = new ExtendedPassportType()
                            {
                                PassportTypeId = PupilEdit.PassportType
                            },
                            Series = PupilEdit.Series,
                            Number = PupilEdit.Number,
                            IssueDate = PupilEdit.IssueDate
                        };
                        newPupil.SchoolName = PupilEdit.SchoolName;
                        newPupil.ClassName = PupilEdit.ClassName;
                        newPupil.SchoolCode = PupilEdit.CodeEkis;
                        newPupil.ReestrCode = PupilReestrCode.HasValue?PupilReestrCode.Value.ToString():"";
                        db.UpdatePupil(claim.Pupil.UserId, newPupil);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("(Идентификатор Ученика: {0})", claim.Pupil.UserId);

                        if (oldPupil.Fio != newPupil.Fio)
                            sb.AppendFormat("(Имя: {0} -> {1})", oldPupil.Fio, newPupil.Fio);
                        if (oldPupil.Birthday != newPupil.Birthday)
                            sb.AppendFormat("(Дата рождения: {0} -> {1})", oldPupil.Birthday, newPupil.Birthday);
                        if (oldPupil.Passport.PassportType.PassportTypeId != newPupil.Passport.PassportType.PassportTypeId)
                            sb.AppendFormat("(Пасспорт, тип: {0} -> {1})", oldPupil.Passport.PassportType.PassportTypeId, newPupil.Passport.PassportType.PassportTypeId);
                        if (oldPupil.Passport.Series != newPupil.Passport.Series)
                            sb.AppendFormat("(Пасспорт, серия: {0} -> {1})", oldPupil.Passport.Series, newPupil.Passport.Series);
                        if (oldPupil.Passport.Number != newPupil.Passport.Number)
                            sb.AppendFormat("(Пасспорт, номер: {0} -> {1})", oldPupil.Passport.Number, newPupil.Passport.Number);
                        if (oldPupil.Passport.IssueDate != newPupil.Passport.IssueDate)
                            sb.AppendFormat("(Пасспорт, дата выдачи: {0} -> {1})", oldPupil.Passport.IssueDate, newPupil.Passport.IssueDate);
                        if (oldPupil.Sex != newPupil.Sex)
                            sb.AppendFormat("(Пол: {0} -> {1})", oldPupil.Sex, newPupil.Sex);
                        if (oldPupil.EMail != newPupil.EMail)
                            sb.AppendFormat("(EMail: {0} -> {1})", oldPupil.EMail, newPupil.EMail);
                        if (oldPupil.PhoneNumber != newPupil.PhoneNumber)
                            sb.AppendFormat("(Номер телефона 1: {0} -> {1})", oldPupil.PhoneNumber, newPupil.PhoneNumber);
                        if (oldPupil.ExpressPhoneNumber != newPupil.ExpressPhoneNumber)
                            sb.AppendFormat("(Номер телефона 2: {0} -> {1})", oldPupil.ExpressPhoneNumber, newPupil.ExpressPhoneNumber);
                        if (oldPupil.SchoolName != newPupil.SchoolName)
                            sb.AppendFormat("(Школа: {0} -> {1})", oldPupil.SchoolName, newPupil.SchoolName);
                        if (oldPupil.ClassName != newPupil.ClassName)
                            sb.AppendFormat("(Класс: {0} -> {1})", oldPupil.ClassName, newPupil.ClassName);

                        var oldParent = oldPupil.Parents.Where(i => i.UserId == ParentId).FirstOrDefault();

                        var newParent = new ExtendedUser();
                        newParent.UserId = claim.Parent.UserId;
                        newParent.LastName = parent.LastName;
                        newParent.FirstName = parent.FirstName;
                        newParent.MiddleName = parent.MiddleName;
                        newParent.PhoneNumber = parent.PhoneNumber;
                        newParent.ExpressPhoneNumber = parent.ExpressPhoneNumber;
                        newParent.EMail = parent.EMail;
                        newParent.Passport = new ExtendedPassport()
                        {
                            PassportType = new ExtendedPassportType()
                            {
                                PassportTypeId = parent.PassportType
                            },
                            Series = parent.Series,
                            Number = parent.Number,
                            IssueDate = parent.IssueDate
                        };
                        newParent.UserType = Convert.ToInt32(parent.Relativies);

                        db.UpdateParent(newParent);

                        sb.AppendFormat("(Идентификатор Заявителя: {0})", ParentId);
                        if (oldParent.UserType != newParent.UserType)
                            sb.AppendFormat("(Заявитель: {0} -> {1})", oldParent.UserType.ToString(), newParent.UserType.ToString());
                        if (oldParent.Fio != newParent.Fio)
                            sb.AppendFormat("(Имя заявителя: {0} -> {1})", oldParent.Fio, newParent.Fio);
                        if (oldParent.Passport.PassportType.PassportTypeId != newParent.Passport.PassportType.PassportTypeId)
                            sb.AppendFormat("(Пасспорт заявителя, тип: {0} -> {1})", oldParent.Passport.PassportType.PassportTypeId, newParent.Passport.PassportType.PassportTypeId);
                        if (oldParent.Passport.Series != newParent.Passport.Series)
                            sb.AppendFormat("(Пасспорт заявителя, серия: {0} -> {1})", oldParent.Passport.Series, newParent.Passport.Series);
                        if (oldParent.Passport.Number != newParent.Passport.Number)
                            sb.AppendFormat("(Пасспорт заявителя, номер: {0} -> {1})", oldParent.Passport.Number, newParent.Passport.Number);
                        if (oldParent.Passport.IssueDate != newParent.Passport.IssueDate)
                            sb.AppendFormat("(Пасспорт заявителя, дата выдачи: {0} -> {1})", oldParent.Passport.IssueDate, newParent.Passport.IssueDate);
                        if (oldParent.PhoneNumber != newParent.PhoneNumber)
                            sb.AppendFormat("(Номер телефона заявителя 1: {0} -> {1})", oldParent.PhoneNumber, newParent.PhoneNumber);
                        if (oldParent.ExpressPhoneNumber != newParent.ExpressPhoneNumber)
                            sb.AppendFormat("(Номер телефона заявителя 2: {0} -> {1})", oldParent.ExpressPhoneNumber, newParent.ExpressPhoneNumber);

                        EventsUtility.LogPupilUpdate(UserContext.Profile.UserId, Request.UserHostAddress, sb.ToString());
                    }
                    #endregion
                    lnkChangeStatusDocs.Visible = lnkChangeStatusEnroll.Visible = lnkChangeStatusNextYear.Visible = true;
                    StatusButtonsEnabling(claim.ClaimStatus.ClaimStatusId);
                }
                else
                {
                    //
                    //---------------------------------------------------------------------
                    // Новая заявка
                    using (UserDb db = new UserDb())
                    {
                        #region Проверяем паспорт родителя, если ОК то добавляем и получаем ParentId, нет - ошибка и ретурн
                        lblPassportParentDataError.Visible = false;
                        ParentId = db.CreateParent(parent.LastName,
                                                            parent.FirstName,
                                                            parent.MiddleName, DateTime.Now, false,
                                                            parent.PhoneNumber, parent.EMail, isPupil, parent.PassportType,
                                                            parent.Series, parent.Number, parent.IssueDate.Value, parent.Relativies, parent.ExpressPhoneNumber);
                        lblPassportParentDataError.Visible = false;
                        /*if (!db.CheckExistsingPassport(parent.LastName, parent.FirstName, parent.PassportType, parent.Series, parent.Number, null))
                        {
                            ParentId = db.CreateParent(parent.LastName,
                                                            parent.FirstName,
                                                            parent.MiddleName, DateTime.Now, false,
                                                            parent.PhoneNumber, parent.EMail, isPupil, parent.PassportType,
                                                            parent.Series, parent.Number, parent.IssueDate.Value, parent.Relativies, parent.ExpressPhoneNumber);
                            lblPassportParentDataError.Visible = false;
                        }
                        else
                        {
                            StringBuilder sb = new StringBuilder();
                            List<ExtendedUserPassportMessage> list = db.FindUdodByPassport(parent.PassportType, parent.Series, parent.Number);

                            sb.Append("Введённые Вами данные документа удостоверяющего личность уже были использованы: <br/>");
                            foreach (ExtendedUserPassportMessage user in list)
                            {
                                if (user.ParentFio == "")
                                {
                                    sb.Append("- в документе обучающегося " + user.ChildFio);
                                }
                                else
                                {
                                    sb.Append("- в документе " + user.ParentFio + ", являющегося представителем " + user.ChildFio);
                                }

                                foreach (ExtendedUdodPassportMessage udod in user.Udods)
                                {
                                    if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                    {
                                        sb.Append(", в заявлении(-ях) №");
                                    
                                    foreach (var n in udod.Claims.Where(j => j.ClaimId != 0)) // Вывести список этих ClaimId
                                    {
                                        sb.Append(n.Number.ToString() + ", ");
                                    }
                                    sb.Remove(sb.Length - 2, 2);
                                        }
                                    if (udod.Claims.Count(j => j.ClaimId == 0) > 0)
                                    {
                                        if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                        {
                                            sb.Append(", также являющимся обучающимся ");
                                        }
                                        else
                                        {
                                            sb.Append(", являющимся обучающимся ");
                                        }
                                    }
                                    sb.Append(" в учреждении " + udod.ShortName + ", телефон " + udod.PhoneNumber + ", ");
                                    sb.Remove(sb.Length - 2, 2);
                                }
                                sb.Append(";<br/>");
                            }
                            sb.Remove(sb.Length - 6, 6);
                            sb.Append(". <br/>Вам необходимо связаться с указанным(-ыми) УДО, определиться с чьей стороны допущена ошибка и внести необходимые коррективы.");

                            lblPassportParentDataError.Text = sb.ToString();
                            lblPassportParentDataError.Visible = true;
                            uplblParentError.Update();
                            return false;
                        }*/
                        #endregion
                        /*try
                        {
                            lblDeclarant.Text = "";
                            AsurClass asur = new AsurClass();
                            var asurguid = asur.GetDeclarant(parent.LastName, parent.FirstName, parent.MiddleName,
                                              parent.Relativies == 1 ? false : true, DateTime.Now);
                            lblDeclarant.Text = "ID заявителя в АС УР:" + asurguid.ToString();
                        }
                        catch(Exception e)
                        {
                            errorReestr.ShowError("", "Сервис АС УР временно недоступен", "", "OK", false, true);
                        }
                        */
                        //parent.Attributes.Add("title", asurguid.ToString());

                        #region Проверяем паспорт обучающегося, если ОК то добавляем и получаем PupilId, нет - ошибка и ретурн
                        if (!db.CheckExistsingPassport(isPupil ? parent.LastName : PupilEdit.LastName, isPupil ? parent.FirstName : PupilEdit.FirstName, isPupil ? parent.PassportType : PupilEdit.PassportType, isPupil ? parent.Series : PupilEdit.Series, isPupil ? parent.Number : PupilEdit.Number, null))
                        {
                            PupilId = db.CreatePupil(ParentId, isPupil ? parent.LastName : PupilEdit.LastName, isPupil ? parent.FirstName : PupilEdit.FirstName, isPupil ? parent.MiddleName : PupilEdit.MiddleName,
                                                    PupilEdit.BirthDay, Convert.ToBoolean(PupilEdit.Sex), isPupil, PupilEdit.SchoolName, PupilEdit.CodeEkis, PupilEdit.ClassName,
                                                    PupilEdit.PassportType, PupilEdit.Series, PupilEdit.Number, PupilEdit.IssueDate.Value, PupilReestrCode.HasValue?PupilReestrCode.Value.ToString():"", PupilEdit.ReestrGuid);

                            if (!isPupil)
                                lblPupilPassportDataError.Visible = false;
                        }
                        else
                        {
                            StringBuilder sb = new StringBuilder();
                            List<ExtendedUserPassportMessage> list = db.FindUdodByPassport(isPupil ? parent.PassportType : PupilEdit.PassportType, isPupil ? parent.Series : PupilEdit.Series, isPupil ? parent.Number : PupilEdit.Number);
                            sb.Append("Введённые Вами данные документа удостоверяющего личность уже были использованы: <br/>");
                            foreach (ExtendedUserPassportMessage user in list)
                            {
                                if (user.ParentFio == "" || user.ChildId == user.ParentId)
                                {
                                    sb.Append("- в документе обучающегося " + user.ChildFio);
                                }
                                else
                                {
                                    sb.Append("- в документе " + user.ParentFio + ", являющегося представителем " + user.ChildFio);
                                }

                                foreach (ExtendedUdodPassportMessage udod in user.Udods)
                                {
                                    if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                    {
                                        sb.Append(", в заявлении(-ях) №");

                                        foreach (var n in udod.Claims.Where(j => j.ClaimId != 0)) // Вывести список этих ClaimId
                                        {
                                            sb.Append(n.Number.ToString() + ", ");
                                        }
                                        sb.Remove(sb.Length - 2, 2);
                                    }
                                    if (udod.Claims.Count(j => j.ClaimId == 0) > 0)
                                    {
                                        if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                        {
                                            sb.Append(", также являющимся обучающимся по программам дополнительного образования ");
                                        }
                                        else
                                        {
                                            sb.Append(", являющимся обучающимся по программам дополнительного образования ");
                                        }
                                    }
                                    sb.Append(" в учреждении " + udod.ShortName + ", телефон " + udod.PhoneNumber + ", ");
                                    sb.Remove(sb.Length - 2, 2);
                                }
                                sb.Append(";<br/>");
                            }
                            sb.Remove(sb.Length - 6, 6);
                            sb.Append(". <br/>Вам необходимо связаться с указанным(-ыми) УДО, определиться с чьей стороны допущена ошибка и внести необходимые коррективы.");

                            //if (!isPupil)
                            {
                                lblPupilPassportDataError.Text = sb.ToString();
                                lblPupilPassportDataError.Visible = true;
                            }
                            uplblError.Update();
                            //upPupilEdit.Update();
                            return false;
                        }
                        #endregion
                    }
                }

                //
                //---------------------------------------------------------------------
                // Это в обоих случаях, проверка на лимит УДО и немосквичей на платное
                ExtendedLimit limit;
                using (PupilsDb db = new PupilsDb())
                {
                    //limit = db.GetLimit(PupilId, null);
                }
                DateTime now = DateTime.Today;
                int fullYears = now.Year - PupilEdit.BirthDay.Year;
                if (PupilEdit.BirthDay > now.AddYears(-fullYears)) fullYears--;
                bool islimitHours = false;

                if (RegAddress.Code.Length > 8)
                    using (ChildUnionDb db = new ChildUnionDb())
                    {
                        ExtendedChildUnion cu = db.GetChildUnion(selectUdod.UdodAgeGroupId.Value, null);
                        foreach (ExtendedTypeBudget tb in cu.BudgetTypes)
                        {
                            if (tb.DictTypeBudgetId == 2)
                            {
                                lblLimit.Text = "По существующим правилам, для детей, не имеющих московской регистрации, предоставляется возможность записаться на обучение в учреждения дополнительного образования только на платные программы обучения.";
                                upLimit.Update();
                                popupLimitExtender.Show();
                                return false;
                            }
                        }
                    }
                //
                //---------------------------------------------------------------------
                // адрес пупилу можно добавить в любом случае - и при новой заявке и при редактировании
                using (KladrDb db = new KladrDb())
                {
                    db.AddAddress(RegAddress.Code, RegAddress.Index, RegAddress.NumberHouse, RegAddress.Fraction,
                                  RegAddress.Housing, RegAddress.Building, RegAddress.Flat, RegAddress.AddressTypeId, 0, 0,
                                  PupilId, null, string.Empty, string.Empty, string.Empty, RegAddress.typeAddressCode, string.Empty);
                }

                //
                //---------------------------------------------------------------------
                // создание новой заявки или редактирование старой, продолжение
                if (UserContext.ClaimId.HasValue)
                {
                    using (ClaimDb db = new ClaimDb())
                    {
                        //
                        //---------------------------------------------------------------------
                        // Редактирование и запись в логи, в текущий клейм обновляем нашего пупила (возможно отредактированного) но ИД тот же,
                        // новую дату начала, новый выбранный ДО, статус зачем-то и год обучения
                        var prev = db.GetClaim(UserContext.ClaimId.Value);

                        long? number = db.UpdateClaim(UserContext.ClaimId.Value, PupilId,
                                       !string.IsNullOrEmpty(tbBeginDate.Text)
                                           ? Convert.ToDateTime(tbBeginDate.Text)
                                           : DateTime.Now, selectUdod.UdodAgeGroupId.Value,
                                            prev.ClaimStatus.ClaimStatusId,
                                       Convert.ToInt32(ddlYearOfStudy.SelectedValue), PupilEdit.IsReestr);

                        if (number == -1)
                        {
                            errorReestr.ShowError("Внимание.", "В выбранное ДО на данный Год Обучения обучающийся не подходит по возрасту. Выберите другое ДО или измените Год Обучения.", "", "ОК", false, true);
                            //Response.Write("<script>window.alert('В выбранное ДО обучающийся не подходит по возрасту. Выберите другое ДО.');</script>");
                            return false;
                        }

                        var curr = db.GetClaim(UserContext.ClaimId.Value);

                        if (prev.ClaimStatus.ClaimStatusId != curr.ClaimStatus.ClaimStatusId)
                        {
                            EventsUtility.LogClaimStatusUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                                String.Format("(Номер заявления: {0})(Статус: {1} -> {2})", prev.Number, prev.ClaimStatus.ClaimStatusName, curr.ClaimStatus.ClaimStatusName), PupilId, UserContext.ClaimId.Value, curr.Udod.UdodId, curr.ChildUnion.ChildUnionId);
                        }

                        EventsUtility.LogClaimUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                            EventsUtility.GetClaimUpdateMessage(prev, curr));
                    }
                }
                else
                {
                    //
                    //---------------------------------------------------------------------
                    // Новая заявка, сразу возьмем ее ид и запишем в ЮзерКонтекст, логирование, проверочки всякие, если что ретурн (хотя зачем, заявка то уже создалась, ничего не поделаешь...)
                    long? claimnumber = null;
                    using (ClaimDb db = new ClaimDb())
                    {
                        currentClaimNumber = claimnumber = db.AddClaim(PupilId,
                            !string.IsNullOrEmpty(tbBeginDate.Text) ? Convert.ToDateTime(tbBeginDate.Text) : DateTime.Now, selectUdod.UdodAgeGroupId.Value,
                            Convert.ToInt32(ddlYearOfStudy.SelectedValue), UserContext.Roles.RoleId.Value, ParentId, PupilEdit.IsReestr);

                        if (claimnumber == -1)
                        {
                            errorReestr.ShowError("Внимание.", "В выбранное ДО на данный Год Обучения обучающийся не подходит по возрасту. Выберите другое ДО или измените Год Обучения.", "", "ОК", false, true);
                            //Response.Write("<script>window.alert('В выбранное ДО обучающийся не подходит по возрасту. Выберите другое ДО.');</script>");
                            return false;
                        }

                        claim = db.GetClaims(null, null, null, null, null, null, claimnumber.ToString(),
                            null, null, null, null, null, null, null, null, null, null, null, null, null, null).FirstOrDefault();

                        UserContext.ClaimId = claim.ClaimId;
                        EventsUtility.LogClaimCreate(UserContext.Profile.UserId, Request.UserHostAddress, String.Format("(Номер заявления: {0})", claimnumber), PupilId, claim.ClaimId, claim.Udod.UdodId, claim.ChildUnion.ChildUnionId);
                        /*
                        #region Отправка в ЕСЗ

                        Guid bookingId = Guid.NewGuid();
                        if (SiteUtility.SendClaimInESZ(new ExtendedPupil()
                                                       {
                                                           LastName = isPupil ? parent.LastName : PupilEdit.LastName,
                                                           FirstName = isPupil ? parent.FirstName : PupilEdit.FirstName,
                                                           MiddleName = isPupil ? parent.MiddleName : PupilEdit.MiddleName,
                                                           Birthday = PupilEdit.BirthDay,
                                                           Sex = PupilEdit.Sex,
                                                           Passport = new ExtendedPassport()
                                                                          {
                                                                              Series = isPupil ? parent.Series : PupilEdit.Series,
                                                                              Number = isPupil ? parent.Number : PupilEdit.Number,
                                                                              IssueDate = isPupil ? parent.IssueDate : PupilEdit.IssueDate,
                                                                              PassportType = new ExtendedPassportType()
                                                                                                 {
                                                                                                     PassportTypeId = isPupil ? parent.PassportType : PupilEdit.PassportType
                                                                                                 }
                                                                          }
                                                       }, new ExtendedPupil()
                                                              {
                                                                  LastName = parent.LastName,
                                                                  FirstName = parent.FirstName,
                                                                  MiddleName = parent.MiddleName,
                                                                  UserId = ParentId
                                                              }, claim.ClaimId, selectUdod.UdodAgeGroupId.Value, ""))
                        {
                            using (MessageDb db2 = new MessageDb())
                            {
                                db2.AddMessage(bookingId, claim.ClaimId, 1);
                            }
                        }
                        #endregion */
                    }

                    if (claimnumber == basedOnClaimNumber)
                    {
                        Response.Write("<script>window.alert('Заявка в выбранное ДО для данного обучающегося уже создана');</script>");
                        return false;
                    }

                    if (enrollRightNow)
                    {
                        changeStatus("lnkChangeStatusEnroll");
                    }
                    //
                    //---------------------------------------------------------------------
                    // Выводим попап с информацией только при новой заявке и только если стоит флаг showSaveClaimPopup
                    #region
                    if (showSaveClaimPopup)
                    {
                        lblClaimInfo.Text = "Номер заявления: <b>" + (claimnumber.HasValue ? claimnumber.Value.ToString() : "") + "</b><br/>";
                        lblClaimInfo.Text += "ФИО обучающегося: <b>" + (isPupil ? parent.LastName : PupilEdit.LastName) + " " + (isPupil ? parent.FirstName : PupilEdit.FirstName) + " " + (isPupil ? parent.MiddleName : PupilEdit.MiddleName) + "</b><br/>";
                        lblClaimInfo.Text += "Детское объединение: <b>" + lblChildUnion.Text + "</b><br/>";
                        lblClaimInfo.Text += "Желаемая дата: <b>" + tbBeginDate.Text + "</b><br/>";

                        using (PupilsDb db = new PupilsDb())
                        {
                            limit = db.GetLimit(PupilId, null);
                        }

                        if (limit.Hours + limit.ClaimHours > limit.NormHours)
                        {
                            islimitHours = true;
                        }

                        if (islimitHours)
                            lblClaimInfo.Text +=
                                "<span style='color: red;'> Внимание! Для данного учащегося возможно превышение суммарного  <br/>" +
                                "количества часов по всем зачислениям во все детские объединения. <br/>" +
                                "Это может привести к повышенной утомляемости ребёнка. </span><br/>";

                        if (claimnumber.HasValue)
                        {
                            string message = EmailBody.Text
                                .Replace("#LastName#", parent.LastName)
                                .Replace("#FirstName#", parent.FirstName)
                                .Replace("#MiddleName#", parent.MiddleName)
                                .Replace("#DeadlineDate#", DateTime.Today.AddDays(7).ToShortDateString())
                                .Replace("#ClaimNumber#", (claimnumber.HasValue ? claimnumber.Value.ToString() : ""));

                            using (UserDb db2 = new UserDb())
                            {
                                var pupil = db2.GetUser(PupilId);
                                message = message.Replace("#PupilFio#", pupil.Fio);
                            }

                            using (ChildUnionDb db2 = new ChildUnionDb())
                            {
                                var udod = db2.GetUdod(selectUdod.UdodAgeGroupId.Value);
                                message = message.Replace("#OrgName#", udod.Name)
                                                    .Replace("#OrgSite#", udod.SiteUrl);

                                var cu = db2.GetChildUnion(selectUdod.UdodAgeGroupId.Value, null);
                                message = message.Replace("#ChildUnionName#", cu.Name);
                            }

                            if ((!SendEmailMessage(parent.EMail, EmailSubject.Text, message)) && (!string.IsNullOrEmpty(parent.EMail)))
                            {
                                lblClaimInfo.Text += "<b style='color: red'>Не удалось отправить письмо с уведомлением на ящик " + parent.EMail + ". Обратитесь к администратору системы.</b><br/>";
                            }
                            /*if (!string.IsNullOrEmpty(parent.PhoneNumber) && parent.PhoneNumber.Length==10)
                            {
                                SendSms sms = new SendSms("Заявление в дополнительное образование создано под номером "+claimnumber, parent.PhoneNumber);
                            }
                            if (!string.IsNullOrEmpty(parent.ExpressPhoneNumber) && parent.ExpressPhoneNumber.Length == 10)
                            {
                                SendSms sms = new SendSms("Заявление в дополнительное образование создано под номером " + claimnumber, parent.ExpressPhoneNumber);
                            }*/
                        }
                        
                        
                        popupSaveClaimExtender.Show();
                    }
                    #endregion
                }
            }
        }
        catch (Exception e)
        {
            //errorReestr.ShowError("", e.Message, "", "OK", false,true);
            popupErrorExtender.Show();
        }
        return true;
    }

    protected void lnkSave_Click(object sender, EventArgs e)
    {
        SaveClaim(true);
    }

    private bool SendEmailMessage(string emailTo, string subject, string message)
    {
        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
        System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(ConfigurationManager.AppSettings["SmtpHost"], Int32.Parse(ConfigurationManager.AppSettings["SmtpPort"]));
        try
        {
            msg.From = new MailAddress(ConfigurationManager.AppSettings["SmtpFrom"], ConfigurationManager.AppSettings["SmtpFromName"]);
            msg.To.Add(emailTo);

            msg.Subject = subject;
            msg.IsBodyHtml = true;
            msg.Body = message;

            //smtpClient.EnableSsl = true;
            //smtpClient.UseDefaultCredentials = true;
            //smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SmtpUser"], ConfigurationManager.AppSettings["SmtpPass"]);

            smtpClient.Send(msg);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    protected void btnSelectUdod_Click(object sender, EventArgs e)
    {
        popupSelectExtender.Hide();

        //если зачислить прямо сейчас то якобы жмем save сразу же, если нет - заполняем таблицу выбранным ДО
        if (!enrollRightNow)
        {
            ExtendedChildUnion item;
            using (ChildUnionDb db = new ChildUnionDb())
            {
                item = db.GetChildUnion(selectUdod.UdodAgeGroupId, null);
                lblProgram.Text = item.Program.Name;
                lblSection.Text = item.Section.Name;
                lblChildUnion.Text = item.Name;
                lblAge.Text = "от " + item.AgeStart.ToString() + " до " + item.AgeEnd.ToString();
                ddlYearOfStudy.Items.Clear();
                for (int i = 1; i <= item.NumYears; i++)
                {
                    ddlYearOfStudy.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
            using (KladrDb db = new KladrDb())
            {
                item.Addresses = db.GetAddressesById(null, null, selectUdod.UdodAgeGroupId, null);
                if (item.Addresses.Count() > 0) lblAddress.Text = item.Addresses.FirstOrDefault().AddressStr;
            }
            lblChildUnionDeleted.Visible = false;
            lnkDetail.Visible = true;
            lnkSelectUdod.Text = "<div class='btnBlueLong'>Изменить ДО</div>";
            upUdod.Update();
        }
        else
        {
            if (SaveClaim(false))
                Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
        }
    }

    protected void lnkSelectUdod_Click(object sender, EventArgs e)
    {
        enrollRightNow = false;
        popupSelectExtender.Show();
        popupSelectUdod.Style["visibility"] = "visible";
        popupSelectUdod.Style["display"] = "block";
    }

    protected void lnkDetail_OnClick(object sender, EventArgs e)
    {
        // Загружать детальный просмотр usercontrol Глеба
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var childunion = db.GetChildUnion(selectUdod.UdodAgeGroupId, null);
            if (childunion != null)
                selectDO.SetDOData(childunion);
            popupSelectDOExtender.Show();
            upDO.Update();
        }
    }

    protected void btnSaveClaim_OnClick(object sender, EventArgs e)
    {
        // В зависимости от выбора
        if (select.SelectedValue != "4")
        UserContext.ClaimId = null;
        if (select.SelectedValue == "0")
        {
            Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
        }
        if (select.SelectedValue == "1")
        {
            PupilEdit.Clear();
            PupilEdit.Enabled = true;
            lnkSave.Visible = false;
            upLnk.Update();
        }
        if (select.SelectedValue == "2")
        {
            UserContext.ClaimId = null;
            Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
        }
        if (select.SelectedValue == "3")
        {
            // редирект на список заявок
            Page.Response.Redirect(SiteUtility.GetUrl("~/udod/ClaimList.aspx"));
        }
        if (select.SelectedValue == "4")
        {
            // открыть все кнопки
            Page_Load(sender, e);
            lnkChangeStatusDocs.Visible = lnkChangeStatusEnroll.Visible = lnkChangeStatusNextYear.Visible = true;
            StatusButtonsEnabling(1);
        }
        popupSaveClaimExtender.Hide();
    }

    protected void rbnIsPupil_SelectedChanged(object sender, EventArgs e)
    {
        parent.RelativeEnabled = !Convert.ToBoolean(rbnIsPupil.SelectedValue);
        PupilEdit.Enable14YearsValidation = Convert.ToBoolean(rbnIsPupil.SelectedValue);
        PupilEdit.FioEnabled = !Convert.ToBoolean(rbnIsPupil.SelectedValue);
        PupilEdit.PassportEnabled = !Convert.ToBoolean(rbnIsPupil.SelectedValue) && !IsReestr;
        PupilEdit.LastName = parent.LastName;
        PupilEdit.FirstName = parent.FirstName;
        PupilEdit.MiddleName = parent.MiddleName;
        parent.reestrValidatorsEnabled = true;
        PupilEdit.reestrValidatorsEnabled = false;
        if (rbnIsPupil.SelectedIndex == 0)
        {
            parent.reestrValidatorsEnabled = false;
            PupilEdit.reestrValidatorsEnabled = true;
            parent.Enabled = true;
            PupilEdit.Enabled = true;
            PupilEdit.SetDefault = true;
            //PupilEdit.IsReestr = false;
        }
        IsReestr = false;
        upPupilEdit.Update();
        upIsPupil.Update();
    }

    protected void lnkCancel_OnClick(object sender, EventArgs e)
    {
        Page.Response.Redirect(SiteUtility.GetUrl("~/Udod/ClaimList.aspx"));
    }

    protected void changeStatus_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        changeStatus(btn.ID);
    }

    protected void changeStatus(string btnId)
    {
        int statusId = 4;
        //int DictTypeBudgetId;
        List<ExtendedTypeBudget> typeBudgets = new List<ExtendedTypeBudget>();
        ExtendedChildUnion childUnion = new ExtendedChildUnion();
        ExtendedClaim claim = new ExtendedClaim();

        using (ClaimDb db = new ClaimDb())
        {
            claim = db.GetClaim(UserContext.ClaimId.Value);
        }

        using (ChildUnionDb db = new ChildUnionDb())
        {
            childUnion = db.GetChildUnion(claim.ChildUnion.ChildUnionId, null);

        }
        if (btnId == "lnkChangeStatusEnroll" || btnId == "lnkzach")
        {
            /*if (claim.BeginDate.Date > DateTime.Now.Date)
            {
                errorReestr.ShowError("Ошибка", "Зачисление невозможно так как указанная в заявление желаемая дата поступления ещё не наступила.", "", "OK", false, true);
                return;
            }*/

            using (PupilsDb db2 = new PupilsDb())
            {
                if (!db2.GetIsDismissed(UserContext.ClaimId.Value, null, null))
                {
                    errorReestr.ShowError("Ошибка", "Данный обучающийся уже зачислен в выбранное ДО", "", "OK", false, true);
                    return;
                }
            }

            /*using (AgeGroupDb db2 = new AgeGroupDb())
            {
                if (!db2.GetAgeGroup(claim.AgeGroup.UdodAgeGroupId).IsActive)
                {
                    errorReestr.ShowError("Ошибка", "За время от создания заявления, в выбранном детском объединении произошли изменения. В настоящее время ЗАЧИСЛЕНИЕ в желаемый год обучения с текущим возрастом будущего обучающегося НЕВОЗМОЖНО.", "", "OK", false, true);
                    return;
                }
            }*/
            statusId = 4;
        }
        if (btnId == "lnkChangeStatusAnnul") statusId = 5;
        if (btnId == "lnkChangeStatusDocs") statusId = 7;
        if (btnId == "lnkChangeStatusReserv") statusId = 6;
        if (btnId == "lnkChangeStatusNextYear" || btnId == "lnkzachnextyear") statusId = 8;

        if (statusId==5)
        {
            using (CommonDb db =new CommonDb())
            {
                rbAnnul.DataSource = db.GetReasonAnnuls(claim.ClaimId);
                rbAnnul.DataBind();
            }
            rbAnnul.SelectedIndex = -1;
            modalPopupAnnul.Show();
            rbAnnul.Attributes.Add("claimId", claim.ClaimId.ToString());
            rbAnnul.Attributes.Add("statusId", statusId.ToString());
            rbAnnul.Attributes.Add("pupilId", claim.Pupil.UserId.ToString());
            rbAnnul.Attributes.Add("pupilBirthday", claim.Pupil.Birthday.ToString());
            rbAnnul.Attributes.Add("childUnionId", claim.ChildUnion.ChildUnionId.ToString());
            return;
        }

        if (statusId == 4)
        {
            typeBudgets = childUnion.BudgetTypes;
            if (false)
            {
                DictTypeBudgetId = typeBudgets[0].DictTypeBudgetId;

                if (DictTypeBudgetId == 2)
                {
                    // проверка на 20 часов и на 3 УДО
                    ExtendedLimit limit;
                    using (PupilsDb db = new PupilsDb())
                    {
                        limit = db.GetLimit(claim.Pupil.UserId, null);
                    }

                    DateTime now = DateTime.Today;
                    int fullYears = now.Year - claim.Pupil.Birthday.Year;
                    if (claim.Pupil.Birthday > now.AddYears(-fullYears)) fullYears--;

                    using (AgeGroupDb db = new AgeGroupDb())
                    {
                        /*var list = db.GetAgeGroups(null, null, null, childUnion.ChildUnionId);
                        int minYear = list.Select(i => i.Age).Min();
                        int maxYear = list.Select(i => i.Age).Max();
                        if (fullYears < minYear) fullYears = minYear;
                        if (fullYears > maxYear) fullYears = maxYear;
                        ExtendedAgeGroup item = list.Where(i => i.Age == fullYears).FirstOrDefault();*/

                        ExtendedAgeGroup item = db.GetAgeGroup(claim.AgeGroup.UdodAgeGroupId);

                        if ((limit.Hours + ((double)(item.LessonLength * item.LessonsInWeek) / 60)) > limit.NormHours)
                        {
                            bool isPupil = claim.Pupil.UserId == claim.Parent.UserId;
                            StringBuilder limitText = new StringBuilder();
                            limitText.Append(isPupil ? claim.Parent.LastName : claim.Pupil.LastName);
                            limitText.Append(" " + (isPupil ? claim.Parent.FirstName : claim.Pupil.FirstName));
                            limitText.Append(" " + (isPupil ? claim.Parent.MiddleName : claim.Pupil.MiddleName));
                            limitText.Append(" уже посещает ");
                            using (PupilsDb db2 = new PupilsDb())
                            {
                                List<ExtendedChildUnion> l = db2.GetChildUnion(claim.Pupil.UserId);
                                var previousUdodName = l.Count() > 0 ? l[0].UdodShortName : "";
                                var previousUdodPhone = l.Count() > 0 ? l[0].UdodPhone : "";
                                foreach (ExtendedChildUnion cu in l)
                                {
                                    if (previousUdodName != cu.UdodShortName)
                                    {
                                        limitText.Append(" в УДО " + previousUdodName + " (тел: " + previousUdodPhone + "); ");
                                    }
                                    limitText.Append(cu.Name + ", ");
                                    previousUdodName = cu.UdodShortName;
                                    previousUdodPhone = cu.UdodPhone;
                                }
                                limitText.Remove(limitText.Length - 2, 2);
                                limitText.Append(" в УДО " + previousUdodName + " (тел: " + previousUdodPhone + ") ");
                            }
                            limitText.Append(" с суммарным количеством часов занятий ");
                            limitText.Append(limit.Hours.ToString("F2") + " часов в неделю. ");
                            limitText.Append("Для детей его/её возраста действует ограничение СанПиН в ");
                            limitText.Append(limit.NormHours.ToString("F2") + " часов занятий в неделю. ");
                            limitText.Append("В связи с чем осуществить зачисление в ДО ");
                            limitText.Append(childUnion.Name + " с продолжительностью занятий ");
                            limitText.Append((item.LessonLength * item.LessonsInWeek / 60.0).ToString("F2") + " часов в неделю невозможно, ");
                            limitText.Append("так как это приводит к превышению установленных норм.");

                            lblLimit.Text = limitText.ToString();
                            upLimit.Update();
                            popupLimitExtender.Show();
                            return;
                        }
                    }
                }

                //UpdateStatus(claim.ClaimId, statusId, null);
            }
            else
            {
                // показать 
                using (AgeGroupDb db = new AgeGroupDb())
                {
                    var list = db.GetGroupsPaging(null, null, claim.ChildUnion.ChildUnionId, null, null, null, "", 1,
                                                  500000, true);
                    ddlGroups.DataSource = list;
                    ddlGroups.DataBind();
                    ddlGroups_OnSelectedIndexChanged(ddlGroups, new EventArgs());
                }
                
                popupTypeBudgetExtender.Show();
                ddlGroups.Attributes.Add("claimId", claim.ClaimId.ToString());
                ddlGroups.Attributes.Add("statusId", statusId.ToString());
                ddlGroups.Attributes.Add("pupilId", claim.Pupil.UserId.ToString());
                ddlGroups.Attributes.Add("pupilBirthday", claim.Pupil.Birthday.ToString());
                ddlGroups.Attributes.Add("childUnionId", claim.ChildUnion.ChildUnionId.ToString());
                ddlGroups.Attributes.Add("NumYear", claim.NumYear.ToString());
            }
        }
        else
        {
            UpdateStatus(claim.ClaimId, statusId, null, null, null, null,null);
        }
    }

    private void UpdateStatus(long claimId, int statusId, int? reasonAnnulId, long? udodGroupId, long? ageGroupId, DateTime? startClaim, DateTime? endClaim)
    {
        using (ClaimDb db = new ClaimDb())
        {
            var claim = db.GetClaim(claimId);
            if (claim.ClaimStatus.ClaimStatusName == lblStatus.Text)
            {
                db.UpdateClaimStatus(UserContext.ClaimId.Value, statusId, 2, reasonAnnulId, udodGroupId);
                string comment = "";
                if (reasonAnnulId.HasValue)
                {
                    comment = rbAnnul.SelectedItem.Text;
                }
                
                EventsUtility.LogClaimStatusUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                                                   String.Format("(Номер заявления: {0})(Статус: {1} -> {2})",
                                                                 claim.Number, claim.ClaimStatus.ClaimStatusId, statusId),
                                                   claim.Pupil.UserId, claim.ClaimId, claim.Udod.UdodId,
                                                   claim.ChildUnion.ChildUnionId);

//               ExportInAsGuf tmp = new ExportInAsGuf();
                //tmp.UpdateStatus(statusId, UserContext.ClaimId.Value, comment);

                /*if (ageGroupId.HasValue && startClaim.HasValue && endClaim.HasValue)
                {
                    tmp.UpdateSchedule(claim.ChildUnion.ChildUnionId, ageGroupId.Value, startClaim.Value, endClaim.Value);
                    
                }*/
            }
            claim = db.GetClaim(UserContext.ClaimId.Value);
            lblStatus.Text = claim.ClaimStatus.ClaimStatusName;
            StatusButtonsEnabling(claim.ClaimStatus.ClaimStatusId);

            if ((claim.ClaimStatus.ClaimStatusId == 5) || (claim.ClaimStatus.ClaimStatusId == 4))
            {
                parent.Enabled = PupilEdit.Enabled =
                                 RegAddress.Enabled =
                                 lnkSelectUdod.Visible =
                                 tbBeginDate.Enabled =
                                 ddlYearOfStudy.Enabled =
                                 lnkSave.Visible =
                                 rbnIsPupil.Enabled = false;

                lnkSave.Visible = false;
            }
            /*
            #region Отправка в Реестр и ДО и обучающегося
            // Получаем обучающегося
            if (statusId == 4)
            {

                ExtendedPupil pupil = null;
                using (PupilsDb db1 = new PupilsDb())
                {
                    pupil = db1.GetPupil(claim.Pupil.UserId);



                    if (pupil != null)
                    {
                        using (KladrDb db2 = new KladrDb())
                        {
                            var addresses = db2.GetAddressesById(pupil.UserId, null, null, null);
                            pupil.Addresses = addresses;
                        }

                        // добавление или изменение
                        if (!pupil.ReestrGuid.HasValue || pupil.ReestrCode == "0")
                        {
                            string mappath = Page.MapPath(pupil.Passport.PassportType.PassportTypeId == 2
                                 ? "../Templates/search_Passport_by_Series_and_Number_rq.xml"
                                 : "../Templates/search_by_Series_and_Number_rq.xml");


                            try
                            {
                                var pupilstmp = SiteUtility.GetPupilData(mappath,
                                                     pupil.Passport.Series,
                                                     pupil.Passport.Number);
                                if (pupilstmp.Count == 1) pupil = pupilstmp.FirstOrDefault();
                                else
                                {
                                    if (pupilstmp.Count(i => i.LastName.Trim().ToUpper() == pupil.LastName.Trim().ToUpper() && i.FirstName.Trim().ToUpper() == pupil.FirstName.Trim().ToUpper()) != 0)
                                    {
                                        var pupil1 =
                                            pupilstmp.FirstOrDefault(
                                               i => i.LastName.Trim().ToUpper() == pupil.LastName.Trim().ToUpper() && i.FirstName.Trim().ToUpper() == pupil.FirstName.Trim().ToUpper());
                                        pupil.ReestrCode = pupil1.ReestrCode;
                                        pupil.ReestrGuid = pupil1.ReestrGuid;
                                        int? schoolCode = null;
                                        try
                                        {
                                            schoolCode = Convert.ToInt32(pupil1.SchoolCode);
                                        }
                                        catch (Exception)
                                        {
                                            schoolCode = null;
                                        }
                                        db1.UpdatePupilReestrCode(pupil.UserId, null, Convert.ToInt64(pupil.ReestrCode), pupil.ReestrGuid, "", schoolCode, pupil.SchoolName, pupil.ClassName);
                                    }
                                }
                            }
                            catch (Exception)
                            {
                            }


                            Guid? reestrGuid = null;
                            string pupilReestrUdodCode = "";
                            if (string.IsNullOrEmpty(pupil.ReestrCode))
                                pupilReestrUdodCode = SiteUtility.ReestrAddPupil(pupil, !string.IsNullOrEmpty(pupil.ReestrUdodCode), ref reestrGuid);
                            
                            if (reestrGuid.HasValue)
                            {
                                pupil.ReestrGuid = reestrGuid;
                            }
                        }


                        using (ChildUnionDb db2 = new ChildUnionDb())
                        {
                            try
                            {
                                var childunion = db2.GetChildUnion(claim.ChildUnion.ChildUnionId, null);
                                long result = -1;
                                if (string.IsNullOrEmpty(childunion.ReestrCode))
                                {
                                    var teachers = db2.GetTeacher(childunion.ChildUnionId);

                                    result = SiteUtility.ReestrAddDO(childunion, false, teachers);
                                }
                                else
                                {
                                    result = Convert.ToInt64(childunion.ReestrCode);
                                }
                                //errorClaimList.ShowError("Проверка", result.ToString(), "ок", "cancel", true,false);
                                if (result != -1)
                                {
                                    childunion.ReestrCode = result.ToString();
                                    //отправляем зачисление
                                    //SiteUtility.ReestrAddZach(extendedPupil, childunion, extendedChildUnion, false);
                                    var childunionZach =
                                        db1.GetChildUnion(pupil.UserId).Where(
                                            i => i.ChildUnionId == childunion.ChildUnionId).FirstOrDefault();
                                    if (childunionZach != null)
                                        SiteUtility.ReestrAddZach(pupil, childunion, childunionZach, false);
                                }
                            }
                            catch (Exception e1)
                            {

                            }
                        }
                    }
                }
            }

            #endregion
           */ 
        }
    }

    protected void lnkCreateClaimFromCurrent_OnClick(object sender, EventArgs e)
    {
        UserContext.ClaimId = null;
        enrollRightNow = true;
        popupSelectExtender.Show();
    }

    protected void StatusButtonsEnabling(int claimStatusId)
    {
        lnkChangeStatusDocs.Enabled = false;
        lnkChangeStatusEnroll.Enabled = false;
        lnkChangeStatusNextYear.Enabled = false;
        lnkChangeStatusAnnul.Enabled = false;
        //lnkChangeStatusInviteDiv.Style["background-color"] = "RGB(216,216,216)";
        lnkChangeStatusDocsDiv.Style["background-color"] = "RGB(180,180,180)";
        lnkChangeStatusEnrollDiv.Style["background-color"] = "RGB(180,180,180)";
        divChangeStatusNextYear.Style["background-color"] = "RGB(180,180,180)";
        divChangeStatusAnnul.Style["background-color"] = "RGB(180,180,180)";

        if (claimStatusId == 1) // если на рассмотрении, то можно аннулировать, либо подать документы либо перевести в резерв
        {
            lnkChangeStatusDocs.Enabled = true;
            //lnkChangeStatusNextYear.Enabled = true;
            lnkChangeStatusAnnul.Enabled = true;
            lnkChangeStatusDocsDiv.Style["background-color"] = "RGB(40,84,147)";
            //divChangeStatusReserv.Style["background-color"] = "RGB(40,84,147)";
            divChangeStatusAnnul.Style["background-color"] = "RGB(40,84,147)";
        }
        if (claimStatusId == 7) // если поданы документы, то можно аннулировать, либо зачислить либо перевести в резерв
        {
            lnkChangeStatusEnroll.Enabled = true;
            lnkChangeStatusNextYear.Enabled = true;
            lnkChangeStatusAnnul.Enabled = true;
            lnkChangeStatusEnrollDiv.Style["background-color"] = "RGB(40,84,147)";
            divChangeStatusNextYear.Style["background-color"] = "RGB(40,84,147)";
            divChangeStatusAnnul.Style["background-color"] = "RGB(40,84,147)";
        }
        if (claimStatusId == 6 || claimStatusId == 8) // если в резерве, то можно аннулировать, либо зачислить
        {
            lnkChangeStatusEnroll.Enabled = true;
            lnkChangeStatusAnnul.Enabled = true;
            lnkChangeStatusNextYear.Enabled = true;
            lnkChangeStatusEnrollDiv.Style["background-color"] = "RGB(40,84,147)";
            divChangeStatusNextYear.Style["background-color"] = "RGB(40,84,147)";
            divChangeStatusAnnul.Style["background-color"] = "RGB(40,84,147)";
        }
        if (claimStatusId == 5) // если аннулировано, то ничего нельзя
        {
        }
        if (claimStatusId == 4) // если зачислен то ничего нельзя
        {
        }
    }

    protected void lnkTypeBudgetOk_OnClick(object sender, EventArgs e)
    {
        
        DictTypeBudgetId = Convert.ToInt32(ddlGroups.SelectedValue);
        int statusId = Convert.ToInt32(ddlGroups.Attributes["statusId"]);
        long claimId = Convert.ToInt64(ddlGroups.Attributes["claimId"]);
        int numYear = Convert.ToInt32(ddlGroups.Attributes["NumYear"]);
        Guid PupilId = new Guid(ddlGroups.Attributes["pupilId"]);
        DateTime pupilBirthday = Convert.ToDateTime(ddlGroups.Attributes["pupilBirthday"]);
        long childUnionId = Convert.ToInt64(ddlGroups.Attributes["childUnionId"]);
        bool isEnd = false;
        using (AgeGroupDb db = new AgeGroupDb())
        {
            var group = db.GetGroup(Convert.ToInt64(ddlGroups.SelectedValue));
            //if (DictTypeBudgetId == 2)
            {
                // проверка на 20 часов и на 3 УДО
                /*ExtendedLimit limit;
                using (PupilsDb db = new PupilsDb())
                {
                    limit = db.GetLimit(PupilId, null);
                }
                */
                DateTime now = DateTime.Today;
                int fullYears = now.Year - pupilBirthday.Year;
                if (pupilBirthday > now.AddYears(-fullYears)) fullYears--;


                /*var list = db.GetAgeGroups(null, null, null, childUnionId);
                int minYear = list.Select(i => i.Age).Min();
                int maxYear = list.Select(i => i.Age).Max();
                if (fullYears < minYear) fullYears = minYear;
                if (fullYears > maxYear) fullYears = maxYear;
                ExtendedAgeGroup item = list.Where(i => i.Age == fullYears).FirstOrDefault();*/
                /*
                ExtendedClaim claim = new ExtendedClaim();
                using (ClaimDb db2 = new ClaimDb())
                {
                    claim = db2.GetClaim(claimId);
                }*/
                /*ExtendedAgeGroup item = db.GetGroup(Convert.ToInt64(ddlGroups.SelectedValue));

                if ((limit.Hours + ((double)(item.LessonLength * item.LessonsInWeek) / 60)) > limit.NormHours)
                {
                    //bool isPupil = claim.Pupil.UserId == claim.Parent.UserId;
                    StringBuilder limitText = new StringBuilder();
                    //limitText.Append(isPupil ? claim.Parent.LastName : claim.Pupil.LastName);
                    //limitText.Append(" " + (isPupil ? claim.Parent.FirstName : claim.Pupil.FirstName));
                    //limitText.Append(" " + (isPupil ? claim.Parent.MiddleName : claim.Pupil.MiddleName));
                    limitText.Append("Данный обучающийся уже посещает ");
                    using (PupilsDb db2 = new PupilsDb())
                    {
                        List<ExtendedChildUnion> l = db2.GetChildUnion(PupilId);
                        var previousUdodName = l.Count() > 0 ? l[0].UdodShortName : "";
                        var previousUdodPhone = l.Count() > 0 ? l[0].UdodPhone : "";
                        foreach (ExtendedChildUnion cu in l)
                        {
                            if (previousUdodName != cu.UdodShortName)
                            {
                                limitText.Append(" в УДО " + previousUdodName + " (тел: " + previousUdodPhone + "); ");
                            }
                            limitText.Append(cu.Name + ", ");
                            previousUdodName = cu.UdodShortName;
                            previousUdodPhone = cu.UdodPhone;
                        }
                        limitText.Remove(limitText.Length - 2, 2);
                        limitText.Append(" в УДО " + previousUdodName + " (тел: " + previousUdodPhone + ") ");
                    }
                    limitText.Append(" с суммарным количеством часов занятий ");
                    limitText.Append(limit.Hours.ToString("F2") + " часов в неделю. ");
                    limitText.Append("Для детей его/её возраста действует ограничение СанПиН в ");
                    limitText.Append(limit.NormHours.ToString("F2") + " часов занятий в неделю. ");
                    limitText.Append("В связи с чем осуществить зачисление в ДО ");
                    limitText.Append(item.ChildUnionName + " с продолжительностью занятий ");
                    limitText.Append((item.LessonLength * item.LessonsInWeek / 60.0).ToString("F2") + " часов в неделю невозможно, ");
                    limitText.Append("так как это приводит к превышению установленных норм.");

                    lblLimit.Text = limitText.ToString();
                    upLimit.Update();
                    popupTypeBudgetExtender.Hide();
                    popupLimitExtender.Show();
                    return;
                }*/

                if (!(fullYears >= group.AgeStart && fullYears <= group.AgeEnd))
                {
                    errorReestr.ShowError("Ошибка", "Возраст ребенка не подходит к данной группе", "", "ОК", false, true);
                    return;
                }
                if (group.NumYears.Count(i => i == numYear) == 0)
                {
                    errorReestr.ShowError("Ошибка", "Год обучения ребенка не подходит к данной группе", "", "ОК", false,
                                          true);
                    return;
                }
                isEnd = group.MaxCountPupil - (group.CurrentCountPupil + 1) <= 0;

            }

            UpdateStatus(claimId, statusId, null, Convert.ToInt64(ddlGroups.SelectedValue),
                         isEnd ? Convert.ToInt64(ddlGroups.SelectedValue) : (long?) null, group.StartClaim, group.EndClaim);
        }
        popupTypeBudgetExtender.Hide();
    }

    protected void lnkSchoolError_OnClick(object sender, EventArgs e)
    {
        Page.Response.Redirect(SiteUtility.GetUrl("~/Udod/ChildUnionList.aspx"));
    }

    protected void lnkLoadFromReestr_OnClick(object sender, EventArgs e)
    {
        //lblErrorLoad.Text = "";
        //lblErrorReestrAddress.Text = "";
        //string mappath = "";
        //if (rbnIsPupil.SelectedIndex == 0)
        //    mappath =
        //        Page.MapPath(PupilEdit.PassportType == 2
        //                         ? "../Templates/search_Passport_by_Series_and_Number_rq.xml"
        //                         : "../Templates/search_by_Series_and_Number_rq.xml");
        //else
        //{
        //    mappath =
        //        Page.MapPath(parent.PassportType == 2
        //                         ? "../Templates/search_Passport_by_Series_and_Number_rq.xml"
        //                         : "../Templates/search_by_Series_and_Number_rq.xml");
        //}
        //ExtendedPupil pupil = null;
        //PupilEdit.ReestrGuid = "";
        //try
        //{
        //    //pupil = SiteUtility.GetPupilData(mappath, rbnIsPupil.SelectedIndex == 0 ? PupilEdit.Series : parent.Series,
        //    //rbnIsPupil.SelectedIndex == 0 ? PupilEdit.Number : parent.Number).FirstOrDefault(i => i.LastName.Trim().ToUpper() == PupilEdit.LastName.Trim().ToUpper());
        //    var pupilstmp = SiteUtility.GetPupilData(mappath,
        //                                             rbnIsPupil.SelectedIndex == 0 ? PupilEdit.Series : parent.Series,
        //                                             rbnIsPupil.SelectedIndex == 0 ? PupilEdit.Number : parent.Number);
        //    if (UserContext.Roles.IsAdministrator && pupilstmp.Count>0)
        //    {
        //        Label7.Text = "";
        //        foreach (var extendedPupil in pupilstmp)
        //        {
        //            Label7.Text = Label7.Text + " " + extendedPupil.LastName + " " + extendedPupil.FirstName + " " +
        //                          extendedPupil.MiddleName + " " +
        //                          extendedPupil.Birthday.ToShortDateString() + " " +
        //                          (extendedPupil.Passport.IssueDate.HasValue
        //                              ? extendedPupil.Passport.IssueDate.Value.ToShortDateString()
        //                              : DateTime.Now.ToShortDateString()) + " " +
        //                                extendedPupil.SchoolName + " " + extendedPupil.ClassName + " " +
        //                                extendedPupil.AddressStr+"; ";
        //        }
        //        UpdatePanel3.Update();
        //        popupSchoolErrorExtender.Show();
    
        //    }
            
        //    if (pupilstmp.Count == 1) pupil = pupilstmp.FirstOrDefault();
        //    else
        //    {
        //        if (pupilstmp.Count(i=>i.LastName.Trim().ToUpper() == PupilEdit.LastName.Trim().ToUpper())!=0)
        //        {
        //            pupil =
        //                pupilstmp.FirstOrDefault(
        //                    i => i.LastName.Trim().ToUpper() == PupilEdit.LastName.Trim().ToUpper());
        //        }
        //    }


        //    PupilReestrCode = Convert.ToInt64(pupil.ReestrCode);
        //    if (PupilReestrCode.HasValue && PupilReestrCode.Value == 0) PupilReestrCode = null;
        //    PupilEdit.ReestrGuid = pupil.ReestrGuid.HasValue?pupil.ReestrGuid.Value.ToString():"";

        //}
        //catch (Exception)
        //{
        //    pupil=new ExtendedPupil();
        //}
        //if (string.IsNullOrEmpty(pupil.ReestrCode))
        //{
        //    errorReestr.ShowError("Внимание", "По введённым параметрам в Реестре не найдено ни одной записи. Проверьте правильность заполнения и повторите запрос. Если Вы уверены в корректности введённых данных, то можете создать запись для данного обучающегося вручную. Создать запись вручную?", "Да", "Нет", true, true);
        //    PupilReestrCode = null;
        //}
        //else
            
        //    if ((
        //        (rbnIsPupil.SelectedIndex == 0) && (PupilEdit.LastName.Trim().ToLower() != pupil.LastName.Trim().ToLower()
        //    || PupilEdit.FirstName.Trim().ToLower() != pupil.FirstName.Trim().ToLower())) ||
        //    (rbnIsPupil.SelectedIndex != 0 && (parent.LastName.Trim().ToLower() != pupil.LastName.Trim().ToLower()
        //    || parent.FirstName.Trim().ToLower() != pupil.FirstName.Trim().ToLower())))
        //    {
        //        // Ошибка
        //        lblErrorLoad.Text = "Полученные из Реестра данные отличаются от введённых. Проверьте правильность заполнения и повторите запрос. Если Вы уверены в корректности введённых данных - обратитесь в ОСИП для разрешения ситуации по данному обучающемуся.";
        //    }
        //    else
        //    {
        //        if (!UserContext.Roles.IsAdministrator)
        //        {
        //            IsReestr = true;
        //            PupilEdit.IsReestr = true;
        //        }
        //        if (rbnIsPupil.SelectedIndex != 0)
        //        {
        //            if (!UserContext.Roles.IsAdministrator) parent.IsReestr = true;
        //            parent.MiddleName = pupil.MiddleName;
        //            parent.IssueDate = pupil.Passport.IssueDate;
        //        }
        //        else
        //        {
        //            PupilEdit.MiddleName = pupil.MiddleName;
        //        }
        //        //btnSavePupil.Visible = true;
        //        //PupilFio.LastName = pupil.LastName;
        //        //PupilFio.FirstName = pupil.FirstName;

        //        PupilEdit.BirthDay = pupil.Birthday;
        //        //tbPupilPhoneNumber.Text = pupil.PhoneNumber;
        //        //tbPupilEmail.Text = pupil.EMail;
        //        PupilEdit.ClassName = pupil.ClassName;
        //        PupilEdit.Sex = pupil.Sex;
        //        //cbSex.SelectedIndex = cbSex.Items.IndexOf(cbSex.Items.FindByValue(pupil.Sex.ToString()));
        //        lblReestrAddress.Text = pupil.AddressStr;

        //        string _url = Page.MapPath("../Templates/getUdodInfoByGuid.xml");
        //        bool isReadySchool = true;
        //        try
        //        {
        //            using (DictSchoolDb db = new DictSchoolDb())
        //            {
        //                var tmp = db.GetSchool(pupil.SchoolName);
        //                pupil.SchoolName = tmp.Name;
        //            }
        //        }
        //        catch (Exception)
        //        {
        //            isReadySchool = false;
        //        }

        //        if (!isReadySchool)
        //        {
        //            ExtendedUdod reestrData;
        //            reestrData = SiteUtility.GetUdodData(_url, pupil.SchoolName);
        //            pupil.SchoolName = reestrData.ShortName;

        //        }
        //        PupilEdit.CodeEkis = pupil.SchoolCode;
        //        PupilEdit.SchoolName = pupil.SchoolName;
        //        if (pupil.Passport.IssueDate.Value.Date == DateTime.Now.Date)
        //            lblErrorLoad.Text = "Полученные из реестра данные по дате выдачи документа являются пустыми. В дату выдачи документа ставится текущая дата.";
        //        PupilEdit.IssueDate = pupil.Passport.IssueDate;

        //        if (pupil.Addresses[0].BtiCode != null || !string.IsNullOrEmpty(pupil.Addresses[0].KladrCode))
        //        {
        //            var regAddress = pupil.Addresses[0];
        //            RegAddress.IsBti = string.IsNullOrEmpty(regAddress.KladrCode);

        //            if (RegAddress.IsBti && regAddress.BtiCode.HasValue)
        //                regAddress.BtiCode = (new KladrDb()).GetStreetCodeByHouseCode(regAddress.BtiCode.Value);

        //            if (!(!regAddress.BtiCode.HasValue && !string.IsNullOrEmpty(regAddress.KladrCode) && regAddress.KladrCode.Substring(0, 2) == "77"))
        //            {
        //                RegAddress.Code = string.IsNullOrEmpty(regAddress.KladrCode)
        //                                      ? regAddress.BtiCode.Value.ToString()
        //                                      : regAddress.KladrCode;
        //            }
        //            else
        //            {
        //                lblErrorReestrAddress.Text = " Загруженная информация об адресе не может быть автоматически распознана. В связи с этим поля адреса необходимо заполнить вручную.";
        //            }
        //            RegAddress.Databind();
        //            RegAddress.Index = regAddress.PostIndex;
        //            RegAddress.Housing = regAddress.Housing;
        //            RegAddress.NumberHouse = regAddress.HouseNumber;
        //            RegAddress.Fraction = regAddress.Fraction;
        //            RegAddress.Flat = regAddress.FlatNumber;
        //            RegAddress.Building = regAddress.Building;
        //        }
                

        //    }
        ////upSavePupil.Update();
        //upPupilEdit.Update();
        //upParent.Update();
        //upAddresses.Update();
    }

    protected void errorReestr_OnErrorOkClick(object sender, EventArgs e)
    {
        if (!UserContext.Roles.IsAdministrator)
        {
            IsReestr = true;

            PupilEdit.IsReestr = false;
        }
        if (rbnIsPupil.SelectedIndex != 0)
        {
            if (!UserContext.Roles.IsAdministrator) parent.IsReestr = false;
        }
        errorReestr.HideError();
        upPupilEdit.Update();
        upParent.Update();
    }

    protected void lnkAnnulConfirm_OnClick(object sender, EventArgs e)
    {
        int statusId = Convert.ToInt32(rbAnnul.Attributes["statusId"]);
        long claimId = Convert.ToInt64(rbAnnul.Attributes["claimId"]);
        Guid PupilId = new Guid(rbAnnul.Attributes["pupilId"]);
        DateTime pupilBirthday = Convert.ToDateTime(rbAnnul.Attributes["pupilBirthday"]);
        long childUnionId = Convert.ToInt64(rbAnnul.Attributes["childUnionId"]);

        UpdateStatus(claimId, statusId, Convert.ToInt32(rbAnnul.SelectedValue), null, null, null, null);
        modalPopupAnnul.Hide();
    }

    protected void ddlGroups_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlGroups.SelectedValue))
        {
            using (AgeGroupDb db = new AgeGroupDb())
            {
                var group = db.GetGroup(Convert.ToInt64(ddlGroups.SelectedValue));
                StringBuilder sb = new StringBuilder();
                foreach (var teacher in group.Teachers)
                {
                    sb.Append(teacher.Fio);
                    sb.Append(" ");
                }
                string teachers = sb.ToString();
                sb = new StringBuilder();
                foreach (var year in group.NumYears)
                {
                    sb.Append(year);
                    sb.Append(" ");
                }
                pGroupinfo.Visible = true;
                lblGroupInfo.Text = string.Format("Возраст: от {1} до {2}, Педагоги: {3}, Года обучения: {4}",
                                                  group.Name, group.AgeStart, group.AgeEnd, teachers, sb.ToString());
            }
        }
    }

    protected void lnkzach_OnClick(object sender, EventArgs e)
    {
        
        
    }

    protected void lnkzachnextyear_OnClick(object sender, EventArgs e)
    {
        
    }

    protected void lnkChangeStatusEnroll_OnClick(object sender, EventArgs e)
    {
        using (ClaimDb db = new ClaimDb())
        {
            var claim = db.GetClaim(UserContext.ClaimId.Value);
            if (DateTime.Now < claim.BeginDate)
            {
                ModalPopupStatus1.Show();
            }
            else
            {
                changeStatus_Click(sender, e);
            }
        }
    }
}
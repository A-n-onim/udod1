﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Udod.Dal;
using System.Net.Mail;
using System.Configuration;
using Udod.Dal.Enum;

public partial class Claim_Claim : BasePage
{
    private long? basedOnClaimNumber
    {
        get
        {
            if (ViewState["basedOnClaimNumber"] == null) return null;
            return (long)ViewState["basedOnClaimNumber"];
        }
        set
        {
            ViewState["basedOnClaimNumber"] = value;
        }
    }

    private bool enrollRightNow
    {
        get
        {
            if (ViewState["enrollRightNow"] == null) return false;
            return (bool)ViewState["enrollRightNow"];
        }
        set
        {
            ViewState["enrollRightNow"] = value;
        }
    }

    private long? currentClaimNumber
    {
        get
        {
            return (long?)ViewState["currentClaimNumber"];
        }
        set
        {
            ViewState["currentClaimNumber"] = value;
        }
    }

    //protected ExtendedAddress? RegAddress { get { return (ExtendedAddress?)ViewState["RegAddress"]; } set { ViewState["RegAddress"] = value; } }
    //protected ExtendedAddress? FactAddress { get { return (ExtendedAddress?)ViewState["FactAddress"]; } set { ViewState["FactAddress"] = value; } }
    bool edit {set { ViewState["edit"] = value; } get { return (bool) ViewState["edit"]; }}
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator);
        if (!Page.IsPostBack)
        {
            edit = true;
            lnkCreateClaimFromCurrent.Visible = lnkChangeStatusDocs.Visible = lnkChangeStatusEnroll.Visible = lnkChangeStatusAnnul.Visible = lnkChangeStatusReserv.Visible =
                (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee);
            
            if (UserContext.ClaimId.HasValue)
                LoadClaim();
            else
            {
                lnkPrint.Visible = false;
                RegAddress.Databind();
                //FactAddress.Databind();
                if (UserContext.Roles.RoleId==(int)EnumRoles.UdodEmployee)
                {
                    lnkCreateClaimFromCurrent.Visible = lnkChangeStatusDocs.Visible = lnkChangeStatusEnroll.Visible = lnkChangeStatusAnnul.Visible = lnkChangeStatusReserv.Visible = false;
                    //Page.Response.Redirect(SiteUtility.GetUrl("~/Udod/ClaimList.aspx"));
                }
            }
            //if (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee)
            //    edit = false;

            // сравниваем isNewClaimFromOld только если редактирование, а не новая заявка
            /*if (UserContext.ClaimId.HasValue)
            {
                // если создаем заявку на основе предыдущей то кнопка сохранить доступна
                if ((bool)Session["isNewClaimFromOld"])
                {
                    edit = true;
                }
            }*/
            //rbnIsPupil.Items.Clear();
            //rbnIsPupil.Items.Add(new ListItem { Value = "False", Text = "Заявитель - родитель/законный представитьель" });
            //rbnIsPupil.Items.Add(new ListItem { Value = "True", Text = "Заявитель - будующий обучающийся (старше 14 лет)" });
            /*parent.Enabled = PupilEdit.Enabled =
                rbnFacRegAddress.Enabled = 
                RegAddress.Enabled =
                FactAddress.Enabled =
                lnkSelectUdod.Visible =
                tbBeginDate.Enabled= 
                ddlYearOfStudy.Enabled = 
                lnkSave.Visible=
                lnkPrint.Visible = 
                rbnIsPupil.Enabled = edit;*/

            

            valrDate.MinimumValue = DateTime.Today.ToString().Substring(0,10);

            if (Session["SelectAgeGroupUdodId"] != null)
            {
                selectUdod.UdodAgeGroupId = Convert.ToInt64(Session["SelectAgeGroupUdodId"]);
                btnSelectUdod_Click(selectUdod, new EventArgs());
                Session["SelectAgeGroupUdodId"] = null;
            }

            // сравниваем isNewClaimFromOld только если редактирование, а не новая заявка
            if (UserContext.ClaimId.HasValue)
            {
                // если создаем заявку на основе предыдущей то кнопка сохранить доступна
                if ((bool)Session["isNewClaimFromOld"])
                {
                    edit = true;
                    lnkPrint.Visible = false;
                    rowStatus.Visible = false;
                    lnkSave.Enabled = true;
                    lnkCreateClaimFromCurrent.Visible = lnkChangeStatusDocs.Visible = lnkChangeStatusEnroll.Visible = lnkChangeStatusAnnul.Visible = lnkChangeStatusReserv.Visible = false;
                    using (ClaimDb db = new ClaimDb())
                    {
                        basedOnClaimNumber = db.GetClaim(UserContext.ClaimId.Value).Number;
                    }
                    UserContext.ClaimId = null;
                    Session["isNewClaimFromOld"] = null;
                }
            }

            parent.Enabled = PupilEdit.Enabled =
                rbnFacRegAddress.Enabled =
                RegAddress.Enabled =
                //FactAddress.Enabled =
                lnkSelectUdod.Visible =
                tbBeginDate.Enabled =
                ddlYearOfStudy.Enabled =
                lnkSave.Visible =
                //lnkPrint.Visible =
                rbnIsPupil.Enabled = edit;

            if (UserContext.ClaimId.HasValue)
                rbnIsPupil.Enabled = false;

            PupilEdit.Enable14YearsValidation = Convert.ToBoolean(rbnIsPupil.SelectedValue);
            if (Convert.ToBoolean(rbnIsPupil.SelectedValue))
            {
                parent.RelativeEnabled = !Convert.ToBoolean(rbnIsPupil.SelectedValue);
                PupilEdit.FioEnabled = !Convert.ToBoolean(rbnIsPupil.SelectedValue);
                PupilEdit.PassportEnabled = !Convert.ToBoolean(rbnIsPupil.SelectedValue);
                PupilEdit.LastName = parent.LastName;
                PupilEdit.FirstName = parent.FirstName;
                PupilEdit.MiddleName = parent.MiddleName;
                upIsPupil.Update();
            }

            if (Session["needLimitAlert"] != null)
            {
                lblLimit.Text = Session["needLimitAlert"].ToString();
                upLimit.Update();
                popupLimitExtender.Show();
                Session["needLimitAlert"] = null;
            }
        }
    }

    private void LoadClaim()
    {
        ExtendedClaim claim;
        using (ClaimDb db = new ClaimDb())
        {
            claim = db.GetClaim(UserContext.ClaimId.Value);
            currentClaimNumber = claim.Number;
            // редактирование или нет
            if (UserContext.Roles.RoleId == (int)EnumRoles.Osip )
            {
                if (claim.ClaimStatus.ClaimStatusId == 1)
                    edit = true;
                else
                    edit = false;
            }
            if (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee )
            {
                if (claim.ClaimStatus.ClaimStatusId!=4 && claim.ClaimStatus.ClaimStatusId!=5)
                    edit = true;
                else
                    edit = false;
            }
            if (UserContext.Roles.RoleId == (int)EnumRoles.Administrator) edit = true;
            lnkCreateClaimFromCurrent.Visible = (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee) && ((claim.ClaimStatus.ClaimStatusId == 7) || (claim.ClaimStatus.ClaimStatusId == 6));
            PupilEdit.LastName = claim.Pupil.LastName;
            PupilEdit.FirstName = claim.Pupil.FirstName;
            PupilEdit.MiddleName = claim.Pupil.MiddleName;
            PupilEdit.BirthDay = claim.Pupil.Birthday;
            PupilEdit.Sex = claim.Pupil.Sex;
            PupilEdit.SchoolName = claim.Pupil.SchoolName;
            PupilEdit.ClassName = claim.Pupil.ClassName;
            parent.PassportType = claim.Parent.Passport.PassportType.PassportTypeId;
            parent.Series = claim.Parent.Passport.Series;
            parent.Number = claim.Parent.Passport.Number;
            parent.IssueDate = claim.Parent.Passport.IssueDate;

            parent.LastName = claim.Parent.LastName;
            parent.FirstName = claim.Parent.FirstName;
            parent.MiddleName = claim.Parent.MiddleName;
            parent.EMail = claim.Parent.EMail;
            parent.PhoneNumber = claim.Parent.PhoneNumber;
            parent.ExpressPhoneNumber = claim.Parent.ExpressPhoneNumber;

            PupilEdit.PassportType = claim.Pupil.Passport.PassportType.PassportTypeId;
            PupilEdit.Series = claim.Pupil.Passport.Series;
            PupilEdit.Number = claim.Pupil.Passport.Number;
            PupilEdit.IssueDate = claim.Pupil.Passport.IssueDate;

            if (claim.Parent.UserType != 0)
                parent.Relativies = claim.Parent.UserType;
            else
                parent.Relativies = 1;
            /*ExtendedAddress fAddr = new ExtendedAddress();
            fAddr.KladrCode = FactAddress.Code;
            fAddr.Housing = FactAddress.Housing;
            fAddr.HouseNumber = FactAddress.NumberHouse;
            fAddr.Fraction = FactAddress.Fraction;
            fAddr.FlatNumber = FactAddress.Flat;
            fAddr.Building = FactAddress.Building;

            ExtendedAddress rAddr = new ExtendedAddress();
            rAddr.KladrCode = RegAddress.Code;
            rAddr.Housing = RegAddress.Housing;
            rAddr.HouseNumber = RegAddress.NumberHouse;
            rAddr.Fraction = RegAddress.Fraction;
            rAddr.FlatNumber = RegAddress.Flat;
            rAddr.Building = RegAddress.Building;*/

            rbnIsPupil.SelectedIndex = rbnIsPupil.Items.IndexOf(rbnIsPupil.Items.FindByValue(Convert.ToString(claim.Parent.UserId == claim.Pupil.UserId)));
            

            tbBeginDate.Text = claim.BeginDate.ToShortDateString();
            lblCreatedDate.Text = claim.CreatedDate.ToShortDateString();
            lblNumber.Text = claim.Number.ToString();
            if (claim.ClaimStatus.ClaimStatusId == 1)
            {
                if ((claim.CreatedDate.AddDays(7) - DateTime.Now).Days>0)
                    lblCountDays.Text = (claim.CreatedDate.AddDays(7) - DateTime.Now).Days.ToString();
            }
            else
            {
                lblCountDays.Visible = false;
            }
            lblChildUnion.Text = claim.ChildUnion.Name;
            //lblUdod.Text = claim.AgeGroup.Section.Name;
            lnkSelectUdod.Text = "<div class='btnBlueLong'>Изменить ДО</div>";
            lnkDetail.Visible = true;
            selectUdod.UdodAgeGroupId = claim.ChildUnion.ChildUnionId;
            selectUdod.UdodSectionName = claim.ChildUnion.Name;

            StatusButtonsEnabling(claim.ClaimStatus.ClaimStatusId);
        }

        lblProgram.Text = claim.ChildUnion.Program.Name;
        lblSection.Text = claim.ChildUnion.Section.Name;
        //lblTeacherFIO.Text = claim.Teacher.Fio;
        lblAge.Text = "от " + claim.ChildUnion.AgeStart.ToString() + " до " + claim.ChildUnion.AgeEnd.ToString();
        using (KladrDb db = new KladrDb())
        {
            var Addresses = db.GetAddressesById(null, null, selectUdod.UdodAgeGroupId, null);
            if (Addresses.Count() > 0) lblAddress.Text = Addresses.FirstOrDefault().AddressStr;
        }
        ddlYearOfStudy.Items.Clear();
        for (int i = 1; i <= claim.ChildUnion.NumYears; i++)
        {
            ddlYearOfStudy.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        if (claim.ChildUnion.NumYears >= claim.NumYear)
            ddlYearOfStudy.SelectedIndex = claim.NumYear - 1;
        else ddlYearOfStudy.SelectedIndex = -1;
        //lblBudget.Text = claim.AgeGroup.TypeBudget.TypeBudgetName;
        //lblNumYears.Text = claim.AgeGroup.NumYears.ToString();

        rowStatus.Visible = true;
        //ddlClaimStatus.DataBind();
        //ddlClaimStatus.SelectedIndex = ddlClaimStatus.Items.IndexOf(ddlClaimStatus.Items.FindByValue(claim.ClaimStatus.ClaimStatusId.ToString()));
        lblStatus.Text = claim.ClaimStatus.ClaimStatusName;
        using (KladrDb db = new KladrDb())
        {
            List<ExtendedAddress> addresses = db.GetAddressesById(claim.Pupil.UserId, null, null, null);
            /*var factAddress = addresses.Where(i => i.AddressType.AddressTypeId == 6).FirstOrDefault();
            FactAddress.IsBti = string.IsNullOrEmpty(factAddress.KladrCode);
            FactAddress.Code = string.IsNullOrEmpty(factAddress.KladrCode) ? factAddress.BtiCode.Value.ToString() : factAddress.KladrCode;
            FactAddress.Databind();
            FactAddress.Index = factAddress.PostIndex;
            FactAddress.Housing = factAddress.Housing;
            FactAddress.NumberHouse = factAddress.HouseNumber;
            FactAddress.Fraction = factAddress.Fraction;
            FactAddress.Flat = factAddress.FlatNumber;
            FactAddress.Building = factAddress.Building;
            //lblFactAddress.Text = FactAddress.HasValue? FactAddress.Value.AddressStr:"";
            /*Address.Code = address.KladrCode;
            Address.Housing = address.Housing;
            Address.NumberHouse = address.HouseNumber;
            Address.Fraction = address.Fraction;
            Address.Flat = address.FlatNumber;
            Address.Building = address.Building;
            */
            
            var regAddress = addresses.Where(i => i.AddressType.AddressTypeId == 5).FirstOrDefault();
            RegAddress.IsBti = string.IsNullOrEmpty(regAddress.KladrCode);
            RegAddress.Code = string.IsNullOrEmpty(regAddress.KladrCode) ? regAddress.BtiCode.Value.ToString() : regAddress.KladrCode;
            RegAddress.Databind();
            RegAddress.Index = regAddress.PostIndex;
            RegAddress.Housing = regAddress.Housing;
            RegAddress.NumberHouse = regAddress.HouseNumber;
            RegAddress.Fraction = regAddress.Fraction;
            RegAddress.Flat = regAddress.FlatNumber;
            RegAddress.Building = regAddress.Building;
            //lblRegAddress.Text = RegAddress.HasValue? RegAddress.Value.AddressStr:"";
            /*RegAddress.Code = regaddress.KladrCode;
            RegAddress.Housing = regaddress.Housing;
            RegAddress.NumberHouse = regaddress.HouseNumber;
            RegAddress.Fraction = regaddress.Fraction;
            RegAddress.Flat = regaddress.FlatNumber;
            RegAddress.Building = regaddress.Building;
            */
            upAddresses.Update();
            var adrr = db.GetAddressesById(null, null, null, claim.AgeGroup.UdodAgeGroupId);
            if (adrr.Count() > 0) lblAddress.Text = adrr.FirstOrDefault().AddressStr;

            /*bool isEqual = true;
            if (FactAddress.Code != RegAddress.Code) isEqual = false;
            if (FactAddress.Index != RegAddress.Index) isEqual = false;
            if (FactAddress.Housing != RegAddress.Housing) isEqual = false;
            if (FactAddress.NumberHouse != RegAddress.NumberHouse) isEqual = false;
            if (FactAddress.Fraction != RegAddress.Fraction) isEqual = false;
            if (FactAddress.Flat != RegAddress.Flat) isEqual = false;
            if (FactAddress.Building != RegAddress.Building) isEqual = false;

            rbnFacRegAddress.SelectedIndex = rbnFacRegAddress.Items.IndexOf(rbnFacRegAddress.Items.FindByValue(isEqual.ToString()));
            FactAddressPanel.Enabled = FactAddress.Enabled = !(rbnFacRegAddress.SelectedValue == "True");*/
        }

        if (claim.ChildUnion.isDeleted)
        {
            lnkDetail.Visible = false;
            lblChildUnionDeleted.Visible = true;
            lnkChangeStatusEnroll.Visible = false;
        }
        else
        {
            lblChildUnionDeleted.Visible = false;
        }
        //UserContext.ClaimId = null;
        /*if ((bool)Session["isNewClaimFromOld"])
        {
            if (claim.ClaimStatus.ClaimStatusId == 5) edit = false;
            //UserContext.ClaimId = null;
            //Session["isNewClaimFromOld"] = null;
        }*/
    }

    protected void lnkPrint_Click(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("Claim.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                                string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
        workbook.Open(MapPath("../Templates/ClaimTemplate.xlsx"));
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];
        if (selectUdod.UdodAgeGroupId.HasValue)
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                sheet.Cells[0, 0].PutValue("Заявление №" + currentClaimNumber);
                var childunion = db.GetChildUnion(selectUdod.UdodAgeGroupId, null);
                using (UdodDb db2 = new UdodDb())
                {
                    ExtendedUdod udod = db2.GetUdod(childunion.UdodId);
                    sheet.Cells[0, 2].PutValue("Директору ГБОУ " + udod.FioDirector);
                }

                sheet.Cells[1, 2].PutValue("От " + parent.LastName + " " + parent.FirstName + " " + parent.MiddleName);
                sheet.Cells[3, 2].PutValue("Проживающий (ая) по адресу: " + RegAddress.AddressStr);
                sheet.Cells[4, 2].PutValue("Контактный телефон: " + parent.PhoneNumber);
                sheet.Cells[6, 2].PutValue("Электронная почта: " + parent.EMail);
                sheet.Cells[8, 0].PutValue("Прошу принять моего сына (дочь) " + PupilEdit.LastName + " " +
                                           PupilEdit.FirstName + " " + PupilEdit.MiddleName);
                sheet.Cells[10, 0].PutValue("в коллектив (объединение): " + childunion.Name);
                sheet.Cells[13, 0].PutValue("Дата рождения " + PupilEdit.BirthDay.ToShortDateString());
                sheet.Cells[13, 1].PutValue("№ школы (Д/сада) " + PupilEdit.SchoolName);
                sheet.Cells[13, 3].PutValue("класс (группа) " + PupilEdit.ClassName);
            }
        }
        
        //sheet.Cells.CreateRange(0, 0, row, 11).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
        workbook.Save("Claim.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }

    protected bool SaveClaim(bool showSaveClaimPopup)
    {
        /*if (!Page.IsValid)
        {
            upAddresses.Update();
            return false;
        }
        */
        try
        {
            if (selectUdod.UdodAgeGroupId.HasValue)
            {
                ExtendedClaim claim = new ExtendedClaim();

                if (UserContext.ClaimId.HasValue)
                {
                    using (ClaimDb db = new ClaimDb())
                    {
                        claim = db.GetClaim(UserContext.ClaimId.Value);
                    }
                }
                //bool prevIsPupil = claim.Parent.UserId == claim.Pupil.UserId;

                Guid PupilId = Guid.Empty;
                Guid ParentId = Guid.Empty;
                bool isPupil = Convert.ToBoolean(rbnIsPupil.SelectedValue);

                if (isPupil == false)
                {
                    isPupil = true;
                    if ((parent.FirstName != PupilEdit.FirstName) ||
                        (parent.LastName != PupilEdit.LastName) ||
                        (parent.MiddleName != PupilEdit.MiddleName) ||
                        (parent.PassportType != PupilEdit.PassportType) ||
                        (parent.Series != PupilEdit.Series) ||
                        (parent.Number != PupilEdit.Number) ||
                        (parent.IssueDate != PupilEdit.IssueDate)) isPupil = false;
                }
                ExtendedPupil oldPupil = new ExtendedPupil();
                using (PupilsDb db1 = new PupilsDb())
                {
                    if (claim.Pupil != null)
                        oldPupil = db1.GetPupil(claim.Pupil.UserId);
                }
                using (UserDb db = new UserDb())
                {
                    lblPassportDataError.Visible = false;
                    if (claim.Parent != null)
                    {
                        db.SaveProfile(claim.Parent.UserId, claim.Parent.UserName, parent.LastName, parent.FirstName,
                            parent.MiddleName, DateTime.Now, parent.EMail, parent.PhoneNumber, parent.ExpressPhoneNumber, "", "");
                        ParentId = claim.Parent.UserId;
                    }
                    else if (!db.CheckExistsingPassport(parent.LastName, parent.FirstName, parent.PassportType, parent.Series, parent.Number, null))
                    {
                        ParentId = db.CreateParent(parent.LastName,
                                                        parent.FirstName,
                                                        parent.MiddleName, DateTime.Now, false,
                                                        parent.PhoneNumber, parent.EMail, isPupil, parent.PassportType,
                                                        parent.Series, parent.Number, parent.IssueDate.Value, parent.Relativies, parent.ExpressPhoneNumber);
                        lblPassportDataError.Visible = false;
                    }
                    else
                    {
                        /*StringBuilder sb = new StringBuilder();
                        List<ExtendedUdodPassportMessage> list = db.FindUdodByPassport(parent.PassportType, parent.Series, parent.Number);
                        sb.Append("Введённые Вами данные документа удостоверяющего личность уже были использованы в заявлении(-ях)");
                        foreach (var item in list)
                        {
                            sb.Append(" №");
                            foreach (var n in item.Claims)
                            {
                                sb.Append(n.ToString() + ", ");
                            }
                            sb.Remove(sb.Length - 2, 2);
                            
                            sb.Append(" поданного в учреждение " + item.ShortName + ", телефон " + item.PhoneNumber + ", ");
                        }
                        sb.Remove(sb.Length - 2, 2);
                        sb.Append(" Вам необходимо связаться с указанным(-ыми) УДО, определиться с чьей стороны допущена ошибка и внести необходимые коррективы.");

                        lblPassportDataError.Text = sb.ToString();
                        lblPassportDataError.Visible = true;
                        return false;*/
                    }
                    if (!UserContext.ClaimId.HasValue) // || ((isPupil != prevIsPupil) && (isPupil == false)))
                    {
                        if (!db.CheckExistsingPassport(isPupil ? parent.LastName : PupilEdit.LastName, isPupil ? parent.FirstName : PupilEdit.FirstName, isPupil ? parent.PassportType : PupilEdit.PassportType, isPupil ? parent.Series : PupilEdit.Series, isPupil ? parent.Number : PupilEdit.Number, null))
                        {
                            PupilId = db.CreatePupil(ParentId, isPupil ? parent.LastName : PupilEdit.LastName, isPupil ? parent.FirstName : PupilEdit.FirstName, isPupil ? parent.MiddleName : PupilEdit.MiddleName,
                                                 PupilEdit.BirthDay, Convert.ToBoolean(PupilEdit.Sex), isPupil, PupilEdit.SchoolName, "", PupilEdit.ClassName,
                                                 PupilEdit.PassportType, PupilEdit.Series, PupilEdit.Number, PupilEdit.IssueDate.Value,"","");
                            
                            if (!isPupil)
                                lblPupilPassportDataError.Visible = false;
                        }
                        else
                        {
                            /*StringBuilder sb = new StringBuilder();
                            List<ExtendedUdodPassportMessage> list = db.FindUdodByPassport(isPupil ? parent.PassportType : PupilEdit.PassportType, isPupil ? parent.Series : PupilEdit.Series, isPupil ? parent.Number : PupilEdit.Number);
                            sb.Append("Введённые Вами данные документа удостоверяющего личность уже были использованы в заявлении(-ях)");
                            foreach (var item in list)
                            {
                                sb.Append(" №");
                                foreach (var n in item.Claims)
                                {
                                    sb.Append(n.ToString() + ", ");
                                }
                                sb.Remove(sb.Length - 2, 2);

                                sb.Append(" поданного в учреждение " + item.ShortName + ", телефон " + item.PhoneNumber + ", ");
                            }
                            sb.Remove(sb.Length - 2, 2);
                            sb.Append(" Вам необходимо связаться с указанным(-ыми) УДО, определиться с чьей стороны допущена ошибка и внести необходимые коррективы.");

                            if (!isPupil)
                            {
                                lblPupilPassportDataError.Text = sb.ToString();
                                lblPupilPassportDataError.Visible = true;
                            }
                            return false;*/
                        }
                    }
                    else
                    {
                        db.SaveProfile(claim.Pupil.UserId, claim.Pupil.UserId.ToString(),
                                       isPupil ? parent.LastName : PupilEdit.LastName,
                                       isPupil ? parent.FirstName : PupilEdit.FirstName,
                                       isPupil ? parent.MiddleName : PupilEdit.MiddleName, PupilEdit.BirthDay,
                                       isPupil ? parent.EMail : "",
                                       isPupil ? parent.PhoneNumber : "",
                                       isPupil ? parent.ExpressPhoneNumber : "", PupilEdit.SchoolName, PupilEdit.ClassName);

                        PupilId = claim.Pupil.UserId;

                        // обновление пупила, парента и их паспортов
                        using (PupilsDb db1 = new PupilsDb())
                        {
                            ExtendedPupil pupil = db1.GetPupil(PupilId);
                            pupil.UserId = PupilId;
                            pupil.LastName = isPupil ? parent.LastName : PupilEdit.LastName;
                            pupil.FirstName = isPupil ? parent.FirstName : PupilEdit.FirstName;
                            pupil.MiddleName = isPupil ? parent.MiddleName : PupilEdit.MiddleName;
                            pupil.Birthday = Convert.ToDateTime(PupilEdit.BirthDay);
                            pupil.Sex = Convert.ToInt32(PupilEdit.Sex);
                            pupil.PhoneNumber = parent.PhoneNumber;
                            pupil.ExpressPhoneNumber = parent.ExpressPhoneNumber;
                            pupil.EMail = parent.EMail;
                            pupil.Passport = new ExtendedPassport()
                            {
                                PassportType = new ExtendedPassportType()
                                {
                                    PassportTypeId = PupilEdit.PassportType
                                },
                                Series = PupilEdit.Series,
                                Number = PupilEdit.Number,
                                IssueDate = PupilEdit.IssueDate
                            };
                            pupil.SchoolName = PupilEdit.SchoolName;
                            pupil.ClassName = PupilEdit.ClassName;

                            /*pupil.Parent = new ExtendedUser();
                            pupil.Parent.UserId = ParentId;
                            pupil.Parent.LastName = parent.LastName;
                            pupil.Parent.FirstName = parent.FirstName;
                            pupil.Parent.MiddleName = parent.MiddleName;
                            pupil.Parent.PhoneNumber = parent.PhoneNumber;
                            pupil.Parent.ExpressPhoneNumber = parent.ExpressPhoneNumber;
                            pupil.Parent.EMail = parent.EMail;
                            pupil.Parent.Passport = new ExtendedPassport()
                            {
                                PassportType = new ExtendedPassportType()
                                {
                                    PassportTypeId = parent.PassportType
                                },
                                Series = parent.Series,
                                Number = parent.Number,
                                IssueDate = parent.IssueDate
                            };
                            pupil.Parent.UserType = Convert.ToInt32(parent.Relativies);*/

                            db1.UpdatePupil(PupilId, pupil);

                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat("(Идентификатор Ученика: {0})", PupilId);

                            if (oldPupil.Fio != pupil.Fio)
                                sb.AppendFormat("(Имя: {0} -> {1})", oldPupil.Fio, pupil.Fio);
                            if (oldPupil.Birthday != pupil.Birthday)
                                sb.AppendFormat("(Дата рождения: {0} -> {1})", oldPupil.Birthday, pupil.Birthday);
                            if (oldPupil.Passport.PassportType.PassportTypeId != pupil.Passport.PassportType.PassportTypeId)
                                sb.AppendFormat("(Пасспорт, тип: {0} -> {1})", oldPupil.Passport.PassportType.PassportTypeId, pupil.Passport.PassportType.PassportTypeId);
                            if (oldPupil.Passport.Series != pupil.Passport.Series)
                                sb.AppendFormat("(Пасспорт, серия: {0} -> {1})", oldPupil.Passport.Series, pupil.Passport.Series);
                            if (oldPupil.Passport.Number != pupil.Passport.Number)
                                sb.AppendFormat("(Пасспорт, номер: {0} -> {1})", oldPupil.Passport.Number, pupil.Passport.Number);
                            if (oldPupil.Passport.IssueDate != pupil.Passport.IssueDate)
                                sb.AppendFormat("(Пасспорт, дата выдачи: {0} -> {1})", oldPupil.Passport.IssueDate, pupil.Passport.IssueDate);
                            if (oldPupil.Sex != pupil.Sex)
                                sb.AppendFormat("(Пол: {0} -> {1})", oldPupil.Sex, pupil.Sex);
                            if (oldPupil.PhoneNumber != pupil.PhoneNumber)
                                sb.AppendFormat("(Номер телефона 1: {0} -> {1})", oldPupil.PhoneNumber, pupil.PhoneNumber);
                            if (oldPupil.ExpressPhoneNumber != pupil.ExpressPhoneNumber)
                                sb.AppendFormat("(Номер телефона 2: {0} -> {1})", oldPupil.ExpressPhoneNumber, pupil.ExpressPhoneNumber);
                            if (oldPupil.SchoolName != pupil.SchoolName)
                                sb.AppendFormat("(Школа: {0} -> {1})", oldPupil.SchoolName, pupil.SchoolName);
                            if (oldPupil.ClassName != pupil.ClassName)
                                sb.AppendFormat("(Класс: {0} -> {1})", oldPupil.ClassName, pupil.ClassName);

                            /*if (oldPupil.Parent.UserType != pupil.Parent.UserType)
                                sb.AppendFormat("(Заявитель: {0} -> {1})", oldPupil.Parent.UserType.ToString(), pupil.Parent.UserType.ToString());
                            if (oldPupil.Parent.Fio != pupil.Parent.Fio)
                                sb.AppendFormat("(Имя заявителя: {0} -> {1})", oldPupil.Parent.Fio, pupil.Parent.Fio);
                            if (oldPupil.Parent.Passport.PassportType.PassportTypeId != pupil.Parent.Passport.PassportType.PassportTypeId)
                                sb.AppendFormat("(Пасспорт заявителя, тип: {0} -> {1})", oldPupil.Parent.Passport.PassportType.PassportTypeId, pupil.Parent.Passport.PassportType.PassportTypeId);
                            if (oldPupil.Parent.Passport.Series != pupil.Parent.Passport.Series)
                                sb.AppendFormat("(Пасспорт заявителя, серия: {0} -> {1})", oldPupil.Parent.Passport.Series, pupil.Parent.Passport.Series);
                            if (oldPupil.Parent.Passport.Number != pupil.Parent.Passport.Number)
                                sb.AppendFormat("(Пасспорт заявителя, номер: {0} -> {1})", oldPupil.Parent.Passport.Number, pupil.Parent.Passport.Number);
                            if (oldPupil.Parent.Passport.IssueDate != pupil.Parent.Passport.IssueDate)
                                sb.AppendFormat("(Пасспорт заявителя, дата выдачи: {0} -> {1})", oldPupil.Parent.Passport.IssueDate, pupil.Parent.Passport.IssueDate);
                            if (oldPupil.Parent.PhoneNumber != pupil.Parent.PhoneNumber)
                                sb.AppendFormat("(Номер телефона заявителя 1: {0} -> {1})", oldPupil.Parent.PhoneNumber, pupil.Parent.PhoneNumber);
                            if (oldPupil.Parent.ExpressPhoneNumber != pupil.Parent.ExpressPhoneNumber)
                                sb.AppendFormat("(Номер телефона заявителя 2: {0} -> {1})", oldPupil.Parent.ExpressPhoneNumber, pupil.Parent.ExpressPhoneNumber);*/

                            EventsUtility.LogPupilUpdate(UserContext.Profile.UserId, Request.UserHostAddress, sb.ToString());
                        }
                    }
                }
                // проверка на 20 часов и на 3 УДО
                ExtendedLimit limit;
                using (PupilsDb db = new PupilsDb())
                {
                    limit = db.GetLimit(PupilId, null);
                }

                DateTime now = DateTime.Today;
                int fullYears = now.Year - PupilEdit.BirthDay.Year;
                if (PupilEdit.BirthDay > now.AddYears(-fullYears)) fullYears--;
                bool islimitHours = false;

                if (RegAddress.Code.Length > 8)
                using (ChildUnionDb db = new ChildUnionDb())
                {
                    ExtendedChildUnion cu = db.GetChildUnion(selectUdod.UdodAgeGroupId.Value, null);
                    foreach (ExtendedTypeBudget tb in cu.BudgetTypes)
                    {
                        if (tb.DictTypeBudgetId == 2)
                        {
                            lblLimit.Text = "По существующим правилам, для детей, не имеющих московской регистрации, предоставляется возможность записаться на обучение в учреждения дополнительного образования только на платные программы обучения.";
                            upLimit.Update();
                            popupLimitExtender.Show();
                            return false;
                        }
                    }
                }
                /*using (AgeGroupDb db = new AgeGroupDb())
                {
                    // здесь смотрим если в списке ageGroup группа, подходящая по возрасту ученику
                    var list = db.GetAgeGroups(null, null, null, selectUdod.UdodAgeGroupId.Value);
                    ExtendedAgeGroup item = list.Where(i => i.Age == fullYears).FirstOrDefault();
                    if (item.UdodAgeGroupId == 0)
                    {
                        //lblLimit.Text = "Вы выбранное ДО обучающийся не подходит по возрасту. Выберите другое ДО.";
                        // upLimit.Update();
                        //popupLimitExtender.Show();
                        //return false;
                    }
                    else
                    {
                        // теперь можно проверить лимиты
                        using (PupilsDb db1 = new PupilsDb())
                        {
                            var list1 = db1.GetLimitUdods(PupilId);

                            if (limit.CountUdod >= 3 && list1.Count(i => i.UdodId == item.UdodId) == 0)
                            {
                                // лимит превышен
                                lblLimit.Text = "У данного обучающегося превышен лимит на количество заявлений в разные УДО";
                                upLimit.Update();
                                popupLimitExtender.Show();
                                return false;
                            }
                        }

                        // проверка на часы нужна только если у ДО к которому относится только что найденнная группа, стоит галочка что ДО бесплатное
                        using (ChildUnionDb db1 = new ChildUnionDb())
                        {
                            var item1 = db1.GetChildUnion(null, item.UdodAgeGroupId);
                            foreach (var i in item1.BudgetTypes)
                            {
                                if (i.DictTypeBudgetId == 2)
                                {
                                    if (limit.Hours + item.LessonLength * item.LessonsInWeek / 60 > limit.NormHours)
                                    {
                                        islimitHours = true;
                                        lblLimit.Text = "У данного обучающегося превышен лимит на количество часов в неделю (" + limit.NormHours + " академ.часов). Невозможно сохранить заявку.";
                                        upLimit.Update();
                                        popupLimitExtender.Show();
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
                if (limit.Hours >= limit.NormHours)
                {
                    lblLimit.Text = "У данного обучающегося превышен лимит на количество часов в неделю (" + limit.NormHours + " академ.часов)";
                    upLimit.Update();
                    popupLimitExtender.Show();
                    return;
                }*/

                using (KladrDb db = new KladrDb())
                {
                    // добавление адреса пользователю
                    // если адреса совпадают то вместо фактического адреса записываем регистрации но с другим AddressTypeId
                    /*if (rbnFacRegAddress.SelectedValue == "True")
                        db.AddAddress(RegAddress.Code, RegAddress.Index, RegAddress.NumberHouse, RegAddress.Fraction,
                                      RegAddress.Housing, RegAddress.Building, RegAddress.Flat, FactAddress.AddressTypeId, 0,
                                      0, PupilId, null, string.Empty, string.Empty, string.Empty, RegAddress.typeAddressCode);
                    else
                        db.AddAddress(FactAddress.Code, FactAddress.Index, FactAddress.NumberHouse, FactAddress.Fraction,
                                      FactAddress.Housing, FactAddress.Building, FactAddress.Flat, FactAddress.AddressTypeId,
                                      0, 0, PupilId, null, string.Empty, string.Empty, string.Empty, RegAddress.typeAddressCode);*/
                    //lblFactAddress.Text = FactAddress.Value.AddressStr;

                    db.AddAddress(RegAddress.Code, RegAddress.Index, RegAddress.NumberHouse, RegAddress.Fraction,
                                  RegAddress.Housing, RegAddress.Building, RegAddress.Flat, RegAddress.AddressTypeId, 0, 0,
                                  PupilId, null, string.Empty, string.Empty, string.Empty, RegAddress.typeAddressCode, string.Empty);
                    //lblRegAddress.Text = RegAddress.Value.AddressStr;
                }
                long? claimnumber = null;
                using (ClaimDb db = new ClaimDb())
                {
                    //debug.Text = UserContext.ClaimId.HasValue.ToString() + "<br />";
                    if (!UserContext.ClaimId.HasValue)
                    {
                        // add claim
                        currentClaimNumber = claimnumber = db.AddClaim(PupilId,
                                    !string.IsNullOrEmpty(tbBeginDate.Text)
                                        ? Convert.ToDateTime(tbBeginDate.Text)
                                        : DateTime.Now, selectUdod.UdodAgeGroupId.Value,
                                    Convert.ToInt32(ddlYearOfStudy.SelectedValue), UserContext.Roles.RoleId.Value, ParentId, false);

                        if (claimnumber == -1)
                        {
                            Response.Write("<script>window.alert('В выбранное ДО обучающийся не подходит по возрасту. Выберите другое ДО.');</script>");
                            return false;
                        }

                        if (claimnumber == basedOnClaimNumber)
                        {
                            Response.Write("<script>window.alert('Заявка в выбранное ДО для данного обучающегося уже создана');</script>");
                            return false;
                        }

                        using (ClaimDb db1 = new ClaimDb())
                        {
                            claim = db1.GetClaims(null, null, null, null, null, null, claimnumber.ToString(),
                                null, null, null, null, null, null, null, null, null, null, null, null, null, null).FirstOrDefault();
                            UserContext.ClaimId = claim.ClaimId;

                            EventsUtility.LogClaimCreate(UserContext.Profile.UserId, Request.UserHostAddress, String.Format("(Номер заявления: {0})", claimnumber), PupilId, claim.ClaimId, claim.Udod.UdodId, claim.ChildUnion.ChildUnionId);
                        }
                            //debug.Text += "claim created";

                        if (enrollRightNow)
                        {
                            changeStatus("lnkChangeStatusEnroll");
                        }

                        if (showSaveClaimPopup)
                        {
                            lblClaimInfo.Text = "Номер заявления: <b>" + (claimnumber.HasValue ? claimnumber.Value.ToString() : "") + "</b><br/>";
                            lblClaimInfo.Text += "ФИО обучающегося: <b>" + (isPupil ? parent.LastName : PupilEdit.LastName) + " " + (isPupil ? parent.FirstName : PupilEdit.FirstName) + " " + (isPupil ? parent.MiddleName : PupilEdit.MiddleName) + "</b><br/>";
                            lblClaimInfo.Text += "Детское объединение: <b>" + lblChildUnion.Text + "</b><br/>";
                            lblClaimInfo.Text += "Желаемая дата: <b>" + tbBeginDate.Text + "</b><br/>";

                            
                            if (islimitHours)
                                lblClaimInfo.Text +=
                                    "<span style='color: red;'> Внимание! Для данного учащегося возможно превышение суммарного количества часов <br/>" +
                                    "по всем зачислениям во все детские объединения. Это может привести <br/>" +
                                    "к повышенной утомляемости ребёнка </span><br/>";

                            if (claimnumber.HasValue)
                            {
                                string message = EmailBody.Text
                                    .Replace("#LastName#", parent.LastName)
                                    .Replace("#FirstName#", parent.FirstName)
                                    .Replace("#MiddleName#", parent.MiddleName)
                                    .Replace("#DeadlineDate#", DateTime.Today.AddDays(7).ToShortDateString())
                                    .Replace("#ClaimNumber#", (claimnumber.HasValue ? claimnumber.Value.ToString() : ""));

                                using (UserDb db2 = new UserDb())
                                {
                                    var pupil = db2.GetUser(PupilId);
                                    message = message.Replace("#PupilFio#", pupil.Fio);
                                }

                                using (ChildUnionDb db2 = new ChildUnionDb())
                                {
                                    var udod = db2.GetUdod(selectUdod.UdodAgeGroupId.Value);
                                    message = message.Replace("#OrgName#", udod.Name)
                                                        .Replace("#OrgSite#", udod.SiteUrl);

                                    var cu = db2.GetChildUnion(selectUdod.UdodAgeGroupId.Value, null);
                                    message = message.Replace("#ChildUnionName#", cu.Name);
                                }

                                if ((!SendEmailMessage(parent.EMail, EmailSubject.Text, message)) && (!string.IsNullOrEmpty(parent.EMail)))
                                {
                                    lblClaimInfo.Text += "<b style='color: red'>Не удалось отправить письмо с уведомлением на ящик " + parent.EMail + ". Обратитесь к администратору системы.</b><br/>";
                                }
                            }

                            popupSaveClaimExtender.Show();
                        }
                    }
                    else
                    {
                        //var s = DateTime.Now;
                        var prev = db.GetClaim(UserContext.ClaimId.Value);
                        // update claim
                        db.UpdateClaim(UserContext.ClaimId.Value, PupilId,
                                       !string.IsNullOrEmpty(tbBeginDate.Text)
                                           ? Convert.ToDateTime(tbBeginDate.Text)
                                           : DateTime.Now, selectUdod.UdodAgeGroupId.Value,
                                            prev.ClaimStatus.ClaimStatusId,
                                       Convert.ToInt32(ddlYearOfStudy.SelectedValue), false);

                        var curr = db.GetClaim(UserContext.ClaimId.Value);

                        //debug.Text += (DateTime.Now - s) + " ms";

                        if (prev.ClaimStatus.ClaimStatusId != curr.ClaimStatus.ClaimStatusId)
                        {
                            EventsUtility.LogClaimStatusUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                                String.Format("(Номер заявления: {0})(Статус: {1} -> {2})", prev.Number, prev.ClaimStatus.ClaimStatusName, curr.ClaimStatus.ClaimStatusName), PupilId, UserContext.ClaimId.Value, curr.Udod.UdodId, curr.ChildUnion.ChildUnionId);
                        }

                        EventsUtility.LogClaimUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                            EventsUtility.GetClaimUpdateMessage(prev, curr));
                    }
                }
            }
        }
        catch (Exception e)
        {
            popupErrorExtender.Show();
        }
        return true;
    }

    protected void lnkSave_Click(object sender, EventArgs e)
    {
        SaveClaim(true);
    }

    protected void passDataValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        //args.IsValid = false;
    }

    private bool SendEmailMessage(string emailTo, string subject, string message)
    {
        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
        System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(ConfigurationManager.AppSettings["SmtpHost"], Int32.Parse(ConfigurationManager.AppSettings["SmtpPort"]));
        try
        {
            msg.From = new MailAddress(ConfigurationManager.AppSettings["SmtpFrom"], ConfigurationManager.AppSettings["SmtpFromName"]);
            msg.To.Add(emailTo);

            msg.Subject = subject;
            msg.IsBodyHtml = true;
            msg.Body = message;

            //smtpClient.EnableSsl = true;
            //smtpClient.UseDefaultCredentials = true;
            //smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SmtpUser"], ConfigurationManager.AppSettings["SmtpPass"]);

            smtpClient.Send(msg);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    protected void btnSelectUdod_Click(object sender, EventArgs e)
    {
        popupSelectExtender.Hide();

        //если зачислить прямо сейчас то якобы жмем save сразу же, если нет - заполняем таблицу выбранным ДО
        if (!enrollRightNow)
        {
            //lblUdod.Text = selectUdod.UdodSectionName;
            ExtendedChildUnion item;
            using (ChildUnionDb db = new ChildUnionDb())
            {
                item = db.GetChildUnion(selectUdod.UdodAgeGroupId, null);
                lblProgram.Text = item.Program.Name;
                lblSection.Text = item.Section.Name;
                //lblTeacherFIO.Text = item.TeacherFio;
                lblChildUnion.Text = item.Name;
                lblAge.Text = "от " + item.AgeStart.ToString() + " до " + item.AgeEnd.ToString();
                //lblBudget.Text = item.TypeBudget.TypeBudgetName;
                //lblNumYears.Text = item.NumYears.ToString();
                ddlYearOfStudy.Items.Clear();
                for (int i = 1; i <= item.NumYears; i++)
                {
                    ddlYearOfStudy.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
            using (KladrDb db = new KladrDb())
            {
                item.Addresses = db.GetAddressesById(null, null, selectUdod.UdodAgeGroupId, null);
                if (item.Addresses.Count() > 0) lblAddress.Text = item.Addresses.FirstOrDefault().AddressStr;
            }
            lblChildUnionDeleted.Visible = false;
            lnkDetail.Visible = true;
            lnkSelectUdod.Text = "<div class='btnBlueLong'>Изменить ДО</div>";
            upUdod.Update();
        }
        else
        {
            if (SaveClaim(false))
                Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
        }
    }

    protected void lnkSelectUdod_Click(object sender, EventArgs e)
    {
        enrollRightNow = false;
        popupSelectExtender.Show();
        popupSelectUdod.Style["visibility"]= "visible";
        popupSelectUdod.Style["display"]= "block";
    }

    protected void lnkDetail_OnClick(object sender, EventArgs e)
    {
        // Загружать детальный просмотр usercontrol Глеба
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var childunion = db.GetChildUnion(selectUdod.UdodAgeGroupId, null);
            if (childunion!=null)
            selectDO.SetDOData(childunion);
            popupSelectDOExtender.Show();
            upDO.Update();
        }
    }

    protected void btnSaveClaim_OnClick(object sender, EventArgs e)
    {
        // В зависимости от выбора
        UserContext.ClaimId = null;
        if (select.SelectedValue == "0")
        {

        }
        if (select.SelectedValue == "1")
        {
            PupilEdit.Clear();
        }
        if (select.SelectedValue == "2")
        {
            //ucDocument.Clear();
            /*PupilEdit.Clear();
            parent.Clear();
            selectUdod.Clear();*/
            Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
        }
        if (select.SelectedValue == "3")
        {
            // редирект на список заявок
            Page.Response.Redirect(SiteUtility.GetUrl("~/udod/ClaimList.aspx"));
        }

        popupSelectExtender.Hide();
    }

    protected void btnSelectAddress_OnClick(object sender, EventArgs e)
    {
        /*if (PopupAddress.AddressTypeId==5)
        {
            RegAddress = new ExtendedAddress()
            {
                AddressStr = PopupAddress.AddressStr,
                AddressType = new ExtendedAddressType()
                {
                    AddressTypeId = PopupAddress.AddressTypeId
                },
                HouseNumber = PopupAddress.NumberHouse,
                PostIndex = PopupAddress.Index,
                Building = PopupAddress.Building,
                Fraction = PopupAddress.Fraction,
                Housing = PopupAddress.Housing,
                FlatNumber = PopupAddress.Flat,
                KladrCode = PopupAddress.Code
            };
            lblRegAddress.Text = RegAddress.Value.AddressStr;
            upRegAddress.Update();
        }
        else
        {
            FactAddress = new ExtendedAddress()
            {
                AddressStr = PopupAddress.AddressStr,
                AddressType = new ExtendedAddressType()
                {
                    AddressTypeId = PopupAddress.AddressTypeId
                },
                HouseNumber = PopupAddress.NumberHouse,
                PostIndex = PopupAddress.Index,
                Building = PopupAddress.Building,
                Fraction = PopupAddress.Fraction,
                Housing = PopupAddress.Housing,
                FlatNumber = PopupAddress.Flat,
                KladrCode = PopupAddress.Code
            };
            lblFactAddress.Text = FactAddress.Value.AddressStr;
            upFactAddress.Update();
        }

        popupAddAddressExtender.Hide();*/
    }

    protected void lnkAddRegAddress_OnClick(object sender, EventArgs e)
    {
        /*LinkButton btn = (LinkButton) sender;
        if (btn.ID == "lnkAddRegAddress")
        {
            PopupAddress.AddressTypeId = 5;
            if (RegAddress.HasValue)
            {
                PopupAddress.Code = RegAddress.Value.KladrCode;
                PopupAddress.Housing = RegAddress.Value.Housing;
                PopupAddress.NumberHouse = RegAddress.Value.HouseNumber;
                PopupAddress.Fraction = RegAddress.Value.Fraction;
                PopupAddress.Flat = RegAddress.Value.FlatNumber;
                PopupAddress.Building = RegAddress.Value.Building;
            }
        }
        else
        {
            PopupAddress.AddressTypeId = 6;
            if (FactAddress.HasValue)
            {
                PopupAddress.Code = FactAddress.Value.KladrCode;
                PopupAddress.Housing = FactAddress.Value.Housing;
                PopupAddress.NumberHouse = FactAddress.Value.HouseNumber;
                PopupAddress.Fraction = FactAddress.Value.Fraction;
                PopupAddress.Flat = FactAddress.Value.FlatNumber;
                PopupAddress.Building = FactAddress.Value.Building;
            }
        }
        //upAddress.Update();
        popupAddAddressExtender.Show();*/
    }

    protected void rbnIsPupil_SelectedChanged(object sender, EventArgs e)
    {
        parent.RelativeEnabled = !Convert.ToBoolean(rbnIsPupil.SelectedValue);
        PupilEdit.Enable14YearsValidation = Convert.ToBoolean(rbnIsPupil.SelectedValue);
        PupilEdit.FioEnabled = !Convert.ToBoolean(rbnIsPupil.SelectedValue);
        PupilEdit.PassportEnabled = !Convert.ToBoolean(rbnIsPupil.SelectedValue);
        PupilEdit.LastName = parent.LastName;
        PupilEdit.FirstName = parent.FirstName;
        PupilEdit.MiddleName = parent.MiddleName;
        upIsPupil.Update();
    }

    protected void rbnFacRegAddress_SelectedChanged(object sender, EventArgs e)
    {
        //FactAddressPanel.Enabled = FactAddress.Enabled = !Convert.ToBoolean(rbnFacRegAddress.SelectedValue);
        upAddresses.Update();
    }

    protected void lnkCancel_OnClick(object sender, EventArgs e)
    {
        Page.Response.Redirect(SiteUtility.GetUrl("~/Udod/ClaimList.aspx"));
    }

    protected void changeStatus_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        changeStatus(btn.ID);
    }

    protected void changeStatus(string btnId)
    {
        int statusId = 2;
        //LinkButton btn = (LinkButton)sender;
        if (btnId == "lnkChangeStatusDocs")
        {
            statusId = 7;
        }
        if (btnId == "lnkChangeStatusEnroll")
        {
            // перед сменой статуса на "Зачислен" необходимо проверить лимит на часы
            Guid PupilId = Guid.Empty;
            if (UserContext.ClaimId.HasValue)
            {
                using (ClaimDb db = new ClaimDb())
                {
                    PupilId = db.GetClaim(UserContext.ClaimId.Value).Pupil.UserId;
                }
            }
            // проверка на 20 часов и на 3 УДО
            ExtendedLimit limit;
            using (PupilsDb db = new PupilsDb())
            {
                limit = db.GetLimit(PupilId, null);
            }

            DateTime now = DateTime.Today;
            int fullYears = now.Year - PupilEdit.BirthDay.Year;
            if (PupilEdit.BirthDay > now.AddYears(-fullYears)) fullYears--;

            using (AgeGroupDb db = new AgeGroupDb())
            {
                var list = db.GetAgeGroups(null, null, null, selectUdod.UdodAgeGroupId.Value);
                ExtendedAgeGroup item = list.Where(i => i.Age == fullYears).FirstOrDefault();

                // проверка на часы нужна только если у ДО к которому относится только что найденнная группа, стоит галочка что ДО бесплатное
                using (ChildUnionDb db1 = new ChildUnionDb())
                {
                    var item1 = db1.GetChildUnion(selectUdod.UdodAgeGroupId.Value, null);
                    foreach (var i in item1.BudgetTypes)
                    {
                        if (i.DictTypeBudgetId == 2)
                        {
                            if (limit.Hours + item.LessonLength * item.LessonsInWeek / 60 > limit.NormHours)
                            {
                                bool isPupil = Convert.ToBoolean(rbnIsPupil.SelectedValue);
                                StringBuilder limitText = new StringBuilder();
                                limitText.Append(isPupil ? parent.LastName : PupilEdit.LastName);
                                limitText.Append(" " + (isPupil ? parent.FirstName : PupilEdit.FirstName));
                                limitText.Append(" " + (isPupil ? parent.MiddleName : PupilEdit.MiddleName));
                                limitText.Append(" уже посещает ");
                                using (PupilsDb db2 = new PupilsDb())
                                {
                                    List<ExtendedChildUnion> l = db2.GetChildUnion(PupilId);
                                    foreach (ExtendedChildUnion cu in l)
                                    {
                                        limitText.Append(cu.Name + ", ");
                                    }
                                    limitText.Remove(limitText.Length - 2, 2);
                                }
                                limitText.Append(" с суммарным количеством часов занятий ");
                                limitText.Append(limit.Hours + " часов в неделю. ");
                                limitText.Append("Для детей его/её возраста действует ограничение СанПиН в ");
                                limitText.Append(limit.NormHours + " часов занятий в неделю. ");
                                limitText.Append("В связи с чем осуществить зачисление в ДО ");
                                limitText.Append(selectUdod.UdodSectionName + " с продолжительностью занятий ");
                                limitText.Append((item.LessonLength * item.LessonsInWeek / 60) + " часов в неделю невозможно, ");
                                limitText.Append("так как это приводит к превышению установленных норм.");

                                if (enrollRightNow) Session["needLimitAlert"] = limitText.ToString();
                                lblLimit.Text = limitText.ToString();
                                upLimit.Update();
                                popupLimitExtender.Show();
                                return;
                            }
                        }
                    }
                }
            }

            using (PupilsDb db = new PupilsDb())
            {
                if (!db.GetIsDismissed(UserContext.ClaimId.Value, null, null))
                {
                    lblLimit.Text = "Данный обучающийся уже зачислен в выбранное ДО";
                    upLimit.Update();
                    popupLimitExtender.Show();
                    return;
                }
            }
            statusId = 4;
        }
        if (btnId == "lnkChangeStatusReserv")
        {
            statusId = 6;
        }
        if (btnId == "lnkChangeStatusAnnul")
        {
            statusId = 5;
        }
        
        using (ClaimDb db = new ClaimDb())
        {
            ExtendedClaim claim = db.GetClaim(UserContext.ClaimId.Value);

            if (claim.ClaimStatus.ClaimStatusName == lblStatus.Text)
            {   
                db.UpdateClaimStatus(UserContext.ClaimId.Value, statusId, null, null, null);
//               ExportInAsGuf tmp = new ExportInAsGuf();
                //tmp.UpdateStatus(statusId, UserContext.ClaimId.Value, "");

                //StatusButtonsEnabling(statusId);

                EventsUtility.LogClaimStatusUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                        String.Format("(Номер заявления: {0})(Статус: {1} -> {2})", claim.Number, claim.ClaimStatus.ClaimStatusId, statusId), claim.Pupil.UserId, claim.ClaimId, claim.Udod.UdodId, claim.ChildUnion.ChildUnionId);
            }
            claim = db.GetClaim(UserContext.ClaimId.Value);

            lblStatus.Text = claim.ClaimStatus.ClaimStatusName;
            
            StatusButtonsEnabling(claim.ClaimStatus.ClaimStatusId);

            if ((claim.ClaimStatus.ClaimStatusId == 5) || (claim.ClaimStatus.ClaimStatusId == 4))
            {
                parent.Enabled = PupilEdit.Enabled =
                    rbnFacRegAddress.Enabled =
                    RegAddress.Enabled =
                    //FactAddress.Enabled =
                    lnkSelectUdod.Visible =
                    tbBeginDate.Enabled =
                    ddlYearOfStudy.Enabled =
                    lnkSave.Visible =
                    //lnkPrint.Visible =
                    rbnIsPupil.Enabled = false;

                lnkSave.Visible = false;
            }
        }
    }

    protected void lnkCreateClaimFromCurrent_OnClick(object sender, EventArgs e)
    {
        UserContext.ClaimId = null;
        enrollRightNow = true;
        popupSelectExtender.Show();
    }

    protected void StatusButtonsEnabling(int claimStatusId)
    {
        lnkChangeStatusDocs.Enabled = false;
        lnkChangeStatusEnroll.Enabled = false;
        lnkChangeStatusReserv.Enabled = false;
        lnkChangeStatusAnnul.Enabled = false;
        //lnkChangeStatusInviteDiv.Style["background-color"] = "RGB(216,216,216)";
        lnkChangeStatusDocsDiv.Style["background-color"] = "RGB(180,180,180)";
        lnkChangeStatusEnrollDiv.Style["background-color"] = "RGB(180,180,180)";
        divChangeStatusReserv.Style["background-color"] = "RGB(180,180,180)";
        divChangeStatusAnnul.Style["background-color"] = "RGB(180,180,180)";
            
        if (claimStatusId == 1) // если на рассмотрении, то можно аннулировать, либо подать документы либо перевести в резерв
        {
            lnkChangeStatusDocs.Enabled = true;
            lnkChangeStatusReserv.Enabled = true;
            lnkChangeStatusAnnul.Enabled = true;
            lnkChangeStatusDocsDiv.Style["background-color"] = "RGB(40,84,147)";
            divChangeStatusReserv.Style["background-color"] = "RGB(40,84,147)";
            divChangeStatusAnnul.Style["background-color"] = "RGB(40,84,147)";
        }
        if (claimStatusId == 7) // если поданы документы, то можно аннулировать, либо зачислить либо перевести в резерв
        {
            lnkChangeStatusEnroll.Enabled = true;
            lnkChangeStatusReserv.Enabled = true;
            lnkChangeStatusAnnul.Enabled = true;
            lnkChangeStatusEnrollDiv.Style["background-color"] = "RGB(40,84,147)";
            divChangeStatusReserv.Style["background-color"] = "RGB(40,84,147)";
            divChangeStatusAnnul.Style["background-color"] = "RGB(40,84,147)";
        }
        if (claimStatusId == 6) // если в резерве, то можно аннулировать, либо зачислить
        {
            lnkChangeStatusEnroll.Enabled = true;
            lnkChangeStatusAnnul.Enabled = true;
            lnkChangeStatusEnrollDiv.Style["background-color"] = "RGB(40,84,147)";
            divChangeStatusAnnul.Style["background-color"] = "RGB(40,84,147)";
        }
        if (claimStatusId == 5) // если аннулировано, то ничего нельзя
        {
        }
        if (claimStatusId == 4) // если зачислен то ничего нельзя
        {
        }
    }

    protected void lnkSelectUdod_OnClick(object sender, EventArgs e)
    {
        
    }
}
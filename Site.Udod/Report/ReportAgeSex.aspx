﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="ReportAgeSex.aspx.cs" Inherits="Report_ReportAgeSex" %>

<asp:Content runat="server" ID="content" ContentPlaceHolderID="body">

    <script type="text/javascript">
    /*Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageload1);

    function pageload1() {
        var reporttable = document.getElementById('ReportTableId');
        var rows = $(reporttable).find('tr');
        var i = 1;
        rows.each(function() {
            i++;
            if (i > 4) {
                var cells = $(this).find("td");
                if (i % 2 == 1) {
                    $(cells[0]).attr("rowspan", "2");
                } else {
                    $(cells[0]).remove();
                }
            }
        });
    }*/
</script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Label runat="server" Font-Size="24px" ID="lblTitle" Text="Комплектование контингента программ дополнительного образования в разрезе направленностей и возрастов" />
            <table class="ReportTable">
                <tr>
                    <td class="ReportTable" runat="server" ID="td1">
                        <asp:CheckBox runat="server" ID="cbCity" Text="Выбрать отдельный округ" AutoPostBack="true" OnCheckedChanged="cbCity_OnCheckedChanged"/>
                    </td>
                    <td class="ReportTable" runat="server" ID="td2">
                        <asp:CheckBox runat="server" ID="cbTypeUdod" Text="Разделить по типу ОУ" AutoPostBack="true" OnCheckedChanged="cbTypeUdod_OnCheckedChanged"/>
                    </td>
                    <td class="ReportTable" runat="server" ID="td3">
                        Выбрать учреждение
                    </td>
                    <td class="ReportTable" rowspan="2">
                        <asp:RadioButtonList runat="server" ID="rblDistinct">
                            <Items>
                                <asp:ListItem runat="server" Value="false" Selected="true">Считать по человеко-кружкам</asp:ListItem>
                                <asp:ListItem runat="server" Value="true">Считать по уникальным детям</asp:ListItem>
                            </Items>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="ReportTable" runat="server" ID="td4">
                        <div style="height: 150px; width: 200px; overflow: auto;">
                            <asp:CheckBoxList runat="server" RepeatColumns="2" ID="cblCity" AutoPostBack="true" Enabled="false" DataSourceID="dsCity" DataTextField="ShortName" DataValueField="CityId" OnDataBound="cblCity_OnDataBound" OnSelectedIndexChanged="cblCity_OnSelectedIndexChanged"/>
                        </div>
                    </td>
                    <td class="ReportTable" style="vertical-align: top;" runat="server" ID="td5">
                        <asp:CheckBoxList runat="server" ID="cblUdodType" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="cblUdodType_OnSelectedIndexChanged">
                            <Items>
                                <asp:ListItem Value="0" Selected="true">УДО</asp:ListItem>
                                <asp:ListItem Value="1" Selected="true">ОУ</asp:ListItem>
                                <asp:ListItem Value="2" Selected="true">ДОУ</asp:ListItem>
                                <asp:ListItem Value="3" Selected="true">СПО</asp:ListItem>
                            </Items>
                        </asp:CheckBoxList>
                    </td>
                    <td class="ReportTable" runat="server" ID="td6">
                        <asp:TextBox runat="server" ID="tbUdodFilter" AutoPostBack="true" OnTextChanged="tbUdodFilter_OnTextChanged"></asp:TextBox>
                        <br/>
                        <asp:DropDownList CssClass="input" runat="server" ID="ddlUdod" DataTextField="ShortName" DataValueField="UdodId" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="ReportTable" align="right">
                        <asp:LinkButton runat="server" ID="lnkFind" OnClick="lnkFind_OnClick"><div class="btnBlue">Построить отчет</div></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
<asp:LinkButton runat="server" ID="lnkExport" OnClick="lnkExport_OnClick"><div class="btnBlue">Экспорт в Excel</div></asp:LinkButton>
    <asp:UpdatePanel runat="server" ID="upReport" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Repeater runat="server" ID="repReport" >
                <HeaderTemplate>
                    <table id="ReportTableId" class="ReportTable" cellspacing="0px" style="width: 950px;">
                    <tr class="reportHeader">
                        <td class="ReportTable" rowspan="3">Направленность</td>
                        <td class="ReportTable" rowspan="3">Вид услуги</td>
                        <td class="ReportTable" colspan="54">Кол-во обучающихся по программам доп-образвоания по направленностям:</td>
                    </tr>
                    <tr class="reportHeader">
                        <td class="ReportTable" colspan="3">До 5 лет</td>
                        <td class="ReportTable" colspan="3">5 лет</td>
                        <td class="ReportTable" colspan="3">6 лет</td>
                        <td class="ReportTable" colspan="3">7 лет</td>
                        <td class="ReportTable" colspan="3">8 лет</td>
                        <td class="ReportTable" colspan="3">9 лет</td>
                        <td class="ReportTable" colspan="3">10 лет</td>
                        <td class="ReportTable" colspan="3">11 лет</td>
                        <td class="ReportTable" colspan="3">12 лет</td>
                        <td class="ReportTable" colspan="3">13 лет</td>
                        <td class="ReportTable" colspan="3">14 лет</td>
                        <td class="ReportTable" colspan="3">15 лет</td>
                        <td class="ReportTable" colspan="3">16 лет</td>
                        <td class="ReportTable" colspan="3">17 лет</td>
                        <td class="ReportTable" colspan="3">18 лет</td>
                        <td class="ReportTable" colspan="3">19 лет и более</td>
                        <td class="ReportTable" colspan="3">Итого 12-15 лет</td>
                        <td class="ReportTable" colspan="3">ИТОГО</td>
                    </tr>
                    <tr>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 10px; font-weight: bold;">всего</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 20px;">мальч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 20px;">девоч.</td>
                        <td class="ReportTable" style="width: 45px; padding-right: 20px; font-weight: bold;">всего</td>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="ReportTable"><%#Eval("ProgramName")%></td>
                        <td class="ReportTable"><%#Eval("TypeBudgetName")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[0]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[1]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[2]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[3]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[4]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[5]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[6]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[7]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[8]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[9]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[10]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[11]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[12]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[13]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[14]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[15]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[16]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[17]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[18]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[19]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[20]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[21]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[22]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[23]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[24]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[25]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[26]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[27]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[28]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[29]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[30]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[31]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[32]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[33]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[34]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[35]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[36]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[37]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[38]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[39]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[40]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[41]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[42]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[43]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[44]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[45]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[46]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[47]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[48]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[49]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[50]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[51]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[52]")).ToString("### ###")%></td>
                        <td class="ReportTable"><%#Convert.ToInt32(Eval("CountPupils[53]")).ToString("### ###")%></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                </table>
                </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:ObjectDataSource runat="server" ID="dsCity" TypeName="Udod.Dal.DictCityDb" SelectMethod="GetCities"></asp:ObjectDataSource>
</asp:Content>
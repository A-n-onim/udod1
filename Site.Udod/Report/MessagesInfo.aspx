﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="MessagesInfo.aspx.cs" Inherits="Report_MessagesInfo" %>

<asp:Content runat="server" ID="contentMessages" ContentPlaceHolderID="body">
    <asp:GridView runat="server" ID="gvMessages" CssClass="dgc" AutoGenerateColumns="false" DataSourceID="dsMessages">
        <EmptyDataTemplate>
            <asp:Label ID="lblEmptyGvPupilList" runat="server" style="font-size:16px">Сообщений нет</asp:Label>
        </EmptyDataTemplate>
        <Columns>
            <asp:TemplateField HeaderText="№" >
                <ItemTemplate><%# gvMessages.Rows.Count+1%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Дата сообщения" >
                <ItemTemplate>
                    <%# ((DateTime)Eval("CreatedDate")).ToShortDateString() + " " + ((DateTime)Eval("CreatedDate")).ToShortTimeString()%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Сообщение" >
                <ItemTemplate>
                    <%# Eval("Message")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource runat="server" ID="dsMessages" TypeName="Udod.Dal.MessageDb" SelectMethod="GetUserMessage">
        <SelectParameters>
            <asp:ProfileParameter runat="server" PropertyName="UserId" Name="userId" DbType="Guid"/>
            <asp:Parameter runat="server" Name="viewAll" DbType="Boolean" DefaultValue="true"/>
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
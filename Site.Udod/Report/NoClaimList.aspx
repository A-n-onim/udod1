﻿<%@ Page Language="C#" MasterPageFile="../Master/MasterPage.master" AutoEventWireup="true" CodeFile="NoClaimList.aspx.cs" Inherits="Reports_NoClaimList" %>
<%@ Register src="../UserControl/Filter/ucCityUdodFilter.ascx" tagName="CityUdodFilter" tagPrefix="uct" %>

<asp:Content runat="server" ID="ContentClaimNumber" ContentPlaceHolderID="body">
    <asp:UpdatePanel ID="upCityUdodFilter" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <uct:CityUdodFilter ID="cityUdodFilter" runat="server" UdodDdlEnabled="true" AddAllProperty="true" OnCityChange="UdodChange" OnUdodChange="UdodChange" onloadComplete="UdodChange"/>
    </ContentTemplate>
    </asp:UpdatePanel>
    <br/>
    
    <table>
        <tr valign="top">
            <td>
                <asp:UpdatePanel runat="server" ID="upChildUnionGV" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:GridView runat="server" ID="gvChildUnionClaimNumber" CssClass="dgc"  AutoGenerateColumns="false">
                        <EmptyDataTemplate>
                            <asp:Label ID="lblEmptyGvChildUnionClaimNumber" runat="server" style="font-size:16px">Нет пустых детских объединений</asp:Label>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="№" >
                                <ItemTemplate><%# (gvChildUnionClaimNumber.Rows.Count + 1)%></ItemTemplate>
                            </asp:TemplateField>
                        
                            <asp:TemplateField HeaderText="Детское объединение">
                                <ItemTemplate>
                                    <%# Eval("ChildUnionName") %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Кол-во заявок">
                                <ItemTemplate>
                                    <%# Eval("ChildUnionClaimNum")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel runat="server" ID="upSectionGV" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:GridView runat="server" ID="gvSectionClaimCount" CssClass="dgc"  AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField HeaderText="№" >
                                <ItemTemplate><%# (gvSectionClaimCount.Rows.Count + 1)%></ItemTemplate>
                            </asp:TemplateField>
                        
                            <asp:TemplateField HeaderText="Вид деятельности">
                                <ItemTemplate>
                                    <%# Eval("ChildUnionSection") %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Кол-во заявок">
                                <ItemTemplate>
                                    <%# Eval("ChildUnionClaimNum")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel runat="server" ID="upProfileGV" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:GridView runat="server" ID="gvProfileClaimCount" CssClass="dgc"  AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField HeaderText="№" >
                                <ItemTemplate><%# (gvProfileClaimCount.Rows.Count + 1)%></ItemTemplate>
                            </asp:TemplateField>
                        
                            <asp:TemplateField HeaderText="Профиль">
                                <ItemTemplate>
                                    <%# Eval("ChildUnionProfile") %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Кол-во заявок">
                                <ItemTemplate>
                                    <%# Eval("ChildUnionClaimNum")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel runat="server" ID="upProgramGV" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:GridView runat="server" ID="gvProgramClaimCount" CssClass="dgc"  AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField HeaderText="№" >
                                <ItemTemplate><%# (gvProgramClaimCount.Rows.Count + 1)%></ItemTemplate>
                            </asp:TemplateField>
                        
                            <asp:TemplateField HeaderText="Направленность">
                                <ItemTemplate>
                                    <%# Eval("ChildUnionProgram") %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Кол-во заявок">
                                <ItemTemplate>
                                    <%# Eval("ChildUnionClaimNum")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    
    <asp:LinkButton runat="server" ID="lnkPrint" OnClick="lnkPrint_Click"><div class="btnBlue">Сохранить в .xls</div></asp:LinkButton>
</asp:Content>
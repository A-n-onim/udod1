﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Report_ReportForOsip : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsOsip || UserContext.Roles.IsUdodEmployee || UserContext.Roles.IsDepartment);
        CityUdod.Visible = UserContext.Roles.RoleId == (int) EnumRoles.Administrator;
        if (!Page.IsPostBack)
        {
            if (!UserContext.Roles.IsAdministrator) DatabindReport();
        }
    }

    private void DatabindReport()
    {
        using (ReportDb db = new ReportDb())
        {
            int? cityId;
            if (!UserContext.CityId.HasValue)
            {
                cityId = CityUdod.SelectedCity;
            }
            else
            {
                cityId = UserContext.CityId;
            }

            int? LLfrom, LLto, LIWfrom, LIWto;
            try { LLfrom = Convert.ToInt32(tbLLFrom.Text); }
            catch { LLfrom = null; }

            try { LLto = Convert.ToInt32(tbLLTo.Text); }
            catch { LLto = null; }

            try { LIWfrom = Convert.ToInt32(tbLIWFrom.Text); }
            catch { LIWfrom = null; }

            try { LIWto = Convert.ToInt32(tbLIWTo.Text); }
            catch { LIWto = null; }

            //if (cityId.HasValue || UserContext.UdodId.HasValue)
            {
                var list = db.GetReportLesson(cityId, LLfrom, LLto, LIWfrom, LIWto, UserContext.Roles.RoleId == (int)EnumRoles.Osip ? null : UserContext.UdodId);
                gvAgeGroupList.DataSource = list;
                gvAgeGroupList.DataBind();
            }
        }
    }

    protected void lnkExportInExcel_OnClick(object sender, EventArgs e)
    {
        int? cityId;
        if (!UserContext.CityId.HasValue)
        {
            cityId = CityUdod.SelectedCity;
        }
        else
        {
            cityId = UserContext.CityId;
        }
        //if (cityId.HasValue || UserContext.UdodId.HasValue)
        {
            HttpContext.Current.Response.Clear();

            string filename = string.Format("Report.xls");

            HttpContext.Current.Response.AddHeader("content-disposition",
                                                    string.Format("attachment; filename={0}", filename));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
            Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];

            #region Стили

            Aspose.Cells.Style style_area = workbook.Styles[0];
            Aspose.Cells.Style style_header = workbook.Styles[1];
            style_area.ShrinkToFit = true;

            style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle =
                Aspose.Cells.CellBorderType.Thin;
            style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
            style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle =
                Aspose.Cells.CellBorderType.Thin;
            style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
            style_header.Font.IsBold = true;
            style_header.Font.Size = 12;
            style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
            style_header.BackgroundColor = System.Drawing.Color.Gray;
            style_header.Pattern = Aspose.Cells.BackgroundType.Solid;

            #endregion

            long? udodId = null;
            if (UserContext.Roles.RoleId==(int)EnumRoles.UdodEmployee)
            {
                udodId = UserContext.UdodId;
            }

            using (ReportDb db = new ReportDb())
            {
                int? LLfrom, LLto, LIWfrom, LIWto;
                try { LLfrom = Convert.ToInt32(tbLLFrom.Text); }
                catch { LLfrom = null; }

                try { LLto = Convert.ToInt32(tbLLTo.Text); }
                catch { LLto = null; }

                try { LIWfrom = Convert.ToInt32(tbLIWFrom.Text); }
                catch { LIWfrom = null; }

                try { LIWto = Convert.ToInt32(tbLIWTo.Text); }
                catch { LIWto = null; }

                var list = db.GetReportLesson(cityId, LLfrom, LLto, LIWfrom, LIWto, udodId);
                int i = 0;
                sheet.Cells[i, 0].PutValue("Округ");
                sheet.Cells[i, 1].PutValue("Учреждение");
                sheet.Cells[i, 2].PutValue("Детское Объединение");
                sheet.Cells[i, 3].PutValue("Продолжительность занятия");
                sheet.Cells[i, 4].PutValue("Количество занятий в неделю");
                i++;
                foreach (var item in list)
                {
                    sheet.Cells[i, 0].PutValue(item.CityName);
                    sheet.Cells[i, 1].PutValue(item.UdodName);
                    sheet.Cells[i, 2].PutValue(item.ChildUnionName);
                    sheet.Cells[i, 3].PutValue(item.LessonLength);
                    sheet.Cells[i, 4].PutValue(item.LessonsInWeek);
                    i++;
                }
                sheet.Cells.CreateRange(0, 0, i, 5).ApplyStyle(style_area,
                                                                new Aspose.Cells.StyleFlag() {All = true});
            }
            sheet.AutoFitColumns();
            workbook.Save("Report.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel,
                            Response);
            HttpContext.Current.Response.End();
        }
    }
    
    protected void CityUdod_OnCityChange(object sender, EventArgs e)
    {
        DatabindReport();
    }

    protected void lnkSearch_OnClick(object sender, EventArgs e)
    {
        DatabindReport();
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="TransferReportForOsip.aspx.cs" Inherits="Report_ReportForOsip" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Register src="../UserControl/Filter/ucCityUdodFilter.ascx" tagName="CityUdodFilter" tagPrefix="uct" %>

<asp:Content runat="server" ID="contentReport" ContentPlaceHolderID="body">
    <uct:CityUdodFilter runat="server" ID="CityUdodFilter" UdodDdlEnabled="false" AddAllProperty="true" />
    
    <asp:LinkButton runat="server" ID="lnkExportInExcel" OnClick="lnkExportInExcel_OnClick"><div class="btnBlue">Экспорт в Excel</div></asp:LinkButton>
    
    <asp:UpdatePanel runat="server" ID="upTransferReportList" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView runat="server" ID="gvTransferReportList" CssClass="dgc" AutoGenerateColumns="false" DataSourceID="dsTransferReport" OnRowCreated="gvTransferReportList_RowCreated" >
                <EmptyDataTemplate>
                    <asp:Label ID="lblEmptyGvTransferReportList" runat="server" style="font-size:16px">Учреждения не найдены</asp:Label>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="№" >
                        <ItemTemplate><%# (gvTransferReportList.Rows.Count + 1)%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Учреждение" >
                        <ItemTemplate>
                            <%# Eval("ShortName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Есть блок ДО" >
                        <ItemTemplate>
                            <%# Eval("IsDOEnabled")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Всего зачислений" >
                        <ItemTemplate>
                            <%# Eval("EnrollSum")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Заканчивает обучение" >
                        <ItemTemplate>
                            <%# Eval("EndOfStudy")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Будет переведен" >
                        <ItemTemplate>
                            <%# Eval("WillBeTransfer")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Повторит программу" >
                        <ItemTemplate>
                            <%# Eval("Repear")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Будет отчислен" >
                        <ItemTemplate>
                            <%# Eval("WillBeDismissed")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Нет возрастной группы" >
                        <ItemTemplate>
                            <%# Eval("AgeGroupNotExist")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Будет перебор часов" >
                        <ItemTemplate>
                            <%# Eval("TooMuchHours")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="CityUdodFilter"/>
        </Triggers>
    </asp:UpdatePanel>

    <asp:ObjectDataSource runat="server" ID="dsTransferReport" TypeName="Udod.Dal.ReportDb" SelectMethod="GetReportTransfer"  >
        <SelectParameters>
            <asp:ControlParameter Name="cityId" ControlID="cityUdodFilter" PropertyName="SelectedCity" ConvertEmptyStringToNull="true" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

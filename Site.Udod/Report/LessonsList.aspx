﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="LessonsList.aspx.cs" Inherits="Report_ReportForOsip" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Register src="../UserControl/Filter/ucCityUdodFilter.ascx" tagName="CityUdodFilter" tagPrefix="uct" %>

<asp:Content runat="server" ID="contentReport" ContentPlaceHolderID="body">
    <uct:CityUdodFilter runat="server" ID="CityUdod" UdodDdlEnabled="true" AddAllProperty="true"  OnCityChange="CityUdod_OnCityChange" OnUdodChange="CityUdod_OnCityChange" OnloadComplete="CityUdod_OnCityChange"/>
    <asp:Panel ID="Panel2" runat="server" DefaultButton="lnkSearch">
    <table>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblLLFrom" Text="Продолжительность занятия " />
            </td>
            <td>
                ≥
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbLLFrom" CssClass="input" Text="0"/>
            </td>
            <td>
                <asp:Label runat="server" ID="lblLLTo" Text="≤" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbLLTo" CssClass="input" Text="0" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblLIWFrom" Text="Количество занятий в неделю " />
            </td>
            <td>
                ≥
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbLIWFrom" CssClass="input" Text="0" />
            </td>
            <td>
                <asp:Label runat="server" ID="lblLIWTo" Text="≤" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbLIWTo" CssClass="input" Text="0"/>
            </td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton runat="server" ID="lnkSearch" OnClick="lnkSearch_OnClick"><div class="btnBlue">Поиск</div></asp:LinkButton>
            </td>
        </tr>
    </table>
    </asp:Panel>
    
    <asp:LinkButton runat="server" ID="lnkExportInExcel" OnClick="lnkExportInExcel_OnClick"><div class="btnBlue">Экспорт в Excel</div></asp:LinkButton>
    
    <asp:UpdatePanel runat="server" ID="upAgeGroupList" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView runat="server" ID="gvAgeGroupList" CssClass="dgc" AutoGenerateColumns="false" >
                <EmptyDataTemplate>
                    <asp:Label ID="lblEmptyGvAgeGroupList" runat="server" style="font-size:16px">Возрастные группы не найдены</asp:Label>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="№" >
                        <ItemTemplate><%# (gvAgeGroupList.Rows.Count + 1)%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Округ" >
                        <ItemTemplate>
                            <%# Eval("CityName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Учреждение" >
                        <ItemTemplate>
                            <%# Eval("UdodName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Детское объединение" >
                        <ItemTemplate>
                            <%# Eval("ChildUnionName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Продолжительность занятия" >
                        <ItemTemplate>
                            <%# Eval("LessonLength")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Количество занятий в неделю" >
                        <ItemTemplate>
                            <%# Eval("LessonsInWeek")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="CityUdod"/>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

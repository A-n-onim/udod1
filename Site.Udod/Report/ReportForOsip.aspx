﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="ReportForOsip.aspx.cs" Inherits="Report_ReportForOsip" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Register src="../UserControl/Filter/ucCityUdodFilter.ascx" tagName="CityUdodFilter" tagPrefix="uct" %>

<asp:Content runat="server" ID="contentReport" ContentPlaceHolderID="body">
    <uct:CityUdodFilter runat="server" ID="CityUdod" UdodDdlEnabled="true" AddAllProperty="true" RemoveAllCityProperty="true" OnCityChange="CityUdod_OnCityChange" OnUdodChange="CityUdod_OnCityChange" OnloadComplete="CityUdod_OnCityChange"/>
    <table>
        <tr runat="server" ID="trDdlTypeUdod">
            <td>Выбор типа учреждения: </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlTypeUdod" AutoPostBack="true" OnSelectedIndexChanged="ddlTypeUdod_OnSelectedIndexChanged">
                    <Items>
                        <asp:ListItem runat="server" Value="0">УДО</asp:ListItem>
                        <asp:ListItem runat="server" Value="1">ОУ</asp:ListItem>
                        <asp:ListItem runat="server" Value="2">ДОУ</asp:ListItem>
                        <asp:ListItem runat="server" Value="3">СПО</asp:ListItem>
                    </Items>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    
    <asp:LinkButton runat="server" ID="lnkExportInExcel" OnClick="lnkExportInExcel_OnClick"><div class="btnBlue">Экспорт в Excel</div></asp:LinkButton>
    <asp:Repeater runat="server" ID="repReport" >
        <HeaderTemplate>
            <table border="1px;" class="ReportTable" cellspacing="0px" style="width: 1000px;">
                <tr class="reportHeader">
                    <td rowspan=2>Наименование учреждения</td>
                    <td rowspan=2>Есть БДО</td>
                    <td colspan=2>1 год</td>
                    <td colspan=2>2 год</td>
                    <td colspan=2>3 год</td>
                    <td colspan=2>4 год</td>
                    <td colspan=2>5 год</td>
                    <td colspan=2>6 год</td>
                    <td colspan=2>7 год</td>
                    <td colspan=2>8 год</td>
                    <td colspan=2>9 год</td>
                    <td colspan=2>10 год</td>
                    <td colspan=2>11 год</td>
                    <td colspan=3>Итого</td>
                </tr>
                <tr class="reportHeader">
                    <td>Платно</td>
                    <td>Беспл.</td>
                    <td>Платно</td>
                    <td>Беспл.</td>
                    <td>Платно</td>
                    <td>Беспл.</td>
                    <td>Платно</td>
                    <td>Беспл.</td>
                    <td>Платно</td>
                    <td>Беспл.</td>
                    <td>Платно</td>
                    <td>Беспл.</td>
                    <td>Платно</td>
                    <td>Беспл.</td>
                    <td>Платно</td>
                    <td>Беспл.</td>
                    <td>Платно</td>
                    <td>Беспл.</td>
                    <td>Платно</td>
                    <td>Беспл.</td>
                    <td>Платно</td>
                    <td>Беспл.</td>
                    <td>Платно</td>
                    <td>Беспл.</td>
                    <td>Общее</td>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><%#Eval("NameUdod")%></td>
                <td><%#Eval("IsDOEnabled")%></td>
                <td><%#Convert.ToInt32(Eval("Years[0].FeeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[0].FreeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[1].FeeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[1].FreeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[2].FeeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[2].FreeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[3].FeeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[3].FreeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[4].FeeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[4].FreeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[5].FeeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[5].FreeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[6].FeeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[6].FreeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[7].FeeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[7].FreeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[8].FeeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[8].FreeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[9].FeeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[9].FreeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[10].FeeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Years[10].FreeEnrollCount")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("SumFee")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("SumFree")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
                <td><%#Convert.ToInt32(Eval("Sum")).ToString("### ###", CultureInfo.InvariantCulture)%></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>

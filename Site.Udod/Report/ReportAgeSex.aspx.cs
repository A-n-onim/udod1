﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class Report_ReportAgeSex : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator|| UserContext.Roles.IsUdodEmployee);
        if (!Page.IsPostBack)
        {
            td1.Visible =
                td2.Visible = td3.Visible = td4.Visible = td5.Visible = td6.Visible = UserContext.Roles.IsAdministrator;
        }
    }

    protected void cbTypeUdod_OnCheckedChanged(object sender, EventArgs e)
    {
        cblUdodType.Enabled = cbTypeUdod.Checked;
    }

    protected void cblCity_OnDataBound(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            foreach (ListItem item in cblCity.Items)
            {
                item.Selected = true;
            }
        }
    }

    protected void lnkFind_OnClick(object sender, EventArgs e)
    {
        using (var db = new ReportDb())
        {
            var sb = new MyStringBuilder(", ");

            if (cbTypeUdod.Checked)
            {
                foreach (ListItem item in cblUdodType.Items)
                {
                    if (item.Selected)
                    {
                        sb.Append(item.Value);
                    }
                }
            }
            var sb1 = new MyStringBuilder(", ");
            if (cbCity.Checked)
            {
                foreach (ListItem item in cblCity.Items)
                {
                    if (item.Selected)
                    {
                        sb1.Append(item.Value);
                    }
                }
            }
            long? udodid = null;
            try
            {
                udodid = UserContext.Roles.IsAdministrator ? Convert.ToInt64(ddlUdod.SelectedValue) : UserContext.UdodId;
                
            }
            catch (Exception)
            {
            }
            
            repReport.DataSource = db.GetAgeSexReport(sb.ToString(), sb1.ToString(), udodid,
                                                      Convert.ToBoolean(rblDistinct.SelectedValue));
            repReport.DataBind();
        }
        upReport.Update();
    }


    protected void cbCity_OnCheckedChanged(object sender, EventArgs e)
    {
        cblCity.Enabled = cbCity.Checked;
    }

    protected void repReport_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
    }

    protected void tbUdodFilter_OnTextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(tbUdodFilter.Text))
        {
            using (var db = new UdodDb())
            {
                var sb = new MyStringBuilder(", ");

                if (cbTypeUdod.Checked)
                {
                    foreach (ListItem item in cblUdodType.Items)
                    {
                        if (item.Selected)
                        {
                            sb.Append(item.Value);
                        }
                    }
                }
                var sb1 = new MyStringBuilder(", ");
                if (cbCity.Checked)
                {
                    foreach (ListItem item in cblCity.Items)
                    {
                        if (item.Selected)
                        {
                            sb1.Append(item.Value);
                        }
                    }
                }

                List<ExtendedUdod> list = db.GetUdodsForReport(sb1.ToString(), tbUdodFilter.Text, sb.ToString());
                if (list.Count == 0) list.Add(new ExtendedUdod {UdodId = -1, ShortName = "Учреждение не найдено"});
                ddlUdod.DataSource = list;
                ddlUdod.DataBind();
            }
        }
        else
        {
            ddlUdod.Items.Clear();
        }
    }

    protected void cblCity_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        tbUdodFilter.Text = "";
        tbUdodFilter_OnTextChanged(tbUdodFilter, new EventArgs());
    }

    protected void cblUdodType_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        tbUdodFilter.Text = "";
        tbUdodFilter_OnTextChanged(tbUdodFilter, new EventArgs());
    }

    protected void lnkExport_OnClick(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("Report.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                               string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        var workbook = new Aspose.Cells.Workbook();
        workbook.Open(MapPath("../Templates/ReportAgeSex.xlsx"));
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];

        #region Стили

        Aspose.Cells.Style style_area = workbook.Styles[0];
        Aspose.Cells.Style style_header = workbook.Styles[1];
        style_area.ShrinkToFit = true;

        style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.Black;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.Black;
        style_header.Font.IsBold = true;
        style_header.Font.Size = 12;
        style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
        style_header.BackgroundColor = Color.Gray;
        style_header.Pattern = Aspose.Cells.BackgroundType.Solid;

        #endregion

        using (var db = new ReportDb())
        {
            var sb = new MyStringBuilder(", ");

            if (cbTypeUdod.Checked)
            {
                foreach (ListItem item in cblUdodType.Items)
                {
                    if (item.Selected)
                    {
                        sb.Append(item.Value);
                    }
                }
            }
            var sb1 = new MyStringBuilder(", ");
            if (cbCity.Checked)
            {
                foreach (ListItem item in cblCity.Items)
                {
                    if (item.Selected)
                    {
                        sb1.Append(item.Value);
                    }
                }
            }
            long? udodid = null;
            try
            {
                udodid = UserContext.Roles.IsAdministrator ? Convert.ToInt64(ddlUdod.SelectedValue) : UserContext.UdodId;
            }
            catch (Exception)
            {
            }

            var list = db.GetAgeSexReport(sb.ToString(), sb1.ToString(), udodid,
                                                      Convert.ToBoolean(rblDistinct.SelectedValue));


            int k = 3;
            foreach (var report in list)
            {
                sheet.Cells[k, 0].PutValue(report.ProgramName);
                sheet.Cells[k, 1].PutValue(report.TypeBudgetName);
                for (int i1 = 0; i1 < 54; i1++)
                {
                    sheet.Cells[k, i1 + 2].PutValue(report.CountPupils[i1]);
                }
                k++;
            }
            sheet.Cells.CreateRange(3, 0, list.Count, 56).ApplyStyle(style_area, new Aspose.Cells.StyleFlag { All = true });
        }
        workbook.Save("ReportAgeSex.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }
}
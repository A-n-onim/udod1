﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class Report_ReportClaimCount : BasePage
{
    private int countrow = 0;
    private int countrow1 = 0;
    private int countrow2 = 0;
    private int countrow3 = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsDepartment);
    }

    protected void gvReportClaimCount_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType==DataControlRowType.DataRow)
        {
            ExtendedReportClaim item = (ExtendedReportClaim) e.Row.DataItem;
            if (!string.IsNullOrEmpty(item.UdodName))
            {
                e.Row.Attributes.Add("rayonId", item.RayonId.ToString());
                e.Row.Attributes.Add("cityId1", item.CityId.ToString());
                e.Row.Style.Add("display", "none");
                e.Row.Cells[0].Text = item.UdodName;
                if (countrow2 == 0)
                {
                    e.Row.Cells[0].Attributes.Add("rowspan", "10");
                    e.Row.Cells[0].CssClass = "UdodCell";
                    countrow2 = 10;
                }
                else
                {
                    e.Row.Cells.RemoveAt(0);
                }
                countrow2--;
            }
            else
            {
                if (!string.IsNullOrEmpty(item.RayonName))
                {
                    e.Row.Attributes.Add("cityId", item.CityId.ToString());
                    e.Row.Style.Add("display", "none");
                    e.Row.Cells[0].Text = item.RayonName;
                    e.Row.Cells[0].Attributes.Add("mainrayonId", item.RayonId.ToString());
                    e.Row.Cells[0].Attributes.Add("isView", "0");
                    if (countrow1 == 0)
                    {
                        e.Row.Cells[0].Attributes.Add("rowspan", "10");
                        e.Row.Cells[0].CssClass = "RayonCell";
                        countrow1 = 10;
                    }
                    else
                    {
                        e.Row.Cells.RemoveAt(0);
                    }
                    countrow1--;
                }
                else
                {
                    if (countrow == 0)
                    {
                        e.Row.Cells[0].Attributes.Add("mainCityId", item.CityId.ToString());
                        e.Row.Cells[0].Attributes.Add("isView", "0");
                        e.Row.Cells[0].Attributes.Add("rowspan", "10");
                        e.Row.Cells[0].CssClass = "AOCell";
                        countrow = 10;
                    }
                    else
                    {
                        e.Row.Cells.RemoveAt(0);
                    }

                    countrow--;
                }
            }
        }
    }
}

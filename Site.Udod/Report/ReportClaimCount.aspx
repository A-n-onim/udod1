﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportClaimCount.aspx.cs" MasterPageFile="../Master/MasterPage.master" Inherits="Report_ReportClaimCount" %>

<asp:Content runat="server" ID="reportContent" ContentPlaceHolderID="body">
<script type="text/javascript">
    /*$(document).ready(function () {
        $("td[mainCityId]").each(function() {
            $(this).bind("click", openrow);
        });
        $("td[mainrayonId]").each(function () {
            $(this).bind("click", openrowudod);
        });
    });*/
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(bindonload);
    function bindonload(sender, args) {
        $("td[mainCityId]").each(function () {
            $(this).bind("click", openrow);
        });
        $("td[mainrayonId]").each(function () {
            $(this).bind("click", openrowudod);
        });
    }
    
    function openrowudod() {
        var isview = $(this).attr('isView');
        var rayonid = $(this).attr('mainrayonId');
        if (isview == '0') {
            $("tr[rayonId=" + rayonid + "]").each(function () {
                $(this).css("display", "");
            });
            $(this).attr('isView', '1');
        }
        else {
            $("tr[rayonId=" + rayonid + "]").each(function () {
                $(this).css("display", "none");
            });
            $(this).attr('isView', '0');
        }
    }
    
    function openrow() {
        var isview = $(this).attr('isView');
        var cityid = $(this).attr('mainCityId');
        if (isview == '0') {
            $("tr[cityId=" + cityid + "]").each(function () {
                $(this).css("display", "");
            });
            $(this).attr('isView','1');
        }
        else {
            $("tr[cityId=" + cityid + "]").each(function () {
                $(this).css("display", "none");
                $(this).find('td[isView]').each(function () {
                    $(this).attr('isView', '0');
                }); 
            });
            $("tr[cityId1=" + cityid + "]").each(function () {
                $(this).css("display", "none");
                $(this).attr('isView', '0');
            });
            $(this).attr('isView', '0');
        }
    }
</script>

<asp:GridView runat="server" ID="gvReportClaimCount" AutoGenerateColumns="false" DataSourceID="dsreport" OnRowDataBound="gvReportClaimCount_OnRowDataBound" >
    <Columns>
        <asp:TemplateField HeaderText="Округ" >
            <ItemTemplate>
                <%# Eval("CityName")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Операция" >
            <ItemTemplate>
                <%# Eval("TypeMessage").ToString() == "0" ? "Подано заявлений через портал гос.услуг" : Eval("TypeMessage").ToString() == "1" ? "На рассмотрении" : Eval("TypeMessage").ToString() == "2" ? "Подано заявлений через сотрудника ОСИП" : Eval("TypeMessage").ToString() == "3" ? "Подано заявлений через сотрудника УДО" : Eval("TypeMessage").ToString() == "4" ? "Зачислено" : Eval("TypeMessage").ToString() == "5" ? "Аннулировано" : Eval("TypeMessage").ToString() == "6" ? "Резерв" : Eval("TypeMessage").ToString() == "7" ? "Поданы документы" : Eval("TypeMessage").ToString() == "8" ? "Заявителей обратилось" : "Заявителей зачислено"%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Январь" >
            <ItemTemplate>
                <%# Eval("Count1")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Февраль" >
            <ItemTemplate>
                <%# Eval("Count2")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Март" >
            <ItemTemplate>
                <%# Eval("Count3")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Апрель" >
            <ItemTemplate>
                <%# Eval("Count4")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Май" >
            <ItemTemplate>
                <%# Eval("Count5")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Июнь" >
            <ItemTemplate>
                <%# Eval("Count6")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Июль" >
            <ItemTemplate>
                <%# Eval("Count7")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Август" >
            <ItemTemplate>
                <%# Eval("Count8")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Сентябрь" >
            <ItemTemplate>
                <%# Eval("Count9")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Октябрь" >
            <ItemTemplate>
                <%# Eval("Count10")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Ноябрь" >
            <ItemTemplate>
                <%# Eval("Count11")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Декабрь" >
            <ItemTemplate>
                <%# Eval("Count12")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Итого" >
            <ItemTemplate>
                <%# Eval("CountAll")%>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:ObjectDataSource runat="server" ID="dsreport" TypeName="Udod.Dal.ReportDb" SelectMethod="GetReportClaimCount">

</asp:ObjectDataSource>
</asp:Content>
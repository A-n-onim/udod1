﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Reports_ClaimNumber : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsDepartment);

        if (!Page.IsPostBack)
        {
            List<ExtendedReport> cityList;
            List<ExtendedReport> rayonList;
            List<ExtendedReport> fullList = new List<ExtendedReport>();
            using (ReportDb db = new ReportDb())
            {
                cityList = db.GetCityClaimCount(null);

                rayonList = db.GetRayonClaimCount(null);

                foreach (var city in cityList)
                {
                    fullList.Add(city);

                    var currentCityRayonList = rayonList.Where(i => i.CityName == city.CityName).ToList();

                    fullList.AddRange(currentCityRayonList);
                }

                gvClaimNumber.DataSource = fullList;
                gvClaimNumber.DataBind();
            }
        }
    }

    protected void lnkPrint_Click(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("CityClaimCount.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                                string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
        //workbook.Open(MapPath("../Templates/ClaimTemplate.xlsx"));
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];

        List<ExtendedReport> cityList;
        List<ExtendedReport> rayonList;
        List<ExtendedReport> fullList = new List<ExtendedReport>();
        using (ReportDb db = new ReportDb())
        {
            cityList = db.GetCityClaimCount(null);

            rayonList = db.GetRayonClaimCount(null);

            Aspose.Cells.Style style_area = workbook.Styles[0];
            style_area.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
            style_area.ShrinkToFit = true;

            style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
            style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;

            Aspose.Cells.Cells cells = sheet.Cells;
            cells.Merge(0, 0, 1, 10);
            cells.Merge(1, 0, 1, 10);
            sheet.Cells[0, 0].PutValue("Количество заявок по Округам/Районам");
            sheet.Cells[1, 0].PutValue("Дата: " + DateTime.Today.ToString().Substring(0, 10));
            Aspose.Cells.Style style = sheet.Cells[0, 0].GetStyle();
            Aspose.Cells.Font font = style.Font;
            font.Size = 18;
            font.IsBold = true;
            cells[0, 0].SetStyle(style);
            cells.SetRowHeight(0, 20);
            cells.SetRowHeight(1, 20);
            cells[1, 0].SetStyle(style);

            int offset = 3;

            int row = 1;
            sheet.Cells[offset, 0].PutValue("№");
            sheet.Cells[offset, 1].PutValue("Округ");
            sheet.Cells[offset, 2].PutValue("Район");
            sheet.Cells[offset, 3].PutValue("Кол-во заявок");
            foreach (var city in cityList)
            {
                sheet.Cells[row + offset, 0].PutValue(row);
                sheet.Cells[row + offset, 1].PutValue(city.CityName);
                sheet.Cells[row + offset, 3].PutValue(city.ChildUnionClaimNum);
                
                var currentCityRayonList = rayonList.Where(i => i.CityName == city.CityName).ToList();
                
                row++;

                foreach (var rayon in currentCityRayonList)
                {
                    sheet.Cells[row + offset, 0].PutValue(row);
                    sheet.Cells[row + offset, 1].PutValue(city.CityName);
                    sheet.Cells[row + offset, 2].PutValue(rayon.UdodRayon);
                    sheet.Cells[row + offset, 3].PutValue(rayon.ChildUnionClaimNum);

                    row++;
                }
            }
            sheet.Cells.CreateRange(offset, 0, row, 4).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
        }
        sheet.AutoFitColumns();

        //sheet.Cells.CreateRange(0, 0, row, 11).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
        workbook.Save("CityClaimCount.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Report_ReportForOsip : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsOsip || UserContext.Roles.IsUdodEmployee || UserContext.Roles.IsDepartment);
        CityUdod.Visible = UserContext.Roles.RoleId == (int) EnumRoles.Administrator;
        if (!Page.IsPostBack)
        {

            ddlTypeUdod.Visible = trDdlTypeUdod.Visible = (UserContext.Roles.RoleId != (int)EnumRoles.UdodEmployee);
            
            if (!UserContext.Roles.IsAdministrator) DatabindReport();
        }
    }

    private void DatabindReport()
    {
        using (ReportDb db = new ReportDb())
        {
            int? cityId;
            if (!UserContext.CityId.HasValue)
            {
                cityId = CityUdod.SelectedCity;
            }
            else
            {
                cityId = UserContext.CityId;
            }
            if (cityId.HasValue || UserContext.UdodId.HasValue)
            {
                var list = db.GetReportForOsip(cityId, Convert.ToInt32(ddlTypeUdod.SelectedValue), UserContext.Roles.RoleId==(int)EnumRoles.Osip?null:UserContext.UdodId);
                repReport.DataSource = list;
                repReport.DataBind();
            }
        }
    }

    protected void lnkExportInExcel_OnClick(object sender, EventArgs e)
    {
        int? cityId;
        if (!UserContext.CityId.HasValue)
        {
            cityId = CityUdod.SelectedCity;
        }
        else
        {
            cityId = UserContext.CityId;
        }
        if (cityId.HasValue || UserContext.UdodId.HasValue)
        {
            HttpContext.Current.Response.Clear();

            string filename = string.Format("Report.xls");

            HttpContext.Current.Response.AddHeader("content-disposition",
                                                    string.Format("attachment; filename={0}", filename));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
            Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];

            #region Стили

            Aspose.Cells.Style style_area = workbook.Styles[0];
            Aspose.Cells.Style style_header = workbook.Styles[1];
            style_area.ShrinkToFit = true;

            style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle =
                Aspose.Cells.CellBorderType.Thin;
            style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
            style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle =
                Aspose.Cells.CellBorderType.Thin;
            style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
            style_header.Font.IsBold = true;
            style_header.Font.Size = 12;
            style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
            style_header.BackgroundColor = System.Drawing.Color.Gray;
            style_header.Pattern = Aspose.Cells.BackgroundType.Solid;

            #endregion

            long? udodId = null;
            if (UserContext.Roles.RoleId==(int)EnumRoles.UdodEmployee)
            {
                udodId = UserContext.UdodId;
            }

            using (ReportDb db = new ReportDb())
            {
                var list = db.GetReportForOsip(cityId, Convert.ToInt32(ddlTypeUdod.SelectedValue), udodId);
                int i = 0;
                sheet.Cells[i, 0].PutValue("Наименование учреждения");
                sheet.Cells[i, 1].PutValue("Есть БДО");
                sheet.Cells[i, 2].PutValue("1 год");
                sheet.Cells[i, 5].PutValue("2 год");
                sheet.Cells[i, 8].PutValue("3 год");
                sheet.Cells[i, 11].PutValue("4 год");
                sheet.Cells[i, 14].PutValue("5 год");
                sheet.Cells[i, 17].PutValue("6 год");
                sheet.Cells[i, 20].PutValue("7 год");
                sheet.Cells[i, 23].PutValue("8 год");
                sheet.Cells[i, 26].PutValue("9 год");
                sheet.Cells[i, 29].PutValue("10 год");
                sheet.Cells[i, 32].PutValue("11 год");
                sheet.Cells[i, 35].PutValue("Итого");
                sheet.Cells.Merge(0, 0, 2, 1);
                sheet.Cells.Merge(0, 1, 2, 1);
                //sheet.Cells.Merge(0, 35, 2, 1);
                i++;
                for (int j = 0; j <= 11; j++)
                {
                    sheet.Cells[i, 3 * j + 2].PutValue("Платно");
                    sheet.Cells[i, 3 * j + 3].PutValue("Бесплатно");
                    sheet.Cells[i, 3 * j + 4].PutValue("Общее");
                    sheet.Cells.Merge(0, 3 * j + 2, 1, 3);
                }
                i++;
                foreach (var udod in list)
                {
                    sheet.Cells[i, 0].PutValue(udod.NameUdod);
                    sheet.Cells[i, 1].PutValue(udod.IsDOEnabled);

                    for (int j = 0; j <= 10; j++)
                    {
                        sheet.Cells[i, 3 * j + 2].PutValue(udod.Years[j].FeeEnrollCount);
                        sheet.Cells[i, 3 * j + 3].PutValue(udod.Years[j].FreeEnrollCount);
                        sheet.Cells[i, 3 * j + 4].PutValue(udod.Years[j].AllEnrollCount);
                    }

                    sheet.Cells[i, 35].PutValue(udod.SumFee);
                    sheet.Cells[i, 36].PutValue(udod.SumFree);
                    sheet.Cells[i, 37].PutValue(udod.Sum);

                    i++;
                }
                sheet.Cells.CreateRange(0, 0, i, 38).ApplyStyle(style_area,
                                                                new Aspose.Cells.StyleFlag() {All = true});
            }
            sheet.AutoFitColumns();
            workbook.Save("Report.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel,
                            Response);
            HttpContext.Current.Response.End();
        }
    }

    protected void ddlTypeUdod_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DatabindReport();
    }

    protected void CityUdod_OnCityChange(object sender, EventArgs e)
    {
        DatabindReport();
    }
}
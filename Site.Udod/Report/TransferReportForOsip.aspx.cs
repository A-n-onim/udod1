﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Report_ReportForOsip : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsOsip);
        CityUdodFilter.Visible = UserContext.Roles.RoleId == (int) EnumRoles.Administrator;
    }

    protected void lnkExportInExcel_OnClick(object sender, EventArgs e)
    {
        int? cityId;
        if (!UserContext.CityId.HasValue)
        {
            cityId = CityUdodFilter.SelectedCity;
        }
        else
        {
            cityId = UserContext.CityId;
        }
        //if (cityId.HasValue || UserContext.UdodId.HasValue)
        {
            HttpContext.Current.Response.Clear();

            string filename = string.Format("Report.xls");

            HttpContext.Current.Response.AddHeader("content-disposition",
                                                    string.Format("attachment; filename={0}", filename));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
            Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];

            #region Стили

            Aspose.Cells.Style style_area = workbook.Styles[0];
            Aspose.Cells.Style style_header = workbook.Styles[1];
            style_area.ShrinkToFit = true;

            style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle =
                Aspose.Cells.CellBorderType.Thin;
            style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
            style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle =
                Aspose.Cells.CellBorderType.Thin;
            style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
            style_header.Font.IsBold = true;
            style_header.Font.Size = 12;
            style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
            style_header.BackgroundColor = System.Drawing.Color.Gray;
            style_header.Pattern = Aspose.Cells.BackgroundType.Solid;

            #endregion

            using (ReportDb db = new ReportDb())
            {

                var list = db.GetReportTransfer(cityId);
                int i = 0;
                sheet.Cells[i, 0].PutValue("Округ");
                sheet.Cells[i, 1].PutValue("Учреждение");
                sheet.Cells[i, 2].PutValue("Есть блок ДО");
                sheet.Cells[i, 3].PutValue("Номер учреждения");
                sheet.Cells[i, 4].PutValue("Всего зачислений");
                sheet.Cells[i, 5].PutValue("Количество зачислений со статусом:");
                sheet.Cells.Merge(i, 5, 1, 6);
                sheet.Cells.Merge(i, 0, 2, 1);
                sheet.Cells.Merge(i, 1, 2, 1);
                sheet.Cells.Merge(i, 2, 2, 1);
                sheet.Cells.Merge(i, 3, 2, 1);
                sheet.Cells.Merge(i, 4, 2, 1);
                i++;
                sheet.Cells[i, 5].PutValue("Заканчивает обучение");
                sheet.Cells[i, 6].PutValue("Будет переведен");
                sheet.Cells[i, 7].PutValue("Повторит программу");
                sheet.Cells[i, 8].PutValue("Будет отчислен");
                sheet.Cells[i, 9].PutValue("Нет возрастной группы");
                sheet.Cells[i, 10].PutValue("Будет перебор часов");
                i++;
                foreach (var item in list)
                {
                    sheet.Cells[i, 0].PutValue(item.CityName);
                    sheet.Cells[i, 1].PutValue(item.ShortName);
                    sheet.Cells[i, 2].PutValue(item.IsDOEnabled);
                    sheet.Cells[i, 3].PutValue(item.UdodNumber);
                    sheet.Cells[i, 4].PutValue(item.EnrollSum);
                    sheet.Cells[i, 5].PutValue(item.EndOfStudy);
                    sheet.Cells[i, 6].PutValue(item.WillBeTransfer);
                    sheet.Cells[i, 7].PutValue(item.Repear);
                    sheet.Cells[i, 8].PutValue(item.WillBeDismissed);
                    sheet.Cells[i, 9].PutValue(item.AgeGroupNotExist);
                    sheet.Cells[i, 10].PutValue(item.TooMuchHours);
                    i++;
                }
                sheet.Cells.CreateRange(0, 0, i, 11).ApplyStyle(style_area,
                                                                new Aspose.Cells.StyleFlag() {All = true});
            }
            sheet.AutoFitColumns();
            workbook.Save("Report.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel,
                            Response);
            HttpContext.Current.Response.End();
        }
    }

    protected void gvTransferReportList_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            TableCell HeaderCell = new TableCell();
            HeaderCell.Text = "";
            HeaderCell.ColumnSpan = 4;
            HeaderCell.BackColor = System.Drawing.Color.Gray;
            HeaderCell.Font.Bold = true;
            HeaderCell.Font.Size = FontUnit.Point(12);
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "Количество зачислений со статусом:";
            HeaderCell.ColumnSpan = 6;
            HeaderCell.BackColor = System.Drawing.Color.Gray;
            HeaderCell.Font.Bold = true;
            HeaderCell.Font.Size = FontUnit.Point(12);
            HeaderGridRow.Cells.Add(HeaderCell);

            gvTransferReportList.Controls[0].Controls.AddAt(0, HeaderGridRow);

        }
    }
}
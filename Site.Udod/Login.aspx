﻿<%@ Page Language="C#" MasterPageFile="Master/MasterLogin.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content runat="server" ID="contentLogin" ContentPlaceHolderID="body">
<script type="javascript" src="~/Scripts/NativeBridge.js"></script>
<script type="javascript" src="~/Scripts/CodeCryproPro.js"></script>

<script type="text/javascript">
/*
function CreateObject(name) {
    switch (navigator.appName) {
        case "Microsoft Internet Explorer":
            return new ActiveXObject(name);
        default:
            var userAgent = navigator.userAgent;
            //if (userAgent.match(/ipod/i) || userAgent.match(/ipad/i) || userAgent.macth(/iphone/i)) {
            //    return call_ru_cryptopro_npcades_10_native_bridge("CreateObject", [name]);
            //}
            var cadesobject = document.getElementById("cadesplugin");
            return cadesobject.CreateObject(name);
    }
}

Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(run);


function FillCertList(lstId, BrowserName) {
    var oStore = CreateObject("CAPICOM.store");
    if (!oStore) {
        alert("store failed");
        return;
    }

    try {
        oStore.Open();
    }
    catch (e) {
        alert("Ошибка при открытии хранилища: " + GetErrorMessage(e));
        return;
    }

    var certCnt = oStore.Certificates.Count;

    var lst = $("#selId");
    for (var i = 1; i <= certCnt; i++) {
        var cert;
        try {
            cert = oStore.Certificates.Item(i);
        }
        catch (ex) {
            alert("Ошибка при перечислении сертификатов: " + GetErrorMessage(ex));
            return;
        }

        var oOpt = $('<option>four</option>');
        try {
            oOpt.text(cert.SubjectName);
        }
        catch (e) {
            alert("Ошибка при получении свойства SubjectName: " + GetErrorMessage(e));
        }
        try {
            oOpt.val(cert.Thumbprint);
        }
        catch (e) {
            alert("Ошибка при получении свойства Thumbprint: " + GetErrorMessage(e));
        }
        $(lstId).append(oOpt);
        //lst.options[0]= oOpt;
    }
    //alert($('#selId').html);
    oStore.Close();
}

function CheckForPlugIn() {
    switch (navigator.appName) {
        case 'Microsoft Internet Explorer':
            try {
                var obj = new ActiveXObject("CAdESCOM.CPSigner");
                return true;
            }
            catch (err) {
                return false;
            }
        //case 'Netscape':
        default:
            var userAgent = navigator.userAgent;
            if (userAgent.match(/ipod/i) || userAgent.match(/ipad/i) || userAgent.match(/iphone/i)) {
                return true;
            }
            var cadesobject = document.getElementById('FFembeded');
            var mimetype = navigator.mimeTypes["application/x-cades"];
            if (mimetype) {
                var plugin = mimetype.enabledPlugin;
                if (plugin) {
                    return true;
                }
                else return false;
            }
    }

    
}

function run() {
    if (CheckForPlugIn) {
        FillCertList('#lbCert', '');
    }
    //alert($(certList).html);
    
    
}
function btnClick() {
    var browserName = navigator.appName;
    var appname1 = $('<%="#"+hf1.ClientID %>');
    appname1.val("-111");
    if (CheckForPlugIn) {
        var cert = $('#lbCert').val();
        var thumbprint = cert.split(" ").reverse().join("").replace( /\s/g , "").toUpperCase();
        try {
            var oStore = CreateObject("CAPICOM.store", browserName);
            oStore.Open();
        } catch(err) {
            return true;
        }
        var CAPICOM_CERTIFICATE_FIND_SHA1_HASH = 0;
        var oCerts = oStore.Certificates.Find(CAPICOM_CERTIFICATE_FIND_SHA1_HASH, thumbprint);

        if (oCerts.Count == 0) {
            oStore.Close();
            return true;
        }
        var oCert = oCerts.Item(1);
        try {
            var oSigner = CreateObject("CAdESCOM.CPSigner", browserName);
        } catch (err) {
            oStore.Close();
            return true;
        }
        if (oSigner) {
            oSigner.Certificate = oCert;
        }
        else {
            oStore.Close();
            return true;
        }

        var oSignedData = CreateObject("CAdESCOM.CadesSignedData", browserName);
        var txtDataToSign = "Хочу все знать";
        var CADES_BES = 1;
        var CADESCOM_CADES_DEFAULT = 0;
        oSignedData.Content = txtDataToSign;
        oSigner.Options = 1;
        try {
            var sSignedData = oSignedData.SignCades(oSigner, CADES_BES);
        }
        catch (e) {
            oStore.Close();
            return true;
        }

        appname1.val(cert);
        oStore.Close();
    }
    return true;
}
*/
</script>
<%--
<object id="cadesplugin" type="application/x-cades">
</object>
--%>

<table width="800px" style="height: 400px" >
    <tr align="center" valign="middle">
        <td>
            <asp:Panel ID="Panel1" runat="server" GroupingText="Авторизация" Width="240px" style="padding: 5px;">
                <table border=0>
                    <tr>
                        <td class="tdx">
                            Логин:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input">
                                <asp:TextBox runat="server" ID="tbLogin" CssClass="inputShort" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdx">
                            Пароль:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input">
                                <asp:TextBox runat="server" ID="tbPassword" TextMode="Password" CssClass="inputShort" />
                            </div>
                        </td>
                        <td>
                            <asp:Button style="margin-top:4px; margin-left:4px" runat="server" ID="lbEnter" onclick="lbEnter_Click" Text="Вход >>"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button runat="server" ID="lnkShowPopup" Visible="false" OnClick="lnkShowPopup_OnClick" Text="Войти через ЭЦП"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <asp:Label runat="server" ID="lblInfo"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
</table>

<asp:Panel runat="server" ID="popupEnterCert" style="padding: 5px; display: none; border: 2px solid black" BackColor="White">
        <asp:Panel ID="EnterCertHeader" runat="server" >
            <asp:Label ID="Label1" Text="<span class='headerCaptions'>Вход через ЭЦП</span>" runat="server"/>
            <asp:LinkButton ID="LinkButton1" runat="server" 
                OnClientClick="$find('EnterCertPopup').hide(); return false;" />
        </asp:Panel>
        <table>
            <tr>
                <td>Выберите ЭЦП для входа</td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <select id="lbCert" style="width: 300px;"></select>
                            <asp:HiddenField runat="server" ID="hf1" Value=""/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <div >
            <asp:Button ID="btnEnterCert" runat="server" Text="Войти" CausesValidation="false" OnClientClick="javascript:return btnClick();" OnClick="btnEnterCert_OnClick"/>
            <asp:Button runat="server" ID="btnCancel" Text="Отмена"/>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupEnterCertExtender" PopupDragHandleControlID="EnterCertHeader"
        PopupControlID="popupEnterCert" CancelControlID="btnCancel"
        TargetControlID="btnShow1" RepositionMode="None"
        BehaviorID="EnterCertPopup"/>
    <asp:Button ID="btnShow1" runat="server" style="display:none" />  

</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Udod.Dal;


namespace Site.Udod.WebServises
{

    /// <summary>
    /// Summary description for Kladr
    /// </summary>
    [WebService(Namespace = "Site.Udod.WebServises")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Kladr : BaseService
    {

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetRegion()
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();
            
            using (KladrDb db = new KladrDb())
            {
                list = db.GetRegion();
                /*
                foreach (var extendedKladr in list)
                {
                    object returnOneObject = new {Name = extendedKladr.Name, Code = extendedKladr.Code};
                    returnObject.Add(returnOneObject);
                }
                */
                return list.OrderBy(i=>i.Name).Select(p=>new {Name = p.Name, Code=p.Code});
            }
            
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetRayon(string code)
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();

            using (KladrDb db = new KladrDb())
            {
                list = db.GetRayon(code);
                return list.OrderBy(i => i.Name).Select(p => new { Name = p.Name, Code = p.Code });
            }

        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetCity(string code)
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();

            using (KladrDb db = new KladrDb())
            {
                list = db.GetCity(code);
                return list.OrderBy(i => i.Name).Select(p => new { Name = p.Name, Code = p.Code });
            }

        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetnasPunkt(string code)
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();

            using (KladrDb db = new KladrDb())
            {
                list = db.GetNasPunkt(code);
                return list.OrderBy(i => i.Name).Select(p => new { Name = p.Name, Code = p.Code });
            }

        }
        
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetStreetNew(string prefixText, int count, string contextKey)
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();

            using (KladrDb db = new KladrDb())
            {
                list = db.GetStreet(contextKey, prefixText, count);
                //if (list.Count==0) list.Add(new ExtendedKladr(){Code = "", Name = "Такой улицы нет. Проверьте правильность написания"});
                return list.OrderBy(i => i.Name).Select(p=>p.Name).ToArray();
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetHouse(string prefixText, int count, string contextKey)
        {
            List<ExtendedKladr> list = new List<ExtendedKladr>();

            using (KladrDb db = new KladrDb())
            {
                list = db.GetHouse(contextKey, prefixText, count);
                return list.Select(p => p.Name).ToArray();
            }
        }
        
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetOkatoAO(string code)
        {
            List<ExtendedOkato> list = new List<ExtendedOkato>();

            using (KladrDb db = new KladrDb())
            {
                list = db.GetOkatoAO(code);
                return list.OrderBy(i => i.Name).Select(p => new { Name = p.Name, Code = p.Code });
            }

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetOkatoRayon(string code)
        {
            List<ExtendedOkato> list = new List<ExtendedOkato>();

            using (KladrDb db = new KladrDb())
            {
                list = db.GetOkatoRayon(code);
                return list.OrderBy(i => i.Name).Select(p => new { Name = p.Name, Code = p.Code });
            }

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetAddress(string code, string houseNumber, string fraction, string housing, string building, string flatNumber, int? typeAddressCode)
        {
            ExtendedAddress item = new ExtendedAddress();

            using (KladrDb db = new KladrDb())
            {
                item = db.GetAddress(code, houseNumber, fraction, housing, building, flatNumber, typeAddressCode);
                return item.AddressStr;
            }

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetAddressTypes()
        {
            List<ExtendedAddressType> list = new List<ExtendedAddressType>();

            using (KladrDb db = new KladrDb())
            {
                list = db.GetAddressTypes();
                return list.OrderBy(i => i.Name).Select(p => new { Name = p.Name, Id = p.AddressTypeId});
            }

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetAddressesById(Guid? userId, long? udodId, long сhildUnionId, long udodAgeGroup)
        {
            List<ExtendedAddress> list = new List<ExtendedAddress>();

            using (KladrDb db = new KladrDb())
            {
                list = db.GetAddressesById(userId, udodId, сhildUnionId, udodAgeGroup);
                return list.OrderBy(i => i.AddressStr).Select(p => new { Name = p.AddressStr, Id = p.KladrCode });
            }

        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetStreetCode(string CityValue, string StreetName)
        {

            int i = StreetName.IndexOf("   ");

            using (KladrDb db = new KladrDb())
            {
                string tmp = db.GetStreetCode(CityValue, i > 0 ? StreetName.Substring(0, i).Trim() : StreetName);
                return tmp;
            }

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetHouseCode(string StreetValue, string HouseName)
        {

            int streetCode = int.Parse(StreetValue);
            string pattern = @"[^\s,]*";
            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase); 

            MatchCollection matches = regex.Matches(HouseName.Trim());

            string house = matches[0].Value;
            string krt = null;
            string strt = null;
            bool is_error = false;
            foreach (Match match in matches)
            {
                if (!string.IsNullOrEmpty(match.Value) && match.Index!=0)
                {
                    if (match.Value.Contains("корп."))
                    {
                        krt = match.Value.Replace("корп.", "");
                    } else
                    if (match.Value.Contains("стр."))
                    {
                        strt = match.Value.Replace("стр.", "");
                    }
                    else
                    {
                        is_error = true;
                    }
                }
            }
            if (is_error) return "";
            using (KladrDb db = new KladrDb())
            {
                int tmp = db.GetHouseCode(streetCode, house, krt, strt);
                return tmp!=0?tmp.ToString():"";
            }

        }
    }
}
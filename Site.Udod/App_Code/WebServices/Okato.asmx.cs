﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Udod.Dal;

namespace Site.Udod.WebServises
{
    /// <summary>
    /// Summary description for Okato
    /// </summary>
    [WebService(Namespace = "Site.Udod.WebServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Okato : System.Web.Services.WebService
    {

        [WebMethod]
        public List<ExtendedOkato> GetCitiesWithOkato()
        {
            using (KladrDb db = new KladrDb())
            {
                return db.GetOkatoAO("");
            }
        }
        [WebMethod]
        public List<ExtendedCity> GetCitiesWithCityId()
        {
            using (DictCityDb db = new DictCityDb())
            {
                return db.GetCities();
            }
        }
        [WebMethod]
        public List<ExtendedOkato> GetRayonsByOkatoId(string code)
        {
            using (KladrDb db = new KladrDb())
            {
                return db.GetOkatoRayon(code);
            }
        }
        [WebMethod]
        public List<ExtendedOkato> GetRayonsByCityId(int code)
        {
            using (KladrDb db = new KladrDb())
            {
                return db.GetOkatoRayon(code);
            }
        }
    }
}

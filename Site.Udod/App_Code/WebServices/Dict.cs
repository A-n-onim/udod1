﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.Services.Protocols;
using Udod.Dal;

namespace Site.Udod.WebServises
{
    /// <summary>
    /// Summary description for Dict
    /// </summary>
    [WebService(Namespace = "Site.Udod.WebServises")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Dict : System.Web.Services.WebService
    {
        
        public Dict()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }
        
        
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetCity()
        {
            using (DictCityDb db =new DictCityDb())
            {
                var list = db.GetCities();
                return list.ToArray();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetSchool(string prefixText, int count, string contextKey)
        {
            using (DictSchoolDb db = new DictSchoolDb())
            {
                var list = db.GetSchools(contextKey, prefixText, count);
                return list.OrderBy(i => i.Name).Select(i=>i.Name).ToArray();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetProgramNew(string prefixText, int count, string contextKey)
        {
            using (DictProgramDb db = new DictProgramDb())
            {
                var list = db.GetPrograms(contextKey, prefixText, count);
                return list.Select(i => i.Name).ToArray();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetProfileNew(string prefixText, int count, string contextKey)
        {
            using (DictProfileDb db = new DictProfileDb())
            {
                var list = db.GetProfiles(contextKey, prefixText, count);
                return list.Select(i => i.Name).ToArray();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetSectionNew(string prefixText, int count, string contextKey)
        {
            using (DictSectionDb db = new DictSectionDb())
            {
                var list = db.GetSections(contextKey, prefixText, count);
                return list.Select(i => i.Name).Distinct().ToArray();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object GetAddressUdodName(string prefixText, int count, string contextKey)
        {
            using (UdodDb db = new UdodDb())
            {
                var list = db.GetUdods(contextKey, prefixText, count);
                return list.Select(i => i.ShortName).Distinct().ToArray();
            }
        }
    }
}
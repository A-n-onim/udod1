﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Runtime.Serialization;
using Udod.Dal;


namespace Site.Udod.WebServises
{

    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "Site.Udod.WebServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Export : System.Web.Services.WebService
    {
        [WebMethod (Description = "Возвращает список УДО")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<ExtendedUdod> GetUdodsList(int? cityId, string cityCode, string rayonCode, int? metroId, int? programId, int? profileId, int? sectionId, string udodName, int? typeBudgetId)
        {
            using (UdodDb db = new UdodDb())
            {
                var list = db.GetUdods(cityId, cityCode, rayonCode, metroId, programId, profileId, sectionId, udodName, typeBudgetId, null, null, null, null, null, 1);
                return list.ToList();
            }
        }

        [WebMethod(Description = "Возвращает УДО")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ExtendedUdod GetUdod(long udodId)
        {
            using (UdodDb db = new UdodDb())
            {
                var item = db.GetUdod(udodId);
                return item;
            }
        }

        [WebMethod(Description = "Возвращает список заявлений")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<ExtendedClaim> GetClaimsList(Guid? userId, long? udodId, int? cityId, long? childUnionId, int? sectionId, int? claimStatusId, string number, string lastName, string firstName, string middleName, string udodName, string childUnion, DateTime? startDate, DateTime? endDate)
        {
            using (ClaimDb db = new ClaimDb())
            {
                var list = db.GetClaims(userId, udodId, cityId, childUnionId, sectionId, claimStatusId, number, lastName, firstName, middleName, udodName, childUnion, startDate, endDate, null, null,null, null, null,null, null);
                return list.ToList();
            }
        }

        [WebMethod(Description = "Возвращает заявление")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ExtendedClaim GetClaim(long claimId)
        {
            using (ClaimDb db = new ClaimDb())
            {
                return db.GetClaim(claimId);
            }
        }

        [WebMethod(Description = "Возвращает список статусов заявлений")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<ExtendedClaimStatus> GetClaimStatusesList()
        {
            using (ClaimDb db = new ClaimDb())
            {
                var list = db.GetClaimStatuses();
                return list.ToList();
            }
        }

        [WebMethod(Description = "Возвращает список профилей образовательной программы")]
        public List<ExtendedProfile> GetProfilesList(int programId)
        {
            List<ExtendedProfile> list = new List<ExtendedProfile>();

            using (DictProfileDb db = new DictProfileDb())
            {
                list = db.GetProfiles(programId,null);
                return list.OrderBy(i => i.Name).ToList();
            }
        }

        [WebMethod(Description = "Возвращает профиль по коду")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ExtendedProfile GetProfile(int profileId)
        {
            ExtendedProfile? item = new ExtendedProfile();

            using (DictProfileDb db = new DictProfileDb())
            {
                item = db.GetProfile(profileId, null);
                if (item.HasValue)
                    return item.Value;
                else
                    return new ExtendedProfile();
            }
        }

        [WebMethod(Description = "Возвращает список образовательных программ")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<ExtendedProgram> GetProgramsList()
        {
            List<ExtendedProgram> list = new List<ExtendedProgram>();

            using (DictProgramDb db = new DictProgramDb())
            {
                list = db.GetPrograms(null);
                return list.OrderBy(i => i.Name).ToList();
            }
        }

        [WebMethod(Description = "Возвращает образовательную программу по коду")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ExtendedProgram GetProgram(int programId)
        {
            ExtendedProgram? item = new ExtendedProgram();

            using (DictProgramDb db = new DictProgramDb())
            {
                item = db.GetProgram(programId, null);
                if (item.HasValue)
                    return item.Value;
                else
                    return new ExtendedProgram();
            }
        }

        [WebMethod(Description = "Возвращает список основных видов деятельности (предмет)")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<ExtendedSection> GetSectionsList(int? profileId, int? programId, long? udodId)
        {
            List<ExtendedSection> list = new List<ExtendedSection>();

            using (DictSectionDb db = new DictSectionDb())
            {
                list = db.GetSections(profileId, programId, udodId,"");
                return list.OrderBy(i => i.Name).ToList();
            }
        }

        [WebMethod(Description = "Возвращает основной вид деятельности (предмет) по коду")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ExtendedSection GetSection(int sectionId)
        {
            ExtendedSection item = new ExtendedSection();

            using (DictSectionDb db = new DictSectionDb())
            {
                item = db.GetSection(sectionId);
                return item;
            }
        }

        [WebMethod(Description = "Возвращает список метро")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<ExtendedMetro> GetMetrosList()
        {
            List<ExtendedMetro> list = new List<ExtendedMetro>();

            using (DictMetroDb db = new DictMetroDb())
            {
                list = db.GetMetros();
                return list.OrderBy(i => i.Name).ToList();
            }
        }

        [WebMethod(Description = "Возвращает метро по коду")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ExtendedMetro GetMetro(int metroId)
        {
            ExtendedMetro? item = new ExtendedMetro();

            using (DictMetroDb db = new DictMetroDb())
            {
                item = db.GetMetro(metroId);
                if (item.HasValue)
                    return item.Value;
                else
                    return new ExtendedMetro();
            }
        }

        [WebMethod(Description = "Возвращает список видов услуги")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<ExtendedTypeBudget> GetBudgetTypesList()
        {
            List<ExtendedTypeBudget> list = new List<ExtendedTypeBudget>();

            using (DictBudgetTypeDb db = new DictBudgetTypeDb())
            {
                list = db.GetBudgetTypes();
                return list.OrderBy(i => i.TypeBudgetName).ToList();
            }
        }

        [WebMethod(Description = "Возвращает список основных форм занятий")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<ExtendedEducationType> GetEducationTypesList()
        {
            List<ExtendedEducationType> list = new List<ExtendedEducationType>();

            using (DictEducationTypeDb db = new DictEducationTypeDb())
            {
                list = db.GetEducationTypes();
                return list.OrderBy(i => i.EducationTypeName).ToList();
            }
        }

        [WebMethod(Description = "Возвращает основную форму занятий по коду")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ExtendedEducationType GetEducationType(int educationTypeId)
        {
            ExtendedEducationType? item = new ExtendedEducationType();

            using (DictEducationTypeDb db = new DictEducationTypeDb())
            {
                item = db.GetEducationType(educationTypeId);
                if (item.HasValue)
                    return item.Value;
                else
                    return new ExtendedEducationType();
            }
        }

        [WebMethod(Description = "Возвращает список типов документов")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<ExtendedPassportType> GetPassportTypesList()
        {
            List<ExtendedPassportType> list = new List<ExtendedPassportType>();

            using (CommonDb db = new CommonDb())
            {
                list = db.GetPasportType();
                return list;
            }
        }

        [WebMethod(Description = "Возвращает список детских объединений")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<ExtendedChildUnion> GetChildUnionsList(long udodId, int? programId, int? profileId, int? sectionId, int? typeBudgetId)
        {
            List<ExtendedChildUnion> list = new List<ExtendedChildUnion>();

            using (ChildUnionDb db = new ChildUnionDb())
            {
                list = db.GetChildUnions(null, udodId, programId, profileId, sectionId,typeBudgetId, null, null, null, null,null,null,null, null, null);
                return list.OrderBy(i => i.Name).ToList();
            }
        }

        [WebMethod(Description = "Возвращает детское объединение")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ExtendedChildUnion GetChildUnion(long? childUnionId, long? ageGroupId)
        {
            ExtendedChildUnion item = new ExtendedChildUnion();

            using (ChildUnionDb db = new ChildUnionDb())
            {
                item = db.GetChildUnion(childUnionId, ageGroupId);
                return item;
            }
        }

        [WebMethod(Description = "Возвращает педагогов детского объединения")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<ExtendedChildUnionTeacher> GetChildUnionTeachers(long childUnionId)
        {
            List<ExtendedChildUnionTeacher> list = new List<ExtendedChildUnionTeacher>();

            using (ChildUnionDb db = new ChildUnionDb())
            {
                list = db.GetTeacher(childUnionId);
                return list.OrderBy(i => i.TeacherName).ToList(); ;
            }
        }


        [WebMethod(Description = "Возвращает список возрастных групп")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<ExtendedAgeGroup> GetAgeGroupsList(long udodId, int? sectionId, long? childUnionId)
        {
            List<ExtendedAgeGroup> list = new List<ExtendedAgeGroup>();

            using (AgeGroupDb db = new AgeGroupDb())
            {
                list = db.GetAgeGroups(null, udodId, sectionId, childUnionId);
                return list.OrderBy(i => i.Name).ToList();
            }
        }

        [WebMethod(Description = "Возвращает возрастную группу")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ExtendedAgeGroup GetAgeGroup(long udodAgeGroupId)
        {
            ExtendedAgeGroup item = new ExtendedAgeGroup();

            using (AgeGroupDb db = new AgeGroupDb())
            {
                item = db.GetAgeGroup(udodAgeGroupId);
                return item;
            }
        }
        [WebMethod(Description = "Изменяет статус заявления")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void UpdateClaimStatus(long claimId, int claimStatusId)
        {
            using (ClaimDb db = new ClaimDb())
            {
                db.UpdateClaimStatus(claimId, claimStatusId, null, null, null);
                //ExportInAsGuf tmp = new ExportInAsGuf();
                //tmp.UpdateStatus(claimStatusId, claimId,"");
            }
        }
    }
}
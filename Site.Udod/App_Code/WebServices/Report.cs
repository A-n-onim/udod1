﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Udod.Dal;

namespace Site.Udod.WebServises
{
    /// <summary>
    /// Summary description for Report
    /// </summary>
    [WebService(Namespace = "Site.Udod.WebServises")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Report : System.Web.Services.WebService
    {

        public Report()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<ExtendedExportReportClaim> GetReport()
        {
            using (ReportDb db = new ReportDb())
            {
                List<ExtendedExportReportClaim> returnList = new List<ExtendedExportReportClaim>();
                var list = db.GetReportClaimCount();
                var listUdod = list.Where(i=>i.UdodId!=0).Select(i => i.UdodId).Distinct().ToList();
                foreach (int udodid in listUdod)
                {
                    ExtendedExportReportClaim item = new ExtendedExportReportClaim();
                    var tmp = list.Where(i => i.UdodId == udodid).ToList();
                    foreach (ExtendedReportClaim extendedReportClaim in tmp)
                    {
                        // 1 - на рассмотрении
                        // 2 - поступило через осип
                        // 3 - поступило через УДО
                        // 4 - зачислено
                        // 5 - аннулировано
                        // 6 - в резерве
                        // 7 - поданы документы
                        // 8 - заявителей обратилось
                        // 9 - заявителей зачисленны
                        // 0 - поступило через портал
                        item.CityName = extendedReportClaim.CityName;
                        item.RayonName = extendedReportClaim.RayonName;
                        item.UdodName = extendedReportClaim.UdodName;
                        item.UdodShortName = extendedReportClaim.UdodShortName;
                        if (extendedReportClaim.TypeMessage==0)
                        {
                            item.CountType0 = extendedReportClaim.CountAll;
                        }
                        if (extendedReportClaim.TypeMessage == 1)
                        {
                            item.CountType1 = extendedReportClaim.CountAll;
                        }
                        if (extendedReportClaim.TypeMessage == 2)
                        {
                            item.CountType2 = extendedReportClaim.CountAll;
                        }
                        if (extendedReportClaim.TypeMessage == 3)
                        {
                            item.CountType3 = extendedReportClaim.CountAll;
                        }
                        if (extendedReportClaim.TypeMessage == 4)
                        {
                            item.CountType4 = extendedReportClaim.CountAll;
                        }
                        if (extendedReportClaim.TypeMessage == 5)
                        {
                            item.CountType5 = extendedReportClaim.CountAll;
                        }
                        if (extendedReportClaim.TypeMessage == 6)
                        {
                            item.CountType6 = extendedReportClaim.CountAll;
                        }
                        if (extendedReportClaim.TypeMessage == 7)
                        {
                            item.CountType7 = extendedReportClaim.CountAll;
                        }
                        if (extendedReportClaim.TypeMessage == 8)
                        {
                            item.CountType8 = extendedReportClaim.CountAll;
                        }
                        if (extendedReportClaim.TypeMessage == 9)
                        {
                            item.CountType9 = extendedReportClaim.CountAll;
                        }
                        
                    }
                    item.CountType10 = item.CountType2 + item.CountType3 + item.CountType0;
                    returnList.Add(item);
                }
                return returnList;
            }
        }

    }
}
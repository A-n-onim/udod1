﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;

namespace Site.Udod.Context
{
    /// <summary>
    /// Summary description for UserProfile
    /// </summary>
    public class UserProfile : Base
    {
        private new ProfileBase Profile
        {
            get { return HttpContext.Current.Profile; }
        }

        private bool _isModified = false;

        public UserProfile(UserContext context) : base(context)
        {
        }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string Username
        {
            get
            {
                if (HttpContext.Current.User.Identity == null)
                    return string.Empty;

                return HttpContext.Current.User.Identity.Name;
            }
        }

        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public Guid UserId
        {
            get { return (Guid) Profile["UserId"]; }
        }

        /// <summary>
        /// Идентификатор выбранного города
        /// </summary>
        public int? CityId
        {
            get { return (int?) Profile["CityId"]; }
            set
            {
                Profile["CityId"] = value;
                _isModified = true;
            }
        }

        /// <summary>
        /// Идентификатор выбранной школы
        /// </summary>
        public int? UdodId
        {
            get { return (int?) Profile["UdodId"]; }
            set
            {
                Profile["UdodId"] = value;
                _isModified = true;
            }
        }

        /// <summary>
        /// Email пользователя
        /// </summary>
        public string EMail
        {
            get { return (string) Profile["EMail"]; }
        }

        public bool IsModified
        {
            get { return _isModified; }
            private set { _isModified = value; }
        }

        /// <summary>
        /// ФИО пользователя
        /// </summary>
        public string Fio
        {
            get { return (string) Profile["Fio"]; }
        }
    }
}
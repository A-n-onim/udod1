﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Udod.Dal;
using Udod.Dal.Enum;

namespace Site.Udod.Context
{
    public class UserRole : Base
    {
        public List<ExtendedRole> Roles { get; private set; }

        public void Load()
        {
            using (RoleDb db = new RoleDb())
            {
                Roles = db.GetRolesByUserId(Profile.UserId);
            }
            
        }

        public UserRole(UserContext context) : base(context) { Load();}

        public bool IsAdministrator {get { return Roles.Exists(i => i.RoleId == (int)EnumRoles.Administrator); }}
        public bool IsOsip {get { return Roles.Exists(i => i.RoleId == (int)EnumRoles.Osip); }}
        public bool IsDepartment {get { return Roles.Exists(i => i.RoleId == (int)EnumRoles.Department); }}
        public bool IsUdodEmployee { get { return Roles.Exists(i => i.RoleId == (int)EnumRoles.UdodEmployee); } }
        public bool IsUser { get { return Roles.Exists(i => i.RoleId == (int)EnumRoles.User); } }
        public bool IsPupil { get { return Roles.Exists(i => i.RoleId == (int)EnumRoles.Pupil); } }

        public int? RoleId {get
        {
            if (IsAdministrator) return (int) EnumRoles.Administrator;
            if (IsDepartment) return (int) EnumRoles.Department;
            if (IsOsip) return (int) EnumRoles.Osip;
            if (IsUdodEmployee) return (int) EnumRoles.UdodEmployee;
            if (IsUser) return (int) EnumRoles.User;
            return null;
        }}
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Site.Udod.Context;

/// <summary>
/// Summary description for BaseMaster
/// </summary>
public class BaseMaster : MasterPage
{
    public UserContext UserContext { get; private set; }
    public PageEvents PageEvents { get; private set; }

    protected override void Construct()
    {
        
        UserContext=new UserContext();
        PageEvents = new PageEvents(UserContext);
        base.Construct();
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using Udod.Dal;

namespace Site.Udod.Context
{
    /// <summary>
    /// Summary description for BaseContext
    /// </summary>
    public abstract class Base
    {
        protected UserContext context;

        protected ProfileCommon Profile
        {
            get { return context.Profile; }
        }

        protected HttpSessionState Session
        {
            get { return HttpContext.Current.Session; }
        }

        protected HttpApplicationState Application
        {
            get { return HttpContext.Current.Application; }
        }

        public Base(UserContext context)
        {
            this.context = context;
        }

    }
}
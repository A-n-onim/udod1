﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using Site.Udod.Context;
using Udod.Dal;
using System.Web.Caching;

    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class BaseService : System.Web.Services.WebService
    {
        protected UserContext UserContext { get; set; }
        
        protected HttpRequest Request
        {
            get { return HttpContext.Current.Request; }
        }

        protected HttpResponse Response
        {
            get { return HttpContext.Current.Response; }
        }

        protected Cache Cache
        {
            get { return HttpContext.Current.Cache; }
        }

        protected bool IsAuthenticated
        {
            get
            {
                if (HttpContext.Current == null)
                    return false;

                return Request.IsAuthenticated;
            }
        }

        protected T GetProfileValue<T>(string name)
        {
            object value = HttpContext.Current.Profile[name];

            if (value != null || value is T)
                return (T)value;

            return default(T);
        }

        protected BaseService()
        {
            if (IsAuthenticated)
            {
                UserContext = new UserContext();
            }
        }

    }

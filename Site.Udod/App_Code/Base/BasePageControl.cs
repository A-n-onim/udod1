﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using Site.Udod.Context;

public class BasePageControl : UserControl
{
    #region Properties

    public UserContext UserContext
    {
        get { return BasePage.UserContext; }
    }

    public BasePage BasePage
    {
        get { return Page as BasePage; }
    }

    public PageEvents PageEvents { get { return BasePage.PageEvents; } }

    //public int UdodId { get { return BasePage.UdodId; } }
    //public int CityId { get { return BasePage.CityId; } }

    #endregion
}
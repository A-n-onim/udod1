﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Site.Udod.Context;

/// <summary>
/// Summary description for BasePage
/// </summary>
public class BasePage : Page
{
    /*
    public int UdodId 
    { 
        get { return ((BaseMaster) Master).SelectedUdod;}
        protected set { ((BaseMaster) Master).SelectedUdod = value; }
    }
    public int CityId
    {
        get { return ((BaseMaster)Master).SelectedCity; }
        protected set { ((BaseMaster)Master).SelectedCity = value; }
    }
    */
    public UserContext UserContext
    {
        get { return ((BaseMaster)Master).UserContext; }
    }

    public PageEvents PageEvents { get { return ((BaseMaster) Master).PageEvents; } }

    public UserContext Context { get { return ((BaseMaster) Master).UserContext; } }

    protected void CheckPermissions(bool allow)
    {
        if (!allow)
        {
            //throw new UnauthorizedAccessException();
            Page.Response.Redirect(SiteUtility.GetUrl("~/default.aspx"));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Security;
using System.Text;
using System.Web;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
/*using CryptoPro.Sharpei.ServiceModel;
using Microsoft.Web.Services2.Security.X509;
using Microsoft.Web.Services3;
using Microsoft.Web.Services3.Addressing;
using Microsoft.Web.Services3.Messaging;
using Service.ASUR;
using Service.asguf;
using Service.esz;
using Service.NSI;*/
using Udod.Dal;
/*using ItemsChoiceType1 = Service.NSI.ItemsChoiceType1;
using Attribute = Service.NSI.Attribute;
using X509Certificate = System.Security.Cryptography.X509Certificates.X509Certificate;
using X509CertificateCollection = System.Security.Cryptography.X509Certificates.X509CertificateCollection;*/


/// <summary>
/// Summary description for Utility
/// </summary>
public class ExtendedAttribute
{
    public string name { get; set; }
    public string type { get; set; }
    public string value { get; set; }
}

public class SendSms
{
    public SendSms(string TextMessage, string Number)
    {
        var request = HttpWebRequest.Create("http://utils.altarix.ru:8000/?login=armada&passwd=735o75N&service=1&msisdn="+Number+"&text="+TextMessage+"&Operation=send&type=sms");
        request.Method = "GET";
        var result = request.GetResponse();

    }
}
/*
public class AsurClass
{
    public Guid GetDeclarant(string lastName, string firstName, string middleName, bool sex, DateTime birthDay)
    {
        IdentificationServiceClient client = new IdentificationServiceClient();
        client.ChannelFactory.Endpoint.Contract.ProtectionLevel = System.Net.Security.ProtectionLevel.Sign;
        CustomBinding binding = new CustomBinding(client.Endpoint.Binding);

        SMEVMessageEncodingBindingElement textBindingElement = new SMEVMessageEncodingBindingElement()
        {
            MessageVersion = MessageVersion.CreateVersion(EnvelopeVersion.Soap11, AddressingVersion.None),
            RecipientActor = "RSMEVAUTH",
            SenderActor = "RSMEVAUTH"
        };
        binding.Elements.Remove<TextMessageEncodingBindingElement>();
        binding.Elements.Insert(0, textBindingElement);
        //var tmp1 = binding.Elements.Find<AsymmetricSecurityBindingElement>();
        //tmp1.EnableUnsecuredResponse = true;
        //var tmp2 = binding.Elements.Find<SecurityBindingElement>();
        //tmp2.DefaultAlgorithmSuite = CryptoPro.Sharpei.ServiceModel.GostAlgorithmSuite.BasicGostObsolete;
            
        client.Endpoint.Binding = binding;
        SignedDeclarant declarant = new SignedDeclarant();
        declarant.Data = new Declarant();
        declarant.Data.LastName = lastName;
        declarant.Data.FirstName = firstName;
        declarant.Data.Gender = sex?Gender.Male:Gender.Female;
        string snils = "";
        int sum = 0;
        Random rnd = new Random();
        for (int i = 9; i > 0; i--)
        {
            int first = rnd.Next(0, 9);
            sum = sum + first * i;
            snils = snils + first.ToString();
            if (i == 7 || i == 4) snils = snils + "-";
        }
        if (sum < 100) snils = snils + " " + sum.ToString();
        else
            if (sum == 100 || sum == 101) snils = snils + " 00";
            else
            {
                int ost = sum % 101;
                snils = snils + " " + (ost < 10 ? "0" + ost.ToString() : ost.ToString());
            }
        int[] n2 = new int[]{7,2,4,10,3,5,9,4,6,8};
        int[] n1 = new int[]{3,7,2,4,10,3,5,9,4,6,8};
        sum = 0;
        int sum2 = 0;
        string inn = "";
        for (int i = 0; i < 10; i++)
        {
            int first = rnd.Next(0, 9);
            sum = sum + first*n2[i];
            sum2 = sum2 + first*n1[i];
            inn = inn + first.ToString();
        }
        int firstnumber = sum % 11;
        if (firstnumber == 10) firstnumber = 0;
        sum2 = sum2 + firstnumber * n1[10];
        int secondNumber = sum2 % 11;
        if (secondNumber == 10) secondNumber = 0;
        inn = inn + firstnumber.ToString() + secondNumber.ToString();


        declarant.Data.Inn = inn;
        declarant.Data.Snils = snils;
        declarant.Data.BirthDate = birthDay;
        Guid res = Guid.Empty;
        try
        {
            res = client.Declarant_Insert(declarant, null);
        }
        catch (Exception e1)
        {
            string mess = e1.Message;
        }
        
        return res;
    }
}
*/
/*
public class ClassSoapClient : SoapClient
{
    private String soapAction;
    public ClassSoapClient(string soapAction, EndpointReference ref1)
        : base(ref1)
    {
        this.soapAction = soapAction;
    }

    public SoapEnvelope SendEnvelope(XmlDocument xmlDoc)
    {
        SoapEnvelope soapRequest = new SoapEnvelope();
        soapRequest.InnerXml = xmlDoc.InnerXml;
        return base.SendRequestResponse(soapAction, soapRequest);
    }
}
*/
public class MyStringBuilder
{
    private string _split;
    private List<string> list; 

    public MyStringBuilder(string split)
    {
        _split = split;
        list=new List<string>();
    }
    public MyStringBuilder(string split, IEnumerable<string> strings)
    {
        _split = split;
        list = new List<string>(strings);
    }

    public void Append(string item)
    {
        list.Add(item);
    }
    public void Remote(string item)
    {
        list.RemoveAt(list.IndexOf(item));
    }

    public override string ToString()
    {
        if (list.Count == 0) return "";
        StringBuilder sb=new StringBuilder();
        for (int i = 0; i < list.Count-1; i++)
        {
            sb.Append(list[i].Trim() + _split);
        }
        sb.Append(list[list.Count - 1]);
        return sb.ToString();
    }
}

public static class SiteUtility
{
    public static bool SendEmailMessage(string emailTo, string subject, string message, string fNReply, string fNRequest)
    {
        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
        System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(ConfigurationManager.AppSettings["SmtpHost"], Int32.Parse(ConfigurationManager.AppSettings["SmtpPort"]));
        try
        {
            msg.From = new MailAddress(ConfigurationManager.AppSettings["SmtpFrom"], ConfigurationManager.AppSettings["SmtpFromName"]);
            msg.To.Add(emailTo);

            msg.Subject = subject;
            msg.IsBodyHtml = true;
            msg.Body = message;
            Attachment att = new Attachment(fNRequest);
            msg.Attachments.Add(att);
            msg.Attachments.Add(new Attachment(fNReply));
            //smtpClient.EnableSsl = true;
            //smtpClient.UseDefaultCredentials = true;
            //smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SmtpUser"], ConfigurationManager.AppSettings["SmtpPass"]);

            smtpClient.Send(msg);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
    /*
    public static void SendScheduleESZ(long childUnionId, long groupId, int maxCountPupil, DateTime startLesson, DateTime startClaim, DateTime endClaim, int statusId)
    {
        IntegrationClient client = new IntegrationClient();
        client.Endpoint.Behaviors.Add(new InterceptorCustomBehavior());
        client.Open();
        var messageId = Guid.NewGuid();
        var messageId1 = Guid.NewGuid();

        AdmissionPlanRequest admissionPlan = new AdmissionPlanRequest();
        admissionPlan.header = new packageHeader();
        admissionPlan.header.id = messageId.ToString();
        admissionPlan.header.receiver = "ESZ";
        admissionPlan.header.sender = "UDOD";
        admissionPlan.header.createDateTime = DateTime.Now;
        admissionPlan.planServices = new AdmissionPlanRequestPlanServices();
        admissionPlan.planServices.academicYear = startClaim.AddDays(-151).Year;// 2013;
        admissionPlan.planServices.planService = new AdmissionPlanRequestPlanServicesPlanService[1];

        AdmissionSheduleRequest admissionShedule = new AdmissionSheduleRequest();
        admissionShedule.header = new packageHeader();
        admissionShedule.header.id = messageId1.ToString();
        admissionShedule.header.receiver = "ESZ";
        admissionShedule.header.sender = "UDOD";
        admissionShedule.header.createDateTime = DateTime.Now;
        admissionShedule.shedule = new ArrayOfAdmissionSheduleRequestSlotSlot[1];

        admissionPlan.planServices.planService[0] = new AdmissionPlanRequestPlanServicesPlanService();
        admissionPlan.planServices.planService[0].id = groupId.ToString();
        admissionPlan.planServices.planService[0].serviceId = childUnionId.ToString();
        admissionPlan.planServices.planService[0].startOfTraining = startLesson;
        admissionPlan.planServices.planService[0].teacher = "";
        //admissionPlan.planServices.planService[0].placeCount = maxCountPupil;
        //admissionPlan.planServices.planService[0].placeCountSpecified = true;

        admissionShedule.shedule[0] = new ArrayOfAdmissionSheduleRequestSlotSlot();
        admissionShedule.shedule[0].dateStart = startClaim;
        admissionShedule.shedule[0].dateEnd = endClaim;
        admissionShedule.shedule[0].id = groupId.ToString();
        admissionShedule.shedule[0].statusId = statusId;
        admissionShedule.shedule[0].dateStartSpecified = true;
        admissionShedule.shedule[0].dateEndSpecified = true;
        admissionShedule.shedule[0].planServiceId = groupId.ToString();

        try
        {
            var result1 = client.AdmissionPlan(admissionPlan);
            var result2 = client.AdmissionShedule(admissionShedule);
        }
        catch (Exception)
        {
        }
    }

    public static void SendBatchESZ(int? cityId, long? udodId, bool onlyDo, bool delete, long? childUnionId)
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var list = new List<ExtendedChildUnion>();
            bool? isActive = null;
            list = db.GetChildUnionsPaging(cityId, udodId, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, 500000, "", null, isActive, false, false, childUnionId);
            //if (childUnionId.HasValue) list = list.Where(i => i.ChildUnionId ==childUnionId.Value).ToList();
            //list = list.Where(i=>i.ci).ToList();

            IntegrationClient client = new IntegrationClient();
            client.Endpoint.Behaviors.Add(new InterceptorCustomBehavior());
            client.Open();
            
            //FileInfo file = new FileInfo("c:\\udod\\ErrorESZ" + Guid.NewGuid());
            //var writer = file.AppendText();
            int batchcount = 0;
            int num = 0;
            while (num < list.Count)
            {
                #region Отправка в ЕСЗ
                using (UdodDb db1 = new UdodDb())
                {
                    var messageId = Guid.NewGuid();
                    var messageId1 = Guid.NewGuid();
                    var messageId2 = Guid.NewGuid();



                    ServiceListRequest request = new ServiceListRequest();
                    request.header = new packageHeader();
                    request.header.id = messageId.ToString();
                    request.header.receiver = "ESZ";
                    request.header.sender = "UDOD";
                    request.header.createDateTime = DateTime.Now;

                    AdmissionPlanRequest admissionPlan = new AdmissionPlanRequest();
                    admissionPlan.header = new packageHeader();
                    admissionPlan.header.id = messageId1.ToString();
                    admissionPlan.header.receiver = "ESZ";
                    admissionPlan.header.sender = "UDOD";
                    admissionPlan.header.createDateTime = DateTime.Now;
                    admissionPlan.planServices = new AdmissionPlanRequestPlanServices();
                    admissionPlan.planServices.academicYear = 2013;
                    admissionPlan.planServices.planService = new AdmissionPlanRequestPlanServicesPlanService[100];
                    


                    AdmissionSheduleRequest admissionShedule = new AdmissionSheduleRequest();
                    admissionShedule.header = new packageHeader();
                    admissionShedule.header.id = messageId2.ToString();
                    admissionShedule.header.receiver = "ESZ";
                    admissionShedule.header.sender = "UDOD";
                    admissionShedule.header.createDateTime = DateTime.Now;
                    admissionShedule.shedule = new ArrayOfAdmissionSheduleRequestSlotSlot[100];

                    ArrayOfServiceListRequestServiceService[] array = new ArrayOfServiceListRequestServiceService[100];


                    //if (udod.CityId == 11 || udod.CityId == 1) continue;
                    
                    while (batchcount < 100 && num < list.Count)
                    {
                        var ecu1 = list[num];

                        var ecu = db.GetChildUnion(ecu1.ChildUnionId, null);

                        var udod = db1.GetUdod(ecu.UdodId);

                        if (!udod.IsDOEnabled || udod.UdodStatusId == 2 || ecu.ChildUnionDescriptionId == 0 || udod.CityId == 15)
                        {
                            num++;
                            continue;
                        }
                        ecu.MessageId = messageId;

                        array[batchcount] = new ArrayOfServiceListRequestServiceService();
                        //array[batchcount].
                        // Мин и макс значений возрастов
                        int min = ecu.AgeStart;
                        int max = ecu.AgeEnd;

                        var countGroups = db.GetCountGroups(ecu.ChildUnionId, 1);
                       
                        min = countGroups[1];
                        max = countGroups[2];

                        array[batchcount].id = ecu.ChildUnionId.ToString();
                        array[batchcount].code = ecu.ChildUnionId.ToString();
                        array[batchcount].name = ecu.Name;
                        array[batchcount].programmService = new stringFileField();
                        array[batchcount].programmService.@string = ecu.ProgramService;
                        array[batchcount].ruleService = new stringFileField();
                        array[batchcount].ruleService.@string = string.Format("Возраст ребёнка для получения данной образовательной услуги: от {0} до {1} лет. Возраст ребёнка для записи с Портала: от {2} до {3} лет.", ecu.AgeStart, ecu.AgeEnd, min, max);
                        array[batchcount].classificatiorRBNDOid = ecu.Section.EszSectionId.ToString();
                        array[batchcount].altAndSex = new ArrayOfServiceListRequestServiceServiceAltAndSex();
                        if (ecu.SexId == 1)
                        {
                            array[batchcount].altAndSex.male = new altsType();
                            array[batchcount].altAndSex.male.altStart = min;
                            array[batchcount].altAndSex.male.altEnd = max;
                        }
                        else if (ecu.SexId == 2)
                        {
                            array[batchcount].altAndSex.femail = new altsType();
                            array[batchcount].altAndSex.femail.altStart = min;
                            array[batchcount].altAndSex.femail.altEnd = max;
                        }
                        else if (ecu.SexId == 3)
                        {
                            array[batchcount].altAndSex.male = new altsType();
                            array[batchcount].altAndSex.male.altStart = min;
                            array[batchcount].altAndSex.male.altEnd = max;

                            array[batchcount].altAndSex.femail = new altsType();
                            array[batchcount].altAndSex.femail.altStart = min;
                            array[batchcount].altAndSex.femail.altEnd = max;
                        }
                        // неизвестно что посылать
                        //array[batchcount].restrictionsId = 
                        //array[batchcount].restrictionTypeId

                        array[batchcount].durationOfTraning = new ArrayOfServiceListRequestServiceServiceDurationOfTraning();
                        array[batchcount].durationOfTraning.ItemElementName = ItemChoiceType.years;
                        array[batchcount].durationOfTraning.Item = ecu.NumYears;

                        array[batchcount].levelId = "2";
                        array[batchcount].placesPlan = 0;

                        //array[batchcount].altStart = ecu.AgeStart;
                        //array[batchcount].altEnd = ecu.AgeEnd;
                        array[batchcount].testService = ecu.TestService;
                        array[batchcount].toursNumber = ecu.ToursNumber;
                        if (ecu.BudgetTypes.Count(i => i.DictTypeBudgetId == 2) > 0)
                            array[batchcount].typeFinancingId = "1";
                        else if (ecu.BudgetTypes.Count(i => i.DictTypeBudgetId == 1) > 0)
                            array[batchcount].typeFinancingId = "2";
                        array[batchcount].typeValueServiceId = ecu.TypeValueServiceId.ToString();
                        array[batchcount].finansing = ecu.Finansing;
                        array[batchcount].finansingSpecified = true;
                        array[batchcount].subFinanceService = ecu.SubFinance;
                        array[batchcount].subFinanceServiceSpecified = true;
                        array[batchcount].dateStart = ecu.DateStart;
                        array[batchcount].dateEnd = ecu.DateEnd;
                        //array[batchcount].sexId = ddlSex.SelectedItem.Value;
                        string statusid = "2";
                        if (delete) statusid = "3";
                        else if (countGroups[0]==0 || !ecu.IsActive || !ecu.isPublicPortal)
                        {
                            statusid = "1";
                        }
                        array[batchcount].statusId = statusid;
                        //array[batchcount].statusId = delete?"3": ? "1" : ecu.IsActive ? ecu.StatusId.ToString() : "1";
                        array[batchcount].formsServiceId = ecu.FormId.ToString();
                        array[batchcount].scheduleTypeOfServiceId = ecu.EducationTypes.Count(i => i.EducationTypeId == 1 || i.EducationTypeId == 2) > 0 ? "1" : "2";
                        array[batchcount].periodGettingDocument = ecu.DocsDeadline;
                        array[batchcount].organizationId = udodId.HasValue ? (new UdodDb()).GetUdod(udodId.Value).EkisId.ToString() : ""; //UserContext.UdodId.HasValue?UserContext.UdodId.Value.ToString():"";

                        var teachers = db.GetTeacher(ecu.ChildUnionId);

                        MyStringBuilder sb = new MyStringBuilder(", ", teachers.Select(i => i.TeacherName).ToList());

                        array[batchcount].teacher = sb.ToString();

                        array[batchcount].sheduleOfService = new ArrayOfServiceListRequestServiceServiceSheduleOfService();

                        array[batchcount].placeService = new ArrayOfServiceListRequestServiceServicePlaceService();

                        array[batchcount].sheduleOfService = new ArrayOfServiceListRequestServiceServiceSheduleOfService();

                        array[batchcount].itemsWorks = new ArrayOfServiceListRequestServiceItemsWorkItemsWork[1];
                        array[batchcount].itemsWorks[0] = new ArrayOfServiceListRequestServiceItemsWorkItemsWork();

                        array[batchcount].itemsWorks[0].sheduleOfItemWork = new ArrayOfServiceListRequestServiceItemsWorkItemsWorkSheduleOfItemWork();
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.monday = new ArrayOfServiceListRequestServiceItemsWorkItemsWorkSheduleOfItemWorkMonday();
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.monday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.monday.timeEnd = new DateTime(2013, 07, 12, 18, 0, 0);

                        array[batchcount].itemsWorks[0].sheduleOfItemWork.friday = new sheduleOfDayType();

                        //array[batchcount].itemsWorks[0].sheduleOfItemWork.friday = new ArrayOfServiceListRequestServiceItemsWorkItemsWorkSheduleOfItemWorkMonday();
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.friday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.friday.timeEnd = new DateTime(2013, 07, 12, 17, 45, 0);
                        
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.thursday = new sheduleOfDayType();
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.thursday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.thursday.timeEnd = new DateTime(2013, 07, 12, 18, 0, 0);

                        array[batchcount].itemsWorks[0].sheduleOfItemWork.tuesday = new sheduleOfDayType();
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.tuesday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.tuesday.timeEnd = new DateTime(2013, 07, 12, 18, 0, 0);

                        array[batchcount].itemsWorks[0].sheduleOfItemWork.wednesday = new sheduleOfDayType();
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.wednesday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.wednesday.timeEnd = new DateTime(2013, 07, 12, 18, 0, 0);

                        array[batchcount].organizationId = udod.EkisId.ToString();

                        var addresses = udod.Addresses;
                        var address1 = addresses.Where(i => i.IsPrimary).FirstOrDefault();

                        if (address1.AddressId == 0)
                        {
                            address1 = addresses.FirstOrDefault(i => i.BtiHouseCode != null);
                        }
                        if (address1.AddressId == 0)
                        {
                            address1 = addresses.FirstOrDefault();
                        }

                        if (address1.AddressId == 0 && delete)
                        {
                            // при удалении нет адреса
                            array[batchcount].itemsWorks[0].ItemsWorkId = "148802";
                            array[batchcount].itemsWorks[0].name = udod.Name;
                            array[batchcount].itemsWorks[0].address = new addressType();

                            array[batchcount].itemsWorks[0].address.building = "17";
                            array[batchcount].itemsWorks[0].address.codeStreet = "5036";
                            array[batchcount].itemsWorks[0].address.fullAddress = "г. Москва, Понтрягина Академика ул., д.17, к.1";
                            array[batchcount].itemsWorks[0].address.housing = "1";
                            array[batchcount].itemsWorks[0].address.structure = "";

                            array[batchcount].itemsWorks[0].address.unom = "2901046";
                        }

                        if (address1.AddressId != 0)
                        {
                            array[batchcount].itemsWorks[0].ItemsWorkId = address1.AddressId.ToString();
                            array[batchcount].itemsWorks[0].name = udod.Name;
                            array[batchcount].itemsWorks[0].address = new addressType();

                            array[batchcount].itemsWorks[0].address.building = address1.HouseNumber;
                            array[batchcount].itemsWorks[0].address.codeStreet = address1.BtiCode.HasValue ? address1.BtiCode.Value.ToString() : "";
                            array[batchcount].itemsWorks[0].address.fullAddress = address1.AddressStr;
                            array[batchcount].itemsWorks[0].address.housing = address1.Housing;
                            array[batchcount].itemsWorks[0].address.structure = address1.Building;
                            if (address1.BtiHouseCode.HasValue)
                                array[batchcount].itemsWorks[0].address.unom = address1.BtiHouseCode.Value.ToString();

                        }
                        var ecuaddresses = ecu.Addresses.Where(i => i.BtiHouseCode != null).ToList();
                        if (ecuaddresses.Count == 0)
                        {
                            ecuaddresses = ecu.Addresses;
                        }
                        if (ecuaddresses.Count == 0 && delete)
                        {
                            ecuaddresses.Add(new ExtendedAddress()
                            {
                                AddressId = 148802,
                                HouseNumber = "17",
                                BtiCode = 5036,
                                AddressStr = "г. Москва, Понтрягина Академика ул., д.17, к.1",
                                Housing = "1",
                                Building = "",
                                BtiHouseCode = 2901046
                            });
                        }
                        foreach (var address in ecuaddresses)
                        {
                            //if (item.Selected)
                            {
                                array[batchcount].placeService.PlaceServiceId = address.AddressId.ToString();
                                array[batchcount].placeService.name = udod.Name;
                                array[batchcount].placeService.address = new addressType();

                                if (address.AddressId != 0)
                                {
                                    array[batchcount].placeService.address.building = address.HouseNumber;
                                    array[batchcount].placeService.address.codeStreet = address.BtiCode.HasValue ? address.BtiCode.Value.ToString() : "";
                                    array[batchcount].placeService.address.fullAddress = address.AddressStr;
                                    array[batchcount].placeService.address.housing = address.Housing;
                                    array[batchcount].placeService.address.structure = address.Building;
                                    if (address.BtiHouseCode.HasValue)
                                        array[batchcount].placeService.address.unom = address.BtiHouseCode.Value.ToString();

                                    //array[batchcount].placeService.address.ownership = address.Building;

                                }



                            }
                        }

                        var scheduleList = db.GetChildUnionSimpleSchedule(ecu.ChildUnionId);

                        if (delete && scheduleList.Count == 0)
                        {
                            scheduleList.Add(new ExtendedSimpleSchedule()
                            {
                                ChildUnionId = ecu1.ChildUnionId,
                                DayOfWeek = 1,
                                TimeDayId = 1,
                                TimeDayName = "День"
                            });
                        }

                        foreach (var schedule in scheduleList)
                        {
                            if (schedule.DayOfWeek == 1)
                            {
                                array[batchcount].sheduleOfService.monday = new sheduleOfServiceDayType();
                                Service.esz.ItemsChoiceType[] elementName = new Service.esz.ItemsChoiceType[1];
                                elementName[0] = Service.esz.ItemsChoiceType.shedulePeriodId;
                                array[batchcount].sheduleOfService.monday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[batchcount].sheduleOfService.monday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 2)
                            {
                                array[batchcount].sheduleOfService.tuesday = new sheduleOfServiceDayType();
                                Service.esz.ItemsChoiceType[] elementName = new Service.esz.ItemsChoiceType[1];
                                elementName[0] = Service.esz.ItemsChoiceType.shedulePeriodId;
                                array[batchcount].sheduleOfService.tuesday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[batchcount].sheduleOfService.tuesday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 4)
                            {
                                array[batchcount].sheduleOfService.thursday = new sheduleOfServiceDayType();
                                Service.esz.ItemsChoiceType[] elementName = new Service.esz.ItemsChoiceType[1];
                                elementName[0] = Service.esz.ItemsChoiceType.shedulePeriodId;
                                array[batchcount].sheduleOfService.thursday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[batchcount].sheduleOfService.thursday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 3)
                            {
                                array[batchcount].sheduleOfService.wednesday = new sheduleOfServiceDayType();
                                Service.esz.ItemsChoiceType[] elementName = new Service.esz.ItemsChoiceType[1];
                                elementName[0] = Service.esz.ItemsChoiceType.shedulePeriodId;
                                array[batchcount].sheduleOfService.wednesday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[batchcount].sheduleOfService.wednesday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 5)
                            {
                                array[batchcount].sheduleOfService.friday = new sheduleOfServiceDayType();
                                Service.esz.ItemsChoiceType[] elementName = new Service.esz.ItemsChoiceType[1];
                                elementName[0] = Service.esz.ItemsChoiceType.shedulePeriodId;
                                array[batchcount].sheduleOfService.friday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[batchcount].sheduleOfService.friday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 6)
                            {
                                array[batchcount].sheduleOfService.saturday = new sheduleOfServiceDayType();
                                Service.esz.ItemsChoiceType[] elementName = new Service.esz.ItemsChoiceType[1];
                                elementName[0] = Service.esz.ItemsChoiceType.shedulePeriodId;
                                array[batchcount].sheduleOfService.saturday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[batchcount].sheduleOfService.saturday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 7)
                            {
                                array[batchcount].sheduleOfService.sunday = new sheduleOfServiceDayType();
                                Service.esz.ItemsChoiceType[] elementName = new Service.esz.ItemsChoiceType[1];
                                elementName[0] = Service.esz.ItemsChoiceType.shedulePeriodId;
                                array[batchcount].sheduleOfService.sunday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[batchcount].sheduleOfService.sunday.ItemsElementName = elementName;
                            }
                        }
                        //array[batchcount].sheduleOfService.monday.

                        array[batchcount].OrganizationResource = new ArrayOfServiceListRequestServiceOrganizationResourceOrganizationResource[1];
                        array[batchcount].OrganizationResource[0] = new ArrayOfServiceListRequestServiceOrganizationResourceOrganizationResource();
                        array[batchcount].OrganizationResource[0].name = udod.FioDirector;
                        array[batchcount].OrganizationResource[0].email = udod.EMail;
                        array[batchcount].OrganizationResource[0].phone = udod.PhoneNumber;
                        array[batchcount].OrganizationResource[0].organizationResourceId = udod.EkisId.ToString();
                        array[batchcount].OrganizationResource[0].position = "Директор";


                        #region План приема



                        admissionPlan.planServices.planService[batchcount] = new AdmissionPlanRequestPlanServicesPlanService();
                        admissionPlan.planServices.planService[batchcount].id = ecu.ChildUnionId.ToString();
                        admissionPlan.planServices.planService[batchcount].serviceId = ecu.ChildUnionId.ToString();
                        admissionPlan.planServices.planService[batchcount].startOfTraining = new DateTime(2013, 09, 01);
                        admissionPlan.planServices.planService[batchcount].teacher = "";
                        

                        #endregion
                        //newesz.Integration dd = new Integration();

                        #region расписание приема

                        admissionShedule.shedule[batchcount] = new ArrayOfAdmissionSheduleRequestSlotSlot();
                        admissionShedule.shedule[batchcount].dateStart = new DateTime(2013, 08, 15, 08, 00, 00, 00);
                        admissionShedule.shedule[batchcount].dateEnd = new DateTime(2014, 05, 31, 20, 00, 00, 00);
                        admissionShedule.shedule[batchcount].id = ecu.ChildUnionId.ToString();
                        admissionShedule.shedule[batchcount].statusId = ecu.isReception?2:4;
                        admissionShedule.shedule[batchcount].dateStartSpecified = true;
                        admissionShedule.shedule[batchcount].dateEndSpecified = true;
                        admissionShedule.shedule[batchcount].planServiceId = ecu.ChildUnionId.ToString();
                        #endregion

                        num++;
                        batchcount++;
                    }
                    request.services = array;

                    try
                    {
                        var result = client.ServiceList(request);
                        batchcount = 0;

                        if (!onlyDo)
                        {
                            var result1 = client.AdmissionPlan(admissionPlan);
                            var result2 = client.AdmissionShedule(admissionShedule);
                        }
                    }
                    catch (Exception e)
                    {
                        using(EventsDb db3=new EventsDb())
                        {
                            if (childUnionId.HasValue)
                            {
                                db3.AddException(childUnionId.Value,null,e.Message);
                            }
                        }
                        batchcount = 0;
                    }

                }

                #endregion

            }
            client.Close();

            //writer.Flush();
            //writer.Close();
        }
    }
    
    public static void SendBatchESZ(List<long> childUnions)
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var list = new List<ExtendedChildUnion>();
            bool? isActive = null;
            //list = db.GetChildUnionsPaging(cityId, udodId, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, 500000, "", null, isActive, false, false);
            //if (childUnionId.HasValue) list = list.Where(i => i.ChildUnionId == childUnionId.Value).ToList();
            //list = list.Where(i=>i.ci).ToList();
            foreach(var id in childUnions)
            {
                list.Add(new ExtendedChildUnion(){ChildUnionId = id});
            }
            


            IntegrationClient client = new IntegrationClient();
            client.Endpoint.Behaviors.Add(new InterceptorCustomBehavior());
            client.Open();

            //FileInfo file = new FileInfo("c:\\udod\\ErrorESZ" + Guid.NewGuid());
            //var writer = file.AppendText();
            int batchcount = 0;
            int num = 0;
            while (num < list.Count)
            {
                #region Отправка в ЕСЗ
                using (UdodDb db1 = new UdodDb())
                {
                    var messageId = Guid.NewGuid();
                    var messageId1 = Guid.NewGuid();
                    var messageId2 = Guid.NewGuid();



                    ServiceListRequest request = new ServiceListRequest();
                    request.header = new packageHeader();
                    request.header.id = messageId.ToString();
                    request.header.receiver = "ESZ";
                    request.header.sender = "UDOD";
                    request.header.createDateTime = DateTime.Now;

                    AdmissionPlanRequest admissionPlan = new AdmissionPlanRequest();
                    admissionPlan.header = new packageHeader();
                    admissionPlan.header.id = messageId1.ToString();
                    admissionPlan.header.receiver = "ESZ";
                    admissionPlan.header.sender = "UDOD";
                    admissionPlan.header.createDateTime = DateTime.Now;
                    admissionPlan.planServices = new AdmissionPlanRequestPlanServices();
                    admissionPlan.planServices.academicYear = 2013;
                    admissionPlan.planServices.planService = new AdmissionPlanRequestPlanServicesPlanService[100];



                    AdmissionSheduleRequest admissionShedule = new AdmissionSheduleRequest();
                    admissionShedule.header = new packageHeader();
                    admissionShedule.header.id = messageId2.ToString();
                    admissionShedule.header.receiver = "ESZ";
                    admissionShedule.header.sender = "UDOD";
                    admissionShedule.header.createDateTime = DateTime.Now;
                    admissionShedule.shedule = new ArrayOfAdmissionSheduleRequestSlotSlot[100];

                    ArrayOfServiceListRequestServiceService[] array = new ArrayOfServiceListRequestServiceService[100];


                    //if (udod.CityId == 11 || udod.CityId == 1) continue;
                    


                    while (batchcount < 100 && num < list.Count)
                    {
                        var ecu1 = list[num];
                        bool deleted = false;
                        var ecu = db.GetChildUnion(ecu1.ChildUnionId, null);
                        if (ecu==null)
                        {
                            ecu = db.GetDeletedChildUnion(ecu1.ChildUnionId, null);
                            deleted = true;
                        }
                        var udod = db1.GetUdod(ecu.UdodId);

                        if (!udod.IsDOEnabled || udod.UdodStatusId == 2 || ecu.ChildUnionDescriptionId == 0 || udod.CityId == 15)
                        {
                            
                            deleted=true;
                        }
                        ecu.MessageId = messageId;

                        array[batchcount] = new ArrayOfServiceListRequestServiceService();
                        //array[batchcount].
                        // Мин и макс значений возрастов
                        int min = ecu.AgeStart;
                        int max = ecu.AgeEnd;

                        var countGroups = db.GetCountGroups(ecu.ChildUnionId, 1);
                       
                        min = countGroups[1];
                        max = countGroups[2];

                        array[batchcount].id = ecu.ChildUnionId.ToString();
                        array[batchcount].code = ecu.ChildUnionId.ToString();
                        array[batchcount].name = ecu.Name;
                        array[batchcount].programmService = new stringFileField();
                        array[batchcount].programmService.@string = ecu.ProgramService;
                        array[batchcount].ruleService = new stringFileField();
                        array[batchcount].ruleService.@string = string.Format("Возраст ребёнка для получения данной образовательной услуги: от {0} до {1} лет. Возраст ребёнка для записи с Портала: от {2} до {3} лет.", ecu.AgeStart, ecu.AgeEnd, min, max);
                        array[batchcount].classificatiorRBNDOid = ecu.Section.EszSectionId.ToString();
                        array[batchcount].altAndSex = new ArrayOfServiceListRequestServiceServiceAltAndSex();
                        if (ecu.SexId == 1)
                        {
                            array[batchcount].altAndSex.male = new altsType();
                            array[batchcount].altAndSex.male.altStart = min;
                            array[batchcount].altAndSex.male.altEnd = max;
                        }
                        else if (ecu.SexId == 2)
                        {
                            array[batchcount].altAndSex.femail = new altsType();
                            array[batchcount].altAndSex.femail.altStart = min;
                            array[batchcount].altAndSex.femail.altEnd = max;
                        }
                        else if (ecu.SexId == 3)
                        {
                            array[batchcount].altAndSex.male = new altsType();
                            array[batchcount].altAndSex.male.altStart = min;
                            array[batchcount].altAndSex.male.altEnd = max;

                            array[batchcount].altAndSex.femail = new altsType();
                            array[batchcount].altAndSex.femail.altStart = min;
                            array[batchcount].altAndSex.femail.altEnd = max;
                        }
                        // неизвестно что посылать
                        //array[batchcount].restrictionsId = 
                        //array[batchcount].restrictionTypeId

                        array[batchcount].durationOfTraning = new ArrayOfServiceListRequestServiceServiceDurationOfTraning();
                        array[batchcount].durationOfTraning.ItemElementName = ItemChoiceType.years;
                        array[batchcount].durationOfTraning.Item = ecu.NumYears;

                        array[batchcount].levelId = "2";
                        array[batchcount].placesPlan = 0;

                        //array[batchcount].altStart = ecu.AgeStart;
                        //array[batchcount].altEnd = ecu.AgeEnd;
                        array[batchcount].testService = ecu.TestService;
                        array[batchcount].toursNumber = ecu.ToursNumber;
                        if (ecu.BudgetTypes.Count(i => i.DictTypeBudgetId == 2) > 0)
                            array[batchcount].typeFinancingId = "1";
                        else if (ecu.BudgetTypes.Count(i => i.DictTypeBudgetId == 1) > 0)
                            array[batchcount].typeFinancingId = "2";
                        array[batchcount].typeValueServiceId = ecu.TypeValueServiceId.ToString();
                        array[batchcount].finansing = ecu.Finansing;
                        array[batchcount].finansingSpecified = true;
                        array[batchcount].subFinanceService = ecu.SubFinance;
                        array[batchcount].subFinanceServiceSpecified = true;
                        array[batchcount].dateStart = ecu.DateStart;
                        array[batchcount].dateEnd = ecu.DateEnd;
                        //array[batchcount].sexId = ddlSex.SelectedItem.Value;
                        string statusid = "2";
                        if (deleted) statusid = "3";
                        else if (countGroups[0] == 0 || !ecu.IsActive || !ecu.isPublicPortal)
                        {
                            statusid = "1";
                        }
                        array[batchcount].statusId = statusid;
                        //array[batchcount].statusId = delete?"3": ? "1" : ecu.IsActive ? ecu.StatusId.ToString() : "1";
                        array[batchcount].formsServiceId = ecu.FormId.ToString();
                        array[batchcount].scheduleTypeOfServiceId = ecu.EducationTypes.Count(i => i.EducationTypeId == 1 || i.EducationTypeId == 2) > 0 ? "1" : "2";
                        array[batchcount].periodGettingDocument = ecu.DocsDeadline;
                        array[batchcount].organizationId = udod.EkisId.ToString(); //UserContext.UdodId.HasValue?UserContext.UdodId.Value.ToString():"";

                        var teachers = db.GetTeacher(ecu.ChildUnionId);

                        MyStringBuilder sb = new MyStringBuilder(", ", teachers.Select(i => i.TeacherName).ToList());

                        array[batchcount].teacher = sb.ToString();

                        array[batchcount].sheduleOfService = new ArrayOfServiceListRequestServiceServiceSheduleOfService();

                        array[batchcount].placeService = new ArrayOfServiceListRequestServiceServicePlaceService();

                        array[batchcount].sheduleOfService = new ArrayOfServiceListRequestServiceServiceSheduleOfService();

                        array[batchcount].itemsWorks = new ArrayOfServiceListRequestServiceItemsWorkItemsWork[1];
                        array[batchcount].itemsWorks[0] = new ArrayOfServiceListRequestServiceItemsWorkItemsWork();

                        array[batchcount].itemsWorks[0].sheduleOfItemWork = new ArrayOfServiceListRequestServiceItemsWorkItemsWorkSheduleOfItemWork();
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.monday = new ArrayOfServiceListRequestServiceItemsWorkItemsWorkSheduleOfItemWorkMonday();
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.monday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.monday.timeEnd = new DateTime(2013, 07, 12, 18, 0, 0);

                        array[batchcount].itemsWorks[0].sheduleOfItemWork.friday = new sheduleOfDayType();

                        //array[batchcount].itemsWorks[0].sheduleOfItemWork.friday = new ArrayOfServiceListRequestServiceItemsWorkItemsWorkSheduleOfItemWorkMonday();
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.friday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.friday.timeEnd = new DateTime(2013, 07, 12, 17, 45, 0);
                       
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.thursday = new sheduleOfDayType();
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.thursday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.thursday.timeEnd = new DateTime(2013, 07, 12, 18, 0, 0);

                        array[batchcount].itemsWorks[0].sheduleOfItemWork.tuesday = new sheduleOfDayType();
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.tuesday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.tuesday.timeEnd = new DateTime(2013, 07, 12, 18, 0, 0);

                        array[batchcount].itemsWorks[0].sheduleOfItemWork.wednesday = new sheduleOfDayType();
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.wednesday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[batchcount].itemsWorks[0].sheduleOfItemWork.wednesday.timeEnd = new DateTime(2013, 07, 12, 18, 0, 0);

                        array[batchcount].organizationId = udod.EkisId.ToString();

                        var addresses = udod.Addresses;
                        var address1 = addresses.Where(i => i.IsPrimary).FirstOrDefault();

                        if (address1.AddressId == 0)
                        {
                            address1 = addresses.FirstOrDefault(i => i.BtiHouseCode != null);
                        }
                        if (address1.AddressId == 0)
                        {
                            address1 = addresses.FirstOrDefault();
                        }
                        if (address1.AddressId == 0 && deleted)
                        {
                            // при удалении нет адреса
                            array[batchcount].itemsWorks[0].ItemsWorkId = "148802";
                            array[batchcount].itemsWorks[0].name = udod.Name;
                            array[batchcount].itemsWorks[0].address = new addressType();

                            array[batchcount].itemsWorks[0].address.building = "17";
                            array[batchcount].itemsWorks[0].address.codeStreet = "5036";
                            array[batchcount].itemsWorks[0].address.fullAddress = "г. Москва, Понтрягина Академика ул., д.17, к.1";
                            array[batchcount].itemsWorks[0].address.housing = "1";
                            array[batchcount].itemsWorks[0].address.structure = "";

                            array[batchcount].itemsWorks[0].address.unom = "2901046";
                        }
                        if (address1.AddressId != 0)
                        {
                            array[batchcount].itemsWorks[0].ItemsWorkId = address1.AddressId.ToString();
                            array[batchcount].itemsWorks[0].name = udod.Name;
                            array[batchcount].itemsWorks[0].address = new addressType();

                            array[batchcount].itemsWorks[0].address.building = address1.HouseNumber;
                            array[batchcount].itemsWorks[0].address.codeStreet = address1.BtiCode.HasValue ? address1.BtiCode.Value.ToString() : "";
                            array[batchcount].itemsWorks[0].address.fullAddress = address1.AddressStr;
                            array[batchcount].itemsWorks[0].address.housing = address1.Housing;
                            array[batchcount].itemsWorks[0].address.structure = address1.Building;
                            if (address1.BtiHouseCode.HasValue)
                                array[batchcount].itemsWorks[0].address.unom = address1.BtiHouseCode.Value.ToString();

                        }
                        var ecuaddresses = ecu.Addresses.Where(i => i.BtiHouseCode != null).ToList();
                        if (ecuaddresses.Count == 0)
                        {
                            ecuaddresses = ecu.Addresses;
                        }
                        if (ecuaddresses.Count==0 && deleted)
                        {
                            ecuaddresses.Add(new ExtendedAddress()
                                                 {
                                                     AddressId = 148802,
                                                     HouseNumber = "17",
                                                     BtiCode = 5036,
                                                     AddressStr = "г. Москва, Понтрягина Академика ул., д.17, к.1",
                                                     Housing = "1",
                                                     Building = "",
                                                     BtiHouseCode = 2901046
                                                 });
                        }
                        foreach (var address in ecuaddresses)
                        {
                            //if (item.Selected)
                            {
                                array[batchcount].placeService.PlaceServiceId = address.AddressId.ToString();
                                array[batchcount].placeService.name = udod.Name;
                                array[batchcount].placeService.address = new addressType();

                                if (address.AddressId != 0)
                                {
                                    array[batchcount].placeService.address.building = address.HouseNumber;
                                    array[batchcount].placeService.address.codeStreet = address.BtiCode.HasValue ? address.BtiCode.Value.ToString() : "";
                                    array[batchcount].placeService.address.fullAddress = address.AddressStr;
                                    array[batchcount].placeService.address.housing = address.Housing;
                                    array[batchcount].placeService.address.structure = address.Building;
                                    if (address.BtiHouseCode.HasValue)
                                        array[batchcount].placeService.address.unom = address.BtiHouseCode.Value.ToString();

                                    //array[batchcount].placeService.address.ownership = address.Building;

                                }



                            }
                        }

                        var scheduleList = db.GetChildUnionSimpleSchedule(ecu.ChildUnionId);

                        if (deleted && scheduleList.Count==0)
                        {
                            scheduleList.Add(new ExtendedSimpleSchedule()
                                                 {
                                                     ChildUnionId = ecu1.ChildUnionId,
                                                     DayOfWeek = 1,
                                                     TimeDayId = 1,
                                                     TimeDayName = "День"
                                                 });
                        }

                        foreach (var schedule in scheduleList)
                        {
                            if (schedule.DayOfWeek == 1)
                            {
                                array[batchcount].sheduleOfService.monday = new sheduleOfServiceDayType();
                                Service.esz.ItemsChoiceType[] elementName = new Service.esz.ItemsChoiceType[1];
                                elementName[0] = Service.esz.ItemsChoiceType.shedulePeriodId;
                                array[batchcount].sheduleOfService.monday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[batchcount].sheduleOfService.monday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 2)
                            {
                                array[batchcount].sheduleOfService.tuesday = new sheduleOfServiceDayType();
                                Service.esz.ItemsChoiceType[] elementName = new Service.esz.ItemsChoiceType[1];
                                elementName[0] = Service.esz.ItemsChoiceType.shedulePeriodId;
                                array[batchcount].sheduleOfService.tuesday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[batchcount].sheduleOfService.tuesday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 4)
                            {
                                array[batchcount].sheduleOfService.thursday = new sheduleOfServiceDayType();
                                Service.esz.ItemsChoiceType[] elementName = new Service.esz.ItemsChoiceType[1];
                                elementName[0] = Service.esz.ItemsChoiceType.shedulePeriodId;
                                array[batchcount].sheduleOfService.thursday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[batchcount].sheduleOfService.thursday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 3)
                            {
                                array[batchcount].sheduleOfService.wednesday = new sheduleOfServiceDayType();
                                Service.esz.ItemsChoiceType[] elementName = new Service.esz.ItemsChoiceType[1];
                                elementName[0] = Service.esz.ItemsChoiceType.shedulePeriodId;
                                array[batchcount].sheduleOfService.wednesday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[batchcount].sheduleOfService.wednesday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 5)
                            {
                                array[batchcount].sheduleOfService.friday = new sheduleOfServiceDayType();
                                Service.esz.ItemsChoiceType[] elementName = new Service.esz.ItemsChoiceType[1];
                                elementName[0] = Service.esz.ItemsChoiceType.shedulePeriodId;
                                array[batchcount].sheduleOfService.friday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[batchcount].sheduleOfService.friday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 6)
                            {
                                array[batchcount].sheduleOfService.saturday = new sheduleOfServiceDayType();
                                Service.esz.ItemsChoiceType[] elementName = new Service.esz.ItemsChoiceType[1];
                                elementName[0] = Service.esz.ItemsChoiceType.shedulePeriodId;
                                array[batchcount].sheduleOfService.saturday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[batchcount].sheduleOfService.saturday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 7)
                            {
                                array[batchcount].sheduleOfService.sunday = new sheduleOfServiceDayType();
                                Service.esz.ItemsChoiceType[] elementName = new Service.esz.ItemsChoiceType[1];
                                elementName[0] = Service.esz.ItemsChoiceType.shedulePeriodId;
                                array[batchcount].sheduleOfService.sunday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[batchcount].sheduleOfService.sunday.ItemsElementName = elementName;
                            }
                        }
                        //array[batchcount].sheduleOfService.monday.

                        array[batchcount].OrganizationResource = new ArrayOfServiceListRequestServiceOrganizationResourceOrganizationResource[1];
                        array[batchcount].OrganizationResource[0] = new ArrayOfServiceListRequestServiceOrganizationResourceOrganizationResource();
                        array[batchcount].OrganizationResource[0].name = udod.FioDirector;
                        array[batchcount].OrganizationResource[0].email = udod.EMail;
                        array[batchcount].OrganizationResource[0].phone = udod.PhoneNumber;
                        array[batchcount].OrganizationResource[0].organizationResourceId = udod.EkisId.ToString();
                        array[batchcount].OrganizationResource[0].position = "Директор";


                        #region План приема



                        admissionPlan.planServices.planService[batchcount] = new AdmissionPlanRequestPlanServicesPlanService();
                        admissionPlan.planServices.planService[batchcount].id = ecu.ChildUnionId.ToString();
                        admissionPlan.planServices.planService[batchcount].serviceId = ecu.ChildUnionId.ToString();
                        admissionPlan.planServices.planService[batchcount].startOfTraining = new DateTime(2013, 09, 01);
                        admissionPlan.planServices.planService[batchcount].teacher = "";


                        #endregion
                        //newesz.Integration dd = new Integration();

                        #region расписание приема

                        admissionShedule.shedule[batchcount] = new ArrayOfAdmissionSheduleRequestSlotSlot();
                        admissionShedule.shedule[batchcount].dateStart = new DateTime(2013, 08, 15, 08, 00, 00, 00);
                        admissionShedule.shedule[batchcount].dateEnd = new DateTime(2014, 05, 31, 20, 00, 00, 00);
                        admissionShedule.shedule[batchcount].id = ecu.ChildUnionId.ToString();
                        admissionShedule.shedule[batchcount].statusId = ecu.isReception ? 2 : 4;
                        admissionShedule.shedule[batchcount].dateStartSpecified = true;
                        admissionShedule.shedule[batchcount].dateEndSpecified = true;
                        admissionShedule.shedule[batchcount].planServiceId = ecu.ChildUnionId.ToString();
                        #endregion

                        num++;
                        batchcount++;
                    }
                    request.services = array;

                    try
                    {
                        var result = client.ServiceList(request);
                        batchcount = 0;

                        
                    }
                    catch (Exception)
                    {
                        //writer.WriteLine("Error " + messageId);
                        batchcount = 0;
                    }

                }

                #endregion

            }
            client.Close();

            //writer.Flush();
            //writer.Close();
        }
    }

    public static void SendBatchScheduleESZ(long childUnionId, int checkPriem, List<ExtendedGroup> groups)
    {
        //using (AgeGroupDb db = new AgeGroupDb())
        {
            IntegrationClient client = new IntegrationClient();
            client.Endpoint.Behaviors.Add(new InterceptorCustomBehavior());
            client.Open();

            //FileInfo file = new FileInfo("c:\\udod\\ErrorESZ" + Guid.NewGuid());
            //var writer = file.AppendText();
            int batchcount = 0;
            int num = 0;
            while (num < groups.Count)
            {
                #region Отправка в ЕСЗ
                using (UdodDb db1 = new UdodDb())
                {
                    var messageId = Guid.NewGuid();
                    var messageId1 = Guid.NewGuid();

                    AdmissionPlanRequest admissionPlan = new AdmissionPlanRequest();
                    admissionPlan.header = new packageHeader();
                    admissionPlan.header.id = messageId.ToString();
                    admissionPlan.header.receiver = "ESZ";
                    admissionPlan.header.sender = "UDOD";
                    admissionPlan.header.createDateTime = DateTime.Now;
                    admissionPlan.planServices = new AdmissionPlanRequestPlanServices();
                    admissionPlan.planServices.academicYear = groups.Min(i=>i.StartClaim).HasValue?groups.Min(i=>i.StartClaim).Value.AddDays(-191).Year:2013;
                    admissionPlan.planServices.academicYearSpecified = true;
                    admissionPlan.planServices.planService = new AdmissionPlanRequestPlanServicesPlanService[100];



                    AdmissionSheduleRequest admissionShedule = new AdmissionSheduleRequest();
                    admissionShedule.header = new packageHeader();
                    admissionShedule.header.id = messageId1.ToString();
                    admissionShedule.header.receiver = "ESZ";
                    admissionShedule.header.sender = "UDOD";
                    admissionShedule.header.createDateTime = DateTime.Now;
                    admissionShedule.shedule = new ArrayOfAdmissionSheduleRequestSlotSlot[100];

                    ArrayOfServiceListRequestServiceService[] array = new ArrayOfServiceListRequestServiceService[100];


                    


                    while (batchcount < 100 && num < groups.Count)
                    {
                        var ecu1 = groups[num];

                        #region План приема

                        admissionPlan.planServices.planService[batchcount] = new AdmissionPlanRequestPlanServicesPlanService();
                        admissionPlan.planServices.planService[batchcount].id = ecu1.UdodAgeGroupId.ToString();
                        admissionPlan.planServices.planService[batchcount].serviceId = childUnionId.ToString();
                        admissionPlan.planServices.planService[batchcount].startOfTraining = ecu1.StartLesson.HasValue?ecu1.StartLesson.Value:new DateTime(2013, 09, 01);
                        admissionPlan.planServices.planService[batchcount].teacher = "";
                        //admissionPlan.planServices.planService[batchcount].placeCount = ecu1.MaxCountPupil;
                        //admissionPlan.planServices.planService[batchcount].placeCountSpecified = true;


                        #endregion
                        //newesz.Integration dd = new Integration();

                        #region расписание приема

                        admissionShedule.shedule[batchcount] = new ArrayOfAdmissionSheduleRequestSlotSlot();
                        admissionShedule.shedule[batchcount].dateStart = ecu1.StartClaim.HasValue ? ecu1.StartClaim.Value : new DateTime(2013, 08, 15, 08, 00, 00, 00);
                        admissionShedule.shedule[batchcount].dateEnd = ecu1.EndClaim.HasValue ? ecu1.EndClaim.Value : new DateTime(2014, 05, 31, 20, 00, 00, 00);
                        admissionShedule.shedule[batchcount].id = ecu1.UdodAgeGroupId.ToString();
                        // со статусом надо разбиратся
                        int statusId = 2;
                        if (checkPriem==0)
                        {
                            // переключились на "Нет приема"
                            statusId = 4;
                        }
                        else if (checkPriem==1)
                        {
                            // Переключились на прием есть, тогда надо проверять кол-во в группе и максимальное кол-во
                            if (ecu1.MaxCountPupil-ecu1.CurrentCountPupil>0)
                            {
                                // места есть
                                statusId = 2;
                            }
                            else
                            {
                                statusId = 3;
                            }
                        }
                        admissionShedule.shedule[batchcount].statusId = statusId;
                        admissionShedule.shedule[batchcount].dateStartSpecified = true;
                        admissionShedule.shedule[batchcount].dateEndSpecified = true;
                        admissionShedule.shedule[batchcount].planServiceId = ecu1.UdodAgeGroupId.ToString();
                        #endregion

                        num++;
                        batchcount++;
                    }
                    //request.services = array;
                    int typ1 = 1;
                    try
                    {
                        var result1 = client.AdmissionPlan(admissionPlan);
                        typ1 = 2;
                        var result2 = client.AdmissionShedule(admissionShedule);
                    }
                    catch (Exception e)
                    {
                        using (EventsDb db3 = new EventsDb())
                        {
                            db3.AddException(childUnionId, typ1, e.Message);
                            
                        }
                        //writer.WriteLine("Error " + messageId);
                        batchcount = 0;
                    }
                }
                #endregion
            }
            client.Close();
            //writer.Flush();
            //writer.Close();
        }
    }

    public static void SendBatchDeleteScheduleESZ(List<ExtendedGroup> groups)
    {
        //using (AgeGroupDb db = new AgeGroupDb())
        {
            IntegrationClient client = new IntegrationClient();
            client.Endpoint.Behaviors.Add(new InterceptorCustomBehavior());
            client.Open();

            //FileInfo file = new FileInfo("c:\\udod\\ErrorESZ" + Guid.NewGuid());
            //var writer = file.AppendText();
            int batchcount = 0;
            int num = 0;
            while (num < groups.Count)
            {
                #region Отправка в ЕСЗ
                using (UdodDb db1 = new UdodDb())
                {
                    var messageId = Guid.NewGuid();
                    var messageId1 = Guid.NewGuid();

                    AdmissionPlanRequest admissionPlan = new AdmissionPlanRequest();
                    admissionPlan.header = new packageHeader();
                    admissionPlan.header.id = messageId.ToString();
                    admissionPlan.header.receiver = "ESZ";
                    admissionPlan.header.sender = "UDOD";
                    admissionPlan.header.createDateTime = DateTime.Now;
                    admissionPlan.planServices = new AdmissionPlanRequestPlanServices();
                    admissionPlan.planServices.academicYear = 2013;
                    admissionPlan.planServices.planService = new AdmissionPlanRequestPlanServicesPlanService[100];



                    AdmissionSheduleRequest admissionShedule = new AdmissionSheduleRequest();
                    admissionShedule.header = new packageHeader();
                    admissionShedule.header.id = messageId1.ToString();
                    admissionShedule.header.receiver = "ESZ";
                    admissionShedule.header.sender = "UDOD";
                    admissionShedule.header.createDateTime = DateTime.Now;
                    admissionShedule.shedule = new ArrayOfAdmissionSheduleRequestSlotSlot[100];

                    ArrayOfServiceListRequestServiceService[] array = new ArrayOfServiceListRequestServiceService[100];





                    while (batchcount < 100 && num < groups.Count)
                    {
                        var ecu1 = groups[num];

                        #region План приема

                        admissionPlan.planServices.planService[batchcount] = new AdmissionPlanRequestPlanServicesPlanService();
                        admissionPlan.planServices.planService[batchcount].id = ecu1.UdodAgeGroupId.ToString();
                        admissionPlan.planServices.planService[batchcount].serviceId = ecu1.ChildUnionId.ToString();
                        admissionPlan.planServices.planService[batchcount].startOfTraining = ecu1.StartLesson.HasValue ? ecu1.StartLesson.Value : new DateTime(2013, 09, 01);
                        admissionPlan.planServices.planService[batchcount].teacher = "";
                        //admissionPlan.planServices.planService[batchcount].placeCount = ecu1.MaxCountPupil;
                        //admissionPlan.planServices.planService[batchcount].placeCountSpecified = true;


                        #endregion
                        //newesz.Integration dd = new Integration();

                        #region расписание приема

                        admissionShedule.shedule[batchcount] = new ArrayOfAdmissionSheduleRequestSlotSlot();
                        admissionShedule.shedule[batchcount].dateStart = ecu1.StartClaim.HasValue ? ecu1.StartClaim.Value : new DateTime(2013, 08, 15, 08, 00, 00, 00);
                        admissionShedule.shedule[batchcount].dateEnd = ecu1.EndClaim.HasValue ? ecu1.EndClaim.Value : new DateTime(2014, 05, 31, 20, 00, 00, 00);
                        admissionShedule.shedule[batchcount].id = ecu1.UdodAgeGroupId.ToString();
                        // со статусом надо разбиратся
                        int statusId = 3;
                        
                        admissionShedule.shedule[batchcount].statusId = statusId;
                        admissionShedule.shedule[batchcount].dateStartSpecified = true;
                        admissionShedule.shedule[batchcount].dateEndSpecified = true;
                        admissionShedule.shedule[batchcount].planServiceId = ecu1.UdodAgeGroupId.ToString();
                        #endregion

                        num++;
                        batchcount++;
                    }
                    //request.services = array;

                    try
                    {
                        var result1 = client.AdmissionPlan(admissionPlan);
                        var result2 = client.AdmissionShedule(admissionShedule);
                        batchcount = 0;
                    }
                    catch (Exception)
                    {
                        //writer.WriteLine("Error " + messageId);
                        batchcount = 0;
                    }
                }
                #endregion
            }
            client.Close();
            //writer.Flush();
            //writer.Close();
        }
    }

    public static void SendBatchScheduleDeleteESZ(List<ExtendedChildUnion> groups)
    {
        //using (AgeGroupDb db = new AgeGroupDb())
        {
            IntegrationClient client = new IntegrationClient();
            client.Endpoint.Behaviors.Add(new InterceptorCustomBehavior());
            client.Open();

            //FileInfo file = new FileInfo("c:\\udod\\ErrorESZ" + Guid.NewGuid());
            //var writer = file.AppendText();
            int batchcount = 0;
            int num = 0;
            while (num < groups.Count)
            {
                #region Отправка в ЕСЗ
                using (UdodDb db1 = new UdodDb())
                {
                    var messageId = Guid.NewGuid();
                    var messageId1 = Guid.NewGuid();

                    AdmissionPlanRequest admissionPlan = new AdmissionPlanRequest();
                    admissionPlan.header = new packageHeader();
                    admissionPlan.header.id = messageId.ToString();
                    admissionPlan.header.receiver = "ESZ";
                    admissionPlan.header.sender = "UDOD";
                    admissionPlan.header.createDateTime = DateTime.Now;
                    admissionPlan.planServices = new AdmissionPlanRequestPlanServices();
                    admissionPlan.planServices.academicYear = 2013;
                    admissionPlan.planServices.planService = new AdmissionPlanRequestPlanServicesPlanService[100];



                    AdmissionSheduleRequest admissionShedule = new AdmissionSheduleRequest();
                    admissionShedule.header = new packageHeader();
                    admissionShedule.header.id = messageId1.ToString();
                    admissionShedule.header.receiver = "ESZ";
                    admissionShedule.header.sender = "UDOD";
                    admissionShedule.header.createDateTime = DateTime.Now;
                    admissionShedule.shedule = new ArrayOfAdmissionSheduleRequestSlotSlot[100];

                    ArrayOfServiceListRequestServiceService[] array = new ArrayOfServiceListRequestServiceService[100];





                    while (batchcount < 100 && num < groups.Count)
                    {
                        var ecu1 = groups[num];

                        #region План приема

                        admissionPlan.planServices.planService[batchcount] = new AdmissionPlanRequestPlanServicesPlanService();
                        admissionPlan.planServices.planService[batchcount].id = ecu1.ChildUnionId.ToString();
                        admissionPlan.planServices.planService[batchcount].serviceId = ecu1.ChildUnionId.ToString();
                        admissionPlan.planServices.planService[batchcount].startOfTraining = new DateTime(2013, 09, 01);
                        admissionPlan.planServices.planService[batchcount].teacher = "";
                        //admissionPlan.planServices.planService[batchcount].placeCount = ecu1.MaxCountPupil;
                        //admissionPlan.planServices.planService[batchcount].placeCountSpecified = true;


                        #endregion
                        //newesz.Integration dd = new Integration();

                        #region расписание приема

                        admissionShedule.shedule[batchcount] = new ArrayOfAdmissionSheduleRequestSlotSlot();
                        admissionShedule.shedule[batchcount].dateStart =  new DateTime(2013, 08, 15, 08, 00, 00, 00);
                        admissionShedule.shedule[batchcount].dateEnd = new DateTime(2014, 05, 31, 20, 00, 00, 00);
                        admissionShedule.shedule[batchcount].id = ecu1.ChildUnionId.ToString();
                        // со статусом надо разбиратся
                        int statusId = 3;
                        
                        admissionShedule.shedule[batchcount].statusId = statusId;
                        admissionShedule.shedule[batchcount].dateStartSpecified = true;
                        admissionShedule.shedule[batchcount].dateEndSpecified = true;
                        admissionShedule.shedule[batchcount].planServiceId = ecu1.ChildUnionId.ToString();
                        #endregion

                        num++;
                        batchcount++;
                    }
                    //request.services = array;

                    try
                    {
                        var result1 = client.AdmissionPlan(admissionPlan);
                        var result2 = client.AdmissionShedule(admissionShedule);
                        batchcount = 0;
                    }
                    catch (Exception)
                    {
                        //writer.WriteLine("Error " + messageId);
                        batchcount = 0;
                    }
                }
                #endregion
            }
            client.Close();
            //writer.Flush();
            //writer.Close();
        }
    }

    public static long ReestrAddZach(ExtendedPupil pupil, ExtendedChildUnion childUnion, ExtendedChildUnion childUnionZach, bool isUpdate)
    {
        NSIServiceClient client1 = new NSIServiceClient();
        client1.Endpoint.Behaviors.Add(new InterceptorCustomBehavior1());
        client1.Open();
        NSIRequestType type = new NSIRequestType();
        type.MessageData = new Service.NSI.ExtMessageDataType();
        type.MessageData.AppData = new ExtAppDataType();

        type.MessageData.AppData.context = new Context();
        type.MessageData.AppData.context.User = "UDOD_SOAP";
        type.MessageData.AppData.context.Password = "rlap0a3h";
        type.MessageData.AppData.context.Company = "dogm_nsi";

        ItemsChoiceType1[] itemsChoiceType1 = new ItemsChoiceType1[1];

        for (int k = 0; k < 1; k++)
        {
            itemsChoiceType1[k] = ItemsChoiceType1.item;
        }

        
        Item[] items = new Item[1];
        AttributeValue val = null;
        for (int k = 0; k < 1; k++)
        {
            Attribute[] atributes = new Attribute[9];
            int index = 0;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр наполнения классов PrimSpec/GUID обучаемого";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = pupil.ReestrGuid.ToString();
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр наполнения классов PrimSpec/Код класса";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = childUnion.ReestrCode.ToString();
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр наполнения классов PrimSpec/Вид класса";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = "группа УДОД";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр наполнения классов PrimSpec/Дата начала обучения";
            atributes[index].type = "DATE";

            val = new AttributeValue();
            val.Value = childUnion.DateStart.ToString("yyyy-MM-dd") + "T00:00:00";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр наполнения классов PrimSpec/Дата окончания обучения";
            atributes[index].type = "DATE";

            val = new AttributeValue();
            val.Value = childUnion.DateEnd.ToString("yyyy-MM-dd") + "T00:00:00";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;

            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр наполнения классов PrimSpec/Форма обучения";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = "Очная (дневная)";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр наполнения классов PrimSpec/Год обучения по программе";
            atributes[index].type = "INTEGER";

            val = new AttributeValue();
            val.Value = childUnionZach.PupilNumYear.ToString();
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр наполнения классов PrimSpec/Статус записи";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = "Новая запись";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр наполнения классов PrimSpec/Идентификация в системах КИС";
            atributes[index].type = "GROUPING";

            Attribute[] identificationAttr = new Attribute[4];
            int identificationIndex = 0;
            GroupValue[] identificationGroupValue = new GroupValue[1];
            identificationGroupValue[0] = new GroupValue();
            identificationAttr[identificationIndex] = new Attribute();
            identificationAttr[identificationIndex].name = "Система КИС";
            identificationAttr[identificationIndex].type = "LOOKUP_TABLE";
            var identificationVal = new AttributeValue();
            identificationVal.Value = "Зачисление в УДОД";
            identificationAttr[identificationIndex].value = new AttributeValue[1] { identificationVal };
            identificationIndex++;
            identificationAttr[identificationIndex] = new Attribute();
            identificationAttr[identificationIndex].name = "Идентификатор в системе";
            identificationAttr[identificationIndex].type = "STRING";
            identificationVal = new AttributeValue();
            identificationVal.Value = childUnionZach.PupilAgeGroupId.ToString();
            identificationAttr[identificationIndex].value = new AttributeValue[1] { identificationVal };
            identificationIndex++;
            identificationAttr[identificationIndex] = new Attribute();
            identificationAttr[identificationIndex].name = "Дата активации";
            identificationAttr[identificationIndex].type = "DATE";
            identificationVal = new AttributeValue();
            identificationVal.Value = childUnion.DateStart.ToString("yyyy-MM-dd") + "T00:00:00";
            identificationAttr[identificationIndex].value = new AttributeValue[1] { identificationVal };
            identificationIndex++;
            identificationAttr[identificationIndex] = new Attribute();
            identificationAttr[identificationIndex].name = "Дата деактивации";
            identificationAttr[identificationIndex].type = "DATE";
            identificationVal = new AttributeValue();
            identificationVal.Value = childUnion.DateEnd.ToString("yyyy-MM-dd") + "T00:00:00";
            identificationAttr[identificationIndex].value = new AttributeValue[1] { identificationVal };
            identificationIndex++;
            identificationGroupValue[0].attribute = identificationAttr;
            atributes[index].groupValue = identificationGroupValue;
            #endregion
            index++;


            Item item = new Item();
            item.containerName = "Реестр наполнения классов Catalog";
            item.attribute = atributes;

            if (isUpdate)
            {
                item.primaryKey = childUnion.ReestrPupilAgeGroupId;
            }

            items[k] = item;
        }

        type.MessageData.AppData.Items = items;
        type.MessageData.AppData.ItemsElementName = itemsChoiceType1;

        try
        {
            if (isUpdate)
            {
                var tmpvalue=client1.setItemAttributeValues(type);
            }
            else
            {
                var tmpvalue = client1.createItem(type);
                var primarykey = (tmpvalue.MessageData.AppData.Items.FirstOrDefault() as GeneralResponse).primaryKey;
                long? reestrCode = null;
                try
                {
                    reestrCode = Convert.ToInt64(primarykey[0]);
                }
                catch (Exception)
                {
                }
                if (reestrCode.HasValue)
                {
                    using (PupilsDb db = new PupilsDb())
                    {
                        db.UpdatePupilZachReestrCode(childUnionZach.PupilAgeGroupId, reestrCode.Value);
                    }
                    return reestrCode.Value;
                }
            }
        }
        catch (Exception e1)
        {
            string str = e1.Message;
        }



        client1.Close();
        return -1;
    }

    public static long ReestrAddDO(ExtendedChildUnion childUnion, bool isUpdate, List<ExtendedChildUnionTeacher> teachers)
    {
        NSIServiceClient client1 = new NSIServiceClient();
        client1.Endpoint.Behaviors.Add(new InterceptorCustomBehavior1());
        client1.Open();
        NSIRequestType type = new NSIRequestType();
        type.MessageData = new ExtMessageDataType();
        type.MessageData.AppData = new ExtAppDataType();

        type.MessageData.AppData.context = new Context();
        type.MessageData.AppData.context.User = "UDOD_SOAP";
        type.MessageData.AppData.context.Password = "rlap0a3h";
        type.MessageData.AppData.context.Company = "dogm_nsi";

        ItemsChoiceType1[] itemsChoiceType1 = new ItemsChoiceType1[1];

        for (int k = 0; k < 1; k++)
        {
            itemsChoiceType1[k] = ItemsChoiceType1.item;
        }

        Item[] items = new Item[1];
        AttributeValue val = null;
        for (int k = 0; k < 1; k++)
        {
            Attribute[] atributes = new Attribute[22];
            int index = 0;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/GUID в источнике";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = childUnion.ChildUnionId.ToString();
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/GUID ОУ";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = childUnion.GuidOU;
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Учебный период";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = "учебный год";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Дата начала периода";
            atributes[index].type = "DATE";

            val = new AttributeValue();
            val.Value = childUnion.DateStart.ToString("yyyy-MM-dd") + "T00:00:00";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Дата окончания периода";
            atributes[index].type = "DATE";

            val = new AttributeValue();
            val.Value = childUnion.DateEnd.ToString("yyyy-MM-dd") + "T00:00:00";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;

            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Название класса";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = childUnion.Name;
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Тип объединения";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = "группа УДОД";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Вид класса";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = "группа УДОД";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Форма обучения";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = "Очная (дневная)";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Профиль обучения";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = childUnion.Profile.Name;
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Специализация";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.occurrence = 0;
            val.occurrenceSpecified = true;
            val.Value = childUnion.Program.Name;
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Вид деятельности";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = childUnion.Section.Name;
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Возраст обучающихся от";
            atributes[index].type = "INTEGER";

            val = new AttributeValue();
            val.Value = childUnion.AgeStart.ToString();
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Возраст обучающихся до";
            atributes[index].type = "INTEGER";

            val = new AttributeValue();
            val.Value = childUnion.AgeEnd.ToString();
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Описание программы обучения";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = childUnion.ProgramService;
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Форма финансирования обучения";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = childUnion.BudgetTypes.Count(i => i.DictTypeBudgetId == 2) > 0 ? "Бюджет" : "Договор";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Учебные дни недели";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = "7-дневная неделя";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Классный руководитель";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = teachers.Count > 0 ? teachers.FirstOrDefault().TeacherName : "";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Признак дополнительной образовательной услуги";
            atributes[index].type = "FLAG";

            val = new AttributeValue();
            val.Value = "true";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Признак бесплатности обучения";
            atributes[index].type = "FLAG";

            val = new AttributeValue();
            val.Value = childUnion.BudgetTypes.Count(i => i.DictTypeBudgetId == 2) > 0 ? "true" : "false";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Статус записи";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = "Новая запись";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр классов PrimSpec/Идентификация в системах КИС";
            atributes[index].type = "GROUPING";

            Attribute[] identificationAttr = new Attribute[4];
            int identificationIndex = 0;
            GroupValue[] identificationGroupValue = new GroupValue[1];
            identificationGroupValue[0] = new GroupValue();
            identificationAttr[identificationIndex] = new Attribute();
            identificationAttr[identificationIndex].name = "Система КИС";
            identificationAttr[identificationIndex].type = "LOOKUP_TABLE";
            var identificationVal = new AttributeValue();
            identificationVal.Value = "Зачисление в УДОД";
            identificationAttr[identificationIndex].value = new AttributeValue[1] { identificationVal };
            identificationIndex++;
            identificationAttr[identificationIndex] = new Attribute();
            identificationAttr[identificationIndex].name = "Идентификатор в системе";
            identificationAttr[identificationIndex].type = "STRING";
            identificationVal = new AttributeValue();
            identificationVal.Value = childUnion.ChildUnionId.ToString();
            identificationAttr[identificationIndex].value = new AttributeValue[1] { identificationVal };
            identificationIndex++;
            identificationAttr[identificationIndex] = new Attribute();
            identificationAttr[identificationIndex].name = "Дата активации";
            identificationAttr[identificationIndex].type = "DATE";
            identificationVal = new AttributeValue();
            identificationVal.Value = childUnion.DateStart.ToString("yyyy-MM-dd") + "T00:00:00";
            identificationAttr[identificationIndex].value = new AttributeValue[1] { identificationVal };
            identificationIndex++;
            identificationAttr[identificationIndex] = new Attribute();
            identificationAttr[identificationIndex].name = "Дата деактивации";
            identificationAttr[identificationIndex].type = "DATE";
            identificationVal = new AttributeValue();
            identificationVal.Value = childUnion.DateEnd.ToString("yyyy-MM-dd") + "T00:00:00";
            identificationAttr[identificationIndex].value = new AttributeValue[1] { identificationVal };
            identificationIndex++;
            identificationGroupValue[0].attribute = identificationAttr;
            atributes[index].groupValue = identificationGroupValue;
            #endregion
            index++;


            Item item = new Item();
            item.containerName = "Реестр классов Catalog";
            item.attribute = atributes;

            if (isUpdate) item.primaryKey = childUnion.ReestrCode;

            items[k] = item;
        }

        type.MessageData.AppData.Items = items;
        type.MessageData.AppData.ItemsElementName = itemsChoiceType1;

        try
        {
            if (isUpdate)
            {
                var tmpvalue = client1.setItemAttributeValues(type);
                var primarykey = (tmpvalue.MessageData.AppData.Items.FirstOrDefault() as GeneralResponse).primaryKey;
            }
            else
            {
                var tmpvalue= client1.createItem(type);
                var primarykey = (tmpvalue.MessageData.AppData.Items.FirstOrDefault() as GeneralResponse).primaryKey;
                long? reestrCode = null;
                try
                {
                    reestrCode = Convert.ToInt64(primarykey[0]);
                }
                catch (Exception)
                {
                }
                if (reestrCode.HasValue)
                {
                    using (ChildUnionDb db = new ChildUnionDb())
                    {
                        db.UpdateChildUnionReestrCode(childUnion.ChildUnionId, reestrCode.Value);
                    }
                    return reestrCode.Value;
                }
            }
            
        }
        catch (Exception e1)
        {
            string str = e1.Message;
        }



        client1.Close();

        return -1;
    }

    public static void ReestrDeletePupil(string reestrCodeUdod)
    {
        NSIServiceClient client1 = new NSIServiceClient();
        client1.Endpoint.Behaviors.Add(new InterceptorCustomBehavior1());
        client1.Open();
        NSIRequestType type = new NSIRequestType();
        type.MessageData = new ExtMessageDataType();
        type.MessageData.AppData = new ExtAppDataType();

        type.MessageData.AppData.context = new Context();
        type.MessageData.AppData.context.User = "UDOD_SOAP";
        type.MessageData.AppData.context.Password = "rlap0a3h";
        type.MessageData.AppData.context.Company = "dogm_nsi";

        ItemsChoiceType1[] itemsChoiceType1 = new ItemsChoiceType1[1];

        for (int k = 0; k < 1; k++)
        {
            itemsChoiceType1[k] = ItemsChoiceType1.item;
        }

        Item[] items = new Item[1];
        AttributeValue val = null;
        for (int k = 0; k < 1; k++)
        {
            Attribute[] atributes = new Attribute[1];
            int index = 0;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Статус записи";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = "Удален из системы";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            

            Item item = new Item();
            item.containerName = "Реестр контингента УДОД Catalog";
            item.attribute = atributes;

            item.primaryKey = reestrCodeUdod;

            items[k] = item;
        }


        type.MessageData.AppData.Items = items;
        type.MessageData.AppData.ItemsElementName = itemsChoiceType1;

        try
        {
            var tmpvalue = client1.deleteItem(type);
        }
        catch (Exception)
        {
        }
        client1.Close();
    }

    public static void ReestrDeletePupilZach(string reestrCodeZach)
    {
        NSIServiceClient client1 = new NSIServiceClient();
        client1.Endpoint.Behaviors.Add(new InterceptorCustomBehavior1());
        client1.Open();
        NSIRequestType type = new NSIRequestType();
        type.MessageData = new ExtMessageDataType();
        type.MessageData.AppData = new ExtAppDataType();

        type.MessageData.AppData.context = new Context();
        type.MessageData.AppData.context.User = "UDOD_SOAP";
        type.MessageData.AppData.context.Password = "rlap0a3h";
        type.MessageData.AppData.context.Company = "dogm_nsi";

        ItemsChoiceType1[] itemsChoiceType1 = new ItemsChoiceType1[1];

        for (int k = 0; k < 1; k++)
        {
            itemsChoiceType1[k] = ItemsChoiceType1.item;
        }

        Item[] items = new Item[1];
        AttributeValue val = null;
        for (int k = 0; k < 1; k++)
        {
            Attribute[] atributes = new Attribute[1];
            int index = 0;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр наполнения классов PrimSpec/Статус записи";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = "Удален из системы";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;


            Item item = new Item();
            item.containerName = "Реестр наполнения классов Catalog";
            item.attribute = atributes;

            item.primaryKey = reestrCodeZach;

            items[k] = item;
        }


        type.MessageData.AppData.Items = items;
        type.MessageData.AppData.ItemsElementName = itemsChoiceType1;

        try
        {
            var tmpvalue = client1.deleteItem(type);
        }
        catch (Exception)
        {
        }
        client1.Close();
    }

    public static string ReestrAddPupil(ExtendedPupil pupil, bool isUpdate, ref Guid? reestrGuid)
    {
        NSIServiceClient client1 = new NSIServiceClient();
        client1.Endpoint.Behaviors.Add(new InterceptorCustomBehavior1());
        client1.Open();
        NSIRequestType type = new NSIRequestType();
        type.MessageData = new ExtMessageDataType();
        type.MessageData.AppData = new ExtAppDataType();

        type.MessageData.AppData.context = new Context();
        type.MessageData.AppData.context.User = "UDOD_SOAP";
        type.MessageData.AppData.context.Password = "rlap0a3h";
        type.MessageData.AppData.context.Company = "dogm_nsi";

        ItemsChoiceType1[] itemsChoiceType1 = new ItemsChoiceType1[1];

        for (int k = 0; k < 1; k++)
        {
            itemsChoiceType1[k] = ItemsChoiceType1.item;
        }

        Item[] items = new Item[1];
        AttributeValue val = null;
        for (int k = 0; k < 1; k++)
        {
            Attribute[] atributes = new Attribute[18];
            int index = 0;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Фамилия";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = pupil.LastName;
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Имя";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = pupil.FirstName;
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Отчество";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = pupil.MiddleName;
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Адрес электронной почты";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = pupil.EMail;
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Пол";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = pupil.Sex == 1 ? "Женский" : "Мужской";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            if (!string.IsNullOrEmpty(pupil.PhoneNumber.Trim()))
            {
                #region один элемент Item

                atributes[index] = new Attribute();
                atributes[index].name = "Реестр контингента УДОД PrimSpec/Телефоны";
                atributes[index].type = "GROUPING";

                val = new AttributeValue();
                GroupValue[] groupValue = new GroupValue[1];
                Attribute[] occur = new Attribute[2];
                occur[0] = new Attribute();
                occur[0].name = "Номер телефона";
                occur[0].type = "STRING";
                var occurVal = new AttributeValue();
                occurVal.Value = pupil.PhoneNumber;
                occur[0].value = new AttributeValue[1] {occurVal};
                ;

                occur[1] = new Attribute();
                occur[1].name = "Тип телефона";
                occur[1].type = "STRING";
                occurVal = new AttributeValue();
                occurVal.Value = "Мобильный";
                occur[1].value = new AttributeValue[1] {occurVal};
                ;
                groupValue[0].occurrence = 1;
                groupValue[0].occurrenceSpecified = true;
                val.occurrence = 1;
                val.occurrenceSpecified = true;
                groupValue[0] = new GroupValue();
                groupValue[0].attribute = occur;
                atributes[index].groupValue = groupValue;

                #endregion

                index++;
            }

            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Дата рождения";
            atributes[index].type = "DATE";

            val = new AttributeValue();
            val.Value = pupil.Birthday.ToString("yyyy-MM-dd") + "T00:00:00";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Школа";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = pupil.SchoolCode;
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Класс";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = pupil.ClassName;
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Серия документа";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = pupil.Passport.Series;
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Номер документа";
            atributes[index].type = "STRING";

            val = new AttributeValue();
            val.Value = pupil.Passport.Number;
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Дата выдачи документа";
            atributes[index].type = "DATE";

            val = new AttributeValue();
            val.Value = pupil.Passport.IssueDate.Value.ToString("yyyy-MM-dd") + "T00:00:00";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Тип документа";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = pupil.Passport.PassportType.PassportTypeId == 3 ? "Свидетельство РФ" : pupil.Passport.PassportType.PassportTypeId == 2 ? "Паспорт РФ" : "Иной документ";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            if (pupil.Addresses.Count > 0)
            {
                var address = pupil.Addresses.FirstOrDefault();

                #region один элемент Item

                atributes[index] = new Attribute();
                atributes[index].name = "Реестр контингента УДОД PrimSpec/Адрес";
                atributes[index].type = "GROUPING";
                
                val = new AttributeValue();

                Attribute[] addressAttr = new Attribute[11];
                int addressIndex = 0;
                GroupValue[] addressGroupValue = new GroupValue[1];
                addressGroupValue[0] = new GroupValue();
                addressGroupValue[0].occurrence = 0;
                addressGroupValue[0].occurrenceSpecified = true;

                addressAttr[addressIndex] = new Attribute();
                addressAttr[addressIndex].name = "Полный адрес";
                addressAttr[addressIndex].type = "STRING";
                var addressVal = new AttributeValue();
                addressVal.Value = address.AddressStr;
                addressAttr[addressIndex].value = new AttributeValue[1] { addressVal };
                addressIndex++;
                addressAttr[addressIndex] = new Attribute();
                addressAttr[addressIndex].name = "Почтовый индекс";
                addressAttr[addressIndex].type = "STRING";
                addressVal = new AttributeValue();
                addressVal.Value = address.PostIndex;
                addressAttr[addressIndex].value = new AttributeValue[1] { addressVal };
                addressIndex++;
                addressAttr[addressIndex] = new Attribute();
                addressAttr[addressIndex].name = "Номер дома";
                addressAttr[addressIndex].type = "STRING";
                addressVal = new AttributeValue();
                addressVal.Value = address.HouseNumber;
                addressAttr[addressIndex].value = new AttributeValue[1] { addressVal };
                addressIndex++;
                addressAttr[addressIndex] = new Attribute();
                addressAttr[addressIndex].name = "Дробь дома";
                addressAttr[addressIndex].type = "STRING";
                addressVal = new AttributeValue();
                addressVal.Value = address.Fraction;
                addressAttr[addressIndex].value = new AttributeValue[1] { addressVal };
                addressIndex++;
                addressAttr[addressIndex] = new Attribute();
                addressAttr[addressIndex].name = "Корпус дома";
                addressAttr[addressIndex].type = "STRING";
                addressVal = new AttributeValue();
                addressVal.Value = address.Housing;
                addressAttr[addressIndex].value = new AttributeValue[1] { addressVal };
                addressIndex++;
                addressAttr[addressIndex] = new Attribute();
                addressAttr[addressIndex].name = "Строение дома";
                addressAttr[addressIndex].type = "STRING";
                addressVal = new AttributeValue();
                addressVal.Value = address.Building;
                addressAttr[addressIndex].value = new AttributeValue[1] { addressVal };
                addressIndex++;
                addressAttr[addressIndex] = new Attribute();
                addressAttr[addressIndex].name = "Номер квартиры";
                addressAttr[addressIndex].type = "STRING";
                addressVal = new AttributeValue();
                addressVal.Value = address.FlatNumber;
                addressAttr[addressIndex].value = new AttributeValue[1] { addressVal };
                addressIndex++;
                addressAttr[addressIndex] = new Attribute();
                addressAttr[addressIndex].name = "Тип адреса";
                addressAttr[addressIndex].type = "LOOKUP_TABLE";
                addressVal = new AttributeValue();
                addressVal.Value = "Постоянная регистрация";
                addressAttr[addressIndex].value = new AttributeValue[1] { addressVal };
                addressIndex++;
                addressAttr[addressIndex] = new Attribute();
                addressAttr[addressIndex].name = "КЛАДР.id";
                addressAttr[addressIndex].type = "STRING";
                addressVal = new AttributeValue();
                addressVal.Value = address.KladrCode;
                addressAttr[addressIndex].value = new AttributeValue[1] { addressVal };
                addressIndex++;
                addressAttr[addressIndex] = new Attribute();
                addressAttr[addressIndex].name = "БТИ.street_id";
                addressAttr[addressIndex].type = "INTEGER";
                addressVal = new AttributeValue();
                addressVal.Value = address.BtiCode.HasValue ? address.BtiCode.Value.ToString() : "";
                addressAttr[addressIndex].value = new AttributeValue[1] { addressVal };
                addressIndex++;
                addressAttr[addressIndex] = new Attribute();
                addressAttr[addressIndex].name = "БТИ.house_id";
                addressAttr[addressIndex].type = "INTEGER";
                addressVal = new AttributeValue();
                addressVal.Value = address.BtiHouseCode.HasValue ? address.BtiHouseCode.Value.ToString() : "";
                addressAttr[addressIndex].value = new AttributeValue[1] { addressVal };
                addressIndex++;
                addressGroupValue[0].attribute = addressAttr;
                atributes[index].groupValue = addressGroupValue;

                #endregion

                index++;
            }
            if (pupil.Parents.Count(i2=>i2.UserId!=pupil.UserId) > 0)
            {
                #region один элемент Item

                atributes[index] = new Attribute();
                atributes[index].name = "Реестр контингента УДОД PrimSpec/Представитель";
                atributes[index].type = "GROUPING";
                var parent = pupil.Parents.FirstOrDefault(i2 => i2.UserId != pupil.UserId);
                val = new AttributeValue();
                GroupValue[] parentGroupValue = new GroupValue[1];
                parentGroupValue[0] = new GroupValue();
                parentGroupValue[0].occurrence = 0;
                parentGroupValue[0].occurrenceSpecified = true;
                Attribute[] parentAttr = new Attribute[9];
                int parentIndex = 0;
                parentAttr[parentIndex] = new Attribute();
                parentAttr[parentIndex].name = "Фамилия";
                parentAttr[parentIndex].type = "STRING";
                var parentVal = new AttributeValue();
                parentVal.Value = parent.LastName;
                parentAttr[parentIndex].value = new AttributeValue[1] { parentVal };
                parentIndex++;
                parentAttr[parentIndex] = new Attribute();
                parentAttr[parentIndex].name = "Имя";
                parentAttr[parentIndex].type = "STRING";
                parentVal = new AttributeValue();
                parentVal.Value = parent.FirstName;
                parentAttr[parentIndex].value = new AttributeValue[1] { parentVal };
                parentIndex++;
                parentAttr[parentIndex] = new Attribute();
                parentAttr[parentIndex].name = "Отчество";
                parentAttr[parentIndex].type = "STRING";
                parentVal = new AttributeValue();
                parentVal.Value = parent.MiddleName;
                parentAttr[parentIndex].value = new AttributeValue[1] { parentVal };
                parentIndex++;
                parentAttr[parentIndex] = new Attribute();
                parentAttr[parentIndex].name = "Адрес электронной почты";
                parentAttr[parentIndex].type = "STRING";
                parentVal = new AttributeValue();
                parentVal.Value = parent.EMail;
                parentAttr[parentIndex].value = new AttributeValue[1] { parentVal };
                parentIndex++;

                parentAttr[parentIndex] = new Attribute();
                parentAttr[parentIndex].name = "Телефоны";
                parentAttr[parentIndex].type = "GROUPING";

                val = new AttributeValue();
                GroupValue[] parentPhoneGroupValue = new GroupValue[1];
                parentPhoneGroupValue[0] = new GroupValue();
                parentPhoneGroupValue[0].occurrence = 0;
                parentPhoneGroupValue[0].occurrenceSpecified = true;
                Attribute[] parentoccur = new Attribute[2];
                parentoccur[0] = new Attribute();
                parentoccur[0].name = "Номер телефона";
                parentoccur[0].type = "STRING";
                var occurVal = new AttributeValue();
                occurVal.Value = parent.PhoneNumber;
                parentoccur[0].value = new AttributeValue[1] { occurVal }; ;

                parentoccur[1] = new Attribute();
                parentoccur[1].name = "Тип телефона";
                parentoccur[1].type = "STRING";
                occurVal = new AttributeValue();
                occurVal.Value = "Мобильный";
                parentoccur[1].value = new AttributeValue[1] { occurVal }; ;
                parentPhoneGroupValue[0].attribute = parentoccur;
                parentAttr[parentIndex].groupValue = parentPhoneGroupValue;
                parentIndex++;

                parentAttr[parentIndex] = new Attribute();
                parentAttr[parentIndex].name = "Серия документа";
                parentAttr[parentIndex].type = "STRING";
                parentVal = new AttributeValue();
                parentVal.Value = parent.Passport.Series;
                parentAttr[parentIndex].value = new AttributeValue[1] { parentVal };
                parentIndex++;
                parentAttr[parentIndex] = new Attribute();
                parentAttr[parentIndex].name = "Номер документа";
                parentAttr[parentIndex].type = "STRING";
                parentVal = new AttributeValue();
                parentVal.Value = parent.Passport.Number;
                parentAttr[parentIndex].value = new AttributeValue[1] { parentVal };
                parentIndex++;
                parentAttr[parentIndex] = new Attribute();
                parentAttr[parentIndex].name = "Дата выдачи документа";
                parentAttr[parentIndex].type = "DATE";
                parentVal = new AttributeValue();
                parentVal.Value = parent.Passport.IssueDate.Value.ToString("yyyy-MM-dd") + "T00:00:00";
                parentAttr[parentIndex].value = new AttributeValue[1] { parentVal };
                parentIndex++;
                parentAttr[parentIndex] = new Attribute();
                parentAttr[parentIndex].name = "Тип документа";
                parentAttr[parentIndex].type = "STRING";
                parentVal = new AttributeValue();
                parentVal.Value = parent.Passport.PassportType.PassportTypeId == 3 ? "Свидетельство РФ" : parent.Passport.PassportType.PassportTypeId == 2 ? "Паспорт РФ" : "Иной документ";
                parentAttr[parentIndex].value = new AttributeValue[1] { parentVal };
                parentIndex++;

                parentGroupValue[0].attribute = parentAttr;

                atributes[index].groupValue = parentGroupValue;

                #endregion
                index++;
            }
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Статус записи";
            atributes[index].type = "LOOKUP_TABLE";

            val = new AttributeValue();
            val.Value = "Новая запись";
            atributes[index].value = new AttributeValue[1] { val };
            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new Attribute();
            atributes[index].name = "Реестр контингента УДОД PrimSpec/Идентификация в системах КИС";
            atributes[index].type = "GROUPING";

            Attribute[] identificationAttr = new Attribute[4];
            int identificationIndex = 0;
            GroupValue[] identificationGroupValue = new GroupValue[1];
            identificationGroupValue[0] = new GroupValue();
            identificationAttr[identificationIndex] = new Attribute();
            identificationAttr[identificationIndex].name = "Система КИС";
            identificationAttr[identificationIndex].type = "LOOKUP_TABLE";
            var identificationVal = new AttributeValue();
            identificationVal.Value = "Зачисление в УДОД";
            identificationAttr[identificationIndex].value = new AttributeValue[1] { identificationVal };
            identificationIndex++;
            identificationAttr[identificationIndex] = new Attribute();
            identificationAttr[identificationIndex].name = "Идентификатор в системе";
            identificationAttr[identificationIndex].type = "STRING";
            identificationVal = new AttributeValue();
            identificationVal.Value = pupil.UserId.ToString();
            identificationAttr[identificationIndex].value = new AttributeValue[1] { identificationVal };
            identificationIndex++;
            identificationAttr[identificationIndex] = new Attribute();
            identificationAttr[identificationIndex].name = "Дата активации";
            identificationAttr[identificationIndex].type = "DATE";
            identificationVal = new AttributeValue();
            identificationVal.Value = (new DateTime(2013, 09, 01)).ToString("yyyy-MM-dd") + "T00:00:00";
            identificationAttr[identificationIndex].value = new AttributeValue[1] { identificationVal };
            identificationIndex++;
            identificationAttr[identificationIndex] = new Attribute();
            identificationAttr[identificationIndex].name = "Дата деактивации";
            identificationAttr[identificationIndex].type = "DATE";
            identificationVal = new AttributeValue();
            identificationVal.Value = (new DateTime(2014, 05, 28)).ToString("yyyy-MM-dd") + "T00:00:00";
            identificationAttr[identificationIndex].value = new AttributeValue[1] { identificationVal };
            identificationIndex++;
            identificationGroupValue[0].attribute = identificationAttr;
            atributes[index].groupValue = identificationGroupValue;
            #endregion
            index++;
            

            Item item = new Item();
            item.containerName = "Реестр контингента УДОД Catalog";
            item.attribute = atributes;

            if (isUpdate) item.primaryKey = pupil.ReestrUdodCode;

            items[k] = item;
        }

        type.MessageData.AppData.Items = items;
        type.MessageData.AppData.ItemsElementName = itemsChoiceType1;

        try
        {
            if (isUpdate)
            {
                var tmpvalue = client1.setItemAttributeValues(type);
                //var primarykey = (tmpvalue.MessageData.AppData.Items.FirstOrDefault() as GeneralResponse).primaryKey;
                //var ReestrGuid = (tmpvalue.MessageData.AppData.Items.FirstOrDefault() as GeneralResponse).attribute[0].value;
                
            }
            else
            {
                NSIResponseType tmpvalue = client1.createItem(type);
                //throw new Exception("Ох как мне жаль");
                var primarykey = (tmpvalue.MessageData.AppData.Items.FirstOrDefault() as GeneralResponse).primaryKey;
                var ReestrGuid = (tmpvalue.MessageData.AppData.Items.FirstOrDefault() as GeneralResponse).attribute[2].value;
                long? reestrUdodCode = null;
                try
                {
                    reestrUdodCode = Convert.ToInt64(primarykey[0]);
                }
                catch (Exception e2)
                {
                    throw new Exception(e2.Message+ "_1");
                }
                //Guid? reestrGuid = null;
                try
                {
                    reestrGuid = new Guid(ReestrGuid[0].Value);
                }
                catch (Exception e2)
                {
                    throw new Exception(e2.Message+"_2");
                }

                if (reestrUdodCode.HasValue)
                {
                    using (PupilsDb db = new PupilsDb())
                    {
                        db.UpdatePupilReestrCode(pupil.UserId, null, reestrUdodCode, reestrGuid, "", null,null,null);
                    }
                    return reestrUdodCode.Value.ToString();
                }
            }
        }
        catch (Exception e1)
        {
            string str = e1.Message;
            return e1.Message;
            throw new Exception(e1.Message);
        }



        client1.Close();

        return "";
    }

    public static bool SendClaimInESZ(ExtendedPupil pupil, ExtendedPupil parent, long claimId, long childUnionId, string bookingId)
    {
        var messageId = Guid.NewGuid();
        IntegrationClient client = new IntegrationClient();
        client.Endpoint.Behaviors.Add(new InterceptorCustomBehavior());

        #region План приема

        RequestRequest request = new RequestRequest();
        request.header = new packageHeader();
        request.header.id = messageId.ToString();
        request.header.receiver = "ESZ";
        request.header.sender = "UDOD";
        request.header.createDateTime = DateTime.Now;

        request.data = new RequestRequestData();
        request.data.bookingUid = bookingId.ToString();
        request.data.applicant=  new RequestRequestDataApplicant();
        request.data.applicant.lastName = parent.LastName;
        request.data.applicant.firstName = parent.FirstName;
        request.data.applicant.middleName = parent.MiddleName;
        request.data.applicant.id = parent.UserId.ToString();

        request.data.child= new RequestRequestDataChild();
        request.data.child.lastName = pupil.LastName;
        request.data.child.firstName = pupil.FirstName;
        request.data.child.middleName = pupil.MiddleName;
        request.data.child.dateOfBirth = pupil.Birthday;

        request.data.child.Item = new object();
        request.data.child.ItemElementName = pupil.Sex == 0 ? ItemChoiceType4.male : ItemChoiceType4.female;

        request.data.child.Item1 = new object();
        request.data.child.Item1ElementName = pupil.Passport.PassportType.PassportTypeId == 3
                                                  ? Item1ChoiceType.birthCertificate
                                                  : Item1ChoiceType.passport;
        request.data.child.series = pupil.Passport.Series;
        request.data.child.number = pupil.Passport.Number;
        request.data.child.issued = pupil.Passport.IssueDate.Value;
        request.data.child.issuedBy = "";
        request.data.child.school = pupil.SchoolName;
        request.data.child.contingentId = string.IsNullOrEmpty(pupil.ReestrCode) ? "" : pupil.ReestrCode;
        request.data.email = pupil.EMail;
        request.data.phone = pupil.PhoneNumber;
        request.data.requestUid = claimId.ToString();
        request.data.serviceUid = childUnionId.ToString();
        request.data.admissionSheduleId = childUnionId.ToString();
        try
        {
            var result = client.Request(request);
        }
        catch (Exception)
        {
            return false;
        }

        #endregion

        return true;
    }


    public static void GetNSIBatchPupilDoc(ref XmlDocument xmlDoc, List<ExtendedPupil> pupils)
    {
        XmlNode deleteNode = null;
        XmlNode deleteNode1 = null;
        XmlNode deleteParentNode = null;
        XmlNode childtmp = null;
        CultureInfo culture = new CultureInfo("en-us");
        foreach (XmlNode child in xmlDoc.ChildNodes)
        {
            foreach (XmlNode child1 in child.ChildNodes)
            {
                foreach (XmlNode child2 in child1.ChildNodes)
                {
                    foreach (XmlNode child3 in child2.ChildNodes)
                    {
                        foreach (XmlNode child4 in child3.ChildNodes)
                        {

                            if (child4.Name == "Id" && child3.Name == "header")
                            {
                                child4.InnerXml = Guid.NewGuid().ToString();
                            }
                            if (child4.Name == "smev:AppData")
                            {
                                deleteNode1 = child4;
                                childtmp = child4.CloneNode(true);
                                foreach (XmlNode node in childtmp)
                                {
                                    if (node.Name == "nsi:item")
                                    {
                                        deleteNode = node;
                                    }
                                }
                                foreach (XmlNode child5 in child4.ChildNodes)
                                {
                                    if (child5.Name=="nsi:item")
                                    {
                                        //deleteNode = child5;

                                        foreach (var pupil in pupils)
                                        {
                                            XmlNode item = child5.CloneNode(true);
                                            int k = 1;
                                            foreach (XmlNode child6 in item.ChildNodes)
                                            {
                                                if (child6.Name == "nsi:attribute")
                                                {
                                                    if (k == 1) child6.ChildNodes[2].InnerXml = pupil.LastName;
                                                    if (k == 2) child6.ChildNodes[2].InnerXml = pupil.FirstName;
                                                    if (k == 3) child6.ChildNodes[2].InnerXml = pupil.MiddleName;
                                                    if (k == 4) child6.ChildNodes[2].InnerXml = string.IsNullOrEmpty(pupil.EMail) ? "" : pupil.EMail;
                                                    if (k == 5) child6.ChildNodes[2].InnerXml = pupil.Sex==1?"Женский":"Мужской";
                                                    // телефоны
                                                    if (k == 6)
                                                    {
                                                        // список 
                                                        child6.ChildNodes[2].ChildNodes[0].ChildNodes[2].InnerXml = string.IsNullOrEmpty(pupil.PhoneNumber) ? "" : pupil.PhoneNumber;
                                                        child6.ChildNodes[2].ChildNodes[1].ChildNodes[2].InnerXml = "Рабочий";
                                                    }

                                                    if (k == 7) child6.ChildNodes[2].InnerXml = pupil.Birthday.ToString("yyyy-MM-dd") + "T00:00:00";
                                                    if (k == 8) child6.ChildNodes[2].InnerXml = string.IsNullOrEmpty(pupil.SchoolCode)?"":pupil.SchoolCode;
                                                    if (k == 9) child6.ChildNodes[2].InnerXml = string.IsNullOrEmpty(pupil.ClassName) ? "" : pupil.ClassName;
                                                    if (k == 10) child6.ChildNodes[2].InnerXml = pupil.Passport.Series;
                                                    if (k == 11) child6.ChildNodes[2].InnerXml = pupil.Passport.Number;
                                                    if (k == 12) child6.ChildNodes[2].InnerXml = pupil.Passport.IssueDate.Value.ToString("yyyy-MM-dd") + "T00:00:00";
                                                    if (k == 13) child6.ChildNodes[2].InnerXml = pupil.Passport.PassportType.PassportTypeId == 3 ? "Свидетельство РФ" : pupil.Passport.PassportType.PassportTypeId == 2 ? "Паспорт РФ" : "Иной документ";

                                                    // Guid в реестре 
                                                    if (k == 14) child6.ChildNodes[2].InnerXml = "";
                                                    // адрес
                                                    if (k == 15)
                                                    {
                                                        var address = pupil.Addresses.FirstOrDefault();
                                                        for(int z=0;z<11;z++)
                                                        {
                                                            if (z == 0) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.AddressStr;
                                                            if (z == 1) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = string.IsNullOrEmpty(address.PostIndex) ? "101000" : address.PostIndex;
                                                            if (z == 2) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.HouseNumber;
                                                            if (z == 3) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.Fraction;
                                                            if (z == 4) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.Building;
                                                            if (z == 5) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.Housing;
                                                            if (z == 6) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.FlatNumber;
                                                            if (z == 7) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = "Постоянная регистрация";
                                                            if (z == 8) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = string.IsNullOrEmpty(address.KladrCode) ? "" : address.KladrCode;
                                                            if (z == 9) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.BtiCode.HasValue? address.BtiCode.Value.ToString():"";
                                                            if (z == 10) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.BtiHouseCode.HasValue ? address.BtiHouseCode.Value.ToString() : "";
                                                        }
                                                        
                                                    }
                                                    // представитель
                                                    if (k==16)
                                                    {
                                                        var parent = pupil.Parents.FirstOrDefault();
                                                        if (parent == null)
                                                            deleteParentNode = child6;
                                                        for(int z=0;z<9;z++)
                                                        {
                                                            if (z == 0) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = parent.LastName;
                                                            if (z == 1) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = parent.FirstName;
                                                            if (z == 2) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = parent.MiddleName;
                                                            if (z == 3) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = string.IsNullOrEmpty(parent.EMail)?"":parent.EMail;
                                                            // телефоны
                                                            if (z == 4)
                                                            {
                                                                child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].ChildNodes[0].ChildNodes[2].InnerXml = string.IsNullOrEmpty(parent.PhoneNumber) ? "" : parent.PhoneNumber;
                                                                child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].ChildNodes[1].ChildNodes[2].InnerXml = "Рабочий";
                                                            }
                                                            if (z == 5) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = parent.Passport.Series;
                                                            if (z == 6) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = parent.Passport.Number;
                                                            if (z == 7) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = parent.Passport.IssueDate.Value.ToString("yyyy-MM-dd") + "T00:00:00";
                                                            if (z == 8) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = parent.Passport.PassportType.PassportTypeId == 3 ? "Свидетельство РФ" : parent.Passport.PassportType.PassportTypeId == 2 ? "Паспорт РФ" : "Иной документ"; ;
                                                        }
                                                    }
                                                    if (k == 17) child6.ChildNodes[2].InnerXml = "Новая запись";
                                                    if (k == 18)
                                                    {
                                                        child6.ChildNodes[2].ChildNodes[0].ChildNodes[2].InnerXml = "Зачисление в УДОД";
                                                        child6.ChildNodes[2].ChildNodes[1].ChildNodes[2].InnerXml = pupil.UserId.ToString();
                                                        child6.ChildNodes[2].ChildNodes[2].ChildNodes[2].InnerXml = (new DateTime(2013, 09, 01)).ToString("yyyy-MM-dd") + "T00:00:00";
                                                        child6.ChildNodes[2].ChildNodes[3].ChildNodes[2].InnerXml = (new DateTime(2014, 05, 28)).ToString("yyyy-MM-dd") + "T00:00:00";
                                                    }

                                                    k++;
                                                }
                                            }
                                            if (deleteParentNode != null)
                                                item.RemoveChild(deleteParentNode);
                                            childtmp.AppendChild(item);
                                            
                                        }
                                    }
                                    

                                    
                                    
                                }
                                if (deleteNode!=null)
                                    childtmp.RemoveChild(deleteNode);
                            }
                            
                        }
                        if (deleteNode1!=null)
                        child3.RemoveChild(deleteNode1);
                        if (childtmp != null) child3.AppendChild(childtmp);
                    }
                }
            }
        }

    }

    public static void GetNSIPupilDoc(ref XmlDocument xmlDoc, ExtendedPupil pupil, bool isUpdate)
    {
        XmlNode deleteNode = null;
        XmlNode deleteNode1 = null;
        XmlNode deleteParentNode = null;
        XmlNode childtmp = null;
        CultureInfo culture = new CultureInfo("en-us");
        foreach (XmlNode child in xmlDoc.ChildNodes)
        {
            foreach (XmlNode child1 in child.ChildNodes)
            {
                foreach (XmlNode child2 in child1.ChildNodes)
                {
                    foreach (XmlNode child3 in child2.ChildNodes)
                    {
                        foreach (XmlNode child4 in child3.ChildNodes)
                        {

                            if (child4.Name == "Id" && child3.Name == "header")
                            {
                                child4.InnerXml = Guid.NewGuid().ToString();
                            }
                            if (child4.Name == "ns2:AppData")
                            {
                                deleteNode1 = child4;
                                childtmp = child4.CloneNode(true);
                                foreach (XmlNode node in childtmp)
                                {
                                    if (node.Name == "item")
                                    {
                                        deleteNode = node;
                                    }
                                }
                                foreach (XmlNode child5 in child4.ChildNodes)
                                {
                                    if (child5.Name == "item")
                                    {
                                        //deleteNode = child5;

                                        XmlNode item = child5.CloneNode(true);
                                        int k = 1;
                                        foreach (XmlNode child6 in item.ChildNodes)
                                        {
                                            if (isUpdate && child6.Name == "primaryKey")
                                            {
                                                child6.InnerXml = pupil.ReestrCode;
                                            }

                                            if (child6.Name == "attribute")
                                            {
                                                if (k == 1) child6.ChildNodes[2].InnerXml = pupil.LastName;
                                                if (k == 2) child6.ChildNodes[2].InnerXml = pupil.FirstName;
                                                if (k == 3) child6.ChildNodes[2].InnerXml = pupil.MiddleName;
                                                if (k == 4) child6.ChildNodes[2].InnerXml = string.IsNullOrEmpty(pupil.EMail) ? "" : pupil.EMail;
                                                if (k == 5) child6.ChildNodes[2].InnerXml = pupil.Sex == 1 ? "Женский" : "Мужской";
                                                // телефоны
                                                if (k == 6)
                                                {
                                                    // список 
                                                    child6.ChildNodes[2].ChildNodes[0].ChildNodes[2].InnerXml = string.IsNullOrEmpty(pupil.PhoneNumber) ? "" : pupil.PhoneNumber;
                                                    child6.ChildNodes[2].ChildNodes[1].ChildNodes[2].InnerXml = "Рабочий";
                                                }

                                                if (k == 7) child6.ChildNodes[2].InnerXml = pupil.Birthday.ToString("yyyy-MM-dd") + "T00:00:00";
                                                if (k == 8) child6.ChildNodes[2].InnerXml = string.IsNullOrEmpty(pupil.SchoolCode) ? "" : pupil.SchoolCode;
                                                if (k == 9) child6.ChildNodes[2].InnerXml = string.IsNullOrEmpty(pupil.ClassName) ? "" : pupil.ClassName;
                                                if (k == 10) child6.ChildNodes[2].InnerXml = pupil.Passport.Series;
                                                if (k == 11) child6.ChildNodes[2].InnerXml = pupil.Passport.Number;
                                                if (k == 12) child6.ChildNodes[2].InnerXml = pupil.Passport.IssueDate.Value.ToString("yyyy-MM-dd") + "T00:00:00";
                                                if (k == 13) child6.ChildNodes[2].InnerXml = pupil.Passport.PassportType.PassportTypeId == 3 ? "Свидетельство РФ" : pupil.Passport.PassportType.PassportTypeId == 2 ? "Паспорт РФ" : "Иной документ";

                                                // Guid в реестре 
                                                if (k == 14) child6.ChildNodes[2].InnerXml = "";
                                                // адрес
                                                if (k == 15)
                                                {
                                                    var address = pupil.Addresses.FirstOrDefault();
                                                    for (int z = 0; z < 11; z++)
                                                    {
                                                        if (z == 0) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.AddressStr;
                                                        if (z == 1) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = string.IsNullOrEmpty(address.PostIndex) ? "101000" : address.PostIndex;
                                                        if (z == 2) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.HouseNumber;
                                                        if (z == 3) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.Fraction;
                                                        if (z == 4) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.Building;
                                                        if (z == 5) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.Housing;
                                                        if (z == 6) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.FlatNumber;
                                                        if (z == 7) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = "Постоянная регистрация";
                                                        if (z == 8) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = string.IsNullOrEmpty(address.KladrCode) ? "" : address.KladrCode;
                                                        if (z == 9) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.BtiCode.HasValue ? address.BtiCode.Value.ToString() : "";
                                                        if (z == 10) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = address.BtiHouseCode.HasValue ? address.BtiHouseCode.Value.ToString() : "";
                                                    }

                                                }
                                                // представитель
                                                if (k == 16)
                                                {
                                                    var parent = pupil.Parents.FirstOrDefault();
                                                    if (parent == null)
                                                        deleteParentNode = child6;
                                                    for (int z = 0; z < 9; z++)
                                                    {
                                                        if (z == 0) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = parent.LastName;
                                                        if (z == 1) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = parent.FirstName;
                                                        if (z == 2) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = parent.MiddleName;
                                                        if (z == 3) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = string.IsNullOrEmpty(parent.EMail) ? "" : parent.EMail;
                                                        // телефоны
                                                        if (z == 4)
                                                        {
                                                            child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].ChildNodes[0].ChildNodes[2].InnerXml = string.IsNullOrEmpty(parent.PhoneNumber) ? "" : parent.PhoneNumber;
                                                            child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].ChildNodes[1].ChildNodes[2].InnerXml = "Рабочий";
                                                        }
                                                        if (z == 5) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = parent.Passport.Series;
                                                        if (z == 6) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = parent.Passport.Number;
                                                        if (z == 7) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = parent.Passport.IssueDate.Value.ToString("yyyy-MM-dd") + "T00:00:00";
                                                        if (z == 8) child6.ChildNodes[2].ChildNodes[z].ChildNodes[2].InnerXml = parent.Passport.PassportType.PassportTypeId == 3 ? "Свидетельство РФ" : parent.Passport.PassportType.PassportTypeId == 2 ? "Паспорт РФ" : "Иной документ"; ;
                                                    }
                                                }
                                                if (k == 17) child6.ChildNodes[2].InnerXml = "Новая запись";
                                                if (k == 18)
                                                {
                                                    child6.ChildNodes[2].ChildNodes[0].ChildNodes[2].InnerXml = "Зачисление в УДОД";
                                                    child6.ChildNodes[2].ChildNodes[1].ChildNodes[2].InnerXml = pupil.UserId.ToString();
                                                    child6.ChildNodes[2].ChildNodes[2].ChildNodes[2].InnerXml = (new DateTime(2013, 09, 01)).ToString("yyyy-MM-dd") + "T00:00:00";
                                                    child6.ChildNodes[2].ChildNodes[3].ChildNodes[2].InnerXml = (new DateTime(2014, 05, 28)).ToString("yyyy-MM-dd") + "T00:00:00";
                                                }

                                                k++;
                                            }
                                        }
                                        if (deleteParentNode != null)
                                            item.RemoveChild(deleteParentNode);
                                        childtmp.AppendChild(item);
                                    }



                                    
                                }
                                if (deleteNode != null)
                                    childtmp.RemoveChild(deleteNode);
                            }

                        }
                        if (deleteNode1 != null)
                            child3.RemoveChild(deleteNode1);
                        if (childtmp != null) child3.AppendChild(childtmp);
                    }
                }
            }
        }

    }

    public static void GetNSIDODoc(ref XmlDocument xmlDoc, ExtendedChildUnion childUnion, bool isUpdate)
    {
        XmlNode deleteNode = null;
        XmlNode deleteNode1 = null;
        XmlNode deleteParentNode = null;
        XmlNode childtmp = null;
        CultureInfo culture = new CultureInfo("en-us");
        foreach (XmlNode child in xmlDoc.ChildNodes)
        {
            foreach (XmlNode child1 in child.ChildNodes)
            {
                foreach (XmlNode child2 in child1.ChildNodes)
                {
                    foreach (XmlNode child3 in child2.ChildNodes)
                    {
                        foreach (XmlNode child4 in child3.ChildNodes)
                        {

                            if (child4.Name == "Id" && child3.Name == "header")
                            {
                                child4.InnerXml = Guid.NewGuid().ToString();
                            }
                            if (child4.Name == "ns2:AppData")
                            {
                                deleteNode1 = child4;
                                childtmp = child4.CloneNode(true);
                                foreach (XmlNode node in childtmp)
                                {
                                    if (node.Name == "item")
                                    {
                                        deleteNode = node;
                                    }
                                }
                                foreach (XmlNode child5 in child4.ChildNodes)
                                {
                                    if (child5.Name == "item")
                                    {
                                        //deleteNode = child5;

                                        XmlNode item = child5.CloneNode(true);
                                        int k = 1;
                                        foreach (XmlNode child6 in item.ChildNodes)
                                        {
                                            if (isUpdate && child6.Name == "primaryKey")
                                            {
                                                child6.InnerXml = childUnion.ReestrCode;
                                            }

                                            if (child6.Name == "attribute")
                                            {
                                                if (k == 1) child6.ChildNodes[2].InnerXml = childUnion.ChildUnionId.ToString();
                                                if (k == 2) child6.ChildNodes[2].InnerXml = childUnion.GuidOU;
                                                if (k == 3) child6.ChildNodes[2].InnerXml = "Учебный год";
                                                if (k == 4) child6.ChildNodes[2].InnerXml = childUnion.DateStart.ToString("yyyy-MM-dd") + "T00:00:00";
                                                if (k == 5) child6.ChildNodes[2].InnerXml = childUnion.DateEnd.ToString("yyyy-MM-dd") + "T00:00:00";
                                                if (k == 6) child6.ChildNodes[2].InnerXml = childUnion.Name;
                                                if (k == 7) child6.ChildNodes[2].InnerXml = "группа УДОД";
                                                if (k == 8) child6.ChildNodes[2].InnerXml = "группа УДОД";
                                                if (k == 9) child6.ChildNodes[2].InnerXml = "Очная (дневная)";
                                                if (k == 10) child6.ChildNodes[2].InnerXml = childUnion.Profile.Name;
                                                if (k == 11) child6.ChildNodes[2].InnerXml = childUnion.Program.Name;
                                                if (k == 12) child6.ChildNodes[2].InnerXml = childUnion.Section.Name;
                                                if (k == 13) child6.ChildNodes[2].InnerXml = childUnion.AgeStart.ToString();
                                                if (k == 14) child6.ChildNodes[2].InnerXml = childUnion.AgeEnd.ToString();
                                                if (k == 15) child6.ChildNodes[2].InnerXml = childUnion.ProgramService;
                                                if (k == 16) child6.ChildNodes[2].InnerXml = childUnion.BudgetTypes.Count(i => i.DictTypeBudgetId == 2) > 0 ? "Бюджет" : "Договор";
                                                if (k == 17) child6.ChildNodes[2].InnerXml = "7-дневная неделя";
                                                if (k == 18) child6.ChildNodes[2].InnerXml = childUnion.Teachers.Count>0?childUnion.Teachers.FirstOrDefault().Fio:"";
                                                if (k == 19) child6.ChildNodes[2].InnerXml = "true";
                                                if (k == 20) child6.ChildNodes[2].InnerXml = childUnion.BudgetTypes.Count(i => i.DictTypeBudgetId == 2) > 0 ? "true" : "false";
                                                if (k == 21) child6.ChildNodes[2].InnerXml =  "Новая запись";
                                                if (k == 22)
                                                {
                                                    child6.ChildNodes[2].ChildNodes[0].ChildNodes[2].InnerXml = "Зачисление в УДОД";
                                                    child6.ChildNodes[2].ChildNodes[1].ChildNodes[2].InnerXml = childUnion.ChildUnionId.ToString();
                                                    child6.ChildNodes[2].ChildNodes[2].ChildNodes[2].InnerXml = (new DateTime(2013, 09, 01)).ToString("yyyy-MM-dd") + "T00:00:00";
                                                    child6.ChildNodes[2].ChildNodes[3].ChildNodes[2].InnerXml = (new DateTime(2014, 05, 28)).ToString("yyyy-MM-dd") + "T00:00:00";
                                                }

                                                k++;
                                            }
                                        }
                                        if (deleteParentNode != null)
                                            item.RemoveChild(deleteParentNode);
                                        childtmp.AppendChild(item);
                                    }



                                    
                                }
                                if (deleteNode != null)
                                    childtmp.RemoveChild(deleteNode);
                            }

                        }
                        if (deleteNode1 != null)
                            child3.RemoveChild(deleteNode1);
                        if (childtmp != null) child3.AppendChild(childtmp);
                    }
                }
            }
        }

    }

    public static void GetNSIZachDoc(ref XmlDocument xmlDoc, ExtendedPupil pupil, ExtendedChildUnion childUnion, bool isUpdate)
    {
        XmlNode deleteNode = null;
        XmlNode deleteNode1 = null;
        XmlNode deleteParentNode = null;
        XmlNode childtmp = null;
        CultureInfo culture = new CultureInfo("en-us");
        foreach (XmlNode child in xmlDoc.ChildNodes)
        {
            foreach (XmlNode child1 in child.ChildNodes)
            {
                foreach (XmlNode child2 in child1.ChildNodes)
                {
                    foreach (XmlNode child3 in child2.ChildNodes)
                    {
                        foreach (XmlNode child4 in child3.ChildNodes)
                        {

                            if (child4.Name == "Id" && child3.Name == "header")
                            {
                                child4.InnerXml = Guid.NewGuid().ToString();
                            }
                            if (child4.Name == "ns2:AppData")
                            {
                                deleteNode1 = child4;
                                childtmp = child4.CloneNode(true);
                                foreach (XmlNode node in childtmp)
                                {
                                    if (node.Name == "item")
                                    {
                                        deleteNode = node;
                                    }
                                }
                                foreach (XmlNode child5 in child4.ChildNodes)
                                {
                                    if (child5.Name == "item")
                                    {
                                        //deleteNode = child5;

                                        XmlNode item = child5.CloneNode(true);
                                        int k = 1;
                                        foreach (XmlNode child6 in item.ChildNodes)
                                        {
                                            if (isUpdate && child6.Name == "primaryKey")
                                            {
                                                child6.InnerXml = childUnion.ReestrCode;
                                            }

                                            if (!isUpdate && child6.Name == "attribute")
                                            {
                                                if (k == 1) child6.ChildNodes[2].InnerXml = pupil.UserId.ToString();
                                                if (k == 2) child6.ChildNodes[2].InnerXml = childUnion.ChildUnionId.ToString();
                                                if (k == 3) child6.ChildNodes[2].InnerXml = "группа УДОД";
                                                if (k == 4) child6.ChildNodes[2].InnerXml = childUnion.DateStart.ToString("yyyy-MM-dd") + "T00:00:00";
                                                if (k == 5) child6.ChildNodes[2].InnerXml = childUnion.DateEnd.ToString("yyyy-MM-dd") + "T00:00:00";
                                                if (k == 6) child6.ChildNodes[2].InnerXml = "Очная (дневная)";
                                                if (k == 7) child6.ChildNodes[2].InnerXml = childUnion.PupilNumYear.ToString();
                                                if (k == 8) child6.ChildNodes[2].InnerXml = "Новая запись";
                                                if (k == 9)
                                                {
                                                    child6.ChildNodes[2].ChildNodes[0].ChildNodes[2].InnerXml = "Зачисление в УДОД";
                                                    child6.ChildNodes[2].ChildNodes[1].ChildNodes[2].InnerXml = childUnion.PupilAgeGroupId.ToString();
                                                    child6.ChildNodes[2].ChildNodes[2].ChildNodes[2].InnerXml = (new DateTime(2013, 09, 01)).ToString("yyyy-MM-dd") + "T00:00:00";
                                                    child6.ChildNodes[2].ChildNodes[3].ChildNodes[2].InnerXml = (new DateTime(2014, 05, 28)).ToString("yyyy-MM-dd") + "T00:00:00";
                                                }
                                                k++;
                                            }
                                        }
                                        if (deleteParentNode != null)
                                            item.RemoveChild(deleteParentNode);
                                        childtmp.AppendChild(item);
                                    }



                                    
                                }
                                if (deleteNode != null)
                                    childtmp.RemoveChild(deleteNode);
                            }

                        }
                        if (deleteNode1 != null)
                            child3.RemoveChild(deleteNode1);
                        if (childtmp != null) child3.AppendChild(childtmp);
                    }
                }
            }
        }

    }

    public static void GetESZDoc(ref XmlDocument xmlDoc, ExtendedChildUnion ecu)
    {
        foreach (XmlNode child in xmlDoc.ChildNodes)
        {
            foreach (XmlNode child1 in child.ChildNodes)
            {
                foreach (XmlNode child2 in child1.ChildNodes)
                {
                    foreach (XmlNode child3 in child2.ChildNodes)
                    {
                        foreach (XmlNode child4 in child3.ChildNodes)
                        {

                            if (child4.Name == "Id" && child3.Name=="header")
                            {
                                child4.InnerXml = Guid.NewGuid().ToString();
                            }
                            if (child4.Name == "service")
                            {
                                foreach (XmlNode child5 in child4.ChildNodes)
                                {
                                    
                                    
                                }

                            }
                        }
                    }
                }
            }
        }
    }

    public static void GetDoc(ref XmlDocument xmlDoc, int? codeEkis, int? codeReestr)
    {
        // заменить MessageId
        foreach (XmlNode child in xmlDoc.ChildNodes)
        {
            foreach (XmlNode child1 in child.ChildNodes)
            {
                foreach (XmlNode child2 in child1.ChildNodes)
                {
                    foreach (XmlNode child3 in child2.ChildNodes)
                    {
                        if (child3.Name == "smev:MessageId")
                        {
                            child3.InnerXml = Guid.NewGuid().ToString();
                        }
                        if (child3.Name == "smev:Message" || child3.Name == "smev:MessageData")
                        {
                            foreach (XmlNode child4 in child3.ChildNodes)
                            {
                                if (child4.Name == "smev:Date")
                                {
                                    //child4.InnerXml = DateTime.Now.ToString();
                                }
                                foreach (XmlNode child5 in child4.ChildNodes)
                                {
                                    foreach (XmlNode child6 in child5.ChildNodes)
                                    {
                                        if (codeReestr.HasValue && child6.Name == "nsi:attibuteName")
                                        {
                                            child6.InnerXml = "РОУ XML/Первичный ключ";
                                        }
                                        if (codeEkis.HasValue && child6.Name == "nsi:attibuteValue")
                                        {
                                            child6.InnerXml = codeEkis.Value.ToString();
                                        }
                                        if (codeReestr.HasValue && child6.Name == "nsi:attibuteValue")
                                        {
                                            child6.InnerXml = codeReestr.Value.ToString();
                                        }
                                    }
                                    
                                }
                            }

                        }
                    }

                }
            }
        }
    }

    public static void GetDoc(ref XmlDocument xmlDoc, string guidCode)
    {
        // заменить MessageId
        foreach (XmlNode child in xmlDoc.ChildNodes)
        {
            foreach (XmlNode child1 in child.ChildNodes)
            {
                foreach (XmlNode child2 in child1.ChildNodes)
                {
                    foreach (XmlNode child3 in child2.ChildNodes)
                    {
                        if (child3.Name == "smev:MessageId")
                        {
                            child3.InnerXml = Guid.NewGuid().ToString();
                        }
                        if (child3.Name == "smev:Message" || child3.Name == "smev:MessageData")
                        {
                            foreach (XmlNode child4 in child3.ChildNodes)
                            {
                                if (child4.Name == "smev:Date")
                                {
                                    //child4.InnerXml = DateTime.Now.ToString();
                                }
                                foreach (XmlNode child5 in child4.ChildNodes)
                                {
                                    foreach (XmlNode child6 in child5.ChildNodes)
                                    {
                                        
                                        if (child6.Name == "nsi:attibuteValue")
                                        {
                                            child6.InnerXml = guidCode;
                                        }
                                    }

                                }
                            }

                        }
                    }

                }
            }
        }
    }

    public static void GetDoc(ref XmlDocument xmlDoc, string series, string number)
    {
        // заменить MessageId
        int i = 0;
        foreach (XmlNode child in xmlDoc.ChildNodes)
        {
            foreach (XmlNode child1 in child.ChildNodes)
            {
                foreach (XmlNode child2 in child1.ChildNodes)
                {
                    foreach (XmlNode child3 in child2.ChildNodes)
                    {
                        if (child3.Name == "smev:MessageId")
                        {
                            child3.InnerXml = Guid.NewGuid().ToString();
                        }
                        if (child3.Name == "smev:Message" || child3.Name == "smev:MessageData")
                        {
                            foreach (XmlNode child4 in child3.ChildNodes)
                            {
                                if (child4.Name == "smev:Date")
                                {
                                    //child4.InnerXml = DateTime.Now.ToString();
                                }
                                foreach (XmlNode child5 in child4.ChildNodes)
                                {
                                    if (child5.Name == "nsi:searchPredicate")
                                    {
                                        foreach (XmlNode child6 in child5.ChildNodes)
                                        {
                                            if (child6.Name == "nsi:attibuteValue")
                                            {
                                                if (i == 0) child6.InnerXml = series;
                                                if (i == 1) child6.InnerXml = number;
                                                i++;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }

                }
            }
        }
    }

    public static void GetDoc(ref XmlDocument xmlDoc, string udodId, bool isReestr)
    {
        int i = 0;
        foreach (XmlNode child in xmlDoc.ChildNodes)
        {
            foreach (XmlNode child1 in child.ChildNodes)
            {
                foreach (XmlNode child2 in child1.ChildNodes)
                {
                    foreach (XmlNode child3 in child2.ChildNodes)
                    {
                        if (child3.Name == "smev:MessageId")
                        {
                            child3.InnerXml = Guid.NewGuid().ToString();
                        }
                        if (child3.Name == "smev:Message" || child3.Name == "smev:MessageData")
                        {
                            foreach (XmlNode child4 in child3.ChildNodes)
                            {
                                if (child4.Name == "smev:Date")
                                {
                                    //child4.InnerXml = DateTime.Now.ToString();
                                }
                                foreach (XmlNode child5 in child4.ChildNodes)
                                {
                                    if (child5.Name == "nsi:query")
                                    {
                                        if (isReestr)
                                        {
                                            child5.InnerXml = child5.InnerXml.Replace("РОУ XML/Первичный ключ источника']=519", "РОУ XML/Первичный ключ']="+udodId);
                                        }
                                        else
                                        {
                                            child5.InnerXml = child5.InnerXml.Replace("РОУ XML/Первичный ключ источника']=519", "РОУ XML/Первичный ключ источника']=" + udodId);
                                        }
                                        
                                        
                                    }
                                }
                            }

                        }
                    }

                }
            }
        }
    }

    public static void GetDocSecond(ref XmlDocument xmlDoc, string udodId, bool isReestr)
    {
        // заменить MessageId
        int i = 0;
        foreach (XmlNode child in xmlDoc.ChildNodes)
        {
            foreach (XmlNode child1 in child.ChildNodes)
            {
                foreach (XmlNode child2 in child1.ChildNodes)
                {
                    foreach (XmlNode child3 in child2.ChildNodes)
                    {
                        if (child3.Name == "smev:MessageId")
                        {
                            child3.InnerXml = Guid.NewGuid().ToString();
                        }
                        if (child3.Name == "smev:Message" || child3.Name == "smev:MessageData")
                        {
                            foreach (XmlNode child4 in child3.ChildNodes)
                            {
                                if (child4.Name == "smev:Date")
                                {
                                    //child4.InnerXml = DateTime.Now.ToString();
                                }
                                foreach (XmlNode child5 in child4.ChildNodes)
                                {
                                    if (child5.Name == "nsi:searchPredicate")
                                    {
                                        foreach (XmlNode child6 in child5.ChildNodes)
                                        {
                                            if (isReestr && child6.Name == "nsi:attibuteName")
                                            {
                                                child6.InnerXml = "Реестр обучаемых линейный/ID Образовательного учреждения";
                                            }
                                            if (isReestr && child6.Name == "nsi:attibuteType")
                                            {
                                                child6.InnerXml = "INTEGER";
                                            }
                                            if (child6.Name == "nsi:attibuteValue")
                                            {
                                                if (isReestr)
                                                child6.InnerXml = udodId;
                                                else
                                                {
                                                    while (udodId.Length<6)
                                                    {
                                                        udodId = "0" + udodId;
                                                    }
                                                    child6.InnerXml = udodId;
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }

                }
            }
        }
    }

    public static void GetDocThird(ref XmlDocument xmlDoc, string reestrGuid, int from, int limit)
    {
        // заменить MessageId
        int i = 0;
        foreach (XmlNode child in xmlDoc.ChildNodes)
        {
            foreach (XmlNode child1 in child.ChildNodes)
            {
                foreach (XmlNode child2 in child1.ChildNodes)
                {
                    foreach (XmlNode child3 in child2.ChildNodes)
                    {
                        if (child3.Name == "smev:MessageId")
                        {
                            child3.InnerXml = Guid.NewGuid().ToString();
                        }
                        if (child3.Name == "smev:Message" || child3.Name == "smev:MessageData")
                        {
                            foreach (XmlNode child4 in child3.ChildNodes)
                            {
                                if (child4.Name == "smev:Date")
                                {
                                    //child4.InnerXml = DateTime.Now.ToString();
                                }
                                foreach (XmlNode child5 in child4.ChildNodes)
                                {
                                    if (child5.Name == "nsi:searchPredicate")
                                    {
                                        foreach (XmlNode child6 in child5.ChildNodes)
                                        {
                                            if (child6.Name == "nsi:attibuteName")
                                                child6.InnerXml = "Реестр обучаемых линейный/GUID образовательного учреждения";
                                            if (child6.Name == "nsi:attibuteValue")
                                            {
                                                child6.InnerXml = reestrGuid;
                                            }
                                        }
                                    }
                                    if (child5.Name == "nsi:from")
                                    {
                                        child5.InnerXml = from.ToString();
                                    }

                                }
                            }

                        }
                    }

                }
            }
        }
    }

    public static ExtendedUdod GetUdodData(string _pathFiles, int udodEkisId, int? reestrId )
    {
        ExtendedUdod item = new ExtendedUdod();

        NSIServiceClient client1 = new NSIServiceClient();
        client1.Endpoint.Behaviors.Add(new InterceptorCustomBehavior1());
        client1.Open();
        NSIRequestType type = new NSIRequestType();
        type.MessageData = new Service.NSI.ExtMessageDataType();
        type.MessageData.AppData = new ExtAppDataType();

        type.MessageData.AppData.context = new Context();
        type.MessageData.AppData.context.User = "UDOD_SOAP";
        type.MessageData.AppData.context.Password = "rlap0a3h";
        type.MessageData.AppData.context.Company = "dogm_nsi";

        ItemsChoiceType1[] itemsChoiceType1 = new ItemsChoiceType1[3];
        itemsChoiceType1[0] = ItemsChoiceType1.catalogName;
        itemsChoiceType1[1] = ItemsChoiceType1.matchAllCategories;
        itemsChoiceType1[2] = ItemsChoiceType1.searchPredicate;

        object[] items = new object[3];
        items[0] = "Реестр образовательных учреждений";
        items[1] = true; //"true";

        //for (int k = 0; k < 1; k++)

        SearchPredicate[] atributes = new SearchPredicate[2];
        int index = 0;
        #region один элемент Item
        atributes[index] = new SearchPredicate();
        atributes[index].attributeName = reestrId.HasValue?"Первичный ключ":"Первичный ключ источника";
        atributes[index].attributeType = "INTEGER";
        atributes[index].attributeValue = reestrId.HasValue ? reestrId.Value.ToString() : udodEkisId.ToString();
        atributes[index].attributeOp = "=";

        #endregion
        index++;

        items[2] = atributes[0];

        type.MessageData.AppData.Items = items;
        type.MessageData.AppData.ItemsElementName = itemsChoiceType1;

        try
        {
            {
                var tmpvalue = client1.searchItemsInCatalog(type);
                foreach (var tmp1 in tmpvalue.MessageData.AppData.Items)
                {
                    var udod = (tmp1 as GeneralResponse);

                    foreach (var itemreestr in udod.item)
                    {
                        item.ReestrId = Convert.ToInt32(itemreestr.attribute[0].value[0].Value);
                        item.EkisId = Convert.ToInt32(itemreestr.attribute[1].value[0].Value);
                        item.ShortName = itemreestr.attribute[3].value[0].Value;
                        item.Name = itemreestr.attribute[4].value[0].Value;
                        item.CityName = itemreestr.attribute[10].value[0].Value;
                        item.RayonName = itemreestr.attribute[11].value[0].Value;
                        item.EMail = itemreestr.attribute[14].value[0].Value;
                        item.SiteUrl = itemreestr.attribute[15].value[0].Value;
                        item.FioDirector = itemreestr.attribute[16].value[0].Value;
                        item.JurAddress = itemreestr.attribute[27].value[0].Value;
                        item.ReestrGuid = itemreestr.attribute[33].value[0].Value;
                        item.PhoneNumber = itemreestr.attribute[34].value[0].Value;
                        item.ParentName = itemreestr.attribute[8].value[0].Value;

                        foreach (Attribute attribute in itemreestr.attribute)
                        {
                            if (attribute.name.Equals("/РОУ XML/Нормализация#0/Тип ОУ"))
                            {
                                if (attribute.value[0].Value.Equals("общеобразовательное учреждение"))
                                {
                                    item.UdodTypeId = 1;
                                }
                                if (attribute.value[0].Value.Equals("дошкольное образовательное учреждение"))
                                {
                                    item.UdodTypeId = 2;
                                }
                            }
                        }
                    }
                }
                
            }
        }
        catch (Exception e1)
        {
            string str = e1.Message;
        }



        client1.Close();
       
        return item;
    }
    public static ExtendedUdod GetUdodData(string _pathFiles, string guidCode)
    {
        ExtendedUdod item = new ExtendedUdod();

        NSIServiceClient client1 = new NSIServiceClient();
        client1.Endpoint.Behaviors.Add(new InterceptorCustomBehavior1());
        client1.Open();
        NSIRequestType type = new NSIRequestType();
        type.MessageData = new Service.NSI.ExtMessageDataType();
        type.MessageData.AppData = new ExtAppDataType();

        type.MessageData.AppData.context = new Context();
        type.MessageData.AppData.context.User = "UDOD_SOAP";
        type.MessageData.AppData.context.Password = "rlap0a3h";
        type.MessageData.AppData.context.Company = "dogm_nsi";

        ItemsChoiceType1[] itemsChoiceType1 = new ItemsChoiceType1[3];
        itemsChoiceType1[0] = ItemsChoiceType1.catalogName;
        itemsChoiceType1[1] = ItemsChoiceType1.matchAllCategories;
        itemsChoiceType1[2] = ItemsChoiceType1.searchPredicate;
        
        object[] items = new object[3];
        items[0] = "Реестр образовательных учреждений";
        items[1] = true; //"true";

        //for (int k = 0; k < 1; k++)
        
        SearchPredicate[] atributes = new SearchPredicate[2];
        int index = 0;
        #region один элемент Item
        atributes[index] = new SearchPredicate();
        atributes[index].attributeName = "GUID Образовательного учреждения";
        atributes[index].attributeType = "STRING";
        atributes[index].attributeValue = guidCode;
        atributes[index].attributeOp = "=";

        #endregion
        index++;

        items[2] = atributes[0];
            
        type.MessageData.AppData.Items = items;
        type.MessageData.AppData.ItemsElementName = itemsChoiceType1;

        try
        {
            {
                var tmpvalue = client1.searchItemsInCatalog(type);
                foreach (var tmp1 in tmpvalue.MessageData.AppData.Items)
                {
                    var udod = (tmp1 as GeneralResponse);

                    foreach (var itemreestr in udod.item)
                    {
                        item.ReestrId = Convert.ToInt32(itemreestr.attribute[0].value[0].Value);
                        item.EkisId = Convert.ToInt32(itemreestr.attribute[1].value[0].Value);
                        item.ShortName = itemreestr.attribute[3].value[0].Value;
                        item.Name = itemreestr.attribute[4].value[0].Value;
                        item.CityName = itemreestr.attribute[10].value[0].Value;
                        item.RayonName = itemreestr.attribute[11].value[0].Value;
                        item.EMail = itemreestr.attribute[14].value[0].Value;
                        item.SiteUrl = itemreestr.attribute[15].value[0].Value;
                        item.FioDirector = itemreestr.attribute[16].value[0].Value;
                        item.JurAddress = itemreestr.attribute[27].value[0].Value;
                        item.ReestrGuid = itemreestr.attribute[33].value[0].Value;
                        item.PhoneNumber = itemreestr.attribute[34].value[0].Value;
                    }
                }
                
            }
        }
        catch (Exception e1)
        {
            string str = e1.Message;
        }



        client1.Close();
        
        return item;
    }

    public static List<ExtendedPupil> GetSchoolPupils(string pathFiles, long udodId)
    {
        XmlDocument docXML = new XmlDocument();
        
        List<ExtendedPupil> list = new List<ExtendedPupil>();
        String _UrlWebService = ConfigurationManager.AppSettings["DogmService"]; 	// Change the web 
        ClassSoapClient sc = new ClassSoapClient("", new Uri(_UrlWebService));
        sc.Timeout = 3 * 60000;
        sc.Destination.Address = new Microsoft.Web.Services3.Addressing.Address(new Uri(_UrlWebService));

        docXML.Load(pathFiles);
        
        using (UdodDb db = new UdodDb())
        {
            var udod = db.GetUdod(udodId);
            if (udod.ReestrId.HasValue && udod.ReestrId!=0)
            {
                udodId = udod.ReestrId.Value;
            }
            else
            {
                udodId = udod.EkisId;
            }
            GetDoc(ref docXML, udodId.ToString(), udod.ReestrId.HasValue);
        }

        //docXML.Save("c:\\txt.xml");

        SoapEnvelope soapResponse = new SoapEnvelope();
        soapResponse = sc.SendEnvelope(docXML);
        XmlDocument responceDoc = new XmlDocument();
        responceDoc.InnerXml = soapResponse.InnerXml;
        //responceDoc.Save("c:\\txt1.xml");
        
        //responceDoc.Load(pathFiles.Replace("GetContingentByReestrId.xml", "resultxml.xml"));
        foreach (XmlNode child in responceDoc.ChildNodes)
        {
            foreach (XmlNode child1 in child.ChildNodes)
            {
                foreach (XmlNode child2 in child1.ChildNodes)
                {
                    foreach (XmlNode child3 in child2.ChildNodes)
                    {
                        if (child3.Name == "smev:MessageData")
                        {
                            foreach (XmlNode child4 in child3.ChildNodes)
                            {
                                foreach (XmlNode child5 in child4.ChildNodes)
                                {
                                    if (child5.Name == "nsi:generalResponse")
                                        foreach (XmlNode child6 in child5.ChildNodes)
                                        {
                                            if (child6.Name == "nsi:queryResult")
                                            {
                                                ExtendedPupil item = new ExtendedPupil();
                                                ExtendedPassport passport = new ExtendedPassport();
                                                ExtendedAddress address = new ExtendedAddress();

                                                item.LastName = child6.ChildNodes[0].InnerXml;
                                                item.FirstName = child6.ChildNodes[1].InnerXml;
                                                item.MiddleName = child6.ChildNodes[2].InnerXml;
                                                passport.PassportType=new ExtendedPassportType()
                                                                          {
                                                                              PassportTypeId = string.IsNullOrEmpty(child6.ChildNodes[3].InnerXml)?2:3
                                                                          };
                                                passport.Series = string.IsNullOrEmpty(child6.ChildNodes[3].InnerXml)
                                                                      ? child6.ChildNodes[6].InnerXml
                                                                      : child6.ChildNodes[3].InnerXml;
                                                passport.Number = string.IsNullOrEmpty(child6.ChildNodes[4].InnerXml)
                                                                      ? child6.ChildNodes[7].InnerXml
                                                                      : child6.ChildNodes[4].InnerXml;
                                                try
                                                {
                                                    passport.IssueDate = string.IsNullOrEmpty(child6.ChildNodes[5].InnerXml)
                                                                      ? Convert.ToDateTime(child6.ChildNodes[8].InnerXml)
                                                                      : Convert.ToDateTime(child6.ChildNodes[5].InnerXml);
                                                }
                                                catch (Exception)
                                                {
                                                    passport.IssueDate = DateTime.Now;
                                                }
                                                try
                                                {
                                                    item.Birthday = Convert.ToDateTime(child6.ChildNodes[9].InnerXml);
                                                }
                                                catch (Exception)
                                                {
                                                    item.Birthday = DateTime.Now;
                                                }
                                                item.Sex = child6.ChildNodes[10].InnerXml=="Мужской"?0:1;
                                                item.PhoneNumber = child6.ChildNodes[11].InnerXml;
                                                item.EMail = child6.ChildNodes[12].InnerXml;

                                                item.AddressStr = child6.ChildNodes[13].InnerXml;
                                                address.HouseNumber = child6.ChildNodes[16].InnerXml;
                                                address.Fraction = child6.ChildNodes[17].InnerXml;
                                                address.Building = child6.ChildNodes[18].InnerXml;
                                                address.Housing = child6.ChildNodes[19].InnerXml;
                                                address.FlatNumber = child6.ChildNodes[20].InnerXml;

                                                try
                                                {
                                                    address.BtiCode = Convert.ToInt32(child6.ChildNodes[22].InnerXml);
                                                }
                                                catch (Exception)
                                                {
                                                    address.BtiCode = null;
                                                }
                                                if (child6.ChildNodes[23].InnerXml != "" && !address.BtiCode.HasValue)
                                                {
                                                    address.KladrCode = child6.ChildNodes[23].InnerXml;
                                                }

                                                item.ClassName = child6.ChildNodes[21].InnerXml;
                                                item.ReestrCode = child6.ChildNodes[34].InnerXml;

                                                item.Addresses=new List<ExtendedAddress>();
                                                item.Addresses.Add(address);
                                                item.Passport = passport;

                                                list.Add(item);
                                            }
                                            

                                        }
                                }
                            }

                        }
                    }

                }
            }
        }
        
        return list;
    }

    public static List<ExtendedPupil> GetSchoolPupilsSecond(string _pathFiles, long udodId, string reestrGuid)
    {
        List<ExtendedPupil> list = new List<ExtendedPupil>();

        NSIServiceClient client1 = new NSIServiceClient();
        client1.Endpoint.Behaviors.Add(new InterceptorCustomBehavior1());
        client1.Open();
        NSIRequestType type = new NSIRequestType();
        type.MessageData = new Service.NSI.ExtMessageDataType();
        type.MessageData.AppData = new ExtAppDataType();

        type.MessageData.AppData.context = new Context();
        type.MessageData.AppData.context.User = "UDOD_SOAP";
        type.MessageData.AppData.context.Password = "rlap0a3h";
        type.MessageData.AppData.context.Company = "dogm_nsi";

        ItemsChoiceType1[] itemsChoiceType1 = new ItemsChoiceType1[5];
        itemsChoiceType1[0] = ItemsChoiceType1.catalogName;
        itemsChoiceType1[1] = ItemsChoiceType1.matchAllCategories;
        itemsChoiceType1[2] = ItemsChoiceType1.searchPredicate;
        itemsChoiceType1[4] = ItemsChoiceType1.limit;
        itemsChoiceType1[3] = ItemsChoiceType1.from;
        //itemsChoiceType1[3] = ItemsChoiceType1.searchPredicate;

        //Item[] items = new Item[4];
        
        object[] items = new object[5];
        items[0] = "Реестр обучаемых";
        items[1] = true; //"true";

        //for (int k = 0; k < 1; k++)
        //{
            SearchPredicate[] atributes = new SearchPredicate[1];
            int index = 0;
            #region один элемент Item
            atributes[index] = new SearchPredicate();
            atributes[index].attributeName = "Реестр обучаемых линейный/GUID образовательного учреждения";
            atributes[index].attributeType = "STRING";
            atributes[index].attributeValue = reestrGuid;
            atributes[index].attributeOp = "=";

            #endregion
            index++;
            long z = 1;
            items[2] = atributes[0];
            items[3] = z;
            items[4] = 1000;
        //}

        type.MessageData.AppData.Items = items;
        type.MessageData.AppData.ItemsElementName = itemsChoiceType1;
        
        FileInfo fi = new FileInfo("c:\\udod\\reestr\\1.txt");
        var s = fi.AppendText();
        try
        {
            long countfound = 100000000;
            
            while (z<countfound)
            {
                z = z + 1000;
                var tmpvalue = client1.searchItemsInCatalog(type);
                type.MessageData.AppData.Items[3] = z;
                foreach (var tmp1 in tmpvalue.MessageData.AppData.Items)
                {
                    var pupil = (tmp1 as GeneralResponse);
                    countfound = pupil.found;
                    
                    foreach (var itemreestr in pupil.item)
                    {
                        int z1 = 0;
                        try
                        {

                       
                        ExtendedPupil item = new ExtendedPupil();
                        ExtendedPassport passport = new ExtendedPassport();
                        ExtendedAddress address = new ExtendedAddress();

                        item.ReestrCode = itemreestr.attribute[0].value[0].Value;
                        try
                        {
                            z1 = 6;
                            item.ReestrGuid = new Guid(itemreestr.attribute[6].value[0].Value);
                        }
                        catch (Exception)
                        {
                            item.ReestrGuid = null;
                        }
                            z1 = 1;
                        item.LastName = itemreestr.attribute[1].value[0].Value;
                        z1 = 2;
                        item.FirstName = itemreestr.attribute[2].value[0].Value;
                        z1 = 3;
                        item.MiddleName = string.IsNullOrEmpty(itemreestr.attribute[3].value[0].Value) ? "" : itemreestr.attribute[3].value[0].Value;
                        try
                        {
                            z1 = 5;
                            item.Birthday = Convert.ToDateTime(itemreestr.attribute[5].value[0].Value);
                        }
                        catch (Exception)
                        {
                            item.Birthday = DateTime.Now;
                        }

                        z1 = 4;
                            item.Sex = itemreestr.attribute[4].value[0].Value == "Мужской" ? 0 : 1;
                            z1 = 9;
                            item.PhoneNumber = string.IsNullOrEmpty(itemreestr.attribute[9].value[0].Value) ? "" : itemreestr.attribute[9].value[0].Value;
                            z1 = 84;
                        if (itemreestr.attribute[84].value[0].Value.ToLower() == "свидетельство о рождении")
                        {
                            passport.PassportType = new ExtendedPassportType() { PassportTypeId = 3 };
                        }
                        else if (itemreestr.attribute[84].value[0].Value.ToLower() == "паспорт гражданина российской федерации")
                        {
                            passport.PassportType = new ExtendedPassportType() { PassportTypeId = 2 };
                        }
                        else
                        {
                            passport.PassportType = new ExtendedPassportType() { PassportTypeId = 4 };
                        }

                        if (passport.PassportType.PassportTypeId!=2)
                        {
                            z1 = 10;
                            passport.Series = itemreestr.attribute[10].value[0].Value;
                            z1 = 11;
                            passport.Number = itemreestr.attribute[11].value[0].Value;
                        }
                        else
                        {
                            z1 = 14;
                            passport.Series = itemreestr.attribute[14].value[0].Value;
                            z1 = 15;
                            passport.Number = itemreestr.attribute[15].value[0].Value;
                        }


                        try
                        {
                            z1 = 12;
                            passport.IssueDate = Convert.ToDateTime(itemreestr.attribute[12].value[0].Value);
                        }
                        catch (Exception)
                        {
                            passport.IssueDate = DateTime.Now;
                        }

                        if (passport.PassportType.PassportTypeId == 2)
                        {
                            try
                            {
                                z1 = 16;
                                passport.IssueDate = Convert.ToDateTime(itemreestr.attribute[16].value[0].Value);
                                //passport.PassportType = new ExtendedPassportType() { PassportTypeId = 2 };
                            }
                            catch (Exception)
                            {
                                passport.IssueDate = DateTime.Now;
                            }
                        }

                        z1 = 34;
                        item.AddressStr = string.IsNullOrEmpty(itemreestr.attribute[34].value[0].Value) ? "" : itemreestr.attribute[34].value[0].Value;
                        z1 = 28;
                        address.HouseNumber = string.IsNullOrEmpty(itemreestr.attribute[28].value[0].Value) ? "" : itemreestr.attribute[28].value[0].Value;
                        z1 = 29; 
                            address.Fraction = string.IsNullOrEmpty(itemreestr.attribute[29].value[0].Value) ? "" : itemreestr.attribute[29].value[0].Value;
                            z1 = 31;
                        address.Building = string.IsNullOrEmpty(itemreestr.attribute[31].value[0].Value) ? "" : itemreestr.attribute[31].value[0].Value;
                        z1 = 30;
                        address.Housing = string.IsNullOrEmpty(itemreestr.attribute[30].value[0].Value) ? "" : itemreestr.attribute[30].value[0].Value;
                        z1 = 32;
                        address.FlatNumber = string.IsNullOrEmpty(itemreestr.attribute[32].value[0].Value) ? "" : itemreestr.attribute[32].value[0].Value;
                        try
                        {
                            z1 = 36;
                            address.BtiCode = Convert.ToInt32(itemreestr.attribute[36].value[0].Value);
                        }
                        catch (Exception)
                        {
                            address.BtiCode = null;
                        }

                        z1 = 37;
                        if (itemreestr.attribute[37].value[0].Value != "" && !address.BtiCode.HasValue && itemreestr.attribute[37].value[0].Value.Length > 8)
                        {
                            address.KladrCode = itemreestr.attribute[37].value[0].Value;

                        }
                        z1 = 42;
                        item.SchoolName = string.IsNullOrEmpty(itemreestr.attribute[42].value[0].Value) ? "" : itemreestr.attribute[42].value[0].Value;
                        z1 = 44;
                            item.ClassName = string.IsNullOrEmpty(itemreestr.attribute[44].value[0].Value) ? "" : itemreestr.attribute[44].value[0].Value;
                            z1 = 81;
                        item.EMail = string.IsNullOrEmpty(itemreestr.attribute[81].value[0].Value) ? "" : itemreestr.attribute[81].value[0].Value;
                        z1 = 91;
                        item.SchoolCode = string.IsNullOrEmpty(itemreestr.attribute[91].value[0].Value) ? "" : itemreestr.attribute[91].value[0].Value;
                        z1 = 79;
                        item.IsReestrDelete = itemreestr.attribute[79].value[0].Value.ToUpper() == "УДАЛЕННЫЙ" || itemreestr.attribute[79].value[0].Value.ToUpper() == "УДАЛЕН ИЗ ДОУ" || itemreestr.attribute[79].value[0].Value.ToUpper() == "УДАЛЕН ИЗ СИСТЕМЫ";

                        item.Passport = passport;
                        item.Addresses = new List<ExtendedAddress>();
                        item.Addresses.Add(address);
                        if (!string.IsNullOrEmpty(item.ReestrCode))
                            list.Add(item);
                        }
                        catch (Exception e5)
                        {
                            string str5 = e5.Message;
                            
                            s.Write(str5+" "+z1);
                        }
                    }
                }
                
            }
        }
        catch (Exception e1)
        {
            string str = e1.Message;
            
            
        }
        s.Close();


        client1.Close();
        
        return list.Where(i=>!i.IsReestrDelete).ToList();
    }

    public static string SendPupils(string _pathFiles, List<ExtendedPupil> pupils)
    {
        String _UrlWebService = ConfigurationManager.AppSettings["NewDogmService"]; 	// Change the web 
        ClassSoapClient sc = new ClassSoapClient("", new Uri(_UrlWebService));
        sc.Timeout = 10 * 60000;
        sc.Destination.Address = new Microsoft.Web.Services3.Addressing.Address(new Uri(_UrlWebService));
        XmlDocument docXML = new XmlDocument();
        docXML.Load(_pathFiles);

        GetNSIBatchPupilDoc(ref docXML, pupils);

        docXML.Save("c:\\udod\\sendPupils.xml");

        SoapEnvelope soapResponse = new SoapEnvelope();
        try
        {
            soapResponse = sc.SendEnvelope(docXML);
        }
        catch (Exception e)
        {
            FileInfo file = new FileInfo("c:\\udod\\ErrorSendPupil.txt");
            var writer = file.AppendText();
            writer.WriteLine(e.Message);
            writer.Flush();
            writer.Close();
        }
        
        XmlDocument responceDoc = new XmlDocument();
        responceDoc.InnerXml = soapResponse.InnerXml;
        var guidResult = responceDoc.GetElementsByTagName("uid")[0].InnerXml;
        return guidResult;

    }

    public static ExtendedPupil SendPupil(string _pathFiles, ExtendedPupil pupil, bool isUpdate)
    {
        String _UrlWebService = ConfigurationManager.AppSettings["NewDogmService"]; 	// Change the web 
        ClassSoapClient sc = new ClassSoapClient("", new Uri(_UrlWebService));
        sc.Timeout = 10 * 60000;
        sc.Destination.Address = new Microsoft.Web.Services3.Addressing.Address(new Uri(_UrlWebService));
        sc.SoapVersion = SoapProtocolVersion .Soap11;
        XmlDocument docXML = new XmlDocument();
        docXML.Load(_pathFiles);

        GetNSIPupilDoc(ref docXML, pupil, isUpdate);

        //docXML.Save("c:\\udod\\sendPupil.xml");

        SoapEnvelope soapResponse = new SoapEnvelope();
        try
        {
            soapResponse = sc.SendEnvelope(docXML);
        }
        catch (Exception e)
        {
            FileInfo file = new FileInfo("c:\\udod\\ErrorSendPupil"+Guid.NewGuid()+".txt");
            var writer = file.AppendText();
            writer.WriteLine(e.Message);
            writer.WriteLine(_UrlWebService);
            writer.Flush();
            writer.Close();
        }

        XmlDocument responceDoc = new XmlDocument();
        responceDoc.InnerXml = soapResponse.InnerXml;
        ExtendedPupil guidResult = null;
        try
        {
            guidResult = new ExtendedPupil() { ReestrGuid = new Guid(responceDoc.GetElementsByTagName("attribute")[2].InnerXml), ReestrCode = responceDoc.GetElementsByTagName("primaryKey")[0].InnerXml };
        }
        catch (Exception)
        {
        }
        
        return guidResult;

    }

    public static ExtendedPupil SendDO(string _pathFiles, ExtendedChildUnion childUnion, bool isUpdate)
    {
        String _UrlWebService = ConfigurationManager.AppSettings["NewDogmService"]; 	// Change the web 
        ClassSoapClient sc = new ClassSoapClient("", new Uri(_UrlWebService));
        sc.Timeout = 10 * 60000;
        sc.Destination.Address = new Microsoft.Web.Services3.Addressing.Address(new Uri(_UrlWebService));
        sc.SoapVersion = SoapProtocolVersion.Soap11;

        XmlDocument docXML = new XmlDocument();
        docXML.Load(_pathFiles);

        GetNSIDODoc(ref docXML, childUnion, isUpdate);

        //docXML.Save("c:\\udod\\sendPupil.xml");

        SoapEnvelope soapResponse = new SoapEnvelope();
        try
        {
            soapResponse = sc.SendEnvelope(docXML);
        }
        catch (Exception e)
        {
            FileInfo file = new FileInfo("c:\\udod\\ErrorSendPupil" + Guid.NewGuid() + ".txt");
            var writer = file.AppendText();
            writer.WriteLine(e.Message);
            writer.WriteLine(_UrlWebService);
            writer.Flush();
            writer.Close();
        }

        XmlDocument responceDoc = new XmlDocument();
        responceDoc.InnerXml = soapResponse.InnerXml;
        ExtendedPupil guidResult = null;
        try
        {
            guidResult = new ExtendedPupil() { ReestrGuid = new Guid(responceDoc.GetElementsByTagName("attribute")[2].InnerXml), ReestrCode = responceDoc.GetElementsByTagName("primaryKey")[0].InnerXml };
        }
        catch (Exception)
        {
        }

        return guidResult;

    }

    public static List<ExtendedPupil> GetPupilData(string _pathFiles, string series, string number)
    {
        List<ExtendedPupil> list= new List<ExtendedPupil>();

        NSIServiceClient client1 = new NSIServiceClient();
        client1.Endpoint.Behaviors.Add(new InterceptorCustomBehavior1());
        client1.Open();
        NSIRequestType type = new NSIRequestType();
        type.MessageData = new Service.NSI.ExtMessageDataType();
        type.MessageData.AppData = new ExtAppDataType();

        type.MessageData.AppData.context = new Context();
        type.MessageData.AppData.context.User = "UDOD_SOAP";
        type.MessageData.AppData.context.Password = "rlap0a3h";
        type.MessageData.AppData.context.Company = "dogm_nsi";
        
        ItemsChoiceType1[] itemsChoiceType1 = new ItemsChoiceType1[4];
        itemsChoiceType1[0] = ItemsChoiceType1.catalogName;
        itemsChoiceType1[1] = ItemsChoiceType1.matchAllCategories;
        itemsChoiceType1[2] = ItemsChoiceType1.searchPredicate;
        itemsChoiceType1[3] = ItemsChoiceType1.searchPredicate;
        
        //Item[] items = new Item[4];
       
        object[] items = new object[4];
        items[0] = "Реестр обучаемых";
        items[1] = true; //"true";

        //for (int k = 0; k < 1; k++)
        {
            SearchPredicate[] atributes = new SearchPredicate[2];
            int index = 0;
            #region один элемент Item
            atributes[index] = new SearchPredicate();
            atributes[index].attributeName = _pathFiles.Contains("Passport")?"Реестр обучаемых линейный/Серия паспорта": "Реестр обучаемых линейный/Серия свидетельства";
            atributes[index].attributeType = "STRING";
            atributes[index].attributeValue = series;
            atributes[index].attributeOp = "=";

            #endregion
            index++;
            #region один элемент Item
            atributes[index] = new SearchPredicate();
            atributes[index].attributeName = _pathFiles.Contains("Passport") ? "Реестр обучаемых линейный/Номер паспорта" : "Реестр обучаемых линейный/Номер свидетельства";
            atributes[index].attributeType = "STRING";
            atributes[index].attributeValue = number;
            atributes[index].attributeOp = "=";
            #endregion
            index++;

            items[2] = atributes[0];
            items[3] = atributes[1];
            
        }

        type.MessageData.AppData.Items = items;
        type.MessageData.AppData.ItemsElementName = itemsChoiceType1;

        try
        {
            {
                var tmpvalue = client1.searchItemsInCatalog(type);
                foreach(var tmp1 in tmpvalue.MessageData.AppData.Items)
                {
                    var pupil = (tmp1 as GeneralResponse);
                    
                    foreach(var itemreestr in pupil.item)
                    {
                        ExtendedPupil item = new ExtendedPupil();
                        ExtendedPassport passport = new ExtendedPassport();
                        ExtendedAddress address = new ExtendedAddress();

                        item.ReestrCode = itemreestr.attribute[0].value[0].Value;
                        try
                        {
                            item.ReestrGuid = new Guid(itemreestr.attribute[6].value[0].Value);
                        }
                        catch (Exception)
                        {
                            item.ReestrGuid = null;
                        }
                        item.LastName = itemreestr.attribute[1].value[0].Value;
                        item.FirstName = itemreestr.attribute[2].value[0].Value;
                        item.MiddleName = itemreestr.attribute[3].value[0].Value;
                        try
                        {
                            item.Birthday = Convert.ToDateTime(itemreestr.attribute[5].value[0].Value);
                        }
                        catch (Exception)
                        {
                            item.Birthday = DateTime.Now;
                        }
                        
                        item.Sex = itemreestr.attribute[4].value[0].Value == "Мужской" ? 0 : 1;
                        item.PhoneNumber = itemreestr.attribute[9].value[0].Value;

                        if (itemreestr.attribute[84].value[0].Value.ToLower() == "свидетельство о рождении")
                        {
                            passport.PassportType = new ExtendedPassportType() { PassportTypeId = 3 };
                        }
                        else if (itemreestr.attribute[84].value[0].Value.ToLower() == "паспорт гражданина российской федерации")
                        {
                            passport.PassportType = new ExtendedPassportType() { PassportTypeId = 2 };
                        }
                        else
                        {
                            passport.PassportType = new ExtendedPassportType() { PassportTypeId = 4 };
                        }

                        if (!_pathFiles.Contains("Passport"))
                        {
                            passport.Series = itemreestr.attribute[10].value[0].Value;
                            passport.Number = itemreestr.attribute[11].value[0].Value;
                        }
                        else
                        {
                            passport.Series = itemreestr.attribute[14].value[0].Value;
                            passport.Number = itemreestr.attribute[15].value[0].Value;
                        }
                        

                        try
                        {
                            passport.IssueDate = Convert.ToDateTime(itemreestr.attribute[12].value[0].Value);
                            if (passport.IssueDate == new DateTime(1, 1, 1) || !passport.IssueDate.HasValue) passport.IssueDate = DateTime.Now;
                        }
                        catch (Exception)
                        {
                            passport.IssueDate = DateTime.Now;
                        }

                        if (_pathFiles.Contains("Passport"))
                        {
                            try
                            {
                                passport.IssueDate = Convert.ToDateTime(itemreestr.attribute[16].value[0].Value);
                                if (passport.IssueDate == new DateTime(1, 1, 1) || !passport.IssueDate.HasValue) passport.IssueDate = DateTime.Now;
                                //passport.PassportType = new ExtendedPassportType() { PassportTypeId = 2 };
                            }
                            catch (Exception)
                            {
                                passport.IssueDate = DateTime.Now;
                            }
                        }
                        

                        item.AddressStr = itemreestr.attribute[34].value[0].Value;
                        address.HouseNumber = itemreestr.attribute[28].value[0].Value;
                        address.Fraction = itemreestr.attribute[29].value[0].Value;
                        address.Building = itemreestr.attribute[31].value[0].Value;
                        address.Housing = itemreestr.attribute[30].value[0].Value;
                        address.FlatNumber = itemreestr.attribute[32].value[0].Value;
                        try
                        {
                            address.BtiCode = Convert.ToInt32(itemreestr.attribute[36].value[0].Value);
                        }
                        catch (Exception)
                        {
                            address.BtiCode = null;
                        }


                        if (itemreestr.attribute[37].value[0].Value != "" && !address.BtiCode.HasValue && itemreestr.attribute[37].value[0].Value.Length > 8)
                        {
                            address.KladrCode = itemreestr.attribute[37].value[0].Value;

                        }
                        item.SchoolName = itemreestr.attribute[42].value[0].Value;
                        item.ClassName = itemreestr.attribute[44].value[0].Value;
                        item.EMail = itemreestr.attribute[81].value[0].Value;
                        item.SchoolCode = itemreestr.attribute[93].value[0].Value;

                        item.IsReestrDelete = itemreestr.attribute[79].value[0].Value.ToUpper() == "УДАЛЕННЫЙ" || itemreestr.attribute[79].value[0].Value.ToUpper() == "УДАЛЕН ИЗ ДОУ" || itemreestr.attribute[79].value[0].Value.ToUpper() == "УДАЛЕН ИЗ СИСТЕМЫ";

                        item.Passport = passport;
                        item.Addresses = new List<ExtendedAddress>();
                        item.Addresses.Add(address);
                        if (!string.IsNullOrEmpty(item.ReestrCode))
                            list.Add(item);
                    }
                }
                
            }
        }
        catch (Exception e1)
        {
            string str = e1.Message;
        }



        client1.Close();
        
        return list.Where(i=>!i.IsReestrDelete).ToList();
    }

    public static List<ExtendedPupil> GetPupilData(string _pathFiles, string reestrGuid)
    {
        List<ExtendedPupil> list = new List<ExtendedPupil>();
        String _UrlWebService = ConfigurationManager.AppSettings["DogmService"]; 	// Change the web 
        // service's URL here
        //String _pathFiles = GetUrl("~/Templates/");
        ClassSoapClient sc = new ClassSoapClient("", new Uri(_UrlWebService));
        sc.Timeout = 10 * 60000;
        sc.Destination.Address = new Microsoft.Web.Services3.Addressing.Address(new Uri(_UrlWebService));

        XmlDocument docXML = new XmlDocument();
        docXML.Load(_pathFiles);

        GetDoc(ref docXML, reestrGuid);
        //docXML.Save("c:\\udod\\getpupil_"+Guid.NewGuid().ToString()+".xml");
        SoapEnvelope soapResponse = new SoapEnvelope();
        soapResponse = sc.SendEnvelope(docXML);
        XmlDocument responceDoc = new XmlDocument();
        responceDoc.InnerXml = soapResponse.InnerXml;
        //responceDoc.Load(_pathFiles.Replace("search_by_Series_and_Number_rq.xml", "676807.xml"));
        int k = 1;
        foreach (XmlNode child in responceDoc.ChildNodes)
        {
            foreach (XmlNode child1 in child.ChildNodes)
            {
                foreach (XmlNode child2 in child1.ChildNodes)
                {
                    foreach (XmlNode child3 in child2.ChildNodes)
                    {
                        if (child3.Name == "smev:MessageData")
                        {
                            foreach (XmlNode child4 in child3.ChildNodes)
                            {
                                foreach (XmlNode child5 in child4.ChildNodes)
                                {
                                    if (child5.Name == "nsi:generalResponse")
                                        foreach (XmlNode child6 in child5.ChildNodes)
                                        {
                                            ExtendedPupil item = new ExtendedPupil();
                                            ExtendedPassport passport = new ExtendedPassport();
                                            ExtendedAddress address = new ExtendedAddress();
                                            k = 1;
                                            foreach (XmlNode child7 in child6.ChildNodes)
                                            {
                                                if (child7.Name == "nsi:attribute")
                                                {
                                                    //var tmp = child7.OfType<ExtendedAttribute>();
                                                    if (k == 1) item.ReestrCode = child7.ChildNodes[2].InnerXml;
                                                    if (k == 7)
                                                    {
                                                        try
                                                        {
                                                            item.ReestrGuid = new Guid(child7.ChildNodes[2].InnerXml);
                                                        }
                                                        catch (Exception)
                                                        {
                                                            item.ReestrGuid = null;
                                                        }

                                                    }
                                                    if (k == 2) item.LastName = child7.ChildNodes[2].InnerXml;
                                                    if (k == 3) item.FirstName = child7.ChildNodes[2].InnerXml;
                                                    if (k == 4) item.MiddleName = child7.ChildNodes[2].InnerXml;
                                                    if (k == 6)
                                                    {
                                                        try
                                                        {
                                                            item.Birthday = Convert.ToDateTime(child7.ChildNodes[2].InnerXml);
                                                        }
                                                        catch (Exception)
                                                        {
                                                            item.Birthday = DateTime.Now;
                                                        }

                                                    }
                                                    if (k == 5) item.Sex = child7.ChildNodes[2].InnerXml == "Мужской" ? 0 : 1;
                                                    if (k == 10) item.PhoneNumber = child7.ChildNodes[2].InnerXml;
                                                    if (k == 11)
                                                    {
                                                        passport.Series = child7.ChildNodes[2].InnerXml;
                                                        passport.PassportType = new ExtendedPassportType() { PassportTypeId = 3 };
                                                    }

                                                    if (k == 12) passport.Number = child7.ChildNodes[2].InnerXml;
                                                    if (k == 13)
                                                    {
                                                        try
                                                        {
                                                            passport.IssueDate = Convert.ToDateTime(child7.ChildNodes[2].InnerXml);
                                                        }
                                                        catch (Exception)
                                                        {
                                                            passport.IssueDate = DateTime.Now;
                                                        }

                                                    }
                                                    if (k == 17 && _pathFiles.Contains("Passport"))
                                                    {
                                                        try
                                                        {
                                                            passport.IssueDate = Convert.ToDateTime(child7.ChildNodes[2].InnerXml);
                                                            passport.PassportType = new ExtendedPassportType() { PassportTypeId = 2 };
                                                        }
                                                        catch (Exception)
                                                        {
                                                            passport.IssueDate = DateTime.Now;
                                                        }
                                                    }
                                                    if (k == 86)
                                                    {
                                                        if (child7.ChildNodes[2].InnerXml.ToLower() == "cвидетельство о рождении")
                                                        {
                                                            passport.PassportType = new ExtendedPassportType() { PassportTypeId = 3 };
                                                        }
                                                        else if (child7.ChildNodes[2].InnerXml.ToLower() == "паспорт гражданина российской федерации")
                                                        {
                                                            passport.PassportType = new ExtendedPassportType() { PassportTypeId = 2 };
                                                        }
                                                        else
                                                        {
                                                            passport.PassportType = new ExtendedPassportType() { PassportTypeId = 4 };
                                                        }
                                                    }
                                                    if (k == 35) item.AddressStr = child7.ChildNodes[2].InnerXml;
                                                    if (k == 29) address.HouseNumber = child7.ChildNodes[2].InnerXml;
                                                    if (k == 30) address.Fraction = child7.ChildNodes[2].InnerXml;
                                                    if (k == 32) address.Building = child7.ChildNodes[2].InnerXml;
                                                    if (k == 31) address.Housing = child7.ChildNodes[2].InnerXml;
                                                    if (k == 33) address.FlatNumber = child7.ChildNodes[2].InnerXml;
                                                    if (k == 37)
                                                    {
                                                        try
                                                        {
                                                            address.BtiCode = Convert.ToInt32(child7.ChildNodes[2].InnerXml);
                                                        }
                                                        catch (Exception)
                                                        {
                                                            address.BtiCode = null;
                                                        }

                                                    }
                                                    if (k == 38 && child7.ChildNodes[2].InnerXml != "" && !address.BtiCode.HasValue && child7.ChildNodes[2].InnerXml.Length > 8)
                                                    {
                                                        address.KladrCode = child7.ChildNodes[2].InnerXml;

                                                    }
                                                    if (k == 43) item.SchoolName = child7.ChildNodes[2].InnerXml;
                                                    if (k == 45) item.ClassName = child7.ChildNodes[2].InnerXml;
                                                    if (k == 82) item.EMail = child7.ChildNodes[2].InnerXml;
                                                    if (k == 92) item.SchoolCode = child7.ChildNodes[2].InnerXml;

                                                    if (k == 80) item.IsReestrDelete = child7.ChildNodes[2].InnerXml.ToUpper() == "УДАЛЕННЫЙ" || child7.ChildNodes[2].InnerXml.ToUpper() == "УДАЛЕН ИЗ ДОУ" || child7.ChildNodes[2].InnerXml.ToUpper() == "УДАЛЕН ИЗ СИСТЕМЫ";

                                                    k++;
                                                }
                                            }
                                            item.Passport = passport;
                                            item.Addresses = new List<ExtendedAddress>();
                                            item.Addresses.Add(address);
                                            if (!string.IsNullOrEmpty(item.ReestrCode))
                                                list.Add(item);
                                        }
                                }
                            }

                        }
                    }

                }
            }
        }

        return list.Where(i => !i.IsReestrDelete).ToList();
    }

    public static ExtendedPupil GetPupilData(string _pathFiles, Int64? reestrCode)
    {
        if (!reestrCode.HasValue) return new ExtendedPupil();
        String _UrlWebService = ConfigurationManager.AppSettings["DogmService"]; 	// Change the web 
        // service's URL here
        //String _pathFiles = GetUrl("~/Templates/");
        ClassSoapClient sc = new ClassSoapClient("", new Uri(_UrlWebService));
        sc.Timeout = 10 * 60000;
        sc.Destination.Address = new Microsoft.Web.Services3.Addressing.Address(new Uri(_UrlWebService));

        XmlDocument docXML = new XmlDocument();
        docXML.Load(_pathFiles);

        GetDoc(ref docXML, reestrCode.Value.ToString());

        SoapEnvelope soapResponse = new SoapEnvelope();
        soapResponse = sc.SendEnvelope(docXML);
        XmlDocument responceDoc = new XmlDocument();
        responceDoc.InnerXml = soapResponse.InnerXml;
        //responceDoc.Load(_pathFiles.Replace("search_by_ReestrCode_rq.xml", "12345.xml"));
        int k = 1;
        ExtendedPupil item = new ExtendedPupil();
        ExtendedPassport passport = new ExtendedPassport();
        ExtendedAddress address = new ExtendedAddress();
        foreach (XmlNode child in responceDoc.ChildNodes)
        {
            foreach (XmlNode child1 in child.ChildNodes)
            {
                foreach (XmlNode child2 in child1.ChildNodes)
                {
                    foreach (XmlNode child3 in child2.ChildNodes)
                    {
                        if (child3.Name == "smev:MessageData")
                        {
                            foreach (XmlNode child4 in child3.ChildNodes)
                            {
                                foreach (XmlNode child5 in child4.ChildNodes)
                                {
                                    if (child5.Name == "nsi:generalResponse")
                                        foreach (XmlNode child6 in child5.ChildNodes)
                                        {
                                            foreach (XmlNode child7 in child6.ChildNodes)
                                            {
                                                if (child7.Name == "nsi:attribute")
                                                {
                                                    //var tmp = child7.OfType<ExtendedAttribute>();
                                                    if (k == 1) item.ReestrCode = child7.ChildNodes[2].InnerXml;
                                                    if (k == 2) item.LastName = child7.ChildNodes[2].InnerXml;
                                                    if (k == 3) item.FirstName = child7.ChildNodes[2].InnerXml;
                                                    if (k == 4) item.MiddleName = child7.ChildNodes[2].InnerXml;
                                                    if (k == 6)
                                                    {
                                                        try
                                                        {
                                                            item.Birthday = Convert.ToDateTime(child7.ChildNodes[2].InnerXml);
                                                        }
                                                        catch (Exception)
                                                        {
                                                            item.Birthday = DateTime.Now;
                                                        }

                                                    }
                                                    if (k == 5) item.Sex = child7.ChildNodes[2].InnerXml == "Мужской" ? 0 : 1;
                                                    if (k == 10) item.PhoneNumber = child7.ChildNodes[2].InnerXml;
                                                    if (k == 11)
                                                    {
                                                        passport.Series = child7.ChildNodes[2].InnerXml;
                                                        passport.PassportType = new ExtendedPassportType() { PassportTypeId = 3 };
                                                    }

                                                    if (k == 12) passport.Number = child7.ChildNodes[2].InnerXml;
                                                    if (k == 13)
                                                    {
                                                        try
                                                        {
                                                            passport.IssueDate = Convert.ToDateTime(child7.ChildNodes[2].InnerXml);
                                                        }
                                                        catch (Exception)
                                                        {
                                                            passport.IssueDate = DateTime.Now;
                                                        }

                                                    }
                                                    if (k == 17 && _pathFiles.Contains("Passport"))
                                                    {
                                                        try
                                                        {
                                                            passport.IssueDate = Convert.ToDateTime(child7.ChildNodes[2].InnerXml);
                                                            passport.PassportType = new ExtendedPassportType() { PassportTypeId = 2 };
                                                        }
                                                        catch (Exception)
                                                        {
                                                            passport.IssueDate = DateTime.Now;
                                                        }
                                                    }
                                                    if (k == 86)
                                                    {
                                                        if (child7.ChildNodes[2].InnerXml.ToLower() == "cвидетельство о рождении")
                                                        {
                                                            passport.PassportType = new ExtendedPassportType() { PassportTypeId = 3 };
                                                        }
                                                        else if (child7.ChildNodes[2].InnerXml.ToLower() == "паспорт гражданина российской федерации")
                                                        {
                                                            passport.PassportType = new ExtendedPassportType() { PassportTypeId = 2 };
                                                        }
                                                        else
                                                        {
                                                            passport.PassportType = new ExtendedPassportType() { PassportTypeId = 4 };
                                                        }
                                                    }
                                                    if (k == 35) item.AddressStr = child7.ChildNodes[2].InnerXml;
                                                    if (k == 29) address.HouseNumber = child7.ChildNodes[2].InnerXml;
                                                    if (k == 30) address.Fraction = child7.ChildNodes[2].InnerXml;
                                                    if (k == 32) address.Building = child7.ChildNodes[2].InnerXml;
                                                    if (k == 31) address.Housing = child7.ChildNodes[2].InnerXml;
                                                    if (k == 33) address.FlatNumber = child7.ChildNodes[2].InnerXml;
                                                    if (k == 37)
                                                    {
                                                        try
                                                        {
                                                            address.BtiCode = Convert.ToInt32(child7.ChildNodes[2].InnerXml);
                                                        }
                                                        catch (Exception)
                                                        {
                                                            address.BtiCode = null;
                                                        }

                                                    }
                                                    if (k == 38 && child7.ChildNodes[2].InnerXml != "" && !address.BtiCode.HasValue && child7.ChildNodes[2].InnerXml.Length > 8)
                                                    {
                                                        address.KladrCode = child7.ChildNodes[2].InnerXml;

                                                    }
                                                    if (k == 43) item.SchoolName = child7.ChildNodes[2].InnerXml;
                                                    if (k == 45) item.ClassName = child7.ChildNodes[2].InnerXml;
                                                    if (k == 82) item.EMail = child7.ChildNodes[2].InnerXml;
                                                    if (k == 92) item.SchoolCode = child7.ChildNodes[2].InnerXml;
                                                    if (k == 80) item.IsReestrDelete = child7.ChildNodes[2].InnerXml.ToUpper() == "УДАЛЕННЫЙ" || child7.ChildNodes[2].InnerXml.ToUpper() == "УДАЛЕН ИЗ ДОУ" || child7.ChildNodes[2].InnerXml.ToUpper() == "УДАЛЕН ИЗ СИСТЕМЫ";
                                                    k++;
                                                }
                                            }

                                        }
                                }
                            }

                        }
                    }

                }
            }
        }
        item.Passport = passport;
        item.Addresses = new List<ExtendedAddress>();
        item.Addresses.Add(address);
        return item;
    }
    */
    public static string SiteUrl
    {
        get
        {
            string host = ConfigurationManager.AppSettings["SiteUrl"];
            return string.Concat(host, !host.EndsWith("/") ? "/" : "");
        }
    }

    public static string GetUrl(string url)
    {
        return url.Replace("~/", SiteUrl);
    }

    public enum TransliterationType
    {
        Gost,
        ISO
    }

    public static class Transliteration
    {
        private static Dictionary<string, string> gost = new Dictionary<string, string>(); //ГОСТ 16876-71
        private static Dictionary<string, string> iso = new Dictionary<string, string>(); //ISO 9-95

        public static string Front(string text)
        {
            return Front(text, TransliterationType.ISO);
        }

        public static string Front(string text, TransliterationType type)
        {
            string output = text;
            Dictionary<string, string> tdict = GetDictionaryByType(type);

            foreach (KeyValuePair<string, string> key in tdict)
            {
                output = output.Replace(key.Key, key.Value);
            }
            return output;
        }

        public static string Back(string text)
        {
            return Back(text, TransliterationType.ISO);
        }

        public static string Back(string text, TransliterationType type)
        {
            string output = text;
            Dictionary<string, string> tdict = GetDictionaryByType(type);

            foreach (KeyValuePair<string, string> key in tdict)
            {
                output = output.Replace(key.Value, key.Key);
            }
            return output;
        }

        private static Dictionary<string, string> GetDictionaryByType(TransliterationType type)
        {
            Dictionary<string, string> tdict = iso;
            if (type == TransliterationType.Gost) tdict = gost;
            return tdict;
        }

        static Transliteration()
        {
            gost.Add("Є", "EH");
            gost.Add("І", "I");
            gost.Add("і", "i");
            gost.Add("№", "#");
            gost.Add("є", "eh");
            gost.Add("А", "A");
            gost.Add("Б", "B");
            gost.Add("В", "V");
            gost.Add("Г", "G");
            gost.Add("Д", "D");
            gost.Add("Е", "E");
            gost.Add("Ё", "JO");
            gost.Add("Ж", "ZH");
            gost.Add("З", "Z");
            gost.Add("И", "I");
            gost.Add("Й", "JJ");
            gost.Add("К", "K");
            gost.Add("Л", "L");
            gost.Add("М", "M");
            gost.Add("Н", "N");
            gost.Add("О", "O");
            gost.Add("П", "P");
            gost.Add("Р", "R");
            gost.Add("С", "S");
            gost.Add("Т", "T");
            gost.Add("У", "U");
            gost.Add("Ф", "F");
            gost.Add("Х", "KH");
            gost.Add("Ц", "C");
            gost.Add("Ч", "CH");
            gost.Add("Ш", "SH");
            gost.Add("Щ", "SHH");
            gost.Add("Ъ", "'");
            gost.Add("Ы", "Y");
            gost.Add("Ь", "");
            gost.Add("Э", "EH");
            gost.Add("Ю", "YU");
            gost.Add("Я", "YA");
            gost.Add("а", "a");
            gost.Add("б", "b");
            gost.Add("в", "v");
            gost.Add("г", "g");
            gost.Add("д", "d");
            gost.Add("е", "e");
            gost.Add("ё", "jo");
            gost.Add("ж", "zh");
            gost.Add("з", "z");
            gost.Add("и", "i");
            gost.Add("й", "jj");
            gost.Add("к", "k");
            gost.Add("л", "l");
            gost.Add("м", "m");
            gost.Add("н", "n");
            gost.Add("о", "o");
            gost.Add("п", "p");
            gost.Add("р", "r");
            gost.Add("с", "s");
            gost.Add("т", "t");
            gost.Add("у", "u");

            gost.Add("ф", "f");
            gost.Add("х", "kh");
            gost.Add("ц", "c");
            gost.Add("ч", "ch");
            gost.Add("ш", "sh");
            gost.Add("щ", "shh");
            gost.Add("ъ", "");
            gost.Add("ы", "y");
            gost.Add("ь", "");
            gost.Add("э", "eh");
            gost.Add("ю", "yu");
            gost.Add("я", "ya");
            gost.Add("«", "");
            gost.Add("»", "");
            gost.Add("—", "-");

            iso.Add("Є", "YE");
            iso.Add("І", "I");
            iso.Add("Ѓ", "G");
            iso.Add("і", "i");
            iso.Add("№", "#");
            iso.Add("є", "ye");
            iso.Add("ѓ", "g");
            iso.Add("А", "A");
            iso.Add("Б", "B");
            iso.Add("В", "V");
            iso.Add("Г", "G");
            iso.Add("Д", "D");
            iso.Add("Е", "E");
            iso.Add("Ё", "YO");
            iso.Add("Ж", "ZH");
            iso.Add("З", "Z");
            iso.Add("И", "I");
            iso.Add("Й", "J");
            iso.Add("К", "K");
            iso.Add("Л", "L");
            iso.Add("М", "M");
            iso.Add("Н", "N");
            iso.Add("О", "O");
            iso.Add("П", "P");
            iso.Add("Р", "R");
            iso.Add("С", "S");
            iso.Add("Т", "T");
            iso.Add("У", "U");
            iso.Add("Ф", "F");
            iso.Add("Х", "X");
            iso.Add("Ц", "C");
            iso.Add("Ч", "CH");
            iso.Add("Ш", "SH");
            iso.Add("Щ", "SHH");
            iso.Add("Ъ", "'");
            iso.Add("Ы", "Y");
            iso.Add("Ь", "");
            iso.Add("Э", "E");
            iso.Add("Ю", "YU");
            iso.Add("Я", "YA");
            iso.Add("а", "a");
            iso.Add("б", "b");
            iso.Add("в", "v");
            iso.Add("г", "g");
            iso.Add("д", "d");
            iso.Add("е", "e");
            iso.Add("ё", "yo");
            iso.Add("ж", "zh");
            iso.Add("з", "z");
            iso.Add("и", "i");
            iso.Add("й", "j");
            iso.Add("к", "k");
            iso.Add("л", "l");
            iso.Add("м", "m");
            iso.Add("н", "n");
            iso.Add("о", "o");
            iso.Add("п", "p");
            iso.Add("р", "r");
            iso.Add("с", "s");
            iso.Add("т", "t");
            iso.Add("у", "u");
            iso.Add("ф", "f");
            iso.Add("х", "x");
            iso.Add("ц", "c");
            iso.Add("ч", "ch");
            iso.Add("ш", "sh");
            iso.Add("щ", "shh");
            iso.Add("ъ", "");
            iso.Add("ы", "y");
            iso.Add("ь", "");
            iso.Add("э", "e");
            iso.Add("ю", "yu");
            iso.Add("я", "ya");
            iso.Add("«", "");
            iso.Add("»", "");
            iso.Add("—", "-");
        }
    }
}

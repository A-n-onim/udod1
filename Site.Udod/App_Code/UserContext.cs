﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using Udod.Dal;

namespace Site.Udod.Context
{

    /// <summary>
    /// Summary description for UserContext
    /// </summary>
    public class UserContext
    {
        #region public

        public UserRole Roles { get; set; }
        public ProfileCommon Profile { get; set; }

        public long? UdodId { get { return (long?)Session["UdodId"]; } 
            set 
            { 
                Session["UdodId"] = value;
            if (value.HasValue)
            {
                using (UdodDb db = new UdodDb())
                {
                    UdodTypeId = db.GetUdod(value.Value).UdodTypeId;
                }
            }
            else
            {
                UdodTypeId = null;
            }
            } }
        public long? UdodTypeId { get { return (long?)Session["UdodTypeId"]; } set { Session["UdodTypeId"] = value; } }
        public int? CityId { get { return (int?)Session["CityId"]; } set { Session["CityId"] = value; } }
        public long? ClaimId { get { return (long?)Session["ClaimId"]; } set { Session["ClaimId"] = value; } }

        public bool IsAuthenticated
        {
            get { return HttpContext.Current.Request.IsAuthenticated; }
        }

        public HttpApplication ApplicationInstance
        {
            get { return HttpContext.Current.ApplicationInstance; }
        }
        protected HttpSessionState Session
        {
            get { return HttpContext.Current.Session; }
        }

        public void Load()
        {
            Roles.Load();
        }

        #endregion

        #region Private


        #endregion

        public UserContext()
        {
            Profile = (new ProfileCommon()).GetProfile(HttpContext.Current.Profile.UserName);
            
            Roles = new UserRole(this);
        }
    }
}
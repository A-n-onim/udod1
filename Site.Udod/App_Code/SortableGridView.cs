﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.ComponentModel;

namespace Site.Udod
{
    /// <summary>
    /// SortableGridView
    /// </summary>
    public class SortableGridView : GridView
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            string js = "return !((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13))";
            Attributes.Add("onkeydown", js);
        }

        //protected override void OnPreRender(EventArgs e)
        //{
            //base.OnPreRender(e);

            //AllowSorting = Rows.Count > 1 && PageCount >= 1;

            //if (!AllowSorting)
            //{
            //    if (HeaderRow != null)
            //    {
            //        foreach (TableCell cell in HeaderRow.Cells)
            //        {
            //            cell.CssClass = cell.CssClass.Replace(" sortascheader", "").Replace(" sortdescheader", "");
            //        }
            //    }

            //    foreach (GridViewRow row in Rows)
            //    {
            //        foreach (TableCell cell in row.Cells)
            //        {
            //            cell.CssClass = cell.CssClass.Replace(" sortaltrow", "").Replace(" sortrow", "");
            //        }
            //    }
            //}

        //    if (AdditionalCss.Length > 0)
        //    {
        //        CssClass = string.Concat(CssClass, " ", AdditionalCss);
        //    }
        //}

        protected override void OnRowDataBound(GridViewRowEventArgs e)
        {
            base.OnRowDataBound(e);

            if (SortExpression.Length > 0)
            {
                int cellIndex = -1;

                foreach (DataControlField field in Columns)
                {
                    if (field.SortExpression == SortExpression)
                    {
                        cellIndex = Columns.IndexOf(field);
                        break;
                    }
                }

                if (cellIndex > -1)
                {
                    if (e.Row.RowType == DataControlRowType.Header)
                    {
                        //  this is a header row, set the sort style
                        e.Row.Cells[cellIndex].CssClass += (SortDirection == SortDirection.Ascending ? " sortascheader" : " sortdescheader");
                    }
                    else if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        //  this is an alternating row
                        e.Row.Cells[cellIndex].CssClass += (e.Row.RowIndex % 2 == 0 ? " sortaltrow" : "sortrow");
                    }
                }

            }
        }

        /// <summary>
        /// Подсветка отредактированной строки в гриде
        /// </summary>
        /// <param name="row">номер строки</param>
        public void HighlightRow(int row)
        {
            HighlightRow(row, 5000);
        }

        /// <summary>
        /// Подсветка строки в гриде
        /// </summary>
        /// <param name="row">номер строки</param>
        /// <param name="duration">Продолжительность эффекта в мс</param>
        public void HighlightRow(int row, int duration)
        {
            if (null != HeaderRow && HeaderRow.Visible)
                row++;

            var script = string.Concat("$('#", ClientID, " tr:eq(", row, ") td').effect('highlight', { color: '#9999FF' }, ", duration, ")");

            ScriptManager.RegisterStartupScript(this, GetType(), "HighlightRow", script, true);
        }

        //[Browsable(true)]
        //[Description("Additional css class")]
        //[Category("Behavior")]
        //public string AdditionalCss
        //{
        //    get { return (string)(this.ViewState["AdditionalCss"] ?? string.Empty); }
        //    set { this.ViewState["AdditionalCss"] = value; }
        //}

        [Browsable(true)]
        [Description("Default sort expression")]
        [Category("Behavior")]
        public string DefaultSortExpression
        {
            get
            {
                string defaultSortExpression = (string)this.ViewState["DefaultSortExpression"];
                if (defaultSortExpression == null)
                {
                    return string.Empty;
                }
                return defaultSortExpression;
            }
            set
            {
                this.ViewState["DefaultSortExpression"] = value;
            }
        }

        [Browsable(true)]
        [Description("Default sort direction")]
        [Category("Behavior")]
        public SortDirection DefaultSortDirection
        {
            get
            {
                object defaultSortDirection = this.ViewState["DefaultSortDirection"];
                if (defaultSortDirection == null)
                {
                    return SortDirection.Ascending;
                }
                return (SortDirection)defaultSortDirection;
            }
            set
            {
                this.ViewState["DefaultSortDirection"] = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.DefaultSortExpression) && !Page.IsPostBack)
            {
                this.Sort(this.DefaultSortExpression, this.DefaultSortDirection);
            }

            base.OnInit(e);
        }

        protected override DataSourceSelectArguments CreateDataSourceSelectArguments()
        {
            var arguments = base.CreateDataSourceSelectArguments();

            //Sets up our default sort expression and direction on the gridview.  
            if (String.IsNullOrEmpty(arguments.SortExpression) && (!String.IsNullOrEmpty(DefaultSortExpression)))
            {
                arguments.SortExpression = DefaultSortExpression;
                if (!arguments.SortExpression.EndsWith("DESC") && !arguments.SortExpression.EndsWith("ASC"))
                {
                    if (DefaultSortDirection == SortDirection.Descending)
                    {
                        arguments.SortExpression += " DESC";
                    }
                    else
                    {
                        arguments.SortExpression += " ASC";
                    }
                }
            }

            return arguments;
        }
    }
}
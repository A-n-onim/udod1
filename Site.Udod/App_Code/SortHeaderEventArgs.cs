﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SortHeaderEventArgs
/// </summary>
public class SortHeaderEventArgs : EventArgs
{
    public string SortExpression { get; set; }
    public string SortDirection { get; set; }

	public SortHeaderEventArgs()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Udod.Dal;
using Udod.Dal.Enum;
using System.Text;

/// <summary>
/// Summary description for EventsUtility
/// </summary>
public class EventsUtility
{
    public static void LogEvent(EnumEventTypes eventType, Guid? userId, string remoteIP, string message, Guid? pupilId, long? claimId, long? udodId, long? childUnionId)
    {
        using (EventsDb db = new EventsDb())
        {
            db.AddEvent(eventType, userId, remoteIP, message, pupilId, claimId, udodId, childUnionId);
        }
    }

    public static void Loglogin(Guid? userId, string remoteIP, string message)
    {
        LogEvent(EnumEventTypes.Login, userId, remoteIP, message, null, null, null, null);
    }

    public static void LogError(Guid? userId, string remoteIP, string message)
    {
        LogEvent(EnumEventTypes.ApplicationError, userId, remoteIP, message,null,null,null,null);
    }

    public static void LogClaimCreate(Guid? userId, string remoteIP, string message, Guid pupilId, long claimId, long udodId, long childUnionId)
    {
        LogEvent(EnumEventTypes.ClaimCreated, userId, remoteIP, message, pupilId, claimId, udodId, childUnionId);
    }

    public static void LogPupilUpdate(Guid? userId, string remoteIP, string message)
    {
        LogEvent(EnumEventTypes.PupilUpdate, userId, remoteIP, message, null, null, null, null);
    }

    public static void LogPupilUdodUpdate(Guid? userId, string remoteIP, string message, Guid pupilId, long udodId, long childUnionId)
    {
        LogEvent(EnumEventTypes.PupilUdodUpdate, userId, remoteIP, message, pupilId, null, udodId, childUnionId);
    }

    public static void LogPupilUdodDelete(Guid? userId, string remoteIP, string message, Guid pupilId, long udodId, long childUnionId)
    {
        LogEvent(EnumEventTypes.PupilUdodDelete, userId, remoteIP, message, pupilId, null, udodId, childUnionId);
    }

    public static void LogClaimUpdate(Guid? userId, string remoteIP, string message)
    {
        LogEvent(EnumEventTypes.ClaimEdited, userId, remoteIP, message, null, null, null, null);
    }

    public static void LogClaimStatusUpdate(Guid? userId, string remoteIP, string message, Guid pupilId, long claimId, long udodId, long childUnionId)
    {
        LogEvent(EnumEventTypes.ClaimStatusChanged, userId, remoteIP, message, pupilId, claimId, udodId, childUnionId);
    }

    public static void ScheduleLessonAdd(Guid? userId, string remoteIP, string message, long? udodId, long childUnionId)
    {
        LogEvent(EnumEventTypes.AddScheduleLesson, userId, remoteIP, message, null, null, udodId, childUnionId);
    }

    public static void ScheduleLessonUpdate(Guid? userId, string remoteIP, string message, long? udodId, long childUnionId)
    {
        LogEvent(EnumEventTypes.UpdateScheduleLesson, userId, remoteIP, message, null, null, udodId, childUnionId);
    }

    public static void ScheduleLessonDelete(Guid? userId, string remoteIP, string message, long? udodId)
    {
        LogEvent(EnumEventTypes.DeleteScheduleLesson, userId, remoteIP, message, null, null, udodId, null);
    }

    public static string GetClaimUpdateMessage(ExtendedClaim prev, ExtendedClaim curr)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendFormat("(Номер заявления: {0})", prev.Number);

        if (prev.ClaimStatus.ClaimStatusId != curr.ClaimStatus.ClaimStatusId)
            sb.AppendFormat("(Статус: {0} -> {1})", prev.ClaimStatus.ClaimStatusName, curr.ClaimStatus.ClaimStatusName);

        string changedSection = "(Сведения о заявителе: "; // Название группы полей
        if (prev.Parent.Fio != curr.Parent.Fio)
        {
            sb.AppendFormat("{0}(ФИО: {1} -> {2})", changedSection, prev.Parent.Fio, curr.Parent.Fio);
            changedSection = "";
        }

        if (prev.Parent.Passport.PassportType.PassportTypeId != curr.Parent.Passport.PassportType.PassportTypeId)
        {
            sb.AppendFormat("{0}(Тип документа: : {1} -> {2})", changedSection, prev.Parent.Passport.PassportType.PassportTypeName, curr.Parent.Passport.PassportType.PassportTypeName);
            changedSection = "";
        }

        if (prev.Parent.Passport.Series != curr.Parent.Passport.Series)
        {
            sb.AppendFormat("{0}(Серия: {1} -> {2})", changedSection, prev.Parent.Passport.Series, curr.Parent.Passport.Series);
            changedSection = "";
        }

        if (prev.Parent.Passport.Number != curr.Parent.Passport.Number)
        {
            sb.AppendFormat("{0}(Номер: {1} -> {2})", changedSection, prev.Parent.Passport.Number, curr.Parent.Passport.Number);
            changedSection = "";
        }

        if (prev.Parent.Passport.IssueDate != curr.Parent.Passport.IssueDate)
        {
            sb.AppendFormat("{0}(Дата выдачи: {1} -> {2})", changedSection,
                prev.Parent.Passport.IssueDate.HasValue ? prev.Parent.Passport.IssueDate.Value.ToShortDateString() : "",
                curr.Parent.Passport.IssueDate.HasValue ? curr.Parent.Passport.IssueDate.Value.ToShortDateString() : "");
            changedSection = "";
        }

        if (prev.Parent.PhoneNumber != curr.Parent.PhoneNumber)
        {
            sb.AppendFormat("{0}(Телефон: {1} -> {2})", changedSection, prev.Parent.PhoneNumber, curr.Parent.PhoneNumber);
            changedSection = "";
        }

        if (prev.Parent.EMail != curr.Parent.EMail)
        {
            sb.AppendFormat("{0}(EMail: {1} -> {2})", changedSection, prev.Parent.EMail, curr.Parent.EMail);
            changedSection = "";
        }

        if (String.IsNullOrEmpty(changedSection))
            sb.Append(")");
        else
            changedSection = "(Сведения о будущем обучающемся: ";

        if (prev.Pupil.Fio != curr.Pupil.Fio)
        {
            sb.AppendFormat("{0}(ФИО: {1} -> {2})", changedSection, prev.Pupil.Fio, curr.Pupil.Fio);
            changedSection = "";
        }

        if (prev.Pupil.Birthday != curr.Pupil.Birthday)
        {
            sb.AppendFormat("{0}(Дата рождения: {1} -> {2})", changedSection, prev.Pupil.Birthday.ToShortDateString(), curr.Pupil.Birthday.ToShortDateString());
            changedSection = "";
        }

        if (prev.Pupil.Sex != curr.Pupil.Sex)
        {
            sb.AppendFormat("{0}(Пол: {1} -> {2})", changedSection, prev.Pupil.Sex, curr.Pupil.Sex);
            changedSection = "";
        }

        if (String.IsNullOrEmpty(changedSection))
            sb.Append(")");

        if (prev.Udod.UdodId != curr.Udod.UdodId || prev.ChildUnion.ChildUnionId != curr.ChildUnion.ChildUnionId)
            sb.AppendFormat("(Детское объединение/организация: {0}/{1} -> {2}/{3})", prev.ChildUnion.Name, prev.Udod.Name, curr.ChildUnion.Name, curr.Udod.Name);

        if (prev.BeginDate != curr.BeginDate)
        {
            sb.AppendFormat("(Дата желаемого начала обучения: {0} -> {1})", prev.BeginDate.ToShortDateString(), curr.BeginDate.ToShortDateString());
        }

        return sb.ToString();
    }

    public static void LogUdodCreate(Guid? userId, string remoteIP, string message)
    {
        LogEvent(EnumEventTypes.UdodCreated, userId, remoteIP, message, null, null, null, null);
    }

    public static void LogUdodUpdate(Guid? userId, string remoteIP, string message)
    {
        LogEvent(EnumEventTypes.UdodUpdated, userId, remoteIP, message, null, null, null, null);
    }

    public static void LogUdodDelete(Guid? userId, string remoteIP, string message)
    {
        LogEvent(EnumEventTypes.UdodDeleted, userId, remoteIP, message, null, null, null, null);
    }

    public static void LogUdodStatusUpdate(Guid? userId, string remoteIP, string message)
    {
        LogEvent(EnumEventTypes.UdodStatusUpdate, userId, remoteIP, message, null, null, null, null);
    }

    public static void LogUdodMerge(Guid? userId, string remoteIP, string message)
    {
        LogEvent(EnumEventTypes.UdodMerged, userId, remoteIP, message, null, null, null, null);
    }

    public static void LogUdodAttach(Guid? userId, string remoteIP, string message)
    {
        LogEvent(EnumEventTypes.UdodAttached, userId, remoteIP, message, null, null, null, null);
    }

    public static void LogUdodDetach(Guid? userId, string remoteIP, string message)
    {
        LogEvent(EnumEventTypes.UdodDetached, userId, remoteIP, message, null, null, null, null);
    }

    public static void LogUdodDivide(Guid? userId, string remoteIP, string message)
    {
        LogEvent(EnumEventTypes.UdodDivided, userId, remoteIP, message, null, null, null, null);
    }
}
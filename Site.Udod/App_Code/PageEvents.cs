﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Udod.Context
{
    /// <summary>
    /// Различные события страницы
    /// </summary>
    public class PageEvents
    {
        private UserContext context;

        public PageEvents(UserContext context)
        {
            this.context = context;
        }

        public event EventHandler UdodChanged;
        public void UdodChangedEvent()
        {
            if (UdodChanged != null)
            {
                UdodChanged(this, null);
            }
        }

        public event EventHandler CityChanged;
        public void CityChangedEvent()
        {
            if (CityChanged != null)
            {
                CityChanged(this, null);
            }
        }

    }
}
﻿<%@ Page EnableEventValidation="false" Language="C#" MasterPageFile="Master/MasterLogin.master" AutoEventWireup="true" CodeFile="Maintenance.aspx.cs" Inherits="Maintenance" %>
<%@ Register src="UserControl/Menu/ucTimer.ascx" tagName="Timer" tagPrefix="uct" %>
<asp:Content runat="server" ID="ContentMaintenance" ContentPlaceHolderID="body">

<h1>На сайте проводятся технические работы</h1>

<uct:Timer ID="timer" runat="server" />

<div class="btnBlueLong"><a href="default.aspx" runat="server" >Вернуться на Главную</a></div>
</asp:Content>
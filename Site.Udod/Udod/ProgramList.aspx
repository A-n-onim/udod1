﻿<%@ Page Language="C#" MasterPageFile="../Master/MasterPage.master" AutoEventWireup="true" CodeFile="ProgramList.aspx.cs" Inherits="Udod_ProgramList" %>

<asp:Content runat="server" ID="contentPrograms" ContentPlaceHolderID="body">
    <br />
    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="up">
    <ContentTemplate>
        <asp:LinkButton runat="server" ID="lnkAddProgram" OnClick="AddProgram_Click"><div class='btnBlueLong'>Добавить направление</div></asp:LinkButton>
        <asp:GridView runat="server" ID="gvProgramList" CssClass="dgc"  AutoGenerateColumns="false" DataKeyNames="ProgramId" 
                        DataSourceID="datasource" onselectedindexchanged="gvProgramList_SelectedIndexChanged">
            <EmptyDataTemplate>
                <asp:Label ID="lblEmptyGvProgramList" runat="server" style="font-size:16px">Нет объединений</asp:Label>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="№" >
                    <ItemTemplate><%# (gvProgramList.Rows.Count + 1)%></ItemTemplate>
                </asp:TemplateField>
                        
                <asp:TemplateField HeaderText="Наименование">
                    <ItemTemplate>
                        <%# Eval("Name") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField  ButtonType="Link" ShowSelectButton="true" HeaderText="Изменить" SelectText="<div class='btnBlue'>Изменить</div>"/>
                <asp:CommandField  ButtonType="Link" ShowDeleteButton="true" HeaderText="Удаление" DeleteText="<div class='btnRed'>Удалить</div>" />
            </Columns>
        </asp:GridView>
    </ContentTemplate>
    </asp:UpdatePanel>
    <asp:ObjectDataSource ID= "datasource" TypeName="Udod.Dal.DictProgramDb" DeleteMethod="DeleteProgram" runat="server" SelectMethod="GetPrograms"
        SelectCountMethod="GetProgramsCount">
        <SelectParameters>
            <asp:Parameter Name="udodId" ConvertEmptyStringToNull="true" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:UpdatePanel runat="server" ID="upPopup" UpdateMode="Conditional">
    <ContentTemplate>
    <asp:Panel runat="server" ID="popupAddProgram" BackColor="White" style="display: none; border: 2px solid black" CssClass="modalPopup">
        <asp:UpdatePanel ID="upAddProgram" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <asp:Panel ID="pnlHeader" runat="server" >
            <asp:Label ID="litTitle" Text="<span class='headerCaptions'>Добавление направления</span>" runat="server"/>
            <asp:LinkButton ID="lnkClose" runat="server" 
                OnClientClick="$find('AddProgramPopup').hide(); return false;" />
        </asp:Panel>
        <table><tr><td>
            Наименование:
            </td><td>
            <div class="inputXShort">
            <asp:TextBox runat="server" ID="tbProgramName" CssClass="inputXShort" />
            </div>
            </td></tr>
        </table>
        </ContentTemplate>
        </asp:UpdatePanel>
        <table><tr><td>
            <asp:LinkButton ID="btnAddProgram" runat="server" Text="Сохранить" CausesValidation="false" OnClick="SaveProgram_Click"><div class="btnBlue">Сохранить</div></asp:LinkButton>
            </td><td>
            <asp:LinkButton ID="btnCancel" runat="server" Text="Отменить" CausesValidation="false"><div class="btnRed">Отменить</div></asp:LinkButton>
            </td></tr></table>
        </div>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupAddExtender" PopupDragHandleControlID="pnlHeader"
        PopupControlID="popupAddProgram" CancelControlID="btnCancel"
        TargetControlID="btnShowPopup" RepositionMode="None"
        BehaviorID="AddProgramPopup"/>

    <asp:Button ID="btnShowPopup" runat="server" style="display:none" />
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnAddProgram"/>
    </Triggers>
    </asp:UpdatePanel>

</asp:Content>
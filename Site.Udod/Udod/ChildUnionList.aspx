﻿<%@ Page EnableEventValidation="false" Language="C#" MasterPageFile="../Master/MasterPage.master" AutoEventWireup="true" CodeFile="ChildUnionList.aspx.cs" Inherits="Udod_ChildUnionList" %>
<%@ Register src="../UserControl/Menu/ucPager.ascx" tagName="Pager" tagPrefix="uc" %>
<%@ Register src="../UserControl/Udod/ucDOPassport.ascx" tagName="DOPassport" tagPrefix="uct" %>
<%@ Register TagPrefix="uct" TagName="FindFilter" Src="~/UserControl/Filter/ucFindFilter.ascx" %>
<%@ Register tagPrefix="uc" namespace="Site.Udod" %>
<%@ Register TagPrefix="uct" TagName="SortHeader" Src="~/UserControl/Menu/ucSortHeader.ascx" %>
<%@ Register src="../UserControl/Filter/ucCityUdodFilter.ascx" tagName="CityUdodFilter" tagPrefix="uct" %>

<asp:Content runat="server" ID="ContentChildUnionList" ContentPlaceHolderID="body">
<script type="text/javascript">
    function pageLoad() {
        var html = document.documentElement;

        if (parseInt(html.scrollTop) < 200) {
            //alert(html.scrollTop);
            $('<%="#"+popupSelectDO.ClientID %>').css('position', 'absolute');
            $('<%="#"+popupSelectDO.ClientID %>').addClass('popupTop');
            //alert($('<%="#"+popupSelectDO.ClientID %>').css('top'));
        }
        else {
            $('<%="#"+popupSelectDO.ClientID %>').removeClass('popupTop');
        }
    }
</script>

<asp:UpdatePanel ID="upCityUdodFilter" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <uct:CityUdodFilter ID="cityUdodFilter" Visible="false" runat="server" UdodDdlEnabled="true" AddAllProperty="true" OnCityChange="cityUdodFilter_OnUdodChangeLoadComplete" OnUdodChange="cityUdodFilter_OnUdodChangeLoadComplete" OnloadComplete="cityUdodFilter_OnUdodChangeLoadComplete"/>
    </ContentTemplate>
</asp:UpdatePanel>

<table border=0>
    <tr >
        <td colspan=2>
            <asp:LinkButton runat="server" ID="addDO" onclick="AddDO_Click"><div class='btnBlueLong'>Добавить детское объединение</div></asp:LinkButton>
            <asp:LinkButton runat="server" ID="lnkESZ" onclick="lnkESZ_OnClick" Visible="false" OnClientClick="javascript:return confirm('Вы уверены?');"><div class='btnBlueLong'>отправить все ДО и ПЛАНЫ в ЕСЗ</div></asp:LinkButton>
            <asp:LinkButton runat="server" ID="lnkESZ1" onclick="lnkESZ1_OnClick" Visible="false" OnClientClick="javascript:return confirm('Вы уверены?');"><div class='btnBlueLong'>отправить все ПЛАНЫ в ЕСЗ</div></asp:LinkButton>
            <asp:LinkButton runat="server" ID="lnkESZ2" onclick="lnkESZ2_OnClick" Visible="false" OnClientClick="javascript:return confirm('Вы уверены?');"><div class='btnBlueLong'>отправить только ДО и пакетно</div></asp:LinkButton>
            <asp:LinkButton runat="server" ID="lnkESZ3" onclick="lnkESZ3_OnClick" Visible="false" OnClientClick="javascript:return confirm('Вы уверены?');"><div class='btnBlueLong'>Удалить удаленные ДО</div></asp:LinkButton>
            <asp:LinkButton runat="server" ID="lnkESZ4" onclick="lnkESZ4_OnClick" Visible="false" OnClientClick="javascript:return confirm('Вы уверены?');"><div class='btnBlueLong'>Отправить только планы и расписание</div></asp:LinkButton>
            <asp:LinkButton runat="server" ID="lnkESZ5" onclick="lnkESZ5_OnClick" Visible="false" OnClientClick="javascript:return confirm('Вы уверены?');"><div class='btnBlueLong'>Отправить удаление</div></asp:LinkButton>
            <asp:LinkButton runat="server" ID="lnkReestr" onclick="lnkReestr_OnClick" Visible="false" OnClientClick="javascript:return confirm('Вы уверены?');"><div class='btnBlueLong'>отправить в реестр</div></asp:LinkButton>
            <asp:LinkButton runat="server" ID="lnkESZ6" onclick="lnkSMS_OnClick" Visible="false" OnClientClick="javascript:return confirm('Вы уверены?');"><div class='btnBlueLong'>Отправить смс</div></asp:LinkButton>
        </td>
        <td>
            Поиск по Id ДО
            <asp:Textbox runat="server" ID="tbChildUnionId" CssClass="input"></asp:Textbox>
            <asp:RegularExpressionValidator ControlToValidate="tbChildUnionId" ValidationExpression="\d+" ErrorMessage="*"  runat="server" ID="RegularExpressionValidator5" SetFocusOnError="true" ></asp:RegularExpressionValidator>
        </td>
    </tr>
</table>

<asp:Panel DefaultButton="lnkFind" runat="server" ID="pFind">
<asp:UpdatePanel runat="server" ID="upFilter" UpdateMode="Conditional">
<ContentTemplate>
<table border=0>
    <tr>
        <td>
            <uct:FindFilter ID="filterSection" runat="server" BudgetTypeVisible="false"/>
        </td>
        <td>
            <table>
            <tr style="height: 40px;">
                <td>
                    Вид услуги
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:DropDownList CssClass="input" runat="server" ID="ddlBudgetType" DataSourceID="dsBudgetType" DataTextField="TypeBudgetName" DataValueField="DictTypeBudgetId" OnSelectedIndexChanged="ddlBudgetType_OnSelectedIndexChanged" OnDataBound="ddlBudgetType_OnDataBound" />
                    </td>
                </tr>
             </table>
        </td>
        <td>
            <table>
            <tr style="height: 40px;">
                <td>
                Содержит ошибки
                </td>
            </tr>
            <tr>
            <td>
            <asp:DropDownList runat="server" ID="ddlIsActive" CssClass="input">
                <Items>
                    <asp:ListItem runat="server" Value="">Все</asp:ListItem>
                    <asp:ListItem runat="server" Value="true">Без ошибок</asp:ListItem>
                    <asp:ListItem runat="server" Value="false">С ошибками</asp:ListItem>
                </Items>
            </asp:DropDownList>
            </td>
            </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <asp:LinkButton runat="server" ID="lnkFind" OnClick="lnkFind_OnClick"><div class="btnBlueLong">Поиск</div></asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton runat="server" ID="lnkClear" OnClick="lnkClear_OnClick"><div class="btnBlueLong">Сбросить фильтры</div></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:LinkButton runat="server" ID="lnkExport" OnClick="lnkExport_OnClick"><div class="btnBlueLong">Экспорт в Excel</div></asp:LinkButton>
        </td>
    </tr>
</table>
</ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="filterSection" />
    <asp:PostBackTrigger ControlID="lnkExport" />
</Triggers>
</asp:UpdatePanel>
</asp:Panel>

<table>
    <tr>
        <td colspan=2>
            <asp:UpdatePanel runat="server" ID="upDO" UpdateMode="Conditional">
            <ContentTemplate>
            <uc:SortableGridView ID="gvChildUnion" runat="server" AutoGenerateColumns="false" CssClass="dgc"
                            DataKeyNames="ChildUnionId" OnSelectedIndexChanged="gvChildUnion_SelectedIndexChanged" OnRowDeleting="gvChildUnion_Deleting" >
                    <EmptyDataTemplate>
                        <asp:Label ID="lblEmptyGvChildUnion" runat="server" style="font-size:16px">Нет детских объединений</asp:Label>
                    </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <%# gvChildUnion.Rows.Count + pgrChildUnionList.PagerIndex %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Название">
                        <HeaderTemplate>
                            <uct:SortHeader ID="shName" runat="server" HeaderCaption="Название" OnSortingChanged="gvChildUnionSortingChanged" SortExpression="Name"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("Name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Направленность">
                        <HeaderTemplate>
                            <uct:SortHeader ID="shProgramName" runat="server" HeaderCaption="Направленность" OnSortingChanged="gvChildUnionSortingChanged" SortExpression="ProgramName"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("Program.Name")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Профиль">
                        <HeaderTemplate>
                            <uct:SortHeader ID="shProfileName" runat="server" HeaderCaption="Профиль" OnSortingChanged="gvChildUnionSortingChanged" SortExpression="ProfileName"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("Profile.Name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Вид деятельности">
                        <HeaderTemplate>
                            <uct:SortHeader ID="shSectionName" runat="server" HeaderCaption="Вид деятельности" OnSortingChanged="gvChildUnionSortingChanged" SortExpression="SectionName"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("Section.Name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Педагог">
                        <%-- <HeaderTemplate>
                            <uct:SortHeader ID="shTeacher" runat="server" HeaderCaption="Педагог" OnSortingChanged="gvChildUnionSortingChanged" SortExpression="TeacherLastName"/>
                        </HeaderTemplate>--%>
                        <ItemTemplate>
                            <%# GetChildUnionTeachers(Container.DataItem) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Возраст обучающихся">
                        <HeaderTemplate>
                            <uct:SortHeader ID="shAge" runat="server" HeaderCaption="Возраст обучающихся" OnSortingChanged="gvChildUnionSortingChanged" SortExpression="AgeStart"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# GetChildUnionAge(Container.DataItem)%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ошибки">
                        <ItemTemplate>
                            <%# (bool)Eval("IsActive")?"":"Существуют ошибки" %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="true" SelectText="<img src='../Images/edit_smth.png' border=0 title='Изменить ДО'>"/>
                    <asp:CommandField ShowDeleteButton="true" DeleteText="<img src='../Images/delete_smth.png' border=0 title='Удалить ДО'>"/>
                </Columns>
                <PagerStyle CssClass="gridpager" VerticalAlign="Bottom" HorizontalAlign="Left"/>
                <PagerSettings Mode="Numeric" Position="Bottom" PageButtonCount="10" />
            </uc:SortableGridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger runat="server" ControlID="btnSaveDOPassport"/>
                <asp:AsyncPostBackTrigger runat="server" ControlID="btnCancelPopup" />
                <asp:AsyncPostBackTrigger runat="server" ControlID="filterSection" />
            </Triggers>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>

<uc:Pager runat="server" ID="pgrChildUnionList" needStartDots="false" needEndDots="false" pagesCount="10" OnChange="pgrChildUnionList_OnChange"/>
    
    <asp:Panel runat="server" Width="900" ID="popupSelectDO" style="padding: 5px; display: none; border: 2px solid black" BackColor="White">
        <asp:Panel ID="pnlHeader" runat="server" >
            <asp:Label ID="Label1" Text="<span class='headerCaptions'>Паспорт детского объединения</span>" runat="server"/>
            <asp:LinkButton ID="lnkClose" runat="server" 
                OnClientClick="$find('SelectDOPopup').hide(); return false;" />
                </asp:Panel>
                <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div >
                            <uct:DOPassport runat="server" ID="selectDO" Edit="true"/>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="addDO"/>
                    </Triggers>
                </asp:UpdatePanel>
            <div >
            <table>
                <tr>
                    <td>
                        <asp:LinkButton ID="btnSaveDOPassport" runat="server" CausesValidation="true" ValidationGroup="79" OnClick="btnSaveDOPassport_Click"><div class="btnBlue">Сохранить</div></asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnCancelPopup" runat="server" CausesValidation="false" OnClick="btnCloseDOPassport_Click"><div class="btnBlue">Отменить</div></asp:LinkButton>
                    </td>
                </tr>
            </table>
            </div>
    </asp:Panel>
    
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupSelectDOExtender" PopupDragHandleControlID="pnlHeader"
        PopupControlID="popupSelectDO"
        TargetControlID="btnShowSelectDO" RepositionMode="None"
        BehaviorID="SelectDOPopup"/>
    
    <asp:Button ID="btnShowSelectDO" runat="server" style="display:none" />

    <asp:Panel runat="server" ID="errorPopup" style="display: none; padding: 5px; border: 2px solid black" BackColor="White">
        <asp:UpdatePanel ID="upError" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pHeaderError" runat="server" >
                    <asp:Label ID="lblErrorHeader" Text="<span class='headerCaptions'>Ошибка</span>" runat="server"/>
                    <asp:LinkButton ID="LinkButton3" runat="server" 
                        OnClientClick="$find('PopupError').hide(); return false;" />
                </asp:Panel>
                <asp:Label ID="lblErrorText" Text="<div style='font-size: 18pt; padding-top:10px; padding-bottom: 10px;'>В данном УДО уже есть ДО с таким названием</div>" runat="server" />
                <table>
                    <tr>
                        <td>
                            <asp:LinkButton runat="server" Text="<div class='btnBlue'>Резерв</div>" ID="lnkErrorOk" OnClick="lnkErrorOkOnClick"/>
                        </td>
                        <td>
                            <asp:LinkButton runat="server" Text="<div class='btnBlue'>Отмена</div>" ID="lnkErrorCancel" OnClick="lnkErrorCancelOnClick"/>
                        </td>
                    </tr>
                </table>
                </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    
    <ajaxToolkit:ModalPopupExtender runat="server" ID="errorModalPopupExtender" 
        PopupControlID="errorPopup"
        TargetControlID="btnShowErrorModalPopup" RepositionMode="None" PopupDragHandleControlID="pHeaderError"
        CancelControlID="lnkErrorCancel"
        BehaviorID="PopupError"/>
    
    <asp:Button ID="btnShowErrorModalPopup" runat="server" style="display:none" />
    <asp:ObjectDataSource runat="server" ID="dsBudgetType" TypeName="Udod.Dal.DictBudgetTypeDb" SelectMethod="GetBudgetTypes" />
</asp:Content>
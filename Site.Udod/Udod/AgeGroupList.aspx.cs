﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Udod_AgeGroupList : BasePage
{
    private long? agegroupid { get { return (long?)ViewState["agegroupid"]; } set { ViewState["agegroupid"] = value; } }
    protected string OrderBy
    {
        get
        {
            return (string)ViewState["OrderBy"];
        }
        set
        {
            ViewState["OrderBy"] = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsUdodEmployee || UserContext.Roles.IsOsip);

        if (!Page.IsPostBack)
        {
            gvAgeGroupList.Columns[10].Visible = gvAgeGroupList.Columns[11].Visible = btnAddAgeGroup.Visible = lnkAddGroup.Visible = !UserContext.Roles.IsOsip;
            
            

            if (cityUdodFilter.SelectedUdod.HasValue)
            {
                using (ChildUnionDb db = new ChildUnionDb())
                {
                    var list =
                        db.GetChildUnionsShort(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null,
                                               null, null, null, null, null, null, null, null, null, null, null, null,false).
                            OrderBy(i => i.Name);
                    //var sections = list.Sections;
                    ddlChildUnion.DataSource = list;
                    ddlChildUnionsForAdd.DataSource = list;
                    ddlChildUnionsForAdd.DataBind();
                    upPopup.Update();
                    upfilter.Update();
                }
            }
            ddlChildUnion.DataBind();
            GroupDatabind();

            cvStartLesson1.ValueToCompare = DateTime.Now.AddYears(-1).ToShortDateString();
            cvStartLesson2.ValueToCompare = DateTime.Now.AddYears(2).ToShortDateString();

            cvStartClaim1.ValueToCompare = DateTime.Now.AddYears(-1).ToShortDateString();
            cvStartClaim2.ValueToCompare = DateTime.Now.AddYears(2).ToShortDateString();

            cvEndClaim1.ValueToCompare = DateTime.Now.AddYears(-1).ToShortDateString();
            cvEndClaim2.ValueToCompare = DateTime.Now.AddYears(2).ToShortDateString();
        }
    }

    private void PopupdataBind(int? childUnionId, Guid? teacherId, int? typeBudgetId, int? educationTypeId)
    {
        /*
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var list = db.GetChildUnions(UserContext.UdodId.Value, null, null, null, null);
            //var sections = list.Sections;
            //ddlChildUnionSelect.DataSource = list;
            //ddlChildUnionSelect.DataBind();
            //if (childUnionId.HasValue) ddlChildUnionSelect.SelectedIndex = ddlChildUnionSelect.Items.IndexOf(ddlChildUnionSelect.Items.FindByValue(childUnionId.Value.ToString()));
        }
        */
        /*
        using (TeacherDb db = new TeacherDb())
        {
            var list = db.GetTeacher(null, UserContext.UdodId.Value, Convert.ToInt32(ddlChildUnionSelect.SelectedValue));
            ddlTeacher.DataSource = list;
            ddlTeacher.DataBind();
            if (teacherId.HasValue) ddlTeacher.SelectedIndex = ddlTeacher.Items.IndexOf(ddlTeacher.Items.FindByValue(teacherId.Value.ToString()));
        }
        */
        /*
        using (DictBudgetTypeDb db = new DictBudgetTypeDb())
        {
            var list = db.GetBudgetTypes();
            ddlTypeBudget.DataSource = list;
            ddlTypeBudget.DataBind();
            if (typeBudgetId.HasValue) ddlTypeBudget.SelectedIndex = ddlTypeBudget.Items.IndexOf(ddlTypeBudget.Items.FindByValue(typeBudgetId.Value.ToString()));
        }
        *//*
        using (DictEducationTypeDb db = new DictEducationTypeDb())
        {
            var list = db.GetEducationTypes();
            ddlEducationType.DataSource = list;
            ddlEducationType.DataBind();
            if (educationTypeId.HasValue) ddlEducationType.SelectedIndex = ddlEducationType.Items.IndexOf(ddlEducationType.Items.FindByValue(educationTypeId.Value.ToString()));
        }*/
    }

    protected void ddlSection_OnDataBound(object sender, EventArgs e)
    {
        ddlChildUnion.Items.Insert(0,new ListItem("Все",""));
    }

    protected void gvAgeGroupList_OnSelectedIndexChanged(object sender, EventArgs e)
    {

        // редактирование
        litTitle.Text = "Редактирование возрастной группы";
        agegroupid = (long)gvAgeGroupList.SelectedDataKey.Value;
        
        using (AgeGroupDb db = new AgeGroupDb())
        {
            ExtendedGroup agegroup = db.GetGroup(agegroupid.Value);
            tbAgeStart.Text = agegroup.AgeStart.ToString();
            tbAgeEnd.Text = agegroup.AgeEnd.ToString();
            tbName.Text = agegroup.Name;
            tbComment.Text = agegroup.Comment;
            tbPlace.Text = agegroup.Place;
            ddlChildUnionsForAdd.SelectedIndex = ddlChildUnionsForAdd.Items.IndexOf(ddlChildUnionsForAdd.Items.FindByValue(agegroup.ChildUnionId.ToString()));

            ddlChildUnionsForAdd_OnSelectedIndexChanged(sender, e);

            ddlTypeBudgetForAdd.SelectedIndex = ddlTypeBudgetForAdd.Items.IndexOf(ddlTypeBudgetForAdd.Items.FindByValue(agegroup.TypeBudget.DictTypeBudgetId.ToString()));
            ddlEducationType.SelectedIndex = ddlEducationType.Items.IndexOf(ddlEducationType.Items.FindByValue(agegroup.EducationType.EducationTypeId.ToString()));

            tbStartClaim.Text = agegroup.StartClaim.HasValue? agegroup.StartClaim.Value.ToShortDateString():"";
            tbEndClaim.Text = agegroup.EndClaim.HasValue?agegroup.EndClaim.Value.ToShortDateString():"";
            tbStartLesson.Text = agegroup.StartLesson.HasValue? agegroup.StartLesson.Value.ToShortDateString():"";
            tbMaxCountPupil.Text = agegroup.MaxCountPupil.ToString();
            cvMaxCountPupil1.ValueToCompare = agegroup.CurrentCountPupil.ToString();
            cvAgeEnd1.Enabled = cvAgeStart1.Enabled = agegroup.CurrentCountPupil != 0;
            cvAgeStart1.ValueToCompare = agegroup.AgeStart.ToString();
            cvAgeEnd1.ValueToCompare = agegroup.AgeEnd.ToString();
            foreach (int year in agegroup.NumYears)
            {
                cblNumYear.Items.FindByValue(year.ToString()).Selected = true;
            }
            foreach (var teacher in agegroup.Teachers)
            {
                //if (cblTeachers.Items.FindByValue(teacher.UserId.ToString()) !=null)
                    cblTeachers.Items.FindByValue(teacher.UserId.ToString()).Selected = true;
            }
            foreach (var address in agegroup.Adresses)
            {
                ddlAddress.SelectedIndex= ddlAddress.Items.IndexOf(ddlAddress.Items.FindByValue(address.AddressId.ToString()));
            }

            
            ddlChildUnionsForAdd.Enabled = cblNumYear.Enabled=ddlEducationType.Enabled=ddlTypeBudgetForAdd.Enabled= agegroup.CurrentCountPupil == 0;
            btnAddAgeGroup.Visible = agegroup.Name != "Автогруппа" && !UserContext.Roles.IsOsip;

            //PopupdataBind(agegroup.Section.SectionId, agegroup.TeacherId, agegroup.TypeBudget.DictTypeBudgetId, agegroup.EducationType.EducationTypeId);
            //tbAgeStart.Text = agegroup.AgeStart.ToString();
            //tbAgeEnd.Text = agegroup.AgeEnd.ToString();
            //tbNumYears.Text = agegroup.NumYears.ToString();
            //tbMaxCountPupil.Text = agegroup.MaxCountPupil.ToString();

            // инициализировать DropDownList и CheckBoxList

        }
        // 
        
        upPopup.Update();
        popupAddExtender.Show();

    }


    protected void btnAddAgeGroup_OnClick(object sender, EventArgs e)
    {
        // сохранение
        int maxCountPupil = 0;
        try
        {
            maxCountPupil = Convert.ToInt32(tbMaxCountPupil.Text);
        }
        catch (Exception)
        {
            maxCountPupil = 0;
        }
        using (AgeGroupDb db = new AgeGroupDb())
        {
            if (agegroupid.HasValue)
            {
                // update
                

                db.UpdateGroup(agegroupid.Value, tbName.Text, Convert.ToInt64(ddlChildUnionsForAdd.SelectedValue),
                            Convert.ToInt32(tbAgeStart.Text), Convert.ToInt32(tbAgeEnd.Text),
                            Convert.ToInt32(ddlTypeBudgetForAdd.Text), Convert.ToInt32(ddlEducationType.SelectedValue),
                            tbComment.Text, tbPlace.Text, Convert.ToDateTime(tbStartLesson.Text), Convert.ToDateTime(tbStartClaim.Text), Convert.ToDateTime(tbEndClaim.Text), maxCountPupil);

                foreach (ListItem item in cblTeachers.Items)
                {
                    if (item.Selected)
                    {
                        db.AddGroupTeacher(agegroupid.Value, new Guid(item.Value));
                    }
                    else
                    {
                        db.DeleteGroupTeacher(agegroupid.Value, new Guid(item.Value));
                    }
                }
                bool isFirstYear = false;
                foreach (ListItem item in cblNumYear.Items)
                {
                    if (item.Selected)
                    {
                        isFirstYear = item.Value == "1" || isFirstYear;
                        db.AddGroupYear(agegroupid.Value, Convert.ToInt32(item.Value));
                    }
                    else
                    {
                        db.DeleteGroupYear(agegroupid.Value, Convert.ToInt32(item.Value));
                    }
                }
                foreach (ListItem item in ddlAddress.Items)
                {
                    if (item.Selected)
                    {
                        db.AddGroupAddress(agegroupid.Value, Convert.ToInt32(item.Value));
                    }
                    else
                    {
                        db.DeleteGroupAddress(agegroupid.Value, Convert.ToInt32(item.Value));
                    }
                }

                int statusId = 2;
                int currentPupil = 0;
                try
                {
                    currentPupil = Convert.ToInt32(cvMaxCountPupil1.ValueToCompare);
                }
                catch (Exception)
                {
                    
                }
                if (currentPupil>=maxCountPupil || !isFirstYear)
                {
                    statusId = 3;
                }
                
                //SiteUtility.SendScheduleESZ(Convert.ToInt64(ddlChildUnionsForAdd.SelectedValue), agegroupid.Value, maxCountPupil, Convert.ToDateTime(tbStartLesson.Text), Convert.ToDateTime(tbStartClaim.Text), Convert.ToDateTime(tbEndClaim.Text),statusId);

            }
            else
            {
                //insert
                //db.AddAgeGroup(UserContext.UdodId.Value, Convert.ToInt32(ddlChildUnionSelect.SelectedValue), Convert.ToInt32(tbAgeStart.Text), Convert.ToInt32(tbAgeEnd.Text), Convert.ToInt32(tbNumYears.Text), Convert.ToInt32(ddlEducationType.SelectedValue), Convert.ToInt32(tbLessonLength.Text), Convert.ToInt32(tbBreakLength.Text), Convert.ToInt32(tbLessonsInWeek.Text), new Guid(ddlTeacher.SelectedValue), Convert.ToInt32(tbYear.Text), Convert.ToInt32(ddlTypeBudget.SelectedValue), Convert.ToInt32(tbMaxCountPupil.Text));

                long groupId=db.AddGroup(tbName.Text, Convert.ToInt64(ddlChildUnionsForAdd.SelectedValue),
                            Convert.ToInt32(tbAgeStart.Text), Convert.ToInt32(tbAgeEnd.Text),
                            Convert.ToInt32(ddlTypeBudgetForAdd.Text), Convert.ToInt32(ddlEducationType.SelectedValue),
                            tbComment.Text, tbPlace.Text, Convert.ToDateTime(tbStartLesson.Text), Convert.ToDateTime(tbStartClaim.Text), Convert.ToDateTime(tbEndClaim.Text),maxCountPupil);
                if (groupId != -1)
                {
                    // добавление адреса
                    db.AddGroupAddress(groupId, Convert.ToInt64(ddlAddress.SelectedValue));
                    // добавление списка педагогов
                    foreach (ListItem item in cblTeachers.Items)
                    {
                        if (item.Selected)
                        {
                            db.AddGroupTeacher(groupId, new Guid(item.Value));
                        }
                        else
                        {
                            db.DeleteGroupTeacher(groupId, new Guid(item.Value));
                        }
                    }
                    bool isFirstYear = false;   
                    foreach (ListItem item in cblNumYear.Items)
                    {
                        if (item.Selected)
                        {
                            isFirstYear = item.Value == "1";
                            db.AddGroupYear(groupId, Convert.ToInt32(item.Value));
                        }
                        else
                        {
                            db.DeleteGroupYear(groupId, Convert.ToInt32(item.Value));
                        }
                    }

                    // добавление в ESZ
                    //if (isFirstYear) SiteUtility.SendScheduleESZ(Convert.ToInt64(ddlChildUnionsForAdd.SelectedValue), groupId, maxCountPupil, Convert.ToDateTime(tbStartLesson.Text), Convert.ToDateTime(tbStartClaim.Text), Convert.ToDateTime(tbEndClaim.Text),2);
                }
                else
                {
                    // Ошибка
                }
            }
            // всегда отправлять ДО
            //SiteUtility.SendBatchESZ(UserContext.CityId, UserContext.UdodId, true, false, Convert.ToInt64(ddlChildUnionsForAdd.SelectedValue));
        }
        
        GroupDatabind();
        popupAddExtender.Hide();
    }

    protected void ddlChildUnionSelect_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        /*using (TeacherDb db = new TeacherDb())
        {
            var list = db.GetTeacher(null, UserContext.UdodId.Value, Convert.ToInt32(ddlChildUnionSelect.SelectedValue));
            ddlTeacher.DataSource = list;
            ddlTeacher.DataBind();
        }*/
    }

    /*protected void AgeGroupDatabind()
    {
        if (UserContext.UdodId.HasValue)
            dsTeachers.SelectParameters["udodId"].DefaultValue = UserContext.UdodId.Value.ToString();
        else
            dsTeachers.SelectParameters["udodId"].DefaultValue = null;

        if (UserContext.CityId.HasValue)
            dsTeachers.SelectParameters["cityId"].DefaultValue = UserContext.CityId.Value.ToString();
        else
            dsTeachers.SelectParameters["cityId"].DefaultValue = null;

        gvTeacherList.DataBind();
        upTeachers.Update();
    }*/

    protected void FilterDatabind(object sender, EventArgs e)
    {
        //AgeGroupDatabind();

        using (ChildUnionDb db = new ChildUnionDb())
        {
            var list = db.GetChildUnionsShort(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null, null, null, null, null, null, null, null, null, null, null, null, null,false).OrderBy(i => i.Name);
            ddlChildUnion.DataSource = list;
            ddlChildUnion.DataBind();
            ddlChildUnionsForAdd.DataSource = list;
            ddlChildUnionsForAdd.DataBind();

        }
        upfilter.Update();
        upPopup.Update();
        GroupDatabind();
    }

    protected void ddlNumItemsOnPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }

    protected void pgrGroupList_OnChange(object sender, EventArgs e)
    {
        GroupDatabind();
    }
    protected void GroupDatabind()
    {
        using (AgeGroupDb db = new AgeGroupDb())
        {
            var list = new List<ExtendedGroup>();

            long? childunionId = null;
            try
            {
                childunionId = Convert.ToInt64(ddlChildUnion.SelectedValue);
            }
            catch (Exception)
            {
            }
            int? typebudget = null;
            try
            {
                typebudget = Convert.ToInt32(ddlBudgetType.SelectedValue);
            }
            catch (Exception)
            {
            }
            int? numYear = null;
            try
            {
                numYear = Convert.ToInt32(ddlNumYear.SelectedValue);
            }
            catch (Exception)
            {

            }
            if (UserContext.Roles.RoleId == (int)EnumRoles.Administrator)
            {
                pgrGroupList.numElements = db.GetGroupsCount(cityUdodFilter.SelectedUdod, tbGroupName.Text, childunionId, null, numYear, typebudget, null, 0, 0);
                list = db.GetGroupsPaging(cityUdodFilter.SelectedUdod, tbGroupName.Text, childunionId, null, numYear, typebudget, OrderBy, pgrGroupList.PagerIndex, pgrGroupList.PagerLength, null);
            }
            else
            {
                pgrGroupList.numElements = db.GetGroupsCount(UserContext.UdodId, tbGroupName.Text, childunionId, null, numYear, typebudget, null, 0, 0);
                list = db.GetGroupsPaging(UserContext.UdodId, tbGroupName.Text, childunionId, null, numYear, typebudget, OrderBy, pgrGroupList.PagerIndex, pgrGroupList.PagerLength, null);
            }
            gvAgeGroupList.DataSource = list;
            gvAgeGroupList.DataBind();
            upAgeGroup.Update();
            upfilter.Update();
        }
        
    }

    protected void ddlBudgetType_OnDataBound(object sender, EventArgs e)
    {
        ddlBudgetType.Items.Insert(0, new ListItem("Все", ""));
    }

    protected void ddlBudgetType_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        GroupDatabind();
    }

    protected void ddlChildUnion_OnDataBound(object sender, EventArgs e)
    {
        if (ddlChildUnion.Items.Count == 0)
        {
            ddlChildUnion.Items.Insert(0, new ListItem("Выберите УДО", ""));
        }
        else
        {
            ddlChildUnion.Items.Insert(0, new ListItem("Все", ""));
        }
    }
    protected string GetTeachers(object group)
    {
        var list = (group as ExtendedGroup).Teachers;

        StringBuilder sb = new StringBuilder();

        foreach (var p in list)
        {
            sb.Append(p.Fio + "<br>");
        }
        if (sb.ToString() != "")
            return sb.ToString();
        else
            return "";
    }

    protected string GetYears(object group)
    {
        var list = (group as ExtendedGroup).NumYears;

        StringBuilder sb = new StringBuilder();

        foreach (var p in list)
        {
            sb.Append(","+p);
        }
        if (sb.ToString() != "")
            return sb.ToString().Substring(1,sb.Length-1);
        else
            return "";
    }

    protected void ddlChildUnion_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        GroupDatabind();
    }

    protected void lnkRefresh_OnClick(object sender, EventArgs e)
    {
        GroupDatabind();
    }

    protected void gvAgeGroupList_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        e.Cancel = true;
        long ageGroupId = Convert.ToInt64(gvAgeGroupList.DataKeys[e.RowIndex].Value);
        
    }


    protected void lnkAddGroup_OnClick(object sender, EventArgs e)
    {
        agegroupid = null;
        litTitle.Text = "<span class='headerCaptions'>Добавление группы</span>";
        btnAddAgeGroup.Visible = true;
        upHeader.Update();
        cvAgeEnd1.Enabled = cvAgeStart1.Enabled = false;
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var list =
                db.GetChildUnionsShort(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null,
                                       null, null, null, null, null, null, null, null, null, null, null, null,false).
                    OrderBy(i => i.Name);
            ddlChildUnionsForAdd.DataSource = list;
            ddlChildUnionsForAdd.DataBind();
        }
        tbName.Text = tbAgeStart.Text = tbAgeEnd.Text = tbComment.Text = tbGroupName.Text = tbPlace.Text= lblAgeStart.Text=lblAgeEnd.Text="";
        tbStartClaim.Text = tbStartLesson.Text = tbEndClaim.Text = tbMaxCountPupil.Text= "";
        ddlChildUnionsForAdd.SelectedIndex = ddlChildUnionsForAdd.Items.IndexOf(ddlChildUnionsForAdd.Items.FindByValue(ddlChildUnion.SelectedValue));
        ddlChildUnionsForAdd.Enabled =
            tbAgeStart.Enabled =
            tbAgeEnd.Enabled = cblNumYear.Enabled = ddlEducationType.Enabled = ddlTypeBudgetForAdd.Enabled = true;
        ddlChildUnionsForAdd_OnSelectedIndexChanged(sender, e);
        upPopup.Update();
        popupAddExtender.Show();
    }

    protected void ddlChildUnionsForAdd_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {
            long childunionId = (Convert.ToInt64(ddlChildUnionsForAdd.SelectedValue));
            var childunion= db.GetChildUnion(childunionId, null);
            cblTeachers.DataSource = db.GetTeacher(childunionId);
            cblTeachers.DataBind();

            ddlAddress.DataSource = childunion.Addresses;
            ddlAddress.DataBind();
            ddlTypeBudgetForAdd.Items.Clear();
            foreach (var budgetType in childunion.BudgetTypes)
            {
                ddlTypeBudgetForAdd.Items.Add(new ListItem(budgetType.TypeBudgetName,budgetType.DictTypeBudgetId.ToString()));
            }
            ddlEducationType.Items.Clear();
            foreach (var educationType in childunion.EducationTypes)
            {
                ddlEducationType.Items.Add(new ListItem(educationType.EducationTypeName, educationType.EducationTypeId.ToString()));
            }
            cblNumYear.Items.Clear();
            for (var i = !childunion.IsActive?2:1; i <= childunion.NumYears; i++)
            {
                cblNumYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            cvAgeStart.ValueToCompare = childunion.AgeStart.ToString();
            cvAgeEnd.ValueToCompare = childunion.AgeEnd.ToString();
            cvMaxCountPupil.ValueToCompare = childunion.MinCountPupil.ToString();
            hfAgeStart.Value = childunion.AgeStart.ToString();
            hfAgeEnd.Value = childunion.AgeEnd.ToString();
            hfMinCountPupilDO.Value = childunion.MinCountPupil.ToString();
            lblAgeStart.Text = "От "+ childunion.AgeStart.ToString();
            lblAgeEnd.Text = "До "+ childunion.AgeEnd.ToString();
            upPopup.Update();
        }
    }

    protected void btnCancel_OnClick(object sender, EventArgs e)
    {
        popupAddExtender.Hide();
        upPopup.Update();
    }

    protected void ddlNumYear_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        GroupDatabind();
    }

    protected void lnkDelete_OnClick(object sender, EventArgs e)
    {
        
        var ageGroupId = Convert.ToInt64((sender as LinkButton).Attributes["UdodAgeGroupId"]);
        using (AgeGroupDb db = new AgeGroupDb())
        {
            ExtendedGroup agegroup = db.GetGroup(ageGroupId);

            if (agegroup.CurrentCountPupil == 0)
            {
                errorPopup.Attributes.Add("UdodAgeGroupId", ageGroupId.ToString());
                errorPopup.ShowError("Предупреждение", "Из базы будет удалена выбранная группа и все пункты расписания, <br/>ссылающиеся на неё. Отменить данную операцию будет невозможно. <br/>Продолжить?", "Да", "Нет", true,true);
                //errorPopup.ErrorOkClick += lnkDeleteOk_OnClick;
                
            }
            else
            {
                errorPopup.ShowError("Ошибка", "В группе находятся обучающиеся. Удалить данную группу невозможно", "", "Ок", false, true);
            }
        }
    }

    protected void lnkDeleteOk_OnClick(object sender, EventArgs e)
    {
        var ageGroupId = Convert.ToInt64(errorPopup.Attributes["UdodAgeGroupId"]);
        using (AgeGroupDb db = new AgeGroupDb())
        {
            ExtendedGroup agegroup = db.GetGroup(ageGroupId);
            db.DeleteGroup(ageGroupId);
            //SiteUtility.SendScheduleESZ(agegroup.ChildUnionId, agegroup.UdodAgeGroupId, agegroup.MaxCountPupil,
            //                            agegroup.StartLesson.HasValue ? agegroup.StartLesson.Value : DateTime.Now,
            //                            agegroup.StartClaim.HasValue ? agegroup.StartClaim.Value : DateTime.Now,
            //                            agegroup.EndClaim.HasValue ? agegroup.EndClaim.Value : DateTime.Now, 3);

            using (ChildUnionDb db1 = new ChildUnionDb())
            {
                //var count = db1.GetCountGroups(agegroup.ChildUnionId, 1);
                //if (count[0]==0)
                {
                    //SiteUtility.SendBatchESZ(UserContext.CityId,UserContext.UdodId,true,false, agegroup.ChildUnionId);
                }

            }
        }
        GroupDatabind();
    }

    protected void gvAgeGroupList_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType==DataControlRowType.DataRow)
        {
            ExtendedGroup item = (ExtendedGroup)(e.Row.DataItem);

            LinkButton btn = (e.Row.FindControl("lnkDelete") as LinkButton);
            if (btn!=null) btn.Attributes.Add("UdodAgeGroupId", item.UdodAgeGroupId.ToString());
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="AddressList.aspx.cs" Inherits="Udod_AddressList" %>
<%@ Register TagPrefix="uct" TagName="Address" Src="~/UserControl/Address/ucNewAddress.ascx" %>

<asp:Content runat="server" ID="content" ContentPlaceHolderID="body">
    <asp:UpdatePanel runat="server" ID="upgridView" UpdateMode="Always">
    <ContentTemplate>
    
    <asp:GridView runat="server" ID="gvUdodAddresses" AutoGenerateColumns="false" DataSourceID="dsAddresses" OnRowDataBound="gvUdodAddresses_OnRowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="№">
                <ItemTemplate>
                    <%# (gvUdodAddresses.Rows.Count+1)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Наименование"  >
                <ItemTemplate>
                    <%# Eval("UdodName")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Код ЕКИС"  >
                <ItemTemplate>
                    <%# Eval("EoId")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Адрес"  >
                <ItemTemplate>
                    <%# Eval("AddressStr")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Адрес"  >
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lnkEdit" OnClick="lnkEdit_OnClick"><div class="btnBlue">Изменить адрес</div></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
    </asp:GridView>

    </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:Panel runat="server" ID="popupSelectAddress" style="padding: 5px; display: none; z-index: 0; border: 2px solid black" BackColor="White">
        <asp:UpdatePanel runat="server" ID="upHeader" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pSelectAddressHeader" runat="server" >
                <asp:Label ID="Label2" Text="<span class='headerCaptions'>Редактирование адреса</span>" runat="server"/>
                <asp:LinkButton ID="LinkButton1" runat="server" 
                    OnClientClick="$find('SelectAddressPopup').hide(); return false;" />
            </asp:Panel>
        </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel runat="server" ID="upAddressPopup" UpdateMode="Conditional">
        <ContentTemplate>
        <div >
            <uct:Address runat="server" ID="AddressPopup" AddressTypeId="2" VisibleFlat="false" IsValidateEdodEdit="true" />
            
            <table>
                <tr>
                    <td class="tdx">
                        График работы:
                    </td>
                    <td>
                        <asp:TextBox ID="tbAddressWorkTime" runat="server" CssClass="input"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Телефон в корпусе:
                    </td>
                    <td>
                        <asp:TextBox ID="tbAddressPhone" runat="server" CssClass="input"></asp:TextBox>
                        <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbAddressPhone" Mask="8(999)999-99-99" MaskType="Number" />
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Email:
                    </td>
                    <td>
                        <asp:TextBox ID="tbAddressEmail" runat="server" CssClass="input"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan=2>
                        <%-- <asp:RequiredFieldValidator runat="server" ID="val4" ValidationGroup="80" ErrorMessage="Поле 'EMail' не заполнено" ControlToValidate="tbAddressEmail" Display="Dynamic"  ></asp:RequiredFieldValidator> --%>
                        <asp:RegularExpressionValidator runat="server" ID="valEmail" ErrorMessage="Неверный формат электронной почты" ValidationGroup="80"
                            ControlToValidate="tbAddressEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Принадлежит ОУ
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="tbAddressUdodName" CssClass="input"></asp:TextBox>
                        <ajaxToolkit:AutoCompleteExtender runat="server" ID="acAddressUdodName" TargetControlID="tbAddressUdodName" ServiceMethod="GetAddressUdodName" EnableCaching="true" CompletionSetCount="50" MinimumPrefixLength="1" UseContextKey="true" BehaviorID="AutoCompleteEx" CompletionListCssClass="addresslist"/>
                    </td>
                </tr>
            </table>
        </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger runat="server" ControlID="btnSaveAddress"/>
        </Triggers>
    </asp:UpdatePanel>
        <div >
            <table>
                <tr>
                    <td>
                        <asp:LinkButton ID="btnSaveAddress" runat="server" ValidationGroup="80" Text="<div class='btnBlue'>Сохранить</div>" CausesValidation="true" OnClick="btnSaveAddress_OnClick"/>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnCancelAddress" runat="server" Text="<div class='btnBlue'>Отменить</div>" CausesValidation="false" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupSelectAddressExtender" PopupDragHandleControlID="pSelectAddressHeader"
        PopupControlID="popupSelectAddress" CancelControlID="btnCancelAddress"
        TargetControlID="btnShowAddressPopup" RepositionMode="None"
        BehaviorID="SelectAddressPopup"/>

    <asp:Button ID="btnShowAddressPopup" runat="server" style="display:none" />

    <asp:ObjectDataSource runat="server" ID="dsAddresses" TypeName="Udod.Dal.KladrDb" SelectMethod="UdodGetUdodAddress"></asp:ObjectDataSource>
</asp:Content>
﻿<%@ Page Language="C#" MasterPageFile="../Master/MasterPage.master" AutoEventWireup="true" CodeFile="UdodList.aspx.cs" Inherits="Udod_UdodList" %>
<%@ Import Namespace="Udod.Dal.Enum" %>
<%@ Register src="../UserControl/Filter/ucCityUdodFilter.ascx" tagName="CityUdodFilter" tagPrefix="uct" %>
<%@ Register src="../UserControl/Menu/ucPager.ascx" tagName="Pager" tagPrefix="uc" %>

<asp:Content runat="server" ID="ContentUdodList" ContentPlaceHolderID="body">
    <asp:UpdatePanel ID="upCityUdodFilter" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <uct:CityUdodFilter ID="cityUdodFilter" runat="server" UdodDdlEnabled="false" AddAllProperty="true" OnCityChange="cityUdodFilter_cityChange"/>
    </ContentTemplate>
    </asp:UpdatePanel>
    <br/>
    <asp:UpdatePanel ID="upUdodFilter" runat="server">
    <ContentTemplate>
    <asp:Panel ID="Panel2" runat="server" DefaultButton="lnkFindByName">
    <table  border=0 width="100%">
        <tr>
            <td  align="left">
                Наименование учреждения
            </td>
            <td align="left">
                <div class="inputShort">
                    <asp:TextBox runat="server" ID="tbUdodName" CssClass="inputShort" />
                </div>
            </td>
            <td>Статус ввода контингента
            </td>
            <td align="left">
                <asp:DropDownList runat="server" ID="ddlStatus" CssClass="input" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_OnSelectedIndexChanged">
                    <Items>
                        <asp:ListItem Value="">Все</asp:ListItem>
                        <asp:ListItem Value="false">Завершили</asp:ListItem>
                        <asp:ListItem Value="true">Продолжают</asp:ListItem>
                    </Items>
                </asp:DropDownList>
            </td>
            <td>Номер:</td>
            <td>
                <asp:TextBox runat="server" ID="tbNumber" CssClass="inputShort"></asp:TextBox>
            </td>
            <td>
                <asp:LinkButton ID="lnkFindByName" runat="server" OnClick="lnkFindByName_OnClick"><div class="btnBlue">Поиск</div></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:LinkButton runat="server" ID="lnkExportProgram" OnClick="lnkExportProgram_OnClick"><div class="btnBlueLong">Отчет по направленностям</div></asp:LinkButton>
            </td>
            <td colspan="2">
                <asp:LinkButton ID="lnkExportContingent" runat="server" OnClick="lnkExportContingent_OnClick"><div class="btnBlueLong">Отчет по вводу контингента</div></asp:LinkButton>
            </td>
        </tr>
    </table>
    </asp:Panel>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger runat="server" ControlID="lnkExportProgram"/>
        <asp:PostBackTrigger runat="server" ControlID="lnkExportContingent"/>
    </Triggers>
</asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="upGridView" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:LinkButton runat="server" ID="AddUdod" onclick="AddUdod_Click"><div class="btnBlueLong">Добавить учреждение</div></asp:LinkButton>
        <div style="font-size: 0px; height: 5px;"></div> 
        <table>
            <tr>
                <td><asp:Label ID="lblStatusUdod" Text="Статус:" runat="server" Visible="false"/></td>
                <td><asp:DropDownList ID="ddlStautsUdod" CssClass="input" runat="server" Visible="false" DataSourceID="dsUdodStatus"  DataTextField="UdodStatusName" DataValueField="UdodStatusId" AutoPostBack="true" OnDataBound="ddlStatusUdod_OnDataBound" OnSelectedIndexChanged="ddlStatusUdod_OnSelectedIndexChanged"/></td>
            </tr>
        </table>
        <asp:GridView runat="server" ID="gvUdodList" CssClass="dgc" AutoGenerateColumns="false" DataKeyNames="UdodId"
            DataSourceID="datasource" onselectedindexchanged="gvUdodList_SelectedIndexChanged" OnRowDataBound="gvUdodList_OnRowDataBound" 
            OnRowDeleted="gvUdodList_UdodDeleted">
            <EmptyDataTemplate>
                <asp:Label ID="lblEmptyGvUdodList" runat="server" style="font-size:16px">Нет учреждений дополнительного образования детей</asp:Label>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="№" >
                    <ItemTemplate><%# gvUdodList.Rows.Count + pgrUdodList.PagerIndex %></ItemTemplate>
                </asp:TemplateField>
                        
                <asp:TemplateField HeaderText="Наименование">
                    <ItemTemplate>
                        <%# Eval("ShortName") %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Есть блок ДО">
                    <ItemTemplate>
                        <%# (bool)Eval("IsDOEnabled") ? "Да" : "Нет" %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Есть в гос-задании доп-образование">
                    <ItemTemplate>
                        <%# (bool)Eval("IsInvalid") ? "Да" : "Нет" %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ФИО Директора">
                    <ItemTemplate>
                        <%# Eval("FioDirector") %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Телефон">
                    <ItemTemplate>
                        <%# Eval("PhoneNumber") %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Email">
                    <ItemTemplate>
                        <%# Eval("Email") %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Адреса" Visible="false">
                    <ItemTemplate>
                        <%# GetAddresses(Container.DataItem)%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Изменить">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="gvEdit" OnClick="gvEdit_OnClick"><img src='../Images/edit_smth.png' border=0 title='Изменить'></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ButtonType="Link" ShowDeleteButton="true" HeaderText="Удаление" DeleteText="<img src='../Images/delete_smth.png' border=0 title='Удалить'>"/>
                <asp:TemplateField HeaderText="Статус" Visible="false">
                    <ItemTemplate>
                        <asp:DropDownList runat="server" ID="ddlUdodStatus" CssClass="input" DataSourceID="dsUdodStatus"  DataTextField="UdodStatusName" DataValueField="UdodStatusId" OnSelectedIndexChanged="ddlUdodStatus_OnChange" AutoPostBack="true" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Округ">
                    <ItemTemplate>
                        <%# Eval("CityName")%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="gridpager" VerticalAlign="Bottom" HorizontalAlign="Left"/>
            <PagerSettings Mode="Numeric" Position="Bottom" PageButtonCount="10" />
        </asp:GridView>
    </ContentTemplate>
    </asp:UpdatePanel>

    <uc:Pager runat="server" ID="pgrUdodList" needStartDots="false" needEndDots="false" pagesCount="10" OnChange="pgrUdodList_OnChange"/>

    <asp:ObjectDataSource ID= "dsUdodStatus" TypeName="Udod.Dal.UdodDb" runat="server" SelectMethod="GetUdodStatuses"/>

    <asp:ObjectDataSource ID= "datasource" TypeName="Udod.Dal.UdodDb" DeleteMethod="DeleteUdod" runat="server" 
        SelectMethod="GetUdodsPaging" OnSelected="ds_Selected" OnSelecting="ds_Selecting">
        <SelectParameters>
            <asp:ControlParameter Name="cityId" ConvertEmptyStringToNull="true" ControlID="cityUdodFilter" PropertyName="SelectedCity"/>
            <asp:Parameter Name="cityCode" ConvertEmptyStringToNull="true"/>
            <asp:Parameter Name="rayonId" ConvertEmptyStringToNull="true"/>
            <asp:Parameter Name="metroId" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="programId" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="sectionId" ConvertEmptyStringToNull="true" />
            <asp:ControlParameter Name="udodName" ControlID="tbUdodName" PropertyName="Text" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="typeBudgetId" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="profileId" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="sectionName" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="profileName" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="programName" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="childUnionName" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="rayonCode" ConvertEmptyStringToNull="true" />
            <asp:ControlParameter Name="isContingent" ControlID="ddlStatus" PropertyName="SelectedValue" ConvertEmptyStringToNull="true" />
            <asp:ControlParameter Name="UdodStatusId" ConvertEmptyStringToNull="true" ControlID="ddlStautsUdod" PropertyName="SelectedValue" />
            <asp:ControlParameter Name="pagerIndex" ControlID="pgrUdodList" ConvertEmptyStringToNull="true" PropertyName="PagerIndex" />
            <asp:ControlParameter Name="pagerLength" ControlID="pgrUdodList" ConvertEmptyStringToNull="true" PropertyName="PagerLength" />
            <asp:Parameter Name="isOsip" ConvertEmptyStringToNull="true" />
            <asp:ControlParameter Name="number" ControlID="tbNumber" PropertyName="Text" ConvertEmptyStringToNull="true" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:Panel runat="server" ID="ReorganisePanel">
        <asp:Label ID="lblReorganisation" runat="server" Text="Реорганизация УДО:" />
        <table>
            <tr>
                <td>
                    <div class="btnBlue"><a href="UdodReorganisation.aspx?type=Merge" runat="server" ID="lbMerge" >Слияние</a></div>
                </td>
                <td>
                    <div class="btnBlue"><a href="UdodReorganisation.aspx?type=Attach" runat="server" ID="lbAttach" >Присоединение</a></div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="font-size: 0px; height: 5px;"></div> 
                </td>
            </tr>
            <tr>
                <td>
                    <div class="btnBlue"><a href="UdodReorganisation.aspx?type=Detach" runat="server" ID="lbDetach" >Выделение</a></div>
                </td>
                <td>
                    <div class="btnBlue"><a href="UdodReorganisation.aspx?type=Divide" runat="server" ID="lbDivide" >Разделение</a></div>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
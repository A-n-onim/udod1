﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Udod_ProgramList : BasePage
{
    private bool isAdd {
        get
        {
            return (bool)ViewState["isAdd"];
        } 
        set
        {
            ViewState["isAdd"]=value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsDepartment || UserContext.Roles.IsOsip);

        if (UserContext.Roles.RoleId == (int)EnumRoles.Osip)
        {
            gvProgramList.Columns[gvProgramList.Columns.Count - 1].Visible = 
                gvProgramList.Columns[gvProgramList.Columns.Count - 2].Visible = 
                lnkAddProgram.Visible = false;
        }
        if (!Page.IsPostBack)
        {
        }
    }

    protected void AddProgram_Click(object sender, EventArgs e)
    {
        isAdd = true;

        litTitle.Text = "<span class='headerCaptions'>Добавление направления</span>";
        tbProgramName.Text = "";
        popupAddExtender.Show();
        upAddProgram.Update();
    }

    protected void SaveProgram_Click(object sender, EventArgs e)
    {
        using (DictProgramDb db = new DictProgramDb())
        {
        
            if (isAdd)
                db.AddProgram(tbProgramName.Text);
            else
            {
                long id = Convert.ToInt64(gvProgramList.SelectedDataKey.Value);
                db.UpdateProgram(tbProgramName.Text, id);
            }

            gvProgramList.DataBind();
            up.Update();
        }
        popupAddExtender.Hide();
    }

    protected void gvProgramList_SelectedIndexChanged(object sender, EventArgs e)
    {
        isAdd = false;

        litTitle.Text = "<span class='headerCaptions'>Редактирование направления</span>";
        long id = Convert.ToInt64(gvProgramList.SelectedDataKey.Value);
        using (DictProgramDb db = new DictProgramDb())
        {
            ExtendedProgram? program = db.GetProgram(id,null);
            if (program.HasValue)
            {
                tbProgramName.Text = program.Value.Name;
            }
        }
        popupAddExtender.Show();
        upAddProgram.Update();
    }

    protected void gv_rowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        var tmp = e.Keys;
    }
}
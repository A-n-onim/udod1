﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class Udod_AddressList : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator);
        acAddressUdodName.ServicePath = SiteUtility.GetUrl("~/Webservices/Dict.asmx");
    }

    protected long? UdodId 
    { 
        get { if (ViewState["UdodId"] == null) return null; return (long)ViewState["UdodId"]; }
        set { ViewState["UdodId"] = value; }
    }

    protected void lnkEdit_OnClick(object sender, EventArgs e)
    {
        GridViewRow row = (GridViewRow)((LinkButton) sender).Parent.Parent;
        var addressId = Int64.Parse(row.Attributes["AddressId"]);
        UdodId = Int64.Parse(row.Attributes["UdodId"]);

        using (UdodDb db = new UdodDb())
        {
            List<int> a = new List<int>();
            ExtendedAddress ea = db.GetUdod(UdodId.Value).Addresses.Where(i => i.AddressId == addressId).FirstOrDefault();
            AddressPopup.IsBti = string.IsNullOrEmpty(ea.KladrCode);
            AddressPopup.Code = string.IsNullOrEmpty(ea.KladrCode) ? ea.BtiCode.Value.ToString() : ea.KladrCode;
            AddressPopup.AddressId = ea.AddressId;
            AddressPopup.NumberHouse = ea.HouseNumber;
            AddressPopup.Fraction = ea.Fraction;
            AddressPopup.Housing = ea.Housing;
            AddressPopup.Building = ea.Building;
            AddressPopup.Flat = ea.FlatNumber;
            AddressPopup.AddressTypeId = ea.AddressType.AddressTypeId;
            AddressPopup.Databind();

            tbAddressWorkTime.Text = ea.AddressWorkTime;
            tbAddressPhone.Text = ea.AddressPhone;
            tbAddressEmail.Text = ea.AddressEmail;
            tbAddressUdodName.Text = ea.AddressUdodName;
        }
        upAddressPopup.Update();
        popupSelectAddressExtender.Show();
    }

    protected void gvUdodAddresses_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType==DataControlRowType.DataRow)
        {
            ExtendedUdodAddress address = (ExtendedUdodAddress) e.Row.DataItem;
            e.Row.Attributes["AddressId"] = address.AddressId.ToString();
            e.Row.Attributes["UdodId"] = address.UdodId.ToString();
        }
    }

    protected void btnSaveAddress_OnClick(object sender, EventArgs e)
    {
        using (KladrDb db = new KladrDb())
        {
            db.UpdateAddress(AddressPopup.Code, AddressPopup.Index, AddressPopup.NumberHouse, AddressPopup.Fraction, AddressPopup.Housing,
                AddressPopup.Building, AddressPopup.Flat, AddressPopup.AddressTypeId, 0, 0, null, UdodId, tbAddressWorkTime.Text,
                tbAddressPhone.Text, tbAddressEmail.Text, AddressPopup.AddressId, AddressPopup.typeAddressCode, tbAddressUdodName.Text);
            
        }
        //gvUdodAddresses.DataBind();
        popupSelectAddressExtender.Hide();
        //upgridView.Update();
    }
}
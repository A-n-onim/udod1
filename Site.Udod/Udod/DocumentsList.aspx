﻿<%@ Page EnableEventValidation="false" Language="C#" MasterPageFile="../Master/MasterPage.master" AutoEventWireup="true" CodeFile="DocumentsList.aspx.cs" Inherits="Udod_DocumentsList" %>

<asp:Content runat="server" ID="ContentNewsList" ContentPlaceHolderID="body">

<asp:Panel runat="server" ID="pnlAddDoc">
    <table>
        <tr>
            <td>
                Название документа
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbDocumentName" />
            </td>
        </tr>
        <tr>
            <td>
                Путь к документу
            </td>
            <td>
                <asp:FileUpload runat="server" ID="fileUpload" />
            </td>
        </tr>
        <tr>
            <td colspan=2>
                <asp:LinkButton runat="server" ID="lnkAddDocument" OnClick="lnkAddDocument_OnClick" Visible="true"><div class="btnBlueLong">Загрузить</div></asp:LinkButton>
            </td>
        </tr>
    </table>
</asp:Panel>

<asp:UpdatePanel ID="upRep" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Repeater runat="server" ID="rptDocuments" OnItemDataBound="rptDocuments_OnItemDataBound" DataSourceID="dsDocuments" >
            <HeaderTemplate>
                <div><table>
                <tr>
                    <td colspan=2>
                        <hr />
                    </td>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="padding-left: 3px;">
                        <a href="<%# SiteUtility.SiteUrl+"uploadedfiles/"+ Eval("DocumentPath") %>"><div style="color:Blue"><%# Eval("DocumentName") %></div></a>
                    </td>
                    <td style="padding-left: 3px;">
                        <asp:LinkButton runat="server" ID="lnkDeleteDocument" OnClick="lnkDeleteDocument_OnClick"><div style="color:Red">Удалить</div></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan=2>
                        <hr />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table></div>
            </FooterTemplate>
        </asp:Repeater>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:ObjectDataSource runat="server" ID="dsDocuments" TypeName="Udod.Dal.DocumentsDb" SelectMethod="GetDocuments"></asp:ObjectDataSource>

</asp:Content>
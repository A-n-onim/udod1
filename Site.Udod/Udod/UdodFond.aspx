﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="UdodFond.aspx.cs" Inherits="Udod_UdodFond" %>
<%@ Register src="../UserControl/Menu/ucPager.ascx" tagName="Pager" tagPrefix="uc" %>
<%@ Register TagPrefix="uct" TagName="SortHeader" Src="~/UserControl/Menu/ucSortHeader.ascx" %>
<%@ Register tagPrefix="uc" namespace="Site.Udod" %>

<asp:Content runat="server" ID="ContentUdodFond" ContentPlaceHolderID="body">


<table>
    <tr>
        <td colspan=2>
            <asp:UpdatePanel runat="server" ID="upFond" UpdateMode="Conditional">
            <ContentTemplate>
            <uc:SortableGridView ID="gvFond" runat="server" AutoGenerateColumns="false" CssClass="dgc"
                            DataKeyNames="FondId" OnSelectedIndexChanged="gvFond_SelectedIndexChanged" OnRowDeleting="gvFond_Deleting" >
                    <EmptyDataTemplate>
                        <asp:Label ID="lblEmptyGvFond" runat="server" style="font-size:16px">Нет детских объединений</asp:Label>
                    </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <%# gvFond.Rows.Count + pgrFondList.PagerIndex %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Название">
                        <HeaderTemplate>
                            <uct:SortHeader ID="shName" runat="server" HeaderCaption="Название" OnSortingChanged="gvFondSortingChanged" SortExpression="Name"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("Name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Направленность">
                        <HeaderTemplate>
                            <uct:SortHeader ID="shProgramName" runat="server" HeaderCaption="Направленность" OnSortingChanged="gvFondSortingChanged" SortExpression="ProgramName"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("Program.Name")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Профиль">
                        <HeaderTemplate>
                            <uct:SortHeader ID="shProfileName" runat="server" HeaderCaption="Профиль" OnSortingChanged="gvFondSortingChanged" SortExpression="ProfileName"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("Profile.Name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Вид деятельности">
                        <HeaderTemplate>
                            <uct:SortHeader ID="shSectionName" runat="server" HeaderCaption="Вид деятельности" OnSortingChanged="gvFondSortingChanged" SortExpression="SectionName"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("Section.Name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ошибки">
                        <ItemTemplate>
                            <%# (bool)Eval("IsActive")?"":"Существуют ошибки" %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowSelectButton="true" SelectText="<img src='../Images/edit_smth.png' border=0 title='Изменить ДО'>"/>
                    <asp:CommandField ShowDeleteButton="true" DeleteText="<img src='../Images/delete_smth.png' border=0 title='Удалить ДО'>"/>
                </Columns>
                <PagerStyle CssClass="gridpager" VerticalAlign="Bottom" HorizontalAlign="Left"/>
                <PagerSettings Mode="Numeric" Position="Bottom" PageButtonCount="10" />
            </uc:SortableGridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger runat="server" ControlID="btnSaveDOPassport"/>
                <asp:AsyncPostBackTrigger runat="server" ControlID="btnCancelPopup" />
                <asp:AsyncPostBackTrigger runat="server" ControlID="filterSection" />
            </Triggers>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
<uc:Pager runat="server" ID="pgrFondList" needStartDots="false" needEndDots="false" pagesCount="10" OnChange="pgrChildUnionList_OnChange"/>

</asp:Content>
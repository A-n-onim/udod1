﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Udod_ClaimList : BasePage
{
    private const string SessionKey = "Udod_ClaimList_";
    private int? DictTypeBudgetId
    {
        get { return (int?)ViewState["DictTypeBudgetId"]; }
        set { ViewState["DictTypeBudgetId"] = value; }
    } 
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator||UserContext.Roles.IsOsip||UserContext.Roles.IsUdodEmployee || UserContext.Roles.IsDepartment);
        //td7.Visible = td6.Visible = td1.Visible = td3.Visible = td4.Visible = false;
        //PageEvents.UdodChanged += ClaimListDataBind;
        //lnkSendClaim.Visible = UserContext.Roles.IsAdministrator;
        if (!Page.IsPostBack)
        {
            lnkAddClaim.Visible = !UserContext.Roles.IsDepartment;
            UserContext.ClaimId = null;
            /*if (Page.Request.Params["Number"] != null) tbNumber.Text = Page.Request.Params["Number"];
            if (Page.Request.Params["LastName"] != null) tbLastName.Text = Page.Request.Params["LastName"];
            if (Page.Request.Params["FirstName"] != null) tbFirstName.Text = Page.Request.Params["FirstName"];
            if (Page.Request.Params["MiddleName"] != null) tbMiddleName.Text = Page.Request.Params["MiddleName"];
            if (Page.Request.Params["BirthdayBegin"] != null) tbBirthdayBegin.Text = Page.Request.Params["BirthdayBegin"];
            if (Page.Request.Params["BirthdayEnd"] != null) tbBirthdayEnd.Text = Page.Request.Params["BirthdayEnd"];
            if (Page.Request.Params["birthdayFindType"] != null) rblBirthday.SelectedIndex = Convert.ToInt32(Page.Request.Params["birthdayFindType"]);*/
            //StoreFilterValues();

            /*gvClaimList.Columns[gvClaimList.Columns.Count - 3].Visible =
            gvClaimList.Columns[gvClaimList.Columns.Count - 5].Visible =
            gvClaimList.Columns[gvClaimList.Columns.Count - 6].Visible =
            gvClaimList.Columns[gvClaimList.Columns.Count - 4].Visible = false;*/
            if (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee)
            {
                td7.Visible= td6.Visible= td1.Visible = td3.Visible = td4.Visible = false; // поиск по дате рождения
                /*gvClaimList.Columns[gvClaimList.Columns.Count - 3].Visible =
                gvClaimList.Columns[gvClaimList.Columns.Count - 5].Visible =
                gvClaimList.Columns[gvClaimList.Columns.Count - 6].Visible =
                gvClaimList.Columns[gvClaimList.Columns.Count - 4].Visible = true;*/

                gvClaimList.Columns[5].Visible = false;
            }

            if (UserContext.Roles.RoleId == (int)EnumRoles.Osip || UserContext.Roles.RoleId == (int)EnumRoles.Department)
            {
                UserContext.UdodId = null;
            }

            if (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee)
            {
                //lnkAddClaim.Visible = false;
                pfilter.Height = 170;
                pFindUdod.Height = 170;
                pfilterDiv.Style.Add("height", "140px");
                pFindUdodDiv.Style.Add("height", "140px");
                trFilters.Style.Add("height", "200px");

                using (ChildUnionDb db = new ChildUnionDb())
                {
                    ddlDO.DataSource = db.GetChildUnionsShort(UserContext.CityId, UserContext.UdodId, null, null, null, null, null, null, null, null, null, null, null, null, null, null,false).OrderBy(i => i.Name);
                }

                using (TeacherDb db = new TeacherDb())
                {
                    ddlTeacher.DataSource = db.GetTeacher(UserContext.CityId, UserContext.UdodId, null);
                }
            }

            ddlDO.DataBind();
            ddlTeacher.DataBind();

            /*using (DictSectionDb db = new DictSectionDb())
            {
                ddlSection.DataSource = db.GetSectionsFind(null, null, UserContext.UdodId);
                ddlSection.DataBind();
            }*/
            //upDO.Update();
            //upTeacher.Update();
            //upSection.Update();

            RestoreFilterValues();

            if (!cbxRegDateEnabled.Checked)
            {
                if (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee)
                {
                    tbDateStart.Text = (Session[SessionKey + "RegDateStart"] == null) ?
                        DateTime.Now.AddDays(-30).ToString() : Session[SessionKey + "RegDateStart"] + "";
                }
                else
                {
                    tbDateStart.Text = (Session[SessionKey + "RegDateStart"] == null) ?
                        DateTime.Now.AddDays(-7).ToString() : Session[SessionKey + "RegDateStart"] + "";
                }
                tbDateEnd.Text = (Session[SessionKey + "RegDateEnd"] == null) ?
                    DateTime.Now.AddDays(1).ToString() : Session[SessionKey + "RegDateEnd"] + "";
                upCalendar.Update();
            }
        }
        StoreFilterValues();
    }

    protected void lnkAddClaim_OnClick(object sender, EventArgs e)
    {
        StoreFilterValues();

        UserContext.ClaimId = null;
        Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
    }

    protected void lnkRefresh_OnClick(object sender, EventArgs e)
    {
        DateTime minDate = new DateTime(1900, 1, 1);

        if (!string.IsNullOrEmpty(tbDateStart.Text) && Convert.ToDateTime(tbDateStart.Text) < minDate) tbDateStart.Text = minDate.ToString();
        if (!string.IsNullOrEmpty(tbDateEnd.Text) && Convert.ToDateTime(tbDateEnd.Text) < minDate) tbDateEnd.Text = minDate.ToString();

        gvClaimList.EmptyDataText = "<div class='EmptyClaimList'><div class='EmptyClaimListText'>Записей удовлетворяющих условиям поиска НЕ НАЙДЕНО</div></div>";
        ClaimListDataBind();
    }

    protected void dsClaimList_OnLoad(object sender, EventArgs e)
    {
        //if (UserContext.UdodId.HasValue)
        //dsClaimList.SelectParameters["udodId"].DefaultValue= UserContext.UdodId.Value.ToString();
        upClaimList.Update();
    }

    protected void dsClaimList_OnUpdating(object sender, ObjectDataSourceMethodEventArgs e)
    {
        
    }

    protected void filterStatus_Changed(object sender, EventArgs e)
    {
        pgrClaimList.Clear();
    }

    protected void ClaimListDataBind()
    {
        //if (UserContext.UdodId.HasValue)
        //dsClaimList.SelectParameters["udodId"].DefaultValue = UserContext.UdodId.Value.ToString();
        pgrClaimList.Clear();
        gvClaimList.DataBind();
        upClaimList.Update();
    }

    protected void gvClaimList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        UserContext.ClaimId = (long)(gvClaimList.SelectedDataKey.Value);
        Session["isNewClaimFromOld"] = false;

        Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
    }

    protected void gvClaimList_OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        /*var lbl = (Label)gvClaimList.Rows[e.RowIndex].FindControl("lbPrevStatus");
        var ddl = (DropDownList)gvClaimList.Rows[e.RowIndex].FindControl("ddlEditClaimStatus");
        dsClaimList.UpdateParameters["statusId"].DefaultValue = ddl.SelectedValue;

        if (lbl.Text != ddl.SelectedItem.Text)
        {
            // перенести в ChangeStatus_OnClick
            EventsUtility.LogClaimStatusUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                String.Format("(Номер заявки: {0})(Статус: {1} -> {2})", gvClaimList.Rows[e.RowIndex].Cells[1].Text, lbl.Text, ddl.SelectedItem.Text));
        }*/
    }

    protected void gvClaimList_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ExtendedClaim claim = (ExtendedClaim)e.Row.DataItem;
            // зачисление
            LinkButton btn = (LinkButton)e.Row.FindControl("lnkChangeStatus2");
            btn.Visible = ((claim.ClaimStatus.ClaimStatusId == 8 || claim.ClaimStatus.ClaimStatusId == 7 || claim.ClaimStatus.ClaimStatusId == 6) && (UserContext.Roles.RoleId != (int)EnumRoles.Osip) && (UserContext.Roles.RoleId != (int)EnumRoles.Department)); ;
            // приглашен на собеседование
            /*btn = (LinkButton)e.Row.FindControl("lnkChangeStatus1");
            btn.Visible = claim.ClaimStatus.ClaimStatusId == 1;*/
            // поданы документы
            btn = (LinkButton)e.Row.FindControl("lnkChangeStatus4");
            btn.Visible = false;//((claim.ClaimStatus.ClaimStatusId == 1) && (UserContext.Roles.RoleId != (int)EnumRoles.Osip));
            // переведен в резерв
            //btn = (LinkButton)e.Row.FindControl("lnkChangeStatus5") ;
           // btn.Visible = false;// ((claim.ClaimStatus.ClaimStatusId == 1 || claim.ClaimStatus.ClaimStatusId == 7) && (UserContext.Roles.RoleId != (int)EnumRoles.Osip));
            // аннулирование
            btn = (LinkButton)e.Row.FindControl("lnkChangeStatus3");
            btn.Visible = ((claim.ClaimStatus.ClaimStatusId == 8 || claim.ClaimStatus.ClaimStatusId == 1 || claim.ClaimStatus.ClaimStatusId == 7 || claim.ClaimStatus.ClaimStatusId == 6) && (UserContext.Roles.RoleId != (int)EnumRoles.Osip) && (UserContext.Roles.RoleId != (int)EnumRoles.Department));
            // создание на основе текущей
            btn = (LinkButton)e.Row.FindControl("lnkNewClaimFromOld");
            btn.Visible = (UserContext.Roles.RoleId == (int)EnumRoles.Osip || UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee || UserContext.Roles.RoleId == (int)EnumRoles.Administrator);


            e.Row.Attributes.Add("ClaimId", claim.ClaimId.ToString());
        }
    }

    protected void ChangeStatus_OnClick(object sender, EventArgs e)
    {
        long id = -1;
        LinkButton btn = (LinkButton)sender;
        if (btn.ID == "lnkzachnextyear" || btn.ID == "lnkzach")
        {
            id = Convert.ToInt64(btn.Attributes["ClaimId"]);
        }
        else id=Convert.ToInt64((((LinkButton)sender).Parent.Parent as GridViewRow).Attributes["ClaimId"]);
        int statusId = 4;
        //int DictTypeBudgetId;
        List<ExtendedTypeBudget> typeBudgets=new List<ExtendedTypeBudget>();
        ExtendedChildUnion childUnion = new ExtendedChildUnion();
        ExtendedClaim claim = new ExtendedClaim();

        using (ClaimDb db = new ClaimDb())
        {
            claim = db.GetClaim(id);
        }

        using (ChildUnionDb db = new ChildUnionDb())
        {
            childUnion = db.GetChildUnion(claim.ChildUnion.ChildUnionId, null);
        }

        
        if (btn.ID == "lnkChangeStatus2")
        {
            /*if (claim.BeginDate.Date > DateTime.Now.Date)
            {
                errorClaimList.ShowError("Ошибка", "Зачисление невозможно так как указанная в заявление желаемая дата поступления ещё не наступила.", "", "OK", false, true);
                return;
            }*/

            using (PupilsDb db2 = new PupilsDb())
            {
                if (!db2.GetIsDismissed(id, null, null))
                {
                    errorClaimList.ShowError("Ошибка", "Данный обучающийся уже зачислен в выбранное ДО", "", "OK", false, true);
                    return;
                }
            }
            /*sing (AgeGroupDb db2 = new AgeGroupDb())
            {
                if (!db2.GetAgeGroup(claim.AgeGroup.UdodAgeGroupId).IsActive)
                {
                    errorClaimList.ShowError("Ошибка", "За время от создания заявления, в выбранном детском объединении произошли изменения. В настоящее время ЗАЧИСЛЕНИЕ в желаемый год обучения с текущим возрастом будущего обучающегося НЕВОЗМОЖНО.", "", "OK", false, true);
                    return;
                }
            }*/
            statusId = 4;
        }
        if (btn.ID == "lnkChangeStatus3") statusId = 5;
        if (btn.ID == "lnkChangeStatus4") statusId = 7;
        if (btn.ID == "lnkChangeStatus5") statusId = 6;
        if (btn.ID == "lnkzachnextyear") statusId = 8;
        int? reasonAnnul = null;
        if (statusId == 5)
        {
            using (CommonDb db = new CommonDb())
            {
                rbAnnul.DataSource = db.GetReasonAnnuls(claim.ClaimId);
                rbAnnul.DataBind();
            }
            rbAnnul.SelectedIndex = -1;
            modalPopupAnnul.Show();
            rbAnnul.Attributes.Add("claimId", claim.ClaimId.ToString());
            rbAnnul.Attributes.Add("statusId", statusId.ToString());
            rbAnnul.Attributes.Add("pupilId", claim.Pupil.UserId.ToString());
            rbAnnul.Attributes.Add("pupilBirthday", claim.Pupil.Birthday.ToString());
            rbAnnul.Attributes.Add("childUnionId", claim.ChildUnion.ChildUnionId.ToString());
            return;
        }
        if (statusId == 4)
        {
            typeBudgets = childUnion.BudgetTypes;
            if (false)
            {
                DictTypeBudgetId = typeBudgets[0].DictTypeBudgetId;

                if (DictTypeBudgetId == 2)
                {
                    // проверка на 20 часов и на 3 удод
                    ExtendedLimit limit;
                    using (PupilsDb db = new PupilsDb())
                    {
                        limit = db.GetLimit(claim.Pupil.UserId, null);
                    }

                    DateTime now = DateTime.Today;
                    int fullYears = now.Year - claim.Pupil.Birthday.Year;
                    if (claim.Pupil.Birthday > now.AddYears(-fullYears)) fullYears--;

                    using (AgeGroupDb db = new AgeGroupDb())
                    {
                        /*var list = db.GetAgeGroups(null, null, null, childUnion.ChildUnionId);
                        int minYear = list.Select(i => i.Age).Min();
                        int maxYear = list.Select(i => i.Age).Max();
                        if (fullYears < minYear) fullYears = minYear;
                        if (fullYears > maxYear) fullYears = maxYear;
                        ExtendedAgeGroup item = list.Where(i => i.Age == fullYears).FirstOrDefault();*/

                        ExtendedAgeGroup item = db.GetAgeGroup(claim.AgeGroup.UdodAgeGroupId);

                        if ((limit.Hours + ((double)(item.LessonLength * item.LessonsInWeek) / 60)) > limit.NormHours)
                        {
                            bool isPupil = claim.Pupil.UserId == claim.Parent.UserId;
                            StringBuilder limitText = new StringBuilder();
                            limitText.Append(isPupil ? claim.Parent.LastName : claim.Pupil.LastName);
                            limitText.Append(" " + (isPupil ? claim.Parent.FirstName : claim.Pupil.FirstName));
                            limitText.Append(" " + (isPupil ? claim.Parent.MiddleName : claim.Pupil.MiddleName));
                            limitText.Append(" уже посещает ");
                            using (PupilsDb db2 = new PupilsDb())
                            {
                                List<ExtendedChildUnion> l = db2.GetChildUnion(claim.Pupil.UserId);
                                var previousUdodName = l.Count() > 0 ? l[0].UdodShortName : "";
                                var previousUdodPhone = l.Count() > 0 ? l[0].UdodPhone : "";
                                foreach (ExtendedChildUnion cu in l)
                                {
                                    if (previousUdodName != cu.UdodShortName)
                                    {
                                        limitText.Append(" в УДО " + previousUdodName + " (тел: " + previousUdodPhone + "); ");
                                    }
                                    limitText.Append(cu.Name + ", ");
                                    previousUdodName = cu.UdodShortName;
                                    previousUdodPhone = cu.UdodPhone;
                                }
                                limitText.Remove(limitText.Length - 2, 2);
                                limitText.Append(" в УДО " + previousUdodName + " (тел: " + previousUdodPhone + ") ");
                            }
                            limitText.Append(" с суммарным количеством часов занятий ");
                            limitText.Append(limit.Hours.ToString("F2") + " часов в неделю. ");
                            limitText.Append("Для детей его/её возраста действует ограничение СанПиН в ");
                            limitText.Append(limit.NormHours.ToString("F2") + " часов занятий в неделю. ");
                            limitText.Append("В связи с чем осуществить зачисление в ДО ");
                            limitText.Append(childUnion.Name + " с продолжительностью занятий ");
                            limitText.Append((item.LessonLength * item.LessonsInWeek / 60.0).ToString("F2") + " часов в неделю невозможно, ");
                            limitText.Append("так как это приводит к превышению установленных норм.");

                            lblLimit.Text = limitText.ToString();
                            upLimit.Update();
                            popupLimitExtender.Show();
                            return;
                        }
                    }
                }
                
                //UpdateStatus(claim.ClaimId, statusId, reasonAnnul);
            }
            else
            {
                using (AgeGroupDb db = new AgeGroupDb())
                {
                    var list = db.GetGroupsPaging(null, null, claim.ChildUnion.ChildUnionId, null, null, null, "", 1,
                                                  500000, true);
                    ddlGroups.DataSource = list;
                    ddlGroups.DataBind();
                    ddlGroups_OnSelectedIndexChanged(ddlGroups, new EventArgs());
                }
                // показать 
                popupTypeBudgetExtender.Show();
                ddlGroups.Attributes.Add("claimId", claim.ClaimId.ToString());
                ddlGroups.Attributes.Add("statusId", statusId.ToString());
                ddlGroups.Attributes.Add("pupilId", claim.Pupil.UserId.ToString());
                ddlGroups.Attributes.Add("pupilBirthday", claim.Pupil.Birthday.ToString());
                ddlGroups.Attributes.Add("childUnionId", claim.ChildUnion.ChildUnionId.ToString());
                ddlGroups.Attributes.Add("NumYear", claim.NumYear.ToString());
            }
        }
        else
        {
            UpdateStatus(claim.ClaimId, statusId, reasonAnnul, null, null,null,null);
        }

        gvClaimList.DataBind();
        upClaimList.Update();
    }

    private void UpdateStatus(long claimId, int statusId, int? reasonAnnulId, long? udodGroupId, long? ageGroupId, DateTime? startClaim, DateTime? endClaim)
    {
        using (ClaimDb db = new ClaimDb())
        {
            ExtendedClaim claim = db.GetClaim(claimId);

            db.UpdateClaimStatus(claimId, statusId, DictTypeBudgetId, reasonAnnulId, udodGroupId);
            string comment = "";
            if (reasonAnnulId.HasValue)
            {
                comment = rbAnnul.SelectedItem.Text;
            }
            
            EventsUtility.LogClaimStatusUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                                               String.Format("(Номер заявления: {0})(Статус: {1} -> {2})", claim.Number,
                                                             claim.ClaimStatus.ClaimStatusId, statusId),
                                               claim.Pupil.UserId, claim.ClaimId, claim.Udod.UdodId,
                                               claim.ChildUnion.ChildUnionId);
            //ExportInAsGuf tmp = new ExportInAsGuf();
            //tmp.UpdateStatus(statusId, claimId, comment);
            //if (ageGroupId.HasValue && startClaim.HasValue && endClaim.HasValue)
            //{
            //    tmp.UpdateSchedule(claim.ChildUnion.ChildUnionId, ageGroupId.Value, startClaim.Value, endClaim.Value);
            //}

                  
            //#region Отправка в Реестр и ДО и обучающегося
            //// Получаем обучающегося
            //if (statusId==4)
            //{

            //    ExtendedPupil pupil = null;
            //    using(PupilsDb db1 = new PupilsDb())
            //    {
            //        pupil = db1.GetPupil(claim.Pupil.UserId);



            //        if (pupil != null)
            //        {
            //            using (KladrDb db2 = new KladrDb())
            //            {
            //                var addresses = db2.GetAddressesById(pupil.UserId, null, null, null);
            //                pupil.Addresses = addresses;
            //            }
                        
            //            // добавление или изменение
            //            if (!pupil.ReestrGuid.HasValue || pupil.ReestrCode=="0")
            //            {
            //                string mappath = Page.MapPath(pupil.Passport.PassportType.PassportTypeId == 2
            //                     ? "../Templates/search_Passport_by_Series_and_Number_rq.xml"
            //                     : "../Templates/search_by_Series_and_Number_rq.xml");

                            
            //                try
            //                {
            //                    var pupilstmp = SiteUtility.GetPupilData(mappath,
            //                                         pupil.Passport.Series ,
            //                                         pupil.Passport.Number );
            //                    if (pupilstmp.Count == 1) pupil = pupilstmp.FirstOrDefault();
            //                    else
            //                    {
            //                        if (pupilstmp.Count(i => i.LastName.Trim().ToUpper() == pupil.LastName.Trim().ToUpper() && i.FirstName.Trim().ToUpper() == pupil.FirstName.Trim().ToUpper()) != 0)
            //                        {
            //                            var pupil1 =
            //                                pupilstmp.FirstOrDefault(
            //                                   i => i.LastName.Trim().ToUpper() == pupil.LastName.Trim().ToUpper() && i.FirstName.Trim().ToUpper() == pupil.FirstName.Trim().ToUpper());
            //                            pupil.ReestrCode = pupil1.ReestrCode;
            //                            pupil.ReestrGuid = pupil1.ReestrGuid;
            //                            int? schoolCode = null;
            //                            try
            //                            {
            //                                schoolCode = Convert.ToInt32(pupil1.SchoolCode);
            //                            }
            //                            catch (Exception)
            //                            {
            //                                schoolCode = null;
            //                            }
            //                            db1.UpdatePupilReestrCode(pupil.UserId, null, Convert.ToInt64(pupil.ReestrCode), pupil.ReestrGuid, "", schoolCode, pupil.SchoolName,pupil.ClassName);
            //                        }
            //                    }
            //                }
            //                catch (Exception)
            //                {
            //                }
                            

            //                Guid? reestrGuid = null;
            //                string pupilReestrUdodCode = "";
            //                if (string.IsNullOrEmpty(pupil.ReestrCode) || pupil.ReestrCode == "0")
            //                pupilReestrUdodCode= SiteUtility.ReestrAddPupil(pupil, !string.IsNullOrEmpty(pupil.ReestrUdodCode), ref reestrGuid);
            //                //errorClaimList.ShowError("Ошибка", pupilReestrUdodCode, "", "OK", false, true);
                            
            //                if (reestrGuid.HasValue)
            //                {
            //                    pupil.ReestrGuid = reestrGuid;
            //                }
            //            }

                            
            //            using (ChildUnionDb db2 = new ChildUnionDb())
            //            {
            //                try
            //                {
            //                    var childunion = db2.GetChildUnion(claim.ChildUnion.ChildUnionId, null);
            //                    long result = -1;
            //                    if (string.IsNullOrEmpty(childunion.ReestrCode))
            //                    {
            //                        var teachers = db2.GetTeacher(childunion.ChildUnionId);

            //                        result = SiteUtility.ReestrAddDO(childunion, false, teachers);
            //                    }
            //                    else
            //                    {
            //                        result = Convert.ToInt64(childunion.ReestrCode);
            //                    }
            //                    //errorClaimList.ShowError("Проверка", result.ToString(), "ок", "cancel", true,false);
            //                    if (result != -1)
            //                    {
            //                        childunion.ReestrCode = result.ToString();
            //                        //отправляем зачисление
            //                        //SiteUtility.ReestrAddZach(extendedPupil, childunion, extendedChildUnion, false);
            //                        var childunionZach =
            //                            db1.GetChildUnion(pupil.UserId).Where(
            //                                i => i.ChildUnionId == childunion.ChildUnionId).FirstOrDefault();
            //                        if (childunionZach!=null)
            //                        SiteUtility.ReestrAddZach(pupil, childunion, childunionZach, false);
            //                    }
            //                }
            //                catch (Exception e1)
            //                {
                                
            //                }
            //            }
            //        }

            //    }

            //}

            //#endregion
            
        }
    }

    protected void ddlStatus_OnDataBound(object sender, EventArgs e)
    {
        
    }

    protected void filterUdod_OnCityChange(object sender, EventArgs e)
    {
        if (UserContext.Roles.RoleId == (int)EnumRoles.Osip)
        {
            Session[SessionKey + "SelectedCity"] = filterUdod.SelectedCity;
        }

        ClaimListDataBind();
    }

    protected void filterUdod_OnUdodChange(object sender, EventArgs e)
    {
        if (UserContext.UdodId == null)
        {
            ddlDO.DataSource = new ListItemCollection();
            ddlTeacher.DataSource = new ListItemCollection();
        }
        else
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                ddlDO.DataSource = db.GetChildUnionsShort(UserContext.CityId, UserContext.UdodId, null, null, null, null, null, null, null, null, null, null, null, null, null, null,false).OrderBy(i => i.Name);
            }

            using (TeacherDb db = new TeacherDb())
            {
                ddlTeacher.DataSource = db.GetTeacher(UserContext.CityId, UserContext.UdodId, null);
            }
        }

        ddlDO.DataBind();
        upDO.Update();
        ddlTeacher.DataBind();
        upTeacher.Update();

        if (UserContext.Roles.RoleId == (int)EnumRoles.Osip)
        {
            Session[SessionKey + "SelectedCity"] = filterUdod.SelectedCity;
            Session[SessionKey + "SelectedUdod"] = filterUdod.SelectedUdod;
        }

        ClaimListDataBind();
    }    

    protected void ddlDO_OnDataBound(object sender, EventArgs e)
    {
        if (ddlDO.Items.Count == 0)
        {
            ddlDO.Items.Insert(0, new ListItem("Выберите УДО", ""));
        }
        else
        {
            ddlDO.Items.Insert(0, new ListItem("Все", ""));
        }
    }

    protected void ddlTeacher_OnDataBound(object sender, EventArgs e)
    {
        if (ddlTeacher.Items.Count == 0)
        {
            ddlTeacher.Items.Insert(0, new ListItem("Выберите УДО", ""));
        }
        else
        {
            ddlTeacher.Items.Insert(0, new ListItem("Все", ""));
        }
    }

    protected void ddlDO_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ClaimListDataBind();
    }
    protected void ddlTeacher_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ClaimListDataBind();
    }

    protected void ddlSection_OnDataBound(object sender, EventArgs e)
    {
        //ddlSection.Items.Insert(0, new ListItem("Все", ""));
    }

    protected void ddlSection_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ClaimListDataBind();
    }

    protected void lnkNewClaimFromOld_OnClick(object sender, EventArgs e)
    {
        UserContext.ClaimId = Convert.ToInt64((((LinkButton)sender).Parent.Parent as GridViewRow).Attributes["ClaimId"]);
        Session["isNewClaimFromOld"] = true;
        Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
    }

    protected void ds_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
        {
            //List<ExtendedClaim> list = e.ReturnValue as List<ExtendedClaim>;
            //lblNumItemsInGv.Text = "Всего " + list.Count.ToString() + " элемент(ов)";
        }
    }

    protected void ds_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        using (ClaimDb db = new ClaimDb())
        {
            long? childUnionId;
            try
            {
                childUnionId = Convert.ToInt64(ddlDO.SelectedValue);
            }
            catch
            {
                childUnionId = null;
            }
            DateTime? dateStart;
            try
            {
                dateStart = Convert.ToDateTime(tbDateStart.Text);
            }
            catch
            {
                dateStart = null;
            }
            DateTime? dateEnd;
            try
            {
                dateEnd = Convert.ToDateTime(tbDateEnd.Text);
            }
            catch
            {
                dateEnd = null;
            }
            DateTime? birthdayBegin;
            try
            {
                birthdayBegin = Convert.ToDateTime(tbBirthdayBegin.Text);
            }
            catch
            {
                birthdayBegin = null;
            }
            DateTime? birthdayEnd;
            try
            {
                birthdayEnd = Convert.ToDateTime(tbBirthdayEnd.Text);
            }
            catch
            {
                birthdayEnd = null;
            }
            Guid? teacherId;
            try
            {
                teacherId = new Guid(ddlTeacher.SelectedValue);
            }
            catch
            {
                teacherId = null;
            }

            pgrClaimList.numElements = db.GetClaimsCount(null, filterUdod.SelectedUdod, filterUdod.SelectedCity,
                childUnionId, null, filterStatus.DictClaimStatusId, tbNumber.Text, tbLastName.Text, tbFirstName.Text,
                tbMiddleName.Text, "", "", dateStart, dateEnd, null, birthdayBegin, birthdayEnd,
                Convert.ToInt32(rblBirthday.SelectedValue), null, null, teacherId);
        }
    }

    protected void lnkOpen_OnClick(object sender, EventArgs e)
    {
        UserContext.ClaimId = Convert.ToInt64((((LinkButton)sender).Parent.Parent as GridViewRow).Attributes["ClaimId"]);
        Session["isNewClaimFromOld"] = false;
        Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
    }

    protected void rblBirthday_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        td6.Visible = td7.Visible = rblBirthday.SelectedIndex == 0;
        td3.InnerText = rblBirthday.SelectedIndex == 0 ? "Дата рождения от:" : "Дата рождения:";
        upCalendar.Update();
    }

    protected void cbxRegDateEnabled_OnCheckedChanged(object sender, EventArgs e)
    {
        tbDateStart.Enabled = tbDateEnd.Enabled = !cbxRegDateEnabled.Checked;
        if (cbxRegDateEnabled.Checked)
        {
            tbDateStart.Text = "";
            tbDateEnd.Text = "";
        }
        else
        {
            if (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee)
            {
                tbDateStart.Text = (Session[SessionKey + "RegDateStart"] == null) ?
                    DateTime.Now.AddDays(-30).ToString() : Session[SessionKey + "RegDateStart"] + "";
            }
            else
            {
                tbDateStart.Text = (Session[SessionKey + "RegDateStart"] == null) ?
                    DateTime.Now.AddDays(-7).ToString() : Session[SessionKey + "RegDateStart"] + "";
            }
            tbDateEnd.Text = (Session[SessionKey + "RegDateEnd"] == null) ?
                DateTime.Now.AddDays(1).ToString() : Session[SessionKey + "RegDateEnd"] + "";
        }
    }

    protected void lnkClear_OnClick(object sender, EventArgs e)
    {
        ddlDO.SelectedIndex = ddlTeacher.SelectedIndex = 0;
        upTeacher.Update();
        upDO.Update();
        //upSection.Update();

        if (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee)
        {
            filterStatus.DictClaimStatusId = -1;
            tbDateStart.Text = DateTime.Now.AddDays(-30).ToString();
        }
        else
        {
            filterUdod.Clear();
            upCityUdodFilter.Update();
            filterStatus.Clear();
            tbDateStart.Text = DateTime.Now.AddDays(-7).ToString();
        }
        tbDateEnd.Text = DateTime.Now.AddDays(1).ToString();

        //upCalendar.Update();
        tbNumber.Text = tbLastName.Text = tbFirstName.Text = tbMiddleName.Text = "";
    }

    #region Store/restore filter values

    private void StoreFilterValues()
    {
        Session[SessionKey + "Number"] = String.IsNullOrEmpty(tbNumber.Text) ? null : tbNumber.Text;
        
        Session[SessionKey + "LastName"] = String.IsNullOrEmpty(tbLastName.Text) ? null : tbLastName.Text;
        Session[SessionKey + "FirstName"] = String.IsNullOrEmpty(tbFirstName.Text) ? null : tbFirstName.Text;
        Session[SessionKey + "MiddleName"] = String.IsNullOrEmpty(tbMiddleName.Text) ? null : tbMiddleName.Text;

        DateTime minDate = new DateTime(1900, 1, 1);
        if (UserContext.Roles.RoleId == (int)EnumRoles.Osip)
        {
            Session[SessionKey + "birthdayFindType"] = rblBirthday.SelectedIndex;
            Session[SessionKey + "BirthdayBegin"] = String.IsNullOrEmpty(tbBirthdayBegin.Text) ? null : Convert.ToDateTime(tbBirthdayBegin.Text) < minDate ? minDate.ToString() : tbBirthdayBegin.Text;
            Session[SessionKey + "BirthdayEnd"] = String.IsNullOrEmpty(tbBirthdayEnd.Text) ? null : Convert.ToDateTime(tbBirthdayEnd.Text) < minDate ? minDate.ToString() : tbBirthdayEnd.Text;
        }

        Session[SessionKey + "RegDateStart"] = String.IsNullOrEmpty(tbDateStart.Text) ? null : Convert.ToDateTime(tbDateStart.Text) < minDate ? minDate.ToString() : tbDateStart.Text;
        Session[SessionKey + "RegDateEnd"] = String.IsNullOrEmpty(tbDateEnd.Text) ? null : Convert.ToDateTime(tbDateEnd.Text) < minDate ? minDate.ToString() : tbDateEnd.Text;
        Session[SessionKey + "RegDateCheckbox"] = cbxRegDateEnabled.Checked.ToString();

        if (UserContext.Roles.RoleId != (int)EnumRoles.UdodEmployee)
        {
            Session[SessionKey + "SelectedCity"] = filterUdod.SelectedCity;
            Session[SessionKey + "SelectedUdod"] = filterUdod.SelectedUdod;
        }

        Session[SessionKey + "SelectedDO"] = ddlDO.SelectedValue;
        Session[SessionKey + "SelectedTeacher"] = ddlTeacher.SelectedValue;
        //Session[SessionKey + "SelectedSection"] = ddlSection.SelectedValue;

        Session[SessionKey + "ClaimStatus"] = filterStatus.DictClaimStatusId;
    }

    private void RestoreFilterValues()
    {
        tbNumber.Text = Session[SessionKey + "Number"] + "";

        tbLastName.Text = Session[SessionKey + "LastName"] + "";
        tbFirstName.Text = Session[SessionKey + "FirstName"] + "";
        tbMiddleName.Text = Session[SessionKey + "MiddleName"] + "";

        if (UserContext.Roles.RoleId == (int)EnumRoles.Osip)
        {
            rblBirthday.SelectedIndex = (Session[SessionKey + "birthdayFindType"] + "") == "1" ? 1 : 0;
            tbBirthdayBegin.Text = Session[SessionKey + "BirthdayBegin"] + "";
            tbBirthdayEnd.Text = Session[SessionKey + "BirthdayEnd"] + "";
        }

        tbDateStart.Text = Session[SessionKey + "RegDateStart"] + "";
        tbDateEnd.Text = Session[SessionKey + "RegDateEnd"] + "";

        if (Session[SessionKey + "RegDateCheckbox"] != null)
            cbxRegDateEnabled.Checked = Convert.ToBoolean(Session[SessionKey + "RegDateCheckbox"]);
        tbDateStart.Enabled = tbDateEnd.Enabled = !cbxRegDateEnabled.Checked;

        if (UserContext.Roles.RoleId != (int)EnumRoles.UdodEmployee)
        {
            UserContext.CityId = (int?)Session[SessionKey + "SelectedCity"];
            UserContext.UdodId = (long?)Session[SessionKey + "SelectedUdod"];
        }

        var selectedItem = ddlDO.Items.FindByValue(Session[SessionKey + "SelectedDO"] + "");
        if (selectedItem != null)
            ddlDO.SelectedValue = selectedItem.Value;

        selectedItem = ddlTeacher.Items.FindByValue(Session[SessionKey + "SelectedTeacher"] + "");
        if (selectedItem != null)
            ddlTeacher.SelectedValue = selectedItem.Value;

        /*selectedItem = ddlSection.Items.FindByValue(Session[SessionKey + "SelectedSection"] + "");
        if (selectedItem != null)
            ddlSection.SelectedValue = selectedItem.Value;
        */
        filterStatus.DataBind();
        if (Session[SessionKey + "ClaimStatus"] != null)
            filterStatus.DictClaimStatusId = (int?)Session[SessionKey + "ClaimStatus"];
    }

    #endregion

    protected void lnkTypeBudgetOk_OnClick(object sender, EventArgs e)
    {
        DictTypeBudgetId = Convert.ToInt32(ddlGroups.SelectedValue);
        int statusId = Convert.ToInt32(ddlGroups.Attributes["statusId"]);
        long claimId = Convert.ToInt64(ddlGroups.Attributes["claimId"]);
        Guid PupilId = new Guid(ddlGroups.Attributes["pupilId"]);
        int numYear = Convert.ToInt32(ddlGroups.Attributes["NumYear"]);
        DateTime pupilBirthday = Convert.ToDateTime(ddlGroups.Attributes["pupilBirthday"]);
        long childUnionId = Convert.ToInt64(ddlGroups.Attributes["childUnionId"]);
        using (AgeGroupDb db = new AgeGroupDb())
        {
            //if (DictTypeBudgetId == 2)
            //{
                // проверка на 20 часов и на 3 удод
                /*ExtendedLimit limit;
                using (PupilsDb db = new PupilsDb())
                {
                    limit = db.GetLimit(PupilId, null);
                }
                */
                DateTime now = DateTime.Today;
                int fullYears = now.Year - pupilBirthday.Year;
                if (pupilBirthday > now.AddYears(-fullYears)) fullYears--;


                /*ExtendedClaim claim = new ExtendedClaim();
                using (ClaimDb db2 = new ClaimDb())
                {
                    claim = db2.GetClaim(claimId);
                }
                ExtendedAgeGroup item = db.GetAgeGroup(claim.AgeGroup.UdodAgeGroupId);

                if ((limit.Hours + ((double)(item.LessonLength * item.LessonsInWeek) / 60)) > limit.NormHours)
                {
                    //bool isPupil = claim.Pupil.UserId == claim.Parent.UserId;
                    StringBuilder limitText = new StringBuilder();
                    //limitText.Append(isPupil ? claim.Parent.LastName : claim.Pupil.LastName);
                    //limitText.Append(" " + (isPupil ? claim.Parent.FirstName : claim.Pupil.FirstName));
                    //limitText.Append(" " + (isPupil ? claim.Parent.MiddleName : claim.Pupil.MiddleName));
                    limitText.Append("Данный обучающийся уже посещает ");
                    using (PupilsDb db2 = new PupilsDb())
                    {
                        List<ExtendedChildUnion> l = db2.GetChildUnion(PupilId);
                        var previousUdodName = l.Count() > 0 ? l[0].UdodShortName : "";
                        var previousUdodPhone = l.Count() > 0 ? l[0].UdodPhone : "";
                        foreach (ExtendedChildUnion cu in l)
                        {
                            if (previousUdodName != cu.UdodShortName)
                            {
                                limitText.Append(" в УДО " + previousUdodName + " (тел: " + previousUdodPhone + "); ");
                            }
                            limitText.Append(cu.Name + ", ");
                            previousUdodName = cu.UdodShortName;
                            previousUdodPhone = cu.UdodPhone;
                        }
                        limitText.Remove(limitText.Length - 2, 2);
                        limitText.Append(" в УДО " + previousUdodName + " (тел: " + previousUdodPhone + ") ");
                    }
                    limitText.Append(" с суммарным количеством часов занятий ");
                    limitText.Append(limit.Hours.ToString("F2") + " часов в неделю. ");
                    limitText.Append("Для детей его/её возраста действует ограничение СанПиН в ");
                    limitText.Append(limit.NormHours.ToString("F2") + " часов занятий в неделю. ");
                    limitText.Append("В связи с чем осуществить зачисление в ДО ");
                    limitText.Append(item.ChildUnionName + " с продолжительностью занятий ");
                    limitText.Append((item.LessonLength * item.LessonsInWeek / 60.0).ToString("F2") + " часов в неделю невозможно, ");
                    limitText.Append("так как это приводит к превышению установленных норм.");

                    lblLimit.Text = limitText.ToString();
                    upLimit.Update();
                    popupTypeBudgetExtender.Hide();
                    popupLimitExtender.Show();
                    return;
                }*/
                var group = db.GetGroup(Convert.ToInt64(ddlGroups.SelectedValue));
                if (!(fullYears >= group.AgeStart && fullYears <= group.AgeEnd))
                {
                    errorClaimList.ShowError("Ошибка", "Возраст ребенка не подходит к данной группе", "", "ОК", false,
                                             true);
                    return;
                }

                if (group.NumYears.Count(i => i == numYear) == 0)
                {
                    errorClaimList.ShowError("Ошибка", "Год обучения ребенка не подходит к данной группе", "", "ОК",
                                             false, true);
                    return;
                }
                bool isEnd = group.MaxCountPupil - (group.CurrentCountPupil + 1) <= 0;


            //}
            int? reasonAnnul = null;
            if (statusId == 5) reasonAnnul = Convert.ToInt32(rbAnnul.SelectedValue);
            UpdateStatus(claimId, statusId, reasonAnnul, Convert.ToInt64(ddlGroups.SelectedValue), isEnd ? Convert.ToInt64(ddlGroups.SelectedValue) : (long?)null, group.StartClaim, group.EndClaim);
            popupTypeBudgetExtender.Hide();
        }
        gvClaimList.DataBind();
        upClaimList.Update();
    }

    protected void lnkSendClaim_OnClick(object sender, EventArgs e)
    {
        //using (ClaimDb db = new ClaimDb())
        //{
        //    var claim1 =
        //        db.GetClaims(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
        //                     null, null, null, null, null, null).FirstOrDefault();

        //    claim1 = db.GetClaim(claim1.ClaimId);

        //    SiteUtility.SendClaimInESZ(new ExtendedPupil()
        //    {
        //        LastName = claim1.Pupil.LastName,
        //        FirstName = claim1.Pupil.FirstName,
        //        MiddleName = claim1.Pupil.MiddleName,
        //        Birthday = claim1.Pupil.Birthday,
        //        Sex = claim1.Pupil.Sex,
        //        Passport = new ExtendedPassport()
        //        {
        //            Series = claim1.Pupil.Passport.Series,
        //            Number = claim1.Pupil.Passport.Number,
        //            IssueDate = claim1.Pupil.Passport.IssueDate,
        //            PassportType = new ExtendedPassportType()
        //            {
        //                PassportTypeId = claim1.Pupil.Passport.PassportType.PassportTypeId
        //            }
        //        }
        //    }, new ExtendedPupil()
        //    {
        //        LastName = claim1.Parent.LastName,
        //        FirstName = claim1.Parent.FirstName,
        //        MiddleName = claim1.Parent.MiddleName,
        //        UserId = claim1.Parent.UserId
        //    }, claim1.ClaimId, claim1.ChildUnion.ChildUnionId, "");
        //}
    }

    protected void lnkAnnulConfirm_OnClick(object sender, EventArgs e)
    {
        int statusId = Convert.ToInt32(rbAnnul.Attributes["statusId"]);
        long claimId = Convert.ToInt64(rbAnnul.Attributes["claimId"]);
        Guid PupilId = new Guid(rbAnnul.Attributes["pupilId"]);
        DateTime pupilBirthday = Convert.ToDateTime(rbAnnul.Attributes["pupilBirthday"]);
        long childUnionId = Convert.ToInt64(rbAnnul.Attributes["childUnionId"]);

        UpdateStatus(claimId, statusId, Convert.ToInt32(rbAnnul.SelectedValue),null, null,null,null);
        modalPopupAnnul.Hide();
        gvClaimList.DataBind();
        upClaimList.Update();
    }

    protected void ddlGroups_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlGroups.SelectedValue))
        {
            using (AgeGroupDb db = new AgeGroupDb())
            {
                var group = db.GetGroup(Convert.ToInt64(ddlGroups.SelectedValue));
                StringBuilder sb = new StringBuilder();
                foreach (var teacher in group.Teachers)
                {
                    sb.Append(teacher.Fio);
                    sb.Append(" ");
                }
                string teachers = sb.ToString();
                sb = new StringBuilder();
                foreach (var year in group.NumYears)
                {
                    sb.Append(year);
                    sb.Append(" ");
                }
                pGroupinfo.Visible = true;
                lblGroupInfo.Text = string.Format("Возраст: от {1} до {2}, Педагоги: {3}, Года обучения: {4}",
                                                  group.Name, group.AgeStart, group.AgeEnd, teachers, sb.ToString());
            }
        }
    }

    protected void lnkExportClaim_OnClick(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("ClaimList.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                                string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
        workbook.Open(MapPath("../Templates/ClaimList.xlsx"));
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];
        #region Стили
        Aspose.Cells.Style style_area = sheet.Cells["A2"].GetStyle();
        Aspose.Cells.Style style_header = sheet.Cells["A1"].GetStyle();

        style_header.Pattern = Aspose.Cells.BackgroundType.Solid;
        style_header.ForegroundColor = System.Drawing.Color.Gray;

        #endregion
        using (ClaimDb db = new ClaimDb())
        {
            long? childUnionId;
            try
            {
                childUnionId = Convert.ToInt64(ddlDO.SelectedValue);
            }
            catch
            {
                childUnionId = null;
            }
            DateTime? dateStart;
            try
            {
                dateStart = Convert.ToDateTime(tbDateStart.Text);
            }
            catch
            {
                dateStart = null;
            }
            DateTime? dateEnd;
            try
            {
                dateEnd = Convert.ToDateTime(tbDateEnd.Text);
            }
            catch
            {
                dateEnd = null;
            }
            DateTime? birthdayBegin;
            try
            {
                birthdayBegin = Convert.ToDateTime(tbBirthdayBegin.Text);
            }
            catch
            {
                birthdayBegin = null;
            }
            DateTime? birthdayEnd;
            try
            {
                birthdayEnd = Convert.ToDateTime(tbBirthdayEnd.Text);
            }
            catch
            {
                birthdayEnd = null;
            }
            Guid? teacherId;
            try
            {
                teacherId = new Guid(ddlTeacher.SelectedValue);
            }
            catch
            {
                teacherId = null;
            }
            var list = db.GetExportClaim(null, filterUdod.SelectedUdod, filterUdod.SelectedCity,
                                         childUnionId, null, filterStatus.DictClaimStatusId, tbNumber.Text,
                                         tbLastName.Text, tbFirstName.Text,
                                         tbMiddleName.Text, "", "", dateStart, dateEnd, null, birthdayBegin, birthdayEnd,
                                         Convert.ToInt32(rblBirthday.SelectedValue), teacherId);
            int i = 1;
            foreach (ExtendedClaim extendedClaim in list)
            {
                sheet.Cells[i, 0].PutValue(i);
                sheet.Cells[i, 1].PutValue(extendedClaim.Number);
                sheet.Cells[i, 2].PutValue(extendedClaim.CreatorRoleId == 3 ? "Учреждение" : extendedClaim.CreatorRoleId == 2?"ОСИП":"ПГУ");
                sheet.Cells[i, 3].PutValue(extendedClaim.CreatedDate.ToShortDateString());
                sheet.Cells[i, 4].PutValue(extendedClaim.Pupil.Fio);
                sheet.Cells[i, 5].PutValue(extendedClaim.Pupil.Birthday.ToShortDateString());
                sheet.Cells[i, 6].PutValue(extendedClaim.Pupil.Sex == 0 ? "М" : "Ж");
                sheet.Cells[i, 7].PutValue(string.IsNullOrEmpty(extendedClaim.Pupil.Addresses.Where(a => a.AddressType.AddressTypeId == 5).FirstOrDefault().KladrCode) ? "Москва" : "Московская область");
                //sheet.Cells[i, 6].PutValue(extendedPupil.AddressStr.Substring(0, 9) == "г. Москва" ? "Москва" : "Московская область");
                sheet.Cells[i, 8].PutValue(extendedClaim.Parent.Fio);
                MyStringBuilder sb =new MyStringBuilder("; ");
                sb.Append(extendedClaim.Parent.PhoneNumber);
                sb.Append(extendedClaim.Parent.ExpressPhoneNumber);
                sheet.Cells[i, 9].PutValue(sb.ToString());

                sheet.Cells[i, 10].PutValue(extendedClaim.Parent.EMail);
                sheet.Cells[i, 11].PutValue(extendedClaim.Udod.Name);
                sheet.Cells[i, 12].PutValue(extendedClaim.ChildUnion.Name);


                sheet.Cells[i, 13].PutValue(extendedClaim.ChildUnion.isDeleted?"Удалено":"Действующее");
                sheet.Cells[i, 14].PutValue(extendedClaim.ChildUnion.Program.Name);
                sheet.Cells[i, 15].PutValue(extendedClaim.ChildUnion.Section.Name);
                string strFio = "";
                sb = new MyStringBuilder("; ", extendedClaim.ChildUnion.Teachers.Select(i1=>i1.Fio));
                
                sheet.Cells[i, 16].PutValue(sb.ToString());
                sheet.Cells[i, 17].PutValue(extendedClaim.BeginDate.ToShortDateString());
                sheet.Cells[i, 18].PutValue(extendedClaim.NumYear);
                sheet.Cells[i, 19].PutValue(extendedClaim.ClaimStatus.ClaimStatusName);
                sheet.Cells[i, 20].PutValue(extendedClaim.UpdateStatusDate.ToShortDateString());

                
                i++;
            }
            sheet.Cells.CreateRange(0, 0, 1, 21).ApplyStyle(style_header, new Aspose.Cells.StyleFlag() { All = true });
            sheet.Cells.CreateRange(1, 0, i - 1, 21).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
            sheet.AutoFitRows();

        }
        workbook.Save("ClaimList.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }

    protected void lnkChangeStatus2_OnClick(object sender, EventArgs e)
    {
        long id = Convert.ToInt64((((LinkButton)sender).Parent.Parent as GridViewRow).Attributes["ClaimId"]);
        using (ClaimDb db = new ClaimDb())
        {
            var claim = db.GetClaim(id);
            if (DateTime.Now < claim.BeginDate)
            {
                ModalPopupStatus1.Show();
                lnkzach.Attributes.Add("claimId", claim.ClaimId.ToString());
                lnkzachnextyear.Attributes.Add("claimId", claim.ClaimId.ToString());
            }
            else
            {
                ChangeStatus_OnClick(sender, e);
            }
        }
    }
}
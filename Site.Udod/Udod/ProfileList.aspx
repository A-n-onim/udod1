﻿<%@ Page Language="C#" MasterPageFile="../Master/MasterPage.master" AutoEventWireup="true" CodeFile="ProfileList.aspx.cs" Inherits="Udod_ProfileList" %>
<%@ Register TagPrefix="uct" TagName="ProfileFilter" Src="~/UserControl/Filter/ucProfileFilter.ascx" %>

<asp:Content runat="server" ID="contentProfiles" ContentPlaceHolderID="body">
    <br />
    <asp:UpdatePanel runat="server" ID="up" UpdateMode="Conditional">
        <ContentTemplate>
            <uct:ProfileFilter ID="profileFilter" runat="server" OnChanged="ProgramIndex_Changed"/>
            <asp:LinkButton runat="server" ID="lnkAddProfile" OnClick="AddProfile_Click" Visible="false"><div class="btnBlue">Добавить профиль</div></asp:LinkButton>
            <asp:GridView runat="server" ID="gvProfileList" CssClass="dgc" AutoGenerateColumns="false" DataKeyNames="ProfileId" 
                          DataSourceID="datasource" onselectedindexchanged="gvProfileList_SelectedIndexChanged">
                <EmptyDataTemplate>
                    <asp:Label ID="lblEmptyGvProfileList" runat="server" style="font-size:16px">Нет объединений</asp:Label>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="№" >
                        <ItemTemplate><%# (gvProfileList.Rows.Count + 1)%></ItemTemplate>
                    </asp:TemplateField>
                        
                    <asp:TemplateField HeaderText="Наименование">
                        <ItemTemplate>
                            <%# Eval("Name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField  ButtonType="Link" ShowSelectButton="true" HeaderText="Изменить" SelectText="<div class='btnBlue'>Изменить</div>"/>
                    <asp:CommandField  ButtonType="Link" ShowDeleteButton="true" HeaderText="Удаление" DeleteText="<div class='btnRed'>Удалить</div>" />
                </Columns>
            </asp:GridView>
    </ContentTemplate>
    </asp:UpdatePanel>

    <asp:ObjectDataSource ID= "datasource" TypeName="Udod.Dal.DictProfileDb" DeleteMethod="DeleteProfile" runat="server" 
    SelectMethod="GetProfiles" SelectCountMethod="GetProfilesCount">
        <SelectParameters>
            <asp:ControlParameter Name="ProgramId" ControlID="profileFilter" DbType="Int32" ConvertEmptyStringToNull="true" PropertyName="SelectedValue" />
            <asp:Parameter Name="udodId" ConvertEmptyStringToNull="true" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:Panel runat="server" ID="popupAddProfile" BackColor="White" style="display: none; border: 2px solid black" CssClass="modalPopup">
        <asp:Panel ID="pnlHeader" runat="server" >
            <asp:Label ID="litTitle" Text="<span class='headerCaptions'>Добавление профиля</span>" runat="server"/>
            <asp:LinkButton ID="lnkClose" runat="server" 
                OnClientClick="$find('AddProfilePopup').hide(); return false;" />
        </asp:Panel>
        <asp:UpdatePanel ID="upAddProfile" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <table><tr><td class="tdx">
            Наименование:
            </td><td>
            <div class="inputXShort"><asp:TextBox runat="server" ID="tbProfileName" CssClass="inputXShort" /></div>
            </td></tr></table>
        
        </ContentTemplate>
        </asp:UpdatePanel>
        <table><tr><td>
            <asp:LinkButton ID="btnAddProfile" runat="server" Text="Сохранить" CausesValidation="false" OnClick="SaveProfile_Click"><div class="btnBlue">Сохранить</div></asp:LinkButton>
            </td><td>
            <asp:LinkButton ID="btnCancel" runat="server" Text="Отменить" CausesValidation="false"><div class="btnRed">Отменить</div></asp:LinkButton>
            </td></tr></table>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupAddExtender" PopupDragHandleControlID="pnlHeader"
        PopupControlID="popupAddProfile" CancelControlID="btnCancel"
        TargetControlID="btnShowPopup" RepositionMode="None"
        BehaviorID="AddProfilePopup"/>

    <asp:Button ID="btnShowPopup" runat="server" style="display:none" />
</asp:Content>
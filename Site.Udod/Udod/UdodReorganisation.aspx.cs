﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Udod_UdodReorganisation : BasePage
{
    public string ReorganisationType = "Merge";

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsDepartment);

        ReorganisationType = Page.Request.Params["type"];
        
        if (!Page.IsPostBack)
        {
            if ((ReorganisationType == "Merge") || (ReorganisationType == "Attach"))
            {
                MergeAttachContainer.Visible = true;
                lnkDone.Visible = true;
                lnkPrevious.Visible = false;
                lnkNext.Visible = false;
                
                using (UdodDb db = new UdodDb())
                {
                    /*var udodList = db.GetUdods(UserContext.CityId,null, null,null, null, null, null,null,null,null,null, null, null, null, null);
                    rbnFirstUdod_MergeAttach.DataSource = udodList;
                    rbnFirstUdod_MergeAttach.DataBind();
                    rbnFirstUdod_MergeAttach.SelectedIndex = 0;
                    rbnSecondUdod_MergeAttach.DataSource = udodList;
                    rbnSecondUdod_MergeAttach.DataBind();
                    rbnSecondUdod_MergeAttach.SelectedIndex = 0;*/
                    /*ddlBaseUdod_MergeAttach.Items.Add(rbnFirstUdod_MergeAttach.SelectedItem);
                    ddlBaseUdod_MergeAttach.Items.Add(rbnSecondUdod_MergeAttach.SelectedItem);
                    ddlBaseUdod_MergeAttach.SelectedIndex = 0;*/
                    ddlBaseUdod_MergeAttach.DataBind();
                }
                if (ReorganisationType == "Merge")
                {
                    lblNameUdod_MergeAttach.Visible = true;
                    tbxNameUdod_MergeAttach.Visible = true;
                    lblShortNameUdod_MeregeAttach.Visible = true;
                    tbxShortNameUdod_MergeAttach.Visible = true;
                }
                else
                {
                    lblFirstUdod_MergeAttach.Text = "Выберите основное УДО";
                    lblSecondUdod_MergeAttach.Text = "Выберите УДО, которое хотите присоединить к основному:";
                    lblNameUdod_MergeAttach.Visible = false;
                    tbxNameUdod_MergeAttach.Visible = false;
                    lblShortNameUdod_MeregeAttach.Visible = false;
                    tbxShortNameUdod_MergeAttach.Visible = false;
                }
            }  
            if ((ReorganisationType == "Detach") || (ReorganisationType == "Divide"))
            {
                DetachDivideContainer.Visible = true;
                lnkDone.Visible = false;
                lnkPrevious.Visible = false;
                lnkNext.Visible = true;
                AddressDistribution_DetachDivide.Visible = 
                TeacherDistribution_DetachDivide.Visible = 
                MetroDistribution_DetachDivide.Visible = 
                PassportUdod_DetachDivide.Visible = false;
                if (UserContext.CityId.HasValue)
                {
                    udodPassportFirst_DetachDivide.ClearData(UserContext.CityId.Value);
                    udodPassportSecond_DetachDivide.ClearData(UserContext.CityId.Value);
                }
                else
                {
                    udodPassportFirst_DetachDivide.ClearData(1);
                    udodPassportSecond_DetachDivide.ClearData(1);
                }

                using (UdodDb db = new UdodDb())
                {
                    var udodList = db.GetUdods(UserContext.CityId, null, null,null, null, null, null,null,null,null, null, null, null, null,null);
                    rbnUdodsList_DetachDivide.DataSource = udodList;
                    rbnUdodsList_DetachDivide.DataBind();
                    if (rbnUdodsList_DetachDivide.Items.Count>0) rbnUdodsList_DetachDivide.SelectedIndex = 0;
                    upDetachDivide.Update();
                    upAd.Update();
                    upButtons.Update();
                    upMe.Update();
                    upTe.Update();
                    
                }
                //lbxMainUdodTeachers_Detach = 
                //lbxMainUdodMetro_Detach =
                if (ReorganisationType == "Divide")
                {
                    lblUdodsList_DetachDivide.Text = "Выберите УДО, который хотите разделить на 2 новых УДО:";
                    lblAddressDistribution_DetachDivide.Text = "Распределите адреса между новыми УДО";
                    lblTeacherDistribution_DetachDivide.Text = "Распределите педагогов между 2 новыми УДО";
                    lblMetroDistribution_DetachDivide.Text = "Распределите станции метро между 2 новыми УДО";
                    lblUdodPassportFirst_DetachDivide.Text = "Заполните данные о первом новом УДО";
                }
                else
                {
                    lblUdodPassportFirst_DetachDivide.Visible = false;
                    udodPassportFirst_DetachDivide.Visible = false;
                    upDetachDivide.Update();
                }
            }
        }
    }

    protected void rbnFirstUdod_MergeAttach_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlBaseUdod_MergeAttach.Items[0].Value = rbnFirstUdod_MergeAttach.SelectedValue;
        ddlBaseUdod_MergeAttach.Items[0].Text = rbnFirstUdod_MergeAttach.SelectedItem.Text;
        //ddlBaseUdod_Merge.DataBind();
    }

    protected void rbnSecondUdod_MergeAttach_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlBaseUdod_MergeAttach.Items[1].Value = rbnSecondUdod_MergeAttach.SelectedValue;
        ddlBaseUdod_MergeAttach.Items[1].Text = rbnSecondUdod_MergeAttach.SelectedItem.Text;
        //ddlBaseUdod_Merge.DataBind();
    }

    protected void btnLeft_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        if (btn.ID == "btnLeftAddress_DetachDivide")
        {
            lbxFirstUdodAddresses_DetachDivide.Items.Add(lbxSecondUdodAddresses_DetachDivide.SelectedItem);
            lbxSecondUdodAddresses_DetachDivide.Items.Remove(lbxSecondUdodAddresses_DetachDivide.SelectedItem);
            lbxFirstUdodAddresses_DetachDivide.SelectedIndex = -1;
            lbxSecondUdodAddresses_DetachDivide.SelectedIndex = -1;
        }
        if (btn.ID == "btnLeftTeacher_DetachDivide")
        {
            lbxFirstUdodTeachers_DetachDivide.Items.Add(lbxSecondUdodTeachers_DetachDivide.SelectedItem);
            lbxSecondUdodTeachers_DetachDivide.Items.Remove(lbxSecondUdodTeachers_DetachDivide.SelectedItem);
            lbxFirstUdodTeachers_DetachDivide.SelectedIndex = -1;
            lbxSecondUdodTeachers_DetachDivide.SelectedIndex = -1;
        }
        if (btn.ID == "btnLeftMetro_DetachDivide")
        {
            lbxFirstUdodMetro_DetachDivide.Items.Add(lbxSecondUdodMetro_DetachDivide.SelectedItem);
            lbxSecondUdodMetro_DetachDivide.Items.Remove(lbxSecondUdodMetro_DetachDivide.SelectedItem);
            lbxFirstUdodMetro_DetachDivide.SelectedIndex = -1;
            lbxSecondUdodMetro_DetachDivide.SelectedIndex = -1;
        }
        //lbxFirstUdodAddresses_DetachDivide.DataBind();
        //lbxSecondUdodAddresses_DetachDivide.DataBind();
    }

    protected void btnRight_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        if (btn.ID == "btnRightAddress_DetachDivide")
        {
            lbxSecondUdodAddresses_DetachDivide.Items.Add(lbxFirstUdodAddresses_DetachDivide.SelectedItem);
            lbxFirstUdodAddresses_DetachDivide.Items.Remove(lbxFirstUdodAddresses_DetachDivide.SelectedItem);
            lbxFirstUdodAddresses_DetachDivide.SelectedIndex = -1;
            lbxSecondUdodAddresses_DetachDivide.SelectedIndex = -1;
        }
        if (btn.ID == "btnRightTeacher_DetachDivide")
        {
            lbxSecondUdodTeachers_DetachDivide.Items.Add(lbxFirstUdodTeachers_DetachDivide.SelectedItem);
            lbxFirstUdodTeachers_DetachDivide.Items.Remove(lbxFirstUdodTeachers_DetachDivide.SelectedItem);
            lbxFirstUdodTeachers_DetachDivide.SelectedIndex = -1;
            lbxSecondUdodTeachers_DetachDivide.SelectedIndex = -1;
        }
        if (btn.ID == "btnRightMetro_DetachDivide")
        {
            lbxSecondUdodMetro_DetachDivide.Items.Add(lbxFirstUdodMetro_DetachDivide.SelectedItem);
            lbxFirstUdodMetro_DetachDivide.Items.Remove(lbxFirstUdodMetro_DetachDivide.SelectedItem);
            lbxFirstUdodMetro_DetachDivide.SelectedIndex = -1;
            lbxSecondUdodMetro_DetachDivide.SelectedIndex = -1;
        }

        upAd.Update();
        //lbxFirstUdodAddresses_DetachDivide.DataBind();
        //lbxSecondUdodAddresses_DetachDivide.DataBind();
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Udod/UdodList.aspx");
    }

    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        if (DetachDivideContainer.ActiveTabIndex > 0)
        {
            DetachDivideContainer.ActiveTabIndex--;
        }
        //upDetachDivide.Update();

        ButtonVisibleness();
    }

    protected void ButtonVisibleness()
    {
        if (DetachDivideContainer.ActiveTabIndex == 0) 
            lnkPrevious.Visible = false;
        else 
            lnkPrevious.Visible = true;

        if (DetachDivideContainer.ActiveTabIndex == DetachDivideContainer.Tabs.Count - 1)
            lnkNext.Visible = false;
        else
            lnkNext.Visible = true;

        if (DetachDivideContainer.Tabs[DetachDivideContainer.Tabs.Count - 1].Visible)
        {
            lnkDone.Visible = true;
        }

        upDetachDivide.Update();
        upButtons.Update();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        
        if (DetachDivideContainer.ActiveTabIndex < DetachDivideContainer.Tabs.Count-1)
        {
            DetachDivideContainer.Tabs[DetachDivideContainer.ActiveTabIndex+1].Visible = true;
            DetachDivideContainer.ActiveTabIndex++;
        }
        
        //upAd.Update();
        if (DetachDivideContainer.ActiveTabIndex == 1)
        {
            using (UdodDb db = new UdodDb())
            {
                var udod = db.GetUdod(Convert.ToInt64(rbnUdodsList_DetachDivide.SelectedValue));
                lbxFirstUdodAddresses_DetachDivide.DataSource = udod.Addresses;
                lbxFirstUdodAddresses_DetachDivide.DataBind();
                lbxFirstUdodMetro_DetachDivide.DataSource = udod.Metros;
                lbxFirstUdodMetro_DetachDivide.DataBind();
            }

            using (TeacherDb db = new TeacherDb())
            {
                lbxFirstUdodTeachers_DetachDivide.DataSource = db.GetTeacher(null, Convert.ToInt64(rbnUdodsList_DetachDivide.SelectedValue), null);
                lbxFirstUdodTeachers_DetachDivide.DataBind();
            }
        }

        ButtonVisibleness();
    }

    protected void activeTabChanged(object sender, EventArgs e)
    {
        ButtonVisibleness();
    }

    protected void btnDone_Click(object sender, EventArgs e)
    {
        if ((ReorganisationType == "Merge") || (ReorganisationType == "Attach"))
        {
            using (UdodDb db = new UdodDb())
            {
                if ((ReorganisationType == "Merge") && (tbxNameUdod_MergeAttach.Text == ""))
                {
                    Response.Write("<script>window.alert('Не указано название нового УДО');</script>");
                    return;
                }
                
                if ((ReorganisationType == "Merge") && (tbxShortNameUdod_MergeAttach.Text == ""))
                {
                    Response.Write("<script>window.alert('Не указано короткое имя нового УДО');</script>");
                    return;
                }

                if (rbnFirstUdod_MergeAttach.SelectedValue == rbnSecondUdod_MergeAttach.SelectedValue)
                {
                    Response.Write("<script>window.alert('Выбранные УДО не должны совпадать');</script>");
                    return;
                }

                // обычно данные берутся из основного УДО, но если выбран второстепенный, то просто меняем местами УДОы
                if (rbnSecondUdod_MergeAttach.SelectedValue == ddlBaseUdod_MergeAttach.SelectedValue)
                {
                    var tmp = rbnSecondUdod_MergeAttach.SelectedValue;
                    rbnSecondUdod_MergeAttach.SelectedValue = ddlBaseUdod_MergeAttach.SelectedValue;
                    ddlBaseUdod_MergeAttach.SelectedValue = tmp;
                }

                db.MergeOrAttachUdods(Convert.ToInt64(rbnFirstUdod_MergeAttach.SelectedValue), Convert.ToInt64(rbnSecondUdod_MergeAttach.SelectedValue), 
                    tbxNameUdod_MergeAttach.Visible ? tbxNameUdod_MergeAttach.Text : null, tbxShortNameUdod_MergeAttach.Visible ? tbxShortNameUdod_MergeAttach.Text : null);
            }

            if (ReorganisationType == "Merge")
            {
                EventsUtility.LogUdodMerge(UserContext.Profile.UserId, Request.UserHostAddress,
                    String.Format("(Идентификаторы УДО: {0} -> {1}: {2})", rbnFirstUdod_MergeAttach.SelectedValue, rbnSecondUdod_MergeAttach.SelectedValue, tbxNameUdod_MergeAttach.Text));

                EventsUtility.LogUdodStatusUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                    String.Format("(ОУ ликвидировано в результате слияния: {0})", rbnSecondUdod_MergeAttach.SelectedValue));

            }
            else
            {
                EventsUtility.LogUdodAttach(UserContext.Profile.UserId, Request.UserHostAddress,
                    String.Format("(Идентификаторы УДО: {0} -> {1})", rbnFirstUdod_MergeAttach.SelectedValue, rbnSecondUdod_MergeAttach.SelectedValue));

                EventsUtility.LogUdodStatusUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                    String.Format("(ОУ ликвидировано в результате присоединения: {0})", rbnSecondUdod_MergeAttach.SelectedValue));
            }

            Response.Redirect("~/Udod/UdodList.aspx");
        }

        if ((ReorganisationType == "Detach") || (ReorganisationType == "Divide"))
        {
            using (UdodDb db = new UdodDb())
            {
                /*string firstUdodAddressesIds="";//=String.Join(", ", lbxFirstUdodAddresses_DetachDivide.Items)
                foreach (ListItem lic in lbxFirstUdodAddresses_DetachDivide.Items)
                {
                    firstUdodAddressesIds += lic.Value + ",";
                }
                if (firstUdodAddressesIds != "") firstUdodAddressesIds.Remove(firstUdodAddressesIds.Length - 1, 1);*/

                string secondUdodAddressesIds = "";
                foreach (ListItem lic in lbxSecondUdodAddresses_DetachDivide.Items)
                {
                    secondUdodAddressesIds += lic.Value + ",";
                }
                if (secondUdodAddressesIds != "") secondUdodAddressesIds.Remove(secondUdodAddressesIds.Length - 1, 1);

                string secondUdodTeachersIds = "";
                foreach (ListItem lic in lbxSecondUdodTeachers_DetachDivide.Items)
                {
                    secondUdodTeachersIds += lic.Value + ",";
                }
                if (secondUdodTeachersIds != "") secondUdodTeachersIds.Remove(secondUdodTeachersIds.Length - 1, 1);

                string secondUdodMetrosIds = "";
                foreach (ListItem lic in lbxSecondUdodMetro_DetachDivide.Items)
                {
                    secondUdodMetrosIds += lic.Value + ",";
                }
                if (secondUdodMetrosIds != "") secondUdodMetrosIds.Remove(secondUdodMetrosIds.Length - 1, 1);

                db.DetachOrDivideUdods(Convert.ToInt64(rbnUdodsList_DetachDivide.SelectedValue), 
                    secondUdodAddressesIds, secondUdodTeachersIds, secondUdodMetrosIds,
                    udodPassportFirst_DetachDivide.Visible ? udodPassportFirst_DetachDivide.GetUdodData() : null,
                    udodPassportSecond_DetachDivide.GetUdodData());

                if (ReorganisationType == "Detach")
                {
                    EventsUtility.LogUdodDetach(UserContext.Profile.UserId, Request.UserHostAddress,
                        String.Format("(Идентификатор УДО: {0})(Параметры:(secondUdodAddressesIds: {1}),(secondUdodTeachersIds: {2}),(secondUdodMetrosIds: {3}),(Udod2: {4}))",
                            rbnUdodsList_DetachDivide.SelectedValue,
                            secondUdodAddressesIds, secondUdodTeachersIds, secondUdodMetrosIds,
                            udodPassportSecond_DetachDivide.GetUdodData().Name
                        ));
                }
                else
                {
                    EventsUtility.LogUdodDivide(UserContext.Profile.UserId, Request.UserHostAddress,
                        String.Format("(Идентификатор УДО: {0})(Параметры:(secondUdodAddressesIds: {1}),(secondUdodTeachersIds: {2}),(secondUdodMetrosIds: {3}),(Udod1: {4}),(Udod2: {5}))",
                            rbnUdodsList_DetachDivide.SelectedValue,
                            secondUdodAddressesIds, secondUdodTeachersIds, secondUdodMetrosIds,
                            udodPassportFirst_DetachDivide.GetUdodData().Name,
                            udodPassportSecond_DetachDivide.GetUdodData().Name
                        ));
                }
            }

            Response.Redirect("~/Udod/UdodList.aspx");
        }
    }

    protected void lnkFirstFind_OnClick(object sender, EventArgs e)
    {
        using (UdodDb db = new UdodDb())
        {
            var udodList = db.GetUdods(cityUdodFilterFirst.SelectedCity, null, null, null, null, null, null, tbFirstName.Text, null, null, null,
                                       null, null, null, null);
            udodList = udodList.Where(i => i.UdodStatusId != 2).ToList();
            rbnFirstUdod_MergeAttach.DataSource = udodList;
            rbnFirstUdod_MergeAttach.DataBind();
            if (udodList.Count != 0)
            {
                rbnFirstUdod_MergeAttach.SelectedIndex = 0;
                ddlBaseUdod_MergeAttach.Items.Add(rbnFirstUdod_MergeAttach.SelectedItem);
                ddlBaseUdod_MergeAttach.SelectedIndex = 0;
            }
        }
    }

    protected void lnkSecondFind_OnClick(object sender, EventArgs e)
    {
        using (UdodDb db = new UdodDb())
        {
            var udodList = db.GetUdods(cityUdodFilterSecond.SelectedCity, null, null, null, null, null, null, tbSecondName.Text, null, null, null,
                                       null, null, null, null);
            udodList = udodList.Where(i => i.UdodStatusId != 2).ToList();
            rbnSecondUdod_MergeAttach.DataSource = udodList;
            rbnSecondUdod_MergeAttach.DataBind();
            if (udodList.Count != 0)
            {
                rbnSecondUdod_MergeAttach.SelectedIndex = 0;
                ddlBaseUdod_MergeAttach.Items.Add(rbnSecondUdod_MergeAttach.SelectedItem);
                ddlBaseUdod_MergeAttach.SelectedIndex = 0;
            }
            
        }
    }
}
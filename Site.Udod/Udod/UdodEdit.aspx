﻿<%@ Page Language="C#" MasterPageFile="../Master/MasterPage.master" AutoEventWireup="true" CodeFile="UdodEdit.aspx.cs" Inherits="Udod_UdodEdit" %>
<%@ Register src="../UserControl/Udod/ucUdodPassport.ascx" tagName="UdodPassport" tagPrefix="uct" %>
<%@ Register src="../UserControl/Udod/ucDOPassport.ascx" tagName="DOPassport" tagPrefix="uct" %>
<%@ Register src="../UserControl/Address/ucNewAddress.ascx" tagName="Address" tagPrefix="uct" %>
<%@ Register src="../UserControl/Filter/ucCityUdodFilter.ascx" tagName="CityUdodFilter" tagPrefix="uct" %>
<%@ Register TagPrefix="uct" TagName="FindFilter" Src="~/UserControl/Filter/ucFindFilter.ascx" %>
<%@ Register TagPrefix="uct" TagName="Pager" Src="~/UserControl/Menu/ucPager.ascx" %>
<%@ Register TagPrefix="uct" TagName="SortHeader" Src="~/UserControl/Menu/ucSortHeader.ascx" %>
<%@ Register TagPrefix="uct" TagName="ErrorPopup" Src="~/UserControl/Menu/ucErrorPopup.ascx" %>

<asp:Content runat="server" ID="ContentUdodEdit" ContentPlaceHolderID="body">

<script type="text/javascript">
    var eng = new Array("Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "{", "}", "A", "S", "D", "F", "G", "H", "J", "K", "L", ":", "\"", "Z", "X", "C", "V", "B", "N", "M", "<", ">", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "\'", "z", "x", "c", "v", "b", "n", "m", ",", ".", "@");
    var rus = new Array("\u0419", "\u0426", "\u0423", "\u041A", "\u0415", "\u041D", "\u0413", "\u0428", "\u0429", "\u0417", "\u0425", "\u042A", "\u0424", "\u042B", "\u0412", "\u0410", "\u041F", "\u0420", "\u041E", "\u041B", "\u0414", "\u0416", "\u042D", "\u042F", "\u0427", "\u0421", "\u041C", "\u0418", "\u0422", "\u042C", "\u0411", "\u042E", "\u0439", "\u0446", "\u0443", "\u043A", "\u0435", "\u043D", "\u0433", "\u0448", "\u0449", "\u0437", "\u0445", "\u044A", "\u0444", "\u044B", "\u0432", "\u0430", "\u043F", "\u0440", "\u043E", "\u043B", "\u0434", "\u0436", "\u044D", "\u044F", "\u0447", "\u0441", "\u043C", "\u0438", "\u0442", "\u044C", "\u0431", "\u044E", '"');

    $(document).ready(function () {
        var tbEmail = '#' + '<%= tbAddressEmail.ClientID %>';
        $(tbEmail).bind("keyup", ChangeToEg);

        var gvAddresses = '#' + '<%= gvAddresses.ClientID %>';
        $(gvAddresses).find("input[type=checkbox]").each(function () {
            $(this).bind("click", Uncheck);
        }); 
    });

    function Uncheck(event) {
        var currentel = $(this);
        var gvAddresses = '#' + '<%= gvAddresses.ClientID %>';
        $(gvAddresses).find("input[type=checkbox]").each(function () {
            if (this.id == currentel[0].id) {
                $(this).attr("checked", "true");
            }
            else {
                $(this).removeAttr("checked");
            }
        });
    }


    function isValidAddresses(source, args) {
        var gvAddresses = '<%= gvAddresses.ClientID %>';
        args.IsValid = $('#' + gvAddresses).find("input:checked").length > 0;
    }

    function ChangeToEg(event) {
        if (event.keyCode > 46) {
            var str = $(this).val();
            str = getRus2EngString(str);
            $(this).val(str);
        }
    }

    function getRus2EngString(target) {
        var res = "";
        for (var j = 0; j < target.length; j++) {
            var test = target.charAt(j);
            for (var i = 0; i < eng.length; i++) {
                if (test == rus[i]) if (test != "\'") test = eng[i];
            }
            res += test;
        }
        return res;
    }

    function pageLoad() {
        var html = document.documentElement;

        if (parseInt(html.scrollTop) < 100) {
            //alert(html.scrollTop);
            $('<%="#"+popupSelectDO.ClientID %>').css('position', 'absolute');
            $('<%="#"+popupSelectDO.ClientID %>').addClass('popupTop');
            //alert($('<%="#"+popupSelectDO.ClientID %>').css('top'));
        }
        else {
            $('<%="#"+popupSelectDO.ClientID %>').removeClass('popupTop');
        }
    }
</script>
    <asp:UpdatePanel runat="server" ID="uptmp" UpdateMode="Always">
        <ContentTemplate>
            <uct:ErrorPopup runat="server" ID="ucErrorPopup" Visible="false" Width="200"/> 
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upCityUdodFilter" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <uct:CityUdodFilter ID="cityUdodFilter" runat="server" UdodDdlEnabled="true" onUdodChange="UdodChange" onCityChange="UdodChange"/>
    </ContentTemplate>
    </asp:UpdatePanel>
    <br/>
    
    <div >
        <table>
            <tr>
                <td colspan="4" bgcolor="f2f2f2">
                    <strong><asp:Label ID="litTitle" Text="Добавление учреждения" runat="server"/></strong>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblInfoService" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel runat="server" ID="upPassport" UpdateMode="Conditional">
                        <ContentTemplate>
                        <asp:DropDownList runat="server" ID="ddlIsIndividual" AutoPostBack="true" OnSelectedIndexChanged="ddlIsIndividual_SelectedIndexChanged">
                            <Items>
                                <asp:ListItem Text="Организация, осуществляющая образовательную деятельность" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Индивидуальный предприниматель, осуществляющий образовательную деятельность" Value="1"></asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                        <uct:UdodPassport ID="udodPassport" runat="server"/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <table runat="server" ID="tEkis">
            <tr>
                <td>
                    <asp:TextBox runat="server" ID="tbEkis" CssClass="input"></asp:TextBox>
                </td>
                <td>
                    <asp:LinkButton runat="server" ID="lnkChangeEkis" OnClick="lnkChangeEkis_OnClick"><div class="btnBlue">Сменить ЕКИС</div></asp:LinkButton>
                </td>
            </tr>
        </table>
        <table runat="server" id="table2">
            <tr>
                <td>
                    <b>Реализуемые направленности по лицензии:</b>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel runat="server" ID="upPrograms" UpdateMode="Conditional">
                        <ContentTemplate>
                        <asp:GridView runat="server" ID="gvProgramList" CssClass="dgc" AutoGenerateColumns="false" DataKeyNames="ProgramId" 
                                      DataSourceID="dsUdodProgram" OnRowDataBound="gvProgramList_RowDataBound">
                            <EmptyDataTemplate>
                                <asp:Label ID="lblEmptyGvProgramList" runat="server" style="font-size:16px">Нет направленностей</asp:Label>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField >
                                    <ItemTemplate><%# (gvProgramList.Rows.Count + 1)%></ItemTemplate>
                                </asp:TemplateField>
                        
                                <asp:TemplateField HeaderText="Направленность">
                                    <ItemTemplate>
                                        <%# Eval("Name") %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-Width="140px" HeaderText="Есть в лицензии" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
		                                <asp:CheckBox runat="server" ID="chkIsExist" Checked='<%# Eval("isExistInCurrentUdod") %>' Enabled='<%# Eval("isEnabledToChange") %>' ToolTip='<%# GetToolTip(Container.DataItem) %>'/>
	                                </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="cityUdodFilter" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        
        <table runat="server" id="table1">
            <tr>
                <td>
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <hr />
                </td>
            </tr>
            <tr>
                <td colspan="4" bgcolor="f2f2f2">
                    <strong>Адреса</strong>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel runat="server" ID="upLnkAddAddress" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:LinkButton runat="server" ID="AddAddress" onclick="AddAddress_Click"><div class='btnBlue'>Добавить адрес</div></asp:LinkButton>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr> 
            <tr>
                <td>
                    <asp:UpdatePanel runat="server" ID="upAddresses" UpdateMode="Conditional">
                    <ContentTemplate>
                    <asp:GridView ID="gvAddresses" runat="server" AutoGenerateColumns="false" CssClass="dgc"
                                    DataKeyNames="AddressId" 
                                    OnSelectedIndexChanged="gvAddresses_SelectedIndexChanged" OnRowDeleting="gvAddress_Deleting" OnRowDataBound="gvAddress_RowDataBound">
                            <EmptyDataTemplate>
                                <asp:Label ID="lblEmptyGvAddresses" runat="server" style="font-size:16px">Нет адресов</asp:Label>
                            </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# (gvAddresses.Rows.Count + 1)%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Основной адрес">
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="cbxSelect" Checked='<%# Eval("IsPrimary") %>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Адрес">
                                <ItemTemplate>
                                    <%# Eval("AddressStr") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Принадлежит ОУ">
                                <ItemTemplate>
                                    <%# Eval("AddressUdodName") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <%# GetErrorString(Container.DataItem)%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ButtonType="Link" HeaderText="Редактирование" 
                                SelectText="<div class='btnBlue'>Редактировать адрес</div>" ShowSelectButton="true" />
                            <asp:CommandField  ButtonType="Link" ShowDeleteButton="true" 
                                HeaderText="Удаление" DeleteText="<div class='btnRed'>Удалить адрес</div>"/>
                        </Columns>
                    </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger runat="server" ControlID="btnSaveAddress"/>
                        <asp:AsyncPostBackTrigger runat="server" ControlID="AddAddress"/>
                    </Triggers>
                    </asp:UpdatePanel>
                    <asp:CustomValidator runat="server" ID="valCheckBoxList" ValidationGroup="78" ClientValidationFunction="isValidAddresses" EnableClientScript="true" Display="Dynamic" ErrorMessage="Необходимо отметить один адрес" ></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <hr />
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:LinkButton ID="btnAddUdod" runat="server" CausesValidation="true" ValidationGroup="78" OnClick="SaveUdod_Click"><div class="btnBlue">Сохранить</div></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false" OnClick="BackToUdodList_Click"><div class="btnBlue">Назад</div></asp:LinkButton>
                </td>
            </tr>
        </table>
        <table runat="server" visible="false">
            
            <tr>
                <td colspan="4" bgcolor="f2f2f2">
                    <strong>Детские объединения</strong>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton runat="server" ID="addDO" onclick="AddDO_Click"><div class='btnBlueLong'>Добавить детское объединение</div></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel DefaultButton="lnkFind" runat="server" ID="pFind">
                        <asp:UpdatePanel runat="server" ID="upFilter" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <uct:FindFilter ID="filterSection" runat="server" BudgetTypeVisible="false"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton runat="server" ID="lnkFind" OnClick="lnkFind_OnClick"><div class="btnBlueLong">Поиск</div></asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton runat="server" ID="lnkClear" OnClick="lnkClear_OnClick"><div class="btnBlueLong">Сбросить фильтры</div></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="filterSection" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel runat="server" ID="upDO" UpdateMode="Conditional">
                    <ContentTemplate>
                    <asp:GridView ID="gvChildUnion" runat="server" AutoGenerateColumns="false" CssClass="dgc"
                                    DataKeyNames="ChildUnionId"
                                    OnSelectedIndexChanged="gvChildUnion_SelectedIndexChanged" OnRowDeleting="gvChildUnion_Deleting">
                            <EmptyDataTemplate>
                                <asp:Label ID="lblEmptyGvChildUnion" runat="server" style="font-size:16px">Нет детских объединений</asp:Label>
                            </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# (gvChildUnion.Rows.Count + pgrChildUnionList.PagerIndex)%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Название">
                                <HeaderTemplate>
                                    <uct:SortHeader ID="shName" runat="server" HeaderCaption="Название" OnSortingChanged="gvChildUnionSortingChanged" SortExpression="Name"/>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("Name") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Направленность">
                                <HeaderTemplate>
                                    <uct:SortHeader ID="shProgramName" runat="server" HeaderCaption="Направленность" OnSortingChanged="gvChildUnionSortingChanged" SortExpression="ProgramName"/>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("Program.Name") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Профиль">
                                <HeaderTemplate>
                                    <uct:SortHeader ID="shProfileName" runat="server" HeaderCaption="Профиль" OnSortingChanged="gvChildUnionSortingChanged" SortExpression="ProfileName"/>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("Profile.Name") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Вид деятельности">
                                <HeaderTemplate>
                                    <uct:SortHeader ID="shSectionName" runat="server" HeaderCaption="Вид деятельности" OnSortingChanged="gvChildUnionSortingChanged" SortExpression="SectionName"/>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("Section.Name")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Педагог">
                                <%-- <HeaderTemplate>
                                    <uct:SortHeader ID="shTeacher" runat="server" HeaderCaption="Педагог" OnSortingChanged="gvChildUnionSortingChanged" SortExpression="TeacherLastName"/>
                                </HeaderTemplate>--%>
                                <ItemTemplate>
                                    <%# GetChildUnionTeachers(Container.DataItem) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  HeaderText="Возраст обучающихся">
                                <HeaderTemplate>
                                    <uct:SortHeader ID="shAge" runat="server" HeaderCaption="Возраст обучающихся" OnSortingChanged="gvChildUnionSortingChanged" SortExpression="AgeStart"/>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# GetChildUnionAge(Container.DataItem)%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ShowSelectButton="true" SelectText="<img src='../Images/edit_smth.png' border=0 title='Просмотр паспорта ДО'>"/>
                            <asp:CommandField ShowDeleteButton="true" DeleteText="<img src='../Images/delete_smth.png' border=0 title='Удалить ДО'>"/>
                        </Columns>
                        <PagerStyle CssClass="gridpager" VerticalAlign="Bottom" HorizontalAlign="Left"/>
                        <PagerSettings Mode="Numeric" Position="Bottom" PageButtonCount="10" />
                    </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger runat="server" ControlID="btnSaveDOPassport"/>
                        <asp:AsyncPostBackTrigger runat="server" ControlID="btnCancelPopup" />
                        <asp:AsyncPostBackTrigger runat="server" ControlID="filterSection" />
                    </Triggers>
                    </asp:UpdatePanel>
                    <uct:Pager runat="server" ID="pgrChildUnionList" needEndDots="false" needStartDots="false" pagesCount="10" OnChange="pgrChildUnionList_OnChange" />
                </td>
            </tr>
        </table>
    </div>
    
    <asp:ObjectDataSource ID="dsUdodProgram" TypeName="Udod.Dal.DictProgramDb"  runat="server" 
        SelectMethod="GetUdodPrograms">
        <SelectParameters>
            <asp:ControlParameter Name="udodId" ControlID="cityUdodFilter" PropertyName="SelectedUdod" ConvertEmptyStringToNull="true" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:Panel runat="server" ID="popupSelectDO" CssClass="modalPopup" Width="900" style="padding: 5px; display: none; border: 2px solid black" BackColor="White">
        <asp:Panel ID="pnlHeader" runat="server" >
            <asp:Label ID="Label1" Text="<span class='headerCaptions'>Паспорт детского объединения</span>" runat="server"/>
            <asp:LinkButton ID="lnkClose" runat="server" 
                OnClientClick="$find('SelectDOPopup').hide(); return false;" />
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
        <ContentTemplate>
            <div >
                <uct:DOPassport runat="server" ID="selectDO" Edit="true"/>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="selectDO" />
            <asp:AsyncPostBackTrigger runat="server" ControlID="addDO"/>
        </Triggers>
        </asp:UpdatePanel>
            <div >
            <table>
                <tr>
                    <td>
                        <asp:LinkButton ID="btnSaveDOPassport" runat="server" CausesValidation="true" ValidationGroup="79" OnClick="btnSaveDOPassport_Click"><div class="btnBlue">Сохранить</div></asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnCancelPopup" runat="server" CausesValidation="false" OnClick="btnCloseDOPassport_Click"><div class="btnBlue">Отменить</div></asp:LinkButton>
                    </td>
                </tr>
            </table>
            </div>
        
    </asp:Panel>
    
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupSelectDOExtender" PopupDragHandleControlID="pnlHeader"
        PopupControlID="popupSelectDO"
        TargetControlID="btnShowSelectDO" RepositionMode="None"
        BehaviorID="SelectDOPopup"/>
    
    <asp:Button ID="btnShowSelectDO" runat="server" style="display:none" />

    <asp:Panel runat="server" ID="popupSelectAddress" style="padding: 5px; display: none; z-index: 0; border: 2px solid black" BackColor="White">
        <asp:UpdatePanel runat="server" ID="upGridView" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pSelectAddressHeader" runat="server" >
                <asp:Label ID="Label2" Text="<span class='headerCaptions'>Редактирование адреса</span>" runat="server"/>
                <asp:LinkButton ID="LinkButton1" runat="server" 
                    OnClientClick="$find('SelectAddressPopup').hide(); return false;" />
            </asp:Panel>
        </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel runat="server" ID="upAddressPopup" UpdateMode="Conditional">
        <ContentTemplate>
        <div >
            <uct:Address runat="server" ID="AddressPopup" AddressTypeId="2" VisibleFlat="false" IsValidateEdodEdit="true" />
            
            <table>
                <tr>
                    <td class="tdx">
                        График работы:
                    </td>
                    <td>
                        <asp:TextBox ID="tbAddressWorkTime" runat="server" CssClass="input"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Телефон в корпусе:
                    </td>
                    <td>
                        <asp:TextBox ID="tbAddressPhone" runat="server" CssClass="input"></asp:TextBox>
                        <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbAddressPhone" Mask="8(999)999-99-99" MaskType="Number" />
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Email:
                    </td>
                    <td>
                        <asp:TextBox ID="tbAddressEmail" runat="server" CssClass="input"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan=2>
                        <%-- <asp:RequiredFieldValidator runat="server" ID="val4" ValidationGroup="80" ErrorMessage="Поле 'EMail' не заполнено" ControlToValidate="tbAddressEmail" Display="Dynamic"  ></asp:RequiredFieldValidator> --%>
                        <asp:RegularExpressionValidator runat="server" ID="valEmail" ErrorMessage="Неверный формат электронной почты" ValidationGroup="80"
                            ControlToValidate="tbAddressEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Принадлежит ОУ
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="tbAddressUdodName" CssClass="input"></asp:TextBox>
                        <ajaxToolkit:AutoCompleteExtender runat="server" ID="acAddressUdodName" TargetControlID="tbAddressUdodName" ServiceMethod="GetAddressUdodName" EnableCaching="true" CompletionSetCount="50" MinimumPrefixLength="1" UseContextKey="true" BehaviorID="AutoCompleteEx" CompletionListCssClass="addresslist"/>
                    </td>
                </tr>
            </table>
        </div>
        </ContentTemplate>
    </asp:UpdatePanel>
        <div >
            <table>
                <tr>
                    <td>
                        <asp:LinkButton ID="btnSaveAddress" runat="server" ValidationGroup="80" Text="<div class='btnBlue'>Сохранить</div>" CausesValidation="true" OnClick="btnSaveAddress_Click"/>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnCancelAddress" runat="server" Text="<div class='btnBlue'>Отменить</div>" CausesValidation="false" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupSelectAddressExtender" PopupDragHandleControlID="pSelectAddressHeader"
        PopupControlID="popupSelectAddress" CancelControlID="btnCancelAddress"
        TargetControlID="btnShowAddressPopup" RepositionMode="None"
        BehaviorID="SelectAddressPopup"/>

    <asp:Button ID="btnShowAddressPopup" runat="server" style="display:none" />

    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupSelectMetroExtender" PopupDragHandleControlID="pHeaderMetro"
        PopupControlID="popupSelectMetro" 
        TargetControlID="btnShowSelectMetro" RepositionMode="None"
        BehaviorID="SelectMetroPopup"/>
    
    <asp:Button ID="btnShowSelectMetro" runat="server" style="display:none" />
    
    

    <asp:Panel runat="server" ID="errorPopup" style="display: none; padding: 5px; border: 2px solid black" BackColor="White">
        <asp:UpdatePanel ID="upError" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pHeaderError" runat="server" >
                    <asp:Label ID="lblErrorHeader" Text="<span class='headerCaptions'>Ошибка</span>" runat="server"/>
                    <asp:LinkButton ID="LinkButton3" runat="server" 
                        OnClientClick="$find('PopupError').hide(); return false;" />
                </asp:Panel>
                <asp:Label style="font-size:20;" ID="lblErrorText" Text="<div style='font-size: 12pt; padding-top:10px; padding-bottom: 10px;'>В данном УДО уже есть ДО с таким названием</div>" runat="server" />
                <table>
                    <tr>
                        <td>
                            <asp:LinkButton runat="server" Text="<div class='btnBlue'>Резерв</div>" ID="lnkErrorOk" OnClick="lnkErrorOkOnClick"/>
                        </td>
                        <td>
                            <asp:LinkButton runat="server" Text="<div class='btnBlue'>Отмена</div>" ID="lnkErrorCancel" OnClick="lnkErrorCancelOnClick"/>
                        </td>
                    </tr>
                </table>
                </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    
    <ajaxToolkit:ModalPopupExtender runat="server" ID="errorModalPopupExtender" 
        PopupControlID="errorPopup"
        TargetControlID="btnShowErrorModalPopup" RepositionMode="None" PopupDragHandleControlID="pHeaderError"
        CancelControlID="lnkErrorCancel"
        BehaviorID="PopupError"/>
    
    <asp:Button ID="btnShowErrorModalPopup" runat="server" style="display:none" />

</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Udod_ImportedPupilList : BasePage
{
    protected Guid? PupilId { set { ViewState["pupilId"] = value; } get { if (ViewState["pupilId"] == null) return null;
        return (Guid) ViewState["pupilId"];
    } }
    private bool? IsEdit
    {
        get
        {
            if (ViewState["IsEdit"] == null) return null;
            return (bool)ViewState["IsEdit"];
        }
        set
        {
            ViewState["IsEdit"] = value;
        }
    } 
    protected void Page_Load(object sender, EventArgs e)
    {
        string values = ConfigurationManager.AppSettings["SecondYear"];
        var splits = values.Split(',');
                
        CheckPermissions(UserContext.Roles.IsAdministrator /*||UserContext.Roles.IsUdodEmployee*/ 
            // зеленоград и Северо-запад и север и свао
            || (UserContext.UdodId.HasValue && splits.Contains(UserContext.UdodId.Value.ToString())));
        if (UserContext.Roles.RoleId==(int)EnumRoles.UdodEmployee)
        {
            UserContext.UdodId = UserContext.Profile.UdodId;
            cityUdodFilter.Visible = false;
        }
        
        lnkImportPupils.Visible = UserContext.Roles.IsAdministrator;

        valrDate.MinimumValue = DateTime.Today.AddYears(-22).ToString().Substring(0, 10);
        valrDate.MaximumValue = DateTime.Today.AddYears(-2).ToString().Substring(0, 10);
        valrDate2.MaximumValue = DateTime.Today.ToString().Substring(0, 10);
        valrDate3.MaximumValue = DateTime.Today.AddYears(-14).ToString().Substring(0, 10);
        acSchool.ServicePath = SiteUtility.GetUrl("~/Webservices/Dict.asmx");

        
    }

    protected void pgrPupilList_OnChange(object sender, EventArgs e)
    {
        gvPupilList.DataBind();
        upPupilList.Update();
    }

    protected void cityUdodFilter_OnCityChange(object sender, EventArgs e)
    {
        gvPupilList.DataBind();
        upPupilList.Update();
    }

    protected void cityUdodFilter_OnUdodChange(object sender, EventArgs e)
    {
        gvPupilList.DataBind();
        upPupilList.Update();
    }

    protected void cityUdodFilter_OnloadComplete(object sender, EventArgs e)
    {
        
    }

    protected void gvPupilList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        // Загрузка редактирования
        if (gvPupilList.SelectedDataKey != null)
        {
            string tmp=gvPupilList.Rows[gvPupilList.SelectedIndex].Cells[8].Text;
            Regex regex = new Regex(@"'([^<]+)'>");
            var matches= regex.Matches(tmp);
            StringBuilder sb = new StringBuilder();
            foreach (var match in matches)
            {
                sb.Append(match.ToString().Replace("'>","").Replace("'",""));
            }
            lblErrors.Text = sb.ToString();
            PupilId = (Guid) gvPupilList.SelectedDataKey.Value;
            using (PupilsDb db = new PupilsDb())
            {
                ExtendedImportedPupil pupil = db.GetImportedPupil(PupilId.Value);

                // закрываем на редактирование
                PupilFio.Enabled =
                PupilDocument.Enabled =
                tbBirthday.Enabled =
                tbPupilSchool.Enabled =
                cbSex.Enabled =
                tbPupilClass.Enabled = !pupil.IsEdit;

                PupilFio.Enabled =
                    PupilDocument.Enabled = false;
                if (!pupil.IsEdit)
                {
                    PupilFio.EnabledMiddleName = true;
                    btnSavePupil.Visible = true;
                    upSavePupil.Update();
                }

                PupilFio.LastName = pupil.LastName;
                PupilFio.FirstName = pupil.FirstName;
                PupilFio.MiddleName = pupil.MiddleName;
                
                PupilDocument.Series = pupil.Passport.Series;
                PupilDocument.Number = pupil.Passport.Number;
                PupilDocument.IssueDate = pupil.Passport.IssueDate;
                PupilDocument.PassportType = pupil.Passport.PassportType.PassportTypeId;
                tbBirthday.Text = pupil.Birthday.ToShortDateString();
                tbPupilEmail.Text = pupil.EMail;
                tbPupilPhoneNumber.Text = pupil.PhoneNumber;
                tbPupilSchool.Text = pupil.SchoolName;
                tbPupilClass.Text = pupil.ClassName;
                tbNumYear.Text = pupil.NumYear.ToString();
                tbChildUnionName.Text = pupil.ChildUnions[0].Name;
                cbSex.SelectedIndex = pupil.Sex;
                lblAddressStr.Text = pupil.AddressStr;

                
                
                if (pupil.Parents.Count > 0)
                {
                    /*ParentFio.LastName = pupil.Parents.FirstOrDefault().LastName;
                    ParentFio.FirstName = pupil.Parents.FirstOrDefault().FirstName;
                    ParentFio.MiddleName = pupil.Parents.FirstOrDefault().MiddleName;
                    parentDocument.PassportType = pupil.Parents.FirstOrDefault().Passport.PassportType.PassportTypeId;
                    parentDocument.Series = pupil.Parents.FirstOrDefault().Passport.Series;
                    parentDocument.Number = pupil.Parents.FirstOrDefault().Passport.Number;
                    parentDocument.IssueDate = pupil.Parents.FirstOrDefault().Passport.IssueDate;
                    tbParentPhoneNumber.Text = pupil.Parents.FirstOrDefault().PhoneNumber;
                    tbParentExpressPhoneNumber.Text = pupil.Parents.FirstOrDefault().ExpressPhoneNumber;
                    tbParentEmail.Text = pupil.Parents.FirstOrDefault().EMail;*/
                }
                if (pupil.Addresses.Count > 0)
                {
                    var regAddress = pupil.Addresses.FirstOrDefault();
                    RegAddress.IsBti = string.IsNullOrEmpty(regAddress.KladrCode);
                    RegAddress.Code = string.IsNullOrEmpty(regAddress.KladrCode)
                                          ? regAddress.BtiCode.Value.ToString()
                                          : regAddress.KladrCode;
                    RegAddress.Databind();
                    RegAddress.Index = regAddress.PostIndex;
                    RegAddress.Housing = regAddress.Housing;
                    RegAddress.NumberHouse = regAddress.HouseNumber;
                    RegAddress.Fraction = regAddress.Fraction;
                    RegAddress.Flat = regAddress.FlatNumber;
                    RegAddress.Building = regAddress.Building;
                }
                else
                {
                    RegAddress.Databind();
                }
            }
            upEditPupil.Update();
            //upPupilInfo.Update();
            popupEditPupilExtender.Show();
            gvPupilList.DataBind();
            upPupilList.Update();
        }
    }

    protected void lnkFindByName_OnClick(object sender, EventArgs e)
    {
        lblInfo.Text = "";
        gvPupilList.DataBind();
        upPupilList.Update();
    }

    protected void lnkImportPupils_OnClick(object sender, EventArgs e)
    {
        // загрузка из Excel
        popupLoadFileExtender.Show();
    }

    protected void lnkLoadFile_OnClick(object sender, EventArgs e)
    {
        // загрзка самих данных и их проверка
        //Загрузка файлов включить
        
        FileFormatType format;
        if (fileUpload.HasFile)
        {

            if (fileUpload.FileName.EndsWith(".xlsx"))
            {
                format = FileFormatType.Excel2007Xlsx;
            }
            else
            {
                format = FileFormatType.Excel2003;
            }

            ImportFromExcel(fileUpload.FileContent, format);
            
        }
        upLoadFile.Update();
        gvPupilList.DataBind();
        upPupilList.Update();
        popupLoadFileExtender.Hide();
    }

    protected List<ExtendedPupil> ImportFromExcel(Stream data, FileFormatType format)
    {
        List<ExtendedPupil> pupils = new List<ExtendedPupil>();
        Workbook workbook = new Workbook();
        try
        {
            workbook.Open(data, format);
            //workbook.LoadData(data);
        }
        catch (Exception)
        {
            throw new Exception("Данный формат файлов не поддерживается.");
        }
        using (KladrDb db = new KladrDb())
        {
            HttpContext.Current.Response.Clear();

            string filename = string.Format("PupilList.xls");

            HttpContext.Current.Response.AddHeader("content-disposition",
                                                    string.Format("attachment; filename={0}", filename));
            HttpContext.Current.Response.ContentType = "application/ms-excel";

            Worksheet sheet = workbook.Worksheets[0];
            // загрузка в список и вывод ошибок если что
            int row = 2;
            int d = sheet.Cells.Rows.Count;
            for (; row <= d; row++)
            {
                ExtendedImportedPupil pupil = new ExtendedImportedPupil();
                if (string.IsNullOrEmpty(sheet.Cells[1 + row, 2].StringValue) ||
                    string.IsNullOrEmpty(sheet.Cells[1 + row, 8].StringValue) ||
                    string.IsNullOrEmpty(sheet.Cells[1 + row, 22].StringValue)) { sheet.Cells[1 + row, 24].PutValue("Нет необходимых данных"); continue; }
                string childUnionName = sheet.Cells[1 + row, 21].StringValue;
                int numYear = sheet.Cells[1 + row, 22].IntValue;
                pupil.NumYear = numYear;
                
                pupil.LastName = sheet.Cells[1 + row, 1].StringValue;
                pupil.FirstName = sheet.Cells[1 + row, 2].StringValue;
                pupil.MiddleName = sheet.Cells[1 + row, 3].StringValue;
                string passporttype = sheet.Cells[1 + row, 6].StringValue;
                string number = sheet.Cells[1 + row, 7].StringValue;
                if (passporttype.Trim().ToLower() == "паспорт рф" || passporttype.Trim().ToLower() == "свидетельство о рождении рф")
                {
                    while (number.Length < 6)
                    {
                        number = "0" + number;
                    }
                }

                DateTime issueDate = DateTime.Now;
                try
                {
                    issueDate = Convert.ToDateTime(sheet.Cells[1 + row, 9].StringValue);
                }
                catch (Exception)
                {
                    sheet.Cells[1 + row, 24].PutValue("неверная дата паспорта");
                }

                pupil.Passport = new ExtendedPassport()
                                     {
                                         PassportType =
                                             new ExtendedPassportType()
                                                 {
                                                     PassportTypeId =
                                                         passporttype.Trim().ToLower() == "паспорт рф"
                                                             ? 2
                                                             : passporttype.Trim().ToLower() ==
                                                               "свидетельство о рождении рф"
                                                                   ? 3
                                                                   : 4
                                                 },
                                         Series = sheet.Cells[1 + row, 8].StringValue,
                                         Number = number,
                                         IssueDate = issueDate,
                                     };
                try
                {
                    pupil.Birthday = Convert.ToDateTime(sheet.Cells[1 + row, 4].StringValue);
                }
                catch (Exception)
                {
                    sheet.Cells[1 + row, 24].PutValue("неверная дата рождения");
                    continue;
                }
                
                pupil.Sex = sheet.Cells[1 + row, 5].StringValue.Trim().ToLower() == "м" ? 0 : 1;
                pupil.AddressStr = sheet.Cells[1 + row, 12].StringValue;

                ExtendedAddress address = new ExtendedAddress();
                address.Fraction = address.Housing = address.Building = address.FlatNumber = "";
                string region = sheet.Cells[1 + row, 13].StringValue.Trim().ToLower();
                string rayon = sheet.Cells[1 + row, 14].StringValue.Trim().ToLower();
                string city = sheet.Cells[1 + row, 15].StringValue.Trim().ToLower();
                string street = sheet.Cells[1 + row, 16].StringValue.Trim().ToLower();
                string house = sheet.Cells[1 + row, 17].StringValue.Trim().ToLower();
                string flat = sheet.Cells[1 + row, 18].StringValue.Trim().ToLower();

                string code = db.GetCodeAddress(region, rayon, city, street);
                if (!string.IsNullOrEmpty(code))
                {
                    if (code.Length > 10)
                    {
                        //кладр
                        address.KladrCode = code;
                        address.AddressStr = db.GetAddress(code, house, "", "", "", flat, null).AddressStr;
                    }
                    else
                    {
                        //БТИ
                        address.BtiCode = Convert.ToInt32(code);
                        address.AddressStr = db.GetAddress(code, house, "", "", "", flat, 0).AddressStr;
                    }
                    address.HouseNumber = house;
                    address.FlatNumber = flat;

                    pupil.Addresses = new List<ExtendedAddress>();
                    pupil.Addresses.Add(address);
                }
                pupil.PhoneNumber = sheet.Cells[1 + row, 10].StringValue;
                pupil.EMail = sheet.Cells[1 + row, 11].StringValue;
                pupil.SchoolName = sheet.Cells[1 + row, 19].StringValue;
                pupil.ClassName = sheet.Cells[1 + row, 20].StringValue;
                
                string typeBudget = sheet.Cells[1 + row, 23].StringValue;
                if (typeBudget.Trim().ToLower()=="платно")
                {
                    pupil.TypeBudget = new ExtendedTypeBudget()
                    {
                        DictTypeBudgetId = 1,
                        TypeBudgetName = "Платно"
                    };
                }
                else if (typeBudget.Trim().ToLower() == "бесплатно")
                {
                    pupil.TypeBudget = new ExtendedTypeBudget()
                    {
                        DictTypeBudgetId = 2,
                        TypeBudgetName = "Бесплатно"
                    };
                }
                else pupil.TypeBudget=new ExtendedTypeBudget();
                //pupils.Add(pupil);

                if (pupil.TypeBudget.DictTypeBudgetId==0)
                {
                    sheet.Cells[1 + row, 24].PutValue("не указан вид услуги");
                    continue;

                }

                long ChildUnionId = 0;
                using (ChildUnionDb db1 = new ChildUnionDb())
                {
                    var childUnion = db1.GetChildUnionsShort(null, UserContext.UdodId.Value, null, null, null, null,
                                                              null, null, null, childUnionName, null, null, null, null,
                                                              null, null,false).FirstOrDefault();
                    if (childUnion != null && childUnion.ChildUnionId != 0)
                        ChildUnionId = childUnion.ChildUnionId;
                }

                using (PupilsDb db1 = new PupilsDb())
                {

                    /*
                    db.ImportedAddPupil(PupilId, Guid.Empty, UserContext.UdodId.Value, PupilFio.LastName, PupilFio.FirstName,
                                                   PupilFio.MiddleName,
                                                   Convert.ToDateTime(tbBirthday.Text),
                                                   Convert.ToBoolean(cbSex.SelectedIndex), PupilDocument.PassportType,
                                                   PupilDocument.Series, PupilDocument.Number, PupilDocument.IssueDate.HasValue? PupilDocument.IssueDate.Value:DateTime.Now,
                                                   RegAddress.Code, "", RegAddress.NumberHouse,
                                                   RegAddress.Fraction, RegAddress.Housing, RegAddress.Building,
                                                   RegAddress.Flat, RegAddress.AddressTypeId, RegAddress.typeAddressCode, "",
                                                   0, 0, tbPupilEmail.Text, tbPupilPhoneNumber.Text, tbPupilSchool.Text, null, tbPupilClass.Text, tbChildUnionName.Text, numyear, Convert.ToInt32(ddlTypeBudgetFilter.SelectedValue), childUnionId, IsEdit);
                     */
                    //var pupilReestr = SiteUtility.GetPupilData(Page.MapPath(pupil.Passport.PassportType.PassportTypeId == 2 ? "../Templates/search_Passport_by_Series_and_Number_rq.xml" : "../Templates/search_by_Series_and_Number_rq.xml"), pupil.Passport.Series,
                    //             pupil.Passport.Number).FirstOrDefault(i => i.LastName.Trim().ToUpper() == pupil.LastName.Trim().ToUpper());

                    //if (!string.IsNullOrEmpty(pupilReestr.LastName))
                    //{
                    //    if (pupil.LastName.Trim().ToLower() != pupilReestr.LastName.Trim().ToLower() ||
                    //        pupil.FirstName.Trim().ToLower() != pupilReestr.FirstName.Trim().ToLower())
                    //    {
                    //        // ошибка. обратитесь в осип.
                    //        sheet.Cells[1 + row, 24].PutValue("Полученные из Реестра данные отличаются от введённых. Проверьте правильность заполнения и повторите запрос. Если Вы уверены в корректности введённых данных - обратитесь в ОСИП для разрешения ситуации по данному обучающемуся.");
                    //    }
                    //    else
                    //        if (ChildUnionId != 0)
                    //        {
                    //            if (string.IsNullOrEmpty(pupilReestr.ClassName)) pupilReestr.ClassName = pupil.ClassName;
                    //            if (string.IsNullOrEmpty(pupilReestr.SchoolName)) pupilReestr.ClassName = pupil.SchoolName;

                    //            db1.ImportedAddPupil(null, Guid.Empty, UserContext.UdodId.Value, pupilReestr.LastName,
                    //                                 pupilReestr.FirstName, pupilReestr.MiddleName, pupilReestr.Birthday,
                    //                                 Convert.ToBoolean(pupilReestr.Sex),
                    //                                 pupilReestr.Passport.PassportType.PassportTypeId, pupilReestr.Passport.Series,
                    //                                 pupilReestr.Passport.Number,
                    //                                 pupilReestr.Passport.IssueDate.HasValue
                    //                                     ? pupilReestr.Passport.IssueDate.Value
                    //                                     : DateTime.Now,
                    //                                 address.BtiCode.HasValue
                    //                                     ? address.BtiCode.Value.ToString()
                    //                                     : address.KladrCode, "", address.HouseNumber, address.Fraction,
                    //                                 address.Housing, address.Building, address.FlatNumber, 5,
                    //                                 address.BtiCode.HasValue ? 0 : 1, pupilReestr.AddressStr, 0, 0, pupilReestr.EMail,
                    //                                 pupilReestr.PhoneNumber, pupilReestr.SchoolName, null, pupilReestr.ClassName,
                    //                                 childUnionName, numYear,
                    //                                 pupil.TypeBudget.DictTypeBudgetId == 0
                    //                                     ? (int?)null
                    //                                     : pupil.TypeBudget.DictTypeBudgetId, ChildUnionId,
                    //                                 true);
                    //        }
                    //        else
                    //        {
                    //            sheet.Cells[1 + row, 24].PutValue("Не найдено ДО");
                    //        }
                    //}
                    //else
                    {
                        if (ChildUnionId != 0)
                        {
                            db1.ImportedAddPupil(null, Guid.Empty, UserContext.UdodId.Value, pupil.LastName,
                                                pupil.FirstName, pupil.MiddleName, pupil.Birthday,
                                                Convert.ToBoolean(pupil.Sex),
                                                pupil.Passport.PassportType.PassportTypeId, pupil.Passport.Series,
                                                pupil.Passport.Number,
                                                pupil.Passport.IssueDate.HasValue
                                                    ? pupil.Passport.IssueDate.Value
                                                    : DateTime.Now,
                                                address.BtiCode.HasValue
                                                    ? address.BtiCode.Value.ToString()
                                                    : address.KladrCode, "", address.HouseNumber, address.Fraction,
                                                address.Housing, address.Building, address.FlatNumber, 5,
                                                address.BtiCode.HasValue ? 0 : 1, pupil.AddressStr, 0, 0, pupil.EMail,
                                                pupil.PhoneNumber, pupil.SchoolName, null, pupil.ClassName,
                                                childUnionName, numYear,
                                                pupil.TypeBudget.DictTypeBudgetId == 0
                                                    ? (int?) null
                                                    : pupil.TypeBudget.DictTypeBudgetId, ChildUnionId,
                                                false);
                        }
                        else
                        {
                            sheet.Cells[1 + row, 24].PutValue("Не найдено ДО");
                        }
                    }

                }
            }
            workbook.Save("PupilList.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        }
        return pupils;
    }

    protected void selectUdod_OnChange(object sender, EventArgs e)
    {
        // перевод выбранных учеников в ДО
        popupChangeDOExtender.Hide();
        if (selectUdod.UdodAgeGroupId.HasValue)
        {
            using (PupilsDb db = new PupilsDb())
            {
                var list = db.GetImportedPupilsPaging(UserContext.UdodId.Value, "", "", "","", null, null, null, false, null, null);
                list = list.Where(i => i.Checked).ToList();
                int countZach = 0;
                foreach (var importedPupil in list)
                {
                    Guid result= db.ImportedAddPupilInAgegroup(importedPupil.UserId, selectUdod.UdodAgeGroupId.Value);
                    if (result != Guid.Empty) countZach++;
                }
                if (list.Count==countZach)
                lblInfo.Text = string.Format("Операция зачисления успешно завершена. Зачислено {0} из {1} записей.", countZach, list.Count);
                else lblInfo.Text = string.Format("Операция зачисления завершена частично. Зачислено {0} из {1} записей. Возникшие ошибки отображены в колонке 'Ошибки'. Устраните возникшие ошибки и повторите процедуру зачисления.", countZach, list.Count);

            }
        }
        gvPupilList.DataBind();
        upPupilList.Update();
    }

    protected void lnkAddPupil_OnClick(object sender, EventArgs e)
    {
        // Добавление ученика вручную

        #region Очистка полей
        PupilFio.Clear();
        PupilDocument.Clear();
        /*ParentFio.Clear();
        parentDocument.Clear();
        tbParentEmail.Text = "";
        tbParentPhoneNumber.Text = "";
        tbParentExpressPhoneNumber.Text = "";*/
        PupilDocument.DataBind();
        tbBirthday.Text = "";
        tbNumYear.Text = "";
        tbPupilClass.Text = "";
        tbPupilSchool.Text = "";
        tbPupilEmail.Text = "";
        tbPupilPhoneNumber.Text = "";
        tbChildUnionName.Text = "";
        RegAddress.Code = "";
        RegAddress.NumberHouse = "";
        RegAddress.Flat = "";
        IsEdit = null;
        lblErrorLoad.Text = "";
        lblAddressStr.Text = "";
        #endregion

        PupilId = null;
        PupilFio.Enabled =
                PupilDocument.Enabled =
                tbBirthday.Enabled =
                tbPupilSchool.Enabled =
                cbSex.Enabled =
                tbPupilClass.Enabled = true;
        tbBirthday.Enabled =
                tbPupilSchool.Enabled =
                tbPupilClass.Enabled =
                tbPupilPhoneNumber.Enabled= 
                tbPupilEmail.Enabled=
                cbSex.Enabled = false;

        btnSavePupil.Visible = false;
        upSavePupil.Update();
        RegAddress.Databind();
        upAddresses.Update();
        popupEditPupilExtender.Show();
        upPupilInfo.Update();
        //upParentInfo.Update();
    }

    protected void btnSavePupil_OnClick(object sender, EventArgs e)
    {
        try
        {
            // сохранение ученика при редактировании или создании
            using (PupilsDb db = new PupilsDb())
            {
                int? numyear;
                try
                {
                    numyear = Convert.ToInt32(ddlNumYearFilter.SelectedValue);
                }
                catch (Exception)
                {
                    if (PupilId.HasValue) numyear = null;
                    else
                    numyear = 2;
                }

                /*Guid parentId = db.ImportedAddParent(null, ParentFio.LastName, ParentFio.FirstName, ParentFio.MiddleName,
                                                     parentDocument.PassportType, parentDocument.Series,
                                                     parentDocument.Number,
                                                     parentDocument.IssueDate.HasValue
                                                         ? parentDocument.IssueDate.Value
                                                         : DateTime.Now, tbParentEmail.Text, tbParentPhoneNumber.Text);*/

                long? childUnionId = null;
                try
                {
                    childUnionId=Convert.ToInt32(ddlDO1.SelectedValue);
                }
                catch (Exception)
                {
                }
                int? dictTypeBudget = null;
                try
                {
                    dictTypeBudget = Convert.ToInt32(ddlTypeBudgetFilter.SelectedValue);
                }
                catch (Exception)
                {
                }
                if (!(!PupilId.HasValue && !childUnionId.HasValue))
                {
                    Guid pupilId = db.ImportedAddPupil(PupilId, Guid.Empty, UserContext.UdodId.Value, PupilFio.LastName,
                                                       PupilFio.FirstName,
                                                       PupilFio.MiddleName,
                                                       Convert.ToDateTime(tbBirthday.Text),
                                                       Convert.ToBoolean(cbSex.SelectedIndex),
                                                       PupilDocument.PassportType,
                                                       PupilDocument.Series, PupilDocument.Number,
                                                       PupilDocument.IssueDate.HasValue
                                                           ? PupilDocument.IssueDate.Value
                                                           : DateTime.Now,
                                                       RegAddress.Code, "", RegAddress.NumberHouse,
                                                       RegAddress.Fraction, RegAddress.Housing, RegAddress.Building,
                                                       RegAddress.Flat, RegAddress.AddressTypeId,
                                                       RegAddress.typeAddressCode, "",
                                                       0, 0, tbPupilEmail.Text, tbPupilPhoneNumber.Text,
                                                       tbPupilSchool.Text, null, tbPupilClass.Text,
                                                       tbChildUnionName.Text, numyear, dictTypeBudget
                                                       , childUnionId,
                                                       IsEdit);
                }
            }
            popupEditPupilExtender.Hide();
            gvPupilList.DataBind();
            upPupilList.Update();
        }
        catch (Exception)
        {
            popupErrorSaveExtender.Show();
            upPupilInfo.Update();
        }
    }

    protected void dsPupilList_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        if (cityUdodFilter.SelectedUdod.HasValue)
        {
            using (PupilsDb db = new PupilsDb())
            {
                int? numyear;
                try
                {
                    numyear = Convert.ToInt32(ddlNumYearFilter.Text);
                }
                catch (Exception)
                {
                    numyear = null;
                }
                int? dictTypeBudget;
                try
                {
                    dictTypeBudget=Convert.ToInt32(ddlTypeBudgetFilter.SelectedValue);
                }
                catch (Exception)
                {
                    dictTypeBudget = null;

                }
                long? childUnionId;
                try
                {
                    childUnionId = Convert.ToInt64(ddlDO1.SelectedValue);
                }
                catch (Exception)
                {
                    childUnionId = null;
                }

                pgrPupilList.numElements = db.GetImportedPupilsCount(cityUdodFilter.SelectedUdod.Value, 
                    tbPupilLastName.Text, tbPupilFirstName.Text, tbPupilMiddleName.Text, ddlDO.Text, numyear, childUnionId, dictTypeBudget, cbxShowDeleted.Checked, null, null);
            }
            //if (ddlDO.Items.Count == 0)
            {
                using (PupilsDb db = new PupilsDb())
                {
                    var list = db.ImportedGetChildUnions(UserContext.UdodId.Value);
                    ddlDO.DataSource = list;
                    ddlDO.DataBind();
                }
            }
            if (ddlNumYear.Items.Count == 0)
            {
                int[] numyears = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,13,14,15};
                ddlNumYear.DataSource = numyears;
                ddlNumYear.DataBind();
            }
            upFilters.Update();

            /*if (cbxShowDeleted.Checked)
            {
                gvPupilList.Columns[gvPupilList.Columns.Count - 1].Visible = false;
                gvPupilList.Columns[gvPupilList.Columns.Count - 2].Visible = false;
                gvPupilList.Columns[gvPupilList.Columns.Count - 3].Visible = false;
                gvPupilList.Columns[gvPupilList.Columns.Count - 4].Visible = true;
                gvPupilList.Columns[gvPupilList.Columns.Count - 5].Visible = false;
            }
            else
            {
                gvPupilList.Columns[gvPupilList.Columns.Count - 1].Visible = true;
                gvPupilList.Columns[gvPupilList.Columns.Count - 2].Visible = true;
                gvPupilList.Columns[gvPupilList.Columns.Count - 3].Visible = true;
                gvPupilList.Columns[gvPupilList.Columns.Count - 4].Visible = false;
                gvPupilList.Columns[gvPupilList.Columns.Count - 5].Visible = true;
            }*/
            upPupilList.Update();
        }
        else
        {
            e.Cancel = true;
        }
    }

    protected void lnkSelectDO_OnClick(object sender, EventArgs e)
    {
        // загрузить выбор ДО
        selectUdod.SelectButtonText = "<div class='btnBlue'>Зачислить</div>";
        popupChangeDOExtender.Show();
        gvPupilList.DataBind();
        upPupilList.Update();
        upChangeDO.Update();
    }

    protected void gvPupilList_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType==DataControlRowType.DataRow)
        {
            // проверка на различные ошибки
            var item = (e.Row.DataItem as ExtendedImportedPupil);
            if (item != null)
            {
                e.Row.Cells[9].Text = item.ErrorMessage;
                LinkButton btn = (LinkButton) e.Row.FindControl("lnkDeletePupil");
                //System.Web.UI.WebControls.CheckBox cbx = (System.Web.UI.WebControls.CheckBox)e.Row.FindControl("cbxSelect");

                btn.Attributes.Add("pupilId", item.UserId.ToString());
                /*cbx.Attributes.Add("pupilId", item.UserId.ToString());
                cbx.Checked = item.Checked;*/
                // проверка на документ в БД
                switch (item.Passport.PassportType.PassportTypeId)
                {
                    case 2:
                        {
                            // Проверка паспорта на соответствие формату
                            string message = "";
                            Regex regex = new Regex(@"\d{4}");
                            bool isRight = true;
                            if (!regex.IsMatch(item.Passport.Series) || item.Passport.Series.Length!=4)
                            {
                                message += "Серия паспорта не сооветствует формату.";
                                isRight = false;
                            }
                            regex = new Regex(@"\d{6}");
                            if (!regex.IsMatch(item.Passport.Number) || item.Passport.Number.Length != 6)
                            {
                                message += "Номер паспорта не сооветствует формату.";
                                isRight = false;
                            }
                            if (!isRight) e.Row.Cells[9].Text += "<div class='ImportedErrorMessage' title='" + message + "'> Ошибка формата паспорта;</div>";
                            break;
                        }
                    case 3:
                        {
                            // Проверка свидетельства о рождении на соответствие формату
                            string message = "";
                            Regex regex = new Regex(@"[IVXLCDMivxlcd]{1,3}-[А-Яа-я][А-Яа-я]");
                            bool isRight = true;
                            if (!regex.IsMatch(item.Passport.Series))
                            {
                                message += "Серия свидетельства не сооветствует формату. Формат серии свидетельства от 1 до 3 римских цифр IVXLCDM, затем тире и 2 буквы русского алфавита. Пример: IV-МЮ ";
                                isRight = false;
                            }
                            regex = new Regex(@"\d{6}");
                            if (!regex.IsMatch(item.Passport.Number) || item.Passport.Number.Length != 6)
                            {
                                message += "Номер свидетельства не сооветствует формату. Формат номера свидетельства 6 цифр.";
                                isRight = false;
                            }
                            if (!isRight) e.Row.Cells[9].Text += "<div class='ImportedErrorMessage' title='" + message + "'> Ошибка формата свидетельства;</div>";
                            break;
                        }
                    default: break;
                }
                using (UserDb db = new UserDb())
                {
                    if (db.CheckExistsingPassport(item.LastName, item.FirstName, item.Passport.PassportType.PassportTypeId, item.Passport.Series,item.Passport.Number, null))
                    {
                        /*var list = db.ImportedGetPupilUdods(item.Passport.PassportType.PassportTypeId,
                                                            item.Passport.Series,
                                                            item.Passport.Number);
                        if (list.Count > 0)
                        {
                            {
                                // готовить сообщение об ошибке
                                StringBuilder sb = new StringBuilder();
                                sb.Append("Данный документ существует у следующих пользователей ");
                                foreach (var pupilUdod in list)
                                {
                                    sb.Append(pupilUdod.LastName+" "+pupilUdod.FirstName+" "+pupilUdod.MiddleName +" "+ pupilUdod.UdodName+", номер телефона "+pupilUdod.UdodPhoneNumber);
                                }
                                sb.Append(". Просьба связатся с с указанным(-ыми) УДО, определиться с чьей стороны допущена ошибка и внести необходимые коррективы.");
                                string message = "<div class='ImportedErrorMessage' title='" + sb.ToString() + "'> Ошибка паспорта; </div>";
                                e.Row.Cells[9].Text += message;
                            }
                        }*/
                        var list = db.FindUdodByPassport(item.Passport.PassportType.PassportTypeId, item.Passport.Series, item.Passport.Number);
                        StringBuilder sb = new StringBuilder();
                        sb.Append("Введённые Вами данные документа удостоверяющего личность уже были использованы: \n");
                        foreach (ExtendedUserPassportMessage user in list)
                        {
                            if (user.ParentFio == "")
                            {
                                sb.Append("- в документе обучающегося " + user.ChildFio);
                            }
                            else
                            {
                                sb.Append("- в документе " + user.ParentFio + ", являющегося представителем " + user.ChildFio);
                            }

                            foreach (ExtendedUdodPassportMessage udod in user.Udods)
                            {
                                if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                {
                                    sb.Append(", в заявлении(-ях) №");

                                    foreach (var n in udod.Claims.Where(j => j.ClaimId != 0)) // Вывести список этих ClaimId
                                    {
                                        sb.Append(n.Number.ToString() + ", ");
                                    }
                                    sb.Remove(sb.Length - 2, 2);
                                }
                                if (udod.Claims.Count(j => j.ClaimId == 0) > 0)
                                {
                                    if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                    {
                                        sb.Append(", также являющимся обучающимся ");
                                    }
                                    else
                                    {
                                        sb.Append(", являющимся обучающимся ");
                                    }
                                }
                                sb.Append(" в учреждении " + udod.ShortName + ", телефон " + udod.PhoneNumber + ", ");
                                sb.Remove(sb.Length - 2, 2);
                            }
                            sb.Append(";\n");
                        }
                        sb.Remove(sb.Length - 2, 2);
                        sb.Append(". \nВам необходимо связаться с указанным(-ыми) УДО, определиться с чьей стороны допущена ошибка и внести необходимые коррективы.");
                        string message = "<div class='ImportedErrorMessage' title='" + sb.ToString() + "'> Ошибка паспорта; </div>";
                        e.Row.Cells[8].Text += message;
                    }
                }
                // Проверка на адрес
                if (item.Addresses.Count>0 && string.IsNullOrEmpty(item.Addresses.FirstOrDefault().AddressStr))
                {
                    string message = "<div class='ImportedErrorMessage' title='Некоторые поля детализированного адреса не соответствуют справочнику БТИ г.Москвы. Откройте запись на редактирование и устраните ошибку.'> Ошибка адреса; </div>";
                    e.Row.Cells[9].Text += message;
                }

                if (item.NumYear == 0)
                {
                    string message = "<div class='ImportedErrorMessage' title='Для успешной загрузки данных необходимо цифрой указать год обучения'> Не указан год обучения; </div>";
                    e.Row.Cells[9].Text += message;
                }
                if (item.NumYear == 1)
                {
                    string message = "<div class='ImportedErrorMessage' title='Зачисление на 1й год обучения не может быть осуществлено через загрузку данных из файла. Удалите запись из списка или откорректируйте год обучения.'> Ошибка года обучения; </div>";
                    e.Row.Cells[9].Text += message;
                }
                if (item.NumYear > 15)
                {
                    string message = "<div class='ImportedErrorMessage' title='Указанный год обучения превышает предельно допустимые в Системе значения. Откорректируйте год обучения.'> Ошибка года обучения; </div>";
                    e.Row.Cells[9].Text += message;
                }
                // проверка на существование вида услуг
                if (item.TypeBudget.DictTypeBudgetId==0)
                {
                    string message = "<div class='ImportedErrorMessage' title='Не указан или некорректно указан вид получаемой образовательной услуги (платно, бесплатно).'> Вид услуги; </div>";
                    e.Row.Cells[9].Text += message;
                }

                using (PupilsDb db = new PupilsDb())
                {
                    db.ImportedCheckPupil(item.UserId, e.Row.Cells[9].Text == item.ErrorMessage);
                }
                //cbx.Enabled = e.Row.Cells[7].Text==item.ErrorMessage;
                /*if (cbx.Enabled == false) cbx.Checked = false;*/
            }
        }
    }

    protected void lnkDeletePupil_OnClick(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton) sender;
        Guid userId = new Guid(btn.Attributes["pupilId"]);
        using (PupilsDb db = new PupilsDb())
        {
            db.DeleteImportedPupil(userId);
        }
        gvPupilList.DataBind();
        upPupilList.Update();
        
    }

    protected void cbxSelect_OnCheckedChanged(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.CheckBox cbx = (System.Web.UI.WebControls.CheckBox) sender;
        Guid userId = new Guid(cbx.Attributes["pupilId"]);
        using (PupilsDb db = new PupilsDb())
        {
            db.ImportedCheckPupil(userId, cbx.Checked);
        }
    }

    protected void ddlNumYear_OnDataBound(object sender, EventArgs e)
    {
        //ddlNumYear.Items.Insert(0, "");
    }

    protected void ddlDO_OnDataBound(object sender, EventArgs e)
    {
        ddlDO.Items.Insert(0, new ListItem("Все",""));
    }

    protected void ddlDO1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDO1.SelectedValue != "")
        {
            lnkZach.Visible = true;
            lnkAddPupil.Visible = true;
            using (ChildUnionDb db = new ChildUnionDb())
            {
                ExtendedChildUnion childunion = db.GetChildUnion(Convert.ToInt64(ddlDO1.SelectedValue), null);
                if (childunion != null)
                {
                    ddlTypeBudgetFilter.DataSource = childunion.BudgetTypes;
                    ddlTypeBudgetFilter.DataBind();

                    ddlNumYearFilter.Items.Clear();
                    for (int i = 2; i <= childunion.NumYears; i++)
                    {
                        ddlNumYearFilter.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }
                    var teachers = db.GetTeacher(childunion.ChildUnionId);
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Список педагогов: ");
                    foreach (var teacher in teachers)
                    {
                        sb.Append(teacher.TeacherName + "; ");
                    }
                    sb.Append("<br/>Возраст обучающихся: от " + childunion.AgeStart.ToString() + " до " +
                              childunion.AgeEnd +
                              "; ");
                    sb.Append("<br/>Адреса:");
                    foreach (var extendedAddress in childunion.Addresses)
                    {
                        sb.Append(extendedAddress.AddressStr + "; ");
                    }
                    lblChildUnionInfo.Text = sb.ToString();
                }
            }
        }
        else
        {
            lnkZach.Visible = false;
            lnkAddPupil.Visible = false;
            ddlTypeBudgetFilter.Items.Clear();
            ddlNumYearFilter.Items.Clear();
        }

        upFilters.Update();
        upLnkZach.Update();
        upLnk.Update();
    }

    protected void ddlTypeBudgetFilter_OnDataBound(object sender, EventArgs e)
    {
        //ddlTypeBudgetFilter.Items.Insert(0, new ListItem("Все",""));
    }

    protected void lnkZach_OnClick(object sender, EventArgs e)
    {
        // перевод выбранных учеников в ДО
        popupChangeDOExtender.Hide();
        //if (selectUdod.UdodAgeGroupId.HasValue)
        {
            using (PupilsDb db = new PupilsDb())
            {
                int? numyear;
                try
                {
                    numyear = Convert.ToInt32(ddlNumYearFilter.Text);
                }
                catch (Exception)
                {
                    numyear = null;
                }
                int? dictTypeBudget;
                try
                {
                    dictTypeBudget = Convert.ToInt32(ddlTypeBudgetFilter.SelectedValue);
                }
                catch (Exception)
                {
                    dictTypeBudget = null;

                }
                var list = db.GetImportedPupilsPaging(UserContext.UdodId.Value, "", "", "", "",numyear, Convert.ToInt64(ddlDO1.SelectedValue), dictTypeBudget, false, null, null);
                list = list.Where(i => i.Checked).ToList();
                int countZach = 0;
                foreach (var importedPupil in list)
                {
                    Guid result = db.ImportedAddPupilInAgegroup(importedPupil.UserId, null);
                    if (result != Guid.Empty)
                    {
                        countZach++;
                        EventsUtility.LogEvent(EnumEventTypes.SecondYearZach, UserContext.Profile.UserId,
                                               Request.UserHostAddress, string.Format("Зачислен, как обучавшийся до создания Системы"), result, null, UserContext.UdodId,
                                               importedPupil.ChildUnions[0].ChildUnionId);
                    }
                }
                if (list.Count == countZach)
                    lblInfo.Text = string.Format("Операция зачисления успешно завершена. Зачислено {0} из {1} записей.", countZach, list.Count);
                else lblInfo.Text = string.Format("Операция зачисления завершена частично. Зачислено {0} из {1} записей. Возникшие ошибки отображены в колонке 'Ошибки'. Устраните возникшие ошибки и повторите процедуру зачисления.", countZach, list.Count);

            }
        }
        gvPupilList.DataBind();
        upPupilList.Update();
    }

    protected void ddlDO1_OnDataBound(object sender, EventArgs e)
    {
        ddlDO1.Items.Insert(0, new ListItem("Все",""));
        ddlDO1_OnSelectedIndexChanged(sender, e);
    }

    protected void lnkLoadFromReestr_OnClick(object sender, EventArgs e)
    {
        //ExtendedPupil pupil = new ExtendedPupil();
        //try
        //{
        //    pupil = (SiteUtility.GetPupilData(Page.MapPath(PupilDocument.PassportType == 2 ? "../Templates/search_Passport_by_Series_and_Number_rq.xml" : "../Templates/search_by_Series_and_Number_rq.xml"), PupilDocument.Series,
        //                         PupilDocument.Number)).FirstOrDefault();
        //}
        //catch (Exception)
        //{
        //}

        //tbPupilPhoneNumber.Enabled =
        //    tbPupilEmail.Enabled = true;
        //lblErrorLoad.Text = "";
        //lblAddressStr.Text = string.IsNullOrEmpty(pupil.AddressStr) ? "В загруженных данных информация об адресе не введена. В связи с этим поля адреса необходимо заполнить вручную." : pupil.AddressStr;
        
        //if (string.IsNullOrEmpty(pupil.LastName))
        //{
        //    /*lblErrorLoad.Text =
        //        "По введённым параметрам в Реестре не найдено ни одной записи. Проверьте правильность заполнения и повторите запрос. Если Вы уверены в корректности введённых данных, то можете создать запись для данного обучающегося вручную. Создать запись в ручную?";*/
        //    popupErrorExtender.Show();
        //} else
        //if ((PupilFio.LastName.Trim().ToLower() != pupil.LastName.Trim().ToLower() || PupilFio.FirstName.Trim().ToLower() != pupil.FirstName.Trim().ToLower()))
        //{
        //    // Ошибка
        //    using (DictSchoolDb db = new DictSchoolDb())
        //    {
        //        var tmp = db.GetSchool(pupil.SchoolName);
        //        pupil.SchoolName = tmp.Name;
        //    }
        //    //lblErrorLoad.Text = "Полученные из Реестра данные отличаются от введённых. Проверьте правильность заполнения и повторите запрос. Если Вы уверены в корректности введённых данных - обратитесь в ОСИП для разрешения ситуации по данному обучающемуся, указав серию и номер документа и ОУ учащегося: "+pupil.SchoolName;
        //    lblErrorLoad.Text = "Полученные из Реестра данные отличаются от введённых. Проверьте правильность заполнения и повторите запрос. Если Вы уверены в корректности введённых данных - обратитесь в ОСИП для разрешения ситуации по данному обучающемуся.";
        //} 
        //else
        //{
        //    btnSavePupil.Visible = true;
        //    //PupilFio.LastName = pupil.LastName;
        //    //PupilFio.FirstName = pupil.FirstName;
        //    PupilFio.MiddleName = pupil.MiddleName;
        //    tbBirthday.Text = pupil.Birthday.ToShortDateString();
        //    tbPupilPhoneNumber.Text = pupil.PhoneNumber;
        //    tbPupilEmail.Text = pupil.EMail;
        //    tbPupilClass.Text = pupil.ClassName;
        //    cbSex.SelectedIndex = cbSex.Items.IndexOf(cbSex.Items.FindByValue(pupil.Sex.ToString()));

        //    string _url = Page.MapPath("../Templates/getUdodInfoByGuid.xml");
        //    bool isReadySchool = true;
        //    try
        //    {
        //        using (DictSchoolDb db = new DictSchoolDb())
        //        {
        //            var tmp = db.GetSchool(pupil.SchoolName);
        //            pupil.SchoolName = tmp.Name;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        isReadySchool = false;
        //    }

        //    if (!isReadySchool)
        //    {
        //        ExtendedUdod reestrData;
        //        reestrData = SiteUtility.GetUdodData(_url, pupil.SchoolName);
        //        pupil.SchoolName = reestrData.ShortName;

        //    }

        //    tbPupilSchool.Text = pupil.SchoolName;
        //    PupilDocument.IssueDate = pupil.Passport.IssueDate;

        //    if (pupil.Addresses[0].BtiCode!=null || !string.IsNullOrEmpty(pupil.Addresses[0].KladrCode))
        //    {
        //        var regAddress = pupil.Addresses[0];
        //        RegAddress.IsBti = string.IsNullOrEmpty(regAddress.KladrCode);
        //        if (RegAddress.IsBti)
        //            regAddress.BtiCode = (new KladrDb()).GetStreetCodeByHouseCode(regAddress.BtiCode.Value);
        //        RegAddress.Code = string.IsNullOrEmpty(regAddress.KladrCode) ? regAddress.BtiCode.Value.ToString() : regAddress.KladrCode;
        //        RegAddress.Databind();
        //        RegAddress.Index = regAddress.PostIndex;
        //        RegAddress.Housing = regAddress.Housing;
        //        RegAddress.NumberHouse = regAddress.HouseNumber;
        //        RegAddress.Fraction = regAddress.Fraction;
        //        RegAddress.Flat = regAddress.FlatNumber;
        //        RegAddress.Building = regAddress.Building;
        //    }
        //    else
        //    {
        //        lblAddressStr.Text += " Загруженная информация об адресе не может быть автоматически распознана. В связи с этим поля адреса необходимо заполнить вручную.";
        //    }
        //    PupilFio.Enabled =
        //        PupilDocument.Enabled =
        //        tbBirthday.Enabled =
        //        tbPupilSchool.Enabled =
        //        tbPupilClass.Enabled = 
        //        cbSex.Enabled= false;

        //    IsEdit = true;
        //}
        //upSavePupil.Update();
        //upEditPupil.Update();
        //upAddresses.Update();
    }

    protected void btnYesError_OnClick(object sender, EventArgs e)
    {
        PupilFio.Enabled = false;
        PupilDocument.Enabled = false;
        PupilFio.EnabledMiddleName = true;
        btnSavePupil.Visible = true;
        tbBirthday.Enabled =
                tbPupilSchool.Enabled =
                tbPupilClass.Enabled =
                cbSex.Enabled = true;
        upPupilInfo.Update();
        upSavePupil.Update();
        popupErrorExtender.Hide();
    }

    protected void btnNoError_OnClick(object sender, EventArgs e)
    {
        //popupLoadFileExtender.Hide();
    }

    protected void ddlNumYearFilter_OnDataBound(object sender, EventArgs e)
    {
        //ddlNumYearFilter.Items.Remove(ddlNumYearFilter.Items.FindByText("1"));
    }

    protected void ddlTeacher_OnDataBound(object sender, EventArgs e)
    {
        ddlTeacher.Items.Insert(0,new ListItem("Все", ""));
    }

    protected void ddlTeacher_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ddlDO1.DataBind();
        upCityUdodFilter.Update();
    }
}
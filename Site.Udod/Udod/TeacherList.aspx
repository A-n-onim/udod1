﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="TeacherList.aspx.cs" Inherits="Udod_TeacherList" %>
<%@ Register TagPrefix="uct" TagName="FIO" Src="~/UserControl/User/ucFIO_vertical.ascx" %>
<%@ Register src="../UserControl/Filter/ucCityUdodFilter.ascx" tagName="CityUdodFilter" tagPrefix="uct" %>
<%@ Register src="../UserControl/Menu/ucPager.ascx" tagName="Pager" tagPrefix="uc" %>

<asp:Content runat="server" ID="contentTeacherList" ContentPlaceHolderID="body">
<asp:UpdatePanel ID="upCityUdodFilter" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <uct:CityUdodFilter ID="cityUdodFilter" runat="server" UdodDdlEnabled="true" AddAllProperty="true" OnUdodChange="FilterDatabind" OnCityChange="FilterDatabind"/>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel runat="server" ID="upTeachers" UpdateMode="Conditional">
<ContentTemplate>
    <asp:LinkButton runat="server" ID="lnkAddTeacher" OnClick="lnkAddTeacher_OnClick"><div class="btnBlue">Добавить педагога</div>
    </asp:LinkButton>

    <asp:GridView runat="server" ID="gvTeacherList" CssClass="dgc" AutoGenerateColumns="false" DataKeyNames="UserId" DataSourceID="dsTeachers" 
    OnSelectedIndexChanged="gvTeacherList_OnSelectedIndexChanged" OnRowDataBound="gvTeacherList_OnRowDataBound" >
        <EmptyDataTemplate>
            <asp:Label ID="lblEmptyGvTeacherList" runat="server" style="font-size:16px">Педагоги не найдены</asp:Label>
        </EmptyDataTemplate>
        <Columns>
            <asp:TemplateField HeaderText="№" >
                <ItemTemplate><%# gvTeacherList.Rows.Count + pgrTeacherList.PagerIndex%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ФИО" >
                <ItemTemplate>
                    <%# Eval("Fio") %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowSelectButton="true" ButtonType="Link" SelectText="<div class='btnBlue'>Изменить</div>"/>
            <asp:TemplateField HeaderText="" >
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lnkDelete" OnClick="lnkDelete_OnClick"><div class='btnBlue'>Удалить</div></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <uc:Pager runat="server" ID="pgrTeacherList" needStartDots="false" needEndDots="false" pagesCount="10" OnChange="pgrTeacherList_OnChange"/>

    <asp:Panel runat="server" ID="popupShowMessage" style="display: none; border: 2px solid black" BackColor="White">
        <asp:Panel ID="Panel2" runat="server" >
            <asp:Label ID="Label1" Text="<span class='headerCaptions'>Удаление</span>" runat="server"/>
            <asp:LinkButton ID="LinkButton1" runat="server" 
                OnClientClick="$find('ShowMessagePopup').hide(); return false;" />
        </asp:Panel>
        <div>
            <asp:Label runat="server" ID="lblInfo"></asp:Label>
        </div>
        <div>
            <table>
                <tr>
                    <td>
                        <asp:LinkButton ID="btnOkMessage" runat="server" CausesValidation="false" OnClick="btnCancelMessage_OnClick"><div class="btnBlue">Отмена</div></asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnCancelMessage" runat="server" CausesValidation="false"><div class="btnBlue">Отмена</div></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupShowMessageExtender" PopupDragHandleControlID="pnlHeader"
        PopupControlID="popupShowMessage" CancelControlID="btnCancelMessage"
        TargetControlID="btnShow1" RepositionMode="None"
        BehaviorID="ShowMessagePopup"/>
    <asp:Button ID="btnShow1" runat="server" style="display:none" />

</ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger runat="server" ControlID="btnAddTeacher"/>
</Triggers>
</asp:UpdatePanel>

<asp:ObjectDataSource runat="server" ID="dsTeachers" TypeName="Udod.Dal.TeacherDb" SelectMethod="GetTeacherPaging" DeleteMethod="DeleteTeacherFromUdod"  OnSelected="ds_Selected">
<SelectParameters>
    <asp:ControlParameter Name="udodId" ConvertEmptyStringToNull="true" ControlID="cityUdodFilter" PropertyName="SelectedUdod" />
    <asp:ControlParameter Name="cityId" ConvertEmptyStringToNull="true" ControlID="cityUdodFilter" PropertyName="SelectedCity" />
    <asp:Parameter  Name="childUnionId" ConvertEmptyStringToNull="true" />
    <asp:ControlParameter Name="pagerIndex" ControlID="pgrTeacherList" ConvertEmptyStringToNull="true" PropertyName="PagerIndex" />
    <asp:ControlParameter Name="pagerLength" ControlID="pgrTeacherList" ConvertEmptyStringToNull="true" PropertyName="PagerLength" />
</SelectParameters>
<DeleteParameters>
    <asp:ProfileParameter Name="udodId" ConvertEmptyStringToNull="true" PropertyName="UdodId"/>
</DeleteParameters>
</asp:ObjectDataSource>

<asp:Panel runat="server" ID="popupAddTeacher" style="display: none; border: 2px solid black;width: 320px;" BackColor="White">
        <asp:Panel ID="pnlHeader" runat="server" >
            <asp:UpdatePanel runat="server" ID="upTitle" UpdateMode="Conditional">
            <ContentTemplate>
            <asp:Label ID="litTitle" Text="<span class='headerCaptions'>Редактирование педагога</span>" runat="server"/>
            </ContentTemplate>
            </asp:UpdatePanel>
            <asp:LinkButton ID="lnkClose" runat="server" 
                OnClientClick="$find('AddTeacherPopup').hide(); return false;" />
        </asp:Panel>
        <div style="width: 300px;">
            <asp:UpdatePanel runat="server" ID="up" UpdateMode="Conditional">
            <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <uct:FIO runat="server" ID="ucFio"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBoxList runat="server" ID="cblSection" DataTextField="Name" DataValueField="SectionId" RepeatColumns="3" />
                    </td>
                </tr>
            </table>
            </ContentTemplate>
            </asp:UpdatePanel>

        </div>
        <div >
            <table>
                <tr>
                    <td>
                        <asp:LinkButton ID="btnAddTeacher" runat="server" CausesValidation="false" OnClick="btnAddTeacher_OnClick"><div class='btnBlue'>Сохранить</div></asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false"><div class='btnBlue'>Отменить</div></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupAddExtender" PopupDragHandleControlID="pnlHeader"
        PopupControlID="popupAddTeacher" CancelControlID="btnCancel"
        TargetControlID="btnShowPopup" RepositionMode="None"
        BehaviorID="AddTeacherPopup"/>

    <asp:Button ID="btnShowPopup" runat="server" style="display:none" />

      
</asp:Content>

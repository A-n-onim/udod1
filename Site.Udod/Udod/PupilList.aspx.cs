﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Aspose.Cells;
using Udod.Dal;
using Udod.Dal.Enum;
using Label = System.Web.UI.WebControls.Label;
using TextBox = System.Web.UI.WebControls.TextBox;

public partial class Udod_PupilList : BasePage
{

    private Guid PupilId { get { return (Guid)ViewState["PupilId"]; } set { ViewState["PupilId"] = value; } }
    private long ChildUnionIdOld { get { return (long)ViewState["ChildUnionIdOld"]; } set { ViewState["ChildUnionIdOld"] = value; } }
    private int? DictTypeBudgetId
    {
        get { return (int?)ViewState["DictTypeBudgetId"]; }
        set { ViewState["DictTypeBudgetId"] = value; }
    }
    protected Int64? PupilReestrCode
    {
        set { ViewState["PupilReestrCode"] = value; }
        get
        {
            if (ViewState["PupilReestrCode"] == null) return null;
            return (Int64)ViewState["PupilReestrCode"];
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            btnSavePupil.Visible = !(UserContext.Roles.RoleId == (int)EnumRoles.Osip) && !(UserContext.Roles.RoleId == (int)EnumRoles.Department);
            upLoad.Visible = lnkTransfer.Visible = UserContext.Roles.IsAdministrator;
            /*foreach (RepeaterItem item in parentRepeater.Items)
            {
                using (UserDb db = new UserDb())
                {
                    (item.FindControl("cbRelative") as RadioButtonList).DataSource = db.GetUserTypes();
                    (item.FindControl("cbRelative") as RadioButtonList).DataBind();
                }
            }*/
            //popupEditPupil.Attributes.Add("isview", "false");
            tabEditPupil.Visible = false;
            //gvPupilList.Attributes.Add("isview", "false");
        }
        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsUdodEmployee || UserContext.Roles.IsOsip || UserContext.Roles.IsDepartment);
        lnkSendReestr.Visible= lnkUpdateReestrCode.Visible = UserContext.Roles.IsAdministrator;
        acSchool.ServicePath = SiteUtility.GetUrl("~/Webservices/Dict.asmx");
        if (!Page.IsPostBack)
        {
            if (UserContext.Roles.RoleId == (int)EnumRoles.Administrator)
            {
                gvPupilUdods.Columns[8].Visible = false;
                upPupilUdods.Update();
            }

            valrDate.MinimumValue = DateTime.Today.AddYears(-22).ToString().Substring(0, 10);
            valrDate.MaximumValue = DateTime.Today.AddYears(-3).ToString().Substring(0, 10);

            valrDate2.MaximumValue = DateTime.Today.ToString().Substring(0, 10);

            //valrDate3.MaximumValue = DateTime.Today.AddYears(-14).ToString().Substring(0, 10);
            /*using (PupilsDb db = new PupilsDb())
            {
                var list = db.GetPupils(UserContext.UdodId.Value, null);
                gvPupilList.DataSource = list;
                gvPupilList.DataBind();
            }*/

            //if (UserContext.CityId.HasValue)
            //    cityUdodFilter.SelectedCity = UserContext.CityId.Value;

            //if (UserContext.UdodId.HasValue)
            //    cityUdodFilter.SelectedUdod = UserContext.UdodId.Value;

            gvPupilList.Attributes.Add("isview", "false");

            //FiltersDatabind();
        }
    }

    protected void lnkDeleteParent_OnClick(object sender, EventArgs e)
    {
        using (PupilsDb db = new PupilsDb())
        {
            db.DeleteParent(new Guid((sender as LinkButton).Attributes["ParentId"]), PupilId);
            parentRepeater.DataSource = db.GetPupil(PupilId).Parents.Where(i => i.UserId != PupilId);
            parentRepeater.DataBind();
        }
    }

    protected void lnkMergePupilOk_OnClick(object sender, EventArgs e)
    {
        using (PupilsDb db = new PupilsDb())
        {
            List<Guid> list = db.FindSimilarPupil(PupilFio.LastName, PupilFio.FirstName, PupilDocument.PassportType, PupilDocument.Series, PupilDocument.Number, Convert.ToDateTime(tbBirthday.Text), PupilId);

            foreach (var id in list)
            {
                db.MergePupils(PupilId, id);
                PupilId = id;
            }
        }

        SavePupil();
        popupMergePupilExtender.Hide();
    }

    protected void btnSavePupil_OnClick(object sender, EventArgs e)
    {
        using (PupilsDb db = new PupilsDb())
        {
            List<Guid> list = db.FindSimilarPupil(PupilFio.LastName, PupilFio.FirstName, PupilDocument.PassportType, PupilDocument.Series, PupilDocument.Number, Convert.ToDateTime(tbBirthday.Text), PupilId);

            if (list.Count > 0)
            {
                popupMergePupilExtender.Show();
                return;
            }
        }

        SavePupil();
        PupilReestrCode = null;
    }

    protected void SavePupil()
    {
        lblPupilPassportDataError.Visible = false;

        using (PupilsDb db = new PupilsDb())
        {
            ExtendedPupil pupil = db.GetPupil(PupilId);

            using (UserDb db2 = new UserDb())
            {
                if (db2.CheckExistsingPassport(PupilFio.LastName, PupilFio.FirstName, PupilDocument.PassportType, PupilDocument.Series, PupilDocument.Number, PupilId))
                {
                    StringBuilder sb2 = new StringBuilder();
                    var list = db2.FindUdodByPassport(PupilDocument.PassportType, PupilDocument.Series, PupilDocument.Number);
                    sb2.Append("Введённые Вами данные документа удостоверяющего личность уже были использованы: <br/>");
                    foreach (ExtendedUserPassportMessage user in list)
                    {
                        if (user.ParentFio == "")
                        {
                            sb2.Append("- в документе обучающегося " + user.ChildFio);
                        }
                        else
                        {
                            sb2.Append("- в документе " + user.ParentFio + ", являющегося представителем " + user.ChildFio);
                        }

                        foreach (ExtendedUdodPassportMessage udod in user.Udods)
                        {
                            if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                            {
                                sb2.Append(", в заявлении(-ях) №");

                                foreach (var n in udod.Claims.Where(j => j.ClaimId != 0)) // Вывести список этих ClaimId
                                {
                                    sb2.Append(n.Number.ToString() + ", ");
                                }
                                sb2.Remove(sb2.Length - 2, 2);
                            }
                            if (udod.Claims.Count(j => j.ClaimId == 0) > 0)
                            {
                                if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                {
                                    sb2.Append(", также являющимся обучающимся по программам дополнительного образования ");
                                }
                                else
                                {
                                    sb2.Append(", являющимся обучающимся по программам дополнительного образования ");
                                }
                            }
                            sb2.Append(" в учреждении " + udod.ShortName + ", телефон " + udod.PhoneNumber + ", ");
                            sb2.Remove(sb2.Length - 2, 2);
                        }
                        sb2.Append(";<br/>");
                    }
                    sb2.Remove(sb2.Length - 6, 6);
                    sb2.Append(". <br/>Вам необходимо связаться с указанным(-ыми) УДОД, определиться с чьей стороны допущена ошибка и внести необходимые коррективы.");
                    lblPupilPassportDataError.Text = sb2.ToString();
                    lblPupilPassportDataError.Visible = true;
                    tabEditPupil.ActiveTabIndex = tabEditPupil.Tabs.IndexOf(pPupil);
                    upPupilInfo.Update();
                    upEditPupil.Update();
                    return;
                }
                    /*if (list.Where(i => ((i.ChildId != pupil.UserId) || (i.ParentFio != "")) ).Count() > 0) // есть ли записи с повтором паспорта кроме самого себя (своего PupilId)
                    {
                        if (list.Count(j => j.Claims.Count(k => k.ClaimId != 0) > 0) > 0) // есть ли записи, в списке Claim которых есть записи с ненулевым ClaimId (если есть - была заявка, если 0 - запись создана с помощью загрузки второго года)
                        {
                            sb2.Append("Введённые Вами данные документа удостоверяющего личность уже были использованы ");
                            foreach (var item in list.Where(j => j.Claims.Count(k => k.ClaimId != 0) > 0)) // для всех записей у которых в Claim указан хотя бы один ClaimId
                            {
                                if (item.ParentFio == "") // повтор паспорта обнаружился у обучающегося
                                {
                                    sb2.Append(" в документе обучающегося " + item.ChildFio);
                                }
                                else // повтор пасопрта обнаружился у родителя
                                {
                                    sb2.Append(" в документе " + item.ParentFio + ", являющегося представителем " + item.ChildFio);
                                }
                                sb2.Append(", в заявлении(-ях) №");
                                foreach (var n in item.Claims.Where(j => j.ClaimId != 0)) // Вывести список этих ClaimId
                                {
                                    sb2.Append(n.Number.ToString() + ", ");
                                }
                                sb2.Remove(sb2.Length - 2, 2);
                                sb2.Append(" поданного в учреждение " + item.ShortName + ", телефон " + item.PhoneNumber + ", ");
                            }
                            sb2.Remove(sb2.Length - 2, 2);
                            sb2.Append(" Вам необходимо связаться с указанным(-ыми) УДО, определиться с чьей стороны допущена ошибка и внести необходимые коррективы.");
                        }
                        else
                        {
                            sb2.Append("Данный документ существует у следующих пользователей: ");
                            foreach (var pupilUdod in list.Where(j => j.Claims.Count(k => k.ClaimId == 0) > 0)) // для всех записей у которых в Claim неизвестен ни один ClaimId
                            {
                                if (pupilUdod.ParentFio == "") // повтор паспорта обнаружился у обучающегося
                                {
                                    sb2.Append(" в документе обучающегося " + pupilUdod.ChildFio);
                                }
                                else // повтор пасопрта обнаружился у родителя
                                {
                                    sb2.Append(" в документе " + pupilUdod.ParentFio + ", являющегося представителем " + pupilUdod.ChildFio);
                                }
                                sb2.Append(", " + pupilUdod.ShortName + ", номер телефона УДО " + pupilUdod.PhoneNumber + ", ");
                            }
                            sb2.Remove(sb2.Length - 2, 2);
                            sb2.Append(". Просьба связатся с указанным(-ыми) УДО, определиться с чьей стороны допущена ошибка и внести необходимые коррективы.");
                        }
                        lblPupilPassportDataError.Text = sb2.ToString();
                        lblPupilPassportDataError.Visible = true;
                        tabEditPupil.ActiveTabIndex = tabEditPupil.Tabs.IndexOf(pPupil);
                        upPupilInfo.Update();
                        upEditPupil.Update();
                        return;
                    }
                }*/

                foreach (RepeaterItem item in parentRepeater.Items)
                {
                    string ln = (item.FindControl("ParentFio") as UserControl_User_ucFIO_vertical).LastName;
                    string fn = (item.FindControl("ParentFio") as UserControl_User_ucFIO_vertical).FirstName;
                    string mn = (item.FindControl("ParentFio") as UserControl_User_ucFIO_vertical).MiddleName;
                    string ps = (item.FindControl("parentDocument") as UserControl_User_ucDocument_Vertical).Series;
                    string pn = (item.FindControl("parentDocument") as UserControl_User_ucDocument_Vertical).Number;
                    int pt = (item.FindControl("parentDocument") as UserControl_User_ucDocument_Vertical).PassportType;
                    Guid pid = new Guid((item.FindControl("ParentFio") as UserControl_User_ucFIO_vertical).Attributes["ParentId"]);
                    //if (pid == PupilId) continue;

                    (item.FindControl("lblParentPassportDataError") as Label).Visible = false;
                    /*if (db2.CheckExistsingPassport(ln, fn, pt, ps, pn, pid))
                    {
                        StringBuilder sb2 = new StringBuilder();
                        var list = db2.FindUdodByPassport(pt, ps, pn);
                        sb2.Append("Введённые Вами данные документа удостоверяющего личность уже были использованы: <br/>");
                        foreach (ExtendedUserPassportMessage user in list)
                        {
                            if (user.ParentFio == "")
                            {
                                sb2.Append("- в документе обучающегося " + user.ChildFio);
                            }
                            else
                            {
                                sb2.Append("- в документе " + user.ParentFio + ", являющегося представителем " + user.ChildFio);
                            }

                            foreach (ExtendedUdodPassportMessage udod in user.Udods)
                            {
                                if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                {
                                    sb2.Append(", в заявлении(-ях) №");

                                    foreach (var n in udod.Claims.Where(j => j.ClaimId != 0)) // Вывести список этих ClaimId
                                    {
                                        sb2.Append(n.Number.ToString() + ", ");
                                    }
                                    sb2.Remove(sb2.Length - 2, 2);
                                }
                                if (udod.Claims.Count(j => j.ClaimId == 0) > 0)
                                {
                                    if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                    {
                                        sb2.Append(", также являющимся обучающимся ");
                                    }
                                    else
                                    {
                                        sb2.Append(", являющимся обучающимся ");
                                    }
                                }
                                sb2.Append(" в учреждении " + udod.ShortName + ", телефон " + udod.PhoneNumber + ", ");
                                sb2.Remove(sb2.Length - 2, 2);
                            }
                            sb2.Append(";<br/>");
                        }
                        sb2.Remove(sb2.Length - 6, 6);
                        sb2.Append(". <br/>Вам необходимо связаться с указанным(-ыми) УДО, определиться с чьей стороны допущена ошибка и внести необходимые коррективы.");
                        (item.FindControl("lblParentPassportDataError") as Label).Text = sb2.ToString();
                        (item.FindControl("lblParentPassportDataError") as Label).Visible = true;

                        tabEditPupil.ActiveTabIndex = tabEditPupil.Tabs.IndexOf(pParent);
                        upParentInfo.Update();
                        upEditPupil.Update();
                        return;
                    }*/
                }
            }

            bool isPupil = pupil.Parents.Where(i => i.UserId == PupilId).Count() > 0;
            ExtendedPupil oldPupil = db.GetPupil(PupilId);

            pupil.LastName = PupilFio.LastName;
            pupil.FirstName = PupilFio.FirstName;
            pupil.MiddleName = PupilFio.MiddleName;
            pupil.Birthday = Convert.ToDateTime(tbBirthday.Text);
            pupil.Sex = Convert.ToInt32(cbSex.SelectedValue);
            pupil.EMail = isPupil ? tbPupilEmail.Text : "";
            pupil.PhoneNumber = isPupil ? tbPupilPhoneNumber.Text : "";
            pupil.ExpressPhoneNumber = isPupil ? tbPupilExpressPhoneNumber.Text : "";
            pupil.Passport = new ExtendedPassport()
            {
                PassportType = new ExtendedPassportType()
                {
                    PassportTypeId = PupilDocument.PassportType
                },
                Series = PupilDocument.Series,
                Number = PupilDocument.Number,
                IssueDate = PupilDocument.IssueDate
            };
            pupil.SchoolName = tbPupilSchool.Text;
            pupil.ClassName = tbPupilClass.Text;
            pupil.ReestrCode = PupilReestrCode.HasValue?PupilReestrCode.Value.ToString():"";
            pupil.ReestrGuid = oldPupil.ReestrGuid;
            pupil.ReestrUdodCode = oldPupil.ReestrUdodCode;
            pupil.DictSchoolTypeId = Convert.ToInt32(ddlSchoolType.SelectedValue);
            db.UpdatePupil(PupilId, pupil);

            using(KladrDb db1=new KladrDb())
            {
                pupil.Addresses = db1.GetAddressesById(pupil.UserId, null, null,null);
            }

            try
            {
                #region Отправка в Реестр
                if (string.IsNullOrEmpty(pupil.ReestrCode) || pupil.ReestrCode == "0")
                {
                    Guid? reestrGuid = null;
                    //SiteUtility.ReestrAddPupil(pupil, !string.IsNullOrEmpty(pupil.ReestrUdodCode), ref  reestrGuid);
                }

                #endregion
            }
            catch (Exception)
            {
            }
            

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("(Идентификатор Ученика: {0})", PupilId);

            var isPupilEdited = false;
            if (oldPupil.Fio != pupil.Fio)
            {
                sb.AppendFormat("(Имя: {0} -> {1})", oldPupil.Fio, pupil.Fio);
                isPupilEdited = true;
            }
            if (oldPupil.Birthday != pupil.Birthday)
            {
                sb.AppendFormat("(Дата рождения: {0} -> {1})", oldPupil.Birthday, pupil.Birthday);
                isPupilEdited = true;
            }
            if (oldPupil.Passport.PassportType.PassportTypeId != pupil.Passport.PassportType.PassportTypeId)
            {
                sb.AppendFormat("(Пасспорт, тип: {0} -> {1})", oldPupil.Passport.PassportType.PassportTypeId, pupil.Passport.PassportType.PassportTypeId);
                isPupilEdited = true;
            }
            if (oldPupil.Passport.Series != pupil.Passport.Series)
            {
                sb.AppendFormat("(Пасспорт, серия: {0} -> {1})", oldPupil.Passport.Series, pupil.Passport.Series);
                isPupilEdited = true;
            }
            if (oldPupil.Passport.Number != pupil.Passport.Number)
            {
                sb.AppendFormat("(Пасспорт, номер: {0} -> {1})", oldPupil.Passport.Number, pupil.Passport.Number);
                isPupilEdited = true;
            }
            if (oldPupil.Passport.IssueDate != pupil.Passport.IssueDate)
            {
                sb.AppendFormat("(Пасспорт, дата выдачи: {0} -> {1})", oldPupil.Passport.IssueDate, pupil.Passport.IssueDate);
                isPupilEdited = true;
            }
            if (oldPupil.Sex != pupil.Sex)
            {
                sb.AppendFormat("(Пол: {0} -> {1})", oldPupil.Sex, pupil.Sex);
                isPupilEdited = true;
            }
            if (oldPupil.EMail != pupil.EMail)
            {
                sb.AppendFormat("(EMail: {0} -> {1})", oldPupil.EMail, pupil.EMail);
                isPupilEdited = true;
            }
            if (oldPupil.PhoneNumber != pupil.PhoneNumber)
            {
                sb.AppendFormat("(Номер телефона 1: {0} -> {1})", oldPupil.PhoneNumber, pupil.PhoneNumber);
                isPupilEdited = true;
            }
            if (oldPupil.ExpressPhoneNumber != pupil.ExpressPhoneNumber)
            {
                sb.AppendFormat("(Номер телефона 2: {0} -> {1})", oldPupil.ExpressPhoneNumber, pupil.ExpressPhoneNumber);
                isPupilEdited = true;
            }
            if (oldPupil.SchoolName != pupil.SchoolName)
            {
                sb.AppendFormat("(Школа: {0} -> {1})", oldPupil.SchoolName, pupil.SchoolName);
                isPupilEdited = true;
            }
            if (oldPupil.ClassName != pupil.ClassName)
            {
                sb.AppendFormat("(Класс: {0} -> {1})", oldPupil.ClassName, pupil.ClassName);
                isPupilEdited = true;
            }

            foreach (RepeaterItem item in parentRepeater.Items)
            {
                ExtendedUser parent = new ExtendedUser();
                parent.LastName = (item.FindControl("ParentFio") as UserControl_User_ucFIO_vertical).LastName;
                parent.FirstName = (item.FindControl("ParentFio") as UserControl_User_ucFIO_vertical).FirstName;
                parent.MiddleName = (item.FindControl("ParentFio") as UserControl_User_ucFIO_vertical).MiddleName;
                parent.PhoneNumber = (item.FindControl("tbParentPhoneNumber") as TextBox).Text;
                parent.ExpressPhoneNumber = (item.FindControl("tbParentExpressPhoneNumber") as TextBox).Text;
                parent.UserId = new Guid((item.FindControl("ParentFio") as UserControl_User_ucFIO_vertical).Attributes["ParentId"]);

                if (isPupilEdited && (parent.UserId == PupilId)) // не обновлять самого обучающегося если его уже обновили только что в его вкладке
                    continue;

                parent.EMail = (item.FindControl("tbParentEmail") as TextBox).Text;

                parent.Passport = new ExtendedPassport()
                {
                    PassportType = new ExtendedPassportType()
                    {
                        PassportTypeId = (item.FindControl("parentDocument") as UserControl_User_ucDocument_Vertical).PassportType
                    },
                    Series = (item.FindControl("parentDocument") as UserControl_User_ucDocument_Vertical).Series,
                    Number = (item.FindControl("parentDocument") as UserControl_User_ucDocument_Vertical).Number,
                    IssueDate = (item.FindControl("parentDocument") as UserControl_User_ucDocument_Vertical).IssueDate
                };
                RadioButtonList rbl = (item.FindControl("cbRelative") as RadioButtonList);
                parent.UserType = Convert.ToInt32(rbl.SelectedValue);

                db.UpdateParent(parent);

                var oldParent = oldPupil.Parents.FirstOrDefault(i => i.UserId == parent.UserId);
                sb.AppendFormat("(Идентификатор Заявителя: {0})", parent.UserId);
                if (oldParent.UserType != parent.UserType)
                    sb.AppendFormat("(Заявитель: {0} -> {1})", rbl.Items.FindByValue(oldParent.UserType.ToString()).Text, rbl.Items.FindByValue(parent.UserType.ToString()).Text);
                if (oldParent.Fio != parent.Fio)
                    sb.AppendFormat("(Имя заявителя: {0} -> {1})", oldParent.Fio, parent.Fio);
                if (oldParent.Passport.PassportType.PassportTypeId != parent.Passport.PassportType.PassportTypeId)
                    sb.AppendFormat("(Паспорт заявителя, тип: {0} -> {1})", oldParent.Passport.PassportType.PassportTypeId, parent.Passport.PassportType.PassportTypeId);
                if (oldParent.Passport.Series != parent.Passport.Series)
                    sb.AppendFormat("(Паспорт заявителя, серия: {0} -> {1})", oldParent.Passport.Series, parent.Passport.Series);
                if (oldParent.Passport.Number != parent.Passport.Number)
                    sb.AppendFormat("(Паспорт заявителя, номер: {0} -> {1})", oldParent.Passport.Number, parent.Passport.Number);
                if (oldParent.Passport.IssueDate != parent.Passport.IssueDate)
                    sb.AppendFormat("(Паспорт заявителя, дата выдачи: {0} -> {1})", oldParent.Passport.IssueDate, parent.Passport.IssueDate);
                if (oldParent.PhoneNumber != parent.PhoneNumber)
                    sb.AppendFormat("(Номер телефона заявителя 1: {0} -> {1})", oldParent.PhoneNumber, parent.PhoneNumber);
                if (oldParent.ExpressPhoneNumber != parent.ExpressPhoneNumber)
                    sb.AppendFormat("(Номер телефона заявителя 2: {0} -> {1})", oldParent.ExpressPhoneNumber, parent.ExpressPhoneNumber);
            }
            EventsUtility.LogPupilUpdate(UserContext.Profile.UserId, Request.UserHostAddress, sb.ToString());
            /*using (UserDb db2= new UserDb())
            {
                db2.SaveProfile(PupilId, PupilId.ToString(), pupil.LastName, pupil.FirstName, pupil.MiddleName, pupil.Birthday, isPupil ? tbPupilEmail.Text : tbParentEmail.Text, pupil.PhoneNumber, pupil.ExpressPhoneNumber, tbPupilSchool.Text, tbPupilClass.Text);
            }*/

            try
            {
                using (KladrDb db2 = new KladrDb())
                {
                    // добавление адреса пользователю
                    // если адреса совпадают то вместо фактического адреса записываем регистрации но с другим AddressTypeId
                    /*if (rbnFacRegAddress.SelectedValue == "True")
                        db2.AddAddress(RegAddress.Code, RegAddress.Index, RegAddress.NumberHouse, RegAddress.Fraction,
                                      RegAddress.Housing, RegAddress.Building, RegAddress.Flat, FactAddress.AddressTypeId, 0,
                                      0, PupilId, null, string.Empty, string.Empty, string.Empty, RegAddress.typeAddressCode);
                    else
                        db2.AddAddress(FactAddress.Code, FactAddress.Index, FactAddress.NumberHouse, FactAddress.Fraction,
                                      FactAddress.Housing, FactAddress.Building, FactAddress.Flat, FactAddress.AddressTypeId,
                                      0, 0, PupilId, null, string.Empty, string.Empty, string.Empty, FactAddress.typeAddressCode);
                    //lblFactAddress.Text = FactAddress.Value.AddressStr;*/

                    db2.AddAddress(RegAddress.Code, RegAddress.Index, RegAddress.NumberHouse, RegAddress.Fraction,
                                  RegAddress.Housing, RegAddress.Building, RegAddress.Flat, RegAddress.AddressTypeId, 0, 0,
                                  PupilId, null, string.Empty, string.Empty, string.Empty, RegAddress.typeAddressCode, string.Empty);
                    //lblRegAddress.Text = RegAddress.Value.AddressStr;
                }
            }
            catch
            {
                tabEditPupil.ActiveTabIndex = tabEditPupil.Tabs.IndexOf(pAddress);
                upEditPupil.Update();
                return;
            }
        }

        // если удалось все нормально сохранить то сообщение об ошибках стирается
        using (UserDb db = new UserDb())
        {
            db.UpdatePersonalDeed(PupilId, "");
        }

        PupilListDatabind();
        //upEditPupil.Update(); - бывший up (updatepanel окна редактирования обучающегося)
        popupEditPupilExtender.Hide();
        gvPupilList.Attributes.Add("isview", "false");
        tabEditPupil.Visible = false;
        upEditPupil.Update();
    }

    protected void rbnFacRegAddress_SelectedChanged(object sender, EventArgs e)
    {
        //FactAddressPanel.Enabled = FactAddress.Enabled = !Convert.ToBoolean(rbnFacRegAddress.SelectedValue);
        upAddresses.Update();
    }

    protected void gvPupilUdods_OnSelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void gvPupilHistory_OnSelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void gvPupilUdods_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ExtendedChildUnion rowdata = (ExtendedChildUnion)(e.Row.DataItem);
            LinkButton btn = (LinkButton)e.Row.FindControl("lnkEditUdod");
            e.Row.Attributes.Add("ChildUnionId", rowdata.ChildUnionId.ToString());
            if ((!UserContext.UdodId.HasValue) || (rowdata.UdodId != UserContext.UdodId.Value) || (UserContext.Roles.RoleId == (int)EnumRoles.Osip || UserContext.Roles.RoleId == (int)EnumRoles.Department))
            {
                btn.Text = "<img width='20px' height='20px' src='../Images/details.png' border=0 title='Просмотр'>";
                btn = (LinkButton)e.Row.FindControl("lnkDeleteUdod");
                btn.Visible = false;
            }
            e.Row.Attributes.Add("NumYear", rowdata.NumYears.ToString());
            //btn = (LinkButton)e.Row.FindControl("lnkCreateClaim");
        }
    }

    protected void gvPupilHistory_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ExtendedEvent rowdata = (ExtendedEvent)(e.Row.DataItem);
            LinkButton btn = (LinkButton)e.Row.FindControl("lnkOpenClaim");
            LinkButton btn2 = (LinkButton)e.Row.FindControl("lnkCreateClaim");
            if (rowdata.Claim.ClaimId != 0)
            {
                e.Row.Attributes.Add("ClaimId", rowdata.Claim.ClaimId.ToString());
            }
            else
            {
                btn.Visible = false;
                btn2.Visible = false;
            }
        }
    }

    protected void lnkEditUdod_OnClick(object sender, EventArgs e)
    {
        GridViewRow row = (GridViewRow)(((LinkButton)sender).Parent.Parent);
        if (((LinkButton)sender).Text == "<img width='20px' height='20px' src='../Images/details.png' border=0 title='Просмотр'>")
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                selectDO.SetDOData(db.GetChildUnion(Convert.ToInt64(row.Attributes["ChildUnionId"]), null));
                upPassportDO.Update();
                popupPassportDOExtender.Show();
            }
        }
        else
        {
            ChildUnionIdOld = Convert.ToInt64(row.Attributes["ChildUnionId"]);
            using (ChildUnionDb db = new ChildUnionDb())
            {
                ddlChangeDO_DO.DataSource = db.GetChildUnionsShort(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null, null, null, null, null, null, null, null, null, null, null, null, null, false).OrderBy(i => i.Name);
            }
            ddlChangeDO_DO.DataBind();
            upChangeDO.Update();
            popupChangeDOExtender.Show();
        }
    }

    protected void gvPupilList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        PupilReestrCode = null;
        lblErrorLoad.Text = "";
        lblPupilPassportDataError.Visible = false;
        PupilDocument.Clear();
        lblAddressStr.Text = "";
        // загрузка ученика и показ popup
        PupilId = (Guid)gvPupilList.SelectedDataKey.Value;
        using (PupilsDb db = new PupilsDb())
        {
            var pupil = db.GetPupil(PupilId);
            if (pupil != null)
            {
                try
                {
                    PupilReestrCode = Convert.ToInt64(pupil.ReestrCode);
                }
                catch(Exception)
                {
                    PupilReestrCode = null;
                }
                PupilFio.LastName = pupil.LastName;
                PupilFio.FirstName = pupil.FirstName;
                PupilFio.MiddleName = pupil.MiddleName;
                tbBirthday.Text = pupil.Birthday.ToShortDateString();
                cbSex.SelectedIndex = cbSex.Items.IndexOf(cbSex.Items.FindByValue(pupil.Sex.ToString()));
                if (pupil.Parents.Where(i => i.UserId == PupilId).Count() > 0)
                {
                    var tmp = pupil.Parents.Where(i => i.UserId == PupilId).FirstOrDefault();
                    tbPupilEmail.Text = tmp.EMail;
                    tbPupilPhoneNumber.Text = tmp.PhoneNumber;
                    tbPupilExpressPhoneNumber.Text = tmp.ExpressPhoneNumber;
                    tr1.Visible = tr2.Visible = tr3.Visible = true;
                }
                else
                    tr1.Visible = tr2.Visible = tr3.Visible = false;

                tbPupilPhoneNumber.Text = pupil.PhoneNumber;
                tbPupilExpressPhoneNumber.Text = pupil.ExpressPhoneNumber;
                tbPupilSchool.Text = pupil.SchoolName;
                tbPupilClass.Text = pupil.ClassName;
                if (!string.IsNullOrEmpty(pupil.Passport.Series))
                {
                    PupilDocument.Series = pupil.Passport.Series;
                    PupilDocument.Number = pupil.Passport.Number;
                    PupilDocument.IssueDate = pupil.Passport.IssueDate;
                    PupilDocument.PassportType = pupil.Passport.PassportType.PassportTypeId;
                }

                parentRepeater.DataSource = null;
                parentRepeater.DataSourceID = null;
                parentRepeater.DataBind();

                if (pupil.Parents.Where(i => i.UserId != PupilId).Count() > 0)
                {
                    parentRepeater.DataSource = pupil.Parents;//.Where(i => i.UserId != PupilId); обучающийся также показывается во вкладке представитель
                    parentRepeater.DataBind();
                }

                pParent.Visible = pupil.Parents.Where(i => i.UserId != PupilId).Count() > 0; ;
                //valrDate3.Enabled = pupil.Parents.Where(i => i.UserId != PupilId).Count() == 0; ;
                //ddlSchoolType.Enabled = !pupil.DictSchoolId.HasValue;
                ddlSchoolType.DataBind();
                ddlSchoolType.SelectedIndex =
                    ddlSchoolType.Items.IndexOf(ddlSchoolType.Items.FindByValue(pupil.DictSchoolTypeId.ToString()));
                lblSchoolType.Enabled = !pupil.DictSchoolId.HasValue;
                ddlSchoolType.Enabled = ddlSchoolType.SelectedValue != "1";
            }

            gvPupilUdods.DataSource = db.GetChildUnion(PupilId);
            gvPupilUdods.DataBind();
            var limit = db.GetLimit(PupilId, true); // true - только зачисленные
            lblUdodInfo.Text = limit.CountUdod.ToString();
            lblChildUnionInfo.Text = limit.CountChildUnion.ToString();
            lblHoursInfo.Text = pupil.Hours.ToString("F2");
            lblNormHoursInfo.Text = limit.NormHours.ToString("F2");

            HistoryDatabind();
        }

        try
        {
            using (KladrDb db = new KladrDb())
            {
                List<ExtendedAddress> addresses = db.GetAddressesById(PupilId, null, null, null);
                /*var factAddress = addresses.Where(i => i.AddressType.AddressTypeId == 6).FirstOrDefault();
                FactAddress.IsBti = string.IsNullOrEmpty(factAddress.KladrCode);
                FactAddress.Code = string.IsNullOrEmpty(factAddress.KladrCode) ? factAddress.BtiCode.Value.ToString() : factAddress.KladrCode;
                FactAddress.Databind();
                FactAddress.Index = factAddress.PostIndex;
                FactAddress.Housing = factAddress.Housing;
                FactAddress.NumberHouse = factAddress.HouseNumber;
                FactAddress.Fraction = factAddress.Fraction;
                FactAddress.Flat = factAddress.FlatNumber;
                FactAddress.Building = factAddress.Building;
                */
                var regAddress = addresses.Where(i => i.AddressType.AddressTypeId == 5).FirstOrDefault();
                RegAddress.IsBti = string.IsNullOrEmpty(regAddress.KladrCode);
                RegAddress.Code = string.IsNullOrEmpty(regAddress.KladrCode) ? regAddress.BtiCode.HasValue?regAddress.BtiCode.Value.ToString():"0" : regAddress.KladrCode;
                RegAddress.Databind();
                RegAddress.Index = regAddress.PostIndex;
                RegAddress.Housing = regAddress.Housing;
                RegAddress.NumberHouse = regAddress.HouseNumber;
                RegAddress.Fraction = regAddress.Fraction;
                RegAddress.Flat = regAddress.FlatNumber;
                RegAddress.Building = regAddress.Building;

                /*bool isEqual = true;
                if (FactAddress.Code != RegAddress.Code) isEqual = false;
                if (FactAddress.Index != RegAddress.Index) isEqual = false;
                if (FactAddress.Housing != RegAddress.Housing) isEqual = false;
                if (FactAddress.NumberHouse != RegAddress.NumberHouse) isEqual = false;
                if (FactAddress.Fraction != RegAddress.Fraction) isEqual = false;
                if (FactAddress.Flat != RegAddress.Flat) isEqual = false;
                if (FactAddress.Building != RegAddress.Building) isEqual = false;

                rbnFacRegAddress.SelectedIndex = rbnFacRegAddress.Items.IndexOf(rbnFacRegAddress.Items.FindByValue(isEqual.ToString()));
                FactAddressPanel.Enabled = FactAddress.Enabled = !(rbnFacRegAddress.SelectedValue == "True");*/

            }
        }
        catch
        {
        }
        //tabEditPupil.ActiveTabIndex = -1;
        popupEditPupilExtender.Show();
        gvPupilList.Attributes.Add("isview", "true");
        tabEditPupil.Visible = true;
        upEditPupil.Update();
        upPupilInfo.Update();

        //popupEditPupil.Style["visibility"] = "visible";
        //popupEditPupil.Style["display"] = "block";
        popupEditPupil.Style["visibility"] = "visible";
        popupEditPupil.Style["display"] = "block";
    }

    protected void lnkChangeDOOk_OnClick(object sender, EventArgs e)
    {
        if (ddlChangeDO_DO.SelectedValue != "")
        {

            UpdateChildUnion(Convert.ToInt64(ddlChangeDO_DO.SelectedValue),
                            Convert.ToInt64(ddlGroups.SelectedValue),
                            Convert.ToInt32(ddlChangeDO_NY.SelectedValue));
            upPupilUdods.Update();
            using (PupilsDb db = new PupilsDb())
            {
                var limit = db.GetLimit(PupilId, true); // true - только зачисленные
                lblUdodInfo.Text = limit.CountUdod.ToString();
                lblChildUnionInfo.Text = limit.CountChildUnion.ToString();
                lblHoursInfo.Text = limit.Hours.ToString("F2");
                lblNormHoursInfo.Text = limit.NormHours.ToString("F2");

                HistoryDatabind();
                upPupilInfo.Update();
            }
            popupChangeDOExtender.Hide();
        }
    }

    protected void lnkChangeDOCancel_OnClick(object sender, EventArgs e)
    {
        popupChangeDOExtender.Hide();
    }

    private void UpdateChildUnion(long ChildUnionIdNew, long groupId, int numYear)
    {
            using (ChildUnionDb db2 = new ChildUnionDb())
            {
                using (PupilsDb db = new PupilsDb())
                {
                    var PupilChildUnionList = db.GetPupilsUdod(PupilId, ChildUnionIdOld);
                    //if (typeBudgetId == 2)
                    //{
                    /*ExtendedChildUnion childunion = db2.GetChildUnion(ChildUnionIdNew, null);
                    // проверка на 20 часов и на 3 удод
                    ExtendedLimit limit;
                    limit = db.GetLimit(PupilId, null);
                    */
                    DateTime now = DateTime.Today;
                    int fullYears = now.Year - Convert.ToDateTime(tbBirthday.Text).Year;
                    if (Convert.ToDateTime(tbBirthday.Text) > now.AddYears(-fullYears)) fullYears--;
                    /*
                    using (AgeGroupDb db3 = new AgeGroupDb())
                    {
                        var list = db3.GetAgeGroups(null, null, null, childunion.ChildUnionId);
                        int minYear = list.Select(i => i.Age).Min();
                        int maxYear = list.Select(i => i.Age).Max();
                        if (fullYears < minYear) fullYears = minYear;
                        if (fullYears > maxYear) fullYears = maxYear;
                        ExtendedAgeGroup item = list.Where(i => i.Age == fullYears).FirstOrDefault();
                        ExtendedAgeGroup itemOld = db3.GetPupilAgeGroup(PupilId, ChildUnionIdOld);

                        if (limit.Hours - itemOld.LessonLength * itemOld.LessonsInWeek / 60.0 + item.LessonLength * item.LessonsInWeek / 60.0 > limit.NormHours)
                        {
                            errorPopup.ShowError("Внимание", "Невозможно перевести обучающегося. Количество часов в неделю будет превышать допустимые нормы.", "", "ОК", false, true);
                            return;
                        }
                    }*/
                    //}

                    if (ChildUnionIdNew != ChildUnionIdOld)
                    {
                        if (!db.GetIsDismissed(null, PupilId, ChildUnionIdNew))
                        {
                            errorPopup.ShowError("Ошибка", "Данный обучающийся уже зачислен в выбранное ДО", "", "OK",
                                                 false, true);
                            return;
                        }
                    }
                    bool isEnd = false;
                    using (AgeGroupDb db3 = new AgeGroupDb())
                    {
                        var group = db3.GetGroup(Convert.ToInt64(ddlGroups.SelectedValue));
                        isEnd = group.MaxCountPupil - (group.CurrentCountPupil + 1) <= 0;
                        if (!(fullYears >= group.AgeStart && fullYears <= group.AgeEnd))
                        {
                            errorPopup.ShowError("Ошибка", "Возраст ребенка не подходит к данной группе", "", "ОК",
                                                 false,
                                                 true);
                            return;
                        }
                        
                        int result = db.UpdateChildUnion(PupilId, ChildUnionIdOld, ChildUnionIdNew, groupId, numYear);
                        if (result == -1)
                        {
                            errorPopup.ShowError("Внимание.",
                                                 "В выбранное ДО на данный Год Обучения обучающийся не подходит по возрасту. Выберите другое ДО или измените Год Обучения.",
                                                 "", "ОК", false, true);
                            return;
                        }
                        //if (isEnd)
                        //{
                        //    //ExportInAsGuf tmp = new ExportInAsGuf();
                        //    //tmp.UpdateSchedule(ChildUnionIdNew, group.UdodAgeGroupId,
                        //    //                   group.StartClaim.HasValue
                        //    //                       ? group.StartClaim.Value
                        //    //                       : new DateTime(2013, 09, 01),
                        //    //                   group.EndClaim.HasValue
                        //    //                       ? group.EndClaim.Value
                        //    //                       : new DateTime(2014, 05, 31));
                        //}
                    }
                    gvPupilUdods.DataSource = db.GetChildUnion(PupilId);
                    gvPupilUdods.DataBind();

                    var oldDO1 = db2.GetChildUnion(ChildUnionIdOld, null);
                    var newDO = db2.GetChildUnion(ChildUnionIdNew, null);
                    string oldName = oldDO1.Name;
                    string newName = newDO.Name;

                    if (ChildUnionIdOld != ChildUnionIdNew)
                    {

                        // отправка в реестр
                        //var childunion = db2.GetChildUnion(Convert.ToInt64(ddlDOZach.SelectedValue), null);
                        long result = -1;
                        if (string.IsNullOrEmpty(newDO.ReestrCode))
                        {
                            var teachers = db2.GetTeacher(newDO.ChildUnionId);

                            //result = SiteUtility.ReestrAddDO(newDO, false, teachers);
                        }
                        else
                        {
                            result = Convert.ToInt64(newDO.ReestrCode);
                        }

                        // переводим
                        foreach (ExtendedPupil extendedPupil in PupilChildUnionList)
                        {
                            //SiteUtility.ReestrDeletePupilZach(extendedPupil.ChildUnions[0].ReestrPupilAgeGroupId);
                        }


                        ExtendedPupil pupil = null;
                        pupil = db.GetPupil(PupilId);

                        if (pupil != null)
                        {
                            // добавление или изменение
                            if (string.IsNullOrEmpty(pupil.ReestrCode) || pupil.ReestrCode == "0")
                            {
                                using (KladrDb db3 = new KladrDb())
                                {
                                    var addresses = db3.GetAddressesById(pupil.UserId, null, null, null);
                                    pupil.Addresses = addresses;
                                }
                                Guid? reestrGuid = null;
                                //string pupilReestrUdodCode = SiteUtility.ReestrAddPupil(pupil,
                                //                                                        !string.IsNullOrEmpty(
                                //                                                            pupil.
                                //                                                                ReestrUdodCode),
                                //                                                        ref reestrGuid);
                                //if (reestrGuid.HasValue)
                                //{
                                //    // проверка в основном контингенте по Guid
                                //    var pupils =
                                //        SiteUtility.GetPupilData(
                                //            Page.MapPath("../search_by_ReestrGuid_rq.xml"),
                                //            reestrGuid.Value.ToString());
                                //    if (pupils.Count > 0)
                                //    {
                                //        // удалить из реестра контингента Удод
                                //        SiteUtility.ReestrDeletePupil(string.IsNullOrEmpty(pupil.ReestrUdodCode)
                                //                                          ? pupilReestrUdodCode
                                //                                          : pupil.ReestrUdodCode);
                                //    }
                                //}
                                if (reestrGuid.HasValue)
                                {
                                    pupil.ReestrGuid = reestrGuid;
                                }
                            }


                            try
                            {
                                //errorClaimList.ShowError("Проверка", result.ToString(), "ок", "cancel", true,false);



                                if (result != -1)
                                {
                                    newDO.ReestrCode = result.ToString();
                                    //отправляем зачисление
                                    //SiteUtility.ReestrAddZach(extendedPupil, childunion, extendedChildUnion, false);
                                    var childunionZach =
                                        db.GetChildUnion(pupil.UserId).Where(
                                            i => i.ChildUnionId == newDO.ChildUnionId).FirstOrDefault();
                                    //if (childunionZach != null)
                                        //SiteUtility.ReestrAddZach(pupil, newDO, childunionZach, false);
                                }

                            }
                            catch (Exception e1)
                            {

                            }

                        }

                    }

                    // изменение ДО
                    EventsUtility.LogPupilUdodUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                                                     "перевод из ДО " + oldName + " в ДО " + newName, PupilId,
                                                     UserContext.UdodId.Value, ChildUnionIdNew);
                    HistoryDatabind();
                }
            }
        
    }

    protected void ddlBudgetType_OnDataBound(object sender, EventArgs e)
    {
        ddlBudgetType.Items.Insert(0, new ListItem("Все", ""));
    }

    protected void ddlBudgetType_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        pgrPupilList.Clear();
        PupilListDatabind();
    }

    protected void ddlPupilNumYear_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        pgrPupilList.Clear();
        PupilListDatabind();
    }

    protected void ddlDO_OnDataBound(object sender, EventArgs e)
    {
        if (ddlDO.Items.Count == 0)
        {
            ddlDO.Items.Insert(0, new ListItem("Выберите УДО", ""));
        }
        else
        {
            ddlDO.Items.Insert(0, new ListItem("Все", ""));
        }
    }

    protected void ddlDO_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlDO.SelectedValue))
        {
            using (AgeGroupDb db = new AgeGroupDb())
            {
                ddlGroupsFilter.DataSource = db.GetGroupsPaging(UserContext.UdodId, null,
                                                                Convert.ToInt64(ddlDO.SelectedValue), null, null, null,
                                                                "", 1, 50000, null);
                ddlGroupsFilter.DataBind();
            }
        }
        else
        {
            ddlGroupsFilter.DataSource = new List<ExtendedGroup>();
            ddlGroupsFilter.DataBind();
        }
        pgrPupilList.Clear();
        PupilListDatabind();
    }

    protected void lnkDeleteUdod_OnClick(object sender, EventArgs e)
    {
        GridViewRow row = (GridViewRow)(((LinkButton)sender).Parent.Parent);
        ChildUnionIdOld = Convert.ToInt64(row.Attributes["ChildUnionId"]);
        tbReason.Text = "";
        upDeleteUdod.Update();
        popupDeleteUdodExtender.Show();
    }

    protected void btnDeleteUdod_OnClick(object sender, EventArgs e)
    {
        using (PupilsDb db = new PupilsDb())
        {
            // получить группу и проверить ее
            using (AgeGroupDb db1 = new AgeGroupDb())
            {
                var group = db1.GetGroup(ChildUnionIdOld, PupilId);
                var deletedlist = db.DeletePupil(PupilId, ChildUnionIdOld, string.IsNullOrEmpty(tbReason.Text) ? "" : tbReason.Text);
                gvPupilUdods.DataSource = db.GetChildUnion(PupilId);
                gvPupilUdods.DataBind();

                // отправка отчисления в реестр
                foreach (long l in deletedlist)
                {
                    //SiteUtility.ReestrDeletePupil(l.ToString());
                }
                

                if (group != null && group.MaxCountPupil <= group.CurrentCountPupil)
                {
                    //SiteUtility.SendBatchScheduleESZ(ChildUnionIdOld, 3, new List<ExtendedGroup>(){group});
                }
            }

            

            // отчисление из до
            EventsUtility.LogPupilUdodDelete(UserContext.Profile.UserId, Request.UserHostAddress, tbReason.Text, PupilId, UserContext.UdodId.Value, ChildUnionIdOld);
            HistoryDatabind();
        }
        upPupilUdods.Update();
        PupilListDatabind();
        popupDeleteUdodExtender.Hide();
    }

    protected void cityUdodFilter_onLoadComplete(object sender, EventArgs e)
    {
        FiltersDatabind();
    }

    protected void cityUdodFilter_OnUdodChange(object sender, EventArgs e)
    {
        FiltersDatabind();
    }

    protected void FiltersDatabind()
    {
        if (cityUdodFilter.SelectedUdod.HasValue)
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                ddlDO.DataSource = db.GetChildUnionsShort(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null, null, null, null, null, null, null, null, null, null, null, null, null,false).OrderBy(i => i.Name);
            }
            using (TeacherDb db = new TeacherDb())
            {
                ddlTeacher.DataSource = db.GetTeacher(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null);
            }
        }
        else
        {
            ddlDO.DataSource = new ListItemCollection();
            ddlTeacher.DataSource = new ListItemCollection();
        }
        ddlDO.DataBind();
        ddlTeacher.DataBind();
        upCityUdodFilter.Update();
        upFilters.Update();
        pgrPupilList.Clear();
        PupilListDatabind();
    }

    protected void btnCancelPupil_OnClick(object sender, EventArgs e)
    {
        PupilListDatabind();
        //upEditPupil.Update(); - бывший up (updatepanel окна редактирования обучающегося)
        popupChangeDOExtender.Hide();
        popupDeleteUdodExtender.Hide();
        popupEditPupilExtender.Hide();
        gvPupilList.Attributes.Add("isview", "false");
        tabEditPupil.Visible = false;
        upEditPupil.Update();

    }

    protected void lnkFindByName_OnClick(object sender, EventArgs e)
    {
        pgrPupilList.Clear();
        PupilListDatabind();
    }

    protected void ds_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        using (PupilsDb db = new PupilsDb())
        {
            long? childUnionId;
            try
            {
                childUnionId = Convert.ToInt64(ddlDO.SelectedValue);
            }
            catch
            {
                childUnionId = null;
            }

            Guid? teacherId;
            try
            {
                teacherId = new Guid(ddlTeacher.SelectedValue);
            }
            catch
            {
                teacherId = null;
            }

            int message;
            try
            {
                message = Convert.ToInt32(ddlMessage.SelectedValue);
            }
            catch
            {
                message = 0;
            }

            int? pupilNumYear;
            try
            {
                pupilNumYear = Convert.ToInt32(ddlPupilNumYear.SelectedValue);
            }
            catch
            {
                pupilNumYear = null;
            }

            int? budgetTypeId;
            try
            {
                budgetTypeId = Convert.ToInt32(ddlBudgetType.SelectedValue);
            }
            catch
            {
                budgetTypeId = null;
            }

            int? groupId=null;
            try
            {
                groupId = Convert.ToInt32(ddlGroupsFilter.SelectedValue);
            }
            catch
            {
            }

            pgrPupilList.numElements = db.GetPupilsCount(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, childUnionId,
                tbPupilLastName.Text, tbPupilFirstName.Text, tbPupilMiddleName.Text, teacherId, message, pupilNumYear, budgetTypeId, cbxShowDelete.Checked, groupId);
        }
    }

    protected void pgrPupilList_OnChange(object sender, EventArgs e)
    {
        PupilListDatabind();
    }

    protected void ddlTeacher_OnDataBound(object sender, EventArgs e)
    {
        if (ddlTeacher.Items.Count == 0)
        {
            ddlTeacher.Items.Insert(0, new ListItem("Выберите УДО", ""));
        }
        else
        {
            ddlTeacher.Items.Insert(0, new ListItem("Все", ""));
        }
    }

    protected void ddlTeacher_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        pgrPupilList.Clear();
        PupilListDatabind();
    }

    protected void PupilListDatabind()
    {
        //Stopwatch sw = new Stopwatch();
        //sw.Start();
        gvPupilList.DataBind();
        upPupilList.Update();
        //sw.Stop();
        //long i = sw.ElapsedMilliseconds;
    }

    protected void lnkClear_OnClick(object sender, EventArgs e)
    {
        ddlDO.SelectedIndex = 0;
        ddlGroupsFilter.SelectedIndex = 0;
        ddlTeacher.SelectedIndex = 0;
        tbPupilLastName.Text = "";
        tbPupilFirstName.Text = "";
        tbPupilMiddleName.Text = "";
        PupilListDatabind();
    }

    protected void OpenClaim_OnClick(object sender, EventArgs e)
    {
        UserContext.ClaimId = Convert.ToInt64((((LinkButton)sender).Parent.Parent as GridViewRow).Attributes["ClaimId"]);
        Session["isNewClaimFromOld"] = false;
        Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
    }

    protected void CreateClaim_OnClick(object sender, EventArgs e)
    {
        UserContext.ClaimId = Convert.ToInt64((((LinkButton)sender).Parent.Parent as GridViewRow).Attributes["ClaimId"]);
        Session["isNewClaimFromOld"] = true;
        Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
    }

    protected void HistoryDatabind()
    {
        using (EventsDb db = new EventsDb())
        {
            gvPupilHistory.DataSource = db.GetEvents(null, new DateTime(2012, 8, 1), null, PupilId, null, null);
            gvPupilHistory.DataBind();
        }
        upPupilHistory.Update();
    }

    protected void lnkExportInExcel_OnClick(object sender, EventArgs e)
    {
        // экспорт в Excel и выдача файла
        HttpContext.Current.Response.Clear();

        string filename = string.Format("PupilList.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                                string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];
        #region Стили
        Aspose.Cells.Style style_area = workbook.Styles[0];
        Aspose.Cells.Style style_header = workbook.Styles[1];
        style_area.ShrinkToFit = true;

        style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
        style_header.Font.IsBold = true;
        style_header.Font.Size = 12;
        style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
        style_header.BackgroundColor = System.Drawing.Color.Gray;
        style_header.Pattern = Aspose.Cells.BackgroundType.Solid;
        #endregion
        if (cityUdodFilter.SelectedUdod.HasValue)
        {
            using (PupilsDb db = new PupilsDb())
            {
                long? childUnionId = null;
                try
                {
                    childUnionId = Convert.ToInt32(ddlDO.SelectedValue);
                }
                catch (Exception)
                {
                    childUnionId = null;
                }
                Guid? teacherId;
                try
                {
                    if (string.IsNullOrEmpty(ddlTeacher.SelectedValue)) throw new Exception();
                    teacherId = new Guid(ddlTeacher.SelectedValue);
                }
                catch (Exception)
                {
                    teacherId = null;
                }
                /*int count = db.GetPupilsCount(cityUdodFilter.SelectedUdod.Value, childUnionId ,
                                              tbPupilLastName.Text, tbPupilFirstName.Text, tbPupilMiddleName.Text,
                                              teacherId);*/
                var list = db.GetPupils(cityUdodFilter.SelectedUdod.Value, childUnionId,
                                              tbPupilLastName.Text, tbPupilFirstName.Text, tbPupilMiddleName.Text, teacherId);
                sheet.Cells[0, 0].PutValue("ФИО");
                sheet.Cells[0, 1].PutValue("Детские объединения");
                sheet.Cells[0, 2].PutValue("Адрес");
                sheet.Cells[0, 3].PutValue("День рождения");
                int i = 1;
                foreach (var extendedPupil in list)
                {
                    sheet.Cells[i, 0].PutValue(extendedPupil.Fio);
                    if (extendedPupil.ChildUnions.Count > 0)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (var childunion in extendedPupil.ChildUnions)
                        {
                            sb.Append(childunion.Name + ";");
                        }
                        sheet.Cells[i, 1].PutValue(sb.ToString());
                    }
                    sheet.Cells[i, 2].PutValue(extendedPupil.AddressStr);
                    sheet.Cells[i, 3].PutValue(extendedPupil.Birthday.ToShortDateString());
                    i++;
                }
                sheet.AutoFitColumns();
                sheet.Cells.CreateRange(1, 0, i, 4).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
                sheet.Cells.CreateRange(0, 0, 1, 4).ApplyStyle(style_header, new Aspose.Cells.StyleFlag() { All = true });


            }
        }

        workbook.Save("PupilList.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }

    protected void lnkExportByChildrens_OnClick(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("PupilListByChildrens.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                                string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
        workbook.Open(MapPath("../Templates/ExportPupilsByChildrens.xlsx"));
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];
        #region Стили
        Aspose.Cells.Style style_area = sheet.Cells["A2"].GetStyle();
        Aspose.Cells.Style style_header = sheet.Cells["A1"].GetStyle();

        style_header.Pattern = Aspose.Cells.BackgroundType.Solid;
        style_header.ForegroundColor = System.Drawing.Color.Gray;

        #endregion
        if (cityUdodFilter.SelectedUdod.HasValue)
        {
            List<ExtendedPupil> list = null;
            using (PupilsDb db = new PupilsDb())
            {
                long? childUnionId = null;
                try
                {
                    childUnionId = Convert.ToInt32(ddlDO.SelectedValue);
                }
                catch (Exception)
                {
                    childUnionId = null;
                }
                Guid? teacherId;
                try
                {
                    if (string.IsNullOrEmpty(ddlTeacher.SelectedValue)) throw new Exception();
                    teacherId = new Guid(ddlTeacher.SelectedValue);
                }
                catch (Exception)
                {
                    teacherId = null;
                }
                int? numYear = null;
                try
                {
                    numYear = Convert.ToInt32(ddlPupilNumYear.SelectedValue);
                }
                catch (Exception)
                {

                }
                int? typeBudgetId = null;
                try
                {
                    typeBudgetId = Convert.ToInt32(ddlBudgetType.SelectedValue);
                }
                catch (Exception)
                {

                }
                long? groupId = null;
                try
                {
                    groupId = Convert.ToInt32(ddlGroupsFilter.SelectedValue);
                }
                catch (Exception)
                {

                }
                list = db.GetPupilsToExport(cityUdodFilter.SelectedUdod.Value, childUnionId,
                                              tbPupilLastName.Text, tbPupilFirstName.Text, tbPupilMiddleName.Text, teacherId, numYear, typeBudgetId, cbxShowDelete.Checked,groupId);

                int i = 1;
                foreach (ExtendedPupil extendedPupil in list)
                {
                    sheet.Cells[i, 0].PutValue(i);
                    sheet.Cells[i, 1].PutValue(extendedPupil.LastName);
                    sheet.Cells[i, 2].PutValue(extendedPupil.FirstName);
                    sheet.Cells[i, 3].PutValue(extendedPupil.MiddleName);
                    sheet.Cells[i, 4].PutValue(extendedPupil.Birthday.ToShortDateString());
                    sheet.Cells[i, 5].PutValue(extendedPupil.Sex == 0 ? "М" : "Ж");
                    sheet.Cells[i, 6].PutValue(extendedPupil.Addresses.Where(a => a.AddressType.AddressTypeId == 5).FirstOrDefault().KladrCode == "" ? "Москва" : "Московская область");
                    //sheet.Cells[i, 6].PutValue(extendedPupil.AddressStr.Substring(0, 9) == "г. Москва" ? "Москва" : "Московская область");
                    sheet.Cells[i, 7].PutValue(extendedPupil.AddressStr);
                    sheet.Cells[i, 8].PutValue(extendedPupil.Parents.Select(i1=>i1.PhoneNumber).Where(i1=>!string.IsNullOrEmpty(i1)).FirstOrDefault());
                    string strFio = "";
                    MyStringBuilder my = new MyStringBuilder(", ", extendedPupil.Parents.Select(i1 => i1.Fio).ToList());
                    
                    sheet.Cells[i, 9].PutValue(my.ToString());
                    sheet.Cells[i, 10].PutValue(extendedPupil.SchoolName);
                    sheet.Cells[i, 11].PutValue(extendedPupil.ClassName);


                    sheet.Cells[i, 13].PutValue(extendedPupil.ChildUnions.Select(j => j.UdodId).Distinct().Count());
                    sheet.Cells[i, 14].PutValue(extendedPupil.ChildUnions.Select(j => j.ChildUnionId).Distinct().Count());
                    sheet.Cells[i, 15].PutValue(extendedPupil.ChildUnions.Where(j => j.PupilBudgetType.TypeBudgetName == "Бесплатно").Select(j => j.PupilHours).Sum());
                    sheet.Cells[i, 16].PutValue(extendedPupil.ChildUnions.Where(j => j.PupilBudgetType.TypeBudgetName == "Платно").Select(j => j.PupilHours).Sum());

                    if (extendedPupil.ChildUnions.Count > 0)
                    {
                        MyStringBuilder sb = new MyStringBuilder(";", extendedPupil.ChildUnions.Select(i1=>i1.Name));
                        sheet.Cells[i, 17].PutValue(sb.ToString());
                    }
                    i++;
                }
                sheet.Cells.CreateRange(0, 0, 1, 18).ApplyStyle(style_header, new Aspose.Cells.StyleFlag() { All = true });
                sheet.Cells.CreateRange(1, 0, i - 1, 18).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
                sheet.AutoFitRows();

            }
        }

        workbook.Save("PupilListByChildrens.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }

    protected void lnkExportByChildUnions_OnClick(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("PupilListByChildUnions.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                                string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
        workbook.Open(MapPath("../Templates/ExportPupilsByChildUnions.xlsx"));
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];
        #region Стили
        Aspose.Cells.Style style_area = sheet.Cells["A2"].GetStyle();
        Aspose.Cells.Style style_header = sheet.Cells["A1"].GetStyle();

        style_header.Pattern = Aspose.Cells.BackgroundType.Solid;
        style_header.ForegroundColor = System.Drawing.Color.Gray;

        #endregion
        if (cityUdodFilter.SelectedUdod.HasValue)
        {
            List<ExtendedPupil> list = null;
            using (PupilsDb db = new PupilsDb())
            {
                long? childUnionId = null;
                try
                {
                    childUnionId = Convert.ToInt32(ddlDO.SelectedValue);
                }
                catch (Exception)
                {
                    childUnionId = null;
                }
                Guid? teacherId;
                try
                {
                    if (string.IsNullOrEmpty(ddlTeacher.SelectedValue)) throw new Exception();
                    teacherId = new Guid(ddlTeacher.SelectedValue);
                }
                catch (Exception)
                {
                    teacherId = null;
                }
                int? numYear = null;
                try
                {
                    numYear = Convert.ToInt32(ddlPupilNumYear.SelectedValue);
                }
                catch (Exception)
                {
                    
                }
                int? typeBudgetId = null;
                try
                {
                    typeBudgetId = Convert.ToInt32(ddlBudgetType.SelectedValue);
                }
                catch (Exception)
                {

                }
                long? groupId = null;
                try
                {
                    groupId = Convert.ToInt64(ddlGroupsFilter.SelectedValue);
                }
                catch (Exception)
                {

                }
                list = db.GetPupilsToExport(cityUdodFilter.SelectedUdod.Value, childUnionId,
                                              tbPupilLastName.Text, tbPupilFirstName.Text, tbPupilMiddleName.Text, teacherId, numYear, typeBudgetId, cbxShowDelete.Checked, groupId);
                // если отчёт с "показывать отчисленных", то ..ю
                if (!cbxShowDelete.Checked)
                {
                    sheet.Cells.HideColumn(17); // Скрываем колонку "дата отчисления" 
                }

                int i = 1;
                foreach (ExtendedPupil extendedPupil in list)
                {
                    //var cus = db.GetChildUnion(extendedPupil.UserId);
                    foreach (ExtendedChildUnion childUnion in extendedPupil.ChildUnions)
                    {
                        sheet.Cells[i, 0].PutValue(i);
                        sheet.Cells[i, 1].PutValue(extendedPupil.LastName);
                        sheet.Cells[i, 2].PutValue(extendedPupil.FirstName);
                        sheet.Cells[i, 3].PutValue(extendedPupil.MiddleName);
                        sheet.Cells[i, 4].PutValue(extendedPupil.Birthday.ToShortDateString());
                        sheet.Cells[i, 5].PutValue(extendedPupil.Sex == 0 ? "М" : "Ж");
                        sheet.Cells[i, 6].PutValue(
                            extendedPupil.Addresses.Where(a => a.AddressType.AddressTypeId == 5).FirstOrDefault().
                                KladrCode == ""
                                ? "Москва"
                                : "Московская область");
                        //sheet.Cells[i, 6].PutValue(extendedPupil.AddressStr.Substring(0, 9) == "г. Москва" ? "Москва" : "Московская область");
                        sheet.Cells[i, 7].PutValue(extendedPupil.AddressStr);
                        sheet.Cells[i, 8].PutValue(
                            extendedPupil.Parents.Select(i1 => i1.PhoneNumber).Where(i1 => !string.IsNullOrEmpty(i1)).
                                FirstOrDefault());
                        MyStringBuilder my = new MyStringBuilder(", ",
                                                                 extendedPupil.Parents.Select(i1 => i1.Fio).ToList());
                        sheet.Cells[i, 9].PutValue(my.ToString());
                        sheet.Cells[i, 10].PutValue(extendedPupil.SchoolName);

                        sheet.Cells[i, 11].PutValue(childUnion.UdodShortName);
                        sheet.Cells[i, 12].PutValue(childUnion.Name);
                        sheet.Cells[i, 13].PutValue(childUnion.Program.Name);
                        sheet.Cells[i, 14].PutValue(childUnion.Profile.Name);
                        sheet.Cells[i, 15].PutValue(childUnion.AgeGroups[0].Name);
                        sheet.Cells[i, 16].PutValue(childUnion.AgeGroups[0].DateBegin.ToShortDateString());
                        if (childUnion.AgeGroups[0].DateEnd.HasValue)
                        {
                            sheet.Cells[i, 17].PutValue(((DateTime)childUnion.AgeGroups[0].DateEnd).ToShortDateString());
                        }
                        sheet.Cells[i, 18].PutValue(childUnion.PupilBudgetType.TypeBudgetName);
                        sheet.Cells[i, 19].PutValue(childUnion.PupilHours);
                        sheet.Cells[i, 20].PutValue(childUnion.PupilNumYear);
                        sheet.Cells[i, 21].PutValue(extendedPupil.UserId);
                        sheet.Cells[i, 22].PutValue(childUnion.PupilAgeGroupId);
                        //sheet.Cells[i, 16].PutValue(childUnion.DateStart.ToShortDateString() + " " + childUnion.DateStart.ToShortTimeString());
                        //sheet.Cells[i, 17].PutValue(childUnion.DateEnd1.HasValue?childUnion.DateEnd1.Value.ToShortDateString()+ " " + childUnion.DateEnd1.Value.ToShortTimeString():"");

                        i++;
                    }
                }
                sheet.Cells.HideColumn(21);
                sheet.Cells.HideColumn(22);
                sheet.Cells.CreateRange(0, 0, 1, 22).ApplyStyle(style_header, new Aspose.Cells.StyleFlag() { All = true });
                sheet.Cells.CreateRange(1, 0, i - 1, 22).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
                sheet.AutoFitRows();

            }
        }

        workbook.Save("PupilListByChildUnions.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }

    protected void ddlMessage_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        pgrPupilList.Clear();
        PupilListDatabind();
    }

    protected void parentRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        ExtendedUser item = (ExtendedUser)e.Item.DataItem;
        if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem)) //&& (item.UserId != PupilId))
        {
            (e.Item.FindControl("ParentFio") as UserControl_User_ucFIO_vertical).LastName = item.LastName;
            (e.Item.FindControl("ParentFio") as UserControl_User_ucFIO_vertical).FirstName = item.FirstName;
            (e.Item.FindControl("ParentFio") as UserControl_User_ucFIO_vertical).MiddleName = item.MiddleName;
            (e.Item.FindControl("ParentFio") as UserControl_User_ucFIO_vertical).Attributes["ParentId"] = item.UserId.ToString(); // пусть фио еще и айдишник содержит

            (e.Item.FindControl("parentDocument") as UserControl_User_ucDocument_Vertical).Series = item.Passport.Series;
            (e.Item.FindControl("parentDocument") as UserControl_User_ucDocument_Vertical).Number = item.Passport.Number;
            (e.Item.FindControl("parentDocument") as UserControl_User_ucDocument_Vertical).PassportType = item.Passport.PassportType.PassportTypeId;
            (e.Item.FindControl("parentDocument") as UserControl_User_ucDocument_Vertical).IssueDate = item.Passport.IssueDate;
            (e.Item.FindControl("tbParentPhoneNumber") as TextBox).Text = item.PhoneNumber;
            (e.Item.FindControl("tbParentExpressPhoneNumber") as TextBox).Text = item.ExpressPhoneNumber;
            (e.Item.FindControl("tbParentEmail") as TextBox).Text = item.EMail;
            //tr1.Visible = pupil.UserId == item.UserId;

            RadioButtonList rbl = (e.Item.FindControl("cbRelative") as RadioButtonList);
            using (UserDb db = new UserDb())
            {
                rbl.DataSource = db.GetUserTypes();
                rbl.DataBind();
            }
            rbl.SelectedIndex = rbl.Items.IndexOf(rbl.Items.FindByValue(item.UserType.ToString()));

            if (UserContext.Roles.RoleId != (int)EnumRoles.Osip && UserContext.Roles.RoleId!=(int)EnumRoles.Department)
            {
                (e.Item.FindControl("lnkDeleteParent") as LinkButton).Attributes["ParentId"] = item.UserId.ToString();

                if (parentRepeater.Items.Count > 0)
                {
                    (e.Item.FindControl("lnkDeleteParent") as LinkButton).Visible = true;
                    (parentRepeater.Items[0].FindControl("lnkDeleteParent") as LinkButton).Visible = true;
                }
            }
        }
    }

    protected string GetParents(object pupil)
    {
        var list = (pupil as ExtendedPupil).Parents;

        StringBuilder sb = new StringBuilder();

        foreach (var p in list)
        {
            sb.Append(p.Fio + "<br>");
        }
        if (sb.ToString() != "")
            return sb.ToString();
        else
            return "Не задано";
    }

    protected string GetPhoneNumbers(object pupil)
    {
        var list = (pupil as ExtendedPupil).Parents;

        StringBuilder sb = new StringBuilder();

        foreach (var p in list)
        {
            sb.Append(p.PhoneNumber + "<br>" + p.ExpressPhoneNumber + "<br>");
        }
        if (sb.ToString() != "")
            return sb.ToString();
        else
            return "Не задано";
    }

    protected void ddlChangeDO_DO_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlChangeDO_DO.SelectedValue != "")
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                ExtendedChildUnion childunion = db.GetChildUnion(Convert.ToInt64(ddlChangeDO_DO.SelectedValue), null);
                if (childunion != null)
                {
                    using (AgeGroupDb db1= new AgeGroupDb())
                    {
                        var groups = db1.GetGroupsPaging(UserContext.UdodId,"", childunion.ChildUnionId,
                                                         null, null, null, "", 1, 500000, true);
                        ddlGroups.DataSource = groups;
                        ddlGroups.DataBind();
                        ddlGroups_OnSelectedIndexChanged(ddlGroups, new EventArgs());
                    }
                    
                    /*
                    ddlChangeDO_NY.Items.Clear();
                    for (int i = 1; i <= childunion.NumYears; i++)
                    {
                        ddlChangeDO_NY.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }*/
                    var teachers = db.GetTeacher(childunion.ChildUnionId);
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Список педагогов: ");
                    foreach (var teacher in teachers)
                    {
                        sb.Append(teacher.TeacherName + "; ");
                    }
                    sb.Append("<br/>Возраст обучающихся: от " + childunion.AgeStart.ToString() + " до " +
                              childunion.AgeEnd +
                              "; ");
                    sb.Append("<br/>Адреса:");
                    foreach (var extendedAddress in childunion.Addresses)
                    {
                        sb.Append(extendedAddress.AddressStr + "; ");
                    }
                    lblChangeDO_ChildUnionInfo.Text = sb.ToString();
                }
            }
        }
        else
        {
            ddlGroups.Items.Clear();
            ddlChangeDO_NY.Items.Clear();
        }
    }

    protected void ddlChangeDO_DO_OnDataBound(object sender, EventArgs e)
    {
        ddlChangeDO_DO.Items.Insert(0, new ListItem("Выберите новое ДО", ""));
        ddlChangeDO_DO_OnSelectedIndexChanged(sender, e);
    }

    protected void lnkLoadFromReestr_OnClick(object sender, EventArgs e)
    {
        //ExtendedPupil pupil = new ExtendedPupil();
        //if (PupilReestrCode.HasValue) pupil = SiteUtility.GetPupilData(Page.MapPath("../Templates/search_by_ReestrCode_rq.xml"), PupilReestrCode);
        //if (string.IsNullOrEmpty(pupil.ReestrCode))
        //{
        //    var pupilstmp = SiteUtility.GetPupilData(Page.MapPath(PupilDocument.PassportType == 2 ? "../Templates/search_Passport_by_Series_and_Number_rq.xml" : "../Templates/search_by_Series_and_Number_rq.xml"), PupilDocument.Series,
        //                             PupilDocument.Number);
        //    if (pupilstmp.Count == 1) pupil = pupilstmp.FirstOrDefault();
        //    else
        //    {
        //        if (pupilstmp.Count(i => i.LastName.Trim().ToUpper() == PupilFio.LastName.Trim().ToUpper()) != 0)
        //        {
        //            pupil =
        //                pupilstmp.FirstOrDefault(
        //                    i => i.LastName.Trim().ToUpper() == PupilFio.LastName.Trim().ToUpper());
        //        }
        //    }
        //}
        //tbPupilPhoneNumber.Enabled =
        //    tbPupilEmail.Enabled = true;
        //lblErrorLoad.Text = "";
        //try
        //{
        //    PupilReestrCode = Convert.ToInt64(pupil.ReestrCode);
            
        //}
        //catch (Exception)
        //{
        //    PupilReestrCode = null;
        //}
        //lblAddressStr.Text = pupil.AddressStr;
        //if (string.IsNullOrEmpty(pupil.LastName))
        //{
        //    lblErrorLoad.Text =
        //        "По введённым параметрам в Реестре не найдено ни одной записи. ";
            
        //}
        //else
        //    if ((PupilFio.LastName.Trim().ToLower() != pupil.LastName.Trim().ToLower() || PupilFio.FirstName.Trim().ToLower() != pupil.FirstName.Trim().ToLower()))
        //    {
        //        // Ошибка
        //        lblErrorLoad.Text = "Полученные из Реестра данные отличаются от введённых. Проверьте правильность заполнения и повторите запрос. Если Вы уверены в корректности введённых данных - обратитесь в ОСИП для разрешения ситуации по данному обучающемуся.";
        //    }
        //    else
        //    {
        //        btnSavePupil.Visible = true;
        //        //PupilFio.LastName = pupil.LastName;
        //        //PupilFio.FirstName = pupil.FirstName;
        //        PupilFio.MiddleName = pupil.MiddleName;
        //        tbBirthday.Text = pupil.Birthday.ToShortDateString();
        //        tbPupilPhoneNumber.Text = pupil.PhoneNumber;
        //        tbPupilEmail.Text = pupil.EMail;
        //        tbPupilClass.Text = pupil.ClassName;
        //        cbSex.SelectedIndex = cbSex.Items.IndexOf(cbSex.Items.FindByValue(pupil.Sex.ToString()));

        //        string _url = Page.MapPath("../Templates/getUdodInfoByGuid.xml");
        //        bool isReadySchool = true;
        //        try
        //        {
        //            using (DictSchoolDb db = new DictSchoolDb())
        //            {
        //                var tmp = db.GetSchool(pupil.SchoolName);
        //                pupil.SchoolName = tmp.Name;
        //            }
        //        }
        //        catch (Exception)
        //        {
        //            isReadySchool = false;
        //        }

        //        //if (!isReadySchool)
        //        int? schoolCode = null;
        //        try
        //        {
        //            schoolCode = Convert.ToInt32(pupil.SchoolCode);
        //        }
        //        catch (Exception)
        //        {
        //            schoolCode = null;
        //        }

        //        if (schoolCode.HasValue)
        //        {
        //            ExtendedUdod reestrData;
        //            reestrData = SiteUtility.GetUdodData(_url, schoolCode.Value, null);
        //            pupil.SchoolName = reestrData.ShortName;
        //            // обновление справочника школ

        //            //pupil.SchoolCode= reestrData.
        //        }
                
        //        using (PupilsDb db = new PupilsDb())
        //        {
        //            db.UpdatePupilReestrCode(PupilId, PupilReestrCode, null, pupil.ReestrGuid, "", schoolCode, pupil.SchoolName, pupil.ClassName);
        //        }

        //        tbPupilSchool.Text = pupil.SchoolName;
        //        PupilDocument.IssueDate = pupil.Passport.IssueDate;

        //        if (pupil.Addresses[0].BtiCode != null || !string.IsNullOrEmpty(pupil.Addresses[0].KladrCode))
        //        {
        //            var regAddress = pupil.Addresses[0];
        //            RegAddress.IsBti = string.IsNullOrEmpty(regAddress.KladrCode);
        //            if (RegAddress.IsBti)
        //                regAddress.BtiCode = (new KladrDb()).GetStreetCodeByHouseCode(regAddress.BtiCode.Value);
        //            RegAddress.Code = string.IsNullOrEmpty(regAddress.KladrCode) ? regAddress.BtiCode.Value.ToString() : regAddress.KladrCode;
        //            RegAddress.Databind();
        //            RegAddress.Index = regAddress.PostIndex;
        //            RegAddress.Housing = regAddress.Housing;
        //            RegAddress.NumberHouse = regAddress.HouseNumber;
        //            RegAddress.Fraction = regAddress.Fraction;
        //            RegAddress.Flat = regAddress.FlatNumber;
        //            RegAddress.Building = regAddress.Building;
        //        }

        //        /*PupilFio.Enabled =
        //            PupilDocument.Enabled =
        //            tbBirthday.Enabled =
        //            tbPupilSchool.Enabled =
        //            tbPupilClass.Enabled =
        //            cbSex.Enabled = false;*/

        //        //IsEdit = true;
        //    }
        //upEditPupil.Update();
        //upAddresses.Update();
        //upErrorLoad.Update();
    }

    protected void lnkTransfer_OnClick(object sender, EventArgs e)
    {
        FileFormatType format;
        if (upLoad.HasFile)
        {

            if (upLoad.FileName.EndsWith(".xlsx"))
            {
                format = FileFormatType.Excel2007Xlsx;
            }
            else
            {
                format = FileFormatType.Excel2003;
            }

            ImportFromExcel(upLoad.FileContent, format);

        }
    }
    protected void ImportFromExcel(Stream data, FileFormatType format)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("PupilList.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                                string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";

        Workbook workbook = new Workbook();
        try
        {
            workbook.Open(data, format);
            //workbook.LoadData(data);
        }
        catch (Exception)
        {
            throw new Exception("Данный формат файлов не поддерживается.");
        }
        Worksheet sheet = workbook.Worksheets[0];
        // загрузка в список и вывод ошибок если что
        int row = 1;
        int countErrors = 0;
        using (PupilsDb db = new PupilsDb())
        {
            using (ChildUnionDb db1 = new ChildUnionDb())
            {
                int d = sheet.Cells.Rows.Count;
                for (; row < d; row++)
                {
                    if (string.IsNullOrEmpty(sheet.Cells[row, 0].StringValue)) continue;
                    
                    Guid pupilId = new Guid(sheet.Cells[row, 14].StringValue);
                    var childunions = db.GetChildUnion(pupilId);
                    var childunion =
                        childunions.Where(i => i.Name.ToLower() == sheet.Cells[row, 8].StringValue.ToLower()).
                            FirstOrDefault();
                    if (childunion != null)
                    {
                        var newchildunion =
                            db1.GetChildUnionsPaging(null, UserContext.UdodId, null, null, null, null, null, null, null,
                                               sheet.Cells[row, 16].StringValue.Trim().Replace(@"\",""), null, null, null,
                                               null, null,1,200,"",null,null, false,false,null).Where(i => i.Name.ToLower() == sheet.Cells[row, 16].StringValue.ToLower()).FirstOrDefault();
                        if (newchildunion!=null)
                        {
                            int numyear = childunion.PupilNumYear;
                            if (!string.IsNullOrEmpty(sheet.Cells[row, 17].StringValue))
                            {
                                numyear = sheet.Cells[row, 17].IntValue;
                            }

                            // перевод в другое ДО
                            if (db.UpdateChildUnion(pupilId, childunion.ChildUnionId, newchildunion.ChildUnionId,
                                childunion.PupilBudgetType.TypeBudgetName=="Платно"?1:2, numyear) == -1)
                            {
                                // ошибка
                                countErrors++;
                                sheet.Cells[row, 18].PutValue("Ошибка");
                            }
                        }
                        else
                        {
                            sheet.Cells[row, 18].PutValue("не найдено новой ДО");
                        }
                    }
                    else sheet.Cells[row, 18].PutValue("Нет в данном ДО");

                }
            }
        }
        lnkTransfer.Text = string.Format("<div class='btnBlueLong'>{0}</div>", countErrors.ToString());

        workbook.Save("PupilList.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
        /*
        int d = sheet.Cells.Rows.Count;
        List<string> list = new List<string>();
        for (; row < d; row++)
        {
            if (string.IsNullOrEmpty(sheet.Cells[row, 0].StringValue)) continue;
            string str = sheet.Cells[row, 0].StringValue;
            list.Add(str);
        }
        */
        //return list;
    }

    protected void lnkCreateClaim_OnClick(object sender, EventArgs e)
    {
        
    }

    protected void gvPupilList_OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        e.Cancel = true;
    }

    protected void gvPupilList_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ExtendedPupil rowdata = (ExtendedPupil)(e.Row.DataItem);
            e.Row.Attributes.Add("PupilId",rowdata.UserId.ToString());
        }
    }

    protected void gvPupilList_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName=="Edit")
        {
            GridViewRow row = (sender as GridView).Rows[Convert.ToInt32(e.CommandArgument)];
            var pupilid = new Guid(row.Attributes["PupilId"]);
            Session["PupilIdForNewClaim"] = pupilid;
            UserContext.ClaimId = null;
            Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
        }
    }

    protected void lnkUpdateReestrCode_OnClick(object sender, EventArgs e)
    {
        //using (PupilsDb db = new PupilsDb())
        //{
        //    var list = db.GetNotReestrCodePupils(UserContext.CityId, UserContext.UdodId);
        //    foreach (var pupil in list)
        //    {
        //        try
        //        {
        //        if (pupil.Passport.UserPassportId != 0 && !string.IsNullOrEmpty(pupil.Passport.Series) && !string.IsNullOrEmpty(pupil.Passport.Number))
        //        {
        //            ExtendedPupil reestrPupil = null;
        //            var Pupils= SiteUtility.GetPupilData(
        //                    Page.MapPath(pupil.Passport.PassportType.PassportTypeId == 2
        //                                     ? "../Templates/search_Passport_by_Series_and_Number_rq.xml"
        //                                     : "../Templates/search_by_Series_and_Number_rq.xml"),
        //                    pupil.Passport.Series, pupil.Passport.Number);
        //            if (Pupils.Count>1)
        //            {
        //                reestrPupil = Pupils.FirstOrDefault(i => i.LastName.Trim().ToLower() == pupil.LastName.Trim().ToLower());
        //            }
        //            else
        //            {
        //                reestrPupil = Pupils.FirstOrDefault();
        //            }
        //            var messages = pupil.ErrorMessage.Split(';');
        //            if (reestrPupil!=null && !string.IsNullOrEmpty(reestrPupil.ReestrCode))
        //            {
        //                if (pupil.LastName.ToLower().Trim() != reestrPupil.LastName.ToLower().Trim() || pupil.FirstName.ToLower().Trim() != reestrPupil.FirstName.ToLower().Trim())
        //                {
        //                    // ошибка несовпадения ФИО
        //                    MyStringBuilder mysb = new MyStringBuilder("; ", messages);
        //                    mysb.Append("ФИО обучающегося не совпадает с ФИО в Реестре контингента");
        //                    if (!pupil.ErrorMessage.Contains("ФИО обучающегося не совпадает с ФИО в Реестре контингента"))
        //                    db.UpdatePupilReestrCode(pupil.UserId, null, null,null, mysb.ToString(),null,null,null);
        //                }
        //                else
        //                {
        //                    MyStringBuilder mysb = new MyStringBuilder("; ", messages);
        //                    if (pupil.ErrorMessage.Contains("ФИО обучающегося не совпадает с ФИО в Реестре контингента"))
        //                    mysb.Remote("ФИО обучающегося не совпадает с ФИО в Реестре контингента");
        //                    if (pupil.ErrorMessage.Contains("Обучающийся не найден в Реестре контингента"))
        //                    mysb.Remote("Обучающийся не найден в Реестре контингента");

        //                    int? schoolCode = null;
        //                    try
        //                    {
        //                        schoolCode = Convert.ToInt32(pupil.SchoolCode);
        //                    }
        //                    catch (Exception)
        //                    {
        //                        schoolCode = null;
        //                    }
        //                    // Обновить код реестра у нас
        //                    db.UpdatePupilReestrCode(pupil.UserId, Convert.ToInt64(reestrPupil.ReestrCode), null, null, mysb.ToString(), schoolCode, pupil.SchoolName, pupil.ClassName);
        //                }
        //            }
        //            else
        //            {
        //                MyStringBuilder mysb = new MyStringBuilder("; ", messages);
        //                mysb.Append("Обучающийся не найден в Реестре контингента");
        //                // не найден обучающийся
        //                if (!pupil.ErrorMessage.Contains("Обучающийся не найден в Реестре контингента"))
        //                    db.UpdatePupilReestrCode(pupil.UserId, null, null,null, mysb.ToString(),null,null,null);
        //            }
                        
        //        }
        //        }
        //        catch (Exception)
        //        {
        //        }
        //    }
        //}
    }

    protected void lnkSendReestr_OnClick(object sender, EventArgs e)
    {
        //using (PupilsDb db = new PupilsDb())
        //{
        //    var list = db.ReestrGetPupils(null, UserContext.CityId, UserContext.UdodId, tbPupilLastName.Text, tbPupilFirstName.Text, tbPupilMiddleName.Text);
        //    foreach (var pupil in list)
        //    {
        //        /*var pupil = db.GetPupil(extendedPupil.UserId);
        //        using (KladrDb db2 = new KladrDb())
        //        {
        //            var addresses = db2.GetAddressesById(pupil.UserId, null, null, null);
        //            pupil.Addresses = addresses;
        //        }*/
        //        try
        //        {
        //            var reestrpupil = SiteUtility.GetPupilData(Page.MapPath(pupil.Passport.PassportType.PassportTypeId == 2 ? "../Templates/search_Passport_by_Series_and_Number_rq.xml" : "../Templates/search_by_Series_and_Number_rq.xml"), pupil.Passport.Series, pupil.Passport.Number);
        //            ExtendedPupil pupil1 = null;
        //            if (reestrpupil.Count == 1) pupil1 = reestrpupil.FirstOrDefault();
        //            else
        //            {
        //                if (reestrpupil.Count(i => i.LastName.Trim().ToUpper() == PupilFio.LastName.Trim().ToUpper()) != 0)
        //                {
        //                    pupil1 =
        //                        reestrpupil.FirstOrDefault(
        //                            i => i.LastName.Trim().ToUpper() == PupilFio.LastName.Trim().ToUpper());
        //                }
        //            }
        //            if (pupil1 != null)
        //            {
        //                int? schoolCode = null;
        //                try
        //                {
        //                    schoolCode = Convert.ToInt32(pupil1.SchoolCode);
        //                }
        //                catch (Exception)
        //                {
        //                    schoolCode = null;
        //                }
        //                db.UpdatePupilReestrCode(pupil.UserId, null, null, pupil1.ReestrGuid, "", schoolCode, pupil1.SchoolName,pupil1.ClassName);
        //            }
        //            /*if (string.IsNullOrEmpty(pupil.ReestrUdodCode))
        //            {

        //                if (!string.IsNullOrEmpty(pupil.ReestrCode) || pupil.ReestrCode=="0")
        //                {
        //                    Guid? reestrGuid = null;
        //                    SiteUtility.ReestrAddPupil(pupil, !string.IsNullOrEmpty(pupil.ReestrUdodCode), ref reestrGuid);
        //                }
        //            }*/
        //        }
        //        catch (Exception)
        //        {
                    
        //        }
                
        //    }
        //    /*List<ExtendedPupil> list = db.GetPupilsPaging(UserContext.CityId, UserContext.UdodId, null, null, null, null, 1, 10,null, 0, null, null, false).Take(10000).ToList();
        //    List<ExtendedPupil> list1 = new List<ExtendedPupil>();

        //    foreach (var pupil in list)
        //    {
        //        var item = db.GetPupil(pupil.UserId);
        //        if (item != null)
        //        {
        //            using (KladrDb db1 = new KladrDb())
        //            {
        //                var addresses = db1.GetAddressesById(pupil.UserId, null, null, null);
        //                item.Addresses = addresses;
        //            }
        //            list1.Add(item);
        //        }
        //    }

        //    var result= SiteUtility.SendPupils(Page.MapPath("../Templates/SendPupilsInReestr.xml"), list1);

        //    FileInfo file1 = new FileInfo("c:\\udod\\resultSendPupil"+Guid.NewGuid());
        //    var writer=file1.AppendText();
        //    writer.WriteLine(result);
        //    writer.Flush();
        //    writer.Close();*/
        //}
    }

    protected void lnkReestrSendZach_OnClick(object sender, EventArgs e)
    {
        long? childUnionId = null;
        try
        {
            childUnionId = Convert.ToInt64(ddlDO.SelectedValue);
        }
        catch (Exception)
        {
            
        }
        using (ChildUnionDb db = new ChildUnionDb())
        {


            using (PupilsDb db1 = new PupilsDb())
            {
                var list = db1.ReestrGetAgeGroupPupils(UserContext.CityId, UserContext.UdodId, childUnionId,
                                               tbPupilLastName.Text,
                                               tbPupilFirstName.Text, tbPupilMiddleName.Text, 1, 50000000, null, 0, null,
                                               null, false);
                
                foreach (var extendedPupil in list)
                {
                    try
                    {
                        //ExtendedPupil pupil = db1.GetPupil(extendedPupil.UserId);
                        if (string.IsNullOrEmpty(extendedPupil.ReestrCode))
                        {
                            if (string.IsNullOrEmpty(extendedPupil.ReestrUdodCode))
                            {
                                Guid? reestrGuid = null;
                                //if (!string.IsNullOrEmpty(extendedPupil.ReestrCode) || extendedPupil.ReestrCode == "0")
                                //{
                                //    extendedPupil.ReestrUdodCode = SiteUtility.ReestrAddPupil(extendedPupil,
                                //                                                              !string.IsNullOrEmpty(
                                //                                                                  extendedPupil.
                                //                                                                      ReestrUdodCode),
                                //                                                              ref reestrGuid);
                                //}
                            }
                        }
                        foreach (var extendedChildUnion in extendedPupil.ChildUnions)
                        {
                            var childunion = db.GetChildUnion(extendedChildUnion.ChildUnionId, null);
                            if (string.IsNullOrEmpty(childunion.ReestrCode) &&
                                string.IsNullOrEmpty(extendedChildUnion.ReestrPupilAgeGroupId))
                            {
                                var teachers = db.GetTeacher(extendedChildUnion.ChildUnionId);
                                //childunion.ReestrCode = SiteUtility.ReestrAddDO(childunion, false, teachers).ToString();
                            }

                            //SiteUtility.ReestrAddZach(extendedPupil, childunion, extendedChildUnion, !string.IsNullOrEmpty(childunion.ReestrPupilAgeGroupId));
                        }
                    }
                    catch
                    {
                        
                    }
                }

            }
        }
    }

    protected void ddlGroups_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlGroups.SelectedValue))
        {
            using (AgeGroupDb db = new AgeGroupDb())
            {
                var group = db.GetGroup(Convert.ToInt64(ddlGroups.SelectedValue));
                ddlChangeDO_NY.Items.Clear();
                foreach (int year in group.NumYears)
                {
                    ddlChangeDO_NY.Items.Add(new ListItem(year.ToString()));
                }
            }
        }
        upChangeDO.Update();
    }

    protected void ddlGroupsFilter_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        
    }

    protected void ddlGroupsFilter_OnDataBound(object sender, EventArgs e)
    {
        if (ddlGroupsFilter.Items.Count == 0)
        {
            ddlGroupsFilter.Items.Insert(0, new ListItem("Выберите ДО", ""));
        }
        else
        {
            ddlGroupsFilter.Items.Insert(0, new ListItem("Все", ""));
        }
    }

    protected void ddlSchoolType_OnDataBound(object sender, EventArgs e)
    {
        ddlSchoolType.Items.Insert(0, new ListItem("", "0"));
    }

    protected void ddlSchoolType_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        lblSchoolType.Visible = ddlSchoolType.Enabled && ddlSchoolType.SelectedValue == "1";
    }
}
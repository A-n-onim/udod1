﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class Udod_ScheduleHistory : BasePage
{
    protected long? ScheduleHistoryId
    {
        get
        {
            if (ViewState["ScheduleLessonHistoryId"] == null) return null;
            return (long) ViewState["ScheduleLessonHistoryId"];
        }
        set { ViewState["ScheduleLessonHistoryId"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsUdodEmployee);
        if (!Page.IsPostBack)
        {
            FiltersDatabind();
            ScheduleHistoryDatabind();
        }
    }

    protected void cityUdodFilter_OnCityChange(object sender, EventArgs e)
    {

    }

    protected void cityUdodFilter_OnUdodChange(object sender, EventArgs e)
    {
        FiltersDatabind();
        ddlDOFilter_OnSelectedIndexChanged(ddlDOFilter, new EventArgs());
        ScheduleHistoryDatabind();
    }

    protected void cityUdodFilter_OnloadComplete(object sender, EventArgs e)
    {

    }
    
    protected void FiltersDatabind()
    {
        if (cityUdodFilter.SelectedUdod.HasValue)
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                var list = db.GetChildUnionsShort(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null, null, null, null, null, null, null, null, null, null, null, null, null,false).OrderBy(i => i.Name);
                ddlDOFilter.DataSource = list;
            }
        }
        else
        {
            ddlTeacherFilter.DataSource = new ListItemCollection();
            ddlDOFilter.DataSource = new ListItemCollection();
        }
        ddlDOFilter.DataBind();
        upCityUdodFilter.Update();
        upFilter.Update();
        upGridView.Update();
    }

    protected void ScheduleHistoryDatabind()
    {
        long? childUnionid = null;
        try
        {
            childUnionid = Convert.ToInt64(ddlDOFilter.SelectedValue);
        }
        catch (Exception)
        {
        }
        Guid? teacherId = null;
        try
        {
            teacherId= new Guid(ddlTeacherFilter.SelectedValue);
        }
        catch (Exception)
        {
        }
        long? groupId = null;
        try
        {
            groupId = Convert.ToInt64(ddlGroupFilter.SelectedValue);
        }
        catch (Exception)
        {
        }
        int? numYear = null;
        try
        {
            numYear = Convert.ToInt32(ddlNumYear.SelectedValue);
        }
        catch (Exception)
        {
        }
        int? dictTypeBudgetId = null;
        try
        {
            dictTypeBudgetId = Convert.ToInt32(ddlTypeBudget.SelectedValue);
        }
        catch (Exception)
        {
        }
        int? dictTypeEducationId = null;
        try
        {
            dictTypeEducationId = Convert.ToInt32(ddlTypeEducation.SelectedValue);
        }
        catch (Exception)
        {
        }
        DateTime? startDate = null;
        try
        {
            startDate = Convert.ToDateTime(tbDateStart.Text);
        }
        catch (Exception)
        {
        }
        DateTime? endDate = null;
        try
        {
            endDate = Convert.ToDateTime(tbDateEnd.Text);
        }
        catch (Exception)
        {
        }
        using (ScheduleDb db = new ScheduleDb())
        {
            var list = db.GetScheduleHistoryPaging(UserContext.UdodId, childUnionid, teacherId, groupId, numYear,
                                                   dictTypeBudgetId, dictTypeEducationId, startDate, endDate,
                                                   pgrScheduleHistory.PagerIndex, pgrScheduleHistory.PagerLength);
            gvScheduleHistory.DataSource=list;
            gvScheduleHistory.DataBind();
        }
        upGridView.Update();
    }
    protected void ddlGroupFilter_OnDataBound(object sender, EventArgs e)
    {
        DropDownList list = sender as DropDownList;
        if (list != null) list.Items.Insert(0, new ListItem("Все", ""));
    }

    protected void ddlDOFilter_OnDataBound(object sender, EventArgs e)
    {
        if (ddlDOFilter.Items.Count == 0)
        {
            ddlDOFilter.Items.Add(new ListItem("Выберите УДО"));
        }
    }

    protected void ddlDOFilter_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        long? childunionId = null;
        try
        {
            childunionId = Convert.ToInt64(ddlDOFilter.SelectedValue);
        }
        catch (Exception)
        {
        }
        using (AgeGroupDb db = new AgeGroupDb())
        {
            if (UserContext.UdodId.HasValue)
            {
                var list = db.GetGroupsPaging(UserContext.UdodId, null, childunionId, null, null, null, "", 1, 5000, null);
                ddlGroupFilter.DataSource = list;
            }
            else
            {
                ddlGroupFilter.DataSource = new List<ExtendedGroup>();
                //ddlTeacherFilter.DataSource = 
            }
            ddlGroupFilter.DataBind();
        }
        if (childunionId.HasValue)
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                var teachers = db.GetTeacher(childunionId.Value);
                ddlTeacherFilter.DataTextField = "TeacherName";
                ddlTeacherFilter.DataSource = teachers;
                ddlTeacherFilter.DataBind();
            }

        }
        else
        {
            if (UserContext.UdodId.HasValue)
            {
                using (TeacherDb db = new TeacherDb())
                {
                    var teachers = db.GetTeacherPaging(null, UserContext.UdodId, null, 1, 5000);
                    ddlTeacherFilter.DataTextField = "Fio";
                    ddlTeacherFilter.DataSource = teachers;
                    ddlTeacherFilter.DataBind();
                }
            }
            else
            {
                ddlTeacherFilter.DataTextField = "TeacherName";
                ddlTeacherFilter.DataSource = new List<ExtendedChildUnionTeacher>(); ;
                ddlTeacherFilter.DataBind();
            }
        }
        if (UserContext.UdodId.HasValue)
        {
            using (DictProgramDb db = new DictProgramDb())
            {
                var list = db.GetPrograms(UserContext.UdodId);
                ddlProgramFilter.DataSource = list;
                ddlProgramFilter.DataBind();
            }
        }
        else
        {
            ddlProgramFilter.DataSource = new List<ExtendedProgram>();
            ddlProgramFilter.DataBind();
        }
        upFilter.Update();
    }

    protected void lnkFindByName_OnClick(object sender, EventArgs e)
    {
        ScheduleHistoryDatabind();
    }

    protected void pgrScheduleHistory_OnChange(object sender, EventArgs e)
    {
        ScheduleHistoryDatabind();
    }

    protected void LoadGroupInfo(long groupId)
    {
        using (AgeGroupDb db = new AgeGroupDb())
        {
            var group = db.GetGroup(groupId);
            lblName.Text = group.Name;
            lblDO.Text = group.ChildUnionName;
            StringBuilder sb = new StringBuilder();  
            foreach (var teacher in group.Teachers)
            {
                sb.Append(teacher.Fio+" ");
            }
            lblTeachers.Text = sb.ToString();
            lblStartAge.Text = group.AgeStart.ToString();
            lblEndAge.Text = group.AgeEnd.ToString();
            lblTypeBudget.Text = group.TypeBudget.TypeBudgetName;
            lblTypeEducation.Text = group.EducationType.EducationTypeName;
            sb= new StringBuilder();
            foreach (var year in group.NumYears)
            {
                sb.Append(year + ", ");
            }
            lblNumYear.Text = sb.ToString();
            lblComment.Text = group.Comment;
            sb = new StringBuilder();
            foreach (var address in group.Adresses)
            {
                sb.Append(address.AddressStr + " ");
            }
            lblbAddress.Text = sb.ToString();
        }
    }

    protected void lnkCommand_OnClick(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton) sender;
        GridViewRow row = (GridViewRow)lnk.Parent.Parent;
        var scheduleLessonHistoryId = ScheduleHistoryId= Convert.ToInt64(row.Attributes["ScheduleLessonHistoryId"]);
        var groupId = Convert.ToInt64(row.Attributes["GroupId"]);
        var scheduleLessonId = Convert.ToInt64(row.Attributes["ScheduleLessonId"]);
        var childUnionId = Convert.ToInt64(row.Attributes["ChildUnionId"]);
        switch (lnk.ID)
        {
            case "lnkShowGroup":
                LoadGroupInfo(groupId);
                litTitle.Text = "<span class='headerCaptions'>Просмотр группы</span>";
                upPopup.Update();
                popupAddExtender.Show();
                break;
            case "lnkShowDO":
                using (ChildUnionDb db = new ChildUnionDb())
                {
                    selectDO.SetDOData(db.GetChildUnion(childUnionId, null));
                    upPassportDO.Update();
                    popupPassportDOExtender.Show();
                }
                break;
            case "lnkShowScheduleLesson":
                LoadScheduleLesson(scheduleLessonId, null, null);
                ddlTeacher.Visible =
                    tbComment.Visible=tbLessonDate.Visible = tbStartTime.Visible = tbEndTime.Visible = tbEndRecess.Visible = false;
                lblEndRecess.Visible =
                    lblScheduleComment.Visible = lblEndTime.Visible = lblStartTime.Visible = lblWeekDay.Visible = lblTeacher.Visible = true;
                popupAddScheduleExtender.Show();
                break;
            case "lnkEdit":

                ddlTeacher.Visible =
                    tbComment.Visible = tbLessonDate.Visible = tbStartTime.Visible = tbEndTime.Visible = tbEndRecess.Visible = true;
                lblEndRecess.Visible =
                    lblScheduleComment.Visible = lblEndTime.Visible = lblStartTime.Visible = lblWeekDay.Visible = lblTeacher.Visible = false;
                LoadScheduleLesson(scheduleLessonId, childUnionId, scheduleLessonHistoryId);
                lnkAddScheduleOk.Attributes["ScheduleLessonHistoryId"] = scheduleLessonHistoryId.ToString();
                popupAddScheduleExtender.Show();
                break;
            case "lnkCancel":

                errorPopup.ShowError("Удаление","Вы действительно хотите удалить занятие", "Да", "Нет", true, true);
                break;
        }

    }

    protected void LoadScheduleLesson(long scheduleLessonId, long? childUnionId, long? scheduleLessonHistoryId)
    {
        if (childUnionId.HasValue)
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                var teachers = db.GetTeacher(childUnionId.Value);
                ddlTeacher.DataSource = teachers;
                ddlTeacher.DataBind();
            }
        }
        using (ScheduleDb db = new ScheduleDb())
        {
            if (!scheduleLessonHistoryId.HasValue)
            {
                var scheduleLesson = db.GetSheduleLesson(scheduleLessonId);
                lblTeacher.Text = scheduleLesson.Fio;
                if (childUnionId.HasValue)
                {
                    ddlTeacher.SelectedIndex =
                        ddlTeacher.Items.IndexOf(ddlTeacher.Items.FindByValue(scheduleLesson.TeacherId.ToString()));
                }
                tdLessondate.InnerText = "День недели";
                lblSection.Text = scheduleLesson.SectionName;
                lblWeekDay.Text = scheduleLesson.Weekday.ToString();
                tbStartTime.Text = lblStartTime.Text = scheduleLesson.StartTime.ToShortTimeString();
                tbEndTime.Text = lblEndTime.Text = scheduleLesson.EndTime.ToShortTimeString();
                tbEndRecess.Text = lblEndRecess.Text = scheduleLesson.EndRecess.ToShortTimeString();
                lblScheduleComment.Text = scheduleLesson.Comment;
            }
            else
            {
                var scheduleLessonHistory = db.GetSheduleLessonHistory(scheduleLessonHistoryId.Value);
                lblTeacher.Text = scheduleLessonHistory.Fio;
                if (childUnionId.HasValue)
                {
                    ddlTeacher.SelectedIndex =
                        ddlTeacher.Items.IndexOf(ddlTeacher.Items.FindByValue(scheduleLessonHistory.TeacherId.ToString()));
                }
                tdLessondate.InnerText = "Дата занятия";
                tbLessonDate.Text = scheduleLessonHistory.LessonDate.ToShortDateString();
                lblSection.Text = scheduleLessonHistory.SectionName;
                lblWeekDay.Text = scheduleLessonHistory.Weekday.ToString();
                tbStartTime.Text = lblStartTime.Text = scheduleLessonHistory.StartTime.ToShortTimeString();
                tbEndTime.Text = lblEndTime.Text = scheduleLessonHistory.EndTime.ToShortTimeString();
                tbEndRecess.Text = lblEndRecess.Text = scheduleLessonHistory.EndRecess.ToShortTimeString();
                tbComment.Text=lblScheduleComment.Text = scheduleLessonHistory.Comment;
                
            }
            upAddSchedule.Update();
        }
    }

    protected void gvScheduleHistory_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType==DataControlRowType.DataRow)
        {
            ExtendedScheduleHistory item = (e.Row.DataItem as ExtendedScheduleHistory);
            e.Row.Attributes["ScheduleLessonHistoryId"] = item.ScheduleLessonHistoryId.ToString();
            e.Row.Attributes["ChildunionId"] = item.ChildUnionId.ToString();
            e.Row.Attributes["GroupId"] = item.GroupId.ToString();
            e.Row.Attributes["ScheduleLessonId"] = item.ScheduleLessonId.ToString();

        }
    }

    protected void lnkAddScheduleOk_OnClick(object sender, EventArgs e)
    {
        // сохранить изменения в расписании
        using (ScheduleDb db = new ScheduleDb())
        {
            var scheduleLessonHistoryId = Convert.ToInt64((sender as LinkButton).Attributes["scheduleLessonHistoryId"]);
            if (db.UpdateSheduleLessonHistory(scheduleLessonHistoryId, Convert.ToDateTime(tbLessonDate.Text), new Guid(ddlTeacher.SelectedValue), Convert.ToDateTime(tbStartTime.Text), Convert.ToDateTime(tbEndTime.Text), Convert.ToDateTime(tbEndRecess.Text),tbComment.Text)==-1)
            {
                // ошибка
                errorPopup.ShowError("Ошибка","Время занятия группы пересекается с другим занятием", "", "Ок", false, true);
            }
        }
        ScheduleHistoryDatabind();
        popupAddScheduleExtender.Hide();
    }

    protected void errorPopup_OnErrorOkClick(object sender, EventArgs e)
    {
        // удаление занятия
        using (ScheduleDb db = new ScheduleDb())
        {
            if (ScheduleHistoryId.HasValue)
            db.DeleteSheduleLessonHistory(ScheduleHistoryId.Value);
        }
        ScheduleHistoryDatabind();
        errorPopup.HideError();
    }
}
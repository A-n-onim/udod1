﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="ContactsOsip.aspx.cs" Inherits="Udod_ContactsOsip" %>

<asp:Content runat="server" ID="contentContacts" ContentPlaceHolderID="body">

<div style="font-size: 21px; text-align: center; font-weight: bold;">Информация об окружных службах информационной поддержки</div>
    
<table class="dgc">
    <tr>
        <th>Адрес ОСИП</th>
        <th>Контактные телефоны ОСИП</th>
        <th>Телефон горячей линии (ОУО ДО)</th>
        <th>Телефон горячей линии (ОУО СОШ, УДОД)</th>
        <th>Е-mail (для обращения по записи в ДОУ)</th>
        <th>Е-mail ( для обращения по записи в школу, по записи на дополнительное образование)</th>
        <th>График работы (без перерыва на обед)</th>
    </tr>
    <tr>
        <td colspan="7" class="ContactsOsipCity">ВОУО</td>
    </tr>
    <tr>
        <td>Ул. 5-я Парковая, д. 51</td>
        <td>8-499-163-81-00<br/>8-499-164-78-16</td>
        <td>8-495-963-56-42</td>
        <td>8-499-163-81-00</td>
        <td>Osip-vostok@rambler.ru</td>
        <td>Garbina.omc@gmail.com</td>
        <td>Пн-Пт: 8:00-20:00<br/>Сб: 10:00-16:00</td>
    </tr>
    <tr>
        <td colspan="7" class="ContactsOsipCity">ЗелОУО</td>
    </tr>
    <tr>
        <td>Зеленоград, корп. 430 А</td>
        <td>8-499-735-74-84 <br/>8-499-736-81-71 <br/>8-499-736-81-61</td>
        <td>8-499-735-75-21</td>
        <td>8-499-735-75-21</td>
        <td>Osip1@zou.ru</td>
        <td>Osip1@zou.ru</td>
        <td>Пн-Пт: 8:00-20:00<br/>Сб: 10:00-13:00</td>
    </tr>
    <tr>
        <td colspan="7" class="ContactsOsipCity">ЗОУО</td>
    </tr>
    <tr>
        <td>Ул. Большая Дорогомиловская, д.10 корп.2</td>
        <td>8-499-240-11-56</td>
        <td>8-499-240-52-74</td>
        <td>8-985-424-73-45</td>
        <td>Osipzao@mail.ru</td>
        <td>osipzaosoh@mail.ru</td>
        <td>Пн–Пт: 8:00-20:00<br/>Сб: 10:00-14:00</td>
    </tr>
    <tr>
        <td colspan="7" class="ContactsOsipCity">СВОУО</td>
    </tr>
    <tr>
        <td>Ул.Новоалексеевская, д. 8</td>
        <td>8-495-687-69-62</td>
        <td>8-499-760-51-54</td>
        <td>8-499-760-51-53</td>
        <td>osip@svuo.educom.ru</td>
        <td>osip@svuo.educom.ru</td>
        <td>Пн–Пт: 8:00-20:00<br/>Сб-Вс: 10:00-14:00</td>
    </tr>
    <tr>
        <td colspan="7" class="ContactsOsipCity">СЗОУО</td>
    </tr>
    <tr>
        <td>Ул. Маршала Тухачевского, дом 43, корп. 1.</td>
        <td>8-495-947-68-31<br/>8-495-947-68-26<br/>8-499-720-24-82</td>
        <td>8-495-947-68-31</td>
        <td>8-495-947-68-31</td>
        <td>Osip1@szouo.ru</td>
        <td>Osip1@szouo.ru</td>
        <td>Пн–Пт: 8:00-20:00<br/>Сб: 10:00-14:00</td>
    </tr>
    <tr>
        <td colspan="7" class="ContactsOsipCity">СОУО</td>
    </tr>
    <tr>
        <td>Ул.Красноармейская, д.15</td>
        <td>8-495-708-14-89<br/>8-495-708-14-89</td>
        <td>8-495-708-14-89</td>
        <td>8-495-708-14-89</td>
        <td>sao.osip@gmail.com</td>
        <td>Osip-sao@mail.ru</td>
        <td>Пн–Пт: 8:00-20:00<br/>Сб: 9:00-19:00</td>
    </tr>
    <tr>
        <td colspan="7" class="ContactsOsipCity">ЮОУО</td>
    </tr>
    <tr>
        <td>Ул. Чертановская, д. 25, к. 2</td>
        <td>8-495-315-88-13<br/>8-495-315-84-81</td>
        <td>8-499-614-88-50</td>
        <td>8-499-614-06-91</td>
        <td>sip@sinergi.ru</td>
        <td>1class@sinergi.ru</td>
        <td>Пн–Пт: 8:00-20:00<br/>Сб: 10:00-15:00</td>
    </tr>
    <tr>
        <td colspan="7" class="ContactsOsipCity">ЮЗОУО</td>
    </tr>
    <tr>
        <td>Ул. Профсоюзная, д.33, к.4</td>
        <td>8-499-506-76-50<br/>8-499-506-98-70</td>
        <td>8(499)176-00-60</td>
        <td>8-499-176-00-60</td>
        <td>Osip1462@mosuzedu.ru</td>
        <td>Osip1462@mosuzedu.ru</td>
        <td>Пн–Пт: 8:00-20:00<br/>Сб: 10:00-16:00</td>
    </tr>
    <tr>
        <td colspan="7" class="ContactsOsipCity">ЦОУО</td>
    </tr>
    <tr>
        <td>Пресненский вал, д.1</td>
        <td>8-499-790-01-45</td>
        <td>8-495-915-50-34</td>
        <td>8-495-915-50-34</td>
        <td>ocupcou@mail.ru</td>
        <td>ocupcou@mail.ru</td>
        <td>Пн–Пт: 8:00-20:00<br/>Сб: 10:00-18:00</td>
    </tr>
    <tr>
        <td colspan="7" class="ContactsOsipCity">ЮВОУО</td>
    </tr>
    <tr>
        <td>Ул. Юных Ленинцев, д. 111 корп. 2</td>
        <td>8-499-172-49-65<br/>8-499-175-63-92<br/>8-495-660-23-84</td>
        <td>8-499-172-08-13</td>
        <td>8-499-172-08-43</td>
        <td>ais@uvuo.ru</td>
        <td>osip_uvuo@mail.ru</td>
        <td>Пн–Пт: 8:00-20:00<br/>Сб: 10:00-16:00<br/>Вс: 10:00-15:00</td>
    </tr>
    <tr>
        <td colspan="7" class="ContactsOsipCity">ТиНАО</td>
    </tr>
    <tr>
        <td>г. Москва, г. Московский,3 мик-н. д.8</td>
        <td>8-985-174-84-95</td>
        <td>8(495) 712-98-46</td>
        <td>8(495) 712-98-46</td>
        <td>Osip2064@yandex.ru</td>
        <td>Osip2064@yandex.r</td>
        <td>Пн-Пт: 9:00–18:00</td>
    </tr>
    <tr>
        <td>г. Москва,п. Рязановское, с. Остафьево, ул. Троицкая, д.1</td>
        <td>8-916-762-78-81</td>
        <td>8(495) 712-98-46</td>
        <td>8(495) 712-98-46</td>
        <td>Osip2080@yandex.ru</td>
        <td>Osip2080@yandex.ru</td>
        <td>Пн-Пт: 09:00–18:00</td>
    </tr>
    <tr>
        <td>г. Москва,п. Марушкинское, д. Марушкино, ул. Липовая аллея, д.6</td>
        <td>8-925-862-84-53</td>
        <td>8(495) 712-98-46</td>
        <td>8(495) 712-98-46</td>
        <td>Osip2059@yandex.ru</td>
        <td>Osip2059@yandex.ru</td>
        <td>Пн,Ср,Чт,Пт: 13:00–18:00</td>
    </tr>
    <tr>
        <td>г. Москва, п. Сосенское, пос. Коммунарка</td>
        <td>8-495-817-74-46</td>
        <td>8(495) 712-98-46</td>
        <td>8(495) 712-98-46</td>
        <td>Osip2070@yandex.ru</td>
        <td>Osip2070@yandex.ru</td>
        <td>Пн-Пт: 9:00–18:00</td>
    </tr>
    <tr>
        <td>г. Москва, п. Вороновское, п. ЛМС, мкр. Центральный</td>
        <td>8-495-850-74-81</td>
        <td>8(495) 712-98-46</td>
        <td>8(495) 712-98-46</td>
        <td>Osip2073@yandex.ru</td>
        <td>Osip2073@yandex.ru</td>
        <td>Пн: 9:15-18:00<br/>Вт-Пт: 10:15-18:00</td>
    </tr>
</table>

</asp:Content>
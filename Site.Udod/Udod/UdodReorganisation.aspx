﻿<%@ Page Language="C#" MasterPageFile="../Master/MasterPage.master" AutoEventWireup="true" CodeFile="UdodReorganisation.aspx.cs" Inherits="Udod_UdodReorganisation" %>
<%@ Register src="../UserControl/Udod/ucUdodPassport.ascx" tagName="UdodPassport" tagPrefix="uct" %>
<%@ Register TagPrefix="uct" TagName="CityUdodFilter" Src="~/UserControl/Filter/ucCityUdodFilter.ascx" %>

<asp:Content runat="server" ID="ContentReorganisation" ContentPlaceHolderID="body">
    
    <asp:Panel runat="server" ID="MergeAttachContainer" Visible="false">
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            <uct:CityUdodFilter ID="cityUdodFilterFirst" runat="server" UdodDdlEnabled="false" AddAllProperty="true"/>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="tbFirstName" CssClass="input"></asp:TextBox>
                        </td>
                        <td>
                            <asp:LinkButton runat="server" ID="lnkFirstFind" OnClick="lnkFirstFind_OnClick"><div class="btnBlue">Поиск</div></asp:LinkButton>
                        </td>
                    </tr>
                </table>
                <asp:Label ID="lblFirstUdod_MergeAttach" runat="server" Text="Выберите первое УДО для слияния" />
                <asp:RadioButtonList ID="rbnFirstUdod_MergeAttach" runat="server" DataTextField="Name" AutoPostBack="true" DataValueField="UdodId" OnSelectedIndexChanged="rbnFirstUdod_MergeAttach_SelectedIndexChanged"/>
                <br />
                <table>
                    <tr>
                        <td>
                            <uct:CityUdodFilter ID="cityUdodFilterSecond" runat="server" UdodDdlEnabled="false" AddAllProperty="true"/>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="tbSecondName" CssClass="input"></asp:TextBox>
                        </td>
                        <td>
                            <asp:LinkButton runat="server" ID="lnkSecondFind" OnClick="lnkSecondFind_OnClick"><div class="btnBlue">Поиск</div></asp:LinkButton>
                        </td>
                    </tr>
                </table>
                <asp:Label ID="lblSecondUdod_MergeAttach" runat="server" Text="Выберите второе УДО для слияния" />
                <asp:RadioButtonList ID="rbnSecondUdod_MergeAttach" runat="server" DataTextField="Name" AutoPostBack="true" DataValueField="UdodId" OnSelectedIndexChanged="rbnSecondUdod_MergeAttach_SelectedIndexChanged"/>
                <br />
                <asp:Label ID="lblBaseUdod_MergeAttach" runat="server" Text="Какие данные взять за основу нового УДО?" />
                <asp:DropDownList CssClass="inputLong" ID="ddlBaseUdod_MergeAttach" runat="server" DataTextField="Name" DataValueField="UdodId"/>
                <br />
                <asp:Label ID="lblNameUdod_MergeAttach" runat="server" Text="Введите название нового УДО" />
                <asp:TextBox CssClass="inputLong" ID="tbxNameUdod_MergeAttach" runat="server"/>
                <br />
                <asp:Label ID="lblShortNameUdod_MeregeAttach" runat="server" Text="Введите короткое имя нового УДО" />
                <asp:TextBox CssClass="inputLong" ID="tbxShortNameUdod_MergeAttach" runat="server"/>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <asp:UpdatePanel ID="upDetachDivide" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <ajaxToolkit:TabContainer ID="DetachDivideContainer" runat="server"  Visible="false" AutoPostBack="true" OnActiveTabChanged="activeTabChanged" 
                Height="550px"
                Width="1000px"
                ScrollBars="Auto">
                <ajaxToolkit:TabPanel ID="SelectUdod_DetachDivide" runat="server" 
                    HeaderText="<span style='color: black;'>Выбор УДО</span>"
                    Enabled="true"
                    ScrollBars="Auto" >
                    <ContentTemplate>
                        <asp:Label ID="lblUdodsList_DetachDivide" runat="server" Text="Выберите УДО из которого хотите выделить новый УДО:" />
                        <asp:RadioButtonList ID="rbnUdodsList_DetachDivide" runat="server" AutoPostBack="true" DataTextField="Name" DataValueField="UdodId"/>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>

                <ajaxToolkit:TabPanel ID="AddressDistribution_DetachDivide" runat="server" 
                    HeaderText="<span style='color: black;'>Распределение адресов</span>" >
                    <ContentTemplate>
                        <asp:UpdatePanel ID="upAd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            
                        <asp:Label ID="lblAddressDistribution_DetachDivide" runat="server" Text="Распределите адреса между старым и новым УДО" />
                        <br />
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFirstUdodAddresses_DetachDivide" runat="server" Text="Адреса первого УДО" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ListBox ID="lbxFirstUdodAddresses_DetachDivide" Width="300" runat="server" DataTextField="AddressStr" DataValueField="AddressId"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnLeftAddress_DetachDivide" runat="server" Text="<<" OnClick="btnLeft_Click"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnRightAddress_DetachDivide" runat="server" Text=">>" OnClick="btnRight_Click"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSecondUdodAddresses_DetachDivide" runat="server" Text="Адреса второго УДО" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ListBox ID="lbxSecondUdodAddresses_DetachDivide" Width="300" runat="server" DataTextField="AddressStr" DataValueField="AddressId"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>

                <ajaxToolkit:TabPanel ID="TeacherDistribution_DetachDivide" runat="server" 
                    HeaderText="<span style='color: black;'>Распределение педагогов</span>"
                    Enabled="true"
                    ScrollBars="Auto" >
                    <ContentTemplate>
                        <asp:UpdatePanel ID="upTe" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        <asp:Label ID="lblTeacherDistribution_DetachDivide" runat="server" Text="Распределите педагогов между старым и новым УДО" />
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFirstUdodTeachers_DetachDivide" runat="server" Text="Педагоги первого УДО" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ListBox ID="lbxFirstUdodTeachers_DetachDivide" Width="300" runat="server" DataTextField="Fio" DataValueField="UserId"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnLeftTeacher_DetachDivide" runat="server" Text="<<" OnClick="btnLeft_Click"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnRightTeacher_DetachDivide" runat="server" Text=">>" OnClick="btnRight_Click"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSecondUdodTeachers_DetachDivide" runat="server" Text="Педагоги второго УДО" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ListBox ID="lbxSecondUdodTeachers_DetachDivide" Width="300" runat="server" DataTextField="Fio" DataValueField="UserId"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>

                <ajaxToolkit:TabPanel ID="MetroDistribution_DetachDivide" runat="server" 
                    HeaderText="<span style='color: black;'>Распределение метро</span>"
                    Enabled="true"
                    ScrollBars="Auto" >
                    <ContentTemplate>
                        <asp:UpdatePanel ID="upMe" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        <asp:Label ID="lblMetroDistribution_DetachDivide" runat="server" Text="Распределите станции метро между старым и новым УДО" />
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFirstUdodMetro_DetachDivide" runat="server" Text="Метро первого УДО" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ListBox ID="lbxFirstUdodMetro_DetachDivide" Width="300" runat="server" DataTextField="Name" DataValueField="MetroId"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnLeftMetro_DetachDivide" runat="server" Text="<<" OnClick="btnLeft_Click"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnRightMetro_DetachDivide" runat="server" Text=">>" OnClick="btnRight_Click"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSecondUdodMetro_DetachDivide" runat="server" Text="Метро второго УДО" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ListBox ID="lbxSecondUdodMetro_DetachDivide" Width="300" runat="server" DataTextField="Name" DataValueField="MetroId"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>

                <ajaxToolkit:TabPanel ID="PassportUdod_DetachDivide" runat="server" 
                    HeaderText="<span style='color: black;'>Паспорт УДО</span>"
                    Enabled="true"
                    ScrollBars="Auto" >
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblUdodPassportFirst_DetachDivide" runat="server" Text="Заполните данные о новом УДО" />
                                    <uct:UdodPassport ID="udodPassportFirst_DetachDivide" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblUdodPassportSecond_DetachDivide" runat="server" Text="Заполните данные о втором новом УДО" />
                                    <uct:UdodPassport ID="udodPassportSecond_DetachDivide" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="upButtons" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="btnPrevious_Click" Visible="false"><div class="btnBlue">Назад</div></asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="lnkNext" runat="server" OnClick="btnNext_Click"><div class="btnBlue">Дальше</div></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkReturn" runat="server" OnClick="btnReturn_Click"><div class="btnBlue">К списку УДО</div></asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="lnkDone" runat="server" OnClick="btnDone_Click" Visible="false" ValidationGroup="78"><div class="btnBlue">Готово</div></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
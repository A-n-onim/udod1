﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Udod_TransferPupilList : BasePage
{
    protected Guid? PupilId { set { ViewState["pupilId"] = value; } get { if (ViewState["pupilId"] == null) return null;
        return (Guid) ViewState["pupilId"];
    } }
    private bool? IsEdit
    {
        get
        {
            if (ViewState["IsEdit"] == null) return null;
            return (bool)ViewState["IsEdit"];
        }
        set
        {
            ViewState["IsEdit"] = value;
        }
    } 
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsUdodEmployee);
        if (UserContext.Roles.RoleId==(int)EnumRoles.UdodEmployee)
        {
            UserContext.UdodId = UserContext.Profile.UdodId;
            cityUdodFilter.Visible = false;
        }

        
        if (!Page.IsPostBack )
        {
            if (cityUdodFilter.SelectedUdod.HasValue)
            {
                using (ChildUnionDb db = new ChildUnionDb())
                {
                    var list =
                        db.GetChildUnionsShort(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null,
                                               null, null,
                                               null, null, null, null, null, null, null, null, null, null, false).
                            OrderBy(
                                i => i.Name);
                    ddlDOZach.DataSource = list;
                    ddlDOZach.DataBind();
                    upChildUnionGroup.Update();
                }
            }
        }
    }

    protected void ddlDOZach_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlDOZach.SelectedValue))
        {
            using (AgeGroupDb db = new AgeGroupDb())
            {
                ddlGroupsForTransfer.DataSource = db.GetGroupsPaging(cityUdodFilter.SelectedUdod, null,
                                                                     Convert.ToInt64(
                                                                         ddlDOZach.SelectedValue), null,
                                                                     null, null, null, 1, 1000, false).OrderBy(i => i.Name);
                ddlGroupsForTransfer.DataBind();

            }
        }
        upChildUnionGroup.Update();
    }

    protected void ddlGroupsForTransfer_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlGroupsForTransfer.SelectedItem.Text != "Автогруппа")
        {
            lnkTransfer.Visible = lnkTransfer.Enabled = true;
            using (AgeGroupDb db = new AgeGroupDb())
            {
                var group = db.GetGroup(Convert.ToInt64(ddlGroupsForTransfer.SelectedValue));
                StringBuilder sb = new StringBuilder();
                foreach (var teacher in group.Teachers)
                {
                    sb.Append(teacher.Fio);
                    sb.Append(" ");
                }
                string teachers = sb.ToString();
                sb = new StringBuilder();
                foreach (var year in group.NumYears)
                {
                    ddlNumYear.Items.Add(new ListItem(year.ToString(),year.ToString()));
                    sb.Append(year);
                    sb.Append(" ");
                }
                pGroupinfo.Visible = true;
                lblGroupInfo.Text = string.Format("Возраст: от {1} до {2}, Педагоги: {3}, Года обучения: {4}",
                                                  group.Name, group.AgeStart, group.AgeEnd, teachers, sb.ToString());
            }

        }
        else
        {
            lnkTransfer.Visible= lnkTransfer.Enabled = false;

        }
        upChildUnionGroup.Update();
    }

    protected void ddlGroupsForTransfer_OnDataBound(object sender, EventArgs e)
    {
        if (ddlGroupsForTransfer.Items.Count > 0) ddlGroupsForTransfer_OnSelectedIndexChanged(sender, e);
        else pGroupinfo.Visible = false;
    }

    protected void lnkTransfer_OnClick(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlDOZach.SelectedValue))
        {
            using (TransferDb db1 = new TransferDb())
            {
                long? childUnionId = null;
                try
                {
                    childUnionId = Convert.ToInt64(ddlDOZach.SelectedValue);
                }
                catch (Exception)
                {
                }

                int? numYear = null;
                int? typeBudgetId = null;
                long? groupfilterId = null;
                try
                {
                    groupfilterId = Convert.ToInt64(ddlGroupsForTransfer.SelectedValue);
                }
                catch
                {
                }
                /*var selectedPupils =
                    db1.GetSelectedPupilCount(cityUdodFilter.SelectedCity.Value);
                long groupId = Convert.ToInt64(ddlGroupsForTransfer.SelectedValue);
                using (AgeGroupDb db = new AgeGroupDb())
                {
                    var group = db.GetGroup(groupId);
                    if (group.MaxCountPupil - group.CurrentCountPupil < selectedPupils)
                    {
                        erropPopup.ShowError("Ошибка", "В выбранной группе количество свободных мест меньше чем детей выделенных для зачисления", "", "ОК", false, true);
                        return;
                    }
                }
                */


                var list = db1.TransferPupilInNewDO(UserContext.UdodId.Value, groupfilterId.Value, Convert.ToInt32(ddlNumYear.SelectedValue));
                gvTransferPupilList.DataBind();
                upTransferPupilList.Update();
                if (list.Count > 0)
                {
                    MyStringBuilder sb = new MyStringBuilder("<br/>", list.Select(i => i.ErrorMessage));
                    erropPopup.ShowError("Ошибка", sb.ToString(), "", "ОК", false, true);
                    upChildUnionGroup.Update();
                    return;
                }
            }
        }
        
        //upChildUnionGroup.Update();
        upTransferPupilList.Update();
    }


    protected void pgrTransferPupilList_OnChange(object sender, EventArgs e)
    {
        gvTransferPupilList.DataBind();
        upTransferPupilList.Update();
    }

    protected void cityUdodFilter_OnCityChange(object sender, EventArgs e)
    {
        using (PupilsDb db = new PupilsDb())
        {
            upFilters.Update();
        }

        gvTransferPupilList.DataBind();
        upTransferPupilList.Update();
    }

    protected void cityUdodFilter_OnUdodChange(object sender, EventArgs e)
    {
        if (cityUdodFilter.SelectedUdod.HasValue)
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                var list =
                    db.GetChildUnionsShort(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null,
                                           null, null,
                                           null, null, null, null, null, null, null, null, null, null, false).
                        OrderBy(
                            i => i.Name);
                ddlDOZach.DataSource = list;
                ddlDOZach.DataBind();
                upChildUnionGroup.Update();
            }
        }
        gvTransferPupilList.DataBind();
        upTransferPupilList.Update();
    }

    protected void cityUdodFilter_OnloadComplete(object sender, EventArgs e)
    {
        
    }

    protected void lnkFindByName_OnClick(object sender, EventArgs e)
    {
        lblInfo.Text = "";
        gvTransferPupilList.DataBind();
        upTransferPupilList.Update();
    }


    protected void dsTransferPupilList_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        if (cityUdodFilter.SelectedUdod.HasValue)
        {
            long? childUnionId;
            try
            {
                childUnionId = Convert.ToInt64(ddlDO.SelectedValue);
            }
            catch
            {
                childUnionId = null;
            }
            int? numYear;
            try
            {
                numYear = Convert.ToInt32(ddlNumYearFilter.SelectedValue);
            }
            catch
            {
                numYear = null;
            }
            int? status;
            try
            {
                status = Convert.ToInt32(ddlTransferStatusFilter.SelectedValue);
            }
            catch
            {
                status = null;
            }

            long? ageGroupId;
            try
            {
                ageGroupId = Convert.ToInt64(ddlAgeGroup.SelectedValue);
            }
            catch
            {
                ageGroupId = null;
            }
            using (TransferDb db = new TransferDb())
            {
                pgrTransferPupilList.numElements = db.GetTransferPupilCount(cityUdodFilter.SelectedUdod.Value, childUnionId, numYear,
                    tbPupilLastName.Text, tbPupilFirstName.Text, tbPupilMiddleName.Text, status, ageGroupId);
            }
            upFilters.Update();

            upTransferPupilList.Update();
        }
        else
        {
            e.Cancel = true;
        }
    }

    protected void gvTransferPupilList_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType==DataControlRowType.DataRow)
        {
            // проверка на различные ошибки
            var item = (e.Row.DataItem as ExtendedTransferPupil);
            if (item != null)
            {
                System.Web.UI.WebControls.CheckBox cbx = (System.Web.UI.WebControls.CheckBox)e.Row.FindControl("cbxSelect");

                cbx.Attributes.Add("ageGroupPupilId", item.AgeGroupPupilId.ToString());
                cbx.Checked = item.TransferIsSelected;
            }
        }
    }


    protected void cbxSelect_OnCheckedChanged(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.CheckBox cbx = (System.Web.UI.WebControls.CheckBox) sender;
        long ageGroupPupilId = Convert.ToInt32(cbx.Attributes["ageGroupPupilId"]);
        using (TransferDb db = new TransferDb())
        {
            db.TransferCheckAgeGroupPupil(ageGroupPupilId, cbx.Checked);
        }
    
    }

    protected void ddlDO_OnDataBound(object sender, EventArgs e)
    {
        ddlDO.Items.Insert(0, new ListItem("Все", ""));
    }

    protected void ddlTransferStatus_OnDataBound(object sender, EventArgs e)
    {
        ddlTransferStatusFilter.Items.Insert(0, new ListItem("Все", ""));
    }

    protected void ddlDO_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {
            long? childUnionId;
            try
            {
                childUnionId = Convert.ToInt64(ddlDO.SelectedValue);
            }
            catch
            {
                childUnionId = null;
            }
            
            ddlNumYearFilter.Items.Clear();

            if (childUnionId.HasValue)
            {
                ExtendedChildUnion childunion = db.GetChildUnion(childUnionId, null);
                ddlNumYearFilter.Items.Insert(0, new ListItem("Все", ""));
                for (int i = 1; i <= childunion.NumYears; i++)
                {
                    ddlNumYearFilter.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }

                using (AgeGroupDb db1 = new AgeGroupDb())
                {
                    ddlAgeGroup.DataSource = db1.GetGroupsPaging(cityUdodFilter.SelectedUdod, null,
                                                                     childUnionId, null,
                                                                     null, null, null, 1, 1000, false).OrderBy(i => i.Name);
                    ddlAgeGroup.DataBind();
                }
            }
            else
            {
                ddlAgeGroup.Items.Clear();
            }
            
            upFilters.Update();
        }

        
    }

    protected void lnkDeSelect_OnClick(object sender, EventArgs e)
    {
        using (TransferDb db = new TransferDb())
        {
            db.TransferDeSelect(UserContext.UdodId.Value);
        }
        gvTransferPupilList.DataBind();
        upTransferPupilList.Update();
    }

    protected void lnkToTransfer_OnClick(object sender, EventArgs e)
    {
        using (TransferDb db = new TransferDb())
        {
            db.ChangeStatusInSelected(UserContext.UdodId.Value, 2);
        }
        gvTransferPupilList.DataBind();
        upTransferPupilList.Update();
    }

    protected void lnkToDismis_OnClick(object sender, EventArgs e)
    {
        using (TransferDb db = new TransferDb())
        {
            db.ChangeStatusInSelected(UserContext.UdodId.Value, 3);
        }
        gvTransferPupilList.DataBind();
        upTransferPupilList.Update();
    }

    protected void lnkToRepeat_OnClick(object sender, EventArgs e)
    {
        using (TransferDb db = new TransferDb())
        {
            db.ChangeStatusInSelected(UserContext.UdodId.Value, 6);
        }
        gvTransferPupilList.DataBind();
        upTransferPupilList.Update();
    }

    protected void lnkExportExcel_OnClick(object sender, EventArgs e)
    {
        if (cityUdodFilter.SelectedUdod.HasValue)
        {
            HttpContext.Current.Response.Clear();

            string filename = string.Format("TransferPupil.xls");

            HttpContext.Current.Response.AddHeader("content-disposition",
                                                   string.Format("attachment; filename={0}", filename));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
            //workbook.Open(MapPath("../Templates/ExportPupilsByChildrens.xlsx"));
            Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];

            #region Стили
            Aspose.Cells.Style style_area = workbook.Styles[0];
            Aspose.Cells.Style style_header = workbook.Styles[1];
            style_area.ShrinkToFit = true;

            style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
            style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
            style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
                style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
            style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
                style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
            style_header.Font.IsBold = true;
            style_header.Font.Size = 12;
            style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
            style_header.IsTextWrapped = true;
            style_header.BackgroundColor = System.Drawing.Color.Gray;
            style_header.Pattern = Aspose.Cells.BackgroundType.Solid;
            #endregion

            long? childUnionId;
            try
            {
                childUnionId = Convert.ToInt64(ddlDO.SelectedValue);
            }
            catch
            {
                childUnionId = null;
            }
            int? numYear;
            try
            {
                numYear = Convert.ToInt32(ddlNumYearFilter.SelectedValue);
            }
            catch
            {
                numYear = null;
            }
            int? status;
            try
            {
                status = Convert.ToInt32(ddlTransferStatusFilter.SelectedValue);
            }
            catch
            {
                status = null;
            }
            long? ageGroupId;
            try
            {
                ageGroupId = Convert.ToInt64(ddlAgeGroup.SelectedValue);
            }
            catch
            {
                ageGroupId = null;
            }
            using (TransferDb db = new TransferDb())
            {

                var list = db.GetTransferPupilPaging(cityUdodFilter.SelectedUdod.Value, childUnionId, numYear,
                                                     tbPupilLastName.Text, tbPupilFirstName.Text, tbPupilMiddleName.Text,
                                                     status, ageGroupId, 1, 50000);
                int i = 0;
                sheet.Cells[i, 0].PutValue("№");
                sheet.Cells[i, 1].PutValue("ФИО");
                sheet.Cells[i, 2].PutValue("Дата рождения");
                sheet.Cells[i, 3].PutValue("Детское объединение");
                sheet.Cells[i, 4].PutValue("Статус ДО");
                sheet.Cells[i, 5].PutValue("Название группы");
                sheet.Cells[i, 6].PutValue("Статус группы");
                sheet.Cells[i, 7].PutValue("Вид услуги");
                sheet.Cells[i, 8].PutValue("Текущий год обучения");
                sheet.Cells[i, 9].PutValue("Период обучения");
                sheet.Cells[i, 10].PutValue("Возможные возраста");
                sheet.Cells[i, 11].PutValue("Возраст обуч-ся на 1 сентября");
                sheet.Cells[i, 12].PutValue("Статус");
                i++;
                foreach (var pupil in list)
                {
                    sheet.Cells[i, 0].PutValue(i);
                    sheet.Cells[i, 1].PutValue(pupil.PupilFio);
                    sheet.Cells[i, 2].PutValue(pupil.PupilBirthday.ToShortDateString());
                    sheet.Cells[i, 3].PutValue(pupil.ChildUnion.Name);
                    sheet.Cells[i, 4].PutValue(pupil.StatusDO==1?"Будет удалено":"Работает");
                    sheet.Cells[i, 5].PutValue(pupil.NameGroup);
                    sheet.Cells[i, 6].PutValue(pupil.Statusgroup==1?"Будет удалено":"Работает");
                    sheet.Cells[i, 7].PutValue(pupil.DictTypeBudgetId==1?"Платно":"Бесплатно");
                    sheet.Cells[i, 8].PutValue(pupil.CurrentNumYear);
                    sheet.Cells[i, 9].PutValue(pupil.TotalNumYear);
                    sheet.Cells[i, 10].PutValue(pupil.ChildUnionAges);
                    sheet.Cells[i, 11].PutValue(pupil.PupilYearsOnSeptFirst);
                    sheet.Cells[i, 12].PutValue(pupil.TransferStatus.TransferStatusName);
                    i++;
                }
                sheet.AutoFitColumns();
                
                sheet.Cells.SetRowHeightPixel(0, 40);

                sheet.Cells.CreateRange(0, 0, 1, 12).ApplyStyle(style_header, new Aspose.Cells.StyleFlag() { All = true });
                sheet.Cells.CreateRange(1, 0, i - 1, 12).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
                
            }


            workbook.Save(filename, Aspose.Cells.FileFormatType.Excel2003,
                          Aspose.Cells.SaveType.OpenInExcel, Response);
            HttpContext.Current.Response.End();
        }
    }

    protected void lnkRecalculate_OnClick(object sender, EventArgs e)
    {
        using (TransferDb db = new TransferDb())
        {
            if (UserContext.UdodId.HasValue)
            {
                db.RecalculateUdod(UserContext.UdodId.Value);
                gvTransferPupilList.DataBind();
                upTransferPupilList.Update();
            }
        }
    }

    protected void ddlAgeGroup_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        
    }

    protected void ddlAgeGroup_OnDataBound(object sender, EventArgs e)
    {
        ddlAgeGroup.Items.Insert(0, new ListItem("Все", ""));
    }
}
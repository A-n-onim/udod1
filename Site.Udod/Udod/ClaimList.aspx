﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="ClaimList.aspx.cs" Inherits="Udod_ClaimList" %>

<%@ Register src="../UserControl/Filter/ucCityUdodFilter_Vertical.ascx" tagName="UdodFilter" tagPrefix="uct" %>
<%@ Register src="../UserControl/Udod/ucUdodFilter.ascx" tagName="StatusFilter" tagPrefix="uct" %>
<%@ Register src="../UserControl/Menu/ucPager.ascx" tagName="Pager" tagPrefix="uc" %>
<%@ Register TagPrefix="uct" TagName="ErrorPopup" Src="~/UserControl/Menu/ucErrorPopup.ascx" %>

<asp:Content runat="server" ID="contentClaimList" ContentPlaceHolderID="body">


<asp:LinkButton runat="server" ID="lnkAddClaim" OnClick="lnkAddClaim_OnClick"><div class="btnBlue">Добавить заявление</div></asp:LinkButton>
<asp:LinkButton runat="server" ID="lnkSendClaim" OnClick="lnkSendClaim_OnClick" Visible="false"> <div class="btnBlue">отправить заявление</div></asp:LinkButton>

<table border=0 width="100%">
    <tr style="height: 280px;" ID="trFilters" runat="server">
        <td>
            <asp:Panel runat="server" ID="pFindUdod" Height="280"  GroupingText="<b><div style='font-size:14pt'>Фильтр по УДО</div></b>">
                <div style="height: 230px;" ID="pFindUdodDiv" runat="server">
                <table border=0 class="media_dgc">
                    <tr>
                        <td colspan="4">
                            <asp:UpdatePanel ID="upCityUdodFilter" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <uct:UdodFilter ID="filterUdod" runat="server" UdodDdlEnabled="true" AddAllProperty="true" OnCityChange="filterUdod_OnCityChange" OnUdodChange="filterUdod_OnUdodChange" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:4px">
                            ФИО педагога:
                        </td>
                        <td align="right" style="padding-right:4px">    
                            <asp:UpdatePanel ID="upTeacher" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="inputShort">
                                        <asp:DropDownList ValidationGroup="88" CssClass="input" runat="server" ID="ddlTeacher" DataTextField="Fio" DataValueField="UserId" OnDataBound="ddlTeacher_OnDataBound" AutoPostBack="true" OnSelectedIndexChanged="ddlTeacher_OnSelectedIndexChanged"/>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:4px">
                            Детское объединение:
                        </td>
                        <td align="right" style="padding-right:4px">    
                            <asp:UpdatePanel ID="upDO" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="inputShort">
                                        <asp:DropDownList ValidationGroup="88" CssClass="input" runat="server" ID="ddlDO" DataTextField="Name" DataValueField="ChildUnionId" OnDataBound="ddlDO_OnDataBound" AutoPostBack="true" OnSelectedIndexChanged="ddlDO_OnSelectedIndexChanged"/>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" >
                            <uct:StatusFilter ID="filterStatus" runat="server" OnChange="filterStatus_Changed"/>
                        </td>
                    </tr>
                </table>
                </div>
            </asp:Panel>
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="upCalendar" UpdateMode="Conditional">
            <ContentTemplate>
            <asp:Panel DefaultButton="lnkRefresh" runat="server" Height="280" ID="pfilter" GroupingText="<b><div style='font-size:14pt'>Поиск заявлений</div></b>">
                <div style="height: 230px;" ID="pfilterDiv" runat="server">
                <table border=0 class="media_dgc">
                    <tr>
                        <td >
                            Номер заявления:
                        </td>
                        <td>
                            <div class="inputXShort">
                                <asp:TextBox runat="server" ID="tbNumber" CssClass="inputXShort" />
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" FilterType="Numbers" TargetControlID="tbNumber" runat="server" />
                            </div>
                        </td>
                        <td >
                            Фамилия:
                        </td>
                        <td>
                            <div class="inputXShort">
                                <asp:TextBox runat="server" ID="tbLastName" CssClass="inputXShort" />
                            </div>
                        </td>
                        <td runat="server" ID="td1" colspan="2">
                            <asp:RadioButtonList runat="server" ID="rblBirthday" AutoPostBack="true" OnSelectedIndexChanged="rblBirthday_OnSelectedIndexChanged">
                                <Items>
                                    <asp:ListItem runat="server" Value="0" Selected="true">Поиск ДР по периоду</asp:ListItem>
                                    <asp:ListItem runat="server" Value="1">Поиск ДР по дате</asp:ListItem>
                                </Items>
                            </asp:RadioButtonList>
                        </td>
                        
                    </tr>
                    <tr>
                        <td >
                            Дата регистрации: от
                        </td>
                        <td>
                            <div class="inputXShort">
                                <asp:TextBox runat="server" ID="tbDateStart" CssClass="inputXShort" />
                            </div>
                            <ajaxToolkit:CalendarExtender runat="server" ID="calendarDateStart" TargetControlID="tbDateStart" Format="dd.MM.yyyy"/>
                            <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbDateStart" Mask="99/99/9999" MaskType="Date"  />
                            <asp:RangeValidator runat="server" ID="dateVal1" ValidationGroup="88" ErrorMessage="Неверная дата" ControlToValidate="tbDateStart" MinimumValue="01.01.1900" MaximumValue="01.01.3000" Type="Date" Display="Dynamic"></asp:RangeValidator>
                        </td>
                        <td >
                            Имя:
                        </td>
                        <td >
                            <div class="inputXShort">
                                <asp:TextBox runat="server" ID="tbFirstName" CssClass="inputXShort" />
                            </div>
                        </td>
                        <td runat="server" ID="td3" >
                            Дата рождения от:
                        </td>
                        <td runat="server" ID="td4">
                            <div class="inputXShort">
                                <asp:TextBox runat="server" ID="tbBirthdayBegin" CssClass="inputXShort" />
                            </div>
                            <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="tbBirthdayBegin" Format="dd.MM.yyyy"/>
                            <ajaxToolkit:MaskedEditExtender ID="mee2" runat="server" TargetControlID="tbBirthdayBegin" Mask="99/99/9999" MaskType="Date" />
                            <asp:RangeValidator runat="server" ID="dateVal2" ValidationGroup="88" ErrorMessage="Неверная дата" ControlToValidate="tbBirthdayBegin" MinimumValue="01.01.1900" MaximumValue="01.01.3000" Type="Date" Display="Dynamic"></asp:RangeValidator>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            до
                        </td>
                        <td>
                            <div class="inputXShort">
                                <asp:TextBox runat="server" ID="tbDateEnd" CssClass="inputXShort" />
                            </div>
                            <ajaxToolkit:CalendarExtender runat="server" ID="calendarDateEnd" TargetControlID="tbDateEnd" Format="dd.MM.yyyy"/>
                            <ajaxToolkit:MaskedEditExtender ID="mee3" runat="server" TargetControlID="tbDateEnd" Mask="99/99/9999" MaskType="Date" />
                            <asp:RangeValidator runat="server" ID="dateVal3" ValidationGroup="88" ErrorMessage="Неверная дата" ControlToValidate="tbDateEnd" MinimumValue="01.01.1900" MaximumValue="01.01.3000" Type="Date" Display="Dynamic"></asp:RangeValidator>
                        </td>
                        <td >
                            Отчество:
                        </td>
                        <td>
                            <div class="inputXShort">
                                <asp:TextBox runat="server" ID="tbMiddleName" CssClass="inputXShort" />
                            </div>
                        </td>
                        
                        <td runat="server" ID="td6">
                            Дата рождения до:
                        </td>
                        <td runat="server" ID="td7">
                            <div class="inputXShort">
                                <asp:TextBox runat="server" ID="tbBirthdayEnd" CssClass="inputXShort" />
                            </div>
                            <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender3" TargetControlID="tbBirthdayEnd" Format="dd.MM.yyyy"/>
                            <ajaxToolkit:MaskedEditExtender ID="mee4" runat="server" TargetControlID="tbBirthdayEnd" Mask="99/99/9999" MaskType="Date" />
                            <asp:RangeValidator runat="server" ID="dateVal4" ValidationGroup="88" ErrorMessage="Неверная дата" ControlToValidate="tbBirthdayEnd" MinimumValue="01.01.1900" MaximumValue="01.01.3000" Type="Date" Display="Dynamic"></asp:RangeValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:CheckBox runat="server" ID="cbxRegDateEnabled" Text="Любая дата регистрации" AutoPostBack="true" OnCheckedChanged="cbxRegDateEnabled_OnCheckedChanged"/>
                        </td>
                        <td colspan="4" align="right">
                            <asp:LinkButton runat="server" ID="lnkRefresh" OnClick="lnkRefresh_OnClick" ValidationGroup="88"><div class="btnBlue">Поиск заявления</div></asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:LinkButton runat="server" ID="lnkExportClaim" OnClick="lnkExportClaim_OnClick"><div class="btnBlue">Выгрузка в Excel</div></asp:LinkButton>
                        </td>
                        <td colspan="5" align="right">
                            <asp:LinkButton runat="server" ID="lnkClear" OnClick="lnkClear_OnClick"><div class="btnBlue">Сбросить фильтры</div></asp:LinkButton>
                        </td>
                    </tr>
                </table>
                </div>
            </asp:Panel> 
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger runat="server" ControlID="lnkExportClaim"/>
            </Triggers>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>

<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upClaimList">
<ContentTemplate>
<asp:GridView runat="server" DataSourceID="dsClaimList" ID="gvClaimList" CssClass="dgc" AutoGenerateColumns="false" DataKeyNames="ClaimId" 
OnSelectedIndexChanged="gvClaimList_OnSelectedIndexChanged" OnRowUpdating="gvClaimList_OnRowUpdating" OnRowDataBound="gvClaimList_OnRowDataBound" >
    <Columns>
        <asp:TemplateField HeaderText="№" >
            <ItemTemplate><%# (gvClaimList.Rows.Count + pgrClaimList.PagerIndex) %></ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="Number" HeaderText="№ заяв." ReadOnly="true" HeaderStyle-Width="30px" >
            <HeaderStyle Width="30px" />
        </asp:BoundField>
        <asp:TemplateField HeaderText="ФИО будущего обучающе гося" >
            <ItemTemplate>
                <%# Eval("Pupil.Fio") %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Дата рождения" >
            <ItemTemplate>
                <%# ((DateTime)Eval("Pupil.Birthday")).ToShortDateString()%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="ФИО заявителя" >
            <ItemTemplate>
                <%# Eval("Parent.Fio") %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Наименование УДО">
            <ItemTemplate>
                <%# Eval("Udod.Name") %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Наименование ДО">
            <ItemTemplate>
                <%# Eval("ChildUnion.Name")%>
            </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Вид деятельности">
            <ItemTemplate>
                <%# Eval("ChildUnion.Section.Name")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Статус" >
            <ItemTemplate>
                <%# Eval("ClaimStatus.ClaimStatusName")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Дата начала обучения" >
            <ItemTemplate>
                <%# ((DateTime)Eval("BeginDate")).ToShortDateString()%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Дата регистрации" >
            <ItemTemplate>
                <%# ((DateTime)Eval("CreatedDate")).ToShortDateString()%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Операции" >
            <ItemTemplate>
                <asp:LinkButton runat="server" ID="lnkOpen" OnClick="lnkOpen_OnClick"><div class="btnOpen" title="Открыть"></div></asp:LinkButton>
                <asp:LinkButton runat="server" ID="lnkChangeStatus4" OnClick="ChangeStatus_OnClick"><div class="btnDoc" title="Поданы документы"></div></asp:LinkButton>
                <asp:LinkButton runat="server" ID="lnkChangeStatus2" OnClick="lnkChangeStatus2_OnClick"><div class="btnZach" title="Зачислить"></div></asp:LinkButton>
                <%-- <asp:LinkButton runat="server" ID="lnkChangeStatus5" OnClick="ChangeStatus_OnClick"><div class="btnReserv" title="Перевести в резерв"></div></asp:LinkButton>--%>
                <asp:LinkButton runat="server" ID="lnkChangeStatus3" OnClick="ChangeStatus_OnClick"><div class="btnAnnul" title="Аннулировать"></div></asp:LinkButton>
                <asp:LinkButton runat="server" ID="lnkNewClaimFromOld" OnClick="lnkNewClaimFromOld_OnClick"><div class="btnOpen1" title="Создать заявление на основе текущей"></div></asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="ddlDO" />
    <asp:AsyncPostBackTrigger ControlID="filterUdod" />
    <asp:AsyncPostBackTrigger ControlID="filterStatus" />
    <asp:AsyncPostBackTrigger ControlID="lnkTypeBudgetOk" />
    <asp:AsyncPostBackTrigger ControlID="lnkAnnulConfirm" />
</Triggers>
</asp:UpdatePanel>

<uc:Pager runat="server" ID="pgrClaimList" needStartDots="false" needEndDots="false" pagesCount="10"/>

<asp:ObjectDataSource runat="server" ID="dsClaimList" TypeName="Udod.Dal.ClaimDb" SelectMethod="GetClaims" UpdateMethod="UpdateClaimStatus" OnLoad="dsClaimList_OnLoad" OnUpdating="dsClaimList_OnUpdating" OnSelecting="ds_Selecting">
    <SelectParameters>
        <asp:ControlParameter Name="udodId" ControlID="filterUdod" PropertyName="SelectedUdod" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="cityId" ControlID="filterUdod" PropertyName="SelectedCity" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="claimStatusId" ControlID="filterStatus" PropertyName="DictClaimStatusId" ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="userId" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="numberStr" ControlID="tbNumber" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="lastName" ControlID="tbLastName" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="firstName" ControlID="tbFirstName" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="middleName" ControlID="tbMiddleName" ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="udodName" ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="childUnion" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="startDate" ControlID="tbDateStart" DbType="DateTime" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="endDate" ControlID="tbDateEnd" DbType="DateTime" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="childUnionId" ControlID="ddlDO" DbType="Int64" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="teacherId" ControlID="ddlTeacher" PropertyName="SelectedValue" ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="sectionId" ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="sectionName" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="birthdayBegin" ControlID="tbBirthdayBegin" DbType="DateTime"  ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="birthdayEnd" ControlID="tbBirthdayEnd" DbType="DateTime" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="birthdayFindType" ControlID="rblBirthday" PropertyName="SelectedValue" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pagerIndex" ControlID="pgrClaimList" ConvertEmptyStringToNull="true" PropertyName="PagerIndex" />
        <asp:ControlParameter Name="pagerLength" ControlID="pgrClaimList" ConvertEmptyStringToNull="true" PropertyName="PagerLength" />
    </SelectParameters>    
    <UpdateParameters>
        <asp:Parameter  Name="claimId" ConvertEmptyStringToNull="true"/>
        <asp:Parameter  Name="statusId" ConvertEmptyStringToNull="true"/>
    </UpdateParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource runat="server" ID="dsClaimStatuses" TypeName="Udod.Dal.ClaimDb" SelectMethod="GetClaimStatuses"></asp:ObjectDataSource>

<asp:Panel runat="server" ID="popupLimit" CssClass="modalPopup" style="display: none; border: 2px solid black; width: 400px;" BackColor="White">
    <asp:Panel ID="pLimitHeader" runat="server" >
        <asp:Label ID="Label4" Text="<span class='headerCaptions'>Внимание</span>" runat="server"/>
        <asp:LinkButton ID="LinkButton4" runat="server" 
            OnClientClick="$find('LimitPopup').hide(); return false;" />
    </asp:Panel>
    <div >
        <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:UpdatePanel runat="server" ID="upLimit" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label runat="server" ID="lblLimit"></asp:Label>
            </ContentTemplate>
            </asp:UpdatePanel>
                
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div >

    <asp:LinkButton ID="btnCancelLimit" runat="server" CausesValidation="false"><div class='btnBlue'>OK</div></asp:LinkButton>
        
    </div>
</asp:Panel>

<ajaxToolkit:ModalPopupExtender runat="server" ID="popupLimitExtender" PopupDragHandleControlID="pLimitHeader"
    PopupControlID="popupLimit" CancelControlID="btnCancelLimit"
    TargetControlID="btnShowLimit" RepositionMode="None" BackgroundCssClass="modalBackground"
    BehaviorID="LimitPopup  "/>
<asp:Button ID="btnShowLimit" runat="server" style="display:none" />

<asp:Panel runat="server" ID="popupTypeBudget" CssClass="modalPopup" style="display: none; border: 2px solid black; width: 400px;" BackColor="White">
        <asp:Panel ID="pTypeBudgetHeader" runat="server" >
           <asp:Label ID="Label5" Text="<span class='headerCaptions'>Выбор группы</span>" runat="server"/>
           <asp:LinkButton ID="LinkButton5" runat="server" 
               OnClientClick="$find('TypeBudgetPopup').hide(); return false;" />
        </asp:Panel>
        <div >
            <asp:UpdatePanel runat="server" ID="upGroups" UpdateMode="Always">
                <ContentTemplate>
                    <asp:DropDownList runat="server" ID="ddlGroups" AutoPostBack="true" CssClass="input" DataTextField="Name" DataValueField="UdodAgeGroupId" OnSelectedIndexChanged="ddlGroups_OnSelectedIndexChanged"/>
                    <asp:RequiredFieldValidator runat="server" ID="reqVal" ControlToValidate="ddlGroups" ErrorMessage="Выберите группу" SetFocusOnError="true" ValidationGroup="100"></asp:RequiredFieldValidator>
                    <br/>
                    <asp:Panel runat="server" ID="pGroupinfo" GroupingText="информация о группе">
                    <asp:Label runat="server" ID="lblGroupInfo"></asp:Label>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div >
            <table>
                <tr>
                    <td><asp:LinkButton ID="lnkTypeBudgetOk" runat="server" CausesValidation="true" ValidationGroup="100" OnClick="lnkTypeBudgetOk_OnClick"><div class='btnBlue'>OK</div></asp:LinkButton></td>
                    <td><asp:LinkButton ID="lnkTypeBudgetCancel" runat="server" CausesValidation="false"><div class='btnBlue'>Отмена</div></asp:LinkButton></td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupTypeBudgetExtender" PopupDragHandleControlID="pTypeBudgetHeader"
        PopupControlID="popupTypeBudget" CancelControlID="lnkTypeBudgetCancel"
        TargetControlID="btnShowTypeBudget" RepositionMode="None" BackgroundCssClass="modalBackground"
        BehaviorID="TypeBudgetPopup"/>
    <asp:Button ID="btnShowTypeBudget" runat="server" style="display:none" />
    <asp:ObjectDataSource runat="server" ID="dsTypeBudget" TypeName="Udod.Dal.DictBudgetTypeDb" SelectMethod="GetBudgetTypes"></asp:ObjectDataSource>

    <asp:Panel runat="server" ID="pAnnul" CssClass="modalPopup" style="display: none; border: 2px solid black; width: 400px;" BackColor="White">
        <asp:Panel ID="pAnnulHeader" runat="server" >
           <asp:Label ID="Label1" Text="<span class='headerCaptions'>Выбор причины аннулирования</span>" runat="server"/>
           <asp:LinkButton ID="LinkButton1" runat="server" 
               OnClientClick="$find('TypeBudgetPopup').hide(); return false;" />
        </asp:Panel>
        <div >
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                <asp:RadioButtonList runat="server" ID="rbAnnul" DataTextField="TypeBudgetName" DataValueField="DictTypeBudgetId" />
                <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="RequiredFieldValidator1" ErrorMessage="Не выбрана причина аннулирования" ControlToValidate="rbAnnul" SetFocusOnError="true" ValidationGroup="234"></asp:RequiredFieldValidator>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div >
            <table>
                <tr>
                    <td><asp:LinkButton ID="lnkAnnulConfirm" runat="server" CausesValidation="true" ValidationGroup="234" OnClick="lnkAnnulConfirm_OnClick"><div class='btnBlue'>OK</div></asp:LinkButton></td>
                    <td><asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="false"><div class='btnBlue'>Отмена</div></asp:LinkButton></td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="modalPopupAnnul" PopupDragHandleControlID="pAnnulHeader"
        PopupControlID="pAnnul" CancelControlID="LinkButton3"
        TargetControlID="Button1" RepositionMode="None" BackgroundCssClass="modalBackground"
        BehaviorID="AnnulPopup"/>
    <asp:Button ID="Button1" runat="server" style="display:none" />
    
    <asp:Panel runat="server" ID="p1" CssClass="modalPopup" style="display: none; border: 2px solid black; width: 600px;" BackColor="White">
        <asp:Panel ID="Panel4" runat="server" >
           <asp:Label ID="Label9" Text="<span class='headerCaptions'>Внимание</span>" runat="server"/>
           <asp:LinkButton ID="LinkButton9" runat="server" 
               OnClientClick="$find('Status1Popup').hide(); return false;" />
        </asp:Panel>
        <div>
            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    Желаемая дата зачисления ещё не наступила. Считать данного ребёнка обучающимся в текущий период времени и выполнить зачисление сейчас? Или отметить его к зачислению на следующий учебный год?
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div >
            <table>
                <tr>
                    <td><asp:LinkButton ID="lnkzach" runat="server" OnClick="ChangeStatus_OnClick"><div class='btnBlue'>Зачислить</div></asp:LinkButton></td>
                    <td><asp:LinkButton ID="lnkzachnextyear" runat="server" OnClick="ChangeStatus_OnClick"><div class='btnBlue'>Зачислить на след.г.</div></asp:LinkButton></td>
                    <td><asp:LinkButton ID="lnkcancel1" runat="server" CausesValidation="false"><div class='btnBlue'>Отмена</div></asp:LinkButton></td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="ModalPopupStatus1" PopupDragHandleControlID="Panel4"
        PopupControlID="p1" CancelControlID="lnkcancel1"
        TargetControlID="Button2" RepositionMode="None" BackgroundCssClass="modalBackground"
        BehaviorID="Status1Popup"/>
    <asp:Button ID="Button2" runat="server" style="display:none" />
    

    <uct:ErrorPopup ID="errorClaimList" runat="server" Width="450"></uct:ErrorPopup> 
</asp:Content>
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="PupilList.aspx.cs" Inherits="Udod_PupilList" %>
<%@ Register TagPrefix="uct" TagName="Fio" Src="~/UserControl/User/ucFIO_vertical.ascx" %>
<%@ Register TagPrefix="uct" TagName="Document" Src="~/UserControl/User/ucDocument_Vertical.ascx" %>
<%@ Register TagPrefix="uct" TagName="Udod" Src="~/UserControl/Udod/ucFindUdod.ascx" %>
<%@ Register TagPrefix="uct" TagName="PupilEdit" Src="~/UserControl/Pupil/ucPupilEdit.ascx" %>
<%@ Register src="../UserControl/Filter/ucCityUdodFilter.ascx" tagName="CityUdodFilter" tagPrefix="uct" %>
<%@ Register TagPrefix="uct" TagName="Address" Src="~/UserControl/Address/ucNewAddress.ascx" %>
<%@ Register src="../UserControl/Udod/ucDOPassport.ascx" tagName="DOPassport" tagPrefix="uct" %>
<%@ Register src="../UserControl/Menu/ucPager.ascx" tagName="Pager" tagPrefix="uc" %>
<%@ Register src="../UserControl/Menu/ucErrorPopup.ascx" tagName="ErrorPopup" tagPrefix="uct" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content runat="server" ID="content" ContentPlaceHolderID="body">
    <script type="text/javascript">
    //$('<%="#"+popupEditPupil.ClientID %>').css('visibility', 'visible');
    //$('<%="#"+popupEditPupil.ClientID %>').css('display', 'block');
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoad1);
    function pageLoad1() {
        var html = document.documentElement;
        //alert(html.scrollTop);
        //alert($('<%="#"+gvPupilList.ClientID %>').attr('isview'));
        if ($.browser.msie && ($.browser.version == '7.0' || $.browser.version == '8.0')) {
            if (($('<%="#"+gvPupilList.ClientID %>').attr('isview') == 'true')) {
                $('<%="#"+popupEditPupil.ClientID %>').css('visibility', 'visible');
                $('<%="#"+popupEditPupil.ClientID %>').css('display', 'block');
            }
            else {
                $('<%="#"+popupEditPupil.ClientID %>').css('visibility', 'hide');
                $('<%="#"+popupEditPupil.ClientID %>').css('display', 'none');
            }
            
        }
        if (parseInt(html.scrollTop) < 100) {
            
            //$('<%="#"+popupChangeDO.ClientID %>').css('position', 'absolute');
            //$('<%="#"+popupChangeDO.ClientID %>').addClass('popupTop');
            $('<%="#"+popupPassportDO.ClientID %>').css('position', 'absolute');
            $('<%="#"+popupPassportDO.ClientID %>').addClass('popupTop');
            //alert($('<%="#"+popupChangeDO.ClientID %>').css('top'));
        }
        else {
            //$('<%="#"+popupChangeDO.ClientID %>').removeClass('popupTop');
            $('<%="#"+popupPassportDO.ClientID %>').removeClass('popupTop');
        }
    }
</script>


<asp:UpdatePanel ID="upCityUdodFilter" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <uct:CityUdodFilter ID="cityUdodFilter" runat="server" UdodDdlEnabled="true" AddAllProperty="true" OnCityChange="cityUdodFilter_OnUdodChange" OnUdodChange="cityUdodFilter_OnUdodChange" OnloadComplete="cityUdodFilter_onLoadComplete"/>
    </ContentTemplate>
</asp:UpdatePanel>
    
<asp:UpdatePanel ID="upFilters" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel ID="Panel2" runat="server" DefaultButton="lnkFindByName" GroupingText="<b><div style='font-size:14pt'>Поиск обучающегося</h3></div></b>">      
        <table>
            <tr>
                <td>
                    Детское объединение:
                </td>
                <td>
                    <asp:DropDownList CssClass="input" runat="server" ID="ddlDO" DataTextField="Name" DataValueField="ChildUnionId" OnDataBound="ddlDO_OnDataBound" AutoPostBack="true" OnSelectedIndexChanged="ddlDO_OnSelectedIndexChanged"/>
                </td>
                <td>
                    Год обучения:
                </td>
                <td>
                    <asp:DropDownList CssClass="input" runat="server" ID="ddlPupilNumYear" AutoPostBack="true" OnSelectedIndexChanged="ddlPupilNumYear_OnSelectedIndexChanged" >
                        <asp:ListItem Text="Все" Value="" Selected="True" />
                        <asp:ListItem Text="1" Value="1" />
                        <asp:ListItem Text="2" Value="2" />
                        <asp:ListItem Text="3" Value="3" />
                        <asp:ListItem Text="4" Value="4" />
                        <asp:ListItem Text="5" Value="5" />
                        <asp:ListItem Text="6" Value="6" />
                        <asp:ListItem Text="7" Value="7" />
                        <asp:ListItem Text="8" Value="8" />
                        <asp:ListItem Text="9" Value="9" />
                        <asp:ListItem Text="10" Value="10" />
                        <asp:ListItem Text="11" Value="11" />
                        <asp:ListItem Text="12" Value="12" />
                        <asp:ListItem Text="13" Value="13" />
                        <asp:ListItem Text="14" Value="14" />
                        <asp:ListItem Text="15" Value="15" />
                    </asp:DropDownList>
                </td>
                <td>Группа:</td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlGroupsFilter" CssClass="input" AutoPostBack="true" DataTextField="Name" DataValueField="UdodAgeGroupId" OnSelectedIndexChanged="ddlPupilNumYear_OnSelectedIndexChanged" OnDataBound="ddlGroupsFilter_OnDataBound">
                        <Items>
                            <asp:ListItem Value="">Выберите ДО</asp:ListItem>
                        </Items>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    ФИО педагога:
                </td>
                <td>
                    <asp:DropDownList CssClass="input" runat="server" ID="ddlTeacher" DataTextField="Fio" DataValueField="UserId" OnDataBound="ddlTeacher_OnDataBound" AutoPostBack="true" OnSelectedIndexChanged="ddlTeacher_OnSelectedIndexChanged"/>
                </td>
                <td>
                    Вид услуги:
                </td>
                <td>
                    <asp:DropDownList CssClass="input" runat="server" ID="ddlBudgetType" DataSourceID="dsBudgetType" DataTextField="TypeBudgetName" DataValueField="DictTypeBudgetId" AutoPostBack="true" OnSelectedIndexChanged="ddlBudgetType_OnSelectedIndexChanged" OnDataBound="ddlBudgetType_OnDataBound" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="cbxShowDelete" Text="Показывать только отчисленных"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPupilLastName" runat="server" Text="Фамилия:" />
                </td>
                <td>
                    <asp:Label ID="lblPupilFirstName" runat="server" Text="Имя:" />
                </td>
                <td>
                    <asp:Label ID="lblPupilMiddleName" runat="server" Text="Отчество:" />
                </td>
                <td></td>
                <td>
                    Сообщение:
                </td>
                <td>
                    <asp:DropDownList CssClass="input" runat="server" ID="ddlMessage" AutoPostBack="true" OnSelectedIndexChanged="ddlMessage_OnSelectedIndexChanged">
                        <asp:ListItem Text="Все" Value="0" Selected="True" />
                        <asp:ListItem Text="Пустое" Value="1" />
                        <asp:ListItem Text="Не пустое" Value="2" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbPupilLastName" runat="server" CssClass="input" />
                </td>
                <td>
                    <asp:TextBox ID="tbPupilFirstName" runat="server" CssClass="input" />
                </td>
                <td>
                    <asp:TextBox ID="tbPupilMiddleName" runat="server" CssClass="input" />
                </td>
                <td>
                    <asp:LinkButton ID="lnkFindByName" runat="server" OnClick="lnkFindByName_OnClick"><div class="btnBlue">Поиск</div></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="lnkClear" runat="server" OnClick="lnkClear_OnClick"><div class="btnBlue">Сбросить фильтры</div></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    
                </td>
            </tr>
        </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<table>
    <tr>
        <td>
            <asp:LinkButton runat="server" ID="lnkExportInExcel"  OnClick="lnkExportInExcel_OnClick" Visible="false"><div class="btnBlueLong">Экспорт в Excel</div></asp:LinkButton>
        </td>
        <td>
            <asp:LinkButton runat="server" ID="lnkExportByChildrens" OnClick="lnkExportByChildrens_OnClick" Visible="true"><div class="btnBlueLong">Выгрузка в Excel по детям</div></asp:LinkButton>
        </td>
        <td>
            <asp:LinkButton runat="server" ID="lnkExportbyChildUnions" OnClick="lnkExportByChildUnions_OnClick" Visible="true"><div class="btnBlueLong">Выгрузка в Excel по ДО</div></asp:LinkButton>
        </td>
    </tr>
    <tr>
        <td style="display: table">
            <asp:FileUpload runat="server" ID="upLoad" />
            <asp:LinkButton runat="server" ID="lnkTransfer" OnClick="lnkTransfer_OnClick" OnClientClick="javascript:return confirm('Вы уверены?');"><div class="btnBlueLong">Перевод в другое ДО</div></asp:LinkButton>
        </td>
        <td>
            <asp:LinkButton runat="server" ID="lnkUpdateReestrCode" OnClick="lnkUpdateReestrCode_OnClick" Visible="false" OnClientClick="javascript:return confirm('Вы уверены?');"><div class="btnBlueLong">Проверить всех в Реестре</div></asp:LinkButton>
        </td>
        
    </tr>
    <tr>
        <td>
            <asp:LinkButton runat="server" ID="lnkSendReestr" OnClick="lnkSendReestr_OnClick" Visible="false"><div class="btnBlueLong">Отправить всех в реестр</div></asp:LinkButton>
        </td>
        <td>
            <asp:LinkButton runat="server" ID="lnkReestrSendZach" OnClick="lnkReestrSendZach_OnClick" Visible="false"><div class="btnBlueLong">Послать зачисления в реестр</div></asp:LinkButton>
        </td>
    </tr>
</table>
<asp:UpdatePanel runat="server" ID="upPupilList" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:GridView runat="server" ID="gvPupilList" DataKeyNames="UserId" CssClass="dgc" AutoGenerateColumns="false" 
                        DataSourceID="dsPupilList" OnSelectedIndexChanged="gvPupilList_OnSelectedIndexChanged" OnRowDataBound="gvPupilList_OnRowDataBound" OnRowCommand="gvPupilList_OnRowCommand">
            <EmptyDataTemplate>
                <asp:Label ID="lblEmptyGvPupilList" runat="server" style="font-size:16px">Ученики не найдены</asp:Label>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="№" >
                    <ItemTemplate><%# (gvPupilList.Rows.Count + pgrPupilList.PagerIndex)%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ФИО обучающегося" >
                    <ItemTemplate>
                        <%# Eval("Fio")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Дата рождения" >
                    <ItemTemplate>
                        <%# ((DateTime)Eval("Birthday")).ToShortDateString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ФИО заявителя" >
                    <ItemTemplate>
                        <%# GetParents(Container.DataItem)%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Контактный телефон" >
                    <ItemTemplate>
                        <%# GetPhoneNumbers(Container.DataItem)%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Адрес регистрации" >
                    <ItemTemplate>
                        <%# Eval("AddressStr")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Сообщение" >
                    <ItemTemplate>
                        <%# Eval("ErrorMessage")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="true" SelectText="<img width='20px' height='20px' src='../Images/Edit.png' border=0 title='Редактирование'>"/>
                <asp:CommandField ShowEditButton="true" EditText="<img width='20px' height='20px' src='../Images/contact_icon.png' border=0 title='Создать заявление'>"/>
            </Columns>
            <PagerStyle CssClass="gridpager" VerticalAlign="Bottom" HorizontalAlign="Left"/>
            <PagerSettings Mode="Numeric" Position="Bottom" PageButtonCount="10" />
        </asp:GridView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnSavePupil"/>
        <asp:AsyncPostBackTrigger ControlID="btnCancel"/>
        <asp:AsyncPostBackTrigger ControlID="ddlDO" />
        <asp:AsyncPostBackTrigger ControlID="ddlTeacher" />
    </Triggers>
</asp:UpdatePanel>
<img width="" height=""/>
<uc:Pager runat="server" ID="pgrPupilList" needStartDots="false" needEndDots="false" pagesCount="10" OnChange="pgrPupilList_OnChange"/>

<asp:ObjectDataSource runat="server" ID="dsBudgetType" TypeName="Udod.Dal.DictBudgetTypeDb" SelectMethod="GetBudgetTypes" />
<asp:ObjectDataSource runat="server" ID="dsSchoolType" TypeName="Udod.Dal.DictSchoolDb" SelectMethod="GetSchoolType" />

<asp:ObjectDataSource runat="server" ID="dsPupilList" TypeName="Udod.Dal.PupilsDb" SelectMethod="GetPupilsPaging" OnSelecting="ds_Selecting">
    <SelectParameters>
        <asp:ControlParameter Name="cityId" ControlID="cityUdodFilter" PropertyName="SelectedCity" ConvertEmptyStringToNull="true" />
        <asp:ControlParameter Name="udodId" ControlID="cityUdodFilter" PropertyName="SelectedUdod" ConvertEmptyStringToNull="true" />
        <asp:ControlParameter Name="childUnionId" ControlID="ddlDO" PropertyName="SelectedValue" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="teacherId" ControlID="ddlTeacher" PropertyName="SelectedValue" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pupilLastName" ControlID="tbPupilLastName" PropertyName="Text" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pupilFirstName" ControlID="tbPupilFirstName" PropertyName="Text" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pupilMiddleName" ControlID="tbPupilMiddleName" PropertyName="Text" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pagerIndex" ControlID="pgrPupilList" ConvertEmptyStringToNull="true" PropertyName="PagerIndex" />
        <asp:ControlParameter Name="pagerLength" ControlID="pgrPupilList" ConvertEmptyStringToNull="true" PropertyName="PagerLength" />
        <asp:ControlParameter Name="message" ControlID="ddlMessage" ConvertEmptyStringToNull="true" PropertyName="SelectedValue" />
        <asp:ControlParameter Name="pupilNumYear" ControlID="ddlPupilNumYear" ConvertEmptyStringToNull="true" PropertyName="SelectedValue" />
        <asp:ControlParameter Name="budgetTypeId" ControlID="ddlBudgetType" ConvertEmptyStringToNull="true" PropertyName="SelectedValue" />
        <asp:ControlParameter Name="groupId" ControlID="ddlGroupsFilter" ConvertEmptyStringToNull="true" PropertyName="SelectedValue" />
        <asp:ControlParameter Name="showDelete" ControlID="cbxShowDelete" PropertyName="Checked" DefaultValue="false"/>
    </SelectParameters>
</asp:ObjectDataSource>

<asp:Panel runat="server" ID="popupEditPupil" style="padding: 5px; display: none; border: 2px solid black;width: 810px;" BackColor="White">
    <asp:Panel ID="pEditPupilHeader" runat="server" >
        <asp:Label ID="litTitle" Text="<span class='headerCaptions'>Редактирование обучающегося</span>" runat="server"/>
        <asp:LinkButton ID="lnkClose" runat="server" 
            OnClientClick="$find('EditPupilPopup').hide(); return false;" />
    </asp:Panel>
    <asp:UpdatePanel ID="upEditPupil" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 800px;">
                <ajaxToolkit:TabContainer runat="server" ID="tabEditPupil" ActiveTabIndex="0">
                    <ajaxToolkit:TabPanel runat="server" ID="pPupil" HeaderText="<span style='color: black;'>Обучающийся</span>" >
                        <ContentTemplate>
                            <asp:UpdatePanel ID="upPupilInfo" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td colspan=2 >
                                                <uct:Fio ID="PupilFio" runat="server" ValidatorEnabled="true"  />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdx">
                                                Дата рождения:
                                            </td>
                                            <td><div class="inputShort">
                                                    <asp:TextBox runat="server" ID="tbBirthday" CssClass="inputShort" />
                                                </div>
                                                <ajaxToolkit:CalendarExtender runat="server" ID="calendar" TargetControlID="tbBirthday" Animated="false" Format="dd.MM.yyyy"/>
                                                <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbBirthday" Mask="99/99/9999" MaskType="Date" />
                                                <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="reqvalrDate" ErrorMessage="Поле 'Дата рождения' не заполнено" ControlToValidate="tbBirthday" SetFocusOnError="true" ValidationGroup="71"></asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="valrDate" runat="server" ControlToValidate="tbBirthday" MinimumValue="01.08.2012" MaximumValue="01.08.3012" Type="Date" text="Возраст обучающегося должен быть<br>в пределах от 3 лет до 21 года<br>" Display="Dynamic" ValidationGroup="50"/>
                                                <asp:RangeValidator ID="valrDate2" runat="server" ControlToValidate="tbBirthday" MinimumValue="01.08.1812" MaximumValue="01.08.3012" Type="Date" text="Дата рождения не может быть<br>позднее Сегодня" Display="Dynamic" ValidationGroup="50"/>
                                                <%-- <asp:RangeValidator ID="valrDate3" runat="server" ControlToValidate="tbBirthday" MinimumValue="01.08.1812" MaximumValue="01.08.3012" Type="Date" text="Заявителю должно быть больше 14 лет" Display="Dynamic" ValidationGroup="50"/>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdx">
                                                Пол:
                                            </td>
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="cbSex" RepeatDirection="Horizontal">
                                                    <Items>
                                                        <asp:ListItem Selected="true" Value="0">М</asp:ListItem>
                                                        <asp:ListItem Value="1">Ж</asp:ListItem>
                                                    </Items>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <uct:Document ID="PupilDocument" runat="server"  ValidatorEnabled="true"  /> 
                                                <asp:Label runat="server" ID="lblPupilPassportDataError" ForeColor="Red"></asp:Label>
                                                <asp:LinkButton runat="server" ID="lnkLoadFromReestr" OnClick="lnkLoadFromReestr_OnClick" ValidationGroup="10"><div class="btnBlue">Загрузить из реестра</div></asp:LinkButton>
                                                <asp:UpdatePanel runat="server" ID="upErrorLoad" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Label runat="server" ID="lblErrorLoad" ForeColor="red"></asp:Label>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr runat="server" id="tr1">
                                            <td>
                                                Номер телефона 1
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbPupilPhoneNumber" CssClass="input"></asp:TextBox>
                                                <ajaxToolkit:MaskedEditExtender ID="mee2" runat="server" TargetControlID="tbPupilPhoneNumber" Mask="8(999)999-99-99" MaskType="Number" />
                                            </td>
                                        </tr>
                                        <tr runat="server" id="tr2">
                                            <td>
                                                Номер телефона 2
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbPupilExpressPhoneNumber" CssClass="input"></asp:TextBox>
                                                <ajaxToolkit:MaskedEditExtender ID="mee2_1" runat="server" TargetControlID="tbPupilExpressPhoneNumber" Mask="8(999)999-99-99" MaskType="Number" />
                                            </td>
                                        </tr>
                                        <tr runat="server" id="tr3">
                                            <td>
                                                EMail
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbPupilEmail" CssClass="input"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Школа
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbPupilSchool" CssClass="inputLong"></asp:TextBox>
                                                <ajaxToolkit:AutoCompleteExtender runat="server" ID="acSchool" TargetControlID="tbPupilSchool" ServiceMethod="GetSchool" EnableCaching="true" CompletionSetCount="20" MinimumPrefixLength="1" UseContextKey="true" BehaviorID="AutoCompleteEx" CompletionListCssClass="addresslist"/>
                                                <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="RequiredFieldValidator1" ErrorMessage="Поле 'Школа' не заполнено" ControlToValidate="tbPupilSchool" SetFocusOnError="true" ValidationGroup="71"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdx">
                                                Класс
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbPupilClass" CssClass="inputLong"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdx">
                                                Тип школы
                                            </td>
                                            <td class="tdx">
                                                <asp:UpdatePanel runat="server" ID="upSchoolType">
                                                <ContentTemplate>
                                                <asp:DropDownList CssClass="input" runat="server" ID="ddlSchoolType" DataSourceID="dsSchoolType" DataTextField="DictSchoolTypeName" DataValueField="DictSchoolTypeId" OnDataBound="ddlSchoolType_OnDataBound" OnSelectedIndexChanged="ddlSchoolType_OnSelectedIndexChanged" AutoPostBack="true"/>
                                                <asp:Label runat="server" ID="lblSchoolType" Visible="false" Enabled="false">Принадлежность к государственному ОУ не может быть указана вручную, а должна быть подтверждена данными из Реестра контингента. Нажмите кнопку "загрузить из Реестра", для получения обновлённых данных на ребёнка. Если в результате загрузки данные на ребёнка не найдены, то обратитесь в то ОУ, где ребёнок получает общее образование, с просьбой повторно выгрузить данные в Реестр контингента.</asp:Label>
                                                </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel runat="server" ID="pParent" HeaderText="<span style='color: black;'>Представитель</span>">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="upParentInfo" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                <div style="overflow: scroll;">
                                    <asp:Repeater runat="server" ID="parentRepeater" OnItemDataBound="parentRepeater_OnItemDataBound">
                                        <HeaderTemplate>
                                            <table border=0><tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td colspan="2">
                                                            <uct:Fio runat="server" ID="ParentFio" ValidatorEnabled="true"  />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table border=0>
                                                                <tr>
                                                                    <td class="tdx">
                                                                        Заявитель:
                                                                    </td>
                                                                    <td >
                                                                        <asp:RadioButtonList runat="server" ID="cbRelative" RepeatDirection="Horizontal" DataTextField="UserTypeName" DataValueField="UserTypeId"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <uct:Document ID="parentDocument" runat="server" ValidatorEnabled="true" />
                                                            <asp:Label runat="server" ID="lblParentPassportDataError" ForeColor="Red"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Номер телефона 1
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="tbParentPhoneNumber" CssClass="input"></asp:TextBox>
                                                            <ajaxToolkit:MaskedEditExtender ID="mee3" runat="server" TargetControlID="tbParentPhoneNumber" Mask="8(999)999-99-99" MaskType="Number" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Номер телефона 2
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="tbParentExpressPhoneNumber" CssClass="input"></asp:TextBox>
                                                            <ajaxToolkit:MaskedEditExtender ID="mee3_1" runat="server" TargetControlID="tbParentExpressPhoneNumber" Mask="8(999)999-99-99" MaskType="Number" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            EMail
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="tbParentEmail" CssClass="input"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan=2>
                                                            <asp:LinkButton runat="server" ID="lnkDeleteParent" Visible="false" OnClientClick="javascript:return confirm('Вы действительно хотите удалить представителя? Внимание отменить данное дествие будет невозможно!');" OnClick="lnkDeleteParent_OnClick"><div class="btnRed">Удалить представителя</div></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tr></table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel runat="server" ID="pAddress" HeaderText="<span style='color: black;'>Адрес</span>">
                        <ContentTemplate>
                            <asp:UpdatePanel runat="server" ID="upAddresses" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" ID="lblAddressStr"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="display: none;">
                                                <table width="200">
                                                    <tr>
                                                        <td>
                                                            Адрес регистрации и адрес фактического пребывания совпадают?
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButtonList runat="server" ID="rbnFacRegAddress" AutoPostBack="true" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbnFacRegAddress_SelectedChanged">
                                                                <Items>
                                                                    <asp:ListItem Value="False">Нет</asp:ListItem>
                                                                    <asp:ListItem Selected="true" Value="True">Да</asp:ListItem>
                                                                </Items>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <asp:Panel ID="RegAddressPanel" runat="server" GroupingText="Адрес регистрации" style="width: 300px;">
                                                    <uct:Address runat="server" ID="RegAddress" AddressTypeId="5" />
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel runat="server" ID="pUdod" HeaderText="<span style='color: black;'>Посещаемые детские объединения</span>">
                        <ContentTemplate>
                            <div style="overflow: scroll; height: 500px;">
                                <asp:UpdatePanel runat="server" ID="upPupilUdods" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table>
                                            <tr>
                                                <td>Общее количество посещаемых УДО: </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblUdodInfo"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Общее количество посещаемых ДО: </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblChildUnionInfo"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Общее количество часов в неделю: </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblHoursInfo"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td>Нормативно одобренное суммарное кол-во часов: </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblNormHoursInfo"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                        <asp:GridView runat="server" ID="gvPupilUdods" CssClass="dgc" AutoGenerateColumns="false" OnSelectedIndexChanged="gvPupilUdods_OnSelectedIndexChanged" OnRowDataBound="gvPupilUdods_OnRowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="№" >
                                                    <ItemTemplate><%# (gvPupilUdods.Rows.Count + 1)%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Наименование УДО">
                                                    <ItemTemplate>
                                                        <%# Eval("UdodShortName") %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Наименование детского объединения">
                                                    <ItemTemplate>
                                                        <%# Eval("Name") %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Направление">
                                                    <ItemTemplate>
                                                        <%# Eval("Program.Name") %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Вид деятельности">
                                                    <ItemTemplate>
                                                        <%# Eval("Section.Name")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Срок обучения">
                                                    <ItemTemplate>
                                                        <%#  Eval("NumYears")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Возраст обучающихся">
                                                    <ItemTemplate>
                                                        <%#  " от " + Eval("AgeStart") +" до " +Eval("AgeEnd")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    
                                                <asp:TemplateField HeaderText="Возрастная группа">
                                                    <ItemTemplate>
                                                        <%#  Eval("AgeGroups[0].Name")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Год обучения">
                                                    <ItemTemplate>
                                                        <%#  Eval("PupilNumYear")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Вид услуги">
                                                    <ItemTemplate>
                                                        <%#  Eval("PupilBudgetType.TypeBudgetName")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lnkEditUdod" OnClick="lnkEditUdod_OnClick"><img width='20px' height='20px' src='../Images/Edit.png' border=0 title='Редактирование'></asp:LinkButton>        
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lnkDeleteUdod" OnClick="lnkDeleteUdod_OnClick"><img src='../Images/delete_smth.png' border=0 title='Отчислить'></asp:LinkButton>        
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel runat="server" ID="pHistory" HeaderText="<span style='color: black;'>История обучающегося</span>">
                        <ContentTemplate>
                            <div style="overflow: scroll; height: 500px;">
                                <asp:UpdatePanel runat="server" ID="upPupilHistory" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Panel GroupingText="Обозначения" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Статус 1 - На рассмотрении.
                                                    </td>
                                                    <td>
                                                        Статус 4 - Зачислен.
                                                    </td>
                                                    <td>
                                                        Статус 5 - Аннулировано.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Статус 6 - Переведено в резерв.
                                                    </td>
                                                    <td>
                                                        Статус 7 - Поданы документы.
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:GridView runat="server" ID="gvPupilHistory" CssClass="dgc" AutoGenerateColumns="false" OnSelectedIndexChanged="gvPupilHistory_OnSelectedIndexChanged" OnRowDataBound="gvPupilHistory_OnRowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="№" >
                                                    <ItemTemplate><%# (gvPupilHistory.Rows.Count + 1)%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Дата/время события">
                                                    <ItemTemplate>
                                                        <%# Eval("EventDate") %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="№ заявления">
                                                    <ItemTemplate>
                                                        <%# Eval("Claim.Number") %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Наименование УДО">
                                                    <ItemTemplate>
                                                        <%# Eval("Udod.ShortName") %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Наименование ДО">
                                                    <ItemTemplate>
                                                        <%# Eval("ChildUnion.Name")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Событие">
                                                    <ItemTemplate>
                                                        <%#  Eval("EventType.Name")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Сообщение">
                                                    <ItemTemplate>
                                                        <%#  Eval("Message")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ФИО пользователя">
                                                    <ItemTemplate>
                                                        <%#  Eval("User.Fio")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Операции" >
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lnkOpenClaim" OnClick="OpenClaim_OnClick"><div class="btnOpen" title="Открыть"></div></asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lnkCreateClaim" OnClick="CreateClaim_OnClick"><div class="btnOpen" title="Создать заявление на основе текущей"></div></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table>
        <tr>
            <td>
                <asp:LinkButton ID="btnSavePupil" ValidationGroup="50" runat="server" Text="<div class='btnBlue'>Сохранить</div>" CausesValidation="true" OnClick="btnSavePupil_OnClick"/>
            </td>
            <td>
                <asp:LinkButton ID="btnCancel" runat="server" Text="<div class='btnBlue'>Отменить</div>" CausesValidation="false" OnClick="btnCancelPupil_OnClick"/>
            </td>
        </tr>
    </table>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender runat="server" ID="popupEditPupilExtender" PopupDragHandleControlID="pEditPupilHeader"
    PopupControlID="popupEditPupil"
    TargetControlID="btnEditPupilPopup" RepositionMode="None"
    BehaviorID="EditPupilPopup"/>
<asp:Button ID="btnEditPupilPopup" runat="server" style="display:none" />

<asp:Panel runat="server" ID="popupChangeDO" style="padding: 5px; border: 2px solid black" BackColor="White">
    <asp:Panel ID="pChangeDOHeader" runat="server" >
        <asp:Label ID="Label1" Text="<span class='headerCaptions'>Выбор детских объединений</span>" runat="server"/>
        <asp:LinkButton ID="LinkButton1" runat="server" 
            OnClientClick="$find('ChangeDOPopup').hide(); return false;" />
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upChangeDO" UpdateMode="Conditional">
        <ContentTemplate>
            <table>
                <tr>
                    <td colspan=2>
                        <asp:Panel ID="Panel1" runat="server" GroupingText="<b><div style='font-size:14pt'>Параметры для зачисления</div></b>" >
                        <table>
                            <tr>
                                <td>
                                    Детское объединение:
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="input" runat="server" ID="ddlChangeDO_DO" DataTextField="Name" DataValueField="ChildUnionId" OnDataBound="ddlChangeDO_DO_OnDataBound" AutoPostBack="true" OnSelectedIndexChanged="ddlChangeDO_DO_OnSelectedIndexChanged"/>
                                </td>
                                <td>
                                    Группа
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlGroups" CssClass="input" AutoPostBack="true" DataTextField="Name" DataValueField="UdodAgeGroupId" OnSelectedIndexChanged="ddlGroups_OnSelectedIndexChanged"/>
                                </td>
                                <td>Год обучения</td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlChangeDO_NY" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan=2>
                        <asp:Panel runat="server" ID="pChilUnionInfo" GroupingText="<b><div style='font-size:14pt'>Краткая информация о выбранном ДО </div></b>" >
                            <asp:Label runat="server" ID="lblChangeDO_ChildUnionInfo"></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td><asp:LinkButton ID="lnkChangeDOOk" runat="server" CausesValidation="false" OnClick="lnkChangeDOOk_OnClick"><div class='btnBlue'>OK</div></asp:LinkButton></td>
                    <td><asp:LinkButton ID="lnkChangeDOCancel" runat="server" CausesValidation="false" OnClick="lnkChangeDOCancel_OnClick"><div class='btnBlue'>Отмена</div></asp:LinkButton></td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender runat="server" ID="popupChangeDOExtender" PopupDragHandleControlID="pChangeDOHeader"
    PopupControlID="popupChangeDO" CancelControlID="lnkChangeDOCancel"
    TargetControlID="btnShowChangeDOPopup" RepositionMode="None"
    BehaviorID="ChangeDOPopup"/>
<asp:Button ID="btnShowChangeDOPopup" runat="server" style="display:none" />    


<asp:Panel runat="server" ID="popupDeleteUdod" style="display: none; border: 2px solid black" BackColor="White">
    <asp:Panel ID="pDeleteUdodHeader" runat="server" >
        <asp:Label ID="Label2" Text="<span class='headerCaptions'>Отчисление из ДО</span>" runat="server"/>
        <asp:LinkButton ID="LinkButton2" runat="server" 
            OnClientClick="$find('DeleteUdodPopup').hide(); return false;" />
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upDeleteUdod" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 270px;">
                <asp:Panel runat="server" ID="pDelete" GroupingText="Причина отчисления">
                    <asp:TextBox runat="server" ID="tbReason" TextMode="MultiLine" Rows="4" MaxLength="500" Style="width: 100%"></asp:TextBox>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div >
        <asp:LinkButton runat="server" ID="btnDeleteUdod" Text="<div class='btnBlue'>Отчислить</div>" CausesValidation="false" OnClick="btnDeleteUdod_OnClick"/>
        <asp:LinkButton ID="btnCancelDeleteUdod" runat="server" Text="<div class='btnBlue'>Отменить</div>" CausesValidation="false" />
    </div>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender runat="server" ID="popupDeleteUdodExtender" PopupDragHandleControlID="pDeleteUdodHeader"
    PopupControlID="popupDeleteUdod" CancelControlID="btnCancelDeleteUdod"
    TargetControlID="btnShowDeleteUdodPopup" RepositionMode="None"
    BehaviorID="DeleteUdodPopup"/>
<asp:Button ID="btnShowDeleteUdodPopup" runat="server" style="display:none" /> 


<asp:Panel runat="server" ID="popupPassportDO" style="padding: 5px; display: none; border: 2px solid black" BackColor="White">
    <asp:Panel ID="pPassportDOHeader" runat="server" >
        <asp:Label ID="Label5" Text="<span class='headerCaptions'>Паспорт детского объединения</span>" runat="server"/>
        <asp:LinkButton ID="LinkButton3" runat="server" 
            OnClientClick="$find('PassportDOPopup').hide(); return false;" />
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upPassportDO" UpdateMode="Conditional">
        <ContentTemplate>
            <div >
                <uct:DOPassport runat="server" ID="selectDO" Edit="false"/>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:LinkButton ID="btnCancelPassportDO" runat="server" CausesValidation="false"><div class="btnBlue">Закрыть</div></asp:LinkButton>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender runat="server" ID="popupPassportDOExtender" PopupDragHandleControlID="pPassportDOHeader"
    PopupControlID="popupPassportDO" CancelControlID="btnCancelPassportDO"
    TargetControlID="btnShowPassportDO" RepositionMode="None"
    BehaviorID="PassportDOPopup"/>
<asp:Button ID="btnShowPassportDO" runat="server" style="display:none" />


    <asp:Panel runat="server" ID="popupMergePupil" CssClass="modalPopup" style="display: none; border: 2px solid black; width: 400px;" BackColor="White">
        <asp:Panel ID="pMergePupilHeader" runat="server" >
           <asp:Label ID="Label4" Text="<span class='headerCaptions'>Внимание</span>" runat="server"/>
           <asp:LinkButton ID="LinkButton4" runat="server" 
               OnClientClick="$find('MergePupilPopup').hide(); return false;" />
        </asp:Panel>
        <asp:UpdatePanel ID="upLnkMerge" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div >
                    <asp:Label ID="lblMergePupil" runat="server" Text="Ваши коррективы привели к тому, что информация по данному ребёнку(родителю) полностью совпали с данными уже существующего в базе ребёнка(родителя). Считать их одним и тем же человеком? Если Вы нажмёте 'Да', то данные по обоим записям будут объединены в одну. Внимание! Отменить данное действие будет невозможно!" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div >
            <table>
                <tr>
                    <td><asp:LinkButton ID="lnkMergePupilOk" runat="server" CausesValidation="false" OnClick="lnkMergePupilOk_OnClick"><div class='btnBlue'>Да</div></asp:LinkButton></td>
                    <td><asp:LinkButton ID="lnkMergePupilCancel" runat="server" CausesValidation="false"><div class='btnBlue'>Нет</div></asp:LinkButton></td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupMergePupilExtender" PopupDragHandleControlID="pMergePupilHeader"
        PopupControlID="popupMergePupil" CancelControlID="lnkMergePupilCancel"
        TargetControlID="btnShowMergePupil" RepositionMode="None" BackgroundCssClass="modalBackground"
        BehaviorID="MergePupilPopup"/>
    <asp:Button ID="btnShowMergePupil" runat="server" style="display:none" />

    <asp:UpdatePanel ID="upErrorPopup" runat="server" UpdateMode="Always" >
    <ContentTemplate>
        <uct:ErrorPopup ID="errorPopup" runat="server" Width="450px"/>
    </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>
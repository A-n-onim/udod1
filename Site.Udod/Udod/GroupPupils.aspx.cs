﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class Udod_GroupPupils : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsUdodEmployee || UserContext.Roles.IsOsip);
        if (!Page.IsPostBack)
        {
            Panel3.Visible = !UserContext.Roles.IsOsip;
        }
    }
    protected void ddlDO_OnDataBound(object sender, EventArgs e)
    {
        if (ddlDO.Items.Count>0)
        {
            ddlDO_OnSelectedIndexChanged(sender, e);
        }
        /*if (ddlDO.Items.Count == 0)
        {
            ddlDO.Items.Insert(0, new ListItem("Выберите УДО", ""));
        }
        else
        {
            ddlDO.Items.Insert(0, new ListItem("Все", ""));
        }*/
    }
    protected void ddlDO_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        pgrPupilList.Clear();
        PupilListDatabind();
        lnkTransfer.Visible= ddlGroupsForTransfer.Visible =!string.IsNullOrEmpty(ddlDO.SelectedValue);
        if (!string.IsNullOrEmpty(ddlDO.SelectedValue))
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                var childunion = db.GetChildUnion(Convert.ToInt64(ddlDO.SelectedValue), null);
                int numYear = childunion.NumYears;
                ddlPupilNumYear.Items.Clear();
                ddlPupilNumYear.Items.Add(new ListItem("Все", ""));
                for (int i = 1; i <= numYear; i++)
                {
                    ddlPupilNumYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                ddlBudgetType.Items.Clear();
                ddlBudgetType.DataSourceID = "";
                ddlBudgetType.DataSource = childunion.BudgetTypes;
                ddlBudgetType.DataBind();
                
            }
            using (AgeGroupDb db = new AgeGroupDb())
            {
                ddlDOZach.SelectedIndex = ddlDO.SelectedIndex;

                ddlGroupsFilter.DataSource = 
                ddlGroupsForTransfer.DataSource = db.GetGroupsPaging(cityUdodFilter.SelectedUdod, null,
                                                                     Convert.ToInt64(
                                                                         ddlDO.SelectedValue), null,
                                                                     null, null, null, 1, 1000, true).OrderBy(i => i.Name);
                ddlGroupsForTransfer.DataBind();
                ddlGroupsFilter.DataBind();
            }
        }
        else
        {
            ddlGroupsFilter.DataSource = new List<ExtendedGroup>();
            ddlGroupsFilter.DataBind();
            ddlBudgetType.DataSourceID = "dsBudgetType";
            ddlBudgetType.DataBind();
        }
        upFilters.Update();
        upChildUnionGroup.Update();
    }

    protected void ddlPupilNumYear_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        pgrPupilList.Clear();
        PupilListDatabind();
    }
    protected void ddlTeacher_OnDataBound(object sender, EventArgs e)
    {
        if (ddlTeacher.Items.Count == 0)
        {
            ddlTeacher.Items.Insert(0, new ListItem("Выберите УДО", ""));
        }
        else
        {
            ddlTeacher.Items.Insert(0, new ListItem("Все", ""));
        }
    }
    protected void ddlTeacher_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        pgrPupilList.Clear();
        Guid? teacherId = null;
        try
        {
            teacherId = new Guid(ddlTeacher.SelectedValue);
        }
        catch (Exception)
        {

        }
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var list =
                db.GetChildUnionsShort(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null, null, null,
                                       null, null, null, null, null, null, null, null, null, teacherId, false).OrderBy(
                                           i => i.Name);
            ddlDOZach.DataSource=ddlDO.DataSource = list;
            ddlDO.DataBind();
            ddlDOZach.DataBind();
            upFilters.Update();
        }
        PupilListDatabind();
    }
    protected void ddlBudgetType_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        pgrPupilList.Clear();
        PupilListDatabind();
    }
    protected void ddlBudgetType_OnDataBound(object sender, EventArgs e)
    {
        ddlBudgetType.Items.Insert(0, new ListItem("Все", ""));
    }
    protected void lnkFindByName_OnClick(object sender, EventArgs e)
    {
        pgrPupilList.Clear();
        PupilListDatabind();
    }
    protected void PupilListDatabind()
    {
        //Stopwatch sw = new Stopwatch();
        //sw.Start();
        gvPupilList.DataBind();
        upPupilList.Update();
        //sw.Stop();
        //long i = sw.ElapsedMilliseconds;
    }
    protected void pgrPupilList_OnChange(object sender, EventArgs e)
    {
        PupilListDatabind();
    }
    protected void ds_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        using (PupilsDb db = new PupilsDb())
        {
            long? childUnionId;
            try
            {
                childUnionId = Convert.ToInt64(ddlDO.SelectedValue);
            }
            catch
            {
                childUnionId = null;
            }

            Guid? teacherId;
            try
            {
                teacherId = new Guid(ddlTeacher.SelectedValue);
            }
            catch
            {
                teacherId = null;
            }

            int? pupilNumYear;
            try
            {
                pupilNumYear = Convert.ToInt32(ddlPupilNumYear.SelectedValue);
            }
            catch
            {
                pupilNumYear = null;
            }

            int? budgetTypeId;
            try
            {
                budgetTypeId = Convert.ToInt32(ddlBudgetType.SelectedValue);
            }
            catch
            {
                budgetTypeId = null;
            }
            long? groupId=null;
            try
            {
                groupId = Convert.ToInt64(ddlGroupsFilter.SelectedValue);
            }
            catch
            {
            }

            pgrPupilList.numElements = db.GetGroupPupilsCount(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, childUnionId,
                tbPupilLastName.Text, tbPupilFirstName.Text, tbPupilMiddleName.Text, teacherId, pupilNumYear, budgetTypeId, groupId);
        }
    }
    protected void cityUdodFilter_OnUdodChange(object sender, EventArgs e)
    {
        FiltersDatabind();
    }
    protected void FiltersDatabind()
    {
        if (cityUdodFilter.SelectedUdod.HasValue)
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                Guid? teacherId = null;
                try
                {
                    teacherId = new Guid(ddlTeacher.SelectedValue);
                }
                catch (Exception)
                {
                    
                }
                var list= db.GetChildUnionsShort(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null, null, null, null, null, null, null, null, null, null, null, null, teacherId,false).OrderBy(i => i.Name);
                ddlDO.DataSource = list;

                ddlDOZach.DataSource = db.GetChildUnionsShort(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null, null, null, null, null, null, null, null, null, null, null, null, null, false).OrderBy(i => i.Name);
                
            }
            using (TeacherDb db = new TeacherDb())
            {
                ddlTeacher.DataSource = db.GetTeacher(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null);
            }
            if (!string.IsNullOrEmpty(ddlDOZach.SelectedValue))
            {
                using (AgeGroupDb db = new AgeGroupDb())
                {
                    ddlGroupsForTransfer.DataSource = db.GetGroupsPaging(cityUdodFilter.SelectedUdod, null,
                                                                         Convert.ToInt64(
                                                                             ddlDOZach.SelectedValue), null,
                                                                         null, null, null, 1, 1000,true).OrderBy(i=>i.Name);
                
                }
            }
        }
        else
        {
            ddlDOZach.DataSource = ddlDO.DataSource = new ListItemCollection();
            ddlTeacher.DataSource = new ListItemCollection();
            ddlGroupsForTransfer.DataSource = new ListItemCollection();
            
        }
        //ddlChildunionForTransfer.DataBind();
        ddlGroupsForTransfer.DataBind();
        ddlDO.DataBind();
        ddlDOZach.DataBind();
        ddlTeacher.DataBind();
        upCityUdodFilter.Update();
        upFilters.Update();
        pgrPupilList.Clear();
        upChildUnionGroup.Update();
        PupilListDatabind();
    }
    protected void cityUdodFilter_onLoadComplete(object sender, EventArgs e)
    {
        FiltersDatabind();
    }

    protected void cbxSelect_OnCheckedChanged(object sender, EventArgs e)
    {
        // сохранить изменение выделения
        System.Web.UI.WebControls.CheckBox cbx = (System.Web.UI.WebControls.CheckBox)sender;
        long AgeGroupPupilId = Convert.ToInt64(cbx.Attributes["AgeGroupPupilId"]);
        
        using (PupilsDb db = new PupilsDb())
        {
            db.SelectGroupPupil(AgeGroupPupilId, ((CheckBox)sender).Checked);
        }
    }

    protected void gvPupilList_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var item = (e.Row.DataItem as ExtendedPupil);
            if (item != null)
            {
                System.Web.UI.WebControls.CheckBox cbx = (System.Web.UI.WebControls.CheckBox)e.Row.FindControl("cbxSelect");
                cbx.Attributes.Add("AgeGroupPupilId", item.AgeGroupPupilId.ToString());
                cbx.Checked = item.IsSelect;
            }
        }
    }

    protected void ddlChildunionForTransfer_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        using (AgeGroupDb db=new AgeGroupDb())
        {

            db.GetGroupsPaging(UserContext.UdodId, null, Convert.ToInt64(ddlDO.SelectedValue), null,
                               null, null, null, 1, 1000, null);
        }
    }

    protected void lnkTransfer_OnClick(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlDOZach.SelectedValue) && !string.IsNullOrEmpty(ddlDO.SelectedValue))
        {
            using (PupilsDb db1 = new PupilsDb())
            {
                long? childUnionId = null;
                try
                {
                    childUnionId = Convert.ToInt64(ddlDO.SelectedValue);
                }
                catch (Exception)
                {
                }

                int? numYear = null;
                try
                {
                    numYear = Convert.ToInt32(ddlPupilNumYear.SelectedValue);
                }
                catch (Exception)
                {
                }

                int? typeBudgetId = null;
                try
                {
                    typeBudgetId = Convert.ToInt32(ddlBudgetType.SelectedValue);
                }
                catch (Exception)
                {
                }
                long? groupfilterId = null;
                try
                {
                    groupfilterId = Convert.ToInt64(ddlGroupsFilter.SelectedValue);
                }
                catch
                {
                }
                var selectedPupils =
                    db1.GetGroupPupilsPaging(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, childUnionId,
                                             null, null,
                                             null, 1, 50000000, null, numYear, typeBudgetId, groupfilterId).Where(i => i.IsSelect).ToList();
                long groupId = Convert.ToInt64(ddlGroupsForTransfer.SelectedValue);
                using (AgeGroupDb db = new AgeGroupDb())
                {
                    var group = db.GetGroup(groupId);
                    if (group.MaxCountPupil-group.CurrentCountPupil<selectedPupils.Count())
                    {
                        erropPopup.ShowError("Ошибка", "В выбранной группе количество свободных мест меньше чем детей выделенных для зачисления", "", "ОК", false, true);
                        return;
                    }
                }

                

                var list = db1.TransferPupil(Convert.ToInt64(ddlDO.SelectedValue), Convert.ToInt64(ddlDOZach.SelectedValue), groupId, numYear, typeBudgetId, groupfilterId);
                if (ddlDO.SelectedValue != ddlDOZach.SelectedValue)
                {
                    // значит перевод из ДО в ДО и надо отправить в реестр
                    using (ChildUnionDb db2 = new ChildUnionDb())
                    {
                        var childunion = db2.GetChildUnion(Convert.ToInt64(ddlDOZach.SelectedValue), null);
                        long result = -1;
                        if (string.IsNullOrEmpty(childunion.ReestrCode))
                        {
                            var teachers = db2.GetTeacher(childunion.ChildUnionId);

                            //result = SiteUtility.ReestrAddDO(childunion, false, teachers);
                        }
                        else
                        {
                            result = Convert.ToInt64(childunion.ReestrCode);
                        }
                        foreach (ExtendedPupil pupil in selectedPupils)
                        {
                            if (list.Count(i => i.UserId == pupil.UserId) == 0 && pupil.ChildUnions.Count > 0 &&
                                !string.IsNullOrEmpty(pupil.ChildUnions[0].ReestrPupilAgeGroupId))
                            {
                                // переводим
                                //SiteUtility.ReestrDeletePupilZach(pupil.ChildUnions[0].ReestrPupilAgeGroupId);


                                ExtendedPupil pupil1 = null;
                                pupil1 = db1.GetPupil(pupil.UserId);

                                if (pupil1 != null)
                                {
                                    // добавление или изменение
                                    if (string.IsNullOrEmpty(pupil1.ReestrCode) || pupil1.ReestrCode == "0")
                                    {
                                        using (KladrDb db3 = new KladrDb())
                                        {
                                            var addresses = db3.GetAddressesById(pupil1.UserId, null, null, null);
                                            pupil1.Addresses = addresses;
                                        }
                                        Guid? reestrGuid = null;
                                        //string pupilReestrUdodCode = SiteUtility.ReestrAddPupil(pupil,
                                        //                                                        !string.IsNullOrEmpty(
                                        //                                                            pupil1.
                                        //                                                                ReestrUdodCode),
                                        //                                                        ref reestrGuid);
                                        if (reestrGuid.HasValue)
                                        {
                                            // проверка в основном контингенте по Guid
                                            //var pupils =
                                            //    SiteUtility.GetPupilData(
                                            //        Page.MapPath("../search_by_ReestrGuid_rq.xml"),
                                            //        reestrGuid.Value.ToString());
                                            //if (pupils.Count > 0)
                                            //{
                                            //    // удалить из реестра контингента Удод
                                            //    SiteUtility.ReestrDeletePupil(string.IsNullOrEmpty(pupil1.ReestrUdodCode)
                                            //                                      ? pupilReestrUdodCode
                                            //                                      : pupil1.ReestrUdodCode);
                                            //}
                                        }
                                        if (reestrGuid.HasValue)
                                        {
                                            pupil1.ReestrGuid = reestrGuid;
                                        }
                                    }


                                    try
                                    {
                                        //errorClaimList.ShowError("Проверка", result.ToString(), "ок", "cancel", true,false);
                                        if (result != -1)
                                        {
                                            childunion.ReestrCode = result.ToString();
                                            //отправляем зачисление
                                            //SiteUtility.ReestrAddZach(extendedPupil, childunion, extendedChildUnion, false);
                                            var childunionZach =
                                                db1.GetChildUnion(pupil1.UserId).Where(
                                                    i => i.ChildUnionId == childunion.ChildUnionId).FirstOrDefault();
                                            //if (childunionZach != null)
                                            //    SiteUtility.ReestrAddZach(pupil, childunion, childunionZach, false);
                                        }
                                    }
                                    catch (Exception e1)
                                    {

                                    }

                                }
                            }
                        }
                    }
                }
                if (list.Count > 0)
                {
                    MyStringBuilder sb = new MyStringBuilder("<br/>", list.Select(i=>i.ErrorMessage));
                    erropPopup.ShowError("Ошибка", sb.ToString(), "", "ОК", false, true);
                    PupilListDatabind();
                    upChildUnionGroup.Update();
                    return;
                }
            }
        }
        PupilListDatabind();
        upChildUnionGroup.Update();
    }

    protected void ddlGroupsForTransfer_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlGroupsForTransfer.SelectedItem.Text != "Автогруппа")
        {
            lnkTransfer.Enabled = true;
            using (AgeGroupDb db = new AgeGroupDb())
            {
                var group = db.GetGroup(Convert.ToInt64(ddlGroupsForTransfer.SelectedValue));
                StringBuilder sb = new StringBuilder();
                foreach (var teacher in group.Teachers)
                {
                    sb.Append(teacher.Fio);
                    sb.Append(" ");
                }
                string teachers = sb.ToString();
                sb = new StringBuilder();
                foreach (var year in group.NumYears)
                {
                    sb.Append(year);
                    sb.Append(" ");
                }
                pGroupinfo.Visible = true;
                lblGroupInfo.Text = string.Format("Возраст: от {1} до {2}, Педагоги: {3}, Года обучения: {4}",
                                                  group.Name, group.AgeStart, group.AgeEnd, teachers, sb.ToString());
            }
            
        }
        else
        {
            lnkTransfer.Enabled = false;
            
        }
        upChildUnionGroup.Update();
    }

    protected void ddlGroupsForTransfer_OnDataBound(object sender, EventArgs e)
    {
        if (ddlGroupsForTransfer.Items.Count > 0) ddlGroupsForTransfer_OnSelectedIndexChanged(sender, e);
        else pGroupinfo.Visible = false;
    }

    protected void lnkClearSelect_OnClick(object sender, EventArgs e)
    {
        if (UserContext.UdodId.HasValue)
        {
            using (PupilsDb db = new PupilsDb())
            {
                db.ClearSelectGroupPupil(UserContext.UdodId.Value);
            }
        }
        PupilListDatabind();
    }

    protected void ddlGroupsFilter_OnDataBound(object sender, EventArgs e)
    {
        if (ddlGroupsFilter.Items.Count == 0)
        {
            ddlGroupsFilter.Items.Insert(0, new ListItem("Выберите ДО", ""));
        }
        else
        {
            ddlGroupsFilter.Items.Insert(0, new ListItem("Все", ""));
        }
    }

    protected void ddlDOZach_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlDO.SelectedValue))
        {
            using (AgeGroupDb db = new AgeGroupDb())
            {
                ddlGroupsForTransfer.DataSource = db.GetGroupsPaging(cityUdodFilter.SelectedUdod, null,
                                                                     Convert.ToInt64(
                                                                         ddlDOZach.SelectedValue), null,
                                                                     null, null, null, 1, 1000, true).OrderBy(i => i.Name);
                ddlGroupsForTransfer.DataBind();
                ddlGroupsFilter.DataBind();
            }
        }
        else
        {
            ddlGroupsFilter.DataSource = new List<ExtendedGroup>();
            ddlGroupsFilter.DataBind();
            ddlBudgetType.DataSourceID = "dsBudgetType";
            ddlBudgetType.DataBind();
        }
        upChildUnionGroup.Update();
    }

    protected void ddlDOZach_OnDataBound(object sender, EventArgs e)
    {
        /*if (ddlDOZach.Items.Count == 0)
        {
            ddlDOZach.Items.Insert(0, new ListItem("Выберите УДО", ""));
        }
        else
        {
            ddlDOZach.Items.Insert(0, new ListItem("Все", ""));
        }
        upChildUnionGroup.Update();*/
    }
}
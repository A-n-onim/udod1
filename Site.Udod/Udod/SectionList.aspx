﻿<%@ Page Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" CodeFile="SectionList.aspx.cs" Inherits="Udod_SectionList" %>
<%@ Register TagPrefix="uct" TagName="SectionFilter" Src="~/UserControl/Filter/ucSectionFilter.ascx" %>
<%@ Register src="../UserControl/Filter/ucCityUdodFilter.ascx" tagName="CityUdodFilter" tagPrefix="uct" %>
<%@ Register TagPrefix="uct" TagName="SortHeader" Src="~/UserControl/Menu/ucSortHeader.ascx" %>

<asp:Content runat="server" ID="contentSections" ContentPlaceHolderID="body">
    <asp:UpdatePanel ID="upCityUdodFilter" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <uct:CityUdodFilter ID="cityUdodFilter" runat="server" UdodDdlEnabled="true" OnUdodChange="cityUdodFilter_OnUdodChange" OnCityChange="cityUdodFilter_OnUdodChange"/>
    </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel runat="server" ID="up" UpdateMode="Conditional">
        <ContentTemplate>
            <uct:SectionFilter ID="sectionFilter" runat="server" onProfileChange="ProfileIndex_Changed" onProgramChange="ProfileIndex_Changed"/>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="upLnkAdd" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:LinkButton runat="server" ID="lnkAddSection" OnClick="AddSection_Click" Visible="false" ><div class='btnBlue'>Добавить секцию</div></asp:LinkButton>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:UpdatePanel runat="server" ID="upGridView" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:GridView runat="server" ID="gvSectionList" CssClass="dgc" AutoGenerateColumns="false" DataKeyNames="SectionId" 
                      DataSourceID="datasource" onselectedindexchanged="gvSectionList_SelectedIndexChanged"
                      AllowPaging="true" PageIndex="0" PageSize="15" OnDataBound="gvSectionList_OnDataBound">
            <EmptyDataTemplate>
                <asp:Label ID="lblEmptyGvSectionList" runat="server" style="font-size:16px">Нет объединений</asp:Label>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField >
                    <ItemTemplate><%# (gvSectionList.Rows.Count + 1) + gvSectionList.PageSize * gvSectionList.PageIndex%></ItemTemplate>
                </asp:TemplateField>
                        
                <asp:TemplateField HeaderText="Вид деятельности (предмет)">
                    <HeaderTemplate>
                        <uct:SortHeader ID="shName" runat="server" HeaderCaption="Вид деятельности (предмет)" OnSortingChanged="gvSectionListSortingChanged" SortExpression="Name"/>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("Name") %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="isExistInCurrentUdod" ItemStyle-Width="140px" HeaderText="Есть в выбранном УДО" ItemStyle-HorizontalAlign="Center">
	                <HeaderTemplate>
                        <uct:SortHeader ID="shChkIsExist" runat="server" HeaderCaption="Есть в выбранном УДО" OnSortingChanged="gvSectionListSortingChanged" SortExpression="UdodSectionId"/>
                    </HeaderTemplate>
                    <ItemTemplate>
		                <asp:CheckBox runat="server" ID="chkIsExist" Checked='<%# Eval("isExistInCurrentUdod") %>' 
			                AutoPostBack="true" OnCheckedChanged="chkIsExist_CheckedChanged" Enabled='<%# Eval("isEnabledToChange") %>' ToolTip='<%# GetToolTip(Container.DataItem) %>'/>
	                </ItemTemplate>
                </asp:TemplateField>

                <asp:CommandField  ButtonType="Link" ShowSelectButton="true" HeaderText="Изменить" SelectText="<div class='btnBlue'>Изменить</div>"/>
                <asp:CommandField  ButtonType="Link" ShowDeleteButton="true" HeaderText="Удаление" DeleteText="<div class='btnRed'>Удалить</div>" />
            </Columns>
            <PagerStyle CssClass="gridpager" VerticalAlign="Bottom" HorizontalAlign="Left"/>
            <PagerSettings Mode="Numeric" Position="Bottom" PageButtonCount="10" />
        </asp:GridView>
    </ContentTemplate>
    </asp:UpdatePanel>

    <table>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Выводить по " />
            </td>
            <td>
                <div class="inputXShort" style="margin-top: 6px;">
                    <asp:DropDownList runat="server" ID="ddlNumItemsOnPage" CssClass="inputXShort" OnSelectedIndexChanged="ddlNumItemsOnPage_SelectedIndexChanged" AutoPostBack="true"/>
                </div>
            </td>
            <td>
                <asp:Label ID="Label2" runat="server" Text=" элементов на странице." />
            </td>
            <td>
                <asp:UpdatePanel ID="upNumItemsInGv" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Label ID="lblNumItemsInGv" runat="server" />
                </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:Label ID="lblSorting" runat="server" Visible="false" />

    <asp:ObjectDataSource ID= "datasource" TypeName="Udod.Dal.DictSectionDb" DeleteMethod="DeleteSection" runat="server" 
        SelectMethod="GetSections" SelectCountMethod="GetSectionsCount" OnLoad="datasource_OnLoad" OnSelected="ds_Selected" >
        <SelectParameters>
            <asp:ControlParameter Name="ProfileId" ControlID="sectionFilter" DbType="Int32" ConvertEmptyStringToNull="true" PropertyName="SelectedProfileValue" />
            <asp:ControlParameter Name="ProgramId" ControlID="sectionFilter" DbType="Int32" ConvertEmptyStringToNull="true" PropertyName="SelectedProgramValue" />
            <asp:ControlParameter Name="UdodId" ControlID="cityUdodFilter" DbType="Int64" ConvertEmptyStringToNull="true" PropertyName="SelectedUdod"/>
            <asp:ControlParameter Name="OrderBy" ControlID="lblSorting" ConvertEmptyStringToNull="true" PropertyName="Text" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:Panel runat="server" ID="popupAddSection" style="display: none; border: 2px solid black" BackColor="White" CssClass="modalPopup">
        <asp:UpdatePanel ID="upAddSection" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
        <asp:Panel ID="pnlHeader" runat="server" >
            <asp:Label ID="litTitle" Text="<span class='headerCaptions'>Добавление секции</span>" runat="server"/>
            <asp:LinkButton ID="lnkClose" runat="server" 
                OnClientClick="$find('AddSectionPopup').hide(); return false;" />
        </asp:Panel>
        
        <table>
            <tr>
                <td class="tdx">
                    Наименование:
                </td>
                <td>
                    <div class="inputXShort"><asp:TextBox runat="server" ID="tbSectionName" CssClass="inputXShort" /></div>
                </td>
            </tr>
            <tr>
                <td class="tdx">
                    Короткое имя:
                </td>
                <td>
                    <div class="inputXShort">
                        <asp:TextBox runat="server" ID="tbSectionShortName" CssClass="inputXShort" />
                    </div>
                </td>
            </tr>
            <tr>
                <td class="tdx">
                    Ограничение по возрасту:
                </td>
                <td>
                    <div class="inputXShort">
                        <asp:TextBox runat="server" ID="tbSectionLimitAge" CssClass="inputXShort" Text="0"/>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan=2>
                    <asp:RequiredFieldValidator runat="server" ID="val4" ValidationGroup="35" ErrorMessage="Поле 'Ограничение по возрасту' не заполнено" ControlToValidate="tbSectionLimitAge" Display="Dynamic"  ></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="valLimitAge" ErrorMessage="Ограничение по возрасту должно<br> быть целым числом" ValidationGroup="35"
                                ControlToValidate="tbSectionLimitAge" ValidationExpression="\d+" Display="Dynamic"></asp:RegularExpressionValidator>
                </td>
            </tr>
        </table>
        </ContentTemplate>
        </asp:UpdatePanel>
        <table width="100%">
            <tr>
                <td>
                    <asp:LinkButton ID="btnAddSection" runat="server" Text="Сохранить" CausesValidation="true" ValidationGroup="35" OnClick="SaveSection_Click"><div class="btnBlue">Сохранить</div></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="btnCancel" runat="server" Text="Отменить" CausesValidation="false"><div class="btnRed">Отменить</div></asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupAddExtender" PopupDragHandleControlID="pnlHeader"
        PopupControlID="popupAddSection" CancelControlID="btnCancel"
        TargetControlID="btnShowPopup" RepositionMode="None"
        BehaviorID="AddSectionPopup"/>

    <asp:Button ID="btnShowPopup" runat="server" style="display:none" />
</asp:Content>
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransferPupilList.aspx.cs" MasterPageFile="../Master/MasterPage.master" Inherits="Udod_TransferPupilList" %>
<%@ Register src="../UserControl/Menu/ucPager.ascx" tagName="Pager" tagPrefix="uc" %>
<%@ Register TagPrefix="uct" TagName="CityUdodFilter" Src="~/UserControl/Filter/ucCityUdodFilter.ascx" %>
<%@ Register TagPrefix="uct" TagName="Udod" Src="~/UserControl/Udod/ucFindUdod.ascx" %>
<%@ Register TagPrefix="uct" TagName="Fio" Src="~/UserControl/User/ucFIO_vertical.ascx" %>
<%@ Register TagPrefix="uct" TagName="Document" Src="~/UserControl/User/ucDocument_Vertical.ascx" %>
<%@ Register TagPrefix="uct" TagName="Address" Src="~/UserControl/Address/ucNewAddress.ascx" %>
<%@ Register TagPrefix="uct" TagName="ErrorPopup" Src="~/UserControl/Menu/ucErrorPopup.ascx" %>
<asp:Content runat="server" ID="content" ContentPlaceHolderID="body">
<script type="text/javascript">

    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(bindtextbox);
    function bindtextbox(sender, args) {
        $('#cbxSelectAll').unbind("change");
        $('#cbxSelectAll').bind("change", GvTransferPupilListChecked);
        }
    
    function GvTransferPupilListChecked() {
        var gvTransferPupilList = '#' + '<%= gvTransferPupilList.ClientID %>';
        if ($('#cbxSelectAll').attr("checked")) {
            $(gvTransferPupilList).find("input[type=checkbox]").each(function () {
                if ($(this).attr('disabled') == 'disabled') {
                    $(this).removeAttr("checked");
                }
                else {
                    $(this).attr("checked", "true");
                }
            });
        }
        else {
            $(gvTransferPupilList).find("input[type=checkbox]").each(function () {
                $(this).removeAttr("checked");
            });
        }
    }
</script>
<asp:UpdatePanel ID="upCityUdodFilter" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <uct:CityUdodFilter ID="cityUdodFilter" runat="server" UdodDdlEnabled="true" OnCityChange="cityUdodFilter_OnCityChange" OnUdodChange="cityUdodFilter_OnUdodChange" OnloadComplete="cityUdodFilter_OnloadComplete"/>
        
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel ID="upFilters" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel ID="Panel2" runat="server" DefaultButton="lnkFindByName" GroupingText="<b><div style='font-size:14pt'>Поиск обучающегося</h3></div></b>">      
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblPupilLastName" runat="server" Text="Фамилия:" />
                </td>
                <td>
                    <asp:Label ID="lblPupilFirstName" runat="server" Text="Имя:" />
                </td>
                <td>
                    <asp:Label ID="lblPupilMiddleName" runat="server" Text="Отчество:" />
                </td>
                <td>Статус</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbPupilLastName" runat="server" CssClass="input" />
                </td>
                <td>
                    <asp:TextBox ID="tbPupilFirstName" runat="server" CssClass="input" />
                </td>
                <td>
                    <asp:TextBox ID="tbPupilMiddleName" runat="server" CssClass="input" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlTransferStatusFilter" AutoPostBack="true" CssClass="input" DataSourceID="dsTransferStatus" DataTextField="TransferStatusName" DataValueField="TransferStatusId" OnDataBound="ddlTransferStatus_OnDataBound"/>
                </td>
                <td>
                    <asp:LinkButton ID="lnkFindByName" runat="server" OnClick="lnkFindByName_OnClick"><div class="btnBlue">Поиск</div></asp:LinkButton>
                </td>
            </tr>
        </table>
        </asp:Panel>
        <asp:Panel ID="Panel1" runat="server" GroupingText="<b><div style='font-size:14pt'>Поиск по Детским Объединениям</div></b>" >
            <table>
                <tr>
                    <td>
                        Детское объединение:
                    </td>
                    <td>
                        <asp:DropDownList CssClass="input" runat="server" ID="ddlDO" DataSourceID="dsDO" DataTextField="Name" DataValueField="ChildUnionId" OnDataBound="ddlDO_OnDataBound" AutoPostBack="true" OnSelectedIndexChanged="ddlDO_SelectedIndexChanged"/>
                    </td>
                    <td>
                        Группа:
                    </td>
                    <td>
                        <asp:DropDownList CssClass="input" runat="server" ID="ddlAgeGroup" DataTextField="Name" DataValueField="UdodAgeGroupId" AutoPostBack="true" OnDataBound="ddlAgeGroup_OnDataBound" OnSelectedIndexChanged="ddlAgeGroup_OnSelectedIndexChanged"/>
                    </td>
                    <td>Год обучения</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlNumYearFilter" AutoPostBack="true" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
    
    
<table width="100%">
            <tr>
                <td>
                    <asp:LinkButton runat="server" ID="lnkExportExcel" OnClick="lnkExportExcel_OnClick"><div class="btnBlue">Экспорт в Excel</div></asp:LinkButton>
                </td>
                <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                                    <asp:LinkButton runat="server" ID="lnkRecalculate" OnClick="lnkRecalculate_OnClick" ><div class="btnBlue">Пересчитать статусы</div></asp:LinkButton>
                
                    </ContentTemplate>
                </asp:UpdatePanel>
                </td>
                <td align="right" style="padding-right: 50px;">
                <asp:UpdatePanel ID="upLnk" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                                    <asp:LinkButton runat="server" ID="lnkDeSelect" OnClick="lnkDeSelect_OnClick" ><div class="btnBlue">Убрать выделение</div></asp:LinkButton>
                
                    </ContentTemplate>
                </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    
<asp:UpdatePanel runat="server" ID="upTransferPupilList" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Label runat="server" ID="lblInfo"></asp:Label>
        <asp:GridView runat="server" ID="gvTransferPupilList" CssClass="dgc" AutoGenerateColumns="false" 
                        DataSourceID="dsTransferPupilList" OnRowDataBound="gvTransferPupilList_OnRowDataBound">
            <EmptyDataTemplate>
                <asp:Label ID="lblEmptyGvTransferPupilList" runat="server" style="font-size:16px">Зачисления не найдены</asp:Label>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="№" >
                    <ItemTemplate><%# (gvTransferPupilList.Rows.Count + pgrTransferPupilList.PagerIndex)%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ФИО обучающегося" >
                    <ItemTemplate>
                        <%# Eval("PupilFio")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Дата рождения" >
                    <ItemTemplate>
                        <%# ((DateTime)Eval("PupilBirthday")).ToShortDateString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Детское объединение" >
                    <ItemTemplate>
                        <%# Eval("ChildUnion.Name")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Статус ДО" >
                    <ItemTemplate>
                        <%# Eval("StatusDO").ToString()=="1" ? "Будет удалено":"Работает"%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Название группы" >
                    <ItemTemplate>
                        <%# Eval("NameGroup")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Статус группы" >
                    <ItemTemplate>
                        <%# Eval("Statusgroup").ToString() == "1" ? "Будет удалено" : "Работает"%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Вид услуги" >
                    <ItemTemplate>
                        <%# Eval("DictTypeBudgetId").ToString() == "1" ? "Платно" : Eval("DictTypeBudgetId").ToString() == "2"?"Бесплатно":""%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Текущий год обучения" >
                    <ItemTemplate>
                        <%# Eval("CurrentNumYear")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Период обучения" >
                    <ItemTemplate>
                        <%# Eval("TotalNumYear")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Возможные возраста" >
                    <ItemTemplate>
                        <%# Eval("ChildUnionAges")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Возраст обуч-ся на 1 сентября" >
                    <ItemTemplate>
                        <%# Eval("PupilYearsOnSeptFirst")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Статус" >
                    <ItemTemplate>
                        <%# Eval("TransferStatus.TransferStatusName")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Выделение">
                    <ItemTemplate>
                        <asp:CheckBox runat="server" ID="cbxSelect" OnCheckedChanged="cbxSelect_OnCheckedChanged" Checked='<%# Eval("TransferIsSelected") %>'/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger runat="server" ControlID="ddlDO"/>
        <asp:AsyncPostBackTrigger runat="server" ControlID="ddlNumYearFilter"/>
        <asp:AsyncPostBackTrigger runat="server" ControlID="lnkDeSelect"/>
    </Triggers>
</asp:UpdatePanel>

<uc:Pager runat="server" ID="pgrTransferPupilList" needStartDots="false" needEndDots="false" pagesCount="10" OnChange="pgrTransferPupilList_OnChange"/>

<asp:UpdatePanel ID="upChangeStatusButtons" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <table>
            <tr>
                <td>
                    <asp:LinkButton runat="server" ID="lnkToTransfer" OnClick="lnkToTransfer_OnClick" ><div class="btnBlueLong">Пометить к переводу</div></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton runat="server" ID="lnkToDismis" OnClick="lnkToDismis_OnClick" ><div class="btnBlueLong">Пометить к отчислению</div></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton runat="server" ID="lnkToRepeat" OnClick="lnkToRepeat_OnClick" Visible="false" ><div class="btnBlueLong">Пометить к повтору</div></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel runat="server" ID="upChildUnionGroup" UpdateMode="Conditional">
    <ContentTemplate>
    <asp:Panel ID="Panel3" runat="server" GroupingText="<b><div style='font-size:14pt'>В новом учебном году продолжит обучаться в ДО</h3></div></b>">      
        <table>
            <tr>
                <td>
                    Детское объединение:
                </td>
                <td>
                    <asp:DropDownList CssClass="input" runat="server" ID="ddlDOZach" DataTextField="Name" DataValueField="ChildUnionId" AutoPostBack="true" OnSelectedIndexChanged="ddlDOZach_OnSelectedIndexChanged"/>
                </td>
                <td>Выберите группу</td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlGroupsForTransfer" CssClass="input" DataTextField="Name" DataValueField="UdodAgeGroupId" AutoPostBack="true" OnSelectedIndexChanged="ddlGroupsForTransfer_OnSelectedIndexChanged" OnDataBound="ddlGroupsForTransfer_OnDataBound" />
                </td>
                <td>Выберите год обучения</td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlNumYear" CssClass="input" AutoPostBack="true" />
                </td>
                <td>
                    <asp:LinkButton runat="server" ID="lnkTransfer" OnClick="lnkTransfer_OnClick" Visible="false"><div class="btnBlue">Перевести</div></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Panel runat="server" ID="pGroupinfo" GroupingText="информация о группе">
                    <asp:Label runat="server" ID="lblGroupInfo"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>

<uct:ErrorPopup ID="erropPopup" runat="server"/> 

<asp:UpdatePanel ID="upErrorPopup" runat="server" UpdateMode="Always" >
    <ContentTemplate>
        <uct:ErrorPopup ID="errorPopup" runat="server" Width="450px"/>
    </ContentTemplate>
</asp:UpdatePanel>
    
<asp:ObjectDataSource runat="server" ID="dsTransferPupilList" TypeName="Udod.Dal.TransferDb" SelectMethod="GetTransferPupilPaging" OnSelecting="dsTransferPupilList_OnSelecting" >
    <SelectParameters>
        <asp:ControlParameter Name="udodId" ControlID="cityUdodFilter" PropertyName="SelectedUdod" ConvertEmptyStringToNull="true" />
        <asp:ControlParameter Name="childUnionId" ControlID="ddlDO" PropertyName="SelectedValue" ConvertEmptyStringToNull="true" />
        <asp:ControlParameter Name="agegroupId" ControlID="ddlAgeGroup" PropertyName="SelectedValue" ConvertEmptyStringToNull="true" />
        <asp:ControlParameter Name="numYear" ControlID="ddlNumYearFilter" PropertyName="SelectedValue" ConvertEmptyStringToNull="true" />
        <asp:ControlParameter Name="pupilLastName" ControlID="tbPupilLastName" PropertyName="Text" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pupilFirstName" ControlID="tbPupilFirstName" PropertyName="Text" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pupilMiddleName" ControlID="tbPupilMiddleName" PropertyName="Text" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="status" ControlID="ddlTransferStatusFilter" PropertyName="SelectedValue" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pagerIndex" ControlID="pgrTransferPupilList" ConvertEmptyStringToNull="true" PropertyName="PagerIndex" />
        <asp:ControlParameter Name="pagerLength" ControlID="pgrTransferPupilList" ConvertEmptyStringToNull="true" PropertyName="PagerLength" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource runat="server" ID="dsDO" TypeName="Udod.Dal.ChildUnionDb" SelectMethod="GetChildUnionsShort" >
    <SelectParameters>
        <asp:ControlParameter Name="cityId" ControlID="cityUdodFilter" PropertyName="SelectedCity" ConvertEmptyStringToNull="true" />
        <asp:ControlParameter Name="udodId" ControlID="cityUdodFilter" PropertyName="SelectedUdod" ConvertEmptyStringToNull="true" />
        <asp:Parameter Name="programId"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="profileId"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="sectionId"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="typeBudgetId"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="sectionName"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="profileName"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="programName"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="childUnionName"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="cityCode"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="rayonId"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="metroId"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="udodName"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="rayonCode"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="teacherId"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="isReception" DefaultValue="false" ConvertEmptyStringToNull="false"/>
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource runat="server" ID="dsTransferStatus" TypeName="Udod.Dal.TransferDb" SelectMethod="GetTransferStatuses" >
</asp:ObjectDataSource>
</asp:Content>
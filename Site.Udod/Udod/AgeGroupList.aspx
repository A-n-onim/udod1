﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="AgeGroupList.aspx.cs" Inherits="Udod_AgeGroupList" %>
<%@ Register src="../UserControl/Filter/ucCityUdodFilter.ascx" tagName="CityUdodFilter" tagPrefix="uct" %>
<%@ Register TagPrefix="uc" TagName="Pager" Src="~/UserControl/Menu/ucPager.ascx" %>
<%@ Register TagPrefix="uc" TagName="ErrorPopup" Src="~/UserControl/Menu/ucErrorPopup.ascx" %>

<asp:Content runat="server" ID="contentAgeGroup" ContentPlaceHolderID="body">
    <script type="text/javascript">
        var deletedtext = 'Из базы будет удалена выбранная группа и все пункты расписания, ссылающиеся на неё. Отменить данную операцию будет невозможно. Продолжить?';
    
        var cbxTeacher = '<%= cblTeachers.ClientID %>';
        function isValidTeacher(source, args) {
            args.IsValid = $('#' + cbxTeacher).find("input:checked").length > 0 && $('#' + cbxTeacher).find("input").length != 0;
        }
        var cbxNumYear = '<%= cblNumYear.ClientID %>';
        function isValidNumYear(source, args) {
            args.IsValid = $('#' + cbxNumYear).find("input:checked").length > 0 && $('#' + cbxNumYear).find("input").length != 0;
        }
    </script>


    <asp:UpdatePanel ID="upCityUdodFilter" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uct:CityUdodFilter ID="cityUdodFilter" runat="server" UdodDdlEnabled="true" AddAllProperty="true" OnUdodChange="FilterDatabind" OnCityChange="FilterDatabind"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="upfilter" UpdateMode="Conditional">
        <ContentTemplate>
        
            <table>
                <tr>
                    <td>
                        Детское объединение:
                    </td>
                    <td>
                        <div class="inputShort">
                        <asp:DropDownList runat="server" ID="ddlChildUnion" CssClass="inputShort" DataValueField="ChildUnionId" DataTextField="Name" AutoPostBack="true" OnSelectedIndexChanged="ddlChildUnion_OnSelectedIndexChanged" OnDataBound="ddlChildUnion_OnDataBound"/>
                        </div>
                    </td>
                    <td>Год обучения</td>
                    <td>
                        <asp:DropDownList CssClass="inputShort" runat="server" ID="ddlNumYear" AutoPostBack="true" OnSelectedIndexChanged="ddlNumYear_OnSelectedIndexChanged">
                            <Items>
                                <asp:ListItem Text="Все" Value="" Selected="True" />
                                <asp:ListItem Text="1" Value="1" />
                                <asp:ListItem Text="2" Value="2" />
                                <asp:ListItem Text="3" Value="3" />
                                <asp:ListItem Text="4" Value="4" />
                                <asp:ListItem Text="5" Value="5" />
                                <asp:ListItem Text="6" Value="6" />
                                <asp:ListItem Text="7" Value="7" />
                                <asp:ListItem Text="8" Value="8" />
                                <asp:ListItem Text="9" Value="9" />
                                <asp:ListItem Text="10" Value="10" />
                                <asp:ListItem Text="11" Value="11" />
                                <asp:ListItem Text="12" Value="12" />
                                <asp:ListItem Text="13" Value="13" />
                                <asp:ListItem Text="14" Value="14" />
                                <asp:ListItem Text="15" Value="15" />
                            </Items>
                        </asp:DropDownList>
                    </td>
                    <td>Вид услуги</td>
                    <td>
                        <asp:DropDownList CssClass="inputShort" runat="server" ID="ddlBudgetType" DataSourceID="dsBudgetType" DataTextField="TypeBudgetName" DataValueField="DictTypeBudgetId" AutoPostBack="true" OnSelectedIndexChanged="ddlBudgetType_OnSelectedIndexChanged" OnDataBound="ddlBudgetType_OnDataBound" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Наименование группы
                    </td>
                    <td colspan="5">
                        <asp:TextBox runat="server" ID="tbGroupName" CssClass="inputLong"></asp:TextBox>
                    </td>
                    <td>
                        <asp:LinkButton runat="server" ID="lnkRefresh" OnClick="lnkRefresh_OnClick" CausesValidation="false"><div class="btnBlue">Поиск</div></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:LinkButton runat="server" ID="lnkAddGroup" CausesValidation="false" OnClick="lnkAddGroup_OnClick"><div class="btnBlue">Добавить группу</div></asp:LinkButton>
                    </td>
                </tr>
            </table>
    </ContentTemplate>
    </asp:UpdatePanel>
     <asp:UpdatePanel runat="server" ID="upAgeGroup" UpdateMode="Conditional">
        <ContentTemplate>
        
            <asp:GridView runat="server" ID="gvAgeGroupList" CssClass="dgc" DataKeyNames="UdodAgeGroupId" AutoGenerateColumns="false" OnSelectedIndexChanged="gvAgeGroupList_OnSelectedIndexChanged" OnRowDeleting="gvAgeGroupList_OnRowDeleting" OnRowDataBound="gvAgeGroupList_OnRowDataBound" >
            <EmptyDataTemplate>
                <asp:Label ID="lblEmptyGvAgeGroupList" runat="server" style="font-size:16px">Групп нет</asp:Label>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="№"  >
                    <ItemTemplate><%# (gvAgeGroupList.Rows.Count) + pgrGroupList.PagerIndex%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Название родительского ДО" >
                    <ItemTemplate>
                        <%# Eval("ChildUnionName")%>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Условное название группы" >
                    <ItemTemplate>
                        <%# Eval("Name")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Возраста в группе" >
                    <ItemTemplate>
                        <%# "От "+Eval("AgeStart").ToString()+" до " + Eval("AgeEnd").ToString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Педагоги" >
                    <ItemTemplate>
                        <%# GetTeachers(Container.DataItem)%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Вид услуги" >
                    <ItemTemplate>
                        <%# Eval("TypeBudget.TypeBudgetName")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Год обучения" >
                    <ItemTemplate>
                        <%# GetYears(Container.DataItem)%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Форма занятия" >
                    <ItemTemplate>
                        <%# Eval("EducationType.EducationTypeName")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Текущее кол-во уч. в группе" >
                    <ItemTemplate>
                        <%# Eval("CurrentCountPupil")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Предельное кол-во уч. в группе" >
                    <ItemTemplate>
                        <%# Eval("MaxCountPupil")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="true" SelectText="<img src='../Images/edit_smth.png' border=0 title='Изменить'/>"/>
                <asp:TemplateField HeaderText="Предельное кол-во уч. в группе" >
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="lnkDelete" OnClick="lnkDelete_OnClick"><img src='../Images/delete_smth.png' border=0 title='Удалить'/></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="gridpager" VerticalAlign="Bottom" HorizontalAlign="Left"/>
            <PagerSettings Mode="Numeric" Position="Bottom" PageButtonCount="10" />
            </asp:GridView>

            <uc:Pager runat="server" ID="pgrGroupList" needStartDots="false" needEndDots="false" pagesCount="10" OnChange="pgrGroupList_OnChange"/>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger runat="server" ControlID="btnAddAgeGroup"/>
        </Triggers>
    </asp:UpdatePanel>
    
    <asp:ObjectDataSource runat="server" ID="dsBudgetType" TypeName="Udod.Dal.DictBudgetTypeDb" SelectMethod="GetBudgetTypes" />
    <asp:ObjectDataSource runat="server" ID="dsEducationType" TypeName="Udod.Dal.DictEducationTypeDb" SelectMethod="GetEducationTypes" />
    <asp:UpdatePanel ID="upErrorPopup" runat="server" UpdateMode="Always" >
    <ContentTemplate>
        <uc:ErrorPopup ID="errorPopup" runat="server"  OnErrorOkClick="lnkDeleteOk_OnClick"/>
    </ContentTemplate>
    </asp:UpdatePanel>
            
    <asp:Panel runat="server" ID="popupAddAgeGroup" style="display: none; border: 2px solid black" BackColor="White">
            
        <asp:Panel ID="pnlHeader" runat="server" >
            <asp:UpdatePanel runat="server" ID="upHeader" UpdateMode="Conditional">
            <ContentTemplate>
            <asp:Label ID="litTitle" Text="<span class='headerCaptions'>Изменение возрастной группы</span>" runat="server"/>
            <asp:LinkButton ID="lnkClose" runat="server" 
                OnClientClick="$find('AddAgeGroupPopup').hide(); return false;" />
            </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upPopup" UpdateMode="Conditional">
            <ContentTemplate>
        <div >
            
            <table>
                <tr>
                    <td>Условное название</td>
                    <td colspan="6"><asp:TextBox runat="server" ID="tbName" CssClass="inputLong"></asp:TextBox></td>
                </tr>
                <tr >
                    <td class="tdx">
                        Детское объединение:
                    </td>
                    <td colspan="6">
                        <asp:DropDownList runat="server" ID="ddlChildUnionsForAdd" CssClass="inputLong" DataValueField="ChildUnionId" DataTextField="Name" AutoPostBack="true" OnSelectedIndexChanged="ddlChildUnionsForAdd_OnSelectedIndexChanged"/>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Педагоги:
                    </td>
                    <td colspan="6">
                        <asp:CheckBoxList runat="server" ID="cblTeachers" DataTextField="TeacherName" DataValueField="UserId"/>
                        <asp:CustomValidator runat="server" ID="CustomValidator1" ValidationGroup="77" ClientValidationFunction="isValidTeacher" EnableClientScript="true" Display="Dynamic" ErrorMessage="Необходимо отметить хотя бы одно значение" ></asp:CustomValidator>
                    </td>
                </tr>
                    
                <tr>
                    <td class="tdx">
                        Возраст от:
                    </td>
                    <td>
                        <div class="inputShort"><asp:TextBox runat="server" ID="tbAgeStart" CssClass="inputShort"></asp:TextBox></div>
                        <asp:RegularExpressionValidator ControlToValidate="tbAgeStart" ValidationExpression="\d+" ErrorMessage="*"  runat="server" ID="RegularExpressionValidator5" ValidationGroup="77" SetFocusOnError="true" ></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="tbAgeStart" ErrorMessage="*" ValidationGroup="77"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" ID="cvAgeStart" ValueToCompare="0" Operator="GreaterThanEqual" ControlToValidate="tbAgeStart" Type="Integer" ValidationGroup="77" Display="Dynamic" ErrorMessage="Возраст должен быть больше или равен возрасту в ДО"></asp:CompareValidator>
                        <asp:CompareValidator runat="server" ID="cvAgeStart2" ValueToCompare="3" Operator="GreaterThanEqual" ControlToValidate="tbAgeStart" Type="Integer" ValidationGroup="77" Display="Dynamic" ErrorMessage="Возраст должен быть больше или равен 3"></asp:CompareValidator>
                        <asp:CompareValidator runat="server" ID="cvAgeStart1" ValueToCompare="0" Operator="LessThanEqual" ControlToValidate="tbAgeStart" Type="Integer" ValidationGroup="77" Display="Dynamic" ErrorMessage="В данной группе существуют зачисления, в связи с этим возрастной диапазон не может быть уменьшен"></asp:CompareValidator>
                        
                    </td>
                    <td><asp:Label runat="server" ID="lblAgeStart"></asp:Label><asp:HiddenField runat="server" ID="hfAgeStart"/> </td>
                    <td class="tdx">
                        Вид услуги:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlTypeBudgetForAdd" CssClass="inputShort" DataSourceID="dsBudgetType" DataTextField="TypeBudgetName" DataValueField="DictTypeBudgetId"/>
                    </td>
                    <td class="tdx" rowspan="3">
                        Год обучения:
                    </td>
                    <td rowspan="3" style="padding-right: 15px;">
                        <asp:CheckBoxList runat="server" ID="cblNumYear" RepeatColumns="2">
                        </asp:CheckBoxList>
                        <asp:CustomValidator runat="server" ID="valCheckBoxList" ValidationGroup="77" ClientValidationFunction="isValidNumYear" EnableClientScript="true" Display="Dynamic" ErrorMessage="Необходимо отметить хотя бы одно значение" ></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Возраст до:
                    </td>
                    <td>
                        <div class="inputShort"><asp:TextBox runat="server" ID="tbAgeEnd" CssClass="inputShort"></asp:TextBox></div>
                        <asp:RegularExpressionValidator ControlToValidate="tbAgeEnd" ValidationExpression="\d+" ErrorMessage="*"  runat="server" ID="RegularExpressionValidator6" ValidationGroup="77" SetFocusOnError="true" ></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="tbAgeEnd" ErrorMessage="*" ValidationGroup="77"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" ID="cvAgeEnd" ValueToCompare="0" Operator="LessThanEqual" ControlToValidate="tbAgeEnd" Type="Integer" ValidationGroup="77" Display="Dynamic" ErrorMessage="Возраст должен быть меньше или равен возрасту в ДО"></asp:CompareValidator>
                        <asp:CompareValidator runat="server" ID="cvAgeEnd2" ValueToCompare="21" Operator="LessThanEqual" ControlToValidate="tbAgeEnd" Type="Integer" ValidationGroup="77" Display="Dynamic" ErrorMessage="Возраст должен быть меньше или равен 21"></asp:CompareValidator>
                        <asp:CompareValidator runat="server" ID="cvAgeEnd1" ValueToCompare="0" Operator="GreaterThanEqual" ControlToValidate="tbAgeEnd" Type="Integer" ValidationGroup="77" Display="Dynamic" ErrorMessage="В данной группе существуют зачисления, в связи с этим возрастной диапазон не может быть уменьшен"></asp:CompareValidator>
                    </td>
                    <td><asp:Label runat="server" ID="lblAgeEnd" Enabled="false"></asp:Label><asp:HiddenField runat="server" ID="hfAgeEnd"/></td>
                    <td class="tdx">
                        Форма занятий:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlEducationType" CssClass="inputShort" DataSourceID="dsEducationType" DataTextField="EducationTypeName" DataValueField="EducationTypeId"/>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Макс. кол-во детей в группе:
                    </td>
                    <td>
                        <div class="inputShort"><asp:TextBox runat="server" ID="tbMaxCountPupil" CssClass="inputShort"></asp:TextBox></div>
                        <asp:RegularExpressionValidator ControlToValidate="tbMaxCountPupil" ValidationExpression="\d+" ErrorMessage="*"  runat="server" ID="RegularExpressionValidator1" ValidationGroup="77" SetFocusOnError="true" ></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbMaxCountPupil" ErrorMessage="*" ValidationGroup="77"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" ID="cvMaxCountPupil" ValueToCompare="0" Operator="GreaterThanEqual" ControlToValidate="tbMaxCountPupil" Type="Integer" ValidationGroup="77" Display="Dynamic" ErrorMessage="Максимальное кол-во детей должно быть больше либо равно минимальному кол-ву указанному в паспорте ДО"></asp:CompareValidator>
                        <asp:CompareValidator runat="server" ID="cvMaxCountPupil1" ValueToCompare="0" Operator="GreaterThanEqual" ControlToValidate="tbMaxCountPupil" Type="Integer" ValidationGroup="77" Display="Dynamic" ErrorMessage="Максимальное кол-во детей должно быть больше либо равно кол-ву зачисленных в группу"></asp:CompareValidator>
                    <td><asp:HiddenField runat="server" ID="hfMinCountPupilDO"/></td>
                    </td>
                    <td class="tdx">
                        Дата начала обучения:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="tbStartLesson" CssClass="inputShort"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender runat="server" ID="calendar" TargetControlID="tbStartLesson" Animated="false" Format="dd.MM.yyyy"/>
                        <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbStartLesson" Mask="99/99/9999" MaskType="Date" />
                        <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="reqvalrDate" ErrorMessage="Поле 'Дата начала обучения' не заполнено" ControlToValidate="tbStartLesson" SetFocusOnError="true" ValidationGroup="77"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" ID="cvStartLesson1" ValueToCompare="30.09.2013" Operator="GreaterThanEqual" ControlToValidate="tbStartLesson" Type="Date" ValidationGroup="77" Display="Dynamic" ErrorMessage="*"></asp:CompareValidator>
                        <asp:CompareValidator runat="server" ID="cvStartLesson2" ValueToCompare="30.09.2013" Operator="LessThanEqual" ControlToValidate="tbStartLesson" Type="Date" ValidationGroup="77" Display="Dynamic" ErrorMessage="*"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Примечание:
                    </td>
                    <td colspan="6">
                        <asp:TextBox runat="server" ID="tbComment" CssClass="inputLong"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Адрес проведения занятий:
                    </td>
                    <td colspan="6">
                        <asp:DropDownList runat="server" ID="ddlAddress" CssClass="inputLong" DataTextField="AddressStr" DataValueField="AddressId"/>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Место проведения занятий:
                    </td>
                    <td colspan="6">
                        <asp:TextBox runat="server" ID="tbPlace" CssClass="inputLong"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Дата начала приема заявок:
                    </td>
                    <td colspan="2">
                        <asp:TextBox runat="server" ID="tbStartClaim" CssClass="input"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="tbStartClaim" Animated="false" Format="dd.MM.yyyy"/>
                        <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="tbStartClaim" Mask="99/99/9999" MaskType="Date" />
                        <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="RequiredFieldValidator2" ErrorMessage="Поле 'Дата начала приема заявок' не заполнено" ControlToValidate="tbStartClaim" SetFocusOnError="true" ValidationGroup="77"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" ID="cvStartClaim1" ValueToCompare="30.09.2013" Operator="GreaterThanEqual" ControlToValidate="tbStartClaim" Type="Date" ValidationGroup="77" Display="Dynamic" ErrorMessage="*"></asp:CompareValidator>
                        <asp:CompareValidator runat="server" ID="cvStartClaim2" ValueToCompare="30.09.2013" Operator="LessThanEqual" ControlToValidate="tbStartClaim" Type="Date" ValidationGroup="77" Display="Dynamic" ErrorMessage="*"></asp:CompareValidator>
                    </td>
                    <td class="tdx">
                        Дата окончания приема заявок:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="tbEndClaim" CssClass="input"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="tbEndClaim" Animated="false" Format="dd.MM.yyyy"/>
                        <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="tbEndClaim" Mask="99/99/9999" MaskType="Date" />
                        <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="RequiredFieldValidator3" ErrorMessage="Поле 'Дата окончания приема заявок' не заполнено" ControlToValidate="tbEndClaim" SetFocusOnError="true" ValidationGroup="77"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" ID="cvEndClaim1" ValueToCompare="30.09.2013" Operator="GreaterThanEqual" ControlToValidate="tbEndClaim" Type="Date" ValidationGroup="77" Display="Dynamic" ErrorMessage="*"></asp:CompareValidator>
                        <asp:CompareValidator runat="server" ID="cvEndClaim2" ValueToCompare="30.09.2013" Operator="LessThanEqual" ControlToValidate="tbEndClaim" Type="Date" ValidationGroup="77" Display="Dynamic" ErrorMessage="*"></asp:CompareValidator>
                    </td>
                    <td></td>
                    <td></td>
                    
                </tr>
            </table>
                
        </div>
        </ContentTemplate>
            </asp:UpdatePanel>
        <div> 
            <table>
                <tr>
                    <td>
                        <asp:UpdatePanel runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:LinkButton ID="btnAddAgeGroup" runat="server" Text="<div class='btnBlue'>Сохранить</div>" CausesValidation="true" ValidationGroup="77" OnClick="btnAddAgeGroup_OnClick"/>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnCancel" runat="server" OnClick="btnCancel_OnClick" Text="<div class='btnBlue'>Отменить</div>" CausesValidation="false" />
                    </td>
                </tr>
            </table>
            
            
        </div>
        
    </asp:Panel>
    
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupAddExtender" PopupDragHandleControlID="pnlHeader"
        PopupControlID="popupAddAgeGroup" CancelControlID="btnCancel"
        TargetControlID="btnShowPopup" RepositionMode="None"
        BehaviorID="AddAgeGroupPopup"/>

    <asp:Button ID="btnShowPopup" runat="server" style="display:none" />
   
</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Udod_UdodEdit : BasePage
{
    public int CurrentError
    {
        get
        {
            return (int)ViewState["CurrentError"];
        }
        set
        {
            ViewState["CurrentError"] = value;
        }
    }

    public long curDOId
    {
        get
        {
            return (long)ViewState["curDOId"];
        }
        set
        {
            ViewState["curDOId"] = value;
        }
    }

    public long? FakeUdodId
    {
        get
        {
            if (ViewState["FakeUdodId"] == null) return null;
            return (long)ViewState["FakeUdodId"];
        }
        set
        {
            ViewState["FakeUdodId"] = value;
        }
    }

    private bool isAdd
    {
        get
        {
            return (bool)ViewState["isAdd"];
        }
        set
        {
            ViewState["isAdd"] = value;
        }
    }

    private bool isAddAddress
    {
        get
        {
            return (bool)ViewState["isAddAddress"];
        }
        set
        {
            ViewState["isAddAddress"] = value;
        }
    }

    private bool isAddDO
    {
        get
        {
            return (bool)ViewState["isAddDO"];
        }
        set
        {
            ViewState["isAddDO"] = value;
        }
    }

    public bool IsEdit
    {
        get
        {
            return (bool)ViewState["isEdit"];
        }
        set
        {
            ViewState["isEdit"] = value;
        }
    }

    public bool IsDOEnabled
    {
        get
        {
            return (bool)ViewState["IsDOEnabled"];
        }
        set
        {
            ViewState["IsDOEnabled"] = value;
        }
    }

    protected string OrderBy
    {
        get
        {
            return (string)ViewState["OrderBy"];
        }
        set
        {
            ViewState["OrderBy"] = value;
        }
    }

    protected void UdodChange(object sender, EventArgs e)
    {
        isAdd = false;
        litTitle.Text = "Редактирование учреждения";
        //long id = (long)gvUdodList.SelectedDataKey.Value;
        if (UserContext.UdodId.HasValue)
        {
            using (UdodDb db = new UdodDb())
            {

                ExtendedUdod udod = db.GetUdod(UserContext.UdodId.Value);

                udodPassport.SetUdodData(udod);

                gvAddresses.DataSource = udod.Addresses;
                AddAddress.Visible = udod.Addresses.Count < 1;
                gvAddresses.DataBind();
                //var list = db.GetMetro(UserContext.UdodId.Value);
                //repMetro.DataSource = list;
                //repMetro.DataBind();

                IsDOEnabled = udod.IsDOEnabled;
                if (IsEdit)
                    addDO.Visible = gvChildUnion.Columns[8].Visible = IsDOEnabled;
            }

            ChildUnionDatabind();
            
            upPassport.Update();
            //upMetro.Update();
            upAddresses.Update();
            upDO.Update();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        CheckPermissions(UserContext.Roles.IsAdministrator||UserContext.Roles.IsDepartment||UserContext.Roles.IsUdodEmployee||UserContext.Roles.IsOsip);
        //PageEvents.UdodChanged += UdodChange;
        //PageEvents.CityChanged += UdodChange;
        //litTitle.Text = "Редактирование учреждения";
        tEkis.Visible = UserContext.Roles.IsAdministrator;
        if (UserContext.Roles.RoleId == (int)EnumRoles.Osip || UserContext.Roles.RoleId == (int)EnumRoles.Department)
        {
            IsEdit = false;
        }
        if (!Page.IsPostBack)
        {
            if (UserContext.Roles.RoleId == (int)EnumRoles.Osip || UserContext.Roles.RoleId == (int)EnumRoles.Department)
            {
                gvChildUnion.Columns[7].Visible = false;
                gvChildUnion.Columns[8].Visible = false;
                addDO.Visible = false;
                IsEdit = false;
            }
            else
            {
                gvChildUnion.Columns[7].Visible = true;
                gvChildUnion.Columns[8].Visible = true;
                addDO.Visible = true;
                IsEdit = true;
            }



            if (!UserContext.UdodId.HasValue || UserContext.UdodId.Value == -1)
            {
                if (UserContext.Roles.RoleId == (int)EnumRoles.Osip)
                {
                    Response.Redirect("~/Udod/UdodList.aspx", true);
                }

                isAdd = true;
                litTitle.Text = IsEdit ? "Добавление учреждения" : "Просмотр учреждения";
                if (UserContext.CityId.HasValue)
                    udodPassport.ClearData(UserContext.CityId.Value);
                else
                    udodPassport.ClearData(1);


                table1.Visible = table2.Visible = false;
                //gvAddresses.DataBind();

                // создаем фейковый УДО
                /*using (UdodDb db = new UdodDb())
                {
                    ExtendedUdod eu = udodPassport.GetUdodData();
                    UserContext.UdodId = db.AddUdod("Новый УДО", "Новый УДО", eu.JurAddress, Convert.ToInt32(eu.CityId), eu.FioDirector, eu.PhoneNumber, eu.FaxNumber, eu.EMail, eu.SiteUrl, 0);

                    EventsUtility.LogUdodCreate(UserContext.Profile.UserId, Request.UserHostAddress,
                        String.Format("(Идентификатор: {0})(Название: 'Новый УДО' _fake)(Параметры: (JurAddress: {1}),(CityId: {2}),(FioDirector: {3}),(PhoneNumber: {4}),(FaxNumber: {5}),(EMail: {6}),(SiteUrl: {7}))",
                            UserContext.UdodId, eu.JurAddress, eu.CityId, eu.FioDirector, eu.PhoneNumber, eu.FaxNumber, eu.EMail, eu.SiteUrl
                        ));

                    FakeUdodId = UserContext.UdodId.Value;
                    addDO.Visible = false;
                }*/
            }
            else
            {
                if (UserContext.UdodId.HasValue)
                {
                    cityUdodFilter.SelectedUdod = UserContext.UdodId.Value;
                }

                if (UserContext.CityId.HasValue)
                {
                    cityUdodFilter.SelectedCity = UserContext.CityId.Value;
                }

                
                isAdd = false;
                litTitle.Text = IsEdit ? "Редактирование учреждения" : "Просмотр учреждения";
                //long id = (long)gvUdodList.SelectedDataKey.Value;
                using (UdodDb db = new UdodDb())
                {
                    ExtendedUdod udod = db.GetUdod(UserContext.UdodId.Value);
                    // Загрузка доп информации
                    udod.DopInfo = db.GetUdodDopInfo(udod.UdodId);
                    udod.License = db.GetUdodLicense(udod.UdodId, 1);
                    udod.Accreditation = db.GetUdodLicense(udod.UdodId, 2);
                    udodPassport.IsIndividual = udod.DopInfo.Is_Individual;
                    ddlIsIndividual.SelectedIndex = Convert.ToInt32(udod.DopInfo.Is_Individual);
                    //if (udod.HasValue)
                    //{
                    tbEkis.Text = udod.EkisId.ToString();
                    udodPassport.IsEdit = IsEdit;
                    btnAddUdod.Visible = IsEdit;
                    //lnkChangeMetro.Visible = IsEdit;

                    udodPassport.SetUdodData(udod);
                    udodPassport.LoadLicense(1);
                    udodPassport.LoadLicense(2);
                    //}

                    // загрузить информацию с сервиса "Реестры ДОгМ"
                    try
                    {
                        int[] reestrId = db.GetReestrDogmId(UserContext.UdodId.Value);
                        string _url = Page.MapPath("../Templates/getUdodInfo.xml");
                        int? ReestrId = null;
                        if (reestrId[1] != 0) ReestrId = reestrId[1];
                        //var reestrData = SiteUtility.GetUdodData(_url, reestrId[0], ReestrId);
                        /*lblInfoService.Text = reestrData.Name + " " + reestrData.ShortName+ " "+ reestrData.JurAddress+ " "+reestrData.FioDirector
                            +" "+reestrData.PhoneNumber+" "+ reestrData.EMail+ " "+reestrData.SiteUrl;
                        */
                        //if (reestrData.ReestrId.HasValue)
                        //{
                        //    db.SetReestrId(UserContext.UdodId.Value, reestrData.ReestrId.Value);
                        //}
                        //udodPassport.SetReestrUdodData(reestrData);

                    }
                    catch (Exception)
                    {
                    }
                    
                    gvAddresses.DataSource = udod.Addresses;
                    AddAddress.Visible = udod.Addresses.Count < 1;
                    gvAddresses.DataBind();

                    gvAddresses.Columns[gvAddresses.Columns.Count - 1].Visible =
                        gvAddresses.Columns[gvAddresses.Columns.Count - 2].Visible =
                        IsEdit;
                    AddAddress.Visible = IsEdit && udod.Addresses.Count < 1;

                    //var list = db.GetMetro(UserContext.UdodId.Value);
                    //repMetro.DataSource = list;
                    //repMetro.DataBind();

                    IsDOEnabled = udod.IsDOEnabled;
                    if (IsEdit)
                        btnSaveDOPassport.Visible = addDO.Visible = gvChildUnion.Columns[8].Visible = IsDOEnabled;
                }

                ChildUnionDatabind();
            }
            acAddressUdodName.ServicePath = SiteUtility.GetUrl("~/Webservices/Dict.asmx");
        }
    }

    protected void gvAddress_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            (e.Row.FindControl("cbxSelect") as CheckBox).Enabled = IsEdit;

        }
    }

    protected string GetErrorString(object address)
    {
        var address1 = ((ExtendedAddress) address);
        int? btiHouseCode = address1.BtiHouseCode;
        if (btiHouseCode.HasValue || !address1.BtiCode.HasValue) return ""; else return "Адрес не соответствует справочнику";
        
    }

    protected void gvProgramList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var cbx = (e.Row.FindControl("chkIsExist") as CheckBox);
            if (cbx.Enabled)    //  а те которые нельзя снять закрыты для всех
                cbx.Enabled = IsEdit;
        }
    }

    protected string GetChildUnionTeachers(object childUnion)
    {
        //var ageGroups = (childUnion as ExtendedChildUnion).AgeGroups;

        //var tmp = ageGroups.Select(i => i.TeacherFio).Distinct().ToList();

        using (ChildUnionDb db = new ChildUnionDb())
        {


            var list = (childUnion as ExtendedChildUnion).Teachers; //db.GetTeacher((childUnion as ExtendedChildUnion).ChildUnionId);


            StringBuilder sb = new StringBuilder();

            foreach (var teacher in list)
            {
                sb.Append(teacher.Fio + "<br>");
            }
            if (sb.ToString() != "")
                return sb.ToString();
            else
                return "Не задано";
        }
    }

    protected string GetChildUnionAge(object childUnion)
    {
        return "от " + (childUnion as ExtendedChildUnion).AgeStart.ToString() + " до " + (childUnion as ExtendedChildUnion).AgeEnd.ToString();
    }

    /*protected string GetAddresses(object section)
    {
        var ageGroups = (section as ExtendedUdodSection).AgeGroups;
        ageGroups.
        var tmp = ageGroups.Select(i => i.Adresses).ToList();

        StringBuilder sb = new StringBuilder();

        foreach (var addr in tmp)
        {
            sb.Append(addr. + "<br>");
        }
        return sb.ToString();
    }*/

    protected void SaveUdod_Click(object sender, EventArgs e)
    {

        if (isAdd)
        {
            using (UdodDb db = new UdodDb())
            {
                ExtendedUdod eu = udodPassport.GetUdodData();
                UserContext.UdodId = db.AddUdod(eu.Name, eu.ShortName, eu.JurAddress, Convert.ToInt32(eu.CityId), eu.FioDirector, eu.PhoneNumber, eu.FaxNumber, eu.EMail, eu.SiteUrl, 0, eu.UdodNumber);

                db.UpdateUdodDopInfo(eu.UdodId, eu.DopInfo);
                udodPassport.SaveLicense(1);
                udodPassport.SaveLicense(2);
                EventsUtility.LogUdodCreate(UserContext.Profile.UserId, Request.UserHostAddress,
                    String.Format("(Идентификатор: {0})(Название: {1})(Параметры: (JurAddress: {2}),(CityId: {3}),(FioDirector: {4}),(PhoneNumber: {5}),(FaxNumber: {6}),(EMail: {7}),(SiteUrl: {8}))",
                        UserContext.UdodId, eu.Name, eu.JurAddress, eu.CityId, eu.FioDirector, eu.PhoneNumber, eu.FaxNumber, eu.EMail, eu.SiteUrl
                    ));
            }

            Response.Redirect("~/Udod/SectionList.aspx");
        }
        else
        {
            using (UdodDb db = new UdodDb())
            {
                ExtendedUdod eu = udodPassport.GetUdodData();
                
                var prev = db.GetUdod(UserContext.UdodId.Value);

                if (!eu.IsDOEnabled && (new ChildUnionDb()).GetChildUnionsCount(null, UserContext.UdodId.Value, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, 15, null, null, false ,false, null) != 0)
                {
                    openErrorPopup(4);
                    return;
                }
                //long id = Convert.ToInt64(gvUdodList.SelectedDataKey.Value);
                db.UpdateUdod(eu.Name, eu.ShortName, eu.JurAddress, Convert.ToInt32(eu.CityId), eu.FioDirector, eu.PhoneNumber, eu.FaxNumber, eu.EMail, eu.SiteUrl, 0, eu.IsDOEnabled, eu.UdodNumber, UserContext.UdodId.Value, eu.IsInvalid);
                db.UpdateUdodDopInfo(UserContext.UdodId.Value, eu.DopInfo);
                udodPassport.SaveLicense(1);
                udodPassport.SaveLicense(2);
                
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("(Идентификатор УДО: {0})", UserContext.UdodId.Value);

                if (prev.Name != eu.Name)
                    sb.AppendFormat("(Название: {0} -> {1})", prev.Name, eu.Name);
                if (prev.ShortName != eu.ShortName)
                    sb.AppendFormat("(Короткое название: {0} -> {1})", prev.ShortName, eu.ShortName);
                if (prev.UdodNumber != eu.UdodNumber)
                    sb.AppendFormat("(Номер: {0} -> {1})", prev.UdodNumber, eu.UdodNumber);
                if (prev.JurAddress != eu.JurAddress)
                    sb.AppendFormat("(Юридический адрес: {0} -> {1})", prev.JurAddress, eu.JurAddress);
                if (prev.CityId != eu.CityId)
                    sb.AppendFormat("(Округ: {0} -> {1})", prev.CityName, eu.CityName);
                if (prev.FioDirector != eu.FioDirector)
                    sb.AppendFormat("(ФИО Директора: {0} -> {1})", prev.FioDirector, eu.FioDirector);
                if (prev.PhoneNumber != eu.PhoneNumber)
                    sb.AppendFormat("(Телефон: {0} -> {1})", prev.PhoneNumber, eu.PhoneNumber);
                if (prev.FaxNumber != eu.FaxNumber)
                    sb.AppendFormat("(Факс: {0} -> {1})", prev.FaxNumber, eu.FaxNumber);
                if (prev.EMail != eu.EMail)
                    sb.AppendFormat("(EMail: {0} -> {1})", prev.EMail, eu.EMail);
                if (prev.SiteUrl != eu.SiteUrl)
                    sb.AppendFormat("(Сайт: {0} -> {1})", prev.SiteUrl, eu.SiteUrl);
                if (prev.IsDOEnabled != eu.IsDOEnabled)
                    sb.AppendFormat("(Услуги ДО: {0} -> {1})", prev.IsDOEnabled, eu.IsDOEnabled);

                EventsUtility.LogUdodUpdate(UserContext.Profile.UserId, Request.UserHostAddress, sb.ToString());

                long primaryAddressId = 0;
                foreach (GridViewRow row in gvAddresses.Rows)
                {
                    var cbx = row.FindControl("cbxSelect") as CheckBox;
                    if (cbx.Checked)
                    {
                        primaryAddressId = Convert.ToInt64(gvAddresses.DataKeys[row.RowIndex].Value);
                        break;
                    }
                }
                if (primaryAddressId != 0)
                    if (db.UpdateUdodPrimaryAddress(primaryAddressId, UserContext.UdodId.Value))
                    {
                        // Изменился главный адрес надо переотправить все
                        //SiteUtility.SendBatchESZ(null, UserContext.UdodId, true, false, null);
                    }

                using (DictProgramDb db2 = new DictProgramDb())
                foreach (GridViewRow row in gvProgramList.Rows)
                {
                    var cbx = row.FindControl("chkIsExist") as CheckBox;
                    db2.UpdateUdodProgram(cityUdodFilter.SelectedUdod.Value, Convert.ToInt32(gvProgramList.DataKeys[row.RowIndex].Value), cbx.Checked);
                }
            }

            Response.Redirect("~/Udod/UdodList.aspx");
        }
    }

    protected string GetToolTip(object program)
    {
        ExtendedProgram p = (ExtendedProgram)program;
        if (!p.isEnabledToChange)
        {
            if (((p.ChildUnionCount - 1) % 10 == 0) && (p.ChildUnionCount != 11))
            {
                return "Данную направленность отключить невозможно, так как с такой направленностью в данном ОУ существует " + p.ChildUnionCount.ToString() + " активное детское объединение.";
            }
            if (((p.ChildUnionCount - 2) % 10 == 0) || ((p.ChildUnionCount - 3) % 10 == 0) || ((p.ChildUnionCount - 4) % 10 == 0))
            {
                return "Данную направленность отключить невозможно, так как с такой направленностью в данном ОУ существует " + p.ChildUnionCount.ToString() + " активных детских объединения.";
            }
            return "Данную направленность отключить невозможно, так как с такой направленностью в данном ОУ существует " + p.ChildUnionCount.ToString() + " активных детских объединений.";
        }
        else
            return "";
    }

    protected void BackToUdodList_Click(object sender, EventArgs e)
    {
        if (FakeUdodId.HasValue)
        {
            using (UdodDb db = new UdodDb())
            {
                db.DeleteUdod(FakeUdodId.Value);
            }
        }
        Response.Redirect("~/Udod/UdodList.aspx");
    }

    protected void gvChildUnion_SelectedIndexChanged(object sender, EventArgs e)
    {
        isAddDO = false;
        using (ChildUnionDb db = new ChildUnionDb())
        {
            selectDO.Edit = IsDOEnabled;

            selectDO.SetDOData(db.GetChildUnion(Convert.ToInt32(gvChildUnion.SelectedDataKey.Value), null));
            UpdatePanel1.Update();
            popupSelectDOExtender.Show();
        }
    }

    protected void AddDO_Click(object sender, EventArgs e)
    {
        isAddDO = true;
        selectDO.ClearData();
        popupSelectDOExtender.Show();
    }

    protected void gvAddresses_SelectedIndexChanged(object sender, EventArgs e)
    {
        isAddAddress = false;
        using (UdodDb db = new UdodDb())
        {
            List<int> a = new List<int>();
            ExtendedAddress ea = db.GetUdod(UserContext.UdodId.Value).Addresses.Where(i => i.AddressId == Convert.ToInt64(gvAddresses.SelectedDataKey.Value)).FirstOrDefault();
            AddressPopup.IsBti = string.IsNullOrEmpty(ea.KladrCode);
            AddressPopup.Code = string.IsNullOrEmpty(ea.KladrCode) ? ea.BtiCode.Value.ToString() : ea.KladrCode;
            AddressPopup.AddressId = ea.AddressId;
            AddressPopup.NumberHouse = ea.HouseNumber;
            AddressPopup.Fraction = ea.Fraction;
            AddressPopup.Housing = ea.Housing;
            AddressPopup.Building = ea.Building;
            AddressPopup.Flat = ea.FlatNumber;
            AddressPopup.AddressTypeId = ea.AddressType.AddressTypeId;
            AddressPopup.Databind();

            tbAddressWorkTime.Text = ea.AddressWorkTime;
            tbAddressPhone.Text = ea.AddressPhone;
            tbAddressEmail.Text = ea.AddressEmail;
            tbAddressUdodName.Text = ea.AddressUdodName;
        }
        upAddressPopup.Update();
        popupSelectAddressExtender.Show();
        popupSelectAddress.Style.Add("z-index", "999");
    }

    protected void btnSaveAddress_Click(object sender, EventArgs e)
    {
        using (KladrDb db = new KladrDb())
        {
            if (isAddAddress)
            {
                db.AddAddress(AddressPopup.Code, AddressPopup.Index, AddressPopup.NumberHouse, AddressPopup.Fraction, AddressPopup.Housing,
                    AddressPopup.Building, AddressPopup.Flat, AddressPopup.AddressTypeId, 0, 0, null, UserContext.UdodId, tbAddressWorkTime.Text,
                    tbAddressPhone.Text, tbAddressEmail.Text, AddressPopup.typeAddressCode, tbAddressUdodName.Text);
            }
            else
            {
                db.UpdateAddress(AddressPopup.Code, AddressPopup.Index, AddressPopup.NumberHouse, AddressPopup.Fraction, AddressPopup.Housing,
                    AddressPopup.Building, AddressPopup.Flat, AddressPopup.AddressTypeId, 0, 0, null, UserContext.UdodId, tbAddressWorkTime.Text,
                    tbAddressPhone.Text, tbAddressEmail.Text, AddressPopup.AddressId, AddressPopup.typeAddressCode, tbAddressUdodName.Text);
            }

            
        }
        using (UdodDb db = new UdodDb())
        {
            ExtendedUdod udod = db.GetUdod(UserContext.UdodId.Value);
            AddAddress.Visible = udod.Addresses.Count < 1;
            gvAddresses.DataSource = udod.Addresses;
            gvAddresses.DataBind();
        }
        upAddresses.Update();
        popupSelectAddressExtender.Hide();

    }

    protected void btnSaveDOPassport_Click(object sender, EventArgs e)
    {
        //Page.Validate();
        //if (Page.IsValid)
        using (ChildUnionDb db = new ChildUnionDb())
        {
            if (selectDO.IsAgeGroupsGood())
            {
                if (selectDO.IsNameExist(isAddDO))
                {
                    openErrorPopup(3); // 3 - уже есть ДО с таким названием
                }
                else
                {
                    EventsUtility.LogUdodUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                        String.Format("(Идентификатор УДО: {0})(" + (isAddDO ? "Добавление ДО)" : "Изменение ДО)"),
                                                                  UserContext.UdodId.Value));

                    // если уже есть фейковый то новый делать не надо, будет апдейт фекового до настоящего
                    if (selectDO.FakeChildUnionId.HasValue)
                        isAddDO = false;
                    // сохранение происходит внутри паспорта ДО
                    selectDO.GetDOData(isAddDO);

                    popupSelectDOExtender.Hide();

                    //обновляем таблицу
                    ChildUnionDatabind();
                }
            }
        }
    }

    protected void AddAddress_Click(object sender, EventArgs e)
    {
        isAddAddress = true;
        /*
        AddressPopup.Code = "7700000000000";
        AddressPopup.NumberHouse = "";
        AddressPopup.Fraction = "";
        AddressPopup.Housing = "";
        AddressPopup.Building = "";
        AddressPopup.Flat = "";
        AddressPopup.AddressTypeId = 2;

        tbAddressWorkTime.Text = "";
        tbAddressPhone.Text = "";
        tbAddressEmail.Text = "";
        */
        AddressPopup.Databind();
        upAddressPopup.Update();
        popupSelectAddressExtender.Show();
    }

    protected void gvAddress_Deleting(object sender, GridViewDeleteEventArgs e)
    {
        e.Cancel = true;
        
        using (KladrDb db = new KladrDb())
        {
            db.DeleteAddress(Convert.ToInt64(gvAddresses.DataKeys[e.RowIndex].Value), null, UserContext.UdodId, null);
        }

        using (UdodDb db = new UdodDb())
        {
            ExtendedUdod udod = db.GetUdod(UserContext.UdodId.Value);

            EventsUtility.LogUdodUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                String.Format("(Идентификатор УДО: {0})(Название: {1})(Удаление адреса: {2})", udod.UdodId, udod.Name, Convert.ToInt64(gvAddresses.DataKeys[e.RowIndex].Value)));
        
            gvAddresses.DataSource = udod.Addresses;
            AddAddress.Visible = udod.Addresses.Count < 1;
            gvAddresses.DataBind();

        }
    }

    protected void gvChildUnion_Deleting(object sender, GridViewDeleteEventArgs e)
    {
        e.Cancel = true;
        curDOId =  Convert.ToInt64(gvChildUnion.DataKeys[e.RowIndex].Value);

        int pupilCount=0;
        int claimCount=0;
        using (PupilsDb db = new PupilsDb())
        {
            pupilCount = db.GetPupils(UserContext.UdodId.Value, curDOId, null, null, null,null).Count;
        }
        using (ClaimDb db = new ClaimDb())
        {
            // считаем только тех кто подал документы или на рассмотрении
            claimCount = db.GetClaims(null, null, null, curDOId, null, 1, null,
                                        string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                        null, null, string.Empty, null, null, null,null,null,null).Count;
            claimCount += db.GetClaims(null, null, null, curDOId, null, 7, null,
                                        string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                        null, null, string.Empty, null, null, null,null,null,null).Count;
        }

        if ((pupilCount == 0) && (claimCount == 0)) openErrorPopup(0);
        if (pupilCount > 0) openErrorPopup(1);
        if ((pupilCount == 0) && (claimCount > 0)) openErrorPopup(2);
        /*using (ChildUnionDb db = new ChildUnionDb())
        {
            db.DeleteChildUnion(Convert.ToInt64(gvChildUnion.DataKeys[e.RowIndex].Value));

            EventsUtility.LogUdodUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                String.Format("(Идентификатор УДО: {0})(Удаление ДО: {1})", UserContext.UdodId.Value, Convert.ToInt64(gvChildUnion.DataKeys[e.RowIndex].Value)));

            //обновляем таблицу
            var list = db.GetChildUnions(null, UserContext.UdodId.Value, null, null, null, null, null, null, null, null, null);
            gvChildUnion.DataSource = list;
            gvChildUnion.DataBind();
            upDO.Update();

            lblNumItemsInGv.Text = "Всего " + list.Count.ToString() + " элемент(ов)";
        }*/
    }

    protected void lnkChangeMetro_OnClick(object sender, EventArgs e)
    {
        //using(DictMetroDb db = new DictMetroDb())
        //{
        //    var list = db.GetMetros();
        //    ddlMetro.DataSource = list;
        //    ddlMetro.DataBind();
        //}
        //gvMetrodatabind();
        
        //popupSelectMetroExtender.Show();
    }
    private void gvMetrodatabind()
    {
        //using (UdodDb db = new UdodDb())
        //{
        //    var list = db.GetMetro(UserContext.UdodId.Value);
        //    gvUdodMetro.DataSource = list;
        //    gvUdodMetro.DataBind();
        //}
        //upSelectMetro.Update();
    }

    //protected void lnkAddMetro_OnClick(object sender, EventArgs e)
    //{
    //    using (UdodDb db = new UdodDb())
    //    {
    //        db.AddMetro(UserContext.UdodId.Value, Convert.ToInt32(ddlMetro.SelectedValue));
    //    }
    //    gvMetrodatabind();
    //}

    //protected void btnCancelMetro_OnClick(object sender, EventArgs e)
    //{
    //    using (UdodDb db = new UdodDb())
    //    {
    //        var list = db.GetMetro(UserContext.UdodId.Value);
    //        repMetro.DataSource = list;
    //        repMetro.DataBind();
    //    }
    //    upMetro.Update();
    //    popupSelectMetroExtender.Hide();
    //}

    //protected void lnkDeleteMetro_OnClick(object sender, EventArgs e)
    //{
    //    //LinkButton btn = (LinkButton) sender;
    //    //long id = Convert.ToInt64(btn.Attributes["UdodMetroId"]);
    //    //using (UdodDb db = new UdodDb())
    //    //{
    //    //    db.DeleteMetro(id);
    //    //}

    //    //EventsUtility.LogUdodUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
    //    //    String.Format("(Идентификатор УДО: {0})(Удаление метро: {1})", UserContext.UdodId.Value, id));

    //    //gvMetrodatabind();
    //    //upSelectMetro.Update();
    //}

    //protected void gvUdodMetro_OnRowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        LinkButton btn = (LinkButton) e.Row.FindControl("lnkDeleteMetro");
    //        var tmp = (ExtendedUdodMetro) e.Row.DataItem;
    //        btn.Attributes.Add("UdodMetroId", tmp.UdodMetroId.ToString());
    //    }
    //}

    protected void btnCloseDOPassport_Click(object sender, EventArgs e)
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {
            if ((selectDO.FakeChildUnionId.HasValue) && (isAddDO))
            db.RealDeleteChildUnion(selectDO.FakeChildUnionId.Value);
        }
        selectDO.FakeChildUnionId = null;
        popupSelectDOExtender.Hide();
    }

    protected void openErrorPopup(int err)
    {
        if (err == 0)
        {
            lblErrorHeader.Text = "<span class='headerCaptions'>Предупреждение</span>";
            lblErrorText.Text = "<div style='font-size: 12pt; padding-top:10px; padding-bottom: 10px;'>Вы уверены, что хотите удалить данное Детское Объединение?</div>";
            lnkErrorOk.Text = "<div class='btnBlue'>Да</div>";
            lnkErrorOk.Visible = true;
            lnkErrorCancel.Text = "<div class='btnBlue'>Нет</div>";
        }

        if (err == 1)
        {
            lblErrorHeader.Text = "<span class='headerCaptions'>Предупреждение</span>";
            lblErrorText.Text = "<div style='font-size: 12pt; padding-top:10px; padding-bottom: 10px;'>ДО не может быть удалено, т.к. в нем есть обучающиеся.</div>";
            lnkErrorOk.Visible = false;
            lnkErrorCancel.Text = "<div class='btnBlue'>OK</div>";
        }

        if (err == 2)
        {
            lblErrorHeader.Text = "<span class='headerCaptions'>Предупреждение</span>";
            lblErrorText.Text = "<div style='font-size: 12pt; padding-top:10px; padding-bottom: 10px;'>Перевести все заявки в резерв и удалить или Вы самостоятельно обработаете имеющиеся заявки?</div>";
            lnkErrorOk.Text = "<div class='btnBlue'>Резерв</div>";
            lnkErrorOk.Visible = true;
            lnkErrorCancel.Text = "<div class='btnBlue'>Отмена</div>";
        }

        if (err == 3)
        {
            lblErrorHeader.Text = "<span class='headerCaptions'>Ошибка</span>";
            lblErrorText.Text = "<div style='font-size: 12pt; padding-top:10px; padding-bottom: 10px;'>В данном УДО уже есть ДО с таким названием</div>";
            lnkErrorOk.Visible = false;
            lnkErrorCancel.Text = "<div class='btnBlue'>OK</div>";
        }

        if (err == 4)
        {
            lblErrorHeader.Text = "<span class='headerCaptions'>Ошибка</span>";
            lblErrorText.Text = "<div style='font-size: 12pt; padding-top:10px; padding-bottom: 10px;'>Признак \"Есть ли в учреждении блок дополнительного образования\" <br/>может быть отключен только после удаления всех <br/>детских объединений в разделе \"список ДО\" </div>";
            lnkErrorOk.Visible = false;
            lnkErrorCancel.Text = "<div class='btnBlue'>OK</div>";
        }

        CurrentError = err;

        errorModalPopupExtender.Show();
        upError.Update();
    }

    protected void lnkErrorOkOnClick(object sender, EventArgs e)
    {
        if (CurrentError == 0) 
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                db.DeleteChildUnion(curDOId);

                EventsUtility.LogUdodUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                    String.Format("(Идентификатор УДО: {0})(Удаление ДО: {1})", UserContext.UdodId.Value, curDOId));

                //обновляем таблицу
                ChildUnionDatabind();
            }
        }

        if (CurrentError == 2)
        {
            // резерв
            List<ExtendedClaim> list;
            using (ClaimDb db = new ClaimDb())
            {
                list = db.GetClaims(null, null, null, curDOId, null, 1, null,
                                            string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                            null, null, string.Empty, null, null, null, null, null, null);

                foreach (ExtendedClaim claim in list)
                {
                    db.UpdateClaimStatus(claim.ClaimId, 6, null, null,null);
                    //ExportInAsGuf tmp = new ExportInAsGuf();
                    //tmp.UpdateStatus(6, claim.ClaimId,"");
                }

                list = db.GetClaims(null, null, null, curDOId, null, 7, null,
                                            string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                            null, null, string.Empty, null, null, null, null, null,null);

                foreach (ExtendedClaim claim in list)
                {
                    db.UpdateClaimStatus(claim.ClaimId, 6, null, null, null);
                    //ExportInAsGuf tmp = new ExportInAsGuf();
                    //tmp.UpdateStatus(6, claim.ClaimId,"");
                }
            }
        }

        errorModalPopupExtender.Hide();
    }

    protected void lnkErrorCancelOnClick(object sender, EventArgs e)
    {
        errorModalPopupExtender.Hide();
    }

    protected void lnkFind_OnClick(object sender, EventArgs e)
    {
        pgrChildUnionList.Clear();
        ChildUnionDatabind();
    }

    protected void lnkClear_OnClick(object sender, EventArgs e)
    {
        pgrChildUnionList.Clear();
        filterSection.Clear();
        ChildUnionDatabind();
    }

    protected void ChildUnionDatabind()
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {
            pgrChildUnionList.numElements = db.GetChildUnionsCount(null, UserContext.UdodId.Value, null, null, null, null, filterSection.SectionName, filterSection.ProfileName, filterSection.ProgramName, filterSection.ChildUnionName, null, null, null, null, null, null, null, null,null, false,false, null);
            var list = db.GetChildUnionsPaging(null, UserContext.UdodId.Value, null, null, null, null, filterSection.SectionName, filterSection.ProfileName, filterSection.ProgramName, filterSection.ChildUnionName, null, null, null, null, null, pgrChildUnionList.PagerIndex, pgrChildUnionList.PagerLength, OrderBy, null,null, false,false,null);
            gvChildUnion.DataSource = list;
            gvChildUnion.DataBind();
            upDO.Update();
        }
        if ((!string.IsNullOrEmpty(OrderBy)) && (gvChildUnion.HeaderRow != null))
        {
            (gvChildUnion.HeaderRow.Cells[0].FindControl("shName") as UserControl_Menu_ucSortHeader).Refresh(OrderBy.Split(' ')[0], OrderBy.Split(' ')[1]);
            (gvChildUnion.HeaderRow.Cells[1].FindControl("shProgramName") as UserControl_Menu_ucSortHeader).Refresh(OrderBy.Split(' ')[0], OrderBy.Split(' ')[1]);
            (gvChildUnion.HeaderRow.Cells[2].FindControl("shProfileName") as UserControl_Menu_ucSortHeader).Refresh(OrderBy.Split(' ')[0], OrderBy.Split(' ')[1]);
            (gvChildUnion.HeaderRow.Cells[3].FindControl("shSectionName") as UserControl_Menu_ucSortHeader).Refresh(OrderBy.Split(' ')[0], OrderBy.Split(' ')[1]);
            //(gvChildUnion.HeaderRow.Cells[4].FindControl("shTeacher") as UserControl_Menu_ucSortHeader).Refresh(OrderBy.Split(' ')[0], OrderBy.Split(' ')[1]);
            (gvChildUnion.HeaderRow.Cells[5].FindControl("shAge") as UserControl_Menu_ucSortHeader).Refresh(OrderBy.Split(' ')[0], OrderBy.Split(' ')[1]);
        }
    }

    protected void pgrChildUnionList_OnChange(object sender, EventArgs e)
    {
        ChildUnionDatabind();
    }

    protected void gvChildUnionSortingChanged(object sender, SortHeaderEventArgs e)
    {
        OrderBy = e.SortExpression + " " + e.SortDirection;
        ChildUnionDatabind();
    }

    protected void lnkChangeEkis_OnClick(object sender, EventArgs e)
    {
        using(UdodDb db = new UdodDb())
        {
            try
            {
                db.ChangeEkisCode(UserContext.UdodId.Value, Convert.ToInt32(tbEkis.Text));
            }
            catch (Exception e1)
            {
            }
            
        }
    }

    protected void ddlIsIndividual_SelectedIndexChanged(object sender, EventArgs e)
    {
        udodPassport.IsIndividual = Convert.ToBoolean(ddlIsIndividual.SelectedIndex);
        upPassport.Update();
    }
}
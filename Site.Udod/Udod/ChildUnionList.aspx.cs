﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Service.esz;
using Udod.Dal;
using Udod.Dal.Enum;
using System.Data;

public partial class Udod_ChildUnionList : BasePage
{
    public int CurrentError
    {
        get
        {
            return (int)ViewState["CurrentError"];
        }
        set
        {
            ViewState["CurrentError"] = value;
        }
    }

    public long curDOId
    {
        get
        {
            return (long)ViewState["curDOId"];
        }
        set
        {
            ViewState["curDOId"] = value;
        }
    }

    private bool isAddDO
    {
        get
        {
            return (bool)ViewState["isAddDO"];
        }
        set
        {
            ViewState["isAddDO"] = value;
        }
    }

    public bool IsEdit
    {
        get
        {
            return (bool)ViewState["isEdit"];
        }
        set
        {
            ViewState["isEdit"] = value;
        }
    }

    public bool IsDOEnabled
    {
        get
        {
            return (bool)ViewState["IsDOEnabled"];
        }
        set
        {
            ViewState["IsDOEnabled"] = value;
        }
    }

    protected string OrderBy
    {
        get
        {
            return (string)ViewState["OrderBy"];
        }
        set
        {
            ViewState["OrderBy"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsDepartment || UserContext.Roles.IsUdodEmployee || UserContext.Roles.IsOsip || UserContext.Roles.IsUser);
        pFind.DefaultButton = "lnkFind";
        //PageEvents.UdodChanged += UdodChange;
        //PageEvents.CityChanged += UdodChange;
        //litTitle.Text = "Редактирование учреждения";
        tbChildUnionId.Visible= lnkESZ6.Visible= lnkESZ5.Visible = lnkESZ4.Visible = lnkESZ3.Visible = lnkESZ2.Visible = lnkESZ1.Visible = lnkESZ.Visible = UserContext.Roles.IsAdministrator;
        lnkExport.Visible = UserContext.Roles.IsAdministrator || UserContext.Roles.IsUdodEmployee || UserContext.Roles.IsOsip;
        if (!Page.IsPostBack)
        {
            if ((UserContext.Roles.RoleId == (int)EnumRoles.Department) || (UserContext.Roles.RoleId == (int)EnumRoles.Osip))
            {
                gvChildUnion.Columns[8].Visible = false;
                gvChildUnion.Columns[9].Visible = false;
                addDO.Visible = false;
                IsEdit = false;
                //cityUdodFilter.UdodDdlEnabled = false;
                cityUdodFilter.Visible = true;
                
            }
            else
            {
                gvChildUnion.Columns[8].Visible = true;
                gvChildUnion.Columns[9].Visible = true;
                addDO.Visible = true;
                IsEdit = true;
            }

            if (UserContext.Roles.RoleId == (int)EnumRoles.Administrator)
            {
                cityUdodFilter.Visible = true;
                addDO.Visible = false;
            }


            addDO.Visible = gvChildUnion.Columns[8].Visible = IsDOEnabled = true;
            if (cityUdodFilter.SelectedUdod.HasValue)
            {
                using (UdodDb db = new UdodDb())
                {
                    ExtendedUdod udod = db.GetUdod(cityUdodFilter.SelectedUdod.Value);

                    IsDOEnabled = udod.IsDOEnabled;
                    addDO.Visible = gvChildUnion.Columns[8].Visible = IsDOEnabled;
                }
            }

            ChildUnionDatabind();
        }
    }


    protected string GetChildUnionTeachers(object childUnion)
    {
        //var ageGroups = (childUnion as ExtendedChildUnion).AgeGroups;

        //var tmp = ageGroups.Select(i => i.TeacherFio).Distinct().ToList();

        using (ChildUnionDb db = new ChildUnionDb())
        {
            var list = (childUnion as ExtendedChildUnion).Teachers;  //db.GetTeacher((childUnion as ExtendedChildUnion).ChildUnionId);


            StringBuilder sb = new StringBuilder();

            foreach (var teacher in list)
            {
                sb.Append(teacher.Fio + "<br>");
            }
            if (sb.ToString() != "")
                return sb.ToString();
            else
                return "Не задано";
        }
    }

    protected string GetChildUnionAge(object childUnion)
    {
        return "от " + (childUnion as ExtendedChildUnion).AgeStart.ToString() + " до " + (childUnion as ExtendedChildUnion).AgeEnd.ToString();
    }

    /*protected string GetAddresses(object section)
    {
        var ageGroups = (section as ExtendedUdodSection).AgeGroups;
        ageGroups.
        var tmp = ageGroups.Select(i => i.Adresses).ToList();

        StringBuilder sb = new StringBuilder();

        foreach (var addr in tmp)
        {
            sb.Append(addr. + "<br>");
        }
        return sb.ToString();
    }*/

    protected void gvChildUnion_SelectedIndexChanged(object sender, EventArgs e)
    {
        isAddDO = false;
        using (ChildUnionDb db = new ChildUnionDb())
        {
            //btnSaveDOPassport.Visible = UserContext.UdodId.HasValue;
            selectDO.Edit = IsDOEnabled;

            selectDO.SetDOData(db.GetChildUnion(Convert.ToInt32(gvChildUnion.SelectedDataKey.Value), null));
            UpdatePanel1.Update();
            popupSelectDOExtender.Show();
            if (gvChildUnion.SelectedIndex<7)
            {
                popupSelectDO.Attributes.Add("top1", "-1");
            }
            else
            {
                popupSelectDO.Attributes.Add("top1", "0");
            }
        }
    }

    protected void AddDO_Click(object sender, EventArgs e)
    {
        isAddDO = true;
        selectDO.ClearData();
        popupSelectDOExtender.Show();
    }

    protected void btnSaveDOPassport_Click(object sender, EventArgs e)
    {
        //if (Page.IsValid)
        //if (!btnSaveDOPassport.Visible) return; 
        using (ChildUnionDb db = new ChildUnionDb())
        {
            if (selectDO.IsAgeGroupsGood())
            {
                if (selectDO.IsNameExist(isAddDO))
                {
                    openErrorPopup(3); // 3 - уже есть ДО с таким названием
                }
                else
                {
                    EventsUtility.LogUdodUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                        String.Format("(Идентификатор УДО: {0})("+(isAddDO ? "Добавление ДО)":"Изменение ДО "+selectDO.ChildUnionId+")"),
                                                                  UserContext.UdodId.Value));

                    // если уже есть фейковый то новый делать не надо, будет апдейт фекового до настоящего
                    if (selectDO.FakeChildUnionId.HasValue)
                        isAddDO = false;
                    // сохранение происходит внутри паспорта ДО
                    selectDO.GetDOData(isAddDO);

                    popupSelectDOExtender.Hide();

                    ChildUnionDatabind();
                }
            }
        }
    }

    protected void gvChildUnion_Deleting(object sender, GridViewDeleteEventArgs e)
    {
        e.Cancel = true;
        curDOId = Convert.ToInt64(gvChildUnion.DataKeys[e.RowIndex].Value);

        int pupilCount = 0;
        int claimCount = 0;
        using (PupilsDb db = new PupilsDb())
        {
            pupilCount = db.GetPupils(UserContext.UdodId.Value, curDOId, null, null, null, null).Count;
        }
        using (ClaimDb db = new ClaimDb())
        {
            // считаем только тех кто подал документы или на рассмотрении
            claimCount = db.GetClaims(null, null, null, curDOId, null, 1, null,
                                        string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                        null, null, string.Empty, null, null, null,null,null,null).Count;
            claimCount += db.GetClaims(null, null, null, curDOId, null, 7, null,
                                        string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                        null, null, string.Empty, null, null, null, null, null,null).Count;

        }

        if ((pupilCount == 0) && (claimCount == 0)) openErrorPopup(0);
        if (pupilCount > 0) openErrorPopup(1);
        if ((pupilCount == 0) && (claimCount > 0)) openErrorPopup(2);
    }

    protected void btnCloseDOPassport_Click(object sender, EventArgs e)
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {
            if ((selectDO.FakeChildUnionId.HasValue) && (isAddDO))
                db.RealDeleteChildUnion(selectDO.FakeChildUnionId.Value);
        }
        selectDO.FakeChildUnionId = null;
        popupSelectDOExtender.Hide();
    }

    protected void pgrChildUnionList_OnChange(object sender, EventArgs e)
    {
        ChildUnionDatabind();
    }

    protected void cityUdodFilter_OnUdodChangeLoadComplete(object sender, EventArgs e)
    {
        addDO.Visible = gvChildUnion.Columns[8].Visible = IsDOEnabled = true;
        if (cityUdodFilter.SelectedUdod.HasValue)
        {
            using (UdodDb db = new UdodDb())
            {
                ExtendedUdod udod = db.GetUdod(cityUdodFilter.SelectedUdod.Value);

                IsDOEnabled = udod.IsDOEnabled;
                btnSaveDOPassport.Visible = addDO.Visible = gvChildUnion.Columns[8].Visible = IsDOEnabled;
            }
        }
        ChildUnionDatabind();
    }

    protected void ChildUnionDatabind()
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var list = new List<ExtendedChildUnion>();
            bool? isActive = null;
            try
            {
                isActive = Convert.ToBoolean(ddlIsActive.SelectedValue);
            }
            catch (Exception)
            {
            }
            int? typeBudgetId = null;
            try
            {
                typeBudgetId = Convert.ToInt32(ddlBudgetType.SelectedValue);
            }
            catch (Exception)
            {
            }
            long? childUnionId = null;
            try
            {
                childUnionId = Convert.ToInt64(tbChildUnionId.Text);
            }
            catch (Exception)
            {
            }
            if (UserContext.Roles.RoleId == (int)EnumRoles.Administrator || UserContext.Roles.RoleId == (int)EnumRoles.Osip || UserContext.Roles.RoleId == (int)EnumRoles.Department)
            {
                pgrChildUnionList.numElements = db.GetChildUnionsCount(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null, null, typeBudgetId, filterSection.SectionName, filterSection.ProfileName, filterSection.ProgramName, filterSection.ChildUnionName, null, null, null, null, null, null, null, null, isActive, false, false, childUnionId);
                list = db.GetChildUnionsPaging(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null, null, typeBudgetId, filterSection.SectionName, filterSection.ProfileName, filterSection.ProgramName, filterSection.ChildUnionName, null, null, null, null, null, pgrChildUnionList.PagerIndex, pgrChildUnionList.PagerLength, OrderBy, null, isActive, false, false, childUnionId);
            }
            else
            {
                pgrChildUnionList.numElements = db.GetChildUnionsCount(null, UserContext.UdodId.Value, null, null, null, typeBudgetId, filterSection.SectionName, filterSection.ProfileName, filterSection.ProgramName, filterSection.ChildUnionName, null, null, null, null, null, null, null, null, isActive, false, false, childUnionId);
                list = db.GetChildUnionsPaging(null, UserContext.UdodId.Value, null, null, null, typeBudgetId, filterSection.SectionName, filterSection.ProfileName, filterSection.ProgramName, filterSection.ChildUnionName, null, null, null, null, null, pgrChildUnionList.PagerIndex, pgrChildUnionList.PagerLength, OrderBy, null, isActive, false, false, childUnionId);
            }
            gvChildUnion.DataSource = list;
            gvChildUnion.DataBind();
            upDO.Update();
        }
        if ((!string.IsNullOrEmpty(OrderBy)) && (gvChildUnion.HeaderRow != null))
        {
            (gvChildUnion.HeaderRow.Cells[0].FindControl("shName") as UserControl_Menu_ucSortHeader).Refresh(OrderBy.Split(' ')[0], OrderBy.Split(' ')[1]);
            (gvChildUnion.HeaderRow.Cells[1].FindControl("shProgramName") as UserControl_Menu_ucSortHeader).Refresh(OrderBy.Split(' ')[0], OrderBy.Split(' ')[1]);
            (gvChildUnion.HeaderRow.Cells[2].FindControl("shProfileName") as UserControl_Menu_ucSortHeader).Refresh(OrderBy.Split(' ')[0], OrderBy.Split(' ')[1]);
            (gvChildUnion.HeaderRow.Cells[3].FindControl("shSectionName") as UserControl_Menu_ucSortHeader).Refresh(OrderBy.Split(' ')[0], OrderBy.Split(' ')[1]);
            //(gvChildUnion.HeaderRow.Cells[4].FindControl("shTeacher") as UserControl_Menu_ucSortHeader).Refresh(OrderBy.Split(' ')[0], OrderBy.Split(' ')[1]);
            (gvChildUnion.HeaderRow.Cells[5].FindControl("shAge") as UserControl_Menu_ucSortHeader).Refresh(OrderBy.Split(' ')[0], OrderBy.Split(' ')[1]);
        }
    }
    
    protected void openErrorPopup(int err)
    {
        if (err == 0)
        {
            lblErrorHeader.Text = "<span class='headerCaptions'>Предупреждение</span>";
            lblErrorText.Text = "<div style='font-size: 12pt; padding-top:10px; padding-bottom: 10px;'>Вы уверены, что хотите удалить данное Детское Объединение?</div>";
            lnkErrorOk.Text = "<div class='btnBlue'>Да</div>";
            lnkErrorOk.Visible = true;
            lnkErrorCancel.Text = "<div class='btnBlue'>Нет</div>";
        }

        if (err == 1)
        {
            lblErrorHeader.Text = "<span class='headerCaptions'>Предупреждение</span>";
            lblErrorText.Text = "<div style='font-size: 12pt; padding-top:10px; padding-bottom: 10px;'>ДО не может быть удалено, т.к. в нем есть обучающиеся.</div>";
            lnkErrorOk.Visible = false;
            lnkErrorCancel.Text = "<div class='btnBlue'>OK</div>";
        }

        if (err == 2)
        {
            lblErrorHeader.Text = "<span class='headerCaptions'>Предупреждение</span>";
            lblErrorText.Text = "<div style='font-size: 12pt; padding-top:10px; padding-bottom: 10px;'>Перевести все заявки в резерв и удалить или Вы самостоятельно обработаете имеющиеся заявки?</div>";
            lnkErrorOk.Text = "<div class='btnBlue'>Резерв</div>";
            lnkErrorOk.Visible = true;
            lnkErrorCancel.Text = "<div class='btnBlue'>Отмена</div>";
        }

        if (err == 3)
        {
            lblErrorHeader.Text = "<span class='headerCaptions'>Ошибка</span>";
            lblErrorText.Text = "<div style='font-size: 12pt; padding-top:10px; padding-bottom: 10px;'>В данном УДО уже есть ДО с таким названием</div>";
            lnkErrorOk.Visible = false;
            lnkErrorCancel.Text = "<div class='btnBlue'>OK</div>";
        }

        CurrentError = err;

        errorModalPopupExtender.Show();
        upError.Update();
    }

    protected void lnkErrorOkOnClick(object sender, EventArgs e)
    {
        if (CurrentError == 0)
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                var ecu = db.GetChildUnion(curDOId, null);
                db.DeleteChildUnion(curDOId);

                EventsUtility.LogUdodUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
                    String.Format("(Идентификатор УДО: {0})(Удаление ДО: {1})", UserContext.UdodId.Value, curDOId));

                ChildUnionDatabind();

                try
                {
                    
                    //SiteUtility.SendBatchESZ(new List<long>(new long[] { curDOId }));
                    /*
                    #region Отправка в ЕСЗ
                    using (UdodDb db1 = new UdodDb())
                    {
                        //var ecu = db.GetChildUnion(ecu1.ChildUnionId, null);

                        var udod = db1.GetUdod(ecu.UdodId);

                        //if (!udod.IsDOEnabled || udod.UdodStatusId == 2 || ecu.ChildUnionDescriptionId == 0 || udod.CityId == 15) continue;

                        //if (udod.CityId == 11 || udod.CityId == 1) continue;
                        var messageId = Guid.NewGuid();
                        ecu.MessageId = messageId;

                        IntegrationClient client = new IntegrationClient();
                        client.Endpoint.Behaviors.Add(new InterceptorCustomBehavior());

                        ServiceListRequest request = new ServiceListRequest();
                        request.header = new packageHeader();
                        request.header.id = messageId.ToString();
                        request.header.receiver = "ESZ";
                        request.header.sender = "UDOD";
                        request.header.createDateTime = DateTime.Now;
                        ArrayOfServiceListRequestServiceService[] array = new ArrayOfServiceListRequestServiceService[1];

                        array[0] = new ArrayOfServiceListRequestServiceService();
                        //array[0].
                        array[0].id = ecu.ChildUnionId.ToString();
                        array[0].code = ecu.ChildUnionId.ToString();
                        array[0].name = ecu.Name;
                        array[0].programmService = new stringFileField();
                        array[0].programmService.@string = ecu.ProgramService;
                        array[0].ruleService = new stringFileField();

                        // Мин и макс значений возрастов
                        int min = ecu.AgeStart;
                        int max = ecu.AgeEnd;
                        if (ecu.AgeGroups.Count(i => i.NumYearStart <= 1 && i.NumYearEnd >= 1 && i.IsActive) > 0)
                        {
                            min = ecu.AgeGroups.Where(i => i.NumYearStart <= 1 && i.NumYearEnd >= 1 && i.IsActive).Min(i => i.AgeStart);
                            max = ecu.AgeGroups.Where(i => i.NumYearStart <= 1 && i.NumYearEnd >= 1 && i.IsActive).Max(i => i.AgeEnd);
                        }

                        array[0].ruleService.@string = string.Format("Возраст ребёнка для получения данной образовательной услуги: от {0} до {1} лет. Возраст ребёнка для записи с Портала: от {2} до {3} лет.", ecu.AgeStart, ecu.AgeEnd, min, max);
                        array[0].classificatiorRBNDOid = ecu.Section.EszSectionId.ToString();
                        array[0].altAndSex = new ArrayOfServiceListRequestServiceServiceAltAndSex();
                        if (ecu.SexId == 1)
                        {
                            array[0].altAndSex.male = new altsType();
                            array[0].altAndSex.male.altStart = min;
                            array[0].altAndSex.male.altEnd = max;
                        }
                        else if (ecu.SexId == 2)
                        {
                            array[0].altAndSex.femail = new altsType();
                            array[0].altAndSex.femail.altStart = min;
                            array[0].altAndSex.femail.altEnd = max;
                        }
                        else if (ecu.SexId == 3)
                        {
                            array[0].altAndSex.male = new altsType();
                            array[0].altAndSex.male.altStart = min;
                            array[0].altAndSex.male.altEnd = max;

                            array[0].altAndSex.femail = new altsType();
                            array[0].altAndSex.femail.altStart = min;
                            array[0].altAndSex.femail.altEnd = max;
                        }
                        // неизвестно что посылать
                        //array[0].restrictionsId = 
                        //array[0].restrictionTypeId

                        array[0].durationOfTraning = new ArrayOfServiceListRequestServiceServiceDurationOfTraning();
                        array[0].durationOfTraning.ItemElementName = ItemChoiceType.years;
                        array[0].durationOfTraning.Item = ecu.NumYears;

                        array[0].levelId = "2";
                        array[0].placesPlan = 0;

                        //array[0].altStart = ecu.AgeStart;
                        //array[0].altEnd = ecu.AgeEnd;
                        array[0].testService = ecu.TestService;
                        array[0].toursNumber = ecu.ToursNumber;
                        if (ecu.BudgetTypes.Count(i => i.DictTypeBudgetId == 1) > 0)
                            array[0].typeFinancingId = "2";
                        else if (ecu.BudgetTypes.Count(i => i.DictTypeBudgetId == 2) > 0)
                            array[0].typeFinancingId = "1";
                        array[0].typeValueServiceId = "1";
                        array[0].finansing = ecu.Finansing;
                        array[0].finansingSpecified = true;
                        array[0].subFinanceService = ecu.SubFinance;
                        array[0].subFinanceServiceSpecified = true;
                        array[0].dateStart = ecu.DateStart;
                        array[0].dateEnd = ecu.DateEnd;
                        //array[0].sexId = ddlSex.SelectedItem.Value;
                        array[0].statusId = "3";
                        array[0].formsServiceId = ecu.FormId.ToString();
                        array[0].scheduleTypeOfServiceId = ecu.EducationTypes.Count(i => i.EducationTypeId == 1 || i.EducationTypeId == 2) > 0 ? "1" : "2";
                        array[0].periodGettingDocument = ecu.DocsDeadline;
                        array[0].organizationId = UserContext.UdodId.HasValue ? (new UdodDb()).GetUdod(UserContext.UdodId.Value).EkisId.ToString() : ""; //UserContext.UdodId.HasValue?UserContext.UdodId.Value.ToString():"";

                        var teachers = db.GetTeacher(ecu.ChildUnionId);

                        MyStringBuilder sb = new MyStringBuilder(", ", teachers.Select(i => i.TeacherName).ToList());

                        array[0].teacher = sb.ToString();

                        array[0].sheduleOfService = new ArrayOfServiceListRequestServiceServiceSheduleOfService();

                        array[0].placeService = new ArrayOfServiceListRequestServiceServicePlaceService();

                        array[0].sheduleOfService = new ArrayOfServiceListRequestServiceServiceSheduleOfService();

                        array[0].itemsWorks = new ArrayOfServiceListRequestServiceItemsWorkItemsWork[1];
                        array[0].itemsWorks[0] = new ArrayOfServiceListRequestServiceItemsWorkItemsWork();

                        array[0].itemsWorks[0].sheduleOfItemWork = new ArrayOfServiceListRequestServiceItemsWorkItemsWorkSheduleOfItemWork();
                        array[0].itemsWorks[0].sheduleOfItemWork.monday = new ArrayOfServiceListRequestServiceItemsWorkItemsWorkSheduleOfItemWorkMonday();
                        array[0].itemsWorks[0].sheduleOfItemWork.monday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[0].itemsWorks[0].sheduleOfItemWork.monday.timeEnd = new DateTime(2013, 07, 12, 18, 0, 0);

                        array[0].itemsWorks[0].sheduleOfItemWork.friday = new sheduleOfDayType();

                        //array[0].itemsWorks[0].sheduleOfItemWork.friday = new ArrayOfServiceListRequestServiceItemsWorkItemsWorkSheduleOfItemWorkMonday();
                        array[0].itemsWorks[0].sheduleOfItemWork.friday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[0].itemsWorks[0].sheduleOfItemWork.friday.timeEnd = new DateTime(2013, 07, 12, 17, 45, 0);
                        
                        array[0].itemsWorks[0].sheduleOfItemWork.thursday = new sheduleOfDayType();
                        array[0].itemsWorks[0].sheduleOfItemWork.thursday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[0].itemsWorks[0].sheduleOfItemWork.thursday.timeEnd = new DateTime(2013, 07, 12, 18, 0, 0);

                        array[0].itemsWorks[0].sheduleOfItemWork.tuesday = new sheduleOfDayType();
                        array[0].itemsWorks[0].sheduleOfItemWork.tuesday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[0].itemsWorks[0].sheduleOfItemWork.tuesday.timeEnd = new DateTime(2013, 07, 12, 18, 0, 0);

                        array[0].itemsWorks[0].sheduleOfItemWork.wednesday = new sheduleOfDayType();
                        array[0].itemsWorks[0].sheduleOfItemWork.wednesday.timeStart = new DateTime(2013, 07, 12, 9, 0, 0);
                        array[0].itemsWorks[0].sheduleOfItemWork.wednesday.timeEnd = new DateTime(2013, 07, 12, 18, 0, 0);

                        array[0].organizationId = udod.EkisId.ToString();

                        var addresses = udod.Addresses;
                        var address1 = addresses.Where(i => i.IsPrimary).FirstOrDefault();

                        if (address1.AddressId == 0)
                        {
                            address1 = addresses.FirstOrDefault(i => i.BtiHouseCode != null);
                        }
                        if (address1.AddressId == 0)
                        {
                            address1 = addresses.FirstOrDefault();
                        }
                        if (address1.AddressId != 0)
                        {
                            array[0].itemsWorks[0].ItemsWorkId = address1.AddressId.ToString();
                            array[0].itemsWorks[0].name = udod.Name;
                            array[0].itemsWorks[0].address = new addressType();

                            array[0].itemsWorks[0].address.building = address1.HouseNumber;
                            array[0].itemsWorks[0].address.codeStreet = address1.BtiCode.HasValue ? address1.BtiCode.Value.ToString() : "";
                            array[0].itemsWorks[0].address.fullAddress = address1.AddressStr;
                            array[0].itemsWorks[0].address.housing = address1.Housing;
                            array[0].itemsWorks[0].address.structure = address1.Building;
                            if (address1.BtiHouseCode.HasValue)
                                array[0].itemsWorks[0].address.unom = address1.BtiHouseCode.Value.ToString();

                        }
                        var ecuaddresses = ecu.Addresses.Where(i => i.BtiHouseCode != null).ToList();
                        if (ecuaddresses.Count == 0)
                        {
                            ecuaddresses = ecu.Addresses;
                        }

                        foreach (var address in ecuaddresses)
                        {
                            //if (item.Selected)
                            {
                                array[0].placeService.PlaceServiceId = address.AddressId.ToString();
                                array[0].placeService.name = udod.Name;
                                array[0].placeService.address = new addressType();

                                if (address.AddressId != 0)
                                {
                                    array[0].placeService.address.building = address.HouseNumber;
                                    array[0].placeService.address.codeStreet = address.BtiCode.HasValue ? address.BtiCode.Value.ToString() : "";
                                    array[0].placeService.address.fullAddress = address.AddressStr;
                                    array[0].placeService.address.housing = address.Housing;
                                    array[0].placeService.address.structure = address.Building;
                                    if (address.BtiHouseCode.HasValue)
                                        array[0].placeService.address.unom = address.BtiHouseCode.Value.ToString();

                                    //array[0].placeService.address.ownership = address.Building;

                                }



                            }
                        }

                        var scheduleList = db.GetChildUnionSimpleSchedule(ecu.ChildUnionId);
                        foreach (var schedule in scheduleList)
                        {
                            if (schedule.DayOfWeek == 1)
                            {
                                array[0].sheduleOfService.monday = new sheduleOfServiceDayType();
                                ItemsChoiceType[] elementName = new ItemsChoiceType[1];
                                elementName[0] = ItemsChoiceType.shedulePeriodId;
                                array[0].sheduleOfService.monday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[0].sheduleOfService.monday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 2)
                            {
                                array[0].sheduleOfService.tuesday = new sheduleOfServiceDayType();
                                ItemsChoiceType[] elementName = new ItemsChoiceType[1];
                                elementName[0] = ItemsChoiceType.shedulePeriodId;
                                array[0].sheduleOfService.tuesday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[0].sheduleOfService.tuesday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 4)
                            {
                                array[0].sheduleOfService.thursday = new sheduleOfServiceDayType();
                                ItemsChoiceType[] elementName = new ItemsChoiceType[1];
                                elementName[0] = ItemsChoiceType.shedulePeriodId;
                                array[0].sheduleOfService.thursday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[0].sheduleOfService.thursday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 3)
                            {
                                array[0].sheduleOfService.wednesday = new sheduleOfServiceDayType();
                                ItemsChoiceType[] elementName = new ItemsChoiceType[1];
                                elementName[0] = ItemsChoiceType.shedulePeriodId;
                                array[0].sheduleOfService.wednesday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[0].sheduleOfService.wednesday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 5)
                            {
                                array[0].sheduleOfService.friday = new sheduleOfServiceDayType();
                                ItemsChoiceType[] elementName = new ItemsChoiceType[1];
                                elementName[0] = ItemsChoiceType.shedulePeriodId;
                                array[0].sheduleOfService.friday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[0].sheduleOfService.friday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 6)
                            {
                                array[0].sheduleOfService.saturday = new sheduleOfServiceDayType();
                                ItemsChoiceType[] elementName = new ItemsChoiceType[1];
                                elementName[0] = ItemsChoiceType.shedulePeriodId;
                                array[0].sheduleOfService.saturday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[0].sheduleOfService.saturday.ItemsElementName = elementName;
                            }
                            if (schedule.DayOfWeek == 7)
                            {
                                array[0].sheduleOfService.sunday = new sheduleOfServiceDayType();
                                ItemsChoiceType[] elementName = new ItemsChoiceType[1];
                                elementName[0] = ItemsChoiceType.shedulePeriodId;
                                array[0].sheduleOfService.sunday.Items = new object[1] { schedule.TimeDayId == 4 || schedule.TimeDayId == 3 ? "4" : schedule.TimeDayId.ToString() };
                                array[0].sheduleOfService.sunday.ItemsElementName = elementName;
                            }
                        }
                        //array[0].sheduleOfService.monday.

                        array[0].OrganizationResource = new ArrayOfServiceListRequestServiceOrganizationResourceOrganizationResource[1];
                        array[0].OrganizationResource[0] = new ArrayOfServiceListRequestServiceOrganizationResourceOrganizationResource();
                        array[0].OrganizationResource[0].name = udod.FioDirector;
                        array[0].OrganizationResource[0].email = udod.EMail;
                        array[0].OrganizationResource[0].phone = udod.PhoneNumber;
                        array[0].OrganizationResource[0].organizationResourceId = udod.EkisId.ToString();
                        array[0].OrganizationResource[0].position = "Директор";

                        request.services = array;
                        //newesz.Integration dd = new Integration();

                        

                        client.Open();
                        try
                        {
                            var result = client.ServiceList(request);
                            int ff = 0;
                        }
                        catch (Exception)
                        {
                            //writer.WriteLine(ecu.ChildUnionId);
                        }
                        client.Close();
                    }

                    #endregion
                    */
                }
                catch (Exception)
                {
                    
                }
            }
        }

        if (CurrentError == 2)
        {
            // резерв
            List<ExtendedClaim> list;
            using (ClaimDb db = new ClaimDb())
            {
                list = db.GetClaims(null, null, null, curDOId, null, 1, null,
                                            string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                            null, null, string.Empty, null, null, null, null, null,null);

                foreach (ExtendedClaim claim in list)
                {
                    db.UpdateClaimStatus(claim.ClaimId, 6, null, null, null);
                    //ExportInAsGuf tmp = new ExportInAsGuf();
                    //tmp.UpdateStatus(6, claim.ClaimId,"");
                }

                list = db.GetClaims(null, null, null, curDOId, null, 7, null,
                                            string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                            null, null, string.Empty, null, null, null, null, null,null);

                foreach (ExtendedClaim claim in list)
                {
                    db.UpdateClaimStatus(claim.ClaimId, 6, null, null, null);
                    //ExportInAsGuf tmp = new ExportInAsGuf();
                    //tmp.UpdateStatus(6, claim.ClaimId,"");
                }
            }
        }

        errorModalPopupExtender.Hide();
    }

    protected void lnkErrorCancelOnClick(object sender, EventArgs e)
    {
        errorModalPopupExtender.Hide();
    }

    protected void lnkFind_OnClick(object sender, EventArgs e)
    {
        pgrChildUnionList.Clear();
        ChildUnionDatabind();
    }

    protected void lnkClear_OnClick(object sender, EventArgs e)
    {
        pgrChildUnionList.Clear();
        filterSection.Clear();
        ddlIsActive.SelectedIndex = 0;
        ChildUnionDatabind();
    }

    protected void gvChildUnionSortingChanged(object sender, SortHeaderEventArgs e)
    {
        OrderBy = e.SortExpression + " " + e.SortDirection;
        ChildUnionDatabind();
    }

    protected void lnkExport_OnClick(object sender, EventArgs e)
    {
        // экспорт в Excel и выдача файла
        HttpContext.Current.Response.Clear();

        string filename = string.Format("ChildUnionList.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                                string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];
        #region Стили
        Aspose.Cells.Style style_area = workbook.Styles[0];
        Aspose.Cells.Style style_header = workbook.Styles[1];
        style_area.ShrinkToFit = true;

        style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
        style_header.Font.IsBold = true;
        style_header.Font.Size = 12;
        style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
        style_header.BackgroundColor = System.Drawing.Color.Gray;
        style_header.Pattern = Aspose.Cells.BackgroundType.Solid;
        #endregion
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var childunions = db.GetChildUnionsForExport(cityUdodFilter.SelectedUdod);

            sheet.Cells[0, 0].PutValue("Наименование ДО");
            sheet.Cells[0, 1].PutValue("Направленность");
            sheet.Cells[0, 2].PutValue("Профиль");
            sheet.Cells[0, 3].PutValue("Вид деятельности");
            sheet.Cells[0, 4].PutValue("Категория программы");
            sheet.Cells[0, 5].PutValue("Возраст от");
            sheet.Cells[0, 6].PutValue("Возраст до");
            sheet.Cells[0, 7].PutValue("Вид услуги");
            sheet.Cells[0, 8].PutValue("ФИО педагога");
            sheet.Cells[0, 9].PutValue("Срок обучения");
            sheet.Cells[0, 10].PutValue("Форма занятия");
            sheet.Cells[0, 11].PutValue("Продолжительность и кол-во часов в неделю");
            sheet.Cells[0, 12].PutValue("публиковать ли на ПГУ");
            sheet.Cells[0, 13].PutValue("ведётся ли набор");
            int i = 1;
            foreach (var childunion in childunions)
            {
                if (UserContext.Roles.IsAdministrator)
                {
                    sheet.Cells[i, 0].PutValue("\"" + childunion.Name.Trim().Replace("\"", "'") + "\"");
                    sheet.Cells[i, 1].PutValue("\"" + childunion.Program.Name.Trim().Replace("\"", "'") + "\"");
                    sheet.Cells[i, 2].PutValue("\"" + childunion.Profile.Name.Trim().Replace("\"", "'") + "\"");
                    sheet.Cells[i, 3].PutValue("\"" + childunion.Section.Name.Trim().Replace("\"", "'") + "\"");
                    sheet.Cells[i, 4].PutValue("\"" + childunion.ProgramCategory.ProgramCategoryName.Trim().Replace("\"", "'") + "\"");
                    sheet.Cells[i, 5].PutValue(childunion.AgeStart);
                    sheet.Cells[i, 6].PutValue(childunion.AgeEnd);
                    string tmp = "";
                    foreach (var budgettype in childunion.BudgetTypes)
                    {
                        tmp = tmp + budgettype.TypeBudgetName + ";";
                    }
                    sheet.Cells[i, 7].PutValue("\"" + tmp.Trim().Replace("\"", "'") + "\"");
                    tmp = "";
                    foreach (var teacher in childunion.Teachers)
                    {
                        tmp = tmp + teacher.Fio + ";";
                    }
                    sheet.Cells[i, 8].PutValue("\"" + tmp.Trim().Replace("\"", "'") + "\"");
                    sheet.Cells[i, 9].PutValue(childunion.NumYears);
                    tmp = "";

                    foreach (var edutype in childunion.EducationTypes)
                    {
                        tmp = tmp + edutype.EducationTypeName + ";";
                    }

                    sheet.Cells[i, 10].PutValue("\"" + tmp.Trim().Replace("\"", "'") + "\"");
                    tmp = "";

                    foreach (var agegroup in childunion.AgeGroups)
                    {
                        tmp = tmp + string.Format("Года:{0}-{1} Возраст:{2}-{3} Пр-сть и кол-во:{4},{5};", agegroup.NumYearStart, agegroup.NumYearEnd, agegroup.AgeStart, agegroup.AgeEnd, agegroup.LessonLength, agegroup.LessonsInWeek);
                    }
                    sheet.Cells[i, 11].PutValue("\"" + tmp.Trim().Replace("\"", "'") + "\"");
                    sheet.Cells[i, 12].PutValue(childunion.isPublicPortal?"Да":"Нет");
                    sheet.Cells[i, 13].PutValue(childunion.isReception?"Да":"Нет");
                    sheet.Cells[i, 14].PutValue(childunion.UdodId);
                    sheet.Cells[i, 15].PutValue(childunion.ChildUnionId);
                }
                else
                {


                    sheet.Cells[i, 0].PutValue(childunion.Name);
                    sheet.Cells[i, 1].PutValue(childunion.Program.Name);
                    sheet.Cells[i, 2].PutValue(childunion.Profile.Name);
                    sheet.Cells[i, 3].PutValue(childunion.Section.Name);
                    sheet.Cells[i, 4].PutValue(childunion.ProgramCategory.ProgramCategoryName);
                    sheet.Cells[i, 5].PutValue(childunion.AgeStart);
                    sheet.Cells[i, 6].PutValue(childunion.AgeEnd);
                    string tmp = "";
                    foreach (var budgettype in childunion.BudgetTypes)
                    {
                        tmp = tmp + budgettype.TypeBudgetName + ";";
                    }
                    sheet.Cells[i, 7].PutValue(tmp);
                    tmp = "";
                    foreach (var teacher in childunion.Teachers)
                    {
                        tmp = tmp + teacher.Fio + ";";
                    }
                    sheet.Cells[i, 8].PutValue(tmp);
                    sheet.Cells[i, 9].PutValue(childunion.NumYears);
                    tmp = "";

                    foreach (var edutype in childunion.EducationTypes)
                    {
                        tmp = tmp + edutype.EducationTypeName + ";";
                    }

                    sheet.Cells[i, 10].PutValue(tmp);
                    tmp = "";

                    foreach (var agegroup in childunion.AgeGroups)
                    {
                        tmp = tmp +
                              string.Format("Года:{0}-{1} Возраст:{2}-{3} Пр-сть и кол-во:{4},{5};",
                                            agegroup.NumYearStart, agegroup.NumYearEnd, agegroup.AgeStart,
                                            agegroup.AgeEnd, agegroup.LessonLength, agegroup.LessonsInWeek);
                    }
                    sheet.Cells[i, 11].PutValue(tmp);
                    sheet.Cells[i, 12].PutValue(childunion.isPublicPortal ? "Да" : "Нет");
                    sheet.Cells[i, 13].PutValue(childunion.isReception ? "Да" : "Нет");
                    
                    sheet.Cells[i, 14].PutValue(childunion.UdodId);
                    sheet.Cells[i, 15].PutValue(childunion.ChildUnionId);
                }
                i++;
            }
            sheet.AutoFitColumns();
            sheet.Cells.HideColumn(14);
            sheet.Cells.HideColumn(15);
            sheet.Cells.CreateRange(1, 0, i, 14).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
            sheet.Cells.CreateRange(0, 0, 1, 14).ApplyStyle(style_header, new Aspose.Cells.StyleFlag() { All = true });


        }
        

        workbook.Save("ChildUnionList.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }

    protected void lnkESZ_OnClick(object sender, EventArgs e)
    {
        //SiteUtility.SendBatchESZ(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, true, false, null);

    }

    protected void lnkESZ1_OnClick(object sender, EventArgs e)
    {
        // Запуск всекх ДО и групп
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var childunions = db.GetChildUnionsPaging(UserContext.CityId, UserContext.UdodId, null, null, null, null, null, null, null, null, null,
                                               null, null, null, null, 1, 500000, "", null, null, false, false,
                                               null);
            foreach (var ecu in childunions)
            {
                //SiteUtility.SendBatchESZ(UserContext.CityId, UserContext.UdodId, true, false, ecu.ChildUnionId);
                int checkPriem = 3;

                if (!ecu.isReception) checkPriem = 0;
                else checkPriem = 1;
                using (AgeGroupDb db2 = new AgeGroupDb())
                {
                    var list = db2.GetGroupsPaging(UserContext.UdodId, "", ecu.ChildUnionId, null, 1, null, "", 1,
                                                   500000,
                                                   null);

                    //SiteUtility.SendBatchScheduleESZ(ecu.ChildUnionId, checkPriem, list);
                }
                /*List<long> list = new List<long>(new long[] { 63920 });
        
                SiteUtility.SendBatchESZ(list);*/
            }
        }
    }

    protected void lnkESZ2_OnClick(object sender, EventArgs e)
    {
        //SiteUtility.SendBatchESZ(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, false, false, null);
        // Запуск всекх ДО и групп
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var childunions = new List<long>(new long[] { 66, 649, 655, 656, 659, 660, 692, 764, 767, 769, 777, 778, 8406, 16136, 36806, 36807, 36809, 36810, 36811, 36812, 36813, 39218, 39219, 39220, 39221, 39222, 39223, 39224, 39225, 39226, 39228, 39229, 39230, 39786, 39789, 39800, 39801, 39807, 39810, 39814, 39815, 39841, 39843, 39846, 39849, 39852, 39853, 39864, 39866, 39873, 39874, 39875, 39899, 39905, 39906, 39912, 39913, 39929, 39931, 39933, 39936, 39939, 39940, 39941, 39943, 39944, 39945, 39953, 39954, 39956, 39959, 39964, 39969, 39970, 39981, 39983, 39984, 39986, 39993, 39994, 39995, 39996, 39997, 39998, 40000, 40001, 40004, 40008, 40010, 43886, 51348, 51349, 51358, 53272, 53283, 53435, 53489, 53508, 53510, 53517, 53525, 53537, 53551, 53560, 53814, 53827, 53832, 58663, 58697, 58825, 58829, 58831, 58833, 58836, 58844, 58882, 58885, 58887, 58888, 58914, 58918, 58921, 58932, 58958, 59072, 59074, 59078, 59083, 59087, 59103, 59143, 60335, 62943, 62992, 62997, 63091, 63134, 63139, 63477, 63501, 63506, 63536, 63538, 63544, 63545, 63546, 63548, 63553, 63559, 63561, 63565, 63569, 63696, 63700, 63703, 63866, 63873, 63987, 64585, 64590, 64599, 65012, 65251, 65254, 67902, 68089, 69181, 69203, 69209, 70219, 71274, 74807, 75681, 75807, 75813, 75817, 75819, 75825, 75827, 75829, 76261, 76262, 76265, 76266, 76269, 76270, 76272, 76676, 79907, 79910, 79913, 79915, 79917, 79918, 79921, 79950, 79952, 80196, 82595, 82596, 82597, 82598, 82599, 82600, 82601, 82629, 83243, 83716, 83812, 83820, 83823, 84410, 84975, 85006, 85027, 85135, 85176, 85224, 85245, 85253, 85273, 85334, 85350, 85351, 85362, 85477, 85478, 85480, 85776, 85777, 85778, 85831, 85832, 85835, 85836, 85837, 85839, 85841, 85843, 85844, 85845, 85846, 85847, 85848, 85851, 85853, 85854, 85855, 85856, 85857, 85858, 85859, 85860, 85861, 85872, 85873, 85874, 85876, 85877, 85878, 85880, 85881, 85980, 85982, 85983, 85985, 85986, 85987, 85989, 85991, 85996, 85997, 85998, 86000, 86004, 86010, 86023, 86024, 86026, 86027, 86029, 86222, 86223, 86226, 86499, 86691, 86744, 88483, 88494, 88575, 88614, 88691, 88755, 88819, 88822, 88824, 89000, 89170, 90507, 90512, 90528, 90538, 90548, 90551, 90618, 90718, 90745, 90860, 90879, 90881, 90883, 90886, 90889, 90892, 90896, 90899, 90900, 90902, 90904, 90905, 90908, 90911, 90914, 90916, 90918, 90920, 90925, 90932, 90971, 90981, 91147, 91212, 91217, 91242, 91420, 91444, 91446, 91532, 91550, 91551, 91659, 91744, 91750, 91751, 91753, 91754, 91862, 91863, 91866, 91931, 91936, 91944, 91952, 91959, 91982, 91983, 92021, 92138, 92140, 92143, 92161, 92165, 92166, 92182, 92361, 92365, 92385, 92391, 92392, 92393, 92394, 92395, 92795, 94403, 94410, 94418, 94424, 94429, 94448, 94472, 94504, 94511, 94514, 94517, 94529, 94532, 94537, 94551, 94581, 94585, 94701, 94708, 94709, 94712, 94731, 94751, 94781, 94785, 94802, 94832, 94834, 94835, 94838, 94842, 94885, 94887, 94895, 95041, 95042, 95057, 95065, 95069, 95071, 95075, 95078, 95141, 95198, 95206, 95210, 95213, 95216, 95219, 95473, 95478, 95513, 95603, 95649, 95799, 95855, 95888, 95901, 95907, 95933, 95988, 96136, 96174, 96177, 96219, 96220, 96221, 96222, 96224, 96225, 96271, 96400, 96402, 96409, 96432, 96435, 96826, 96834, 96837, 96838, 96839, 96840, 96841, 96843, 96844, 96883, 97184, 97195, 97196, 97199, 97265, 97266, 97267, 97268, 97300, 97378, 97380, 97384, 97385, 97387, 97388, 97389, 97401, 97402, 97403, 97587, 97674, 97842, 97888, 97889, 97897, 97906, 97913, 97921, 97932, 97934, 97978, 97984, 97987, 98065, 98274, 98275, 98276, 98278, 98281, 98282, 98283, 98284, 98289, 98298, 98307, 98351, 98370, 98371, 98372, 98454, 98455, 98456, 98459, 98460, 98461, 98462, 98465, 98467, 98469, 98470, 98472, 98473, 98474, 98475, 98476, 98477, 98616, 98621, 98642, 98644, 98645, 98646, 98649, 98725, 98778, 98798, 98817, 98860, 98927, 98943, 98946, 98985, 98987, 98989, 98992, 99017, 99026, 99027, 99029, 99098, 99151, 99157, 99159, 99160, 99161, 99163, 99165, 99176, 99183, 99188, 99194, 99202, 99211, 99244, 99250, 99258, 99270, 99272, 99273, 99275, 99277, 99279, 99281, 99282, 99288, 99289, 99291, 99293, 99296, 99302, 99305, 99308, 99313, 99315, 99344, 99396, 99449, 99457, 99688, 99832, 99938, 99944, 99962, 99965, 99968, 99969, 99970, 99972, 100095, 100099, 100115, 100129, 100146, 100166, 100183, 100191, 100204, 100211, 100216, 100217, 100276, 100293, 100314, 100320, 100321, 100401, 100405, 100410, 100504, 100510, 100514, 100517, 100521, 100544, 100547, 100558, 100570, 100572, 100573, 100578, 100603, 100612, 100613, 100624, 100630, 100664, 100672, 100675, 100683, 100699, 100749, 100758, 100765, 100766, 100767, 100768, 100770, 100771, 100773, 100774, 100775, 100776, 100781, 100785, 100787, 100791, 100794, 100823, 100840, 100877, 100883, 100888, 100903, 100905, 100906, 100916, 100919, 100922, 100935, 100944, 100972, 100973, 100980, 100981, 100982, 100984, 100985, 100998, 101012, 101018, 101027, 101031, 101064, 101068, 101181, 101189, 101191, 101201, 101208, 101216, 101221, 101225, 101228, 101238, 101239, 101242, 101245, 101257, 101287, 101309, 101341, 101346, 101348, 101401, 101404, 101462, 101487, 101633, 101641, 101646, 101653, 101655, 101661, 101662, 101668, 101709, 101711, 101721, 101749, 101787, 101859, 101890, 101895, 101910, 101917, 101921, 101945, 102000, 102038, 102072, 102088, 102089, 102307, 102313, 102336, 102635, 102719, 102771, 102772, 102787, 102835, 102929, 102935, 102964, 103029, 103047, 103048, 103264, 103268, 103458, 103570, 103732, 103850, 103859, 103860, 103864, 103992, 103994, 103995, 104037, 104038, 104044, 104045, 104046, 104047, 104048, 104087, 104118, 104145, 104271, 104285, 104291, 104380, 104381, 104382, 104384, 104388, 104595, 104596, 104757, 104794, 104807, 104915, 104968, 105144, 105188, 105207, 105235, 105436, 105442, 105447, 105709, 105869, 105872, 105874, 105876, 105877, 105878, 105879, 105882, 105883, 105885, 105887, 105889, 106329, 106408, 106410, 106416, 106425, 106433, 106436, 106437, 106441, 106449, 106450, 106520, 106552, 106592, 106653, 106663, 106670, 106680, 106685, 106775, 106777, 106781, 106783, 106966, 106995, 106996, 106997, 107045, 107046, 107047, 107048, 107049, 107050, 107051, 107068, 107119, 107129, 107131, 107161, 107178, 107218, 107442, 107477, 107493, 107501, 107650, 107652, 107679, 107713, 107734, 107851, 107924, 108104, 108116, 108148, 108173, 108198, 108246, 108250, 108253, 108254, 108255, 108260, 108305, 108342, 108377, 108479, 108509, 108571, 108574, 108576, 108604, 108715, 108731, 108735, 109067, 109069, 109077, 109085, 109093, 109236, 109355, 109520, 109578, 109641, 109799, 109819, 109843, 109933, 109949, 109953, 109971, 110082, 110159, 110164, 110175, 110199, 110204, 110205, 110295, 110318, 110335, 110351, 110388, 110391, 110400, 110450, 110584, 110587, 110720, 110761, 110771, 110783, 110847, 110848, 110849, 110912, 110913, 110914, 110915, 110935, 110938, 110939, 110940, 110941, 110942, 110952, 110953, 110954, 110955, 110956, 110957, 110958, 110959, 110960, 110961, 110962, 110963, 110966, 110967, 110968, 110969, 110970, 110971, 110973, 110974, 110975, 110976, 110977, 110978, 110979, 110980, 110981, 110982, 110983, 110984, 110985, 110986, 110987, 110988, 110989, 110990, 110991, 110992, 110993, 110994, 110995, 110996, 110998, 110999, 111000, 111003, 111004, 111005, 111006, 111007, 111008, 111009, 111010, 111011, 111012, 111013, 111014, 111015, 111016, 111017, 111018, 111019, 111020, 111021, 111022, 111024, 111025, 111028, 111029, 111030, 111031, 111032, 111033, 111034, 111036, 111038, 111039, 111040, 111041, 111042, 111044, 111045, 111046, 111047, 111048, 111049, 111050, 111051, 111052, 111054, 111055, 111056, 111057, 111058, 111059, 111060, 111061, 111062, 111063, 111064, 111065, 111067, 111068, 111069, 111070, 111072, 111073, 111074, 111075, 111076, 111077, 111078, 111079, 111080, 111081, 111082});
            foreach (var ecu1 in childunions)
            {
                var ecu = db.GetChildUnion(ecu1, null);

                //SiteUtility.SendBatchESZ(null, null, true, false, ecu.ChildUnionId);
                int checkPriem = 3;

                if (!ecu.isReception) checkPriem = 0;
                else checkPriem = 1;
                using (AgeGroupDb db2 = new AgeGroupDb())
                {
                    var list = db2.GetGroupsPaging(ecu.UdodId, "", ecu.ChildUnionId, null, 1, null, "", 1,
                                                   500000,
                                                   null);

                    //SiteUtility.SendBatchScheduleESZ(ecu.ChildUnionId, checkPriem, list);
                }
                /*List<long> list = new List<long>(new long[] { 63920 });
        
                SiteUtility.SendBatchESZ(list);*/
            }
        }
    }

    protected void lnkESZ3_OnClick(object sender, EventArgs e)
    {
        List<ExtendedGroup> list = new List<ExtendedGroup>();
        using (AgeGroupDb db = new AgeGroupDb())
        {
            list = db.GetGroupsWithoutFirstYear(null, "", null, null, null, null, "", 1, 500000, null);
        }
        //SiteUtility.SendBatchDeleteScheduleESZ(list);
    }

    protected void lnkReestr_OnClick(object sender, EventArgs e)
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {


            var list = db.GetChildUnionsShort(UserContext.CityId, UserContext.UdodId, null, null, null,
                                              null, null, null, null, null, null, null, null, null, null,
                                              null, false);
            foreach (var extendedChildUnion in list)
            {
                var childUnion = db.GetChildUnion(extendedChildUnion.ChildUnionId, null);
                var teachers = db.GetTeacher(extendedChildUnion.ChildUnionId);
                try
                {
                    //SiteUtility.ReestrAddDO(childUnion, !string.IsNullOrEmpty(childUnion.ReestrCode), teachers);
                }
                catch (Exception)
                {
                }
                
            }
        }
    }

    protected void lnkESZ4_OnClick(object sender, EventArgs e)
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var list = new List<ExtendedChildUnion>();
            bool? isActive = null;
            try
            {
                isActive = Convert.ToBoolean(ddlIsActive.SelectedValue);
            }
            catch (Exception)
            {
            }
            long? childunionId = null;
            try
            {
                childunionId = Convert.ToInt64(tbChildUnionId.Text);
            }
            catch (Exception)
            {
            }
            if (UserContext.Roles.RoleId == (int) EnumRoles.Administrator)
            {
                pgrChildUnionList.numElements = db.GetChildUnionsCount(cityUdodFilter.SelectedCity,
                                                                       cityUdodFilter.SelectedUdod, null, null, null,
                                                                       null, filterSection.SectionName,
                                                                       filterSection.ProfileName,
                                                                       filterSection.ProgramName,
                                                                       filterSection.ChildUnionName, null, null, null,
                                                                       null, null, null, null, null, isActive, false,
                                                                       false, childunionId);
                list = db.GetChildUnionsPaging(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null,
                                               null, null, filterSection.SectionName, filterSection.ProfileName,
                                               filterSection.ProgramName, filterSection.ChildUnionName, null, null, null,
                                               null, null, 1, 500000, OrderBy, null, isActive, false, false, childunionId);
            }
            using (AgeGroupDb db1 = new AgeGroupDb())
            {
                foreach (var childUnion in list)
                {
                    try
                    {
                        var groups = db1.GetGroupsPaging(null, "", childUnion.ChildUnionId, null, null, null, "", 1, 500000,
                                                     null);
                        //SiteUtility.SendBatchScheduleESZ(childUnion.ChildUnionId, childUnion.isReception ? 1 : 0, groups);
                    }
                    catch (Exception)
                    {
                        
                    }
                    
                }
            }
        }
    }

    protected void lnkESZ5_OnClick(object sender, EventArgs e)
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var list = new List<ExtendedChildUnion>();
            bool? isActive = null;
            try
            {
                isActive = Convert.ToBoolean(ddlIsActive.SelectedValue);
            }
            catch (Exception)
            {
            }
            long? childunionId = null;
            try
            {
                childunionId = Convert.ToInt64(tbChildUnionId.Text);
            }
            catch (Exception)
            {
            }
            if (UserContext.Roles.RoleId == (int) EnumRoles.Administrator)
            {
                pgrChildUnionList.numElements = db.GetChildUnionsCount(cityUdodFilter.SelectedCity,
                                                                       cityUdodFilter.SelectedUdod, null, null, null,
                                                                       null, filterSection.SectionName,
                                                                       filterSection.ProfileName,
                                                                       filterSection.ProgramName,
                                                                       filterSection.ChildUnionName, null, null, null,
                                                                       null, null, null, null, null, isActive, false,
                                                                       false, childunionId);
                list = db.GetChildUnionsPaging(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null,
                                               null, null, filterSection.SectionName, filterSection.ProfileName,
                                               filterSection.ProgramName, filterSection.ChildUnionName, null, null, null,
                                               null, null, 1, 500000, OrderBy, null, isActive, false, false, childunionId);
            }
            //SiteUtility.SendBatchScheduleDeleteESZ(list);
        }
    }

    protected void lnkSMS_OnClick(object sender, EventArgs e)
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {
            long? childunionId = null;
            try
            {
                childunionId = Convert.ToInt64(tbChildUnionId.Text);
            }
            catch (Exception)
            {
            }
            var list = db.GetChildUnionsPaging(null, null, null, null, null, null, null, null, null, null, null, null,
                                               null, null, null, 1, 500000, "", null, null, false, false, childunionId);
            //var list1 = db.GetDeletedChildUnions(new DateTime(2013, 08, 16));
            list = list.Where(i => !i.isReception).ToList();
            //list.AddRange(list1);
            //var childUnion = list.FirstOrDefault();
            using (AgeGroupDb db2 = new AgeGroupDb())
            {
            foreach (var childUnion in list)
            {
                //SiteUtility.SendScheduleESZ(childUnion.ChildUnionId, childUnion.ChildUnionId, 5000, new DateTime(2013,09,01), new DateTime(2013,09,01), new DateTime(2014,05,31), 3);

                var list1 = db2.GetGroupsPaging(UserContext.UdodId, "", childUnion.ChildUnionId, null, null, null, "", 1, 500000,
                                                   null);
                int checkPriem = 3;
                if (!childUnion.isReception) checkPriem = 0; else checkPriem = 1;
                //SiteUtility.SendBatchScheduleESZ(childUnion.ChildUnionId, checkPriem, list1);
                }
            }

        }
    }

    protected void ddlBudgetType_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ChildUnionDatabind();
    }

    protected void ddlBudgetType_OnDataBound(object sender, EventArgs e)
    {
        ddlBudgetType.Items.Insert(0, new ListItem("Все", ""));
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="Schedule.aspx.cs" Inherits="Udod_Schedule" %>
<%@ Register src="../UserControl/Filter/ucCityUdodFilter.ascx" tagName="CityUdodFilter" tagPrefix="uct" %>
<%@ Register src="../UserControl/Menu/ucErrorPopup.ascx" tagName="ErrorPopup" tagPrefix="uct" %>



<asp:Content runat="server" ID="content" ContentPlaceHolderID="body">

<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoad1);
    function pageLoad1(sender, args) {
        //alert('ok');
        if ($('<%= "#"+hf.ClientID %>').val() == "true") {
            $('<%= "#"+gvShedule.ClientID %>').find('td.deleted').each(function() {
                $(this).remove();
            });
            $('<%= "#"+gvShedule.ClientID %>').find('th.deleted').each(function() {
                $(this).remove();
            });
            var newrow = document.createElement('tr');
            newrow.innerHTML = '<th rowspan="2">Наименование ДО</th><th rowspan="2">Наименование группы</th><th colspan="2">Понедельник</th><th colspan="2">Вторник</th><th colspan="2">Среда</th><th colspan="2">Четверг</th><th colspan="2">Пятница</th><th colspan="2">Суббота</th><th colspan="2">Воскресенье</th>';
            var firstrow = $('<%= "#"+gvShedule.ClientID %>').find('tr')[0];
            if (firstrow != null) firstrow.parentNode.insertBefore(newrow, firstrow);
        }
    }


    var cbxPupils = '<%= cblPupils.ClientID %>';
    function isValidPupils(source, args) {
        args.IsValid = $('#' + cbxPupils).find("input:checked").length > 0;
        //if ($('#'+cbxAddress).find("input").length == 0) args.IsValid = true;
    }
</script>

<table>
    </table>

<asp:UpdatePanel ID="upCityUdodFilter" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <uct:CityUdodFilter ID="cityUdodFilter" runat="server" UdodDdlEnabled="true" AddAllProperty="true" OnCityChange="cityUdodFilter_OnCityChange" OnUdodChange="cityUdodFilter_OnUdodChange" OnloadComplete="cityUdodFilter_OnloadComplete"/>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel runat="server" ID="upHide" UpdateMode="Always">
    <ContentTemplate>
        <asp:HiddenField runat="server" ID="hf" Value="false"/>
    </ContentTemplate>
</asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="upFilter" UpdateMode="Conditional">
        <ContentTemplate>
            <table>
                <tr>
                    <td rowspan="2" valign="top">Отображать по: </td>
                    <td rowspan="2">
                        <asp:RadioButtonList ID="rblViewFilter" runat="server">
                            <asp:ListItem Selected="True" Value="0">- по детским объединениям</asp:ListItem>
                            <asp:ListItem Value="1">- педагогам</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td>Детское объединение</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlDOFilter" CssClass="input" DataTextField="Name" DataValueField="ChildUnionId" AutoPostBack="true" OnDataBound="ddlDOFilter_OnDataBound" OnSelectedIndexChanged="ddlDOFilter_OnSelectedIndexChanged"/>
                    </td>
                    <td>
                        Группа
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlGroupFilter" CssClass="input" DataTextField="Name" DataValueField="UdodAgeGroupId" AutoPostBack="true" OnDataBound="ddlGroupFilter_OnDataBound" OnSelectedIndexChanged="ddlGroupFilter_OnSelectedIndexChanged"></asp:DropDownList>
                    </td>
                    
                </tr>
                <tr>
                    <td>Педагог</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlTeacherFilter" CssClass="input" DataTextField="TeacherName" DataValueField="UserId" AutoPostBack="true" OnDataBound="ddlGroupFilter_OnDataBound" OnSelectedIndexChanged="ddlTeacherFilter_OnSelectedIndexChanged"/>
                    </td>
                    <td>Направленность</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlProgramFilter" CssClass="input" DataTextField="Name" DataValueField="ProgramId" AutoPostBack="true" OnDataBound="ddlGroupFilter_OnDataBound" OnSelectedIndexChanged="ddlProgramFilter_OnSelectedIndexChanged"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:CheckBox runat="server" ID="cbTimeRecess" CssClass="input" AutoPostBack="true" Text="Показывать время занятий с учетом перемены" OnCheckedChanged="cbTimeRecess_OnCheckedChanged"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:CheckBox runat="server" ID="cbComment" CssClass="input" AutoPostBack="true" Text="Показывать примечания" OnCheckedChanged="cbTimeRecess_OnCheckedChanged"/>
                    </td>
                </tr>
            </table>
            <asp:LinkButton runat="server" ID="lnkAddSchedule" OnClick="lnkAddSchedule_OnClick"><div class="btnBlue">Добавить пункт</div></asp:LinkButton>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <div style="width: 100%;" >

    <asp:UpdatePanel runat="server" ID="upScheduleList" UpdateMode="Conditional">
        <ContentTemplate>
        
    <asp:GridView runat="server" ID="gvShedule" CssClass="dgcSchedule" OnRowDataBound="gvShedule_OnRowDataBound" AutoGenerateColumns="false" OnDataBinding="gvShedule_OnDataBinding" OnPreRender="gvShedule_OnPreRender" OnDataBound="gvShedule_OnDataBound">
        <EmptyDataTemplate>
                <asp:Label ID="lblEmptyGvPupilList" runat="server" style="font-size:16px">Расписание не найдено</asp:Label>
            </EmptyDataTemplate>
            
        <Columns>
            <asp:TemplateField HeaderText="" ShowHeader="false" >
                <ItemTemplate>
                    <%# Eval("ChildUnionName")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="" ShowHeader="false">
                <ItemTemplate>
                    <%# Eval("GroupName")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Время" >
                <ItemTemplate>
                    <%# GetLessonTime(Container.DataItem, 1)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Занятия" >
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lnkEdit1" OnClick="lnkEdit1_Click"><div style='color: black;'><%# GetLessonFioSubject(Container.DataItem, 1)%></div> </asp:LinkButton>
                    <asp:LinkButton runat="server" ID="lnkDelete1" OnClientClick="javascript:return confirm('Действительно удалить данный пункт?');" OnClick="lnkDelete1_OnClick" Visible="false"><img width="15" height="15" src='../Images/delete_smth.png' border=0 title='Удалить'></asp:LinkButton> 
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Время" >
                <ItemTemplate>
                    <%# GetLessonTime(Container.DataItem, 2)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Занятия" >
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lnkEdit2" OnClick="lnkEdit1_Click"><div style='color: black;'><%# GetLessonFioSubject(Container.DataItem, 2)%></div> </asp:LinkButton> 
                    <asp:LinkButton runat="server" ID="lnkDelete2" OnClientClick="javascript:return confirm('Действительно удалить данный пункт?');" OnClick="lnkDelete1_OnClick" Visible="false"><img width="15" height="15" src='../Images/delete_smth.png' border=0 title='Удалить'></asp:LinkButton> 
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Время" >
                <ItemTemplate>
                    <%# GetLessonTime(Container.DataItem, 3)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Занятия" >
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lnkEdit3" OnClick="lnkEdit1_Click"><div style='color: black;'><%# GetLessonFioSubject(Container.DataItem, 3)%></div> </asp:LinkButton> 
                    <asp:LinkButton runat="server" ID="lnkDelete3"  OnClientClick="javascript:return confirm('Действительно удалить данный пункт?');" OnClick="lnkDelete1_OnClick" Visible="false"><img width="15" height="15" src='../Images/delete_smth.png' border=0 title='Удалить'></asp:LinkButton> 
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Время" >
                <ItemTemplate>
                    <%# GetLessonTime(Container.DataItem, 4)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Занятия" >
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lnkEdit4" OnClick="lnkEdit1_Click"><div style='color: black;'><%# GetLessonFioSubject(Container.DataItem, 4)%></div> </asp:LinkButton> 
                    <asp:LinkButton runat="server" ID="lnkDelete4" OnClientClick="javascript:return confirm('Действительно удалить данный пункт?');" OnClick="lnkDelete1_OnClick" Visible="false"><img width="15" height="15" src='../Images/delete_smth.png' border=0 title='Удалить'></asp:LinkButton> 
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Время" >
                <ItemTemplate>
                    <%# GetLessonTime(Container.DataItem, 5)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Занятия" >
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lnkEdit5" OnClick="lnkEdit1_Click"><div style='color: black;'><%# GetLessonFioSubject(Container.DataItem, 5)%></div> </asp:LinkButton> 
                    <asp:LinkButton runat="server" ID="lnkDelete5" OnClientClick="javascript:return confirm('Действительно удалить данный пункт?');" OnClick="lnkDelete1_OnClick" Visible="false"><img width="15" height="15" src='../Images/delete_smth.png' border=0 title='Удалить'></asp:LinkButton> 
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Время" >
                <ItemTemplate>
                    <%# GetLessonTime(Container.DataItem, 6)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Занятия" >
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lnkEdit6" OnClick="lnkEdit1_Click"><div style='color: black;'><%# GetLessonFioSubject(Container.DataItem, 6)%></div> </asp:LinkButton> 
                    <asp:LinkButton runat="server" ID="lnkDelete6" OnClientClick="javascript:return confirm('Действительно удалить данный пункт?');" OnClick="lnkDelete1_OnClick" Visible="false"><img width="15" height="15" src='../Images/delete_smth.png' border=0 title='Удалить'></asp:LinkButton> 
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Время" >
                <ItemTemplate>
                    <%# GetLessonTime(Container.DataItem, 7)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Занятия" >
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lnkEdit7" OnClick="lnkEdit1_Click"><div style='color: black;'><%# GetLessonFioSubject(Container.DataItem, 7)%></div> </asp:LinkButton> 
                    <asp:LinkButton runat="server" ID="lnkDelete7" OnClientClick="javascript:return confirm('Действительно удалить данный пункт?');" OnClick="lnkDelete1_OnClick" Visible="false"><img width="15" height="15" src='../Images/delete_smth.png' border=0 title='Удалить'></asp:LinkButton> 
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger runat="server" ControlID="lnkAddScheduleOk"/>
    </Triggers>
    </asp:UpdatePanel>
    </div>
    <asp:Panel runat="server" ID="popupAddSchedule" style="padding: 5px; border: 2px solid black" BackColor="White">
    <asp:Panel ID="pAddScheduleHeader" runat="server" >
        <asp:Label ID="Label1" Text="<span class='headerCaptions'>Добавление расписания</span>" runat="server"/>
        <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="$find('AddSchedulePopup').hide(); return false;" />
    </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upAddSchedule" UpdateMode="Conditional">
        <ContentTemplate>
            <table>
            
                <tr>
                    <td>Детское объединение</td>
                    <td>
                        
    
                        <asp:DropDownList runat="server" ID="ddlDO" CssClass="input" DataTextField="Name" DataValueField="ChildUnionId" AutoPostBack="true" OnSelectedIndexChanged="ddlDO_OnSelectedIndexChanged"/>
                        
                    </td>
                </tr>
                <tr>
                    <td>Группа</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlGroup" CssClass="input" DataTextField="Name" DataValueField="UdodAgeGroupId" AutoPostBack="true" OnSelectedIndexChanged="ddlGroup_OnSelectedIndexChanged"/>
                    </td>
                </tr>
                <tr>
                    <td>Педагог</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlTeacher" CssClass="input" DataTextField="TeacherName" DataValueField="UserId"/>
                    </td>
                </tr>
                <tr>
                    <td>Предмет</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlSection" CssClass="input" DataTextField="Name" DataValueField="SectionId"/>
                    </td>
                </tr>
                <tr runat="server" ID="trPupils" Visible="false">
                    <td>
                        Список обучающихся
                    </td>
                    <td>
                        <asp:CheckBoxList runat="server" ID="cblPupils" DataTextField="Fio" DataValueField="UserId"/>
                        <asp:CustomValidator runat="server" ID="CustomValidator2" ValidationGroup="88" EnableClientScript="true" ClientValidationFunction="isValidPupils"  Display="Dynamic" ErrorMessage="Необходимо отметить хотя бы одно значение" ></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td>День недели</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlWeekDay" CssClass="input">
                            <Items>
                                <asp:ListItem runat="server" Value="1">Понедельник</asp:ListItem>
                                <asp:ListItem runat="server" Value="2">Вторник</asp:ListItem>
                                <asp:ListItem runat="server" Value="3">Среда</asp:ListItem>
                                <asp:ListItem runat="server" Value="4">Четверг</asp:ListItem>
                                <asp:ListItem runat="server" Value="5">Пятница</asp:ListItem>
                                <asp:ListItem runat="server" Value="6">Суббота</asp:ListItem>
                                <asp:ListItem runat="server" Value="7">Воскресенье</asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Время начала занятий</td>
                    <td>
                        <asp:TextBox runat="server" ID="tbStartTime" CssClass="input"></asp:TextBox>
                        <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99:99" MaskType="Time" TargetControlID="tbStartTime"/> 
                        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator3" runat="server" ControlExtender="MaskedEditExtender3" ControlToValidate="tbStartTime"
                        IsValidEmpty="False"
                        EmptyValueMessage="Необходимо значение"
                        InvalidValueMessage="Неверное значение"
                        Display="Dynamic"
                        TooltipMessage=""
                        EmptyValueBlurredText="*"
                        InvalidValueBlurredMessage="*"
                        ValidationGroup="88"/>
        
                    </td>
                </tr>
                <tr>
                    <td>Время окончания занятия/начала перемены</td>
                    <td>
                        
                        <asp:TextBox runat="server" ID="tbEndTime" CssClass="input"></asp:TextBox>
                        <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99:99" MaskType="Time" TargetControlID="tbEndTime"/> 
                        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="MaskedEditExtender1" ControlToValidate="tbEndTime"
                        IsValidEmpty="False"
                        EmptyValueMessage="Необходимо значение"
                        InvalidValueMessage="Неверное значение"
                        Display="Dynamic"
                        TooltipMessage=""
                        EmptyValueBlurredText="*"
                        InvalidValueBlurredMessage="*"
                        ValidationGroup="88"/>
        
                    </td>
                </tr>
                <tr>
                    <td>Время окончания перемены</td>
                    <td>
                        <asp:TextBox runat="server" ID="tbEndRecess" CssClass="input"></asp:TextBox>
                        <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99:99" MaskType="Time" TargetControlID="tbEndRecess"/> 
                        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlExtender="MaskedEditExtender2" ControlToValidate="tbEndRecess"
                        IsValidEmpty="False"
                        EmptyValueMessage="Необходимо значение"
                        InvalidValueMessage="Неверное значение"
                        Display="Dynamic"
                        TooltipMessage=""
                        EmptyValueBlurredText="*"
                        InvalidValueBlurredMessage="*"
                        ValidationGroup="88"/>
                    </td>
                </tr>
                <tr>
                    <td>Комментарий</td>
                    <td>
                        <asp:TextBox runat="server" ID="tbComment" CssClass="input"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            
        </Triggers>
        </asp:UpdatePanel>
        <table>
            <tr>
                <td><asp:LinkButton ID="lnkAddScheduleOk" runat="server" OnClick="lnkAddScheduleOk_OnClick" ValidationGroup="88"><div class='btnBlue'>OK</div></asp:LinkButton></td>
                <td><asp:LinkButton ID="lnkAddScheduleCancel" runat="server" CausesValidation="false" ><div class='btnBlue'>Отмена</div></asp:LinkButton></td>
            </tr>
        </table>    
</asp:Panel>
<ajaxToolkit:ModalPopupExtender runat="server" ID="popupAddScheduleExtender" PopupDragHandleControlID="pAddScheduleHeader"
    PopupControlID="popupAddSchedule" CancelControlID="lnkAddScheduleCancel"
    TargetControlID="btnShowAddSchedulePopup" RepositionMode="None"
    BehaviorID="AddSchedulePopup"/>
<asp:Button ID="btnShowAddSchedulePopup" runat="server" style="display:none" />

<asp:UpdatePanel ID="upErrorPopup" runat="server" UpdateMode="Always" >
    <ContentTemplate>
        <uct:ErrorPopup runat="server" ID="ucError" />
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
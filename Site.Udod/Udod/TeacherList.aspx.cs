﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class Udod_TeacherList : BasePage
{
    private ExtendedTeacher SelectedTeacher;
    private Guid userId { get { return (Guid)ViewState["UserId"]; } set { ViewState["UserId"] = value; } }
    private bool isdeleted { get { return (bool)ViewState["isDeleted"]; } set { ViewState["isDeleted"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator|| UserContext.Roles.IsUdodEmployee);

        if (!Page.IsPostBack)
        {
            //TeacherDatabind();

            if (UserContext.UdodId.HasValue)
                lnkAddTeacher.Visible = true;
            else
                lnkAddTeacher.Visible = false;
        }
    }

    protected void lnkAddTeacher_OnClick(object sender, EventArgs e)
    {
        // добавление педагога
        userId = Guid.Empty;
        ucFio.LastName = "";
        ucFio.FirstName = "";
        ucFio.MiddleName = "";
        litTitle.Text = "<span class='headerCaptions'>Добавление педагога</span>";
        upTitle.Update();
        /*using (UdodDb db = new UdodDb())
        {
            var list = db.GetUdodSections(UserContext.UdodId.Value, null, null);

            cblSection.DataSource = list.Sections;
            cblSection.DataBind();
        }*/
        up.Update();
        popupAddExtender.Show();
    }

    protected string GetTeacherSections(object teacher)
    {
        var sections = (teacher as ExtendedTeacher).Sections;
        StringBuilder sb = new StringBuilder();

        foreach (var extendedSection in sections)
        {
            sb.Append(extendedSection.Name + "<br>");
        }
        return sb.ToString();
    }

    protected void gvTeacherList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        userId = (Guid)(gvTeacherList.SelectedDataKey.Value);
        using (TeacherDb db = new TeacherDb())
        {
            SelectedTeacher = db.GetTeacher(null, UserContext.UdodId, null).Where(i => i.UserId == userId).FirstOrDefault();
        }
        ucFio.LastName = SelectedTeacher.LastName;
        ucFio.FirstName = SelectedTeacher.FirstName;
        ucFio.MiddleName = SelectedTeacher.MiddleName;
        /*using (UdodDb db = new UdodDb())
        {
            var list = db.GetUdodSections(UserContext.UdodId.Value, null, null);

            cblSection.DataSource = list.Sections;
            cblSection.DataBind();

            foreach (var item in cblSection.Items)
            {
                (item as ListItem).Selected =
                    SelectedTeacher.Sections.Count(i => i.SectionId == Convert.ToInt32((item as ListItem).Value))>0;
            }
        }*/
        litTitle.Text = "<span class='headerCaptions'>Редактирование педагога</span>";
        upTitle.Update();
        up.Update();
        popupAddExtender.Show();
    }

    protected void btnAddTeacher_OnClick(object sender, EventArgs e)
    {
        
        using (TeacherDb db = new TeacherDb())
        {
            if (userId == Guid.Empty)
            {
                userId= db.CreateTeacher(UserContext.UdodId.Value, ucFio.LastName, ucFio.FirstName, ucFio.MiddleName);
            }
            else
            {
                db.UpdateTeacher(userId, ucFio.LastName, ucFio.FirstName, ucFio.MiddleName);
            }
            if (userId != Guid.Empty)
            {
                foreach (ListItem item in cblSection.Items)
                {
                    db.SetTeacherSection(UserContext.UdodId.Value, Convert.ToInt32(item.Value), userId, item.Selected);
                }
            }
        }
        TeacherDatabind();
        popupAddExtender.Hide();
    }

    protected void TeacherDatabind()
    {
        gvTeacherList.DataBind();
        upTeachers.Update();
    }

    protected void FilterDatabind(object sender, EventArgs e)
    {
        pgrTeacherList.Clear();
        TeacherDatabind();

        if (UserContext.UdodId.HasValue)
            lnkAddTeacher.Visible = true;
        else
            lnkAddTeacher.Visible = false;
    }

    protected void btnCancelMessage_OnClick(object sender, EventArgs e)
    {
        if (!isdeleted)
        {
            using (TeacherDb db = new TeacherDb())
            {
                if (UserContext.UdodId.HasValue) db.DeleteTeacherFromUdod(userId, UserContext.UdodId.Value);
            }
        }

        popupShowMessageExtender.Hide();
        TeacherDatabind();
    }

    protected void gvTeacherList_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType==DataControlRowType.DataRow)
        {
            ExtendedTeacher teacher = (ExtendedTeacher) e.Row.DataItem;
            LinkButton btn = (LinkButton) e.Row.FindControl("lnkDelete");
            btn.Attributes.Add("TeacherId", teacher.UserId.ToString());
        }
    }

    protected void lnkDelete_OnClick(object sender, EventArgs e)
    {
        // Проверка на этого педагога есть ли он в списках ДО
        LinkButton btn = (LinkButton) sender;
        userId= new Guid(btn.Attributes["TeacherId"]);
        isdeleted = true;
        using (TeacherDb db = new TeacherDb())
        {
            if (UserContext.UdodId.HasValue)
            isdeleted = db.IsTeacherInChildUnion(userId, UserContext.UdodId.Value);
        }
        if (isdeleted)
        {
            lblInfo.Text = "Невозможно удалить педагога, т.к он задействован в одном или более детских объединениях. <br/>Перед удалением педагога из УДО необходимо удалить его из этих детских объединений.";
            btnOkMessage.Text = "<div class='btnBlue'>Отмена</div>";
            btnCancelMessage.Visible = false;
        }
        else
        {
            btnOkMessage.Text = "<div class='btnBlue'>Ок</div>";
            lblInfo.Text = "Данный педагог будет удален из УДО. Продолжить?";
            btnCancelMessage.Visible = true;
        }
        popupShowMessageExtender.Show();   
    }

    protected void ds_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        using (TeacherDb db = new TeacherDb())
        {
            pgrTeacherList.numElements = db.GetTeacherCount(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod);
        }
    }

    protected void pgrTeacherList_OnChange(object sender, EventArgs e)
    {
        TeacherDatabind();
    }
}
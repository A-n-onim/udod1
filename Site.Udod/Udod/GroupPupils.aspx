﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="GroupPupils.aspx.cs" Inherits="Udod_GroupPupils" %>
<%@ Register src="../UserControl/Menu/ucPager.ascx" tagName="Pager" tagPrefix="uc" %>
<%@ Register src="../UserControl/Menu/ucErrorPopup.ascx" tagName="ErrorPopup" tagPrefix="uct" %>
<%@ Register TagPrefix="uct" TagName="CityUdodFilter" Src="~/UserControl/Filter/ucCityUdodFilter.ascx" %>

<asp:Content runat="server" ID="content" ContentPlaceHolderID="body">
    <asp:UpdatePanel ID="upCityUdodFilter" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <uct:CityUdodFilter ID="cityUdodFilter" runat="server" UdodDdlEnabled="true" AddAllProperty="true" OnCityChange="cityUdodFilter_OnUdodChange" OnUdodChange="cityUdodFilter_OnUdodChange" OnloadComplete="cityUdodFilter_onLoadComplete"/>
    </ContentTemplate>
</asp:UpdatePanel>
    <asp:UpdatePanel ID="upFilters" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkFindByName" GroupingText="<b><div style='font-size:14pt'>Показать обучающихся, зачисленных ...:</h3></div></b>">      
        <table>
            <tr>
                <td>
                    Детское объединение:
                </td>
                <td>
                    <asp:DropDownList CssClass="input" runat="server" ID="ddlDO" DataTextField="Name" DataValueField="ChildUnionId" OnDataBound="ddlDO_OnDataBound" AutoPostBack="true" OnSelectedIndexChanged="ddlDO_OnSelectedIndexChanged"/>
                </td>
                <td>
                    Год обучения:
                </td>
                <td>
                    <asp:DropDownList CssClass="input" runat="server" ID="ddlPupilNumYear" AutoPostBack="true" OnSelectedIndexChanged="ddlPupilNumYear_OnSelectedIndexChanged" >
                        <asp:ListItem Text="Все" Value="" Selected="True" />
                        <asp:ListItem Text="1" Value="1" />
                        <asp:ListItem Text="2" Value="2" />
                        <asp:ListItem Text="3" Value="3" />
                        <asp:ListItem Text="4" Value="4" />
                        <asp:ListItem Text="5" Value="5" />
                        <asp:ListItem Text="6" Value="6" />
                        <asp:ListItem Text="7" Value="7" />
                        <asp:ListItem Text="8" Value="8" />
                        <asp:ListItem Text="9" Value="9" />
                        <asp:ListItem Text="10" Value="10" />
                        <asp:ListItem Text="11" Value="11" />
                        <asp:ListItem Text="12" Value="12" />
                        <asp:ListItem Text="13" Value="13" />
                        <asp:ListItem Text="14" Value="14" />
                        <asp:ListItem Text="15" Value="15" />
                    </asp:DropDownList>
                </td>
                <td>Группа:</td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlGroupsFilter" CssClass="input" AutoPostBack="true" DataTextField="Name" DataValueField="UdodAgeGroupId" OnSelectedIndexChanged="ddlPupilNumYear_OnSelectedIndexChanged" OnDataBound="ddlGroupsFilter_OnDataBound">
                        <Items>
                            <asp:ListItem Value="">Выберите ДО</asp:ListItem>
                        </Items>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    ФИО педагога:
                </td>
                <td>
                    <asp:DropDownList CssClass="input" runat="server" ID="ddlTeacher" DataTextField="Fio" DataValueField="UserId" OnDataBound="ddlTeacher_OnDataBound" AutoPostBack="true" OnSelectedIndexChanged="ddlTeacher_OnSelectedIndexChanged"/>
                </td>
                <td>
                    Вид услуги:
                </td>
                <td>
                    <asp:DropDownList CssClass="input" runat="server" ID="ddlBudgetType" DataSourceID="dsBudgetType" DataTextField="TypeBudgetName" DataValueField="DictTypeBudgetId" AutoPostBack="true" OnSelectedIndexChanged="ddlBudgetType_OnSelectedIndexChanged" OnDataBound="ddlBudgetType_OnDataBound" />
                </td>
                <td>
                </td>
            </tr>
        </table>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server" DefaultButton="lnkFindByName" GroupingText="<b><div style='font-size:14pt'>Поиск обучающегося</h3></div></b>">      
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblPupilLastName" runat="server" Text="Фамилия:" />
                </td>
                <td>
                    <asp:Label ID="lblPupilFirstName" runat="server" Text="Имя:" />
                </td>
                <td>
                    <asp:Label ID="lblPupilMiddleName" runat="server" Text="Отчество:" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbPupilLastName" runat="server" CssClass="input" />
                </td>
                <td>
                    <asp:TextBox ID="tbPupilFirstName" runat="server" CssClass="input" />
                </td>
                <td>
                    <asp:TextBox ID="tbPupilMiddleName" runat="server" CssClass="input" />
                </td>
                <td>
                    <asp:LinkButton ID="lnkFindByName" runat="server" OnClick="lnkFindByName_OnClick"><div class="btnBlue">Поиск</div></asp:LinkButton>
                </td>
            </tr>
            
        </table>
        </asp:Panel>
        <table>
            <tr>
                <td style="text-align: right;">
                    <asp:LinkButton runat="server" ID="lnkClearSelect" OnClick="lnkClearSelect_OnClick"><div class="btnBlue">Сбросить выделение</div></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel runat="server" ID="upPupilList" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:GridView runat="server" ID="gvPupilList" DataKeyNames="AgeGroupPupilId" CssClass="dgc" AutoGenerateColumns="false" 
                        DataSourceID="dsPupilList" OnRowDataBound="gvPupilList_OnRowDataBound" >
            <EmptyDataTemplate>
                <asp:Label ID="lblEmptyGvPupilList" runat="server" style="font-size:16px">Ученики не найдены</asp:Label>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="№" >
                    <ItemTemplate><%# (gvPupilList.Rows.Count + pgrPupilList.PagerIndex)%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ФИО обучающегося" >
                    <ItemTemplate>
                        <%# Eval("Fio")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Дата рождения" >
                    <ItemTemplate>
                        <%# ((DateTime)Eval("Birthday")).ToShortDateString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Детское объединение" >
                    <ItemTemplate>
                        <%# Eval("Group.ChildUnionName")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Год обучения" >
                    <ItemTemplate>
                        <%# Eval("ChildUnions[0].PupilNumYear")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Вид услуги" >
                    <ItemTemplate>
                        <%# (Eval("ChildUnions[0].PupilBudgetType.DictTypeBudgetId")).ToString()=="2"?"Бесплатно":"Платно"%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Группа" >
                    <ItemTemplate>
                        <%# Eval("Group.Name")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Выделение" >
                    <ItemTemplate>
                        <asp:CheckBox runat="server" ID="cbxSelect" OnCheckedChanged="cbxSelect_OnCheckedChanged" AutoPostBack="true"/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="gridpager" VerticalAlign="Bottom" HorizontalAlign="Left"/>
            <PagerSettings Mode="Numeric" Position="Bottom" PageButtonCount="10" />
        </asp:GridView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddlDO" />
        <asp:AsyncPostBackTrigger ControlID="ddlTeacher" />
    </Triggers>
</asp:UpdatePanel>

<uc:Pager runat="server" ID="pgrPupilList" needStartDots="false" needEndDots="false" pagesCount="10" OnChange="pgrPupilList_OnChange"/>

<asp:UpdatePanel runat="server" ID="upChildUnionGroup" UpdateMode="Conditional">
    <ContentTemplate>
    <asp:Panel ID="Panel3" runat="server" GroupingText="<b><div style='font-size:14pt'>параметры перевода в группу</h3></div></b>">      
        <table>
            <tr>
                <td>
                    Детское объединение:
                </td>
                <td>
                    <asp:DropDownList CssClass="input" runat="server" ID="ddlDOZach" DataTextField="Name" DataValueField="ChildUnionId" OnDataBound="ddlDOZach_OnDataBound" AutoPostBack="true" OnSelectedIndexChanged="ddlDOZach_OnSelectedIndexChanged"/>
                </td>
                <td>Выберите группу</td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlGroupsForTransfer" Visible="false" CssClass="input" DataTextField="Name" DataValueField="UdodAgeGroupId" AutoPostBack="true" OnSelectedIndexChanged="ddlGroupsForTransfer_OnSelectedIndexChanged" OnDataBound="ddlGroupsForTransfer_OnDataBound"/>
                </td>
                <td>
                    <asp:LinkButton runat="server" ID="lnkTransfer" OnClick="lnkTransfer_OnClick" Visible="false"><div class="btnBlue">Перевести</div></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Panel runat="server" ID="pGroupinfo" GroupingText="информация о группе">
                    <asp:Label runat="server" ID="lblGroupInfo"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:ObjectDataSource runat="server" ID="dsBudgetType" TypeName="Udod.Dal.DictBudgetTypeDb" SelectMethod="GetBudgetTypes" />

<asp:ObjectDataSource runat="server" ID="dsPupilList" TypeName="Udod.Dal.PupilsDb" SelectMethod="GetGroupPupilsPaging" OnSelecting="ds_Selecting">
    <SelectParameters>
        <asp:ControlParameter Name="cityId" ControlID="cityUdodFilter" PropertyName="SelectedCity" ConvertEmptyStringToNull="true" />
        <asp:ControlParameter Name="udodId" ControlID="cityUdodFilter" PropertyName="SelectedUdod" ConvertEmptyStringToNull="true" />
        <asp:ControlParameter Name="childUnionId" ControlID="ddlDO" PropertyName="SelectedValue" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="teacherId" ControlID="ddlTeacher" PropertyName="SelectedValue" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pupilLastName" ControlID="tbPupilLastName" PropertyName="Text" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pupilFirstName" ControlID="tbPupilFirstName" PropertyName="Text" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pupilMiddleName" ControlID="tbPupilMiddleName" PropertyName="Text" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pagerIndex" ControlID="pgrPupilList" ConvertEmptyStringToNull="true" PropertyName="PagerIndex" />
        <asp:ControlParameter Name="pagerLength" ControlID="pgrPupilList" ConvertEmptyStringToNull="true" PropertyName="PagerLength" />
        <asp:ControlParameter Name="pupilNumYear" ControlID="ddlPupilNumYear" ConvertEmptyStringToNull="true" PropertyName="SelectedValue" />
        <asp:ControlParameter Name="budgetTypeId" ControlID="ddlBudgetType" ConvertEmptyStringToNull="true" PropertyName="SelectedValue" />
        <asp:ControlParameter Name="groupId" ControlID="ddlGroupsFilter" ConvertEmptyStringToNull="true" PropertyName="SelectedValue" />
    </SelectParameters>
</asp:ObjectDataSource>
<uct:ErrorPopup ID="erropPopup" runat="server"/> 
</asp:Content>
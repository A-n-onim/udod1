﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Udod_UdodList : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsDepartment || UserContext.Roles.IsOsip);

        if (!Page.IsPostBack)
        {
            if (UserContext.Roles.RoleId == (int)EnumRoles.Osip || UserContext.Roles.RoleId==(int)EnumRoles.Department)
            {
                gvUdodList.Columns[gvUdodList.Columns.Count - 2].Visible =
                    gvUdodList.Columns[gvUdodList.Columns.Count - 3].Visible =
                    AddUdod.Visible = false;
                gvUdodList.Columns[gvUdodList.Columns.Count - 4].HeaderText = "Просмотр";
            }

            bool? isOsip = null;
            if (UserContext.Roles.IsOsip || UserContext.Roles.IsDepartment) isOsip = true;
            datasource.SelectParameters["IsOsip"].DefaultValue = isOsip.ToString();

            if (UserContext.Roles.RoleId == (int)EnumRoles.Administrator)
            {
                gvUdodList.Columns[6].Visible = false;
                gvUdodList.Columns[10].Visible = true;
                lblStatusUdod.Visible = true;
                ddlStautsUdod.Visible = true;
            }

            //AddUdod.Visible = !(UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee);
            ReorganisePanel.Visible = (UserContext.Roles.RoleId == (int)EnumRoles.Administrator);

            if (UserContext.CityId.HasValue)
            {
                cityUdodFilter.SelectedCity = UserContext.CityId.Value;
            }
        }
    }

    protected string GetAddresses(object udod)
    {
        var addrs = (udod as ExtendedUdod).Addresses;

        var tmp = addrs.Select(i => i.AddressStr).Distinct().ToList();

        StringBuilder sb = new StringBuilder();

        foreach (var addr in tmp)
        {
            sb.Append(addr + "<br>");
        }
        if (sb.ToString() != "")
            return sb.ToString();
        else
            return "Не задано";
    }

    protected void AddUdod_Click(object sender, EventArgs e)
    {
        UserContext.UdodId = null;
        Response.Redirect("~/Udod/UdodEdit.aspx", true);
    }

    protected void gvUdodList_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }

    protected void gvEdit_OnClick(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        UserContext.UdodId = Convert.ToInt64(btn.Attributes["UdodId"]);
        using (UdodDb db = new UdodDb())
        {
            UserContext.CityId = db.GetUdod(UserContext.UdodId.Value).CityId;
        }
        //UserContext.CityId = Convert.ToInt32(btn.Attributes["CityId"]);
        Response.Redirect("~/Udod/UdodEdit.aspx", true);
    }

    protected void gvUdodList_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType==DataControlRowType.DataRow)
        {
            LinkButton btn = (LinkButton)e.Row.FindControl("gvEdit");
            ExtendedUdod data = (ExtendedUdod)e.Row.DataItem;
            btn.Attributes.Add("UdodId", data.UdodId.ToString());
            if (UserContext.Roles.RoleId == (int)EnumRoles.Osip || UserContext.Roles.IsDepartment)
            {
                btn.Text = "<div class='btnBlue'>Просмотр</div>";
            }

            DropDownList ddl = (DropDownList)e.Row.FindControl("ddlUdodStatus");
            ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(data.UdodStatusId.ToString()));
            ddl.Attributes.Add("UdodId", data.UdodId.ToString());
        }
    }

    protected void gvUdodList_UdodDeleted(object sender, GridViewDeletedEventArgs e)
    {
        long id = Convert.ToInt64(e.Keys["UdodId"]);
        EventsUtility.LogUdodDelete(UserContext.Profile.UserId, Request.UserHostAddress,
            String.Format("(УДО удален: {0})", id));
    }

    protected void ds_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        bool? isOsip = null;
        if (UserContext.Roles.IsOsip || UserContext.Roles.IsDepartment) isOsip = true;
        datasource.SelectParameters["IsOsip"].DefaultValue = isOsip.ToString();
    }

    protected void ds_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        using (UdodDb db = new UdodDb())
        {
            int? udodStatusId;
            try
            {
                udodStatusId = Convert.ToInt32(ddlStautsUdod.SelectedValue);
            }
            catch
            {
                udodStatusId = null;
            }
            bool? isContingent;
            try
            {
                isContingent = Convert.ToBoolean(ddlStatus.SelectedValue);
            }
            catch
            {
                isContingent = null;
            }
            bool? isOsip = null;
            if (UserContext.Roles.IsOsip || UserContext.Roles.IsDepartment) isOsip = true;
            pgrUdodList.numElements = db.GetUdodsCount(cityUdodFilter.SelectedCity, null, null, null, null, null, null, tbUdodName.Text, null, null, null, null, null, null, udodStatusId, isContingent, isOsip, tbNumber.Text);
        }
    }

    protected void pgrUdodList_OnChange(object sender, EventArgs e)
    {
        UdodDataBind();
    }

    protected void UdodDataBind()
    {
        gvUdodList.DataBind();
        upGridView.Update();
    }

    protected void cityUdodFilter_cityChange(object sender, EventArgs e)
    {
        lnkExportContingent.Visible= lnkExportProgram.Visible = (cityUdodFilter.SelectedCity == UserContext.Profile.CityId)|| (UserContext.Roles.IsAdministrator);
        pgrUdodList.Clear();
        UdodDataBind();
    }

    protected void ddlUdodStatus_OnChange(object sender, EventArgs e)
    {
        DropDownList ddl = (DropDownList)sender;
        long id = Convert.ToInt64(ddl.Attributes["UdodId"]);
        using (UdodDb db = new UdodDb())
        {
            db.UpdateUdodStatus(id, Convert.ToInt32(ddl.SelectedValue));

            EventsUtility.LogUdodStatusUpdate(UserContext.Profile.UserId, Request.UserHostAddress,
            String.Format("(Статус изменен на '{1}'. Идентификатор УДО : {0})", id, ddl.SelectedItem.Text));
        }
    }

    protected void ddlStatusUdod_OnDataBound(object sender, EventArgs e)
    {
        ddlStautsUdod.Items.Insert(0, new ListItem("Все", ""));
    }

    protected void ddlStatusUdod_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        pgrUdodList.Clear();
    }

    protected void gvSetContingent_OnClick(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        UserContext.UdodId = Convert.ToInt64(btn.Attributes["UdodId"]);
        using (UdodDb db = new UdodDb())
        {
            db.SetContingentEnabled(UserContext.UdodId.Value,true);
        }
        gvUdodList.DataBind();
        upGridView.Update();
    }

    protected void ddlStatus_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        UdodDataBind();
    }

    protected void lnkFindByName_OnClick(object sender, EventArgs e)
    {
        UdodDataBind();
    }

    protected void lnkExportContingent_OnClick(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("ContingentReport.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                                string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];
        #region Стили
        Aspose.Cells.Style style_area = workbook.Styles[0];
        Aspose.Cells.Style style_header = workbook.Styles[1];
        style_area.ShrinkToFit = true;

        style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
        style_header.Font.IsBold = true;
        style_header.Font.Size = 11;
        style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
        style_header.BackgroundColor = System.Drawing.Color.Gray;
        style_header.Pattern = Aspose.Cells.BackgroundType.Solid;
        #endregion

        using (UdodDb db = new UdodDb())
        {

            var list = db.GetUdods(cityUdodFilter.SelectedCity, null, null, null, null, null, null, null, null, null,
                                   null, null, null, null, null);
            sheet.Cells[0, 0].PutValue("№ п/п");
            sheet.Cells[0, 1].PutValue("Краткое наименование ОУ");
            sheet.Cells[0, 2].PutValue("№ ОУ");
            sheet.Cells[0, 3].PutValue("Статус ввода контингента");
            
            int i = 1;
            list = list.Where(k => k.UdodTypeId == 1).ToList();
            foreach (var udod in list)
            {
                sheet.Cells[i, 0].PutValue(i);
                sheet.Cells[i, 1].PutValue(udod.ShortName);
                sheet.Cells[i, 2].PutValue(udod.UdodNumber);
                sheet.Cells[i, 3].PutValue(udod.IsContingentEnabled?"Ввод продолжается":"Ввод завершен");
                i++;
            }

            
            sheet.Cells.CreateRange(1, 0, i, 4).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
            sheet.Cells.CreateRange(0, 0, 1, 4).ApplyStyle(style_header, new Aspose.Cells.StyleFlag() { All = true });
            sheet.AutoFitColumns();

        }
        workbook.Save("ContingentReport.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }

    protected void lnkExportProgram_OnClick(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("ProgramReport.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                                string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];
        #region Стили
        Aspose.Cells.Style style_area = workbook.Styles[0];
        Aspose.Cells.Style style_header = workbook.Styles[1];
        style_area.ShrinkToFit = true;

        style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
        style_header.Font.IsBold = true;
        style_header.Font.Size = 11;
        style_header.IsTextWrapped = true;
        style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
        style_header.BackgroundColor = System.Drawing.Color.Gray;
        style_header.Pattern = Aspose.Cells.BackgroundType.Solid;
        #endregion

        var programs = (new DictProgramDb()).GetPrograms(null);
        using (ReportDb db = new ReportDb())
        {
                
            var list = db.GetReportUdodProgram(cityUdodFilter.SelectedCity);
            sheet.Cells[0, 0].PutValue("№ п/п");
            sheet.Cells[0, 1].PutValue("Краткое наименование ОУ");
            sheet.Cells[0, 2].PutValue("№ ОУ");
            
            
            int l = 3;
            if (UserContext.Roles.IsAdministrator)
            {
                sheet.Cells[0, 4].PutValue("Кол-во ДО по направленностям");
                sheet.Cells[0, 3].PutValue("Округ");
                l = 4;
            }
            else
            {
                sheet.Cells[0, 3].PutValue("Кол-во ДО по направленностям");
            }
            sheet.Cells[0, l].PutValue("Есть блок ДО");
            l++;
            sheet.Cells[0, l].PutValue("Есть в госзадании");
            l++;
            foreach (var program in programs)
            {
                sheet.Cells[1, l].PutValue(program.Name);
                l++;
            }
            sheet.Cells[1, l].PutValue("Итого");
            sheet.Cells.CreateRange(0, 0, 2, 1).Merge();
            sheet.Cells.CreateRange(0, 1, 2, 1).Merge();
            sheet.Cells.CreateRange(0, 2, 2, 1).Merge();
            sheet.Cells.CreateRange(0, 3, 2, 1).Merge();
            sheet.Cells.CreateRange(0, 4, 2, 1).Merge();
            if (UserContext.Roles.IsAdministrator)
            {
                sheet.Cells.CreateRange(0, 6, 1, l - 3).Merge();
                sheet.Cells.CreateRange(0, 5, 2, 1).Merge();
            }
            else
                sheet.Cells.CreateRange(0, 5, 1, l-3).Merge();
            int i = 2;
            Int64 PreviusUdod = -1;

            var UdodList = list.Select(k => k.UdodId).Distinct().ToList();

            foreach (var item in UdodList)
            {
                sheet.Cells[i, 0].PutValue(i-1);
                var udod = list.Where(k => k.UdodId == item).FirstOrDefault();
                sheet.Cells[i, 1].PutValue(udod.UdodName);
                sheet.Cells[i, 2].PutValue(udod.UdodNumber);
                
                int x = 3;
                if (UserContext.Roles.IsAdministrator)
                {
                    sheet.Cells[i, 3].PutValue(udod.CityName);
                    x = 4;
                }
                sheet.Cells[i, 4].PutValue(udod.isDO?"Да":"Нет");
                sheet.Cells[i, 5].PutValue(udod.isGos ? "Да" : "Нет");
                x = 6;
                int sum = 0;
                foreach (var extendedProgram in programs)
                {
                    var program = list.Where(k => k.UdodId == item && k.ProgramId==extendedProgram.ProgramId).FirstOrDefault();
                    if (program.UdodId!=0)
                    {
                        sheet.Cells[i, x].PutValue(program.CountChildUnion);
                        sum = sum+program.CountChildUnion;
                    }
                    x++;
                }
                sheet.Cells[i, x].PutValue(sum);
                i++;
            }
            sheet.AutoFitColumns();
            if (UserContext.Roles.IsAdministrator)
            {
                sheet.Cells.CreateRange(1, 0, i, 16).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
                sheet.Cells.CreateRange(0, 0, 2, 16).ApplyStyle(style_header, new Aspose.Cells.StyleFlag() { All = true });
            }
            else
            {
                sheet.Cells.CreateRange(1, 0, i, 15).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
                sheet.Cells.CreateRange(0, 0, 2, 15).ApplyStyle(style_header, new Aspose.Cells.StyleFlag() { All = true });
            }
            sheet.AutoFitRow(1);

        }
        workbook.Save("ProgramReport.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }
}
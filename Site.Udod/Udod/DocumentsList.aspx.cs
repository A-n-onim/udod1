﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Udod_DocumentsList : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        pnlAddDoc.Visible = (UserContext.Roles.RoleId == (int)EnumRoles.Administrator);
    }

    protected void lnkAddDocument_OnClick(object sender, EventArgs e)
    {
        if ((fileUpload.HasFile) && (!string.IsNullOrEmpty(tbDocumentName.Text)))
        {
            string filename = SiteUtility.Transliteration.Front(fileUpload.FileName).Replace(" ", "_");
            fileUpload.SaveAs(Page.MapPath(SiteUtility.GetUrl(@"~\UploadedFiles")) + @"\" + filename);
            
            using (DocumentsDb db = new DocumentsDb())
            {
                db.AddDocument(tbDocumentName.Text, filename);
            }
        }
        tbDocumentName.Text = "";

        rptDocuments.DataBind();
        upRep.Update();
    }

    protected void rptDocuments_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
        {
            ExtendedDocument item = (ExtendedDocument)e.Item.DataItem;

            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkDeleteDocument");
            lnk.Attributes.Add("DocumentId", item.DocumentId.ToString());
            lnk.Visible = (UserContext.Roles.RoleId == (int)EnumRoles.Administrator);
        }
    }

    protected void lnkDeleteDocument_OnClick(object sender, EventArgs e)
    {
        LinkButton lnk = sender as LinkButton;
        long id = Convert.ToInt64(lnk.Attributes["DocumentId"]);
        using (DocumentsDb db = new DocumentsDb())
        {
            db.DeleteDocument(id);
        }
        rptDocuments.DataBind();
        upRep.Update();
    }
}
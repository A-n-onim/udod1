﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ImportedPupilList.aspx.cs" MasterPageFile="../Master/MasterPage.master" Inherits="Udod_ImportedPupilList" %>
<%@ Register src="../UserControl/Menu/ucPager.ascx" tagName="Pager" tagPrefix="uc" %>
<%@ Register TagPrefix="uct" TagName="CityUdodFilter" Src="~/UserControl/Filter/ucCityUdodFilter.ascx" %>
<%@ Register TagPrefix="uct" TagName="Udod" Src="~/UserControl/Udod/ucFindUdod.ascx" %>
<%@ Register TagPrefix="uct" TagName="Fio" Src="~/UserControl/User/ucFIO_vertical.ascx" %>
<%@ Register TagPrefix="uct" TagName="Document" Src="~/UserControl/User/ucDocument_Vertical.ascx" %>
<%@ Register TagPrefix="uct" TagName="Address" Src="~/UserControl/Address/ucNewAddress.ascx" %>
<asp:Content runat="server" ID="content" ContentPlaceHolderID="body">
<script type="text/javascript">
    
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(bindtextbox);
    function bindtextbox(sender, args) {
        $('#cbxSelectAll').unbind("change");
        $('#cbxSelectAll').bind("change", GvPupilListChecked);
        
        
    };
    function GvPupilListChecked() {
        var gvPupilList = '#' + '<%= gvPupilList.ClientID %>';
        if ($('#cbxSelectAll').attr("checked")) {
            $(gvPupilList).find("input[type=checkbox]").each(function() {
                if ($(this).attr('disabled') == 'disabled') {
                    $(this).removeAttr("checked");
                }
                else {
                    $(this).attr("checked", "true");
                }
            });
        }
        else {
            $(gvPupilList).find("input[type=checkbox]").each(function () {
                    $(this).removeAttr("checked");
            });
        }
    }
</script>
<asp:UpdatePanel ID="upCityUdodFilter" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <uct:CityUdodFilter ID="cityUdodFilter" runat="server" UdodDdlEnabled="true" OnCityChange="cityUdodFilter_OnCityChange" OnUdodChange="cityUdodFilter_OnUdodChange" OnloadComplete="cityUdodFilter_OnloadComplete"/>
        
        <table>
            <tr>
                <td>
                    <asp:Panel runat="server" GroupingText="<b><div style='font-size:14pt'>Параметры для зачисления</div></b>" >
                    <table>
                    <tr>
                        <td>
                            Педагог:
                        </td>
                        <td>
                            <asp:DropDownList CssClass="input" runat="server" ID="ddlTeacher" DataSourceID="dsTeacher" DataTextField="Fio" DataValueField="UserId" OnDataBound="ddlTeacher_OnDataBound" AutoPostBack="true" OnSelectedIndexChanged="ddlTeacher_OnSelectedIndexChanged"/>
                        </td>
                        <td>
                            Детское объединение:
                        </td>
                        <td>
                            <asp:DropDownList CssClass="input" runat="server" ID="ddlDO1" DataSourceID="dsDO" DataTextField="Name" DataValueField="ChildUnionId" OnDataBound="ddlDO1_OnDataBound" AutoPostBack="true" OnSelectedIndexChanged="ddlDO1_OnSelectedIndexChanged"/>
                        </td>
                        <td>
                            Вид услуги
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlTypeBudgetFilter" AutoPostBack="true" DataTextField="TypeBudgetName" DataValueField="DictTypeBudgetId"/>
                        </td>
                        <td>Год обучения</td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlNumYearFilter" AutoPostBack="true" OnDataBound="ddlNumYearFilter_OnDataBound">
                            </asp:DropDownList>
                        </td>
                        <td style="display: none;"><asp:LinkButton ID="LinkButton2" runat="server" OnClick="lnkFindByName_OnClick"><div class="btnBlue">Поиск</div></asp:LinkButton></td>
                    </tr>
                </table>
                </asp:Panel>
                </td>
                
            </tr>
            <tr>
                <td>
                    <asp:Panel runat="server" ID="pChilUnionInfo" GroupingText="<b><div style='font-size:14pt'>Краткая информация о выбранном ДО </div></b>" >
                        <asp:Label runat="server" ID="lblChildUnionInfo"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel ID="upLnk" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:LinkButton runat="server" ID="lnkImportPupils" OnClick="lnkImportPupils_OnClick" Visible="false"><div class="btnBlue">Импорт</div></asp:LinkButton>
        <asp:LinkButton runat="server" ID="lnkAddPupil" OnClick="lnkAddPupil_OnClick" Visible="true"><div class="btnBlueLong">Добавить обучающегося</div></asp:LinkButton>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel ID="upFilters" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel ID="Panel2" runat="server" DefaultButton="lnkFindByName" Visible="false" GroupingText="<b><div style='font-size:14pt'>Поиск обучающегося</h3></div></b>">      
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblPupilLastName" runat="server" Text="Фамилия:" />
                </td>
                <td>
                    <asp:Label ID="lblPupilFirstName" runat="server" Text="Имя:" />
                </td>
                <td>
                    <asp:Label ID="lblPupilMiddleName" runat="server" Text="Отчество:" />
                </td>
                <td>
                    <asp:Label ID="lblDO" runat="server" Text="Детское объединение" />
                </td>
                <td>
                    <asp:Label ID="lblNumYears" runat="server" Text="Год обучения" />
                </td>
                <td>
                    Показать удаленных и зачисленных
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="tbPupilLastName" runat="server" CssClass="input" />
                </td>
                <td>
                    <asp:TextBox ID="tbPupilFirstName" runat="server" CssClass="input" />
                </td>
                <td>
                    <asp:TextBox ID="tbPupilMiddleName" runat="server" CssClass="input" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlDO" OnDataBound="ddlDO_OnDataBound" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlNumYear" OnDataBound="ddlNumYear_OnDataBound" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="cbxShowDeleted" />
                </td>
                <td style="display: none;">
                    <asp:LinkButton ID="lnkFindByName" runat="server" OnClick="lnkFindByName_OnClick"><div class="btnBlue">Поиск</div></asp:LinkButton>
                </td>
                
            </tr>
        </table>
        </asp:Panel>

        <asp:LinkButton runat="server" ID="lnkSelectDO" OnClick="lnkSelectDO_OnClick" Visible="false"><div class="btnBlue">Выбрать ДО</div></asp:LinkButton>
    </ContentTemplate>
</asp:UpdatePanel>
    
<asp:UpdatePanel runat="server" ID="upPupilList" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Label runat="server" ID="lblInfo"></asp:Label>
        <asp:GridView runat="server" ID="gvPupilList" DataKeyNames="UserId" CssClass="dgc" AutoGenerateColumns="false" 
                        DataSourceID="dsPupilList" OnSelectedIndexChanged="gvPupilList_OnSelectedIndexChanged" OnRowDataBound="gvPupilList_OnRowDataBound">
            <EmptyDataTemplate>
                <asp:Label ID="lblEmptyGvPupilList" runat="server" style="font-size:16px">Ученики не найдены</asp:Label>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="№" >
                    <ItemTemplate><%# (gvPupilList.Rows.Count + pgrPupilList.PagerIndex)%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ФИО обучающегося" >
                    <ItemTemplate>
                        <%# Eval("Fio")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Дата рождения" >
                    <ItemTemplate>
                        <%# ((DateTime)Eval("Birthday")).ToShortDateString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:TemplateField HeaderText="Тип документа" >
                    <ItemTemplate>
                        <%# Eval("Passport.PassportType.PassportTypeId").ToString() == "2" ? "Паспорт РФ" : Eval("Passport.PassportType.PassportTypeId").ToString() == "3"?"Свидетельство о рождении РФ":""%>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="Серия документа" >
                    <ItemTemplate>
                        <%# Eval("Passport.Series")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Номер документа" >
                    <ItemTemplate>
                        <%# Eval("Passport.Number")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Дата выдачи документа" >
                    <ItemTemplate>
                        <%# ((DateTime)Eval("Passport.IssueDate")).ToShortDateString()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Наименование ДО" >
                    <ItemTemplate>
                        <%# Eval("ChildUnions[0].Name")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Год обучения" >
                    <ItemTemplate>
                        <%# Eval("NumYear")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Вид услуги" >
                    <ItemTemplate>
                        <%# Eval("TypeBudget.DictTypeBudgetId").ToString() == "2" ? "Бесплатно" : "Платно"%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ошибки" >
                    <ItemTemplate>
                        <%# Eval("ErrorMessage")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Зачисление/Удаление" Visible="false">
                    <ItemTemplate>
                        <%# Eval("AddedChildUnionName")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="true" SelectText="<div class='BtnImgEdit' title='Редактирование'></div>"/>
                <asp:TemplateField HeaderText="Удаление" >
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="lnkDeletePupil" OnClick="lnkDeletePupil_OnClick"><div class='BtnImgDelete' title="Удаление"></div></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="gridpager" VerticalAlign="Bottom" HorizontalAlign="Left"/>
            <PagerSettings Mode="Numeric" Position="Bottom" PageButtonCount="10" />
        </asp:GridView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger runat="server" ControlID="btnSavePupil"/>
        <asp:AsyncPostBackTrigger runat="server" ControlID="lnkZach"/>
        <asp:AsyncPostBackTrigger runat="server" ControlID="ddlNumYearFilter"/>
        <asp:AsyncPostBackTrigger runat="server" ControlID="ddlTypeBudgetFilter"/>
    </Triggers>
</asp:UpdatePanel>

<uc:Pager runat="server" ID="pgrPupilList" needStartDots="false" needEndDots="false" pagesCount="10" OnChange="pgrPupilList_OnChange"/>
<asp:UpdatePanel runat="server" ID="upLnkZach" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:LinkButton runat="server" ID="lnkZach" OnClick="lnkZach_OnClick"><div class="btnBlueLong">Зачислить</div> </asp:LinkButton>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:ObjectDataSource runat="server" ID="dsPupilList" TypeName="Udod.Dal.PupilsDb" SelectMethod="GetImportedPupilsPaging" SelectCountMethod="GetImportedPupilsCount" OnSelecting="dsPupilList_OnSelecting" >
    <SelectParameters>
        <asp:ControlParameter Name="udodId" ControlID="cityUdodFilter" PropertyName="SelectedUdod" ConvertEmptyStringToNull="true" />
        <asp:ControlParameter Name="pupilLastName" ControlID="tbPupilLastName" PropertyName="Text" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pupilFirstName" ControlID="tbPupilFirstName" PropertyName="Text" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pupilMiddleName" ControlID="tbPupilMiddleName" PropertyName="Text" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="childUnionName" ControlID="ddlDO" PropertyName="Text" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="numYear" ControlID="ddlNumYearFilter" PropertyName="SelectedValue" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="childUnionId" ControlID="ddlDO1" PropertyName="SelectedValue" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="dictTypeBudgetId" ControlID="ddlTypeBudgetFilter" PropertyName="SelectedValue" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="showDeleted" ControlID="cbxShowDeleted" PropertyName="Checked" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="pagerIndex" ControlID="pgrPupilList" ConvertEmptyStringToNull="true" PropertyName="PagerIndex" />
        <asp:ControlParameter Name="pagerLength" ControlID="pgrPupilList" ConvertEmptyStringToNull="true" PropertyName="PagerLength" />
    </SelectParameters>
</asp:ObjectDataSource>


<asp:Panel runat="server" ID="popupLoadFile" style="padding: 5px; display: none; border: 2px solid black" BackColor="White">
    <asp:Panel ID="pLoadFileHeader" runat="server" >
        <asp:Label ID="Label5" Text="<span class='headerCaptions'>Загрузка данных</span>" runat="server"/>
        <asp:LinkButton ID="LinkButton3" runat="server" 
            OnClientClick="$find('LoadFile').hide(); return false;" />
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upLoadFile" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:FileUpload runat="server" ID="fileUpload" />

            <table>
                <tr>
                    <td>
                        <asp:LinkButton runat="server" ID="lnkLoadFile" OnClick="lnkLoadFile_OnClick"><div class="btnBlue">Загрузить</div></asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnCancelLoadFile" runat="server" CausesValidation="false"><div class="btnBlue">Закрыть</div></asp:LinkButton>
                    </td>
                </tr>
            </table>
    </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger runat="server" ControlID="lnkLoadFile"/>
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender runat="server" ID="popupLoadFileExtender" PopupDragHandleControlID="pLoadFileHeader"
    PopupControlID="popupLoadFile" CancelControlID="btnCancelLoadFile"
    TargetControlID="btnShowLoadFile" RepositionMode="None"
    BehaviorID="LoadFile"/>
<asp:Button ID="btnShowLoadFile" runat="server" style="display:none" />

<asp:Panel runat="server" ID="popupChangeDO" style="border: 2px solid black" BackColor="White">
    <asp:Panel ID="pChangeDOHeader" runat="server" >
        <asp:Label ID="Label1" Text="<span class='headerCaptions'>Выбор детских объединений</span>" runat="server"/>
        <asp:LinkButton ID="LinkButton1" runat="server" 
            OnClientClick="$find('ChangeDOPopup').hide(); return false;" />
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upChangeDO" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 900px;">
                <uct:Udod runat="server" ID="selectUdod" OnChange="selectUdod_OnChange"/>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:LinkButton ID="btnCancelChangeDO" runat="server" Text="<div class='btnBlue'>Отменить</div>" CausesValidation="false" />
</asp:Panel>
<ajaxToolkit:ModalPopupExtender runat="server" ID="popupChangeDOExtender" PopupDragHandleControlID="pChangeDOHeader"
    PopupControlID="popupChangeDO" CancelControlID="btnCancelChangeDO"
    TargetControlID="btnChangeDOPopup" RepositionMode="None"
    BehaviorID="ChangeDOPopup"/>
<asp:Button ID="btnChangeDOPopup" runat="server" style="display:none" />    

<asp:Panel runat="server" ID="popupEditPupil" style="display: none; border: 2px solid black;width: 810px;" BackColor="White">
    <asp:Panel ID="pEditPupilHeader" runat="server" >
        <asp:Label ID="litTitle" Text="<span class='headerCaptions'>Редактирование обучающегося</span>" runat="server"/>
        <asp:LinkButton ID="lnkClose" runat="server" 
            OnClientClick="$find('EditPupilPopup').hide(); return false;" />
    </asp:Panel>
    <asp:UpdatePanel ID="upEditPupil" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 800px;">
                <ajaxToolkit:TabContainer runat="server" ID="tabEditPupil" ActiveTabIndex="0">
                    <ajaxToolkit:TabPanel runat="server" ID="pPupil" HeaderText="<span style='color: black;'>Обучающийся</span>" >
                        <ContentTemplate>
                            <asp:UpdatePanel ID="upPupilInfo" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td colspan="3">
                                                <asp:Label runat="server" ID="lblErrors"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td class="tdx">
                                                Год обучения
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbNumYear" CssClass="inputShort" Visible="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td class="tdx">
                                                Наименование ДО
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox runat="server" ID="tbChildUnionName" CssClass="inputShort" Visible="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td class="tdx">
                                                Вид услуги
                                            </td>
                                            <td colspan="2">
                                                <asp:DropDownList runat="server" ID="ddlTypeBudget" DataTextField="TypeBudgetName" DataValueField="DictTypeBudgetId" DataSourceID="dsTypeBudget" Visible="false"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <uct:Fio ID="PupilFio" runat="server" ValidatorEnabled="true"  />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdx" >
                                                Дата рождения:
                                            </td>
                                            <td colspan="2"><div class="inputShort">
                                                    <asp:TextBox runat="server" ID="tbBirthday" CssClass="inputShort" />
                                                </div>
                                                <ajaxToolkit:CalendarExtender runat="server" ID="calendar" TargetControlID="tbBirthday" Animated="false" Format="dd.MM.yyyy"/>
                                                <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbBirthday" Mask="99/99/9999" MaskType="Date" />
                                                <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="reqvalrDate" ErrorMessage="Поле 'Дата рождения' не заполнено" ControlToValidate="tbBirthday" SetFocusOnError="true" ValidationGroup="71"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="reqvalrDate1" ErrorMessage="Поле 'Дата рождения' не заполнено" ControlToValidate="tbBirthday" SetFocusOnError="true" ValidationGroup="50"></asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="valrDate" runat="server" ControlToValidate="tbBirthday" MinimumValue="01.08.2012" MaximumValue="01.08.3012" Type="Date" text="Возраст обучающегося должен быть<br>в пределах от 6 до 19 лет<br>" Display="Dynamic" ValidationGroup="50"/>
                                                <asp:RangeValidator ID="valrDate2" runat="server" ControlToValidate="tbBirthday" MinimumValue="01.08.1812" MaximumValue="01.08.3012" Type="Date" text="Дата рождения не может быть<br>позднее Сегодня" Display="Dynamic" ValidationGroup="50"/>
                                                <asp:RangeValidator ID="valrDate3" runat="server" ControlToValidate="tbBirthday" MinimumValue="01.08.1812" MaximumValue="01.08.3012" Type="Date" text="Заявителю должно быть больше 14 лет" Display="Dynamic" ValidationGroup="50" Enabled="false"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdx">
                                                Пол:
                                            </td>
                                            <td colspan="2">
                                                <asp:RadioButtonList runat="server" ID="cbSex" RepeatDirection="Horizontal">
                                                    <Items>
                                                        <asp:ListItem Selected="true" Value="0">М</asp:ListItem>
                                                        <asp:ListItem Value="1">Ж</asp:ListItem>
                                                    </Items>
                                                </asp:RadioButtonList>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <uct:Document ID="PupilDocument" runat="server"  ValidatorEnabled="true"  /> 
                                                <asp:LinkButton runat="server" ID="lnkLoadFromReestr" OnClick="lnkLoadFromReestr_OnClick" ValidationGroup="10"><div class="btnBlue">Загрузить из реестра</div></asp:LinkButton>
                                                <asp:Label runat="server" ID="lblErrorLoad" ForeColor="red"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Номер телефона
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox runat="server" ID="tbPupilPhoneNumber" CssClass="input"></asp:TextBox>
                                                <ajaxToolkit:MaskedEditExtender ID="mee2" runat="server" TargetControlID="tbPupilPhoneNumber" Mask="8(999)999-99-99" MaskType="Number" />
                                            </td>
                                        </tr>
                                        <tr runat="server" id="tr1">
                                            <td class="tdx">
                                                EMail
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox runat="server" ID="tbPupilEmail" CssClass="input"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdx">
                                                Школа
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox runat="server" ID="tbPupilSchool" CssClass="inputLong"></asp:TextBox>
                                                <ajaxToolkit:AutoCompleteExtender runat="server" ID="acSchool" TargetControlID="tbPupilSchool" ServiceMethod="GetSchool" EnableCaching="true" CompletionSetCount="20" MinimumPrefixLength="1" UseContextKey="true" BehaviorID="AutoCompleteEx" CompletionListCssClass="addresslist"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdx">
                                                Класс
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox runat="server" ID="tbPupilClass" CssClass="inputLong"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <%-- <ajaxToolkit:TabPanel runat="server" ID="pParent" HeaderText="<span style='color: black;'>Представитель</span>">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="upParentInfo" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                <div style="overflow: scroll;">
                                            <table border=0><tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td colspan="2">
                                                            <uct:Fio runat="server" ID="ParentFio" ValidatorEnabled="true"  />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table border=0>
                                                                <tr>
                                                                    <td class="tdx">
                                                                        Заявитель:
                                                                    </td>
                                                                    <td >
                                                                        <asp:RadioButtonList runat="server" ID="cbRelative" RepeatDirection="Horizontal" DataTextField="UserTypeName" DataValueField="UserTypeId"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <uct:Document ID="parentDocument" runat="server" ValidatorEnabled="true" />
                                                            <asp:Label runat="server" ID="lblParentPassportDataError" ForeColor="Red"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Номер телефона 1
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="tbParentPhoneNumber" CssClass="input"></asp:TextBox>
                                                            <ajaxToolkit:MaskedEditExtender ID="mee3" runat="server" TargetControlID="tbParentPhoneNumber" Mask="8(999)999-99-99" MaskType="Number" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Номер телефона 2
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="tbParentExpressPhoneNumber" CssClass="input"></asp:TextBox>
                                                            <ajaxToolkit:MaskedEditExtender ID="mee3_1" runat="server" TargetControlID="tbParentExpressPhoneNumber" Mask="8(999)999-99-99" MaskType="Number" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            EMail
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="tbParentEmail" CssClass="input"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            </tr></table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>--%>
                    <ajaxToolkit:TabPanel runat="server" ID="pAddress" HeaderText="<span style='color: black;'>Адрес</span>">
                        <ContentTemplate>
                            <asp:UpdatePanel runat="server" ID="upAddresses" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" ID="lblAddressStr"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Panel ID="RegAddressPanel" runat="server" GroupingText="Адрес регистрации" style="width: 300px;">
                                                    <uct:Address runat="server" ID="RegAddress" AddressTypeId="5" />
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table>
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="upSavePupil" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:LinkButton ID="btnSavePupil" ValidationGroup="50" runat="server" Visible="false" Text="<div class='btnBlue'>Сохранить</div>" CausesValidation="true" OnClick="btnSavePupil_OnClick"/>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger runat="server" ControlID="btnYesError"/>
                        <asp:AsyncPostBackTrigger runat="server" ControlID="btnNoError"/>
                    </Triggers>
                </asp:UpdatePanel>
                
            </td>
            <td>
                <asp:LinkButton ID="btnCancel" runat="server" Text="<div class='btnBlue'>Отменить</div>" CausesValidation="false" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender runat="server" ID="popupEditPupilExtender" PopupDragHandleControlID="pEditPupilHeader"
    PopupControlID="popupEditPupil"
    TargetControlID="btnEditPupilPopup" RepositionMode="None"
    BehaviorID="EditPupilPopup" CancelControlID="btnCancel"/> 
<asp:Button ID="btnEditPupilPopup" runat="server" style="display:none" />

<asp:Panel runat="server" ID="popupError" CssClass="modalPopup" style="display: none; border: 2px solid black; width: 400px;" BackColor="White">
        <asp:Panel ID="pErrorHeader" runat="server" >
           <asp:Label ID="Label3" Text="<span class='headerCaptions'>Ошибка</span>" runat="server"/>
           <asp:LinkButton ID="LinkButton4" runat="server" 
               OnClientClick="$find('ErrorPopup').hide(); return false;" />
        </asp:Panel>
        <div >
            <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label runat="server" ID="lblError">По введённым параметрам в Реестре не найдено ни одной записи. Проверьте правильность заполнения и повторите запрос. Если Вы уверены в корректности введённых данных, то можете создать запись для данного обучающегося вручную. Создать запись в ручную?</asp:Label>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div >
            <table>
                <tr>
                    <td><asp:LinkButton ID="btnYesError" runat="server" CausesValidation="false" OnClick="btnYesError_OnClick"><div class='btnBlue'>Да</div></asp:LinkButton></td>
                    <td><asp:LinkButton ID="btnNoError" runat="server" CausesValidation="false" ><div class='btnBlue'>Нет</div></asp:LinkButton></td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupErrorExtender" PopupDragHandleControlID="pErrorHeader"
        PopupControlID="popupError" CancelControlID="btnNoError"
        TargetControlID="btnShowError" RepositionMode="None" BackgroundCssClass="modalBackground"
        BehaviorID="ErrorPopup"/>
    <asp:Button ID="btnShowError" runat="server" style="display:none" />
    
    <asp:Panel runat="server" ID="popupErrorSave" CssClass="modalPopup" style="display: none; border: 2px solid black; width: 400px;" BackColor="White">
        <asp:Panel ID="pErrorSaveHeader" runat="server" >
           <asp:Label ID="Label2" Text="<span class='headerCaptions'>Ошибка</span>" runat="server"/>
           <asp:LinkButton ID="LinkButton5" runat="server" 
               OnClientClick="$find('ErrorSavePopup').hide(); return false;" />
        </asp:Panel>
        <div >
            <asp:UpdatePanel runat="server" ID="upErrorSave" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label runat="server" ID="Label4">Во время сохранения заявки произошла ошибка. Проверьте правильность заполнения адреса и других полей</asp:Label>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div >
            <asp:LinkButton ID="btnCancelError" runat="server" CausesValidation="false"><div class='btnBlue'>OK</div></asp:LinkButton>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupErrorSaveExtender" PopupDragHandleControlID="pErrorSaveHeader"
        PopupControlID="popupErrorSave" CancelControlID="btnCancelError"
        TargetControlID="btnErrorSave" RepositionMode="None" BackgroundCssClass="modalBackground"
        BehaviorID="ErrorSavePopup"/>
    <asp:Button ID="btnErrorSave" runat="server" style="display:none" />

<asp:ObjectDataSource runat="server" ID="dsTypeBudget" TypeName="Udod.Dal.DictBudgetTypeDb" SelectMethod="GetBudgetTypes"></asp:ObjectDataSource>

<asp:ObjectDataSource runat="server" ID="dsDO" TypeName="Udod.Dal.ChildUnionDb" SelectMethod="GetChildUnionsShort" >
    <SelectParameters>
        <asp:ControlParameter Name="cityId" ControlID="cityUdodFilter" PropertyName="SelectedCity" ConvertEmptyStringToNull="true" />
        <asp:ControlParameter Name="udodId" ControlID="cityUdodFilter" PropertyName="SelectedUdod" ConvertEmptyStringToNull="true" />
        <asp:Parameter Name="programId"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="profileId"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="sectionId"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="typeBudgetId"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="sectionName"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="profileName"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="programName"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="childUnionName"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="cityCode"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="rayonId"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="metroId"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="udodName"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="rayonCode"  ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="isReception" DefaultValue="false" ConvertEmptyStringToNull="false"/>
        <asp:ControlParameter Name="teacherId" ControlID="ddlTeacher" PropertyName="SelectedValue"  ConvertEmptyStringToNull="true"/>
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource runat="server" ID="dsTeacher" TypeName="Udod.Dal.TeacherDb" SelectMethod="GetTeacher">
    <SelectParameters>
        <asp:Parameter Name="cityId"  ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="udodId" ControlID="cityUdodFilter" PropertyName="SelectedUdod" ConvertEmptyStringToNull="true" />
        <asp:Parameter Name="childUnionId"  ConvertEmptyStringToNull="true"/>
    </SelectParameters>
</asp:ObjectDataSource>
</asp:Content>
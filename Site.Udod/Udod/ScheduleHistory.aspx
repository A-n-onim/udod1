﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="ScheduleHistory.aspx.cs" Inherits="Udod_ScheduleHistory" %>
<%@ Register TagPrefix="uct" TagName="CityUdodFilter" Src="~/UserControl/Filter/ucCityUdodFilter.ascx" %>
<%@ Register src="../UserControl/Menu/ucPager.ascx" tagName="Pager" tagPrefix="uct" %>
<%@ Register src="../UserControl/Menu/ucErrorPopup.ascx" tagName="ErrorPopup" tagPrefix="uct" %>
<%@ Register TagPrefix="uct" TagName="DOPassport" Src="~/UserControl/Udod/ucDOPassport.ascx" %>

<asp:Content runat="server" ID="content" ContentPlaceHolderID="body">
    <asp:UpdatePanel ID="upCityUdodFilter" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uct:CityUdodFilter ID="cityUdodFilter" runat="server" UdodDdlEnabled="true" AddAllProperty="true" OnCityChange="cityUdodFilter_OnCityChange" OnUdodChange="cityUdodFilter_OnUdodChange" OnloadComplete="cityUdodFilter_OnloadComplete"/>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ID="upFilter" UpdateMode="Conditional">
        <ContentTemplate>
            <table>
                <tr>
                    <td>Детское объединение</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlDOFilter" CssClass="input" DataTextField="Name" DataValueField="ChildUnionId" AutoPostBack="true" OnDataBound="ddlDOFilter_OnDataBound" OnSelectedIndexChanged="ddlDOFilter_OnSelectedIndexChanged"/>
                    </td>
                    <td>
                        Группа
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlGroupFilter" CssClass="input" DataTextField="Name" DataValueField="UdodAgeGroupId" OnDataBound="ddlGroupFilter_OnDataBound"></asp:DropDownList>
                    </td>
                    <td>Форма занятий
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlTypeEducation" DataSourceID="dsTypeEducation" CssClass="input" DataTextField="EducationTypeName" DataValueField="EducationTypeId" OnDataBound="ddlGroupFilter_OnDataBound"/>
                    </td>
                </tr>
                <tr>
                    <td>Педагог</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlTeacherFilter" CssClass="input" DataTextField="TeacherName" DataValueField="UserId" OnDataBound="ddlGroupFilter_OnDataBound"/>
                    </td>
                    <td>Направленность</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlProgramFilter" CssClass="input" DataTextField="Name" DataValueField="ProgramId" OnDataBound="ddlGroupFilter_OnDataBound"/>
                    </td>
                    <td >
                        Дата от
                    </td>
                    <td>
                        <div class="inputXShort">
                            <asp:TextBox runat="server" ID="tbDateStart" CssClass="inputXShort" />
                        </div>
                        <ajaxToolkit:CalendarExtender runat="server" ID="calendarDateStart" TargetControlID="tbDateStart" Format="dd.MM.yyyy"/>
                        <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbDateStart" Mask="99/99/9999" MaskType="Date"  />
                        <asp:RangeValidator runat="server" ID="dateVal1" ValidationGroup="88" ErrorMessage="Неверная дата" ControlToValidate="tbDateStart" MinimumValue="01.01.1900" MaximumValue="01.01.3000" Type="Date" Display="Dynamic"></asp:RangeValidator>
                    </td>
                        
                </tr>
                <tr>
                    <td>
                        Год обучения:
                    </td>
                    <td>
                        <asp:DropDownList CssClass="input" runat="server" ID="ddlNumYear" >
                        <asp:ListItem Text="Все" Value="" Selected="True" />
                        <asp:ListItem Text="1" Value="1" />
                        <asp:ListItem Text="2" Value="2" />
                        <asp:ListItem Text="3" Value="3" />
                        <asp:ListItem Text="4" Value="4" />
                        <asp:ListItem Text="5" Value="5" />
                        <asp:ListItem Text="6" Value="6" />
                        <asp:ListItem Text="7" Value="7" />
                        <asp:ListItem Text="8" Value="8" />
                        <asp:ListItem Text="9" Value="9" />
                        <asp:ListItem Text="10" Value="10" />
                        <asp:ListItem Text="11" Value="11" />
                        <asp:ListItem Text="12" Value="12" />
                        <asp:ListItem Text="13" Value="13" />
                        <asp:ListItem Text="14" Value="14" />
                        <asp:ListItem Text="15" Value="15" />
                    </asp:DropDownList>
                    </td>
                    <td>
                        Вид услуги
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlTypeBudget" DataSourceID="dsTypeBudget" CssClass="input" DataTextField="TypeBudgetName" DataValueField="DictTypeBudgetId" OnDataBound="ddlGroupFilter_OnDataBound"/>
                    </td>
                    <td runat="server" ID="td6">
                        Дата до
                    </td>
                    <td runat="server" ID="td7">
                        <div class="inputXShort">
                            <asp:TextBox runat="server" ID="tbDateEnd" CssClass="inputXShort" />
                        </div>
                        <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender3" TargetControlID="tbDateEnd" Format="dd.MM.yyyy"/>
                        <ajaxToolkit:MaskedEditExtender ID="mee4" runat="server" TargetControlID="tbDateEnd" Mask="99/99/9999" MaskType="Date" />
                        <asp:RangeValidator runat="server" ID="dateVal4" ValidationGroup="88" ErrorMessage="Неверная дата" ControlToValidate="tbDateEnd" MinimumValue="01.01.1900" MaximumValue="01.01.3000" Type="Date" Display="Dynamic"></asp:RangeValidator>
                    </td>
                    
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:LinkButton ID="lnkFindByName" runat="server" OnClick="lnkFindByName_OnClick"><div class="btnBlue">Поиск</div></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ID="upGridView" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView runat="server" ID="gvScheduleHistory" AutoGenerateColumns="false" OnRowDataBound="gvScheduleHistory_OnRowDataBound">
                <EmptyDataTemplate>
                    <asp:Label ID="lblEmptyGvPupilList" runat="server" style="font-size:16px">Расписание не найдено</asp:Label>
                </EmptyDataTemplate>
                
                <Columns>
                    <asp:TemplateField HeaderText="№">
                        
                        <ItemTemplate>
                            <%# (gvScheduleHistory.Rows.Count + pgrScheduleHistory.PagerIndex)%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Дата занятия">
                        <ItemTemplate>
                            <%# ((DateTime)Eval("LessonDate")).ToShortDateString()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Время начала занятия"  >
                        <ItemTemplate>
                            <%# ((DateTime)Eval("StartTime")).ToShortTimeString()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Продолжительность занятий без перемены">
                        <ItemStyle Width="60"></ItemStyle>
                        <ItemTemplate>
                            <%# ((DateTime)((DateTime)Eval("EndTime")) - ((DateTime)Eval("StartTime"))).TotalMinutes%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Продолжительность занятий с переменой"  >
                        <ItemStyle Width="60"></ItemStyle>
                        <ItemTemplate>
                            <%# ((DateTime)((DateTime)Eval("EndRecess")) - ((DateTime)Eval("StartTime"))).TotalMinutes%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Родительское ДО"  >
                        <ItemTemplate>
                            <%# Eval("ChildUnionName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Группа">
                        <ItemTemplate>
                            <%# Eval("GroupName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Педагог">
                        <ItemTemplate>
                            <%# Eval("Fio")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Предмет">
                        <ItemTemplate>
                            <%# Eval("SectionName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText=""  >
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lnkShowGroup" OnClick="lnkCommand_OnClick"><img alt="" width='20px' height='20px' src='../Images/groups_folder.png' border=0 title='Показать группу'></asp:LinkButton>
                            <asp:LinkButton runat="server" ID="lnkShowDO" OnClick="lnkCommand_OnClick"><img alt="" width='20px' height='20px' src='../Images/details.png' border=0 title='Показать ДО'></asp:LinkButton>
                            <asp:LinkButton runat="server" ID="lnkShowScheduleLesson" OnClick="lnkCommand_OnClick"><img alt="" width='20px' height='20px' src='../Images/calendar_information.PNG' border=0 title='Показать элемент расписания'></asp:LinkButton>
                            <asp:LinkButton runat="server" ID="lnkEdit" OnClick="lnkCommand_OnClick"><img alt="" width='20px' height='20px' src='../Images/calendar_edit.PNG' border=0 title='Замена'></asp:LinkButton>
                            <asp:LinkButton runat="server" ID="lnkCancel" OnClick="lnkCommand_OnClick"><img alt="" width='20px' height='20px' src='../Images/calendar_delete.PNG' border=0 title='Удалить'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger runat="server" ControlID="lnkAddScheduleOk"/>
        </Triggers>
    </asp:UpdatePanel>

    <uct:ErrorPopup runat="server" ID="errorPopup" OnErrorOkClick="errorPopup_OnErrorOkClick"/>

    <uct:Pager runat="server" ID="pgrScheduleHistory" needStartDots="false" needEndDots="false" pagesCount="10" OnChange="pgrScheduleHistory_OnChange"/>

    <asp:Panel runat="server" ID="popupPassportDO" style="padding: 5px; display: none; border: 2px solid black" BackColor="White">
    <asp:Panel ID="pPassportDOHeader" runat="server" >
        <asp:Label ID="Label5" Text="<span class='headerCaptions'>Паспорт детского объединения</span>" runat="server"/>
        <asp:LinkButton ID="LinkButton3" runat="server" 
            OnClientClick="$find('PassportDOPopup').hide(); return false;" />
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upPassportDO" UpdateMode="Conditional">
        <ContentTemplate>
            <div >
                <uct:DOPassport runat="server" ID="selectDO" Edit="false"/>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:LinkButton ID="btnCancelPassportDO" runat="server" CausesValidation="false"><div class="btnBlue">Закрыть</div></asp:LinkButton>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender runat="server" ID="popupPassportDOExtender" PopupDragHandleControlID="pPassportDOHeader"
    PopupControlID="popupPassportDO" CancelControlID="btnCancelPassportDO"
    TargetControlID="btnShowPassportDO" RepositionMode="None"
    BehaviorID="PassportDOPopup"/>
<asp:Button ID="btnShowPassportDO" runat="server" style="display:none" />

<asp:Panel runat="server" ID="popupAddAgeGroup" style="display: none; border: 2px solid black" BackColor="White">
            
        <asp:Panel ID="pnlHeader" runat="server" >
            <asp:UpdatePanel runat="server" ID="upHeader" UpdateMode="Conditional">
            <ContentTemplate>
            <asp:Label ID="litTitle" Text="<span class='headerCaptions'>Просмотр группы</span>" runat="server"/>
            <asp:LinkButton ID="lnkClose" runat="server" 
                OnClientClick="$find('AddAgeGroupPopup').hide(); return false;" />
            </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upPopup" UpdateMode="Conditional">
            <ContentTemplate>
        <div >
            
            <table>
                <tr>
                    <td>Условное название</td>
                    <td><asp:Label runat="server" ID="lblName"></asp:Label></td>
                </tr>
                <tr >
                    <td class="tdx">
                        Детское объединение:
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblDO"/>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Педагоги:
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblTeachers"></asp:Label>
                    </td>
                </tr>
                    
                <tr>
                    <td class="tdx">
                        Возраст от:
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblStartAge"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Возраст до:
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblEndAge"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Вид услуги:
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblTypeBudget"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Год обучения:
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblNumYear"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Форма занятий:
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblTypeEducation"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Примечание:
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblComment"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Адрес провердения занятий:
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblbAddress"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Место проведения занятий:
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblPlace"></asp:Label>
                    </td>
                </tr>
            </table>
                
        </div>
        </ContentTemplate>
            </asp:UpdatePanel>
        <asp:LinkButton ID="btnCancel" runat="server" Text="<div class='btnBlue'>Закрыть</div>" CausesValidation="false" />
        
    </asp:Panel>
    
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupAddExtender" PopupDragHandleControlID="pnlHeader"
        PopupControlID="popupAddAgeGroup" CancelControlID="btnCancel"
        TargetControlID="btnShowPopup" RepositionMode="None"
        BehaviorID="AddAgeGroupPopup"/>

    <asp:Button ID="btnShowPopup" runat="server" style="display:none" />

    <asp:Panel runat="server" ID="popupAddSchedule" style="padding: 5px; border: 2px solid black" BackColor="White">
    <asp:Panel ID="pAddScheduleHeader" runat="server" >
        <asp:Label ID="Label1" Text="<span class='headerCaptions'>Элемент расписания</span>" runat="server"/>
        <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="$find('AddSchedulePopup').hide(); return false;" />
    </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upAddSchedule" UpdateMode="Conditional">
        <ContentTemplate>
            <table>
                <tr>
                    <td>Педагог</td>
                    <td>
                        <asp:Label runat="server" ID="lblTeacher"></asp:Label>
                        <asp:DropDownList runat="server" ID="ddlTeacher" CssClass="input" DataTextField="TeacherName" DataValueField="UserId"/>
                    </td>
                </tr>
                <tr>
                    <td>Предмет</td>
                    <td>
                        <asp:Label runat="server" ID="lblSection"></asp:Label>
                    </td>
                </tr>
                
                <tr>
                    <td runat="server" ID="tdLessondate">День недели</td>
                    <td>
                        <asp:Label runat="server" ID="lblWeekDay"></asp:Label>
                        <asp:TextBox runat="server" ID="tbLessonDate" CssClass="input"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="tbLessonDate" Format="dd.MM.yyyy"/>
                        <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender4" runat="server" TargetControlID="tbLessonDate" Mask="99/99/9999" MaskType="Date"  />
                        <asp:RangeValidator runat="server" ID="RangeValidator1" ValidationGroup="88" ErrorMessage="Неверная дата" ControlToValidate="tbLessonDate" MinimumValue="01.01.1900" MaximumValue="01.01.3000" Type="Date" Display="Dynamic"></asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td>Время начала занятий</td>
                    <td>
                        <asp:Label runat="server" ID="lblStartTime"></asp:Label>
                        <asp:TextBox runat="server" ID="tbStartTime" CssClass="input"></asp:TextBox>
                        <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99:99" MaskType="Time" TargetControlID="tbStartTime"/> 
                        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator3" runat="server" ControlExtender="MaskedEditExtender3" ControlToValidate="tbStartTime"
                        IsValidEmpty="False"
                        EmptyValueMessage="Необходимо значение"
                        InvalidValueMessage="Неверное значение"
                        Display="Dynamic"
                        TooltipMessage=""
                        EmptyValueBlurredText="*"
                        InvalidValueBlurredMessage="*"
                        ValidationGroup="88"/>
                    </td>
                </tr>
                <tr>
                    <td>Время окончания занятия</td>
                    <td>
                        <asp:Label runat="server" ID="lblEndTime"></asp:Label>
                        <asp:TextBox runat="server" ID="tbEndTime" CssClass="input"></asp:TextBox>
                        <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99:99" MaskType="Time" TargetControlID="tbEndTime"/> 
                        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="MaskedEditExtender1" ControlToValidate="tbEndTime"
                        IsValidEmpty="False"
                        EmptyValueMessage="Необходимо значение"
                        InvalidValueMessage="Неверное значение"
                        Display="Dynamic"
                        TooltipMessage=""
                        EmptyValueBlurredText="*"
                        InvalidValueBlurredMessage="*"
                        ValidationGroup="88"/>                        
                    </td>
                </tr>
                <tr>
                    <td>Время окончания перемены</td>
                    <td>
                        <asp:Label runat="server" ID="lblEndRecess"></asp:Label>   
                        <asp:TextBox runat="server" ID="tbEndRecess" CssClass="input"></asp:TextBox>
                        <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99:99" MaskType="Time" TargetControlID="tbEndRecess"/> 
                        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlExtender="MaskedEditExtender2" ControlToValidate="tbEndRecess"
                        IsValidEmpty="False"
                        EmptyValueMessage="Необходимо значение"
                        InvalidValueMessage="Неверное значение"
                        Display="Dynamic"
                        TooltipMessage=""
                        EmptyValueBlurredText="*"
                        InvalidValueBlurredMessage="*"
                        ValidationGroup="88"/>                     
                    </td>
                </tr>
                <tr>
                    <td>Комментарий</td>
                    <td>
                        <asp:Label runat="server" ID="lblScheduleComment"></asp:Label>
                        <asp:TextBox runat="server" ID="tbComment" CssClass="input"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            
        </Triggers>
        </asp:UpdatePanel>
        <table>
            <tr>
                <td><asp:LinkButton ID="lnkAddScheduleOk" runat="server" OnClick="lnkAddScheduleOk_OnClick" ValidationGroup="88"><div class='btnBlue'>Сохранить</div></asp:LinkButton></td>
                <td><asp:LinkButton ID="lnkAddScheduleCancel" runat="server" CausesValidation="false" ><div class='btnBlue'>Закрыть</div></asp:LinkButton></td>
            </tr>
        </table>    
</asp:Panel>
<ajaxToolkit:ModalPopupExtender runat="server" ID="popupAddScheduleExtender" PopupDragHandleControlID="pAddScheduleHeader"
    PopupControlID="popupAddSchedule" CancelControlID="lnkAddScheduleCancel"
    TargetControlID="btnShowAddSchedulePopup" RepositionMode="None"
    BehaviorID="AddSchedulePopup"/>
<asp:Button ID="btnShowAddSchedulePopup" runat="server" style="display:none" />

    <asp:ObjectDataSource runat="server" ID="dsTypeBudget" TypeName="Udod.Dal.DictBudgetTypeDb" SelectMethod="GetBudgetTypes"></asp:ObjectDataSource>
    <asp:ObjectDataSource runat="server" ID="dsTypeEducation" TypeName="Udod.Dal.DictEducationTypeDb" SelectMethod="GetEducationTypes"></asp:ObjectDataSource>
</asp:Content>
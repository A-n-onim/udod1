﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Udod_SchoolPupilList : BasePage
{
    protected Guid? PupilId { set { ViewState["pupilId"] = value; } get { if (ViewState["pupilId"] == null) return null;
        return (Guid) ViewState["pupilId"];
    } }
    protected Int64? PupilReestrCode
    {
        set { ViewState["PupilReestrCode"] = value; }
        get
        {
            if (ViewState["PupilReestrCode"] == null) return null;
            return (Int64)ViewState["PupilReestrCode"];
        }
    }
    protected Guid? PupilReestrGuidCode
    {
        set { ViewState["PupilReestrGuidCode"] = value; }
        get
        {
            if (ViewState["PupilReestrGuidCode"] == null) return null;
            return new Guid(ViewState["PupilReestrGuidCode"].ToString());
        }
    }
    private bool? IsEdit
    {
        get
        {
            if (ViewState["IsEdit"] == null) return null;
            return (bool)ViewState["IsEdit"];
        }
        set
        {
            ViewState["IsEdit"] = value;
        }
    } 


    protected void Page_Load(object sender, EventArgs e)
    {
        //string values = ConfigurationManager.AppSettings["Schools"];
        //var splits = values.Split(',');
        lnkSendAll.Visible = UserContext.Roles.IsAdministrator;
        ExtendedUdod udod;
        if (UserContext.UdodId.HasValue)
        using (UdodDb db = new UdodDb())
        {
            udod= db.GetUdod(UserContext.UdodId.Value);
        }
        else
        {
            udod=new ExtendedUdod(){IsContingentEnabled = false};
        }
        CheckPermissions(UserContext.Roles.IsAdministrator || (UserContext.UdodId.HasValue && udod.IsContingentEnabled));
        if (UserContext.Roles.RoleId==(int)EnumRoles.UdodEmployee)
        {
            UserContext.UdodId = UserContext.Profile.UdodId;
            cityUdodFilter.Visible = false;
        }
        valrDate.MinimumValue = DateTime.Today.AddYears(-22).ToString().Substring(0, 10);
        valrDate.MaximumValue = DateTime.Today.AddYears(-2).ToString().Substring(0, 10);
        valrDate2.MaximumValue = DateTime.Today.ToString().Substring(0, 10);
        //valrDate3.MaximumValue = DateTime.Today.AddYears(-14).ToString().Substring(0, 10);
        acSchool.ServicePath = SiteUtility.GetUrl("~/Webservices/Dict.asmx");

        if (!Page.IsPostBack)
        {
            if (UserContext.UdodId.HasValue)
            {
                using (PupilsDb db = new PupilsDb())
                {
                    ddlClass.DataSource = db.SchoolGetClasses(UserContext.UdodId.Value);
                    ddlClass.DataBind();
                    upFilters.Update();
                }
            }
        }
    }

    protected void pgrPupilList_OnChange(object sender, EventArgs e)
    {
        gvPupilList.DataBind();
        upPupilList.Update();
    }

    protected void cityUdodFilter_OnCityChange(object sender, EventArgs e)
    {
        using (PupilsDb db = new PupilsDb())
        {
            ddlClass.DataSource = db.SchoolGetClasses(UserContext.UdodId.Value);
            ddlClass.DataBind();
            upFilters.Update();
        }

        gvPupilList.DataBind();
        upPupilList.Update();
    }

    protected void cityUdodFilter_OnUdodChange(object sender, EventArgs e)
    {
        gvPupilList.DataBind();
        upPupilList.Update();
    }

    protected void cityUdodFilter_OnloadComplete(object sender, EventArgs e)
    {
        
    }

    protected void gvPupilList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        PupilReestrCode = null;
        // Загрузка редактирования
        if (gvPupilList.SelectedDataKey != null)
        {
            string tmp=gvPupilList.Rows[gvPupilList.SelectedIndex].Cells[8].Text;
            Regex regex = new Regex(@"'([^<]+)'>");
            var matches= regex.Matches(tmp);
            StringBuilder sb = new StringBuilder();
            foreach (var match in matches)
            {
                sb.Append(match.ToString().Replace("'>","").Replace("'",""));
            }
            lblErrors.Text = sb.ToString().Replace("ImportedErrorMessage title=","");
            PupilId = (Guid) gvPupilList.SelectedDataKey.Value;
            using (PupilsDb db = new PupilsDb())
            {
                ExtendedImportedPupil pupil = db.GetSchoolPupil(PupilId.Value);
                try
                {
                    PupilReestrCode = Convert.ToInt64(pupil.ReestrCode);
                }
                catch (Exception)
                {
                    PupilReestrCode = null;
                }
                
                // закрываем на редактирование
                PupilFio.Enabled =
                PupilDocument.Enabled =
                tbBirthday.Enabled =
                tbPupilSchool.Enabled =
                cbSex.Enabled =
                tbPupilClass.Enabled = !pupil.IsEdit;

                PupilFio.Enabled =
                    PupilDocument.Enabled = false;
                if (!pupil.IsEdit)
                {
                    PupilFio.EnabledMiddleName = true;
                    btnSavePupil.Visible = true;
                    upSavePupil.Update();
                }

                PupilFio.LastName = pupil.LastName;
                PupilFio.FirstName = pupil.FirstName;
                PupilFio.MiddleName = pupil.MiddleName;
                
                PupilDocument.Series = pupil.Passport.Series;
                PupilDocument.Number = pupil.Passport.Number;
                PupilDocument.IssueDate = pupil.Passport.IssueDate;
                PupilDocument.PassportType = pupil.Passport.PassportType.PassportTypeId;
                tbBirthday.Text = pupil.Birthday.ToShortDateString();
                tbPupilEmail.Text = pupil.EMail;
                tbPupilPhoneNumber.Text = pupil.PhoneNumber;
                tbPupilSchool.Text = pupil.SchoolName;
                tbPupilClass.Text = pupil.ClassName;
                tbNumYear.Text = pupil.NumYear.ToString();
                //tbChildUnionName.Text = pupil.ChildUnions[0].Name;
                cbSex.SelectedIndex = pupil.Sex;
                lblAddressStr.Text = pupil.AddressStr;
                
                if (pupil.Parents.Count > 0)
                {
                    /*ParentFio.LastName = pupil.Parents.FirstOrDefault().LastName;
                    ParentFio.FirstName = pupil.Parents.FirstOrDefault().FirstName;
                    ParentFio.MiddleName = pupil.Parents.FirstOrDefault().MiddleName;
                    parentDocument.PassportType = pupil.Parents.FirstOrDefault().Passport.PassportType.PassportTypeId;
                    parentDocument.Series = pupil.Parents.FirstOrDefault().Passport.Series;
                    parentDocument.Number = pupil.Parents.FirstOrDefault().Passport.Number;
                    parentDocument.IssueDate = pupil.Parents.FirstOrDefault().Passport.IssueDate;
                    tbParentPhoneNumber.Text = pupil.Parents.FirstOrDefault().PhoneNumber;
                    tbParentExpressPhoneNumber.Text = pupil.Parents.FirstOrDefault().ExpressPhoneNumber;
                    tbParentEmail.Text = pupil.Parents.FirstOrDefault().EMail;*/
                }
                if (pupil.Addresses.Count > 0)
                {
                    var regAddress = pupil.Addresses.FirstOrDefault();
                    RegAddress.IsBti = string.IsNullOrEmpty(regAddress.KladrCode);
                    RegAddress.Code = string.IsNullOrEmpty(regAddress.KladrCode)
                                          ? regAddress.BtiCode.Value.ToString()
                                          : regAddress.KladrCode;
                    RegAddress.Databind();
                    RegAddress.Index = regAddress.PostIndex;
                    RegAddress.Housing = regAddress.Housing;
                    RegAddress.NumberHouse = regAddress.HouseNumber;
                    RegAddress.Fraction = regAddress.Fraction;
                    RegAddress.Flat = regAddress.FlatNumber;
                    RegAddress.Building = regAddress.Building;
                }
                else
                {
                    RegAddress.IsBti = true;
                    RegAddress.Code = "0";
                    RegAddress.Databind();
                    RegAddress.Index = "";
                    RegAddress.Housing = "";
                    RegAddress.NumberHouse = "";
                    RegAddress.Fraction = "";
                    RegAddress.Flat = "";
                    RegAddress.Building = "";
                }
            }
            // открываю доступ на редактирование паспорта если есть такая ошибка
            if (lblErrors.Text.Contains("не сооветствует формату"))
            {
                PupilDocument.EnabledTypeDocument = true;
            }
            upEditPupil.Update();
            //upPupilInfo.Update();
            popupEditPupilExtender.Show();
            gvPupilList.DataBind();
            upPupilList.Update();
        }
    }

    protected void lnkFindByName_OnClick(object sender, EventArgs e)
    {
        lblInfo.Text = "";
        gvPupilList.DataBind();
        upPupilList.Update();
    }

    protected void lnkImportPupils_OnClick(object sender, EventArgs e)
    {
        // загрузка из Excel
        popupLoadFileExtender.Show();
    }

    /*protected void lnkLoadFile_OnClick(object sender, EventArgs e)
    {
        // загрзка самих данных и их проверка
        //Загрузка файлов включить
        
        FileFormatType format;
        if (fileUpload.HasFile)
        {

            if (fileUpload.FileName.EndsWith(".xlsx"))
            {
                format = FileFormatType.Excel2007Xlsx;
            }
            else
            {
                format = FileFormatType.Excel2003;
            }

            ImportFromExcel(fileUpload.FileContent, format);
            
        }
        upLoadFile.Update();
        gvPupilList.DataBind();
        upPupilList.Update();
        popupLoadFileExtender.Hide();
    }*/
    /*
    protected List<ExtendedPupil> ImportFromExcel(Stream data, FileFormatType format)
    {
        List<ExtendedPupil> pupils = new List<ExtendedPupil>();
        Workbook workbook = new Workbook();
        try
        {
            workbook.Open(data, format);
            //workbook.LoadData(data);
        }
        catch (Exception)
        {
            throw new Exception("Данный формат файлов не поддерживается.");
        }
        using (KladrDb db = new KladrDb())
        {


            Worksheet sheet = workbook.Worksheets[0];
            // загрузка в список и вывод ошибок если что
            int row = 0;
            int d = sheet.Cells.Rows.Count;
            for (; row <= d; row++)
            {
                ExtendedImportedPupil pupil = new ExtendedImportedPupil();
                if (string.IsNullOrEmpty(sheet.Cells[1 + row, 1].StringValue) || string.IsNullOrEmpty(sheet.Cells[1 + row, 22].StringValue)) continue;
                string childUnionName = sheet.Cells[1 + row, 21].StringValue;
                int numYear = sheet.Cells[1 + row, 22].IntValue;
                
                pupil.LastName = sheet.Cells[1 + row, 1].StringValue;
                pupil.FirstName = sheet.Cells[1 + row, 2].StringValue;
                pupil.MiddleName = sheet.Cells[1 + row, 3].StringValue;
                string passporttype = sheet.Cells[1 + row, 6].StringValue;

                pupil.Passport = new ExtendedPassport()
                                     {
                                         PassportType =
                                             new ExtendedPassportType()
                                                 {
                                                     PassportTypeId =
                                                         passporttype.Trim().ToLower() == "паспорт рф"
                                                             ? 2
                                                             : passporttype.Trim().ToLower() ==
                                                               "свидетельство о рождении рф"
                                                                   ? 3
                                                                   : 4
                                                 },
                                         Series = sheet.Cells[1 + row, 7].StringValue,
                                         Number = sheet.Cells[1 + row, 8].StringValue,
                                         IssueDate = sheet.Cells[1 + row, 9].DateTimeValue,
                                     };
                pupil.Birthday = sheet.Cells[1 + row, 4].DateTimeValue;
                pupil.Sex = sheet.Cells[1 + row, 5].StringValue.Trim().ToLower() == "м" ? 0 : 1;
                pupil.AddressStr = sheet.Cells[1 + row, 12].StringValue;

                ExtendedAddress address = new ExtendedAddress();
                address.Fraction = address.Housing = address.Building = address.FlatNumber = "";
                string region = sheet.Cells[1 + row, 13].StringValue.Trim().ToLower();
                string rayon = sheet.Cells[1 + row, 14].StringValue.Trim().ToLower();
                string city = sheet.Cells[1 + row, 15].StringValue.Trim().ToLower();
                string street = sheet.Cells[1 + row, 16].StringValue.Trim().ToLower();
                string house = sheet.Cells[1 + row, 17].StringValue.Trim().ToLower();
                string flat = sheet.Cells[1 + row, 18].StringValue.Trim().ToLower();

                string code = db.GetCodeAddress(region, rayon, city, street);
                if (!string.IsNullOrEmpty(code))
                {
                    if (code.Length > 10)
                    {
                        //кладр
                        address.KladrCode = code;
                        address.AddressStr = db.GetAddress(code, house, "", "", "", flat, null).AddressStr;
                    }
                    else
                    {
                        //БТИ
                        address.BtiCode = Convert.ToInt32(code);
                        address.AddressStr = db.GetAddress(code, house, "", "", "", flat, 0).AddressStr;
                    }
                    address.HouseNumber = house;
                    address.FlatNumber = flat;

                    pupil.Addresses = new List<ExtendedAddress>();
                    pupil.Addresses.Add(address);
                }
                pupil.PhoneNumber = sheet.Cells[1 + row, 10].StringValue;
                pupil.EMail = sheet.Cells[1 + row, 11].StringValue;
                pupil.SchoolName = sheet.Cells[1 + row, 19].StringValue;
                pupil.ClassName = sheet.Cells[1 + row, 20].StringValue;
                
                string typeBudget = sheet.Cells[1 + row, 21].StringValue;
                if (typeBudget.Trim().ToLower()=="платно")
                {
                    pupil.TypeBudget = new ExtendedTypeBudget()
                    {
                        DictTypeBudgetId = 1,
                        TypeBudgetName = "Платно"
                    };
                }
                else if (typeBudget.Trim().ToLower() == "бесплатно")
                {
                    pupil.TypeBudget = new ExtendedTypeBudget()
                    {
                        DictTypeBudgetId = 2,
                        TypeBudgetName = "Бесплатно"
                    };
                }
                else pupil.TypeBudget=new ExtendedTypeBudget();
                //pupils.Add(pupil);

                using (PupilsDb db1 = new PupilsDb())
                {
                    //long childUnionid = Convert.ToInt64(ddlDO1.SelectedValue);
                    
                    //db1.ImportedAddPupil(null, UserContext.UdodId.Value, pupil.LastName, pupil.FirstName, pupil.MiddleName, pupil.Birthday, Convert.ToBoolean(pupil.Sex), pupil.Passport.PassportType.PassportTypeId, pupil.Passport.Series,pupil.Passport.Number, pupil.Passport.IssueDate.HasValue?pupil.Passport.IssueDate.Value:DateTime.Now,
                    //    address.BtiCode.HasValue ? address.BtiCode.Value.ToString() : address.KladrCode, "", address.HouseNumber, address.Fraction, address.Housing, address.Building, address.FlatNumber, 5, address.BtiCode.HasValue ? 0 : 1, pupil.AddressStr, 0, 0, pupil.EMail, pupil.PhoneNumber, pupil.SchoolName, null, pupil.ClassName, childUnionName, numYear, pupil.TypeBudget.DictTypeBudgetId == 0 ? (int?)null : pupil.TypeBudget.DictTypeBudgetId, childUnionid);
                    
                }
            }
        }
        return pupils;
    }
    */
    protected void selectUdod_OnChange(object sender, EventArgs e)
    {
        // перевод выбранных учеников в ДО
        /*popupChangeDOExtender.Hide();
        if (selectUdod.UdodAgeGroupId.HasValue)
        {
            using (PupilsDb db = new PupilsDb())
            {
                var list = db.GetSchoolPupilsPaging(UserContext.UdodId.Value, "", "", "","", false, null, null);
                list = list.Where(i => i.Checked).ToList();
                int countZach = 0;
                foreach (var schoolPupil in list)
                {
                    int result = db.SchoolAddPupilInAgegroup(schoolPupil.UserId, selectUdod.UdodAgeGroupId.Value);
                    if (result == 0) countZach++;
                }
                if (list.Count==countZach)
                lblInfo.Text = string.Format("Операция зачисления успешно завершена. Зачислено {0} из {1} записей.", countZach, list.Count);
                else lblInfo.Text = string.Format("Операция зачисления завершена частично. Зачислено {0} из {1} записей. Возникшие ошибки отображены в колонке 'Ошибки'. Устраните возникшие ошибки и повторите процедуру зачисления.", countZach, list.Count);

            }
        }
        gvPupilList.DataBind();
        upPupilList.Update();*/
    }

    protected void lnkAddPupil_OnClick(object sender, EventArgs e)
    {
        // Добавление ученика вручную

        #region Очистка полей
        PupilFio.Clear();
        PupilDocument.Clear();
        /*ParentFio.Clear();
        parentDocument.Clear();
        tbParentEmail.Text = "";
        tbParentPhoneNumber.Text = "";
        tbParentExpressPhoneNumber.Text = "";*/
        PupilDocument.DataBind();
        tbBirthday.Text = "";
        tbNumYear.Text = "";
        tbPupilClass.Text = "";
        tbPupilSchool.Text = "";
        tbPupilEmail.Text = "";
        tbPupilPhoneNumber.Text = "";
        tbChildUnionName.Text = "";
        RegAddress.Code = "";
        RegAddress.NumberHouse = "";
        RegAddress.Flat = "";
        IsEdit = null;
        lblErrorLoad.Text = "";
        lblAddressStr.Text = "";
        #endregion

        PupilId = null;
        PupilFio.Enabled =
                PupilDocument.Enabled =
                tbBirthday.Enabled =
                tbPupilSchool.Enabled =
                cbSex.Enabled =
                tbPupilClass.Enabled = true;
        tbBirthday.Enabled =
                tbPupilSchool.Enabled =
                tbPupilClass.Enabled =
                tbPupilPhoneNumber.Enabled= 
                tbPupilEmail.Enabled=
                cbSex.Enabled = false;

        btnSavePupil.Visible = false;
        upSavePupil.Update();
        RegAddress.Databind();
        upAddresses.Update();
        popupEditPupilExtender.Show();
        upPupilInfo.Update();
        //upParentInfo.Update();
    }

    protected void btnSavePupil_OnClick(object sender, EventArgs e)
    {
        try
        {
            // сохранение ученика при редактировании или создании
            //if (selectUdod.UdodAgeGroupId.HasValue)
            {
                using (PupilsDb db = new PupilsDb())
                {
                    
                /*int numyear;
                try
                {
                    numyear = Convert.ToInt32(ddlNumYear.SelectedValue);
                }
                catch (Exception)
                {
                    numyear = 1;
                }
                */
                    /*Guid parentId = db.ImportedAddParent(null, ParentFio.LastName, ParentFio.FirstName, ParentFio.MiddleName,
                                                     parentDocument.PassportType, parentDocument.Series,
                                                     parentDocument.Number,
                                                     parentDocument.IssueDate.HasValue
                                                         ? parentDocument.IssueDate.Value
                                                         : DateTime.Now, tbParentEmail.Text, tbParentPhoneNumber.Text);*/
                    //long childUnionId = selectUdod.UdodAgeGroupId.Value;
                    Guid pupilId = db.SchoolAddPupil(PupilId, Guid.Empty, UserContext.UdodId.Value, PupilFio.LastName,
                                                     PupilFio.FirstName,
                                                     PupilFio.MiddleName,
                                                     Convert.ToDateTime(tbBirthday.Text),
                                                     Convert.ToBoolean(cbSex.SelectedIndex), PupilDocument.PassportType,
                                                     PupilDocument.Series, PupilDocument.Number,
                                                     PupilDocument.IssueDate.HasValue
                                                         ? PupilDocument.IssueDate.Value
                                                         : DateTime.Now,
                                                     RegAddress.Code, "", RegAddress.NumberHouse,
                                                     RegAddress.Fraction, RegAddress.Housing, RegAddress.Building,
                                                     RegAddress.Flat, RegAddress.AddressTypeId,
                                                     RegAddress.typeAddressCode, "",
                                                     0, 0, tbPupilEmail.Text, tbPupilPhoneNumber.Text,
                                                     tbPupilSchool.Text, null, tbPupilClass.Text, tbChildUnionName.Text, 1, 2,
                                                     IsEdit, false, PupilReestrCode.HasValue ? PupilReestrCode.Value.ToString() : "", PupilReestrGuidCode);
                    PupilReestrCode = null;
                }
                popupEditPupilExtender.Hide();
                gvPupilList.DataBind();
                upPupilList.Update();
            }
        }
        catch (Exception e1)
        {
            popupErrorSaveExtender.Show();
            upPupilInfo.Update();
        }
    }

    protected void dsPupilList_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        if (cityUdodFilter.SelectedUdod.HasValue)
        {
            using (PupilsDb db = new PupilsDb())
            {
                pgrPupilList.numElements = db.GetSchoolPupilsCount(cityUdodFilter.SelectedUdod.Value, 
                    tbPupilLastName.Text, tbPupilFirstName.Text, tbPupilMiddleName.Text, "", false, ddlClass.SelectedValue, Convert.ToInt32(cblIsSchool.SelectedValue), null, null);
            }
            //if (ddlDO.Items.Count == 0)
            {
                /*using (PupilsDb db = new PupilsDb())
                {
                    var list = db.SchoolGetChildUnions(UserContext.UdodId.Value);
                    ddlDO.DataSource = list;
                    ddlDO.DataBind();
                }*/
            }
            /*if (ddlNumYear.Items.Count == 0)
            {
                int[] numyears = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
                ddlNumYear.DataSource = numyears;
                ddlNumYear.DataBind();
            }*/
            upFilters.Update();

            /*if (cbxShowDeleted.Checked)
            {
                gvPupilList.Columns[gvPupilList.Columns.Count - 1].Visible = false;
                gvPupilList.Columns[gvPupilList.Columns.Count - 2].Visible = false;
                gvPupilList.Columns[gvPupilList.Columns.Count - 3].Visible = false;
                gvPupilList.Columns[gvPupilList.Columns.Count - 4].Visible = true;
                gvPupilList.Columns[gvPupilList.Columns.Count - 5].Visible = false;
            }
            else
            {
                gvPupilList.Columns[gvPupilList.Columns.Count - 1].Visible = true;
                gvPupilList.Columns[gvPupilList.Columns.Count - 2].Visible = true;
                gvPupilList.Columns[gvPupilList.Columns.Count - 3].Visible = true;
                gvPupilList.Columns[gvPupilList.Columns.Count - 4].Visible = false;
                gvPupilList.Columns[gvPupilList.Columns.Count - 5].Visible = true;
            }*/
            upPupilList.Update();
        }
        else
        {
            e.Cancel = true;
        }
    }

    protected void lnkSelectDO_OnClick(object sender, EventArgs e)
    {
        // загрузить выбор ДО
        selectUdod.SelectButtonText = "<div class='btnBlue'>Зачислить</div>";
        popupChangeDOExtender.Show();
        gvPupilList.DataBind();
        upPupilList.Update();
        upChangeDO.Update();
    }

    protected void gvPupilList_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType==DataControlRowType.DataRow)
        {
            // проверка на различные ошибки
            var item = (e.Row.DataItem as ExtendedImportedPupil);
            if (item != null)
            {
                e.Row.Cells[8].Text = item.ErrorMessage;
                LinkButton btn = (LinkButton) e.Row.FindControl("lnkDeletePupil");
                System.Web.UI.WebControls.CheckBox cbx = (System.Web.UI.WebControls.CheckBox)e.Row.FindControl("cbxSelect");

                btn.Attributes.Add("pupilId", item.UserId.ToString());
                cbx.Attributes.Add("pupilId", item.UserId.ToString());
                cbx.Checked = item.Checked;

                foreach (var childunion in item.ChildUnions)
                {
                    e.Row.Cells[7].Text += childunion.Name + "; ";
                }

                // проверка на документ в БД
                switch (item.Passport.PassportType.PassportTypeId)
                {
                    case 2:
                        {
                            // Проверка паспорта на соответствие формату
                            string message = "";
                            Regex regex = new Regex(@"\d{4}");
                            bool isRight = true;
                            if (!regex.IsMatch(item.Passport.Series) || item.Passport.Series.Length!=4)
                            {
                                message += "Серия паспорта не сооветствует формату.";
                                isRight = false;
                            }
                            regex = new Regex(@"\d{6}");
                            if (!regex.IsMatch(item.Passport.Number) || item.Passport.Number.Length != 6)
                            {
                                message += "Номер паспорта не сооветствует формату.";
                                isRight = false;
                            }
                            if (!isRight) e.Row.Cells[8].Text += "<div class='ImportedErrorMessage' title='" + message + "'> Ошибка формата паспорта;</div>";
                            break;
                        }
                    case 3:
                        {
                            // Проверка свидетельства о рождении на соответствие формату
                            string message = "";
                            Regex regex = new Regex(@"[IVXLCDMivxlcd]{1,3}-[А-Яа-я][А-Яа-я]");
                            bool isRight = true;
                            if (!regex.IsMatch(item.Passport.Series))
                            {
                                message += "Серия свидетельства не сооветствует формату. Формат серии свидетельства от 1 до 3 римских цифр IVXLCDM, затем тире и 2 буквы русского алфавита. Пример: IV-МЮ ";
                                isRight = false;
                            }
                            regex = new Regex(@"\d{6}");
                            if (!regex.IsMatch(item.Passport.Number) || item.Passport.Number.Length != 6)
                            {
                                message += "Номер свидетельства не сооветствует формату. Формат номера свидетельства 6 цифр.";
                                isRight = false;
                            }
                            if (!isRight) e.Row.Cells[8].Text += "<div class='ImportedErrorMessage' title='" + message + "'> Ошибка формата свидетельства;</div>";
                            break;
                        }
                    default: break;
                }
                using (UserDb db = new UserDb())
                {
                    if (db.CheckExistsingPassport(item.LastName, item.FirstName, item.Passport.PassportType.PassportTypeId, item.Passport.Series,item.Passport.Number, null))
                    {
                        /*var list = db.ImportedGetPupilUdods(item.Passport.PassportType.PassportTypeId,
                                                            item.Passport.Series,
                                                            item.Passport.Number);
                        if (list.Count > 0)
                        {
                            {
                                // готовить сообщение об ошибке
                                StringBuilder sb = new StringBuilder();
                                sb.Append("Данный документ существует у следующих пользователей ");
                                foreach (var pupilUdod in list)
                                {
                                    sb.Append(pupilUdod.LastName+" "+pupilUdod.FirstName+" "+pupilUdod.MiddleName +" "+ pupilUdod.UdodName+", номер телефона "+pupilUdod.UdodPhoneNumber);
                                }
                                sb.Append(". Просьба связатся с с указанным(-ыми) УДО, определиться с чьей стороны допущена ошибка и внести необходимые коррективы.");
                                string message = "<div class='ImportedErrorMessage' title='" + sb.ToString() + "'> Ошибка паспорта; </div>";
                                e.Row.Cells[8].Text += message;
                            }
                        }*/
                        var list = db.FindUdodByPassport(item.Passport.PassportType.PassportTypeId,item.Passport.Series,item.Passport.Number);
                        StringBuilder sb = new StringBuilder();
                        sb.Append("Введённые Вами данные документа удостоверяющего личность уже были использованы: \n");
                        foreach (ExtendedUserPassportMessage user in list)
                        {
                            if (user.ParentFio == "")
                            {
                                sb.Append("- в документе обучающегося " + user.ChildFio);
                            }
                            else
                            {
                                sb.Append("- в документе " + user.ParentFio + ", являющегося представителем " + user.ChildFio);
                            }

                            foreach (ExtendedUdodPassportMessage udod in user.Udods)
                            {
                                if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                {
                                    sb.Append(", в заявлении(-ях) №");

                                    foreach (var n in udod.Claims.Where(j => j.ClaimId != 0)) // Вывести список этих ClaimId
                                    {
                                        sb.Append(n.Number.ToString() + ", ");
                                    }
                                    sb.Remove(sb.Length - 2, 2);
                                }
                                if (udod.Claims.Count(j => j.ClaimId == 0) > 0)
                                {
                                    if (udod.Claims.Count(j => j.ClaimId > 0) > 0)
                                    {
                                        sb.Append(", также являющимся обучающимся по программам дополнительного образования ");
                                    }
                                    else
                                    {
                                        sb.Append(", являющимся обучающимся по программам дополнительного образования ");
                                    }
                                }
                                sb.Append(" в учреждении " + udod.ShortName + ", телефон " + udod.PhoneNumber + ", ");
                                sb.Remove(sb.Length - 2, 2);
                            }
                            sb.Append(";\n");
                        }
                        sb.Remove(sb.Length - 2, 2);
                        sb.Append(". \nВам необходимо связаться с указанным(-ыми) УДО, определиться с чьей стороны допущена ошибка и внести необходимые коррективы.");
                        string message = "<div class='ImportedErrorMessage' title='" + sb.ToString() + "'> Ошибка документа; </div>";
                        e.Row.Cells[8].Text += message;
                    }
                }
                // Проверка на адрес
                if (item.Addresses.Count>0 && string.IsNullOrEmpty(item.Addresses.FirstOrDefault().AddressStr))
                {
                    string message = "<div class='ImportedErrorMessage' title='Некоторые поля детализированного адреса не соответствуют справочнику БТИ г.Москвы. Откройте запись на редактирование и устраните ошибку.'> Ошибка адреса; </div>";
                    e.Row.Cells[8].Text += message;
                }
                /*
                if (item.NumYear == 0)
                {
                    string message = "<div class='ImportedErrorMessage' title='Для успешной загрузки данных необходимо цифрой указать год обучения'> Не указан год обучения; </div>";
                    e.Row.Cells[8].Text += message;
                }*/
                /*if (item.NumYear == 1)
                {
                    string message = "<div class='ImportedErrorMessage' title='Зачисление на 1й год обучения не может быть осуществлено через загрузку данных из файла. Удалите запись из списка или откорректируйте год обучения.'> Ошибка года обучения; </div>";
                    e.Row.Cells[8].Text += message;
                }*/
                /*if (item.NumYear > 11)
                {
                    string message = "<div class='ImportedErrorMessage' title='Указанный год обучения превышает предельно допустимые в Системе значения. Откорректируйте год обучения.'> Ошибка года обучения; </div>";
                    e.Row.Cells[8].Text += message;
                }*/
                // проверка на существование вида услуг
                /*if (item.TypeBudget.DictTypeBudgetId==0)
                {
                    string message = "<div class='ImportedErrorMessage' title='Не указан или некорректно указан вид получаемой образовательной услуги (платно, бесплатно).'> Вид услуги; </div>";
                    e.Row.Cells[8].Text += message;
                }*/
                /*
                using (PupilsDb db = new PupilsDb())
                {
                    //db.SchoolCheckPupil(item.UserId, e.Row.Cells[8].Text == item.ErrorMessage);
                }*/
                cbx.Enabled = e.Row.Cells[8].Text==item.ErrorMessage;
                if (cbx.Enabled == false) cbx.Checked = false;
            }
        }
    }

    protected void lnkDeletePupil_OnClick(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton) sender;
        Guid userId = new Guid(btn.Attributes["pupilId"]);
        using (PupilsDb db = new PupilsDb())
        {
            db.DeleteSchoolPupil(userId);
        }
        gvPupilList.DataBind();
        upPupilList.Update();
        
    }

    protected void cbxSelect_OnCheckedChanged(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.CheckBox cbx = (System.Web.UI.WebControls.CheckBox) sender;
        Guid userId = new Guid(cbx.Attributes["pupilId"]);
        using (PupilsDb db = new PupilsDb())
        {
            db.SchoolCheckPupil(userId, cbx.Checked);
        }
    }

    protected void ddlNumYear_OnDataBound(object sender, EventArgs e)
    {
        //ddlNumYear.Items.Insert(0, "");
    }

    protected void ddlDO_OnDataBound(object sender, EventArgs e)
    {
        //ddlDO.Items.Insert(0, new ListItem("Все",""));
    }

    protected void ddlDO1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDO1.SelectedValue != "")
        {
            lnkZach.Visible = true;
            using (ChildUnionDb db = new ChildUnionDb())
            {
                ExtendedChildUnion childunion = db.GetChildUnion(Convert.ToInt64(ddlDO1.SelectedValue), null);
                if (childunion != null)
                {
                    using (AgeGroupDb db2= new AgeGroupDb())
                    {
                        var list = db2.GetGroupsPaging(UserContext.UdodId, "", childunion.ChildUnionId, null, null, null,
                                                       "", 1, 50000, true);
                        ddlGroups.DataSource = list;
                        ddlGroups.DataBind();
                    }

                    ddlGroups_OnSelectedIndexChanged(ddlGroups, new EventArgs());
                    /*
                    ddlNumYearFilter.Items.Clear();
                    for (int i = 1; i <= childunion.NumYears; i++)
                    {
                        ddlNumYearFilter.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }*/
                    var teachers = db.GetTeacher(childunion.ChildUnionId);
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Список педагогов: ");
                    foreach (var teacher in teachers)
                    {
                        sb.Append(teacher.TeacherName + "; ");
                    }
                    sb.Append("<br/>Возраст обучающихся: от " + childunion.AgeStart.ToString() + " до " +
                              childunion.AgeEnd +
                              "; ");
                    sb.Append("<br/>Адреса:");
                    foreach (var extendedAddress in childunion.Addresses)
                    {
                        sb.Append(extendedAddress.AddressStr + "; ");
                    }
                    lblChildUnionInfo.Text = sb.ToString();

                }
            }
        }
        else
        {
            lnkZach.Visible = false;
        }
        upFilters.Update();
        upLnkZach.Update();
    }

    protected void ddlTypeBudgetFilter_OnDataBound(object sender, EventArgs e)
    {
        //ddlTypeBudgetFilter.Items.Insert(0, new ListItem("Все",""));
    }

    protected void lnkZach_OnClick(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlNumYearFilter.SelectedValue) && !string.IsNullOrEmpty(ddlGroups.SelectedValue))
        {
            // перевод выбранных учеников в ДО
            popupChangeDOExtender.Hide();
            //if (selectUdod.UdodAgeGroupId.HasValue)
            {
                using (PupilsDb db = new PupilsDb())
                {
                    var list = db.GetSchoolPupilsPaging(UserContext.UdodId.Value, "", "", "", "", false, "",
                                                        Convert.ToInt32(cblIsSchool.SelectedValue), null, null);
                    list = list.Where(i => i.Checked).ToList();
                    int countZach = 0;
                    using (AgeGroupDb db1 = new AgeGroupDb())
                    {
                        var group = db1.GetGroup(Convert.ToInt32(ddlGroups.SelectedValue));

                        if (list.Count>group.MaxCountPupil-group.CurrentCountPupil)
                        {
                            errorPopup.ShowError("Ошибка", "Количество свободных мест в группе меньше чем количество выделенных обучающихся", "", "ОК", false, true);
                        }
                        else
                        {
                            foreach (var schoolPupil in list)
                            {
                                Guid result = db.SchoolAddPupilInAgegroup(schoolPupil.UserId,
                                                                          Convert.ToInt64(ddlDO1.SelectedValue),
                                                                          Convert.ToInt32(ddlNumYearFilter.SelectedValue), group.UdodAgeGroupId);
                                if (result != Guid.Empty)
                                {
                                    countZach++;
                                    EventsUtility.LogEvent(EnumEventTypes.SchoolZach, UserContext.Profile.UserId,
                                                           Request.UserHostAddress,
                                                           string.Format("Зачислен, как обучавшийся до создания Системы"),
                                                           result, null, UserContext.UdodId,
                                                           Convert.ToInt64(ddlDO1.SelectedValue));
                                }
                            }

                            errorPopup.ShowError("Сообщение",
                                                 string.Format(
                                                     "Операция зачисления успешно завершена. Зачислено {0} из {1} записей.",
                                                     countZach, list.Count), "", "ОК", false, true);
                        }
                        /*if (list.Count == countZach)
                {
                    lblInfo.Text = string.Format("Операция зачисления успешно завершена. Зачислено {0} из {1} записей.", countZach, list.Count);
                    lblInfo.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblInfo.Text = string.Format("Операция зачисления завершена частично. Зачислено {0} из {1} записей. Возникшие ошибки отображены в колонке 'Ошибки'. Устраните возникшие ошибки и повторите процедуру зачисления.", countZach, list.Count);
                    lblInfo.ForeColor = System.Drawing.Color.Red;
                }
                */
                    }
                }
            }
            gvPupilList.DataBind();
            upPupilList.Update();
        }
    }

    protected void ddlDO1_OnDataBound(object sender, EventArgs e)
    {
        ddlDO1.Items.Insert(0, new ListItem("Все", ""));
        ddlDO1_OnSelectedIndexChanged(sender, e);
    }

    protected void lnkLoadFromReestr_OnClick(object sender, EventArgs e)
    {
        ////ExtendedPupil pupil = SiteUtility.GetPupilData(Page.MapPath("../Templates/search_by_ReestrCode_rq.xml"), PupilReestrCode);
        ////try
        ////{
        ////    if (string.IsNullOrEmpty(pupil.ReestrCode))
        ////        pupil = (SiteUtility.GetPupilData(Page.MapPath(PupilDocument.PassportType == 2 ? "../Templates/search_Passport_by_Series_and_Number_rq.xml" : "../Templates/search_by_Series_and_Number_rq.xml"), PupilDocument.Series,
        ////                                 PupilDocument.Number)).FirstOrDefault(i => i.LastName.Trim().ToUpper() == PupilFio.LastName.Trim().ToUpper());
        ////    var tmp = Convert.ToInt64(pupil.ReestrCode);
        ////}
        ////catch (Exception)
        ////{
        //    pupil = new ExtendedPupil();
        ////}
        //tbPupilPhoneNumber.Enabled =
        //    tbPupilEmail.Enabled = true;
        //lblErrorLoad.Text = "";        
        //lblAddressStr.Text = pupil.AddressStr;
        //if (string.IsNullOrEmpty(pupil.LastName))
        //{
        //    /*lblErrorLoad.Text =
        //        "По введённым параметрам в Реестре не найдено ни одной записи. Проверьте правильность заполнения и повторите запрос. Если Вы уверены в корректности введённых данных, то можете создать запись для данного обучающегося вручную. Создать запись в ручную?";*/
        //    popupErrorExtender.Show();
        //} else
        //if ((PupilFio.LastName.Trim().ToLower() != pupil.LastName.Trim().ToLower() || PupilFio.FirstName.Trim().ToLower() != pupil.FirstName.Trim().ToLower()))
        //{
        //    // Ошибка
        //    lblErrorLoad.Text = "Полученные из Реестра данные отличаются от введённых. Проверьте правильность заполнения и повторите запрос. Если Вы уверены в корректности введённых данных - обратитесь в ОСИП для разрешения ситуации по данному обучающемуся.";
        //} 
        //else
        //{
        //    btnSavePupil.Visible = true;
        //    //PupilFio.LastName = pupil.LastName;
        //    //PupilFio.FirstName = pupil.FirstName;
        //    PupilFio.MiddleName = pupil.MiddleName;
        //    tbBirthday.Text = pupil.Birthday.ToShortDateString();
        //    tbPupilPhoneNumber.Text = pupil.PhoneNumber;
        //    tbPupilEmail.Text = pupil.EMail;
        //    tbPupilClass.Text = pupil.ClassName;
        //    cbSex.SelectedIndex = cbSex.Items.IndexOf(cbSex.Items.FindByValue(pupil.Sex.ToString()));

        //    string _url = Page.MapPath("../Templates/getUdodInfoByGuid.xml");
        //    bool isReadySchool = true;
        //    try
        //    {
        //        using (DictSchoolDb db = new DictSchoolDb())
        //        {
        //            var tmp = db.GetSchool(pupil.SchoolName);
        //            pupil.SchoolName = tmp.Name;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        isReadySchool = false;
        //    }
            
        //    if (!isReadySchool)
        //    {
        //        ExtendedUdod reestrData;
        //        reestrData = SiteUtility.GetUdodData(_url, pupil.SchoolName);
        //        pupil.SchoolName = reestrData.ShortName;
                
        //    }

        //    tbPupilSchool.Text = pupil.SchoolName;
        //    PupilDocument.IssueDate = pupil.Passport.IssueDate;

        //    if (pupil.Addresses[0].BtiCode!=null || !string.IsNullOrEmpty(pupil.Addresses[0].KladrCode))
        //    {
        //        var regAddress = pupil.Addresses[0];
        //        RegAddress.IsBti = string.IsNullOrEmpty(regAddress.KladrCode);
        //        if (RegAddress.IsBti)
        //            regAddress.BtiCode = (new KladrDb()).GetStreetCodeByHouseCode(regAddress.BtiCode.Value);
        //        RegAddress.Code = string.IsNullOrEmpty(regAddress.KladrCode) ? regAddress.BtiCode.Value.ToString() : regAddress.KladrCode;
        //        RegAddress.Databind();
        //        RegAddress.Index = regAddress.PostIndex;
        //        RegAddress.Housing = regAddress.Housing;
        //        RegAddress.NumberHouse = regAddress.HouseNumber;
        //        RegAddress.Fraction = regAddress.Fraction;
        //        RegAddress.Flat = regAddress.FlatNumber;
        //        RegAddress.Building = regAddress.Building;
        //    }

        //    PupilFio.Enabled =
        //        PupilDocument.Enabled =
        //        tbBirthday.Enabled =
        //        tbPupilSchool.Enabled =
        //        tbPupilClass.Enabled = 
        //        cbSex.Enabled= false;

        //    IsEdit = true;
        //}
        //upSavePupil.Update();
        //upEditPupil.Update();
        //upAddresses.Update();
    }

    protected void btnYesError_OnClick(object sender, EventArgs e)
    {
        PupilFio.Enabled = false;
        PupilDocument.Enabled = false;
        PupilFio.EnabledMiddleName = true;
        btnSavePupil.Visible = true;
        tbBirthday.Enabled =
                tbPupilSchool.Enabled =
                tbPupilClass.Enabled =
                cbSex.Enabled = true;
        upPupilInfo.Update();
        upSavePupil.Update();
        popupErrorExtender.Hide();
    }

    protected void btnNoError_OnClick(object sender, EventArgs e)
    {
        //popupLoadFileExtender.Hide();
    }

    protected void ddlNumYearFilter_OnDataBound(object sender, EventArgs e)
    {
        //ddlNumYearFilter.Items.Remove(ddlNumYearFilter.Items.FindByText("1"));
    }

    protected void lnkDeSelect_OnClick(object sender, EventArgs e)
    {
        using (PupilsDb db = new PupilsDb())
        {
            db.SchoolDeSelect(UserContext.UdodId.Value);
        }
        gvPupilList.DataBind();
        upPupilList.Update();
    }

    protected void lnkLoadSchool_OnClick(object sender, EventArgs e)
    {
        //string schoolName;
        //ExtendedUdod reestrData;
        //using (UdodDb db = new UdodDb())
        //{
            
        //    var udod = db.GetUdod(UserContext.UdodId.Value);
        //    schoolName = udod.Name;
        //    //if (!udod.ReestrId.HasValue)
        //    {
        //        int[] reestrId = db.GetReestrDogmId(UserContext.UdodId.Value);
        //        string _url = Page.MapPath("../Templates/getUdodInfo.xml");
        //        int? ReestrId = null;
        //        if (reestrId[1] != 0) ReestrId = reestrId[1];
        //        //reestrData = SiteUtility.GetUdodData(_url, reestrId[0], ReestrId);
        //        //reestrData = new ExtendedUdod();

        //        /*lblInfoService.Text = reestrData.Name + " " + reestrData.ShortName+ " "+ reestrData.JurAddress+ " "+reestrData.FioDirector
        //            +" "+reestrData.PhoneNumber+" "+ reestrData.EMail+ " "+reestrData.SiteUrl;
        //        */
        //        if (reestrData.ReestrId.HasValue)
        //        {
        //            db.SetReestrId(UserContext.UdodId.Value, reestrData.ReestrId.Value);
        //        }
                

                  
        //List<ExtendedPupil> list = SiteUtility.GetSchoolPupilsSecond(Page.MapPath("../Templates/GetContingentByReestrId1.xml"), UserContext.UdodId.Value,  reestrData.ReestrGuid);
            
        //using (PupilsDb db1 = new PupilsDb())
        //{
        //    db1.SchoolSetDeleted(UserContext.UdodId.Value);
        //    foreach (ExtendedPupil pupil in list)
        //    {
        //        //null, UserContext.UdodId.Value, pupil.LastName, pupil.FirstName, pupil.MiddleName, pupil.Birthday, Convert.ToBoolean(pupil.Sex), pupil.Passport.PassportType.PassportTypeId, pupil.Passport.Series,pupil.Passport.Number, pupil.Passport.IssueDate.HasValue?pupil.Passport.IssueDate.Value:DateTime.Now,
        //        //       address.BtiCode.HasValue ? address.BtiCode.Value.ToString() : address.KladrCode, "", address.HouseNumber, address.Fraction, address.Housing, address.Building, address.FlatNumber, 5, address.BtiCode.HasValue ? 0 : 1, pupil.AddressStr, 0, 0, pupil.EMail, pupil.PhoneNumber, pupil.SchoolName, null, pupil.ClassName, childUnionName, numYear, pupil.TypeBudget.DictTypeBudgetId == 0 ? (int?)null : pupil.TypeBudget.DictTypeBudgetId, childUnionid
        //        var address = pupil.Addresses.FirstOrDefault();
        //        if (address.BtiCode.HasValue)
        //            address.BtiCode = (new KladrDb()).GetStreetCodeByHouseCode(address.BtiCode.Value);
        //        if (!address.BtiCode.HasValue && string.IsNullOrEmpty(address.KladrCode))
        //        {
        //            address.KladrCode = "";
        //        }
        //        try
        //        {
        //            string tmp = new KladrDb().GetAddress(address.KladrCode, "", "", "", "", "", null).AddressStr;
        //        }
        //        catch
        //        {
        //            address.KladrCode = "";
        //        }
        //        int? typeaddressCode;
        //        if (address.BtiCode.HasValue) typeaddressCode = 0; else typeaddressCode = null;
        //        try
        //        {
        //        db1.SchoolAddPupil(null, Guid.Empty, UserContext.UdodId.Value, pupil.LastName, pupil.FirstName,
        //            pupil.MiddleName, pupil.Birthday < new DateTime(1900, 01, 01) ? DateTime.Now : pupil.Birthday, Convert.ToBoolean(pupil.Sex),
        //                          pupil.Passport.PassportType.PassportTypeId, pupil.Passport.Series,
        //                          pupil.Passport.Number,
        //                          pupil.Passport.IssueDate.HasValue ? pupil.Passport.IssueDate.Value : DateTime.Now,
        //                          address.BtiCode.HasValue?address.BtiCode.Value.ToString():address.KladrCode, "", address.HouseNumber, address.Fraction, address.Housing,
        //                          address.Building, address.FlatNumber, address.AddressType.AddressTypeId,
        //                          typeaddressCode, pupil.AddressStr, 0, 0, pupil.EMail, pupil.PhoneNumber,
        //                          schoolName, string.IsNullOrEmpty(pupil.SchoolCode)?udod.EkisId:Convert.ToInt32(pupil.SchoolCode), pupil.ClassName, "", 1, null, true, true, pupil.ReestrCode, pupil.ReestrGuid);
        //        }
        //        catch (Exception e1)
        //        {
        //            //errorPopup.ShowError("ds", e1.Message + pupil.LastName + " " + pupil.FirstName + " " + pupil.MiddleName, "", "OK", false, true);
                
        //        }    
        //    }
        //    db1.SchoolDeletePupils(UserContext.UdodId.Value);
        //}
        //}
                
        //}

        //gvPupilList.DataBind();
        //upPupilList.Update();
        //upLnk.Update();
    }

    protected void ddlClass_OnDataBound(object sender, EventArgs e)
    {
        ddlClass.Items.Insert(0, new ListItem("Все",""));
    }

    protected void cblIsSchool_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (cblIsSchool.SelectedIndex==0)
        {
            lnkAddPupil.Visible = false;
            ddlClass.Enabled = true;
            lnkLoadSchool.Visible = true;
            //lnkImportPupils.Visible = true;
        }
        else
        {
            lnkAddPupil.Visible = true;
            ddlClass.Enabled = false;
            lnkLoadSchool.Visible = false;
            //lnkImportPupils.Visible = false;
        }

        using (PupilsDb db = new PupilsDb())
        {
            ddlClass.DataSource = db.SchoolGetClasses(UserContext.UdodId.Value);
            ddlClass.DataBind();
            upFilters.Update();
        }

        //ddlClass.DataBind();
        upFilters.Update();
        upLnk.Update();
    }

    protected void lnkDisabledContingent_OnClick(object sender, EventArgs e)
    {
        if (UserContext.UdodId.HasValue)
        {
            (new UdodDb()).SetContingentEnabled(UserContext.UdodId.Value, false);
            Page.Response.Redirect(SiteUtility.GetUrl("~/default.aspx"));
        }
    }

    protected void ddlGroups_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlGroups.SelectedValue))
        {
            using (AgeGroupDb db = new AgeGroupDb())
            {
                var group = db.GetGroup(Convert.ToInt64(ddlGroups.SelectedValue));
                ddlNumYearFilter.Items.Clear();
                foreach (int year in group.NumYears)
                {
                    ddlNumYearFilter.Items.Add(new ListItem(year.ToString()));
                }
            }
            upFilters.Update();
            upLnkZach.Update();
        }
        
    }

    protected void lnkSendAll_OnClick(object sender, EventArgs e)
    {
        //    long[] ud = new long[] { 3265, 250, 1446, 1392, 780, 392, 1144, 1276, 1453, 1776, 1383, 413, 1158, 666, 1410, 1440, 1615, 292, 1336, 309, 896, 518, 748, 1562, 1358, 275, 707, 3239, 1285, 1232, 1008, 1325, 1283, 270, 1022, 419, 1196, 1168, 1663, 1225, 725, 533, 1152, 1315, 1055, 660, 1369, 504, 722, 1132, 314, 1201, 727, 1305, 1408, 704, 1013, 280, 1664, 1006, 738, 1609, 1020, 1282, 485, 1279, 1760, 1620, 255, 1183, 1348, 1293, 1180, 1009, 1570, 1192, 710, 915, 1667, 1185, 1244, 1052, 1280, 695, 1133, 1298, 706, 1149, 1136, 645, 719, 1326, 1118, 1538, 595, 1332, 697, 1157, 449, 1583, 2324, 1267, 1131, 950, 474, 1566, 1098, 2237, 1190, 1042, 3510, 1053, 263, 1335, 1145, 1592, 766, 339, 1291, 996, 1367, 1516, 1274, 1666, 1313, 770, 1596, 733, 1281, 1110, 261, 1593, 2495, 291, 1414, 1328, 1822, 1384, 264, 1584, 252, 1065, 1330, 461, 530, 732, 1515, 2648, 728, 544, 1015, 356, 993, 705, 1271, 1734, 1249, 2640, 1486, 1078, 1050, 1490, 1297, 337, 872, 871, 626, 1245, 282, 1915, 362, 1595, 972, 502, 803, 1529, 998, 1735, 294, 1881, 1107, 1339, 781, 1372, 1467, 470, 755, 1199, 1707, 894, 265, 1550, 1356, 475, 361, 1217, 1690, 3346, 1661, 898, 348, 276, 507, 1072, 1054, 1040, 2071, 973, 2856, 2729, 509, 1074, 731, 1114, 2974, 1355, 974, 3184, 1706, 1540, 1248, 2738, 1522, 3171, 642, 1535, 608, 1106, 1468, 1696, 2777, 940, 462, 1656, 1658, 283, 1246, 1488, 1462, 855, 1659, 1063, 870, 473, 1542, 273, 592, 455, 1216, 1108, 1657, 1686, 1463, 1066, 1695, 760, 1604, 1494, 1528, 1064, 1495, 1105, 1100, 347, 1470, 843, 481, 482, 1096, 360, 1691, 2212, 863 };
        //    List<long> udods = new List<long>(ud);
        //    foreach (var udodId in udods)
        //    {
        //        //UserContext.UdodId = udod;
        //        try
        //        {
        //            string schoolName;
        //            ExtendedUdod reestrData;
        //            using (UdodDb db = new UdodDb())
        //            {
        //                var udod = db.GetUdod(udodId);
        //                schoolName = udod.Name;
        //                //if (!udod.ReestrId.HasValue)
        //                int[] reestrId = db.GetReestrDogmId(udodId);
        //                string _url = Page.MapPath("../Templates/getUdodInfo.xml");
        //                int? ReestrId = null;
        //                if (reestrId[1] != 0) ReestrId = reestrId[1];
        //                //reestrData = SiteUtility.GetUdodData(_url, reestrId[0], ReestrId);


        //                List<ExtendedPupil> list =
        //                    SiteUtility.GetSchoolPupilsSecond(Page.MapPath("../Templates/GetContingentByReestrId1.xml"),
        //                                                      udodId, reestrData.ReestrGuid);

        //                using (PupilsDb db1 = new PupilsDb())
        //                {
        //                    db1.SchoolSetDeleted(udodId);
        //                    foreach (ExtendedPupil pupil in list)
        //                    {
        //                        //null, UserContext.UdodId.Value, pupil.LastName, pupil.FirstName, pupil.MiddleName, pupil.Birthday, Convert.ToBoolean(pupil.Sex), pupil.Passport.PassportType.PassportTypeId, pupil.Passport.Series,pupil.Passport.Number, pupil.Passport.IssueDate.HasValue?pupil.Passport.IssueDate.Value:DateTime.Now,
        //                        //       address.BtiCode.HasValue ? address.BtiCode.Value.ToString() : address.KladrCode, "", address.HouseNumber, address.Fraction, address.Housing, address.Building, address.FlatNumber, 5, address.BtiCode.HasValue ? 0 : 1, pupil.AddressStr, 0, 0, pupil.EMail, pupil.PhoneNumber, pupil.SchoolName, null, pupil.ClassName, childUnionName, numYear, pupil.TypeBudget.DictTypeBudgetId == 0 ? (int?)null : pupil.TypeBudget.DictTypeBudgetId, childUnionid
        //                        var address = pupil.Addresses.FirstOrDefault();
        //                        if (address.BtiCode.HasValue)
        //                            address.BtiCode = (new KladrDb()).GetStreetCodeByHouseCode(address.BtiCode.Value);
        //                        if (!address.BtiCode.HasValue && string.IsNullOrEmpty(address.KladrCode))
        //                        {
        //                            address.KladrCode = "";
        //                        }
        //                        try
        //                        {
        //                            string tmp =
        //                                new KladrDb().GetAddress(address.KladrCode, "", "", "", "", "", null).AddressStr;
        //                        }
        //                        catch
        //                        {
        //                            address.KladrCode = "";
        //                        }
        //                        int? typeaddressCode;
        //                        if (address.BtiCode.HasValue) typeaddressCode = 0;
        //                        else typeaddressCode = null;
        //                        db1.SchoolAddPupil(null, Guid.Empty, udodId, pupil.LastName, pupil.FirstName,
        //                                          pupil.MiddleName, pupil.Birthday, Convert.ToBoolean(pupil.Sex),
        //                                          pupil.Passport.PassportType.PassportTypeId, pupil.Passport.Series,
        //                                          pupil.Passport.Number,
        //                                          pupil.Passport.IssueDate.HasValue
        //                                              ? pupil.Passport.IssueDate.Value
        //                                              : DateTime.Now,
        //                                          address.BtiCode.HasValue
        //                                              ? address.BtiCode.Value.ToString()
        //                                              : address.KladrCode, "", address.HouseNumber, address.Fraction,
        //                                          address.Housing,
        //                                          address.Building, address.FlatNumber, address.AddressType.AddressTypeId,
        //                                          typeaddressCode, pupil.AddressStr, 0, 0, pupil.EMail, pupil.PhoneNumber,
        //                                          schoolName, string.IsNullOrEmpty(pupil.SchoolCode) ? udod.EkisId : Convert.ToInt32(pupil.SchoolCode), pupil.ClassName, "", 1,
        //                                          null, true, true, pupil.ReestrCode, pupil.ReestrGuid);
        //                    }
        //                    db1.SchoolDeletePupils(udodId);
        //                }
        //            }
        //        }
        //        catch (Exception)
        //        {
        //        }

        //    }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class Udod_Schedule : BasePage
{
    protected Int64? ScheduleLessonId
    {
        set { ViewState["ScheduleLessonId"] = value; }
        get
        {
            if (ViewState["ScheduleLessonId"] == null) return null;
            return (Int64)ViewState["ScheduleLessonId"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsUdodEmployee || UserContext.Roles.IsOsip);
        if (!Page.IsPostBack)
        {
            lnkAddScheduleOk.Visible=lnkAddSchedule.Visible = !UserContext.Roles.IsOsip;
            FiltersDatabind();
            if (cityUdodFilter.SelectedUdod.HasValue)
            {
                ScheduleDatabind();
            }
            ddlDOFilter_OnSelectedIndexChanged(ddlDOFilter, new EventArgs());
            /*using (ChildUnionDb db = new ChildUnionDb())
            {
                
                ddlDO.DataSource = db.GetChildUnionsShort(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null, null, null, null, null, null, null, null, null, null, null, null, null).OrderBy(i => i.Name);

            }*/
            hf.Value = "true";
        }else
        hf.Value = "false";
    }

    protected void gvShedule_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        
        if (e.Row.RowType==DataControlRowType.Header)
        {
            e.Row.Cells[0].CssClass = "deleted";
            e.Row.Cells[1].CssClass = "deleted";
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ExtendedSchedule shedule = (ExtendedSchedule)e.Row.DataItem;
            bool tmp = shedule.MaxLessonsInDay != 0;
            if (tmp)
            {
                e.Row.Cells[0].RowSpan = shedule.MaxLessonsInDay;
                e.Row.Cells[1].RowSpan = shedule.MaxLessonsInDay;
            }
            else
            {
                e.Row.Cells[0].CssClass = "deleted";
                e.Row.Cells[1].CssClass = "deleted";
            }

            // так же необходимо добавить кнопку удаления и редактирования, только там где что нибудь есть
            foreach (var weekday in shedule.WeekDays)
            {
                var cell = e.Row.Cells[weekday.Weekday*2 + 1];
                // добавить кнопку редактирования и удаления
                cell.Attributes.Add("ScheduleLessonId", weekday.ScheduleLessonId.ToString());
                //LinkButton lnkEdit = (LinkButton) e.Row.FindControl("lnkEdit" + weekday.Weekday.ToString());

                //lnkEdit.Attributes.Add("ScheduleLessonId", weekday.ScheduleLessonId.ToString());
                var lnkDelete = (LinkButton)e.Row.FindControl("lnkDelete" + weekday.Weekday.ToString());
                //lnkDelete.Attributes.Add("ScheduleLessonId", weekday.ScheduleLessonId.ToString());
                lnkDelete.Visible = !UserContext.Roles.IsOsip;
                // добавить атрибут
                /*lnkEdit.Attributes.Add("ScheduleLessonId", weekday.SheduleLessonId.ToString());
                lnkEdit.Click += new EventHandler(lnkEdit1_Click);
                lnkEdit.Text = "<img width='15px' height='15px' src='../Images/Edit.png' border=0 title='Редактирование'>";
                lnkEdit.ID = "lnkEdit";
                cell.Controls.Add(lnkEdit);*/
                
            }
            
            
            
        }
    }

    protected void lnkEdit1_Click(object sender, EventArgs e)
    {
        //ScheduleDatabind();
        LinkButton btn = (LinkButton) sender;
        
        //ScheduleLessonId = Convert.ToInt64(btn.Attributes["ScheduleLessonId"]);
        ScheduleLessonId = Convert.ToInt64((btn.Parent as TableCell).Attributes["ScheduleLessonId"]);
        using(ScheduleDb db = new ScheduleDb())
        {
            var schedulelesson = db.GetSheduleLesson(ScheduleLessonId.Value);
            ddlDO.SelectedIndex = ddlDO.Items.IndexOf(ddlDO.Items.FindByValue(schedulelesson.ChildUnionId.ToString()));
            ddlDO_OnSelectedIndexChanged(ddlDO,new EventArgs());
            ddlGroup.SelectedIndex = ddlGroup.Items.IndexOf(ddlGroup.Items.FindByValue(schedulelesson.GroupId.ToString()));
            ddlTeacher.SelectedIndex = ddlTeacher.Items.IndexOf(ddlTeacher.Items.FindByValue(schedulelesson.TeacherId.ToString()));
            ddlSection.SelectedIndex = ddlSection.Items.IndexOf(ddlSection.Items.FindByValue(schedulelesson.SectionId.ToString()));
            tbStartTime.Text = schedulelesson.StartTime.ToShortTimeString();
            tbEndTime.Text = schedulelesson.EndTime.ToShortTimeString();
            tbEndRecess.Text = schedulelesson.EndRecess.ToShortTimeString();
            tbComment.Text = schedulelesson.Comment;
            ddlWeekDay.SelectedIndex = ddlWeekDay.Items.IndexOf(ddlWeekDay.Items.FindByValue(schedulelesson.Weekday.ToString()));
            ddlGroup_OnSelectedIndexChanged(ddlGroup, new EventArgs());
            if (trPupils.Visible)
            {
                var list = db.ScheduleLessonGetPupils(schedulelesson.ScheduleLessonId);

                foreach (ListItem item in cblPupils.Items)
                {
                    item.Selected = list.Count(i => i.UserId.ToString() == item.Value) > 0;
                }
            }
            upAddSchedule.Update();
            //schedulelesson.ChildUnionId
        }
        hf.Value = "true";
        //ScheduleDatabind();
        //обработка редактирования
        popupAddScheduleExtender.Show();
    }

    protected string GetLessonTime(object dataItem, int weekDay)
    {
        ExtendedSchedule schedule = (ExtendedSchedule) dataItem;
        var scheduleLesson = schedule.WeekDays.Where(i => i.Weekday == weekDay).FirstOrDefault();
        if (scheduleLesson!=null)
        {
            return string.Format("{0}-{1}", scheduleLesson.StartTime.ToShortTimeString(), scheduleLesson.EndTime.ToShortTimeString());
        }
        return "";
    }
    protected string GetLessonFioSubject(object dataItem, int weekDay)
    {
        ExtendedSchedule schedule = (ExtendedSchedule)dataItem;
        var scheduleLesson = schedule.WeekDays.Where(i => i.Weekday == weekDay).FirstOrDefault();
        if (scheduleLesson != null)
        {
            return string.Format("{0}, {1} {2}", scheduleLesson.Fio, scheduleLesson.SectionName, cbComment.Checked ? scheduleLesson.Comment : "");
        }
        return "";
    }

    protected void lnkAddSchedule_OnClick(object sender, EventArgs e)
    {
        ScheduleLessonId = null;
        tbComment.Text = tbEndRecess.Text = tbEndTime.Text = tbStartTime.Text = "";
        //FiltersDatabind();
        ddlDO.SelectedIndex = ddlDO.Items.IndexOf(ddlDO.Items.FindByValue(ddlDOFilter.SelectedValue));
        ddlGroup.SelectedIndex = ddlGroup.Items.IndexOf(ddlGroup.Items.FindByValue(ddlGroupFilter.SelectedValue));
        ddlDO_OnSelectedIndexChanged(ddlDO, new EventArgs());
        ddlGroup_OnSelectedIndexChanged(ddlGroup, new EventArgs());
        hf.Value = "true";
        //ScheduleDatabind();
        upAddSchedule.Update();
        popupAddScheduleExtender.Show();
    }

    protected void lnkAddScheduleOk_OnClick(object sender, EventArgs e)
    {
        DateTime starttime = Convert.ToDateTime(tbStartTime.Text);
        DateTime endtime = Convert.ToDateTime(tbEndTime.Text);
        DateTime endRecess = Convert.ToDateTime(tbEndRecess.Text);
        if (starttime > endtime || starttime > endRecess || endtime > endRecess)
        {
            ucError.ShowError("Ошибка", "Время начала занятия не должно превышать время окончания занятия или время окончания перемены", "Ok", "", true, false);
            //upScheduleList.Update();
        }
        else
        {
            using (ScheduleDb db = new ScheduleDb())
            {
                var childUnionId = Convert.ToInt64(ddlDO.SelectedValue);
                var groupId = Convert.ToInt64(ddlGroup.SelectedValue);
                var teacherId = new Guid(ddlTeacher.SelectedValue);
                if (ScheduleLessonId.HasValue)
                {
                    var oldScheduleLesson = db.GetSheduleLesson(ScheduleLessonId.Value);
                    // изменение
                    string message = "";
                    if (childUnionId != oldScheduleLesson.ChildUnionId)
                        message += " Код ДО:" + oldScheduleLesson.ChildUnionId + "->" + childUnionId;
                    if (groupId != oldScheduleLesson.GroupId)
                        message += " Код группы:" + oldScheduleLesson.GroupId + "->" + groupId;
                    if (teacherId != oldScheduleLesson.TeacherId)
                        message += " Педагог:" + oldScheduleLesson.Fio + "->" + ddlTeacher.SelectedItem.Text;
                    if (Convert.ToInt32(ddlWeekDay.SelectedValue) != oldScheduleLesson.Weekday)
                        message += " День недели:" + oldScheduleLesson.Weekday + "->" + ddlTeacher.SelectedValue;
                    if (tbStartTime.Text != oldScheduleLesson.StartTime.ToShortTimeString())
                        message += " Время начала:" + oldScheduleLesson.StartTime.ToShortTimeString() + "->" +
                                   tbStartTime.Text;
                    if (tbEndTime.Text != oldScheduleLesson.EndTime.ToShortTimeString())
                        message += " Время окончания:" + oldScheduleLesson.EndTime.ToShortTimeString() + "->" +
                                   tbEndTime.Text;
                    if (tbEndRecess.Text != oldScheduleLesson.EndRecess.ToShortTimeString())
                        message += " Время окончания перемены:" + oldScheduleLesson.EndRecess.ToShortTimeString() + "->" +
                                   tbEndRecess.Text;
                    if (tbComment.Text.ToLower() != oldScheduleLesson.Comment.ToLower())
                        message += " Примечание:" + oldScheduleLesson.Comment + "->" + tbComment.Text;

                    if (db.UpdateSheduleLesson(ScheduleLessonId.Value, childUnionId, groupId, teacherId,
                                               Convert.ToInt32(ddlSection.SelectedValue),
                                               Convert.ToInt32(ddlWeekDay.SelectedValue),
                                               Convert.ToDateTime(tbStartTime.Text),
                                               Convert.ToDateTime(tbEndTime.Text), Convert.ToDateTime(tbEndRecess.Text),
                                               tbComment.Text) != -1)
                    {

                        if (trPupils.Visible)
                        {
                            // сохранение 
                            foreach(ListItem item in cblPupils.Items)
                            {
                                if (item.Selected)
                                {
                                    db.AddScheduleLessonPupil(ScheduleLessonId.Value, new Guid(item.Value));
                                }
                                else
                                {
                                    db.DeleteScheduleLessonPupil(ScheduleLessonId.Value, new Guid(item.Value));
                                }
                            }
                        }

                        EventsUtility.ScheduleLessonUpdate(UserContext.Profile.UserId, Request.UserHostAddress, message,
                                                           UserContext.UdodId, childUnionId);

                    #region Отправка в ЕСЗ расписания

                    #endregion
                    }
                    else
                    {
                        // сообщение об ошибке
                        ucError.ShowError("Ошибка", "Для данной группы уже существует пункт расписания с аналогичными параметрами", "", "Ок", false, true);
                    }
                }
                else
                {
                    long lessonId = db.AddSheduleLesson(childUnionId, groupId, teacherId,
                                                        Convert.ToInt32(ddlSection.SelectedValue),
                                                        Convert.ToInt32(ddlWeekDay.SelectedValue),
                                                        Convert.ToDateTime(tbStartTime.Text),
                                                        Convert.ToDateTime(tbEndTime.Text),
                                                        Convert.ToDateTime(tbEndRecess.Text),
                                                        tbComment.Text);
                    if (lessonId != -1)
                    {
                        string message = "";
                        message += " Код ДО:" + childUnionId;
                        message += " Код группы:" + groupId;
                        message += " Педагог:" + ddlTeacher.SelectedItem.Text;
                        message += " День недели:" + ddlTeacher.SelectedValue;
                        message += " Время начала:" + tbStartTime.Text;
                        message += " Время окончания:" + tbEndTime.Text;
                        message += " Время окончания перемены:" + tbEndRecess.Text;
                        message += " Примечание:" + tbComment.Text;
                        //Расписание добавлено. Записать в лог
                        EventsUtility.ScheduleLessonAdd(UserContext.Profile.UserId, Request.UserHostAddress, message,
                                                        UserContext.UdodId, childUnionId);

                        if (trPupils.Visible)
                        {
                            // сохранение 
                            foreach (ListItem item in cblPupils.Items)
                            {
                                if (item.Selected)
                                {
                                    db.AddScheduleLessonPupil(lessonId, new Guid(item.Value));
                                }
                                else
                                {
                                    db.DeleteScheduleLessonPupil(lessonId, new Guid(item.Value));
                                }
                            }
                        }
                    }
                    else
                    {
                        // сообщение об ошибке
                        ucError.ShowError("Ошибка", "Для данной группы уже существует пункт расписания с аналогичными параметрами", "Ok", "", true, false);
                    }
                }
                ScheduleDatabind();
                popupAddScheduleExtender.Hide();
            }
        }
    }

    protected void ddlDO_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        long childUnionId = Convert.ToInt64(ddlDO.SelectedValue);
        using (AgeGroupDb db = new AgeGroupDb())
        {
            ddlGroup.DataSource = db.GetGroupsPaging(null, null, childUnionId, null, null, null, null, 1, 1000, null).Where(i=>i.Name.ToUpper()!="АВТОГРУППА");
            ddlGroup.DataBind();
        }
        /*using (TeacherDb db= new TeacherDb())
        {
            var teachers = db.GetTeacherPaging(null, null, childUnionId, 1, 1000);
            ddlTeacher.DataSource = teachers;
            ddlTeacher.DataBind();
        }*/
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var childunion = db.GetChildUnion(childUnionId, null);

            var teachers = db.GetTeacher(childUnionId);
            ddlTeacher.DataSource = teachers;
            ddlTeacher.DataBind();
            

            List<ExtendedSection> sections = new List<ExtendedSection>();
            sections.Add(childunion.Section);
            ddlSection.DataSource = sections;
            ddlSection.DataBind();
        }
        hf.Value = "true";
        upScheduleList.Update();
    }

    protected void cityUdodFilter_OnCityChange(object sender, EventArgs e)
    {
        
    }

    protected void cityUdodFilter_OnUdodChange(object sender, EventArgs e)
    {
        FiltersDatabind();
        ScheduleDatabind();
    }

    protected void cityUdodFilter_OnloadComplete(object sender, EventArgs e)
    {
        
    }
    protected void FiltersDatabind()
    {
        if (cityUdodFilter.SelectedUdod.HasValue)
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                var list = db.GetChildUnionsShort(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null, null, null, null, null, null, null, null, null, null, null, null, null, null,false).OrderBy(i => i.Name);
                ddlDO.DataSource = list;
                ddlDOFilter.DataSource = list;

//                ddlTeacher.DataSource = db.GetTeacher(cityUdodFilter.SelectedCity, cityUdodFilter.SelectedUdod, null);
            }
        }
        else
        {
            ddlDO.DataSource = new ListItemCollection();
            ddlTeacher.DataSource = new ListItemCollection();
            ddlDOFilter.DataSource = new ListItemCollection();
        }
        ddlDOFilter.DataBind();
        ddlDO.DataBind();
        upAddSchedule.Update();
        upCityUdodFilter.Update();
        upFilter.Update();
        hf.Value = "false";
        upScheduleList.Update();
    }

    protected void ddlDOFilter_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        long? childunionId = null;
            try
            {
                childunionId = Convert.ToInt64(ddlDOFilter.SelectedValue);
            }
            catch (Exception)
            {
            }
        using (AgeGroupDb db = new AgeGroupDb())
        {
            var list = db.GetGroupsPaging(UserContext.UdodId, null, childunionId, null, null, null, "", 1, 5000, null);
            ddlGroupFilter.DataSource = list;
            ddlGroupFilter.DataBind();


        }
        if (childunionId.HasValue)
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                var teachers = db.GetTeacher(childunionId.Value);
                ddlTeacherFilter.DataTextField = "TeacherName";
                ddlTeacherFilter.DataSource = teachers;
                ddlTeacherFilter.DataBind();
            }
        }
        else
        {
            if (UserContext.UdodId.HasValue)
            {
                using (TeacherDb db = new TeacherDb())
                {
                    var teachers = db.GetTeacherPaging(null, UserContext.UdodId, null, 1, 5000);
                    ddlTeacherFilter.DataTextField = "Fio";
                    ddlTeacherFilter.DataSource = teachers;
                    ddlTeacherFilter.DataBind();
                }
            }
        }
        if (UserContext.UdodId.HasValue)
        {
            using (DictProgramDb db = new DictProgramDb())
            {
                var list = db.GetPrograms(UserContext.UdodId);
                ddlProgramFilter.DataSource = list;
                ddlProgramFilter.DataBind();
            }
        }
        upFilter.Update();
        ScheduleDatabind();
    }

    protected void ddlDOFilter_OnDataBound(object sender, EventArgs e)
    {
        if (ddlDOFilter.Items.Count==0)
        {
            ddlDOFilter.Items.Add(new ListItem("Выберите УДО"));
        }
    }

    protected void ScheduleDatabind()
    {
        using(ScheduleDb db = new ScheduleDb())
        {
            long? childUnionId = null;
            try
            {
                childUnionId = Convert.ToInt64(ddlDOFilter.SelectedValue);
            }
            catch (Exception)
            {
            }
            long? groupId = null;
            try
            {
                groupId = Convert.ToInt64(ddlGroupFilter.SelectedValue);
            }
            catch (Exception)
            {
            }
            int? programId = null;
            try
            {
                programId = Convert.ToInt32(ddlProgramFilter.SelectedValue);
            }
            catch (Exception)
            {
            }
            Guid? teacherId = null;
            try
            {
                teacherId = new Guid(ddlTeacherFilter.SelectedValue);
            }
            catch (Exception)
            {
            }
            if (childUnionId.HasValue)
            {
                var list = db.GetShedules(childUnionId, groupId, teacherId, programId, cbTimeRecess.Checked);
                gvShedule.DataSource = list;
            }
            else
            {
                gvShedule.DataSource = new List<ExtendedSchedule>();
            }
            gvShedule.DataBind();
            hf.Value = "true";
            upScheduleList.Update();
        }
    }


    protected void lnkDelete1_OnClick(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        //ScheduleLessonId = Convert.ToInt64(btn.Attributes["ScheduleLessonId"]);
        ScheduleLessonId = Convert.ToInt64((btn.Parent as TableCell).Attributes["ScheduleLessonId"]);
        using (ScheduleDb db = new ScheduleDb())
        {
            var scheduleLesson = db.GetSheduleLesson(ScheduleLessonId.Value);

            db.DeleteSheduleLesson(ScheduleLessonId.Value);
            EventsUtility.ScheduleLessonDelete(UserContext.Profile.UserId, Request.UserHostAddress, 
                string.Format("Код ДО:{0}, ФИО педагога: {1}, Предмет: {2}, День недели: {3}, Время начала/окончания занятий(перемены): {4}-{5}({6}), Примечание: {7}",
                scheduleLesson.ChildUnionId, scheduleLesson.Fio, scheduleLesson.SectionName, scheduleLesson.Weekday, 
                scheduleLesson.StartTime.ToShortTimeString(),scheduleLesson.EndTime.ToShortTimeString(),scheduleLesson.EndRecess.ToShortTimeString(), scheduleLesson.Comment), UserContext.UdodId);
        }
        ScheduleDatabind();
    }

    protected void lnkFindByName_OnClick(object sender, EventArgs e)
    {
        ScheduleDatabind();
    }

    protected void ddlGroupFilter_OnDataBound(object sender, EventArgs e)
    {
        DropDownList list = sender as DropDownList;
        if (list != null) list.Items.Insert(0, new ListItem("Все", ""));
    }

    protected void gvShedule_OnDataBinding(object sender, EventArgs e)
    {
        /*GridView gv = (GridView)sender;
        GridViewRow oGridViewRow = new GridViewRow(0, 0,
                DataControlRowType.Header, DataControlRowState.Insert);
        TableCell oTableCell = new TableCell();

        //Add Department
        oTableCell.Text = "Наименование ДО";
        oTableCell.RowSpan = 2;
        oGridViewRow.Cells.Add(oTableCell);
        //((GridViewRow)gv.Controls[0].Controls[0]).Cells.RemoveAt(0);
        oTableCell = new TableCell();
        oTableCell.Text = "Наименование группы";
        oTableCell.RowSpan = 2;
        oGridViewRow.Cells.Add(oTableCell);
        //((GridViewRow)gv.Controls[0].Controls[0]).Cells.RemoveAt(0);
        oTableCell = new TableCell();
        oTableCell.Text = "Понедельник";
        oTableCell.ColumnSpan = 2;
        oGridViewRow.Cells.Add(oTableCell);
        oTableCell = new TableCell();
        oTableCell.Text = "Вторник";
        oTableCell.ColumnSpan = 2;
        oGridViewRow.Cells.Add(oTableCell);
        oTableCell = new TableCell();
        oTableCell.Text = "Среда";
        oTableCell.ColumnSpan = 2;
        oGridViewRow.Cells.Add(oTableCell);
        oTableCell = new TableCell();
        oTableCell.Text = "Четверг";
        oTableCell.ColumnSpan = 2;
        oGridViewRow.Cells.Add(oTableCell);
        oTableCell = new TableCell();
        oTableCell.Text = "Пятница";
        oTableCell.ColumnSpan = 2;
        oGridViewRow.Cells.Add(oTableCell);
        oTableCell = new TableCell();
        oTableCell.Text = "Суббота";
        oTableCell.ColumnSpan = 2;
        oGridViewRow.Cells.Add(oTableCell);
        oTableCell = new TableCell();
        oTableCell.Text = "Воскресенье";
        oTableCell.ColumnSpan = 2;
        oGridViewRow.Cells.Add(oTableCell);
        oGridViewRow.Style.Add(HtmlTextWriterStyle.FontStyle, "bold");
        oGridViewRow.RowType = DataControlRowType.Header;
        gv.Controls[0].Controls.AddAt(0, oGridViewRow);*/
    }

    protected void gvShedule_OnDataBound(object sender, EventArgs e)
    {
        
    }

    protected void gvShedule_OnPreRender(object sender, EventArgs e)
    {
        
    }


    protected void cbTimeRecess_OnCheckedChanged(object sender, EventArgs e)
    {
        ScheduleDatabind();
    }

    protected void ddlGroupFilter_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ScheduleDatabind();
    }

    protected void ddlTeacherFilter_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ScheduleDatabind();
    }

    protected void ddlProgramFilter_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ScheduleDatabind();
    }

    protected void ddlGroup_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        // еслииндивидуальное то обновить и показать список детей
        if (!string.IsNullOrEmpty(ddlGroup.SelectedValue))
        {
            using (AgeGroupDb db = new AgeGroupDb())
            {
                var group = db.GetGroup(Convert.ToInt64(ddlGroup.SelectedValue));
                trPupils.Visible=group.EducationType.EducationTypeId == 3;
                if (trPupils.Visible)
                {
                    using (PupilsDb db1 = new PupilsDb())
                    {
                        var pupils = db1.GetGroupPupilsPaging(null, null, null, null, null, null, 1, 50000, null, null,
                                                              null,
                                                              group.UdodAgeGroupId);
                        cblPupils.DataSource = pupils;
                        cblPupils.DataBind();
                    }
                }
            }
        }
        hf.Value = "false";
        upAddSchedule.Update();
    }

    protected void cbComment_OnCheckedChanged(object sender, EventArgs e)
    {
        
    }
}
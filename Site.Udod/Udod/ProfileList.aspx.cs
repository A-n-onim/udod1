﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Udod_ProfileList : BasePage
{
    private bool isAdd
    {
        get
        {
            return (bool)ViewState["isAdd"];
        }
        set
        {
            ViewState["isAdd"] = value;
        }
    }

    private int SelectedItemParentValue
    {
        get
        {
            return (int)ViewState["SelectedItemParentValue"];
        }
        set
        {
            ViewState["SelectedItemParentValue"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsDepartment || UserContext.Roles.IsOsip);

        if (UserContext.Roles.RoleId==(int)EnumRoles.Osip)
        {
            gvProfileList.Columns[gvProfileList.Columns.Count - 1].Visible = 
                gvProfileList.Columns[gvProfileList.Columns.Count - 2].Visible = 
                lnkAddProfile.Visible = false;
        }
        if (!Page.IsPostBack)
        {
        }
    }

    protected void ProgramIndex_Changed(object sender, EventArgs e)
    {
        gvProfileList.DataBind();

        if (profileFilter.SelectedValue.HasValue)
        {
            lnkAddProfile.Visible = true;
        }
        else
        {
            lnkAddProfile.Visible = false;
        }
    }

    protected void AddProfile_Click(object sender, EventArgs e)
    {
        isAdd = true;

        litTitle.Text = "<span class='headerCaptions'>Добавление профиля</span>";
        tbProfileName.Text = "";
        popupAddExtender.Show();
        upAddProfile.Update();
    }

    protected void SaveProfile_Click(object sender, EventArgs e)
    {
        using (DictProfileDb db = new DictProfileDb())
        {
            if (isAdd)
                db.AddProfile(tbProfileName.Text, profileFilter.SelectedValue.Value);
            else
            {
                long id = Convert.ToInt64(gvProfileList.SelectedDataKey.Value);
                db.UpdateProfile(tbProfileName.Text, SelectedItemParentValue, id);
            }

            gvProfileList.DataBind();
        }
    }

    protected void gvProfileList_SelectedIndexChanged(object sender, EventArgs e)
    {
        isAdd = false;

        litTitle.Text = "<span class='headerCaptions'>Редактирование профиля</span>";
        long id = Convert.ToInt64(gvProfileList.SelectedDataKey.Value);
        using (DictProfileDb db = new DictProfileDb())
        {
            ExtendedProfile? profile = db.GetProfile(id, null);
            if (profile.HasValue)
            {
                tbProfileName.Text = profile.Value.Name;
                SelectedItemParentValue = profile.Value.ProgramId;
            }
        }
        popupAddExtender.Show();
        upAddProfile.Update();
    }
}
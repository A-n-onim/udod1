﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Udod_SectionList : BasePage
{
    private bool isAdd
    {
        get
        {
            return (bool)ViewState["isAdd"];
        }
        set
        {
            ViewState["isAdd"] = value;
        }
    }

    private int SelectedItemParentValue
    {
        get
        {
            return (int)ViewState["SelectedItemParentValue"];
        }
        set
        {
            ViewState["SelectedItemParentValue"] = value;
        }
    }

    protected string OrderBy
    {
        get
        {
            return (string)ViewState["OrderBy"];
        }
        set
        {
            ViewState["OrderBy"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator||UserContext.Roles.IsDepartment || UserContext.Roles.IsUdodEmployee);
        //lnkAddSection.Visible = UserContext.Roles.IsAdministrator||UserContext.Roles.IsDepartment;

        if (!Page.IsPostBack)
        {
            gvSectionList.Columns[gvSectionList.Columns.Count - 1].Visible =
                    gvSectionList.Columns[gvSectionList.Columns.Count - 2].Visible =
                    gvSectionList.Columns[gvSectionList.Columns.Count - 3].Visible = false;
            if (UserContext.Roles.RoleId==(int)EnumRoles.Administrator || UserContext.Roles.RoleId==(int)EnumRoles.Department)
            {
                gvSectionList.Columns[gvSectionList.Columns.Count - 3].Visible = UserContext.Roles.IsAdministrator;
                gvSectionList.Columns[gvSectionList.Columns.Count - 1].Visible =
                    gvSectionList.Columns[gvSectionList.Columns.Count - 2].Visible = true;
            }
            if (UserContext.Roles.RoleId==(int)EnumRoles.UdodEmployee )
            {
                gvSectionList.Columns[gvSectionList.Columns.Count - 3].Visible = true;
            }

        
            ddlNumItemsOnPage.Items.Add(new ListItem() { Text = "15", Value = "15" });
            ddlNumItemsOnPage.Items.Add(new ListItem() { Text = "30", Value = "30" });
            ddlNumItemsOnPage.Items.Add(new ListItem() { Text = "50", Value = "50" });
            ddlNumItemsOnPage.SelectedIndex = 0;

            if (UserContext.CityId.HasValue)
                cityUdodFilter.SelectedCity = UserContext.CityId.Value;

            if (UserContext.UdodId.HasValue)
                cityUdodFilter.SelectedUdod = UserContext.UdodId.Value;

            //gvSectionList.DataBind();
            //upGridView.Update();

            if (UserContext.UdodId.HasValue)
                gvSectionList.Columns[2].Visible = true;
            else
                gvSectionList.Columns[2].Visible = false;
        }
    }

    protected void ProfileIndex_Changed(object sender, EventArgs e)
    {
        SectionListDatabind();

        if ((sectionFilter.SelectedProfileValue.HasValue) && (UserContext.Roles.IsAdministrator||UserContext.Roles.IsDepartment))
        {
            lnkAddSection.Visible = true;
        }
        else
        {
            lnkAddSection.Visible = false;
        }

        upLnkAdd.Update();
    }

    protected void AddSection_Click(object sender, EventArgs e)
    {
        isAdd = true;

        litTitle.Text = "<span class='headerCaptions'>Добавление секции</span>";
        tbSectionName.Text = "";
        tbSectionShortName.Text = "";
        tbSectionLimitAge.Text = "0";
        popupAddExtender.Show();
        upAddSection.Update();
    }

    protected void SaveSection_Click(object sender, EventArgs e)
    {
        using (DictSectionDb db = new DictSectionDb())
        {
            if (isAdd)
                db.AddSection(tbSectionName.Text, tbSectionShortName.Text, Convert.ToInt32(tbSectionLimitAge.Text), sectionFilter.SelectedProfileValue.Value);
            else
            {
                long id = Convert.ToInt64(gvSectionList.SelectedDataKey.Value);
                db.UpdateSection(tbSectionName.Text, tbSectionShortName.Text, Convert.ToInt32(tbSectionLimitAge.Text), SelectedItemParentValue, id);
            }

            SectionListDatabind();
        }
    }

    protected void gvSectionList_SelectedIndexChanged(object sender, EventArgs e)
    {
        isAdd = false;

        litTitle.Text = "<span class='headerCaptions'>Редактирование секции</span>";
        int id = Convert.ToInt32(gvSectionList.SelectedDataKey.Value);
        using (DictSectionDb db = new DictSectionDb())
        {
            ExtendedSection section = db.GetSection(id);
            tbSectionName.Text = section.Name;
            tbSectionShortName.Text = section.ShortName;
            tbSectionLimitAge.Text = Convert.ToString(section.LimitAge);
            SelectedItemParentValue = section.ProfileId;
            
        }
        popupAddExtender.Show();
        upAddSection.Update();
    }

    protected void chkIsExist_CheckedChanged(object sender, EventArgs e)
    {
        using (SectionDb db = new SectionDb())
        {
            var chk = sender as CheckBox;

            bool isChecked = chk.Checked;

            var index = (chk.Parent.Parent as GridViewRow).RowIndex;

            var sectionId = (int)gvSectionList.DataKeys[index].Value;

            if (UserContext.UdodId.HasValue)
                db.UpdateUdodSection(UserContext.UdodId.Value, sectionId, isChecked);
        }

        //gvSectionList.DataBind();
    }

    protected void datasource_OnLoad(object sender, EventArgs e)
    {
        //datasource.SelectParameters["UdodId"].DefaultValue = UserContext.UdodId.Value.ToString();
    }

    protected void cityUdodFilter_OnUdodChange(object sender, EventArgs e)
    {
        sectionFilter.Refresh();
        up.Update();
        SectionListDatabind();

        if ((sectionFilter.SelectedProfileValue != null) && (UserContext.Roles.IsAdministrator||UserContext.Roles.IsDepartment))
        {
            lnkAddSection.Visible = true;
        }
        else
        {
            lnkAddSection.Visible = false;
        }

        if (UserContext.UdodId.HasValue)
            gvSectionList.Columns[2].Visible = true;
        else
            gvSectionList.Columns[2].Visible = false;

        upLnkAdd.Update();
    }

    protected void ds_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
        {
            if (UserContext.UdodId.HasValue)
                gvSectionList.Columns[2].Visible = true;
            else
                gvSectionList.Columns[2].Visible = false;

            List<ExtendedSection> list = e.ReturnValue as List<ExtendedSection>;
            lblNumItemsInGv.Text = "Всего " + list.Count.ToString() + " элемент(ов)";
        }
    }

    protected void ddlNumItemsOnPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvSectionList.PageSize = Convert.ToInt32(ddlNumItemsOnPage.SelectedValue);
        SectionListDatabind();
    }

    protected void SectionListDatabind()
    {
        gvSectionList.DataBind();
        upGridView.Update();
    }

    protected string GetToolTip(object section)
    {
        ExtendedSection s = (ExtendedSection)section;
        if (!s.isEnabledToChange)
        {
            if (((s.ChildUnionCount - 1) % 10 == 0) && (s.ChildUnionCount!=11))
            {
                return "Невозможно удалить т.к. в данном УДО есть " + s.ChildUnionCount.ToString() + " активное детское объединение с такой секцией";
            }
            if (((s.ChildUnionCount - 2) % 10 == 0) || ((s.ChildUnionCount - 3) % 10 == 0) || ((s.ChildUnionCount - 4) % 10 == 0))
            {
                return "Невозможно удалить т.к. в данном УДО есть " + s.ChildUnionCount.ToString() + " активных детских объединения с такой секцией";
            }
            return "Невозможно удалить т.к. в данном УДО есть " + s.ChildUnionCount.ToString() + " активных детских объединений с такой секцией";
        }
        else
            return "";
    }

    protected void gvSectionListSortingChanged(object sender, SortHeaderEventArgs e)
    {
        OrderBy = e.SortExpression + " " + e.SortDirection;
        lblSorting.Text = OrderBy;
        SectionListDatabind();
    }

    protected void gvSectionList_OnDataBound(object sender, EventArgs e)
    {
        if ((!string.IsNullOrEmpty(OrderBy)) && (gvSectionList.HeaderRow != null))
        {
            (gvSectionList.HeaderRow.Cells[0].FindControl("shName") as UserControl_Menu_ucSortHeader).Refresh(OrderBy.Split(' ')[0], OrderBy.Split(' ')[1]);
            (gvSectionList.HeaderRow.Cells[1].FindControl("shChkIsExist") as UserControl_Menu_ucSortHeader).Refresh(OrderBy.Split(' ')[0], OrderBy.Split(' ')[1]);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.ServiceModel;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
//using Microsoft.Web.Services2.Security.X509;
using Site.Udod.Providers;
using Udod.Dal;
using System.Diagnostics;
//using X509Certificate = Microsoft.Web.Services2.Security.X509.X509Certificate;
//using X509CertificateCollection = Microsoft.Web.Services2.Security.X509.X509CertificateCollection;


public partial class Login : System.Web.UI.Page
{
    private string ReturnUrl
    {
        get
        {
            string url = Request.QueryString["ReturnUrl"] ?? "Claim/NewClaim.aspx";

            return string.Concat(url, url.Contains("?") ? "&" : "?", "action=login");
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Form.DefaultButton = lbEnter.UniqueID;
         /*
        HttpClientCertificate cert = Request.ClientCertificate;
        if (cert.IsPresent)
        {
            tbLogin.Text = "test";
        }*/
        
        #region Проверка ЭПЦ       
        /*X509Store store = new X509Store(StoreLocation.CurrentUser);
        store.Open(OpenFlags.ReadOnly);
        X509Certificate2Collection certCollection = store.Certificates;
        lblInfo.Text = store.Certificates.Count.ToString();
        X509Certificate2Collection currentCerts = certCollection.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
        X509Certificate2Collection signingCert = currentCerts.Find(X509FindType.FindBySubjectName, "sergey.b.b@bk.ru", false);
        X509Certificate2 cert2 = null;*/
        try
        {
            /*foreach (ListItem item in lbCert.Items)
            {
                if (item.Value=="")
                {
                    EnterInSite("Maltsev");
                }
            }*/
            /*
            if (Request.ClientCertificate.IsPresent)
            {
                //declarant 
                lblInfo.Text = "да впришло";
            }
            cert2 = signingCert[0];
            var tmp33=cert2.PrivateKey.SignatureAlgorithm;
            XmlDocument doc = new XmlDocument();
            doc.Load(Page.MapPath("Templates/getUdodInfo.xml"));

            SignedXml xml = new SignedXml(doc);
            xml.SigningKey = cert2.PrivateKey;
            Reference reference = new Reference();
            reference.Uri = "";
            //reference.DigestMethod = System.Security.Cryptography.Xml
            xml.AddReference(reference);
            KeyInfo keyInfo = new KeyInfo();
            keyInfo.AddClause(new KeyInfoX509Data(cert2));
            xml.KeyInfo = keyInfo;
            //cert2.PrivateKey.ToXmlString(true);
            xml.ComputeSignature();    
            //tbLogin.Text = "test";
            //cert.FriendlyDisplayName
            //if (cert2.Verify())
            //EnterInSite("Maltsev");*/
        }
        catch (Exception ex)
        {
            /*FileInfo file = new FileInfo("c:\\udod\\tmp_.txt");
            var writer = file.AppendText();
            writer.WriteLine(ex.Message + currentCerts.Count + certCollection.Count);
            writer.Flush();
            writer.Close();*/
        }
    #endregion      

    }
    protected void lbEnter_Click(object sender, EventArgs e)
    {
        lblInfo.Text = "";
        string txtUser = tbLogin.Text;
        string txtPassword = tbPassword.Text;
        try
        {
            using (UserDb db = new UserDb())
            {
                
                    var user = db.GetUser(txtUser);
                    if (user.UserName == null)
                        throw new Exception();
                    if (Membership.ValidateUser(txtUser, txtPassword))
                    {
                        EnterInSite(txtUser, user.UserId);
                    }
                
            }
        }
        catch (Exception)
        {
            lblInfo.Text = "Неверный логин или пароль";
        }
    }
    protected void EnterInSite(string txtUser, Guid? userId)
    {
        var ticket = new FormsAuthenticationTicket(2, txtUser, DateTime.Now, DateTime.Now.AddDays(1), false, "", FormsAuthentication.FormsCookiePath);
        HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName)
        {
            HttpOnly = true,
            Path = FormsAuthentication.FormsCookiePath,
            Secure = FormsAuthentication.RequireSSL,
            Value = FormsAuthentication.Encrypt(ticket)
        };
        Response.Cookies.Clear();
        Response.Cookies.Add(cookie);

        if (txtUser == "1111")
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("http://dop.mos.ru:90/", true);
        }
        else
        {


            EventsUtility.Loglogin(userId, Request.UserHostAddress, "Вход в систему");

            Response.Redirect(ReturnUrl, true);
        }
    }

    protected void btnEnterCert_OnClick(object sender, EventArgs e)
    {
        try
        {
            using (UserDb db = new UserDb())
            {
                ExtendedUser user = db.GetUserByCert(hf1.Value);
                if (user.UserName != null)
                {
                    EnterInSite(user.UserName, user.UserId);
                }
            }
        }
        catch (Exception)
        {
        }
    }

    protected void lnkShowPopup_OnClick(object sender, EventArgs e)
    {
        popupEnterCertExtender.Show();
    }
}
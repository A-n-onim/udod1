﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Reports_TopClaimList : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsDepartment);
        
        if (!Page.IsPostBack)
        {
        }
    }

    protected void Databind()
    {
        using (ReportDb db = new ReportDb())
        {
            //if (UserContext.UdodId.HasValue)
            {
                List<ExtendedReport> cuList = db.GetChildUnionClaimCount(UserContext.CityId, UserContext.UdodId, 1);
                gvChildUnionClaimNumber.DataSource = cuList;
                gvChildUnionClaimNumber.DataBind();

                List<ExtendedReport> seList = db.GetSectionClaimCount(UserContext.CityId, UserContext.UdodId, 1);
                gvSectionClaimCount.DataSource = seList;
                gvSectionClaimCount.DataBind();

                List<ExtendedReport> pfList = db.GetProfileClaimCount(UserContext.CityId, UserContext.UdodId, 1);
                gvProfileClaimCount.DataSource = pfList;
                gvProfileClaimCount.DataBind();

                List<ExtendedReport> pgList = db.GetProgramClaimCount(UserContext.CityId, UserContext.UdodId, 1);
                gvProgramClaimCount.DataSource = pgList;
                gvProgramClaimCount.DataBind();
            }
        }
    }

    protected void UdodChange(object sender, EventArgs e)
    {
        Databind();
        upChildUnionGV.Update();
        upSectionGV.Update();
        upProfileGV.Update();
        upProgramGV.Update();
    }

    protected void lnkPrint_Click(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("TopClaimCount.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                                string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
        //workbook.Open(MapPath("../Templates/ClaimTemplate.xlsx"));
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];

        using (ReportDb db = new ReportDb())
        {
            List<ExtendedReport> cuList = db.GetChildUnionClaimCount(UserContext.CityId, UserContext.UdodId, 1);

            List<ExtendedReport> seList = db.GetSectionClaimCount(UserContext.CityId, UserContext.UdodId, 1);

            List<ExtendedReport> pfList = db.GetProfileClaimCount(UserContext.CityId, UserContext.UdodId, 1);

            List<ExtendedReport> pgList = db.GetProgramClaimCount(UserContext.CityId, UserContext.UdodId, 1);

            Aspose.Cells.Style style_area = workbook.Styles[0];
            style_area.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
            style_area.ShrinkToFit = true;

            style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
            style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
                style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;

            int row = 1;
            sheet.Cells[0, 0].PutValue("№");
            sheet.Cells[0, 1].PutValue("Детское объединение");
            sheet.Cells[0, 2].PutValue("Кол-во заявок");

            sheet.Cells[0, 4].PutValue("№");
            sheet.Cells[0, 5].PutValue("Направление");
            sheet.Cells[0, 6].PutValue("Кол-во заявок");

            sheet.Cells[0, 8].PutValue("№");
            sheet.Cells[0, 9].PutValue("Профиль");
            sheet.Cells[0, 10].PutValue("Кол-во заявок");

            sheet.Cells[0, 12].PutValue("№");
            sheet.Cells[0, 13].PutValue("Предмет");
            sheet.Cells[0, 14].PutValue("Кол-во заявок");
            foreach (var cu in cuList)
            {
                sheet.Cells[row, 0].PutValue(row);
                sheet.Cells[row, 1].PutValue(cu.ChildUnionName);
                sheet.Cells[row, 2].PutValue(cu.ChildUnionClaimNum);

                row++;
            }
            sheet.Cells.CreateRange(0, 0, row, 3).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });

            row = 1;
            foreach (var se in seList)
            {
                sheet.Cells[row, 4].PutValue(row);
                sheet.Cells[row, 5].PutValue(se.ChildUnionSection);
                sheet.Cells[row, 6].PutValue(se.ChildUnionClaimNum);

                row++;
            }
            sheet.Cells.CreateRange(0, 4, row, 3).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });

            row = 1;
            foreach (var pf in pfList)
            {
                sheet.Cells[row, 8].PutValue(row);
                sheet.Cells[row, 9].PutValue(pf.ChildUnionProfile);
                sheet.Cells[row, 10].PutValue(pf.ChildUnionClaimNum);

                row++;
            }
            sheet.Cells.CreateRange(0, 8, row, 3).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });

            row = 1;
            foreach (var pg in pgList)
            {
                sheet.Cells[row, 12].PutValue(row);
                sheet.Cells[row, 13].PutValue(pg.ChildUnionProgram);
                sheet.Cells[row, 14].PutValue(pg.ChildUnionClaimNum);

                row++;
            }
            sheet.Cells.CreateRange(0, 12, row, 3).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
        }
        sheet.AutoFitColumns();

        //sheet.Cells.CreateRange(0, 0, row, 11).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
        workbook.Save("TopClaimCount.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }
}
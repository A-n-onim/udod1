﻿<%@ Page Language="C#" MasterPageFile="../Master/MasterPage.master" AutoEventWireup="true" CodeFile="ClaimNumber.aspx.cs" Inherits="Reports_ClaimNumber" %>

<asp:Content runat="server" ID="ContentClaimNumber" ContentPlaceHolderID="body">
    
    <br/>
    
    <asp:UpdatePanel runat="server" ID="upGridView" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:GridView runat="server" ID="gvClaimNumber" CssClass="dgc"  AutoGenerateColumns="false" DataKeyNames="CityId">
            <Columns>
                <asp:TemplateField HeaderText="№" >
                    <ItemTemplate><%# (gvClaimNumber.Rows.Count + 1)%></ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Округ">
                    <ItemTemplate>
                        <%# Eval("CityName") %>
                    </ItemTemplate>
                </asp:TemplateField>
                        
                <asp:TemplateField HeaderText="Район">
                    <ItemTemplate>
                        <%# Eval("UdodRayon") %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Кол-во заявок">
                    <ItemTemplate>
                        <%# Eval("ChildUnionClaimNum")%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </ContentTemplate>
    </asp:UpdatePanel>

    <asp:LinkButton runat="server" ID="lnkPrint" OnClick="lnkPrint_Click"><div class="btnBlue">Сохранить в .xls</div></asp:LinkButton>
</asp:Content>
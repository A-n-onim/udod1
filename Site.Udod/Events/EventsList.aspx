﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" CodeFile="EventsList.aspx.cs" Inherits="Events_EventsList" %>
<%@ Register src="../UserControl/Menu/ucPager.ascx" tagName="Pager" tagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">



<asp:Panel runat="server" ID="pFilter" GroupingText="Фильтр списка событий">
    <table cellpadding="5" cellspacing="0" border="0">
        <tr>
            <td>Тип события:</td>
            <td>
                <asp:DropDownList ID="drdEventTypes" DataSourceID="dsEventTypes" DataTextField="Name" DataValueField="EventTypeId" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="filterChanged">
                    <asp:ListItem Text="-- Все --" Value="" />
                </asp:DropDownList>
                <asp:ObjectDataSource runat="server" ID="dsEventTypes" TypeName="Udod.Dal.EventsDb" SelectMethod="GetEventTypes" />
            </td>
            <td>Период:</td>
            <td>
                <asp:TextBox runat="server" ID="tbDateStart" AutoPostBack="true" CssClass="inputXShort" OnTextChanged="filterChanged"/>
                <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender_DateStart" TargetControlID="tbDateStart" Format="dd.MM.yyyy"/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbDateEnd" AutoPostBack="true" CssClass="inputXShort"  OnTextChanged="filterChanged"/>
                <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender_DateEnd" TargetControlID="tbDateEnd" Format="dd.MM.yyyy"/>
            </td>
            <td>Логин:</td>
            <td>
                <asp:TextBox runat="server" ID="tbLogin" CssClass="inputXShort"></asp:TextBox>
            </td>
            <td>
                <asp:LinkButton ID="lnkRefresh" OnClick="lnkRefresh_OnClick" runat="server"><div class="btnBlueShort">Поиск</div></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                Текст сообщения:
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbMessage" CssClass="inputXShort"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Panel>

<asp:UpdatePanel ID="mainUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:GridView ID="gvEventsList" DataSourceID="dsEventsList" DataKeyNames="EventId" CssClass="dgc" AutoGenerateColumns="false" runat="server">
            <EmptyDataTemplate>
                <asp:Label ID="lblEmptyGvEventList" runat="server" style="font-size:16px">Событий не найдено</asp:Label>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="№" >
                    <ItemTemplate><%# gvEventsList.Rows.Count + pgrEventsList.PagerIndex %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Дата" >
                    <ItemTemplate>
                        <%# Eval("EventDate") %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Тип" >
                    <ItemTemplate>
                        <%# Eval("EventType.Name") %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Сообщение" >
                    <ItemTemplate>
                        <%# Eval("Message") %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Пользователь">
                    <ItemTemplate>
                        (<%# Eval("User.UserName") %>) <%# Eval("User.Fio") %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="IP адрес">
                    <ItemTemplate>
                        <%# Eval("RemoteIP")%>
                    </ItemTemplate>
                </asp:TemplateField>
        
            </Columns>
        </asp:GridView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="tbDateStart" />
        <asp:AsyncPostBackTrigger ControlID="tbDateEnd" />
        <asp:AsyncPostBackTrigger ControlID="drdEventTypes" />
    </Triggers>
</asp:UpdatePanel>

<uc:Pager runat="server" ID="pgrEventsList" needStartDots="false" needEndDots="false" pagesCount="10" OnChange="pgrEventsList_OnChange"/>


<asp:ObjectDataSource ID="dsEventsList" TypeName="Udod.Dal.EventsDb" SelectMethod="GetEventsPaging" runat="server" OnSelected="ds_Selected">
    <SelectParameters>
        <asp:ControlParameter Name="eventTypeId" ControlID="drdEventTypes" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="dateStart" ControlID="tbDateStart" DbType="DateTime" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="dateEnd" ControlID="tbDateEnd" DbType="DateTime" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="login" ControlID="tbLogin" DbType="String" ConvertEmptyStringToNull="true"/>
        <asp:ControlParameter Name="message" ControlID="tbMessage" DbType="String" ConvertEmptyStringToNull="true"/>
        <asp:Parameter Name="PupilId" ConvertEmptyStringToNull="true" />
        <asp:Parameter Name="UdodId" ConvertEmptyStringToNull="true" />
        <asp:Parameter Name="ChildUnionId" ConvertEmptyStringToNull="true" />
        <asp:ControlParameter Name="pagerIndex" ControlID="pgrEventsList" ConvertEmptyStringToNull="true" PropertyName="PagerIndex" />
        <asp:ControlParameter Name="pagerLength" ControlID="pgrEventsList" ConvertEmptyStringToNull="true" PropertyName="PagerLength" />
    </SelectParameters>    
</asp:ObjectDataSource>

</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class Events_EventsList : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator || UserContext.Roles.IsDepartment);
        
        if (!IsPostBack)
        {
            CalendarExtender_DateStart.SelectedDate = DateTime.Today.AddMonths(-1);
            CalendarExtender_DateEnd.SelectedDate = DateTime.Today;
        }
    }

    protected void lnkRefresh_OnClick(object sender, EventArgs e)
    {
        gvEventsList.DataBind();
    }

    protected void filterChanged(object sender, EventArgs e)
    {
        pgrEventsList.Clear();
    }

    protected void pgrEventsList_OnChange(object sender, EventArgs e)
    {
        gvEventsList.DataBind();
    }

    protected void ds_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        using (EventsDb db = new EventsDb())
        {
            int? eventType;
            try
            {
                eventType = Convert.ToInt32(drdEventTypes.SelectedValue);
            }
            catch
            {
                eventType = null;
            }
            DateTime? dateStart;
            try
            {
                dateStart = Convert.ToDateTime(tbDateStart.Text);
            }
            catch
            {
                dateStart = null;
            }
            DateTime? dateEnd;
            try
            {
                dateEnd = Convert.ToDateTime(tbDateEnd.Text);
            }
            catch
            {
                dateEnd = null;
            }
            pgrEventsList.numElements = db.GetEventsCount(eventType, dateStart, dateEnd, null, null, null, tbLogin.Text, tbMessage.Text, null,  null);
        }
    }
}
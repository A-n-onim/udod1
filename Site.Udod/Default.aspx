﻿<%@ Page Language="C#" MasterPageFile="Master/MasterPage.master" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>
<%@ Register TagPrefix="uct" TagName="Fio" Src="~/UserControl/User/ucFIO_vertical.ascx" %>
<%@ Register TagPrefix="uct" TagName="Udod" Src="~/UserControl/Udod/ucFindUdod.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="body" ID="ContentDefault">
    <table >
    
    <tr >
    <td style="vertical-align: top;">
    <asp:UpdatePanel runat="server" ID="upFind" UpdateMode="Conditional">
    <ContentTemplate>
    <asp:Panel runat="server" ID="pOsip" Visible="false" GroupingText="<div class='headerPanelBig'><b>Поиск заявлений</b></div>" >
        
            <table>
            <tr>
                <td style="width: 68px;">Номер заявления:</td>
                <td>
                    <div class="inputShort">
                        <asp:TextBox runat="server" ID="tbNumber" class="inputShort"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" FilterType="Numbers" TargetControlID="tbNumber" runat="server" />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <div>
                    Поиск по заявителям и учащимся
                    <uct:Fio runat="server" ID="ucFio"></uct:Fio>
                </div>
        
                </td>
            </tr>
            <tr>
                <td runat="server" ID="td1" colspan="2">
                    <asp:RadioButtonList runat="server" ID="rblBirthday" AutoPostBack="true" OnSelectedIndexChanged="rblBirthday_OnSelectedIndexChanged">
                        <Items>
                            <asp:ListItem Value="0" Selected="true">Поиск даты рожд. по периоду</asp:ListItem>
                            <asp:ListItem Value="1">Поиск даты рожд. по дате</asp:ListItem>
                        </Items>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td runat="server" ID="td3">Дата рождения от:</td>
                <td runat="server" ID="td4">
                    <div class="inputXShort">
                    <asp:TextBox runat="server" ID="tbBirthdayBegin" CssClass="inputXShort" />
                            
                    </div>
                    <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="tbBirthdayBegin" Format="dd.MM.yyyy"/>
                    <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbBirthdayBegin" Mask="99/99/9999" MaskType="Date" />
                </td>
            </tr>
            <tr>
                <td runat="server" ID="td6">до:</td>
                <td runat="server" ID="td7">
                    <div class="inputXShort">
                    <asp:TextBox runat="server" ID="tbBirthdayEnd" CssClass="inputXShort" />
                            
                    </div>
                    <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender3" TargetControlID="tbBirthdayEnd" Format="dd.MM.yyyy"/>
                    <ajaxToolkit:MaskedEditExtender ID="mee2" runat="server" TargetControlID="tbBirthdayEnd" Mask="99/99/9999" MaskType="Date" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <asp:LinkButton runat="server" ID="lnkFind" OnClick="lnkFind_OnClick"><div class="btnBlueLong"> Поиск</div></asp:LinkButton>
                </td>
            </tr>
            
            </table>
        
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
    </td>
    <td runat="server" ID="cellFind">
    <asp:Panel runat="server" ID="pFind" Visible="false" GroupingText="<div class='headerPanelBig'><b>Подача заявлений</b></div>">
        <uct:Udod runat="server" ID="ucFindUdod" OnChange="ucFindUdod_OnChange"/>
    </asp:Panel> 
    </td>
    </tr>
    </table>

    
</asp:Content>
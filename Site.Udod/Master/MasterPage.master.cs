﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class Master_MasterPage : BaseMaster
{
    protected void Page_Init(object sender, EventArgs e)
    {
        //imgHeader.ImageUrl = SiteUtility.GetUrl("~/img/header.png");
        //imgFooter.ImageUrl = SiteUtility.GetUrl("~/img/Footer.png");
        
        lblInfo.Text = UserContext.Profile.Fio;
        if (!Page.IsPostBack)
        {
            // проверка на роль пользователя и изменение контролов
            if (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee)
            {
                //lblUdod.Visible = true;
                //lblUdod.Text = 
                UserContext.UdodId = UserContext.Profile.UdodId;
                
                if (!UserContext.UdodId.HasValue)
                {
                    using (UserDb db = new UserDb())
                    {
                        UserContext.UdodId = db.GetUser(UserContext.Profile.UserId).UdodId;
                    }
                }

                using (UdodDb db = new UdodDb())
                {
                    var Udod = db.GetUdod(UserContext.UdodId.Value);
                    lblNameUdod.Text = Udod.Name;
                }
                /*ddlUdods.Visible = false;
                ddlCity.Visible = false;*/
            }
            else if (UserContext.Roles.RoleId == (int)EnumRoles.Osip)
            {

                //lblUdod.Visible = false;

                UserContext.CityId = UserContext.Profile.CityId;
                /*List<ExtendedCity> list;
                using (DictCityDb db = new DictCityDb())
                {
                    list = db.GetCities();
                }
                ddlCity.DataSource = list;
                ddlCity.DataBind();
                UserContext.CityId = Convert.ToInt32(ddlCity.SelectedValue);

                UdodDatabind();
                UserContext.UdodId = Convert.ToInt32(ddlUdods.SelectedValue);
                ddlCityDiv.Visible = ddlUdodDiv.Visible = false;*/
            }
            else if (UserContext.Roles.RoleId == (int)EnumRoles.Administrator)
            {
                //lblUdod.Visible = false;
                //lblUdod.Text = 
                //UserContext.UdodId = UserContext.Profile.UdodId;
                //List<ExtendedCity> list;
                //using (DictCityDb db = new DictCityDb())
                //{
                //    list = db.GetCities();
                //}
                //ddlCity.DataSource = list;
                //ddlCity.DataBind();
                //UserContext.CityId = Convert.ToInt32(list.First().CityId);

                //ddlUdods.Visible = true;
                //ddlCity.Visible = true;

                //List<ExtendedUdod> list2;
                //using (UdodDb db = new UdodDb())
                //{
                //    list2 = db.GetUdods(UserContext.CityId, null, null, null, null, null, null, null, null);
                //}
                //ddlUdods.DataSource = list;
                //ddlUdods.DataBind();
                //UserContext.UdodId = Convert.ToInt32(list2.First().UdodId);

                //if (UserContext.UdodId.HasValue)
                //    ddlUdods.SelectedIndex = ddlUdods.Items.IndexOf(ddlUdods.Items.FindByValue(UserContext.UdodId.Value.ToString()));
                //else
                //    UserContext.UdodId = Convert.ToInt32(ddlUdods.SelectedValue);
            }
            else if (UserContext.Roles.RoleId == (int)EnumRoles.Department)
            {
                //lblUdod.Visible = false;
                //lblUdod.Text = 
                //UserContext.UdodId = UserContext.Profile.UdodId;
                /*List<ExtendedCity> list;
                using (DictCityDb db = new DictCityDb())
                {
                    list = db.GetCities();
                }
                ddlCity.DataSource = list;
                ddlCity.DataBind();
                UserContext.CityId = Convert.ToInt32(ddlCity.SelectedValue);

                ddlUdods.Visible = true;
                ddlCity.Visible = true;
                UdodDatabind();
                if (UserContext.UdodId.HasValue)
                    ddlUdods.SelectedIndex = ddlUdods.Items.IndexOf(ddlUdods.Items.FindByValue(UserContext.UdodId.Value.ToString()));
                else
                    UserContext.UdodId = Convert.ToInt32(ddlUdods.SelectedValue);*/
            }
        }
    }

    protected void lnkLogout_OnClick(object sender, EventArgs e)
    {
        
    }
}

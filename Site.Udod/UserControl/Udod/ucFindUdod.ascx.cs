﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class UserControl_Filter_ucFindUdod : BasePageControl
{
    public event EventHandler Change;

    public string SelectButtonText { get { if (ViewState["SelectButtonText"] != null)
        return (string)ViewState["SelectButtonText"];
    return "<div class='btnBlue'>Подать заявления</div>";
    }
        set { if (!string.IsNullOrEmpty(value)) ViewState["SelectButtonText"] = value; else
            ViewState["SelectButtonText"] = null; }
    }

    public long? UdodAgeGroupId
    {
        get
        {
            if (ViewState["UdodAgeGroupId"] == null) return null;
            return (long)ViewState["UdodAgeGroupId"];
        }
        set { ViewState["UdodAgeGroupId"] = value; }
    }

    public string UdodSectionName
    {
        get 
        { 
            if (ViewState["UdodSectionName"] == null) return "";
            return (string) ViewState["UdodSectionName"];
        }
        set 
        {
            ViewState["UdodSectionName"] = value; 
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Udod_DataBind(false); // false - не загружать ДО при первой загрузке
            upFindUdod.Update();
            /*using (DictCityDb db = new DictCityDb())
            {
                List<ExtendedCity> list = db.GetCities();
                ddlCities.DataSource = list;
                ddlCities.DataBind();
            }
            using (DictMetroDb db = new DictMetroDb())
            {
                List<ExtendedMetro> list = db.GetMetros();
                ddlMetro.DataSource = list;
                ddlMetro.DataBind();
            }*/
        }

        if (UserContext.UdodId.HasValue && UserContext.Roles.RoleId==(int)EnumRoles.UdodEmployee)
        {
            pPlace.Visible = false;
            pUdodName.Visible = false;
            //pFilterSection.Visible = false;
            //rowCity.Visible = rowMetro.Visible = rowRayon.Visible = false;
            /*using (UdodDb db = new UdodDb())
            {
                List<ExtendedUdod> list = new List<ExtendedUdod>();
                var udod = db.GetUdod(UserContext.UdodId.Value);
                list.Add(udod);
                lbUdos.DataSource = list;
                lbUdos.DataBind();
                
            }*/
            lbUdos.Visible = false;
            pUdod.GroupingText = "<div class='headerPanel''><b>Список детских объединений</b></div>";
            //lbUdos.SelectedIndex = 0;
            //tbUdoCode.Text = lbUdos.SelectedValue;
            //tbUdoName.Text = lbUdos.SelectedItem.Text;
            ChildUnionDatabind();
            
            //filterSection.Visible = false;
        }
        //upFilter.Update();
    }

    protected void City_SelectedIndex_Changed(object sender, EventArgs e)
    {
        pgrChildUnionList.Clear();
        Udod_DataBind(true);
    }

    protected void Metro_SelectedIndex_Changed(object sender, EventArgs e)
    {
        pgrChildUnionList.Clear();
        Udod_DataBind(true);
    }

    protected void Udod_DataBind(bool CU_Databind)
    {
        if (UserContext.UdodId.HasValue && UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee)
        {
            
        }
        else
        {
            using (UdodDb db = new UdodDb())
            {
                int? metroId;
                if (string.IsNullOrEmpty(ddlMetro.SelectedValue))
                    metroId = null;
                else 
                    metroId = Convert.ToInt32(ddlMetro.SelectedValue);
                /*int? cityId;
                if (string.IsNullOrEmpty(ddlCities.SelectedValue))
                    cityId = null;
                else
                    cityId = Convert.ToInt32(ddlCities.SelectedValue);
                */
                string city = string.IsNullOrEmpty(ddlCities.SelectedValue) ? null : ddlCities.SelectedValue + " административный округ";
                /*int? rayonId;
                if (string.IsNullOrEmpty(ddlDistrict.SelectedValue))
                    rayonId = null;
                else
                    rayonId = Convert.ToInt32(ddlDistrict.SelectedValue);*/
                List<ExtendedUdod> list = db.GetUdods(null, city, ddlDistrict.Text != "Все" ? ddlDistrict.Text : null, metroId, null, null, null, tbUdodName.Text, filterSection.TypeBudgetId, filterSection.SectionName, filterSection.ProfileName, filterSection.ProgramName, filterSection.ChildUnionName, null, 1);
                lbUdos.DataSource = list;
                lbUdos.DataBind();
                /*if (UserContext.Roles.RoleId != (int)EnumRoles.UdodEmployee)
                {
                    if (lbUdos.Items.Count > 0)
                        lbUdos.SelectedIndex = 0;
                }*/
                /*
                if (list.Count == 0)
                {
                    gvUdodSection.EmptyDataText = "";
                    gvUdodSection.DataSource = null;
                    gvUdodSection.DataBind();
                }
                else
                    gvUdodSection.EmptyDataText = "Выберите учреждение, чтобы увидеть доступные детские объединения";
                */
                /*using (ChildUnionDb db1 = new ChildUnionDb())
                {
                    long? udodId;
                    if (!string.IsNullOrEmpty(lbUdos.SelectedValue)) udodId = Convert.ToInt64(lbUdos.SelectedValue);
                    else udodId = null;
                    var list1 = db1.GetChildUnionsShort(cityId, udodId, filterSection.ProgramId, filterSection.ProfileId, null, filterSection.TypeBudgetId, filterSection.SectionName, null, null, metroId, tbUdodName.Text, rayonId);
                    gvUdodSection.DataSource = list1;
                    gvUdodSection.DataBind();
                }*/
                if (CU_Databind)
                    ChildUnionDatabind();
            }
        }
    }

    protected void lbUdos_SelectedIndexChanged(object sender, EventArgs e)
    {
        //tbUdoCode.Text = lbUdos.SelectedValue;
        //tbUdoName.Text = lbUdos.SelectedItem!=null? lbUdos.SelectedItem.Text:"";

        
        if (string.IsNullOrEmpty(lbUdos.SelectedValue))
        {
                
        }
        else
        {
            pgrChildUnionList.Clear();
            ChildUnionDatabind();
        }
        gvUdodSection.EmptyDataText = "Поиск не дал результатов";

        /*
        using (AgeGroupDb db = new AgeGroupDb())
        {
            if (string.IsNullOrEmpty(lbUdos.SelectedValue))
            {
                gvUdodSection.DataSource = new List<ExtendedAgeGroup>();
                gvUdodSection.DataBind();
            }
            else
            {
                var list = db.GetAgeGroups(Convert.ToInt64(lbUdos.SelectedValue), filterSection.SectionId,null);
                gvUdodSection.DataSource = list;
                gvUdodSection.DataBind();
            }
        }*/
    }

    protected void GvUdodSectionSelectedIndexChanged(object sender, EventArgs e)
    {
        if (gvUdodSection.SelectedDataKey!=null)
            UdodAgeGroupId = (long)gvUdodSection.SelectedDataKey.Value;
        else
        {
            if (gvUdodSection.SelectedIndex >= 0)
            {
                LinkButton btn = (LinkButton)gvUdodSection.Rows[gvUdodSection.SelectedIndex].FindControl("lnkShowDetail");
                if (!string.IsNullOrEmpty(btn.Attributes["ChildUnionId"]))
                    UdodAgeGroupId = Convert.ToInt64(btn.Attributes["ChildUnionId"]);
            }
        }
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var item = db.GetChildUnion(UdodAgeGroupId, null);
            //var item = list.FirstOrDefault();
            UdodSectionName = item.Name;
        }
        if (Change!=null) Change(this, e);
    }

    protected void filterSection_OnChange(object sender, EventArgs e)
    {
        pgrChildUnionList.Clear();
        Udod_DataBind(true);
        ClearFields();
    }
    public void Clear()
    {
        ViewState["UdodSectionId"] = null;
        ViewState["UdodSectionName"] = null;

    }

    protected void ddlCities_OnDataBound(object sender, EventArgs e)
    {
        ddlCities.Items.Insert(0, new ListItem("Все", ""));
    }

    protected void ddlDistrict_OnDataBound(object sender, EventArgs e)
    {
        ddlDistrict.Items.Insert(0, new ListItem("Все", ""));
    }

    protected void ddlMetro_OnDataBound(object sender, EventArgs e)
    {
        ddlMetro.Items.Insert(0, new ListItem("Все", ""));
    }

    public void ClearFields()
    {
        //tbUdoCode.Text = "";
        //tbUdoName.Text = "";
        gvUdodSection.DataBind();
    }

    protected void lnkFind_OnClick(object sender, EventArgs e)
    {
        pgrChildUnionList.Clear();
        Udod_DataBind(true);
    }

    protected void ddlDistrict_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        pgrChildUnionList.Clear();
        Udod_DataBind(true);
    }
    protected string GetAddress(object item)
    {
        ExtendedChildUnion childUnion = (ExtendedChildUnion) item;
        if (childUnion.Addresses.Count > 0) return childUnion.Addresses.FirstOrDefault().AddressStr;
        return "";
    }

    protected void gvUdodSection_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType==DataControlRowType.DataRow)
        {
            ExtendedChildUnion item = (ExtendedChildUnion) e.Row.DataItem;
            LinkButton btn = (LinkButton) e.Row.FindControl("lnkShowDetail");
            btn.Attributes.Add("ChildUnionId", item.ChildUnionId.ToString());
        }
    }

    protected void lnkShowDetail_OnClick(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton) sender;
        long id = Convert.ToInt64(btn.Attributes["ChildUnionId"]);
        using (ChildUnionDb db = new ChildUnionDb()  )
        {
            selectDO.SetDOData(db.GetChildUnion(id,null));
            upDO.Update();
            popupSelectDOExtender.Show();
        }
    }

    protected void ChildUnionDatabind()
    {
        (gvUdodSection.Columns[gvUdodSection.Columns.Count - 1] as CommandField).SelectText = SelectButtonText;
        using (ChildUnionDb db = new ChildUnionDb())
        {
            int? metroId;
            if (string.IsNullOrEmpty(ddlMetro.SelectedValue))
                metroId = null;
            else
                metroId = Convert.ToInt32(ddlMetro.SelectedValue);

            /*int? cityId;
            if (string.IsNullOrEmpty(ddlCities.SelectedValue))
                cityId = null;
            else
                cityId = Convert.ToInt32(ddlCities.SelectedValue);
            */
            /*int? rayonId;
            if (string.IsNullOrEmpty(ddlDistrict.SelectedValue))
                rayonId = null;
            else
                rayonId = Convert.ToInt32(ddlDistrict.SelectedValue);*/
            string rayon = string.IsNullOrEmpty(ddlDistrict.SelectedValue)?null:ddlDistrict.SelectedValue;
            string city = string.IsNullOrEmpty(ddlCities.SelectedValue) ? null : ddlCities.SelectedValue;// +" административный округ";

            long? udodId;
            if (UserContext.Roles.RoleId != (int)EnumRoles.UdodEmployee)
            {
                if (!string.IsNullOrEmpty(lbUdos.SelectedValue))
                    udodId = Convert.ToInt64(lbUdos.SelectedValue);
                else
                    udodId = null;
            }
            else
                udodId = UserContext.UdodId;

            pgrChildUnionList.numElements = db.GetChildUnionsCount(null, udodId, null, null, null, filterSection.TypeBudgetId, filterSection.SectionName, filterSection.ProfileName, filterSection.ProgramName, filterSection.ChildUnionName, city, rayon, metroId, tbUdodName.Text, null, null, null, 1, null, true, false, null);
            var list = db.GetChildUnionsPaging(null, udodId, null, null, null, filterSection.TypeBudgetId, filterSection.SectionName, filterSection.ProfileName, filterSection.ProgramName, filterSection.ChildUnionName, city, rayon, metroId, tbUdodName.Text, null, pgrChildUnionList.PagerIndex, pgrChildUnionList.PagerLength, null, 1, null, true, false, null);

            //var list = db.GetChildUnionsShort(cityId, udodId, null, null, null, filterSection.TypeBudgetId, filterSection.SectionName, filterSection.ProfileName, filterSection.ProgramName, filterSection.ChildUnionName, null, null, metroId, tbUdodName.Text, rayonId, null);
            gvUdodSection.DataSource = list;
            gvUdodSection.DataBind();
            upFindUdod.Update();
        }
    }

    protected void pgrChildUnionList_OnChange(object sender, EventArgs e)
    {
        ChildUnionDatabind();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal.Enum;

public partial class UserControl_Udod_ucUdodFilter : BasePageControl
{
    public event EventHandler Change;
    public int? DictClaimStatusId { get { if (string.IsNullOrEmpty(ddlStatus.SelectedValue)) return null; return Convert.ToInt32(ddlStatus.SelectedValue); } set { ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(value.Value.ToString())); } }
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Clear()
    {
        ddlStatus.SelectedIndex = 0;
    }

    protected void ddlStatus_OnDataBound(object sender, EventArgs e)
    {
        if (UserContext.Roles.RoleId==(int)EnumRoles.UdodEmployee)
        {
            ddlStatus.Items.Insert(0, new ListItem("Все активные", "-1"));
            ddlStatus.Items.Insert(1, new ListItem("Все", ""));
            //ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("4"));
        }
        else
        {
            ddlStatus.Items.Insert(0, new ListItem("Все", ""));
        }
    }

    protected void ddlStatus_SelectedChanged(object sender, EventArgs e)
    {
        if (Change != null) Change(this, new EventArgs());
    }
}
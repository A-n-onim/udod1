﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class UserControl_Udod_ucUdodPassport : BasePageControl
{
    public bool IsIndividual 
    {
        set
        {
            trIsFilial.Visible= trAccreditation.Visible= trOwnerShip.Visible= trTypeUdod.Visible= trStudyForm.Visible= trSector.Visible= trOKOGU.Visible= trLegalForm.Visible= trOKATO.Visible= trKPP.Visible= trCountSmen.Visible= trPopupCity.Visible = trJurAddress.Visible = trShortName.Visible = trDopName.Visible = trUdodName.Visible = !value;
            tbActivity.Visible= trCityzenShip.Visible = value;
        }
    }
    public bool IsEdit { 
        set
        {
            lblUdodName.Visible = 
                lblUdodShortName.Visible=
                lbljurAddress.Visible=
                lblEmail.Visible=
                lblFaxNumber.Visible=
                lblFioDirector.Visible=
                lblPhoneNumber.Visible=
                lblPopupCity.Visible=
                lblSiteUrl.Visible=
                ddlIsDOEnabled.Enabled =
                ddlInvalid.Enabled = !value;

            tbUdodName.Visible = 
                tbUdodShortName.Visible=
                tbEMail.Visible=
                tbFaxNumber.Visible=
                tbFioDirector.Visible=
                tbJurAddress.Visible=
                tbPhoneNumber.Visible=
                tbSiteUrl.Visible=
                ddlPopupCity.Visible=
                ddlIsDOEnabled.Enabled = 
                ddlInvalid.Enabled = value;
        } 
    }
    public void SetUdodData(ExtendedUdod udod)
    {
        lblUdodName.Text= tbUdodName.Text = udod.Name;
        lblUdodShortName.Text= tbUdodShortName.Text = udod.ShortName;
        //lblUdodNumber.Text = tbUdodNumber.Text = udod.UdodNumber;
        lbljurAddress.Text= tbJurAddress.Text = udod.JurAddress;
        lblFioDirector.Text= tbFioDirector.Text = udod.FioDirector;
        lblPhoneNumber.Text= tbPhoneNumber.Text = udod.PhoneNumber;
        lblFaxNumber.Text= tbFaxNumber.Text = udod.FaxNumber;
        lblEmail.Text= tbEMail.Text = udod.EMail;
        lblSiteUrl.Text= tbSiteUrl.Text = udod.SiteUrl;
        //tbUdodTypeId.Text = Convert.ToString(udod.Value.UdodTypeId);
        ddlIsDOEnabled.SelectedIndex = ddlIsDOEnabled.Items.IndexOf(ddlIsDOEnabled.Items.FindByValue(Convert.ToInt32(udod.IsDOEnabled).ToString()));
        ddlInvalid.SelectedIndex = ddlInvalid.Items.IndexOf(ddlInvalid.Items.FindByValue(Convert.ToInt32(udod.IsInvalid).ToString()));

        using (DictCityDb db = new DictCityDb())
        {
            ddlPopupCity.DataSource = db.GetCities();
            ddlPopupCity.DataBind();
        }

        ddlPopupCity.SelectedIndex = ddlPopupCity.Items.IndexOf(ddlPopupCity.Items.FindByValue(udod.CityId.ToString()));
        lblPopupCity.Text = ddlPopupCity.SelectedItem.Text;
        // dopinfo
        tbDopName.Text = udod.DopInfo.DopName;
        tbINN.Text = udod.DopInfo.INN;
        tbKPP.Text = udod.DopInfo.KPP;
        tbOGRN.Text = udod.DopInfo.OGRN;
        tbOKATO.Text = udod.DopInfo.code_okato;

        ddlSector.SelectedIndex = ddlSector.Items.IndexOf(ddlSector.Items.FindByValue(udod.DopInfo.sector));
        ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(Convert.ToInt32(udod.DopInfo.Status).ToString()));
        //ddlMainOrganization.SelectedIndex = ddlMainOrganization.Items.IndexOf(ddlMainOrganization.Items.FindByValue(Convert.ToInt32(udod.DopInfo.).ToString()));
        ddlOwnerShip.SelectedIndex = ddlOwnerShip.Items.IndexOf(ddlOwnerShip.Items.FindByValue(Convert.ToInt32(udod.DopInfo.ownership).ToString()));
        ddlStudyForm.SelectedIndex = ddlStudyForm.Items.IndexOf(ddlStudyForm.Items.FindByValue(Convert.ToInt32(udod.DopInfo.study_form).ToString()));
        ddlTypeUdod.SelectedIndex = ddlTypeUdod.Items.IndexOf(ddlTypeUdod.Items.FindByValue(Convert.ToInt32(udod.DopInfo.type_UDOD).ToString()));
        ddlCountSmen.SelectedIndex = ddlCountSmen.Items.IndexOf(ddlCountSmen.Items.FindByValue(Convert.ToInt32(udod.DopInfo.CountSmen).ToString()));
    }

    public void SetReestrUdodData(ExtendedUdod udod)
    {
        //lblReestrName.Text = udod.Name;
        lblReestrShortName.Text = udod.ShortName;
        lblReestrJurAddress.Text = udod.JurAddress;
        lblReestrFioDirector.Text = udod.FioDirector;
        lblReestrpPnoneNumber.Text = udod.PhoneNumber;
        lblReestrEMail.Text = udod.EMail;
        lblReestrSiteUrl.Text = udod.SiteUrl;
    }

    public void SaveLicense(int TypeLicense)
    {
        if (TypeLicense == 1)
        {
            ucLicense.SavedLicense();
        }
        if (TypeLicense == 2)
        {
            ucAccreditation.SavedLicense();
        }


    }
    public void LoadLicense(int TypeLicense)
    {
        if (TypeLicense == 1)
        {
            ucLicense.TypeLicense = TypeLicense;
            ucLicense.LoadLicense();
        }
        if (TypeLicense == 2)
        {
            ucAccreditation.TypeLicense = TypeLicense;
            ucAccreditation.LoadLicense();
        }


    }

    public ExtendedUdod GetUdodData()
    {
        ExtendedUdod eu = new ExtendedUdod();
        eu.Name = tbUdodName.Text;
        eu.ShortName = tbUdodShortName.Text;
        //eu.UdodNumber = tbUdodNumber.Text;
        eu.JurAddress = tbJurAddress.Text;
        eu.FioDirector = tbFioDirector.Text;
        eu.PhoneNumber = tbPhoneNumber.Text;
        eu.FaxNumber = tbFaxNumber.Text;
        eu.EMail = tbEMail.Text;
        eu.SiteUrl = tbSiteUrl.Text;
        eu.CityId = Convert.ToInt32(ddlPopupCity.SelectedValue);
        eu.IsDOEnabled = Convert.ToBoolean(Convert.ToInt32(ddlIsDOEnabled.SelectedValue));
        eu.IsInvalid = Convert.ToBoolean(Convert.ToInt32(ddlInvalid.SelectedValue));


        var Info = new ExtendedUdodDopInfo();
        Info.Activity = tbActivity.Text;
        Info.CityzenShip = tbCityzenShip.Text;
        Info.code_okato = tbOKATO.Text;
        Info.DopName = tbDopName.Text;
        Info.INN = tbINN.Text;
        Info.KPP = tbKPP.Text;
        Info.legalform = tbLegalForm.Text;
        Info.maxPupils = Convert.ToInt32(tbMaxPupils.Text);
        Info.OGRN = tbOGRN.Text;
        Info.OKOGU = tbOKOGU.Text;
        Info.ownership = Convert.ToInt32(ddlOwnerShip.SelectedValue);
        Info.sector = ddlSector.SelectedValue;
        //Info.Shift = 
        Info.Status = ddlStatus.SelectedIndex;
        Info.study_form = Convert.ToInt32(ddlStudyForm.SelectedValue);

        Info.type_UDOD = Convert.ToInt32(ddlTypeUdod.SelectedValue);
        Info.IsFilial = Convert.ToBoolean(ddlIsFilial.SelectedValue);
        Info.CountSmen = Convert.ToInt32(ddlCountSmen.SelectedValue);
        //var License = new ExtendedUdodLicense();
        //License = ucLicense.l

        eu.DopInfo = Info;
        return eu;
    }

    

    public void ClearData(int cityId)
    {
        lblUdodName.Text = tbUdodName.Text = "";
        lblUdodShortName.Text=tbUdodShortName.Text = "";
        lbljurAddress.Text = tbJurAddress.Text = "";

        using (DictCityDb db = new DictCityDb())
        {
            ddlPopupCity.DataSource = db.GetCities();
            ddlPopupCity.DataBind();
        }

        ddlPopupCity.SelectedIndex = ddlPopupCity.Items.IndexOf(ddlPopupCity.Items.FindByValue(cityId.ToString()));
        lblPopupCity.Text = "";
        lblFioDirector.Text = tbFioDirector.Text = "";
        lblPhoneNumber.Text = tbPhoneNumber.Text = "";
        lblFaxNumber.Text = tbFaxNumber.Text = "";
        lblEmail.Text = tbEMail.Text = "";
        lblSiteUrl.Text = tbSiteUrl.Text = "";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            using (DictCityDb db = new DictCityDb())
            {
                ddlPopupCity.DataSource = db.GetCities();
                ddlPopupCity.DataBind();
            }
        }

    }

    protected void ddlIsFilial_OnSelectedIndexChanged(object sender, EventArgs e)
    { 

    }
}
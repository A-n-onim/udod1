﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUdodPassport.ascx.cs" Inherits="UserControl_Udod_ucUdodPassport" %>
<%@ Register src="~/UserControl/Udod/ucUdodLicense.ascx" tagName="UdodLicense" tagPrefix="uct" %>

<table>
    <tr runat="server" ID="trUdodName">
        <td>
            Наименование*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbUdodName" runat="server"></asp:TextBox>
            <asp:Label runat="server" ID="lblUdodName" Visible="false"></asp:Label><br />
            <asp:RequiredFieldValidator runat="server" ID="val1" ValidationGroup="78" ErrorMessage="Поле 'Наименование' не заполнено" ControlToValidate="tbUdodName" Display="Dynamic"  ></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr runat="server" ID="trDopName">
        <td>
            Дополнительное наименование*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbDopName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="78" ErrorMessage="Поле 'Дополнительное наименование' не заполнено" ControlToValidate="tbDopName" Display="Dynamic"  ></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr runat="server" ID="trShortName">
        <td>
            Краткое наименование*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbUdodShortName" runat="server"></asp:TextBox>
            <asp:Label runat="server" ID="lblUdodShortName" Visible="false"></asp:Label><br />
            <asp:RequiredFieldValidator runat="server" ID="val2" ValidationGroup="78" ErrorMessage="Поле 'Короткое имя' не заполнено" ControlToValidate="tbUdodShortName" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr runat="server" ID="trCountSmen">
        <td>
            Кол-во смен*:
        </td>
        <td>
            <asp:DropDownList CssClass="inputLong" ID="ddlCountSmen" runat="server">
                <Items>
                    <asp:ListItem Text='Нет сменности' Value="0"></asp:ListItem>
                    <asp:ListItem Text='Одна смена' Value="1"></asp:ListItem>
                    <asp:ListItem Text='Две смены' Value="2"></asp:ListItem>
                    <asp:ListItem Text='Три смены' Value="3"></asp:ListItem>
                </Items>
            </asp:DropDownList>
        </td>
    </tr>
    <tr  runat="server" ID="trFioDirector">
        <td>
            ФИО Директора:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbFioDirector" runat="server"></asp:TextBox>
            <asp:Label runat="server" ID="lblFioDirector" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr runat="server" ID="trINN">
        <td>
            ИНН*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbINN" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ValidationGroup="78" ErrorMessage="Поле 'ИНН' не заполнено" ControlToValidate="tbINN" Display="Dynamic"  ></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr runat="server" ID="trKPP">
        <td>
            KPP*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbKPP" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="78" ErrorMessage="Поле 'KPP' не заполнено" ControlToValidate="tbKPP" Display="Dynamic"  ></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr runat="server" ID="trOGRN">
        <td>
            ОГРН*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbOGRN" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ValidationGroup="78" ErrorMessage="Поле 'ОГРН' не заполнено" ControlToValidate="tbOGRN" Display="Dynamic"  ></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr runat="server" ID="trCityzenShip">
        <td>
            Гражданство*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbCityzenShip" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ValidationGroup="78" ErrorMessage="Поле 'Гражданство' не заполнено" ControlToValidate="tbCityzenShip" Display="Dynamic"  ></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr runat="server" ID="trStatus">
        <td>
            Статус*:
        </td>
        <td>
            <asp:DropDownList CssClass="inputLong" ID="ddlStatus" runat="server">
                <items>
                    <asp:ListItem Text="функционирует" Value="1"></asp:ListItem>
                    <asp:ListItem Text="капитальный ремонт" Value="2"></asp:ListItem>
                    <asp:ListItem Text="реконструкция" Value="3"></asp:ListItem>
                    <asp:ListItem Text="деятельность приостановлена" Value="4"></asp:ListItem>
                    <asp:ListItem Text="контингент отсутствует" Value="5"></asp:ListItem>
                    <asp:ListItem Text="ожидает открытия" Value="6"></asp:ListItem>
                    <asp:ListItem Text="ликвидирована" Value="7"></asp:ListItem>
                    <asp:ListItem Text="закрыта" Value="8"></asp:ListItem>
                    <asp:ListItem Text="присоединена к другой организации" Value="9"></asp:ListItem>
                </items>
            </asp:DropDownList>
        </td>
    </tr>
    <tr runat="server" ID="trIsFilial">
        <td>
            Юридическое лицо или филиал*:
        </td>
        <td>
            <asp:DropDownList CssClass="inputLong" ID="ddlIsFilial" runat="server" OnSelectedIndexChanged="ddlIsFilial_OnSelectedIndexChanged">
                <items>
                    <asp:ListItem Text="Юридическое лицо" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Филиал" Value="1"></asp:ListItem>
                </items>
            </asp:DropDownList>
        </td>
    </tr>
    <tr runat="server" ID="trMainOrganization">
        <td>
            Головная организация:
        </td>
        <td>
            <asp:DropDownList CssClass="inputLong" ID="ddlMainOrganization" runat="server" DataSourceID="dsMainsUdod" DataValueField="UdodId" DataTextField="ShortName">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            
        </td>
        <td>
            <asp:Label runat="server" ID="lblReestrShortName" CssClass="ReestrDataClass"></asp:Label>
        </td>
    </tr>
    <tr  runat="server" ID="trJurAddress">
        <td>
            Юридический адрес:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbJurAddress" runat="server"></asp:TextBox>
            <asp:Label runat="server" ID="lbljurAddress" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr runat="server" ID="trOKATO">
        <td>
            Код ОКАТО*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbOKATO" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="78" ErrorMessage="Поле 'ОКАТО' не заполнено" ControlToValidate="tbOKATO" Display="Dynamic"  ></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            
        </td>
        <td>
            <asp:Label runat="server" ID="lblReestrJurAddress" CssClass="ReestrDataClass"></asp:Label>
        </td>
    </tr>
    <tr runat="server" ID="trPopupCity">
        <td>
            Округ:
        </td>
        <td>
            <asp:DropDownList CssClass="inputLong" ID="ddlPopupCity" runat="server" DataTextField="Name" 
                DataValueField="CityId" />
            <asp:Label runat="server" ID="lblPopupCity" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
        </td>
        <td>
            <asp:Label runat="server" ID="lblReestrCity" CssClass="ReestrDataClass"></asp:Label>
        </td>
    </tr>
    
    <tr>
        <td>
            
        </td>
        <td>
            <asp:Label runat="server" ID="lblReestrFioDirector" CssClass="ReestrDataClass"></asp:Label>
        </td>
    </tr>
    <tr  runat="server" ID="trPhoneNumber">
        <td>
            Телефон*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbPhoneNumber" runat="server"></asp:TextBox>
            <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbPhoneNumber" Mask="8(999)999-99-99" MaskType="Number" />
            <asp:Label runat="server" ID="lblPhoneNumber" Visible="false"></asp:Label><br />
            <asp:RequiredFieldValidator runat="server" ID="val3" ValidationGroup="78" ErrorMessage="Поле 'Телефон' не заполнено" ControlToValidate="tbPhoneNumber" Display="Dynamic"  ></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            
        </td>
        <td>
            <asp:Label runat="server" ID="lblReestrpPnoneNumber" CssClass="ReestrDataClass"></asp:Label>
        </td>
    </tr>
    <tr runat="server" ID="trFaxNumber">
        <td>
            Факс:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbFaxNumber" runat="server"></asp:TextBox>
            <asp:Label runat="server" ID="lblFaxNumber" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr runat="server" ID="trEMail">
        <td>
            EMail*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbEMail" runat="server"></asp:TextBox>
            <asp:Label runat="server" ID="lblEmail" Visible="false"></asp:Label><br />
            <asp:RequiredFieldValidator runat="server" ID="val4" ValidationGroup="78" ErrorMessage="Поле 'EMail' не заполнено" ControlToValidate="tbEMail" Display="Dynamic"  ></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ID="valEmail" ErrorMessage="Неверный формат электронной почты" ValidationGroup="78"
            ControlToValidate="tbEMail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            
        </td>
        <td>
            <asp:Label runat="server" ID="lblReestrEMail" CssClass="ReestrDataClass"></asp:Label>
        </td>
    </tr>
    <tr runat="server" ID="trSiteUrl">
        <td>
            Сайт*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbSiteUrl" runat="server"></asp:TextBox>
            <asp:Label runat="server" ID="lblSiteUrl" Visible="false"></asp:Label><br />
            <asp:RequiredFieldValidator runat="server" ID="val5" ValidationGroup="78" ErrorMessage="Поле 'Сайт' не заполнено" ControlToValidate="tbSiteUrl" Display="Dynamic"  ></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr runat="server" ID="trAccreditation">
        <td>
            Аккредитация*:
        </td>
        <td>
            <uct:UdodLicense ID="ucAccreditation" runat="server" TypeLicense="2" />
        </td>
    </tr>
    <tr runat="server" ID="trLicense">
        <td>
            Лицензия*:
        </td>
        <td>
            <uct:UdodLicense ID="ucLicense" runat="server" TypeLicense="1" />
        </td>
    </tr>
    <tr runat="server" ID="trSector">
        <td>
            Отраслевая принадлежность по ОКВЭД*:
        </td>
        <td>
            <asp:DropDownList CssClass="inputLong" ID="ddlSector" runat="server">
                <items>
                    <asp:ListItem Value="85.1" Text="Образование общее"></asp:ListItem>
                    <asp:ListItem Value="85.11" Text="Образование дошкольное"></asp:ListItem>
                    <asp:ListItem Value="85.12" Text="Образование начальное общее"></asp:ListItem>
                    <asp:ListItem Value="85.13" Text="Образование основное общее"></asp:ListItem>
                    <asp:ListItem Value="85.14" Text="Образование среднее общее"></asp:ListItem>
                    <asp:ListItem Value="85.2" Text="Образование профессиональное"></asp:ListItem>
                    <asp:ListItem Value="85.21" Text="Образование профессиональное среднее"></asp:ListItem>
                    <asp:ListItem Value="85.22" Text="Образование высшее"></asp:ListItem>
                    <asp:ListItem Value="85.22.1" Text="Образование высшее - бакалавриат"></asp:ListItem>
                    <asp:ListItem Value="85.22.2" Text="Образование высшее - специалитет"></asp:ListItem>
                    <asp:ListItem Value="85.22.3" Text="Образование высшее - магистратура"></asp:ListItem>
                    <asp:ListItem Value="85.23" Text="Подготовка кадров высшей квалификации"></asp:ListItem>
                    <asp:ListItem Value="85.3" Text="Обучение профессиональное"></asp:ListItem>
                    <asp:ListItem Value="85.30" Text="Обучение профессиональное"></asp:ListItem>
                    <asp:ListItem Value="85.4" Text="Образование дополнительное"></asp:ListItem>
                    <asp:ListItem Value="85.41" Text="Образование дополнительное детей и взрослых"></asp:ListItem>
                    <asp:ListItem Value="85.41.1" Text="Образование в области спорта и отдыха"></asp:ListItem>
                    <asp:ListItem Value="85.41.2" Text="Образование в области культуры"></asp:ListItem>
                    <asp:ListItem Value="85.41.9" Text="Образование дополнительное детей и взрослых прочее, не включенное в другие группировки"></asp:ListItem>
                    <asp:ListItem Value="85.42" Text="Образование профессиональное дополнительное"></asp:ListItem>
                    <asp:ListItem Value="85.42.1" Text="Деятельность школ подготовки водителей автотранспортных средств"></asp:ListItem>
                    <asp:ListItem Value="85.42.2" Text="Деятельность школ обучения вождению воздушных и плавательных судов, без выдачи коммерческих сертификатов и лицензий"></asp:ListItem>
                    <asp:ListItem Value="85.42.9" Text="Деятельность по дополнительному профессиональному образованию прочая, не включенная в другие группировки"></asp:ListItem>
                </items>
            </asp:DropDownList>
        </td>
    </tr>
    <tr runat="server" ID="trOwnerShip">
        <td>
            Учредители*:
        </td>
        <td>
            <asp:DropDownList CssClass="inputLong" ID="ddlOwnerShip" runat="server">
                <items>
                    <asp:ListItem Value="1" Text="Российская Федерация" ></asp:ListItem>
                    <asp:ListItem Value="2" Text="Субъект Российской Федерации" ></asp:ListItem>
                    <asp:ListItem Value="3" Text="Муниципальное образование" ></asp:ListItem>
                    <asp:ListItem Value="4" Text="Российские коммерческие организации" ></asp:ListItem>
                    <asp:ListItem Value="5" Text="Иностранные коммерческие организации" ></asp:ListItem>
                    <asp:ListItem Value="6" Text="Российские некоммерческие организации" ></asp:ListItem>
                    <asp:ListItem Value="7" Text="Иностранные некоммерческие организации" ></asp:ListItem>
                    <asp:ListItem Value="8" Text="Российские религиозные организации" ></asp:ListItem>
                    <asp:ListItem Value="9" Text="Иностранные религиозные организации" ></asp:ListItem>
                    <asp:ListItem Value="10" Text="Граждане Российской Федерации" ></asp:ListItem>
                    <asp:ListItem Value="11" Text="Иностранные граждане" ></asp:ListItem>
                </items>
            </asp:DropDownList>
        </td>
    </tr>
    <tr runat="server" ID="trTypeUdod">
        <td>
            Тип организации, осуществляющей образовательную деятельность*:
        </td>
        <td>
            <asp:DropDownList CssClass="inputLong" ID="ddlTypeUdod" runat="server">
                <items>
                    <asp:ListItem Value="1" Text="Образовательные организации"></asp:ListItem>
                    <asp:ListItem Value="11" Text="Дошкольная образовательная организация."></asp:ListItem>
                    <asp:ListItem Value="12" Text="Общеобразовательная организация "></asp:ListItem>
                    <asp:ListItem Value="13" Text="Общеобразовательная организация, осуществляющая образовательную деятельность по адаптированным основным общеобразовательным программам "></asp:ListItem>
                    <asp:ListItem Value="1301" Text="для глухих"></asp:ListItem>
                    <asp:ListItem Value="1302" Text="для слабослышащих"></asp:ListItem>
                    <asp:ListItem Value="1303" Text="для позднооглохших"></asp:ListItem>
                    <asp:ListItem Value="1304" Text="для слепых"></asp:ListItem>
                    <asp:ListItem Value="1305" Text="для слабовидящих"></asp:ListItem>
                    <asp:ListItem Value="1306" Text="с тяжелыми нарушениями речи"></asp:ListItem>
                    <asp:ListItem Value="1307" Text="с нарушениями опорно-двигательного аппарата"></asp:ListItem>
                    <asp:ListItem Value="1308" Text="с задержкой психического развития"></asp:ListItem>
                    <asp:ListItem Value="1309" Text="с умственной отсталостью"></asp:ListItem>
                    <asp:ListItem Value="1310" Text="с расстройствами аутистического спектра"></asp:ListItem>
                    <asp:ListItem Value="1311" Text="со сложными дефектами"></asp:ListItem>
                    <asp:ListItem Value="1312" Text="для других обучающихся с ограниченными возможностями здоровья"></asp:ListItem>
                    <asp:ListItem Value="14" Text="Профессиональная образовательная организация."></asp:ListItem>
                    <asp:ListItem Value="15" Text="Образовательная организация высшего образования"></asp:ListItem>
                    <asp:ListItem Value="16" Text="Организация дополнительного образования."></asp:ListItem>
                    <asp:ListItem Value="17" Text="Организация дополнительного профессионального образования."></asp:ListItem>
                    <asp:ListItem Value="2" Text="Организации, осуществляющие обучение"></asp:ListItem>
                    <asp:ListItem Value="21" Text="Научная организация"></asp:ListItem>
                    <asp:ListItem Value="22" Text="Центр психолого-педагогической, медицинской и социальной помощи"></asp:ListItem>
                    <asp:ListItem Value="23" Text="Организация для детей-сирот и детей, оставшихся без попечения родителей"></asp:ListItem>
                    <asp:ListItem Value="24" Text="Организация, осуществляющая лечение, оздоровление и (или) отдых"></asp:ListItem>
                    <asp:ListItem Value="25" Text="Организация, осуществляющая социальное обслуживание"></asp:ListItem>
                    <asp:ListItem Value="26" Text="Дипломатическое представительство Российской Федерации"></asp:ListItem>
                    <asp:ListItem Value="27" Text="Консульское учреждение Российской Федерации"></asp:ListItem>
                    <asp:ListItem Value="28" Text="Представительство Российской Федерации при международных (межгосударственных, межправительственных) организациях"></asp:ListItem>
                    <asp:ListItem Value="29" Text="Иные юридические лица"></asp:ListItem>
                    <asp:ListItem Value="3" Text="Индивидуальный предприниматель, осуществляющий образовательную деятельность"></asp:ListItem>
                </items>
            </asp:DropDownList>
        </td>
    </tr>
    <tr runat="server" ID="trActivity">
        <td>
            Вид деятельности*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbActivity" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ValidationGroup="78" ErrorMessage="Поле 'Вид деятельности' не заполнено" ControlToValidate="tbActivity" Display="Dynamic"  ></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr runat="server" ID="trLegalForm">
        <td>
            Организационно-правовая форма*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbLegalForm" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ValidationGroup="78" ErrorMessage="Поле 'Организационно-правовая форма' не заполнено" ControlToValidate="tbLegalForm" Display="Dynamic"  ></asp:RequiredFieldValidator>
        </td>
    </tr>

    <tr runat="server" ID="trMaxPupils">
        <td>
            Предельная наполняемость*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbMaxPupils" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ValidationGroup="78" ErrorMessage="Поле 'Предельная наполняемость' не заполнено" ControlToValidate="tbMaxPupils" Display="Dynamic"  ></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr runat="server" ID="trOKOGU">
        <td>
            Принадлежность к государственным или муниципальным органам управления*:
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbOKOGU" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ValidationGroup="78" ErrorMessage="Поле 'Принадлежность к государственным или муниципальным органам управления' не заполнено" ControlToValidate="tbOKOGU" Display="Dynamic"  ></asp:RequiredFieldValidator>
        </td>
    </tr>
    
    <tr runat="server" ID="trStudyForm">
        <td>
            форма обучения*:
        </td>
        <td>
            <asp:DropDownList CssClass="inputLong" ID="ddlStudyForm" runat="server">
                <items>
                    <asp:ListItem Text="Привет" Value="0"></asp:ListItem>
                </items>
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator13" ValidationGroup="78" ErrorMessage="Поле 'форма обучения' не заполнено" ControlToValidate="tbSector" Display="Dynamic"  ></asp:RequiredFieldValidator>
        </td>
    </tr>
    
    

    <tr>
        <td>
            
        </td>
        <td>
            <asp:Label runat="server" ID="lblReestrSiteUrl" CssClass="ReestrDataClass"></asp:Label>
        </td>
    </tr>
    </table>
    <table>
    <tr>
        <th colspan="2" bgcolor="f2f2f2" align="left">
            Дополнительное образование
        </th>
        
    </tr>
    <tr>
        <td>
            <asp:Label runat="server" ID="lblIsDOEnabled" Width="670">Есть ли в учреждении блок дополнительного образования </asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="ddlIsDOEnabled" runat="server" CssClass="input">
                <asp:ListItem Text="Да" Value="1" />
                <asp:ListItem Text="Нет" Value="0" />
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label runat="server" ID="Label1" Width="670">Есть ли в Государственном задании учреждения услуга "Предоставление дополнительного образования, в том числе для детей с ограниченными возможностями здоровья и детей-инвалидов (в том числе с использованием дистанционных образовательных технологий)</asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="ddlInvalid" runat="server" CssClass="input">
                <asp:ListItem Text="Нет" Value="0" Selected="true"/>
                <asp:ListItem Text="Да" Value="1" />
            </asp:DropDownList>
        </td>
    </tr>
</table>

<asp:ObjectDataSource ID="dsMainsUdod" TypeName="Udod.Dal.UdodDb"  runat="server" 
        SelectMethod="GetMainUdods">
</asp:ObjectDataSource>

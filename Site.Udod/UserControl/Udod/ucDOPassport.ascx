﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucDOPassport.ascx.cs" Inherits="UserControl_Udod_ucDOPassport" %>
<%@ Register src="../Menu/ucErrorPopup.ascx" tagName="ErrorPopup" tagPrefix="uc" %>

<script type="text/javascript">

    
    var cbxTypeBudget = '<%= cbxTypeBudget.ClientID %>';
    function isValidTypeBudget(source, args) {
        args.IsValid = $('#'+cbxTypeBudget).find("input:checked").length > 0;
    }
    var cbxEducationType = '<%= cbxEducationType.ClientID %>';
    function isValidEducationType(source, args) {
        args.IsValid = $('#' + cbxEducationType).find("input:checked").length > 0;
    }
    var cbxAddress = '<%= cbxAddress.ClientID %>';
    var cbxOffice = '<%= cblOffice.ClientID %>';
    function isValidAddress(source, args) {
        args.IsValid = $('#' + cbxAddress).find("input:checked").length > 0;
        //if ($('#'+cbxAddress).find("input").length == 0) args.IsValid = true;
    }

    function isValidProgramService(source, args) {
        if ($('#' + '<%= tbProgramService.ClientID %>').val().length > 10) args.IsValid = true;
        else args.IsValid = false;
    }

    function isValidTypeFinanse(source, args) {
        var ischeckedPlatno = false;
        $('#' + cbxTypeBudget).find("tr").each(function () {
            var isnotBudget = false;
            $(this).find("label").each(function () {
                isnotBudget = this.innerHTML == "Платно";
            });
            if ($(this).find("input:checked").length > 0 && isnotBudget) {
                ischeckedPlatno = true;
            }
        });
        if (ischeckedPlatno && $('#' + cbxTypeBudget).find("input:checked").length == 1 && $('#' + '<%= ddlTypeFinance.ClientID %>').val()=="1") {
            args.IsValid = false;
        } else
            args.IsValid = true;
    }
    
    function isValidFinansing(source, args) {
        var ischeckedPlatno = false;
        $('#' + cbxTypeBudget).find("tr").each(function () {
            var isnotBudget = false;
            $(this).find("label").each(function () {
                isnotBudget = this.innerHTML == "Платно";
            });
            if ($(this).find("input:checked").length > 0 && isnotBudget) {
                ischeckedPlatno = true;
            }
        });
        if (ischeckedPlatno && parseInt($('#' + '<%= tbFinansing.ClientID %>').val()) == 0) {
            args.IsValid = false;
        } else
            args.IsValid = true;
    }
    
    function isValidSchedule(source, args) {
        args.IsValid =
            ($('#' + '<%= cbMonday.ClientID %>').attr('checked') == 'checked' ||
            $('#' + '<%= cbThursday.ClientID %>').attr('checked') == 'checked' ||
            $('#' + '<%= cbTuesday.ClientID %>').attr('checked') == 'checked' ||
            $('#' + '<%= cbFriday.ClientID %>').attr('checked') == 'checked' ||
            $('#' + '<%= cbWednesday.ClientID %>').attr('checked') == 'checked' ||
            $('#' + '<%= cbSaturday.ClientID %>').attr('checked') == 'checked' ||
            $('#' + '<%= cbSunday.ClientID %>').attr('checked') == 'checked');
    }
    function isValidOffice(source, args) {
        args.IsValid = $('#' + cbxOffice).find("input:checked").length > 0;
        if ($('#' + cbxOffice).find("input").length == 0) args.IsValid = true;
    }
    var tbFinansing = '<%= tbFinansing.ClientID %>';
    var tbSubFinansing = '<%= tbsubFinance.ClientID %>';

    function bindtextboxFio(sender, args) {
        //$('#' + tbFinansing).bind("keypress", tbKeyUp);
        //$('#' + tbSubFinansing).bind("keypress", tbKeyUp);
    }
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(bindtextboxFio);
    function tbKeyUp(event) {
        //
        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode == 46)) {
            //alert('ok');
            return true;
        }
        else return false;
    }
</script>


<table border=0>
    <tr>
        <td>
            Название детского объединения*:
        </td>
        <td colspan=3>
            <asp:TextBox class="inputLong" ID="tbName" runat="server"></asp:TextBox>
            <asp:Label ID="lblName" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td colspan=3>
            <asp:RequiredFieldValidator runat="server" ID="val1" ValidationGroup="79" ErrorMessage="Поле 'Название' не заполнено" ControlToValidate="tbName" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Направленность:
        </td>
        <td colspan=3>
            <asp:DropDownList class="inputLong" ID="ddlProgram" runat="server" DataTextField="Name" 
                DataValueField="ProgramId" OnSelectedIndexChanged="ProgramIndex_Changed" AutoPostBack="true"/>
            <asp:Label ID="lblProgram" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Профиль:
        </td>
        <td colspan=3>
            <asp:DropDownList class="inputLong" ID="ddlProfile" runat="server" DataTextField="Name" 
                DataValueField="ProfileId" OnSelectedIndexChanged="ProfileIndex_Changed" AutoPostBack="true"/>
            <asp:Label ID="lblProfile" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Основной вид деятельности:
        </td>
        <td colspan=3>
            <asp:DropDownList class="inputLong" ID="ddlSection" runat="server" DataTextField="Name" 
                DataValueField="SectionId" OnSelectedIndexChanged="SectionIndex_Changed" AutoPostBack="true"/>
            <asp:Label ID="lblSection" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="3">Наличие для данного детского объединения утверждённой программы дополнительного образования</td>
        <td style="padding-left: 50px;">
            <asp:DropDownList ID="ddlIsDopProgram" runat="server" CssClass="input">
                <asp:ListItem Text="Нет" Value="0" Selected="true"/>
                <asp:ListItem Text="Да" Value="1" />
            </asp:DropDownList>
            <asp:Label ID="lblIsDopProgram" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="3">Публиковать данное детское объединение на Портале гос-услуг? (публиковать - доступна всем, не публиковать - доступна только контингенту ОУ через раздел "Контингент школы")</td>
        <td style="padding-left: 50px;">
            <asp:DropDownList ID="ddlIsPublicPortal" runat="server" CssClass="input">
                <asp:ListItem Text="Да" Value="1" Selected="true"/>
                <asp:ListItem Text="Нет" Value="0" />
            </asp:DropDownList>
            <asp:Label ID="lblIsPublicPortal" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="3">Ведётся набор детей в группы данного детского объединения</td>
        <td style="padding-left: 50px;">
            <asp:DropDownList ID="ddlIsReception" runat="server" CssClass="input">
                <asp:ListItem Text="Да" Value="1" Selected="true"/>
                <asp:ListItem Text="Нет" Value="0" />
            </asp:DropDownList>
            <asp:Label ID="lblIsReception" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label runat="server" ID="lblProgramCategory" Text="Категория программы:" />
        </td>
        <td colspan=3>
            <asp:DropDownList class="inputLong" ID="ddlProgramCategory" runat="server" DataTextField="ProgramCategoryName" 
                DataValueField="ProgramCategoryId" AutoPostBack="true"/>
            <asp:Label runat="server" ID="lblProgrCateg" />
        </td>
    </tr>
    <tr runat="server" ID="trStatus" style="display: none;">
        <td>Статус:</td>
        <td colspan="3">
            <asp:DropDownList class="inputLong" ID="ddlStatus" runat="server" DataTextField="Name" DataSourceID="dsStatus"
                DataValueField="DictId" OnDataBound="ddlStatus_OnDataBound" />
        </td>
    </tr>
    <tr runat="server" ID="trProgramService">
        <td>Описание услуги:</td>
        <td colspan="3">
            <asp:TextBox runat="server" ID="tbProgramService" CssClass="inputLong" Rows="3" TextMode="MultiLine"></asp:TextBox>
            <asp:CustomValidator runat="server" ID="CustomValidator6" ValidationGroup="79" ClientValidationFunction="isValidProgramService" EnableClientScript="true" Display="Dynamic" ErrorMessage="Слишком короткое описание" ></asp:CustomValidator>
        </td>
    </tr>
    <tr style="display: none;">
        <td>Порядок зачисления на обучение:</td>
        <td colspan="3">
            <asp:TextBox runat="server" ID="tbRuleService" CssClass="inputLong" Rows="3" TextMode="MultiLine"></asp:TextBox>
        </td>
    </tr>
    <tr style="display: none;">
        <td>Срок подачи документов:</td>
        <td colspan="3">
            <asp:TextBox runat="server" ID="tbDocDeadLine" CssClass="input">7</asp:TextBox>
            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ValidationGroup="79" ErrorMessage="*" SetFocusOnError="true" ControlToValidate="tbDocDeadLine" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="79" ErrorMessage="Поле обязательно для заполнения" ControlToValidate="tbDocDeadLine"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr style="display: none;">
        <td>Наличие предварительных испытаний:</td>
        <td>
            <asp:CheckBox runat="server" ID="cbTestService" Checked="false"/>
         </td>
         <td>
            Количество туров предварительных испытаний:
         </td>
        <td>
            <asp:TextBox runat="server" ID="tbTourNumber" CssClass="input">0</asp:TextBox>
            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator2" ValidationGroup="79" ErrorMessage="*" SetFocusOnError="true" ControlToValidate="tbTourNumber" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ValidationGroup="79" ErrorMessage="Поле обязательно для заполнения" ControlToValidate="tbTourNumber"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Прием ведется только на второй и последующие года:
        </td>
        <td colspan=3>
            <asp:CheckBox runat="server" ID="cbxIsActive" Text=""/>
            <asp:Label runat="server" ID="lblIsActive" />   
        </td>
    </tr>
    <tr>
        <td>
            Возраст обучающихся:
        </td>
        <td>
            <asp:UpdatePanel ID="upAgeStart" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Label ID="lblAge" runat="server" Text="от"/>
                    <asp:TextBox class="input" ID="tbAgeStart" runat="server" AutoPostBack="true" OnTextChanged="AgeChanged"/>
                    <asp:RegularExpressionValidator runat="server" ID="val2" ValidationGroup="79" ErrorMessage="*" SetFocusOnError="true" ControlToValidate="tbAgeStart" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator runat="server" ID="val3" ValidationGroup="79" ErrorMessage="Поле обязательно для заполнения" ControlToValidate="tbAgeStart"></asp:RequiredFieldValidator>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
        <td>
            <asp:UpdatePanel ID="upAgeEnd" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Label ID="lblAge2" runat="server" Text="до" />
                    <asp:TextBox class="input" ID="tbAgeEnd" runat="server" AutoPostBack="true" OnTextChanged="AgeChanged"/>
                    <asp:RegularExpressionValidator runat="server" ID="val4" ValidationGroup="79" ErrorMessage="*" SetFocusOnError="true" ControlToValidate="tbAgeEnd" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator runat="server" ID="val5" ValidationGroup="79" ErrorMessage="Поле обязательно для заполнения" ControlToValidate="tbAgeEnd"></asp:RequiredFieldValidator>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
        <td>
            <asp:CompareValidator Display="Dynamic" ID="val6" runat="server" ControlToValidate="tbAgeEnd" ControlToCompare="tbAgeStart" Type="Integer" Operator="GreaterThanEqual" ErrorMessage="Первое значение должно быть меньше второго" ValidationGroup="79"  />
            <asp:RangeValidator Display="Dynamic" runat="server" ID="rangeval1" ValidationGroup="79" ErrorMessage="Возраст может быть от 3 до 21" ControlToValidate="tbAgeStart" MinimumValue="3" MaximumValue="21" Type="Integer"></asp:RangeValidator>
        </td>
    </tr>
    <tr runat="server" ID="trSex">
        <td>
            <asp:Label runat="server" ID="lblSex" Text="Пол обучающихся:" />
        </td>
        <td>
            <asp:DropDownList class="input" ID="ddlSex" runat="server" DataTextField="Name" DataSourceID="dsSex"
                DataValueField="DictId" />
        </td>
        <td>
            Минимальное кол-во детей в группах данного ДО:
        </td>
        <td>
            <asp:TextBox runat="server" ID="tbMinCountPupil" CssClass="input"></asp:TextBox>
            <asp:Label runat="server" ID="lblMinCountPupil"></asp:Label>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ValidationGroup="79" ErrorMessage="Поле обязательно для заполнения" ControlToValidate="tbMinCountPupil"></asp:RequiredFieldValidator>
            <asp:CompareValidator Display="Dynamic" ID="CompareValidator2" runat="server" ControlToValidate="tbMinCountPupil" ValueToCompare="0" Type="Integer" Operator="GreaterThan" ErrorMessage="Значение должно быть больше нуля" ValidationGroup="79"  />
        </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>
            Вид услуги:
        </td>
        <td >
            <asp:UpdatePanel runat="server">
            <ContentTemplate>
            <asp:Label ID="lblTypeBudget" runat="server" />
            <asp:CheckBoxList ID="cbxTypeBudget" runat="server" DataTextField="TypeBudgetName" DataValueField="DictTypeBudgetId"/>
            <asp:CustomValidator runat="server" ID="valCheckBoxList" ValidationGroup="79" ClientValidationFunction="isValidTypeBudget" EnableClientScript="true" Display="Dynamic" ErrorMessage="Необходимо отметить хотя бы одно значение" ></asp:CustomValidator>
            </ContentTemplate>
            </asp:UpdatePanel>

        </td>
        <td runat="server" ID="tdTypeHeader">Тип финансирования:</td>
        <td>
            <asp:DropDownList class="input" ID="ddlTypeFinance" runat="server" DataTextField="Name" DataSourceID="dsTypeFinance"
                DataValueField="DictId" />
            <asp:CustomValidator runat="server" ID="CustomValidator7" ValidationGroup="79" ClientValidationFunction="isValidTypeFinanse" EnableClientScript="true" Display="Dynamic" ErrorMessage="Финансирование не может быть бюджетным при платном виде услуги" ></asp:CustomValidator>
        </td>
    </tr>
    <tr runat="server" ID="trFinansing">
        <td>Стоимость оказания услуги:</td>
        <td>
            <asp:TextBox runat="server" ID="tbFinansing" CssClass="input"></asp:TextBox>
            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator3" ValidationGroup="79" ErrorMessage="*" SetFocusOnError="true" ControlToValidate="tbFinansing" ValidationExpression="\d+[.]{0,1}\d*"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ValidationGroup="79" ErrorMessage="Поле обязательно для заполнения" ControlToValidate="tbFinansing"></asp:RequiredFieldValidator>
            <asp:CustomValidator runat="server" ID="CustomValidator5" ValidationGroup="79" ClientValidationFunction="isValidFinansing" EnableClientScript="true" Display="Dynamic" ErrorMessage="Стоимость должна быть отлична от нуля" ></asp:CustomValidator>
         </td>
         <td>
            Процент субсидии оказания услуги:
         </td>
        <td>
            <asp:TextBox runat="server" ID="tbsubFinance" CssClass="input"></asp:TextBox>
            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator4" ValidationGroup="79" ErrorMessage="*" SetFocusOnError="true" ControlToValidate="tbsubFinance" ValidationExpression="\d+[.]{0,1}\d*"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ValidationGroup="79" ErrorMessage="Поле обязательно для заполнения" ControlToValidate="tbsubFinance"></asp:RequiredFieldValidator>
            
        </td>
    </tr>
    <tr>
        <td style="padding-bottom: 20px;">
            Тип стоимости:
        </td>
        <td style="padding-bottom: 20px;">
            <asp:DropDownList class="input" ID="ddlTypeValueService" runat="server" DataTextField="Name" DataSourceID="dsTypeValueService"
                DataValueField="DictId" />
            <asp:Label runat="server" ID="lblTypeValueService" />
        </td>
        <td colspan="2" style="padding-bottom: 20px;"></td>
    </tr>
    <tr>
        <td>
            ФИО Педагога(ов):
        </td>
        <td colspan=3>
            <asp:Label ID="lblTeacherFioNoEdit" runat="server" Visible="false" />
            <asp:UpdatePanel runat="server" ID="upTeacher" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Repeater runat="server" ID="repTeacher">
                <ItemTemplate>
                    <span>
                        <asp:Label ID="lblTeacherFio" runat="server" ><%# Eval("TeacherName")%></asp:Label>
                    </span>
                </ItemTemplate>
                </asp:Repeater>
                <asp:LinkButton runat="server" ID="lnkChangeTeacher" OnClick="lnkChangeTeacher_OnClick"><div class="btnBlue">Изменить</div></asp:LinkButton>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnCancelTeacher"/>
            </Triggers>
        </asp:UpdatePanel>
        </td>
    </tr>
    <tr>
        <td>
            Предполагаемый срок обучения:
        </td>
        <td colspan=3>
            <asp:Label ID="lblNumYears" runat="server" />
            <asp:DropDownList class="inputLong" ID="ddlNumYears" runat="server" >
                <Items>
                    <asp:ListItem Value="1" Text="1" />
                    <asp:ListItem Value="2" Text="2" />
                    <asp:ListItem Value="3" Text="3" />
                    <asp:ListItem Value="4" Text="4" />
                    <asp:ListItem Value="5" Text="5" />
                    <asp:ListItem Value="6" Text="6" />
                    <asp:ListItem Value="7" Text="7" />
                    <asp:ListItem Value="8" Text="8" />
                    <asp:ListItem Value="9" Text="9" />
                    <asp:ListItem Value="10" Text="10" />
                    <asp:ListItem Value="11" Text="11" />
                    <asp:ListItem Value="12" Text="12" />
                    <asp:ListItem Value="13" Text="13" />
                    <asp:ListItem Value="14" Text="14" />
                    <asp:ListItem Value="15" Text="15" />
                </Items>
            </asp:DropDownList>
        </td>
    </tr>
    <tr runat="server" ID="trPeriod">
        <td>Период прохождения занятий:</td>
        <td>
            <asp:TextBox runat="server" ID="tbStartDate" CssClass="input"></asp:TextBox>
            <ajaxToolkit:CalendarExtender runat="server" ID="calendar" TargetControlID="tbStartDate" Animated="false" Format="dd.MM.yyyy"/>
            <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbStartDate" Mask="99/99/9999" MaskType="Date" />
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="reqvalrDate" ErrorMessage="Поле 'Начало занятий' не заполнено" ControlToValidate="tbStartDate" SetFocusOnError="true" ValidationGroup="79"></asp:RequiredFieldValidator>
             <asp:CompareValidator Display="Dynamic" ID="CompareValidator1" runat="server" ControlToValidate="tbEndDate" ControlToCompare="tbStartDate" Type="Date" Operator="GreaterThanEqual" ErrorMessage="Первое значение должно быть меньше второго" ValidationGroup="79"  />
                                               
         </td>
        <td>
            <asp:TextBox runat="server" ID="tbEndDate" CssClass="input"></asp:TextBox>
            <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="tbEndDate" Animated="false" Format="dd.MM.yyyy"/>
            <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="tbEndDate" Mask="99/99/9999" MaskType="Date" />
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="RequiredFieldValidator1" ErrorMessage="Поле 'Окончание занятий' не заполнено" ControlToValidate="tbEndDate" SetFocusOnError="true" ValidationGroup="79"></asp:RequiredFieldValidator>
            
        </td>
    </tr>
    <tr>
        <td>
            Основная форма занятий:
        </td>
        <td >
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            <asp:Label ID="lblEducationType" runat="server" />
            <asp:CheckBoxList ID="cbxEducationType" runat="server" DataTextField="EducationTypeName" DataValueField="EducationTypeId" />
            <asp:CustomValidator runat="server" ID="CustomValidator1" ValidationGroup="79" EnableClientScript="true" ClientValidationFunction="isValidEducationType"  ControlToValidate="ddlSection"  Display="Dynamic" ErrorMessage="Необходимо отметить хотя бы одно значение" ></asp:CustomValidator>
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>
        <td style="display: none;">
            Форма оказания услуги:
        </td>
        <td style="display: none;">
            <asp:DropDownList class="input" ID="ddlForm" runat="server" DataTextField="Name" DataSourceID="dsForm"
                DataValueField="DictId" OnDataBound="ddlForm_OnDataBound"/>
        </td>
    </tr>
    <tr>
        <td>
            Примечание:
        </td>
        <td colspan=3>
            <asp:TextBox class="inputLong" ID="tbComment" runat="server"></asp:TextBox>
            <asp:Label ID="lblComment" runat="server" />
        </td>
    </tr>
    <%--<tr>
        <td>
            Продолжительность и режим занятий:
        </td>
        <td colspan=3 align="right">
            <asp:UpdatePanel runat="server" ID="upGridView" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="gvLessons" runat="server" CssClass="dgc" AutoGenerateColumns="false" OnRowDeleting="gvLessons_RowDeleting"
                                DataKeyNames="UdodAgeGroupId" OnRowDataBound="gvLessons_OnRowDataBound" OnRowCreated="gvLessons_RowCreated" 
                                OnRowEditing="gvLessons_OnRowEditing" OnRowCancelingEdit="gvLessons_OnRowCancelingEdit" OnRowUpdating="gvLessons_OnRowUpdating" style="width: 100%;">
                        <EmptyDataTemplate>
                            <asp:Label ID="lblEmptyGvLessons" runat="server" style="font-size:16px">Нет возрастных групп</asp:Label>
                        </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Активно" >
                            <ItemTemplate>
                                <asp:CheckBox runat="server" Width="50px" Enabled="false" Checked='<%#Eval("IsActive")%>'></asp:CheckBox>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CheckBox runat="server" CssClass="input"  Width="50px" ID="cbIsActive" Checked='<%#Eval("IsActive")%>'></asp:CheckBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="от" >
                            <ItemTemplate>
                                <%# Eval("NumYearStart") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox runat="server" CssClass="input"  Width="50px" ID="tbNumYearStart" Text='<%#Eval("NumYearStart")%>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="до" >
                            <ItemTemplate>
                                <%# Eval("NumYearEnd") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox runat="server" CssClass="input"  Width="50px" ID="tbNumYearEnd" Text='<%#Eval("NumYearEnd")%>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="от" >
                            <ItemTemplate>
                                <%# Eval("AgeStart") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox runat="server" CssClass="input"  Width="50px" ID="tbAgeStart" Text='<%#Eval("AgeStart")%>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="до" >
                            <ItemTemplate>
                                <%# Eval("AgeEnd") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox runat="server" CssClass="input"  Width="50px" ID="tbAgeEnd" Text='<%#Eval("AgeEnd")%>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="1 занятия (мин)">
                            <ItemTemplate>
                                <%# Eval("LessonLength")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox runat="server" CssClass="input"  Width="70px" ID="tbLessonLength" Text='<%#Eval("LessonLength")%>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="перемены (мин)">
                            <ItemTemplate>
                                <%# Eval("BreakLength")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox runat="server" CssClass="input"  Width="70px" ID="tbBreakLength" Text='<%#Eval("BreakLength")%>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Количество занятий в неделю">
                            <ItemTemplate>
                                <%# Eval("LessonsInWeek")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox runat="server" CssClass="input"  Width="70px" ID="tbLessonsInWeek" Text='<%#Eval("LessonsInWeek")%>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="true" EditText="<div class='btnBlue'>Изменить</div>" ShowDeleteButton="true" ButtonType="Link" UpdateText="<div class='btnBlue'>Сохранить</div>" CancelText="<div class='btnBlue'>Отмена</div>" DeleteText="<div class='btnBlue'>Удалить</div>"/>
                    </Columns>
                    <PagerStyle CssClass="gridpager" VerticalAlign="Bottom" HorizontalAlign="Left"/>
                    <PagerSettings Mode="Numeric" Position="Bottom" PageButtonCount="10" />
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger runat="server" ControlID="gvLessons"/>
                <asp:AsyncPostBackTrigger runat="server" ControlID="tbAgeStart" />
                <asp:AsyncPostBackTrigger runat="server" ControlID="tbAgeEnd" />
            </Triggers>
            </asp:UpdatePanel>
            <asp:LinkButton  runat="server" ID="lnkAddAgeGroup" OnClick="lnkAddAgeGroup_OnClick"><div class="btnBlue">Добавить</div></asp:LinkButton>
        </td>
    </tr>--%>
    <tr runat="server" ID="trSchedule">
        <td>
            Упрощённое расписание занятий:
        </td>
        <td colspan="3">
            <table>
                <tr>
                    <td><asp:CheckBox runat="server" ID="cbMonday"/>Понедельник</td>
                    <td><asp:DropDownList runat="server" ID="ddlMonday" DataSourceID="dsTimeday" DataTextField="Name" DataValueField="DictId"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="cbTuesday"/>Вторник</td>
                    <td><asp:DropDownList runat="server" ID="ddlTuesday" DataSourceID="dsTimeday" DataTextField="Name" DataValueField="DictId"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="cbWednesday"/>Среда</td>
                    <td><asp:DropDownList runat="server" ID="ddlWednesday" DataSourceID="dsTimeday" DataTextField="Name" DataValueField="DictId"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="cbThursday"/>Четверг</td>
                    <td><asp:DropDownList runat="server" ID="ddlThursday" DataSourceID="dsTimeday" DataTextField="Name" DataValueField="DictId"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="cbFriday"/>Пятница</td>
                    <td><asp:DropDownList runat="server" ID="ddlFriday" DataSourceID="dsTimeday" DataTextField="Name" DataValueField="DictId"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="cbSaturday"/>Суббота</td>
                    <td><asp:DropDownList runat="server" ID="ddlSaturday" DataSourceID="dsTimeday" DataTextField="Name" DataValueField="DictId"/></td>
                </tr>
                <tr>
                    <td><asp:CheckBox runat="server" ID="cbSunday"/>Воскресенье</td>
                    <td><asp:DropDownList runat="server" ID="ddlSunday" DataSourceID="dsTimeday" DataTextField="Name" DataValueField="DictId"/></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CustomValidator runat="server" ID="CustomValidator4" ValidationGroup="79" EnableClientScript="true" ClientValidationFunction="isValidSchedule"  Display="Dynamic" ErrorMessage="Необходимо отметить хотя бы одно значение" ></asp:CustomValidator>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan=4>
            <asp:UpdatePanel ID="upAgeGroupWarning" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Label ID="lblAgeGroupWarning" runat="server" Visible="false" Text="<div style='color:Red'>Были изменены возрастные группы, пожалуйста, корректно сохраните детское объединение, иначе данное ДО будет недоступно.</div>" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr>
        <td>
            Адрес проведения занятий:
        </td>
        <td colspan=3>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div style="overflow-y:scroll; height:100px">
                    <asp:CheckBoxList ID="cbxAddress" runat="server" DataTextField="AddressStr" 
                        DataValueField="AddressId" />
                    <asp:Label ID="lblAddress" runat="server" />
                </div>
                <asp:CustomValidator runat="server" ID="CustomValidator2" ValidationGroup="79" EnableClientScript="true" ClientValidationFunction="isValidAddress"  Display="Dynamic" ErrorMessage="Необходимо отметить хотя бы одно значение" ></asp:CustomValidator>
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr style="display: none;">
        <td>
            Адрес пункта работы с заявителем:
        </td>
        <td colspan=3>
            <asp:UpdatePanel ID="upOffice" runat="server">
            <ContentTemplate>
                <div style="overflow-y:scroll; height:100px">
                    <asp:CheckBoxList ID="cblOffice" runat="server" DataTextField="AddressStr" 
                        DataValueField="AddressId" />
                    <asp:Label ID="Label1" runat="server" />
                </div>
                <asp:CustomValidator runat="server" ID="CustomValidator3" Enabled="false" ValidationGroup="79" EnableClientScript="true" ClientValidationFunction="isValidOffice"  Display="Dynamic" ErrorMessage="Необходимо отметить хотя бы одно значение" ></asp:CustomValidator>
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>

<asp:Panel runat="server" ID="popupSelectTeacher" style="padding: 5px; display: none; border: 2px solid black" BackColor="White">
    <asp:Panel ID="pHeaderTeacher" runat="server" >
        <asp:Label ID="Label4" Text="<span class='headerCaptions'>Педагоги</span>" runat="server"/>
        <asp:LinkButton ID="LinkButton2" runat="server" 
            OnClientClick="$find('SelectTeacherPopup').hide(); return false;" />
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upSelectTeacher" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:DropDownList runat="server" ID="ddlTeacher" DataTextField="Fio" DataValueField="UserId"/>
        <div style="font-size: 0px; height: 5px;"></div> 
        <asp:LinkButton  runat="server" ID="lnkAddTeacher" OnClick="lnkAddTeacher_OnClick"><div class="btnBlue">Добавить педагога</div></asp:LinkButton>
        <div style="font-size: 0px; height: 5px;"></div> 
        <asp:GridView runat="server" ID="gvChildUnionTeacher" CssClass="dgc" EmptyDataText="Нет педагогов" DataKeyNames="ChildUnionTeacherId" OnRowDataBound="gvChildUnionTeacher_OnRowDataBound" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField  HeaderText="№">
                    <ItemTemplate>
                        <%# (gvChildUnionTeacher.Rows.Count + 1)%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField  HeaderText="Педагог">
                    <ItemTemplate>
                        <%# Eval("TeacherName")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField  HeaderText="Удалить">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="lnkDeleteTeacher" OnClick="lnkDeleteTeacher_OnClick"><div class="btnBlue">Удалить</div></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="lnkChangeTeacher" />
    </Triggers>
    </asp:UpdatePanel>
        <div >
        <asp:LinkButton ID="btnCancelTeacher" runat="server" Text="<div class='btnBlue'>OK</div>" CausesValidation="false" OnClick="btnCancelTeacher_OnClick"/>
        </div>
        
</asp:Panel>
    
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupSelectTeacherExtender" PopupDragHandleControlID="pHeaderTeacher"
        PopupControlID="popupSelectTeacher" Y="200"
        TargetControlID="btnShowSelectTeacher" RepositionMode="None"
        BehaviorID="SelectTeacherPopup"/>
    
<asp:Button ID="btnShowSelectTeacher" runat="server" style="display:none" />

<asp:ObjectDataSource runat="server" ID="dsSex" TypeName="Udod.Dal.ESZDb" SelectMethod="GetSexs"></asp:ObjectDataSource>
<asp:ObjectDataSource runat="server" ID="dsForm" TypeName="Udod.Dal.ESZDb" SelectMethod="GetForms"></asp:ObjectDataSource>
<asp:ObjectDataSource runat="server" ID="dsStatus" TypeName="Udod.Dal.ESZDb" SelectMethod="GetStatuses"></asp:ObjectDataSource>
<asp:ObjectDataSource runat="server" ID="dsTypeFinance" TypeName="Udod.Dal.ESZDb" SelectMethod="GetTypeFinances"></asp:ObjectDataSource>
<asp:ObjectDataSource runat="server" ID="dsTypeValueService" TypeName="Udod.Dal.ESZDb" SelectMethod="GetTypeValueService"></asp:ObjectDataSource>
<asp:ObjectDataSource runat="server" ID="dsTimeday" TypeName="Udod.Dal.ESZDb" SelectMethod="GetTimeDays"></asp:ObjectDataSource>

<asp:UpdatePanel ID="upErrorPopup" runat="server" UpdateMode="Always" >
<ContentTemplate>
    <uc:ErrorPopup ID="errorPopup" runat="server" Width="450px"/>
</ContentTemplate>
</asp:UpdatePanel>

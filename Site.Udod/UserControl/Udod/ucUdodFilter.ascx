﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUdodFilter.ascx.cs" Inherits="UserControl_Udod_ucUdodFilter" %>

<asp:UpdatePanel runat="server">
<ContentTemplate>
<table  border=0 width="100%">
    <tr>
        <td  align="left">
            Статус заявления:
        </td>
        <td align="right">
            <div class="inputShort" runat="server" ID="ddlStatusDiv">
            <asp:DropDownList runat="server" ID="ddlStatus" DataSourceID="dsClaimStatuses" 
                DataValueField="ClaimStatusId" DataTextField="ClaimStatusName" AutoPostBack="true"
                OnDataBound="ddlStatus_OnDataBound" OnSelectedIndexChanged="ddlStatus_SelectedChanged"
                CssClass="inputShort" />
            </div>
        </td>
    </tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>

<asp:ObjectDataSource runat="server" ID="dsClaimStatuses" TypeName="Udod.Dal.ClaimDb" SelectMethod="GetClaimStatuses"></asp:ObjectDataSource>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucFindUdod.ascx.cs" Inherits="UserControl_Filter_ucFindUdod" %>
<%@ Register TagPrefix="uct" TagName="FindFilter" Src="~/UserControl/Filter/ucFindFilter.ascx" %>
<%@ Register TagPrefix="uct" TagName="DOPassport" Src="~/UserControl/Udod/ucDOPassport.ascx" %>
<%@ Register src="../Menu/ucPager.ascx" tagName="Pager" tagPrefix="uc" %>

<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoad1);
    function pageLoad1() {
        var html = document.documentElement;
        //alert(html.scrollTop);
        if (parseInt(html.scrollTop) < 100) 
        {
            $('<%="#"+popupSelectDO.ClientID %>').css('position', 'absolute');
            $('<%="#"+popupSelectDO.ClientID %>').addClass('popupTop1');
            //alert($('<%="#"+popupSelectDO.ClientID %>').css('top'));
        }
        else 
        {
            $('<%="#"+popupSelectDO.ClientID %>').removeClass('popupTop1');
        }
    }
</script>

<asp:UpdatePanel runat="server" ID="upFindUdod" UpdateMode="Conditional">
<ContentTemplate>
    <asp:Panel ID="findPanel" runat="server" DefaultButton="lnkFind">
        <asp:Panel runat="server" ID="pUdodName" GroupingText="<div class='headerPanel''><b>Поиск по названию</b></div>">
            <table>
                <tr>
                    <td>
                        Название УДО:
                    </td>
                    <td>
                        <div class="inputShort">
                        <asp:TextBox runat="server" ID="tbUdodName" CssClass="inputShort" />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <asp:Panel runat="server" ID="pPlace" GroupingText="<div class='headerPanel''><b>Поиск УДОД по расположению</b></div>">
            <table>
                <tr runat="server" ID="rowCity" >
                    <td align="right">Округ:</td>
                    <td>
                        <div class="inputShort">
                        <asp:DropDownList runat="server" DataSourceID="dsCities" ID="ddlCities" DataTextField="Name" DataValueField="Name" AutoPostBack="true" OnSelectedIndexChanged="City_SelectedIndex_Changed" OnDataBound="ddlCities_OnDataBound" CssClass="inputShort" />
                        </div>
                    </td>
                    <td style="width:100px;" align="right">Район:</td>
                    <td>
                       <div class="inputShort">
                        <asp:DropDownList runat="server" ID="ddlDistrict" DataSourceID="dsrayons" DataTextField="Name" DataValueField="Name" AutoPostBack="true" OnSelectedIndexChanged="ddlDistrict_OnSelectedIndexChanged" OnDataBound="ddlDistrict_OnDataBound" CssClass="inputShort" />
                        </div>
                    </td>
                    <td style="width:100px;" align="right">Метро:</td>
                    <td>
                        <div class="inputShort">
                        <asp:DropDownList runat="server" DataSourceID="dsMetro" ID="ddlMetro" DataTextField="Name" DataValueField="MetroId" AutoPostBack="true" OnSelectedIndexChanged="Metro_SelectedIndex_Changed" OnDataBound="ddlMetro_OnDataBound" CssClass="inputShort" />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <asp:Panel runat="server" ID="pFilterSection" DefaultButton="lnkFind" GroupingText="<div class='headerPanel'><b>Поиск ДО по образовательным услугам </b></div>">
            <uct:FindFilter ID="filterSection" runat="server" />
        </asp:Panel>

        <table>
            <tr>
                <td>
                    <asp:LinkButton runat="server" ID="lnkFind" OnClick="lnkFind_OnClick"><div class="btnBlueLong"> Поиск</div></asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pUdod" GroupingText="<div class='headerPanel''><b>Список учреждений, удовлетворяющих условиям поиска</b></div>">
        <table>
            <tr>
                <td colspan="2" style="width:900px">
                    <asp:ListBox runat="server" ID="lbUdos" Height="200" AutoPostBack="true" OnSelectedIndexChanged="lbUdos_SelectedIndexChanged" DataTextField="ShortName" DataValueField="UdodId" Style="width: 100%;" CssClass="bgControl" ></asp:ListBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                    <asp:GridView runat="server" ID="gvUdodSection" CssClass="dgc" AutoGenerateColumns="false" EmptyDataText="В данном УДО нет детских объединений" DataKeyNames="ChildUnionId" 
                        OnSelectedIndexChanged="GvUdodSectionSelectedIndexChanged" OnRowDataBound="gvUdodSection_OnRowDataBound" >
                        <Columns>
                            <asp:TemplateField HeaderText="№">
                                <ItemTemplate><%# gvUdodSection.Rows.Count + pgrChildUnionList.PagerIndex%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Наименование">
                                <ItemTemplate>
                                    <%# Eval("Name")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Вид деятельности">
                                <ItemTemplate>
                                    <%# Eval("Section.Name")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Возраст обучающихся">
                                <ItemTemplate>
                                    <%# "от " + Eval("AgeStart") + " до " + Eval("AgeEnd")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Адрес">
                                <ItemTemplate>
                                    <%# GetAddress(Container.DataItem) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Детальный просмотр">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="lnkShowDetail" OnClick="lnkShowDetail_OnClick"><div class="btnBlue">Детальный просмотр</div></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField  ButtonType="Link" ShowSelectButton="true" HeaderText="" SelectText="<div class='btnBlue'>Подать заявления</div>"/>
                        </Columns>
                    </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>

<uc:Pager runat="server" ID="pgrChildUnionList" needStartDots="false" needEndDots="false" pagesCount="10" OnChange="pgrChildUnionList_OnChange"/>

<asp:Panel runat="server" ID="popupSelectDO" style="display: none; border: 2px solid black" BackColor="White">
    <asp:Panel ID="pnlHeader" runat="server" >
        <asp:Label ID="Label2" Text="<span class='headerCaptions'>Паспорт детского объединения</span>" runat="server"/>
        <asp:LinkButton ID="LinkButton2" runat="server" 
            OnClientClick="$find('SelectUdodPopup').hide(); return false;" />
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upDO" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width:800px">
                <uct:DOPassport runat="server" ID="selectDO" Edit="false"/>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div >
        <asp:LinkButton ID="btnCancelPopup" runat="server" Text="<div class='btnBlue'>Закрыть</div>" CausesValidation="false" />
    </div>
</asp:Panel>
    
<ajaxToolkit:ModalPopupExtender runat="server" ID="popupSelectDOExtender" PopupDragHandleControlID="pnlHeader"
    PopupControlID="popupSelectDO" CancelControlID="btnCancelPopup"
    TargetControlID="btnShowDOPopup" RepositionMode="None" X="50"
    BehaviorID="AddUdodPopup"/>

<asp:Button ID="btnShowDOPopup" runat="server" style="display:none" />

<asp:ObjectDataSource runat="server" TypeName="Udod.Dal.DictCityDb" SelectMethod="GetCities" ID="dsCities">
    <SelectParameters>
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource runat="server" TypeName="Udod.Dal.DictRayonDb" SelectMethod="GetRayons" ID="dsrayons">
    <SelectParameters>
        <asp:ControlParameter Name="city" ControlID="ddlCities" PropertyName="SelectedValue" DbType="String" ConvertEmptyStringToNull="true"/>
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource runat="server" TypeName="Udod.Dal.DictMetroDb" SelectMethod="GetMetros" ID="dsMetro"></asp:ObjectDataSource>
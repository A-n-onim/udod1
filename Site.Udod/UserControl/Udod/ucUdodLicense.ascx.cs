﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class UserControl_Udod_UdodLicense : BasePageControl
{
    public int TypeLicense { get; set; }
    public void LoadLicense()
    {
        if (UserContext.UdodId.HasValue)
        {
            using (UdodDb db = new UdodDb())
            {
                ExtendedUdodLicense item = new ExtendedUdodLicense();
                item = db.GetUdodLicense(UserContext.UdodId.Value, TypeLicense);
                tbLicenseSeries.Text = item.Series;
                tbLicenseNumber.Text = item.Number;
                tbReestrNumber.Text = item.ReestrNumber;
                tbReestrDate.Text = item.ReestrDate.ToShortDateString();
                tbReestrValidity.Text = item.ReestrValidity.ToString();
            }
        }
    }
    public void SavedLicense()
    {
        if (UserContext.UdodId.HasValue)
        {
            using (UdodDb db = new UdodDb())
            {
                ExtendedUdodLicense item = new ExtendedUdodLicense();
                item.Series=tbLicenseSeries.Text ;
                item.Number=tbLicenseNumber.Text ;
                item.ReestrNumber=tbReestrNumber.Text ;
                if (tbReestrDate.Text!="") item.ReestrDate= Convert.ToDateTime(tbReestrDate.Text);
                if (tbReestrValidity.Text != "") item.ReestrValidity = Convert.ToDateTime(tbReestrValidity.Text);
                db.UpdateUdodLicense(UserContext.UdodId.Value, item, TypeLicense);
                
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        
        if (!Page.IsPostBack )
        {
            
        }

    }
}
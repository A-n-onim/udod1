﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUdodLicense.ascx.cs" Inherits="UserControl_Udod_UdodLicense" %>

<table>
    <tr>
        <td>
            Серия бланка
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbLicenseSeries" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Номер бланка
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbLicenseNumber" runat="server"></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>
            Реестровый номер свидетельства 
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbReestrNumber" runat="server"></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>
            Дата выдачи
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbReestrDate" runat="server"></asp:TextBox>
            <ajaxToolkit:CalendarExtender runat="server" ID="calendar" TargetControlID="tbReestrDate" Animated="false" Format="dd.MM.yyyy"/>
            <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbReestrDate" Mask="99/99/9999" MaskType="Date" />
        </td>
    </tr>    
    <tr>
        <td>
            Срок действия 
        </td>
        <td>
            <asp:TextBox CssClass="inputLong" ID="tbReestrValidity" runat="server"></asp:TextBox>
            <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="tbReestrValidity" Animated="false" Format="dd.MM.yyyy"/>
            <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="tbReestrValidity" Mask="99/99/9999" MaskType="Date" />
        </td>
    </tr>    
    
</table>

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Microsoft.Web.Services3.Addressing;
//using Service.esz;
using Udod.Dal;
using Udod.Dal.Enum;


public partial class UserControl_Udod_ucDOPassport : BasePageControl
{
    public event EventHandler Change;
    public bool Edit { get; set; }

    public long? FakeChildUnionId
    {
        get
        {
            if (ViewState["FakeChildUnionId"] == null) return null;
            return (long)ViewState["FakeChildUnionId"];
        }
        set
        {
            ViewState["FakeChildUnionId"] = value;
        }
    }

    public long ChildUnionId
    {
        get
        {
            return (long)ViewState["ChildUnionId"];
        }
        set
        {
            ViewState["ChildUnionId"] = value;
        }
    }

    public long UdodSectionId
    {
        get
        {
            return (long)ViewState["UdodSectionId"];
        }
        set
        {
            ViewState["UdodSectionId"] = value;
        }
    }

    public bool HaveSchedule
    {
        get
        {
            return (bool)ViewState["HaveSchedule"];
        }
        set
        {
            ViewState["HaveSchedule"] = value;
        }
    }

    public void SetDOData(ExtendedChildUnion childUnion)
    {
        ChildUnionId = childUnion.ChildUnionId;
        UdodSectionId = childUnion.UdodSectionId;
        CustomValidator4.Enabled = true;
        lblAgeGroupWarning.Visible = false;

        //gvLessons.EditIndex = -1;
        ddlIsDopProgram.Visible=
            ddlIsPublicPortal.Visible=
            ddlIsReception.Visible=
            ddlTypeValueService.Visible=
            trFinansing.Visible =
            trPeriod.Visible =
            trProgramService.Visible =
            trSchedule.Visible = trSex.Visible = trStatus.Visible = tdTypeHeader.Visible = ddlTypeFinance.Visible = tbMinCountPupil.Visible = Edit;
        if (Edit && UserContext.UdodId.HasValue)
        {
            //lnkAddAgeGroup.Visible = true;
            ddlNumYears.Visible = true;
            tbName.Text = childUnion.Name;
            tbComment.Text = childUnion.Comment;
            tbMinCountPupil.Text = childUnion.MinCountPupil.ToString();
            tbAgeStart.Text = childUnion.AgeStart.ToString();
            tbAgeEnd.Text = childUnion.AgeEnd.ToString();

            using (DictBudgetTypeDb db = new DictBudgetTypeDb())
            {
                cbxTypeBudget.DataSource = db.GetBudgetTypes();
                cbxTypeBudget.DataBind();
            }

            foreach (ListItem bt in cbxTypeBudget.Items)
            {
                bt.Selected = childUnion.BudgetTypes.Count(i => i.DictTypeBudgetId == Convert.ToInt64(bt.Value)) > 0;
            }

            using (DictEducationTypeDb db = new DictEducationTypeDb())
            {
                cbxEducationType.DataSource = db.GetEducationTypes();
                cbxEducationType.DataBind();
            }

            foreach (ListItem et in cbxEducationType.Items)
            {
                et.Selected = childUnion.EducationTypes.Count(i => i.EducationTypeId == Convert.ToInt64(et.Value)) > 0;
            }

            using (UdodDb db = new UdodDb())
            {
                ExtendedUdod udod = db.GetUdod(childUnion.UdodId);
                cbxAddress.DataSource = udod.Addresses;
                cbxAddress.DataBind();

                // ЕСЗ
                cblOffice.DataSource = udod.Addresses;
                cblOffice.DataBind();
            }

            foreach (ListItem addr in cbxAddress.Items)
            {
                addr.Selected = childUnion.Addresses.Count(i => i.AddressId == Convert.ToInt64(addr.Value)) > 0;
            }

            foreach (ListItem addr in cblOffice.Items)
            {
                addr.Selected = childUnion.Offices.Count(i => i.AddressId == Convert.ToInt64(addr.Value)) > 0;
            }

            using (ChildUnionDb db = new ChildUnionDb())
            {
                var list = db.GetTeacher(ChildUnionId);
                repTeacher.DataSource = list;
                repTeacher.DataBind();
            }

            using (DictProgramCategoryDb db = new DictProgramCategoryDb())
            {
                ddlProgramCategory.DataSource = db.GetProgramCategories();
                ddlProgramCategory.DataBind();
                ddlProgramCategory.SelectedIndex = ddlProgramCategory.Items.IndexOf(ddlProgramCategory.Items.FindByValue(childUnion.ProgramCategory.ProgramCategoryId.ToString()));
            }

            ddlNumYears.SelectedIndex = ddlNumYears.Items.IndexOf(ddlNumYears.Items.FindByValue(childUnion.NumYears.ToString()));
            cbxIsActive.Checked = !childUnion.IsActive;
            cbxIsActive.Visible = true;
            lblIsDopProgram.Visible =
                lblIsPublicPortal.Visible =
                lblIsReception.Visible =
                lblTypeValueService.Visible =
            lblIsActive.Visible = false;
            // комбобоксы
            using (UdodDb db = new UdodDb())
            {
                var list = db.GetUdodSections(UserContext.UdodId.Value, null, null);
                ddlProgram.DataSource = list.Programs;
                ddlProgram.DataBind();

                list = db.GetUdodSections(UserContext.UdodId.Value, childUnion.Program.ProgramId, null);
                ddlProfile.DataSource = list.Profiles;
                ddlProfile.DataBind();

                list = db.GetUdodSections(UserContext.UdodId.Value, childUnion.Program.ProgramId, childUnion.Profile.ProfileId);
                ddlSection.DataSource = list.Sections;
                ddlSection.DataBind();

                ddlProgram.SelectedIndex = ddlProgram.Items.IndexOf(ddlProgram.Items.FindByValue(childUnion.Program.ProgramId.ToString()));
                ddlProfile.SelectedIndex = ddlProfile.Items.IndexOf(ddlProfile.Items.FindByValue(childUnion.Profile.ProfileId.ToString()));
                ddlSection.SelectedIndex = ddlSection.Items.IndexOf(ddlSection.Items.FindByValue(childUnion.Section.SectionId.ToString()));
            }

            //gvLessons.DataSource = childUnion.AgeGroups;
            //gvLessons.DataBind();
            
            tbName.Visible = 
            tbAgeEnd.Visible = 
            tbAgeStart.Visible = 
            tbComment.Visible = 
            ddlProfile.Visible = 
            ddlProgram.Visible = 
            ddlSection.Visible = 
            cbxAddress.Visible = 
            cbxTypeBudget.Visible = 
            cbxEducationType.Visible = true;
            lblName.Visible = 
            lblComment.Visible = 
            lblProfile.Visible = 
            lblProgram.Visible = 
            lblSection.Visible = 
            lblAddress.Visible = 
            lblTypeBudget.Visible = 
            lblEducationType.Visible = 
            lblNumYears.Visible = 
            lblTeacherFioNoEdit.Visible = 
            lblMinCountPupil.Visible = false;

            if ((UserContext.Roles.RoleId == (int)EnumRoles.Department)
                || (UserContext.Roles.RoleId == (int)EnumRoles.Administrator)
                || (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee))
            {
                lblProgrCateg.Visible = false;
                ddlProgramCategory.Visible = true;
            }

            #region ЕСЗ
            
            tbProgramService.Text=childUnion.ProgramService;
            tbRuleService.Text=childUnion.RuleService;
            tbDocDeadLine.Text = childUnion.DocsDeadline.ToString();
            cbTestService.Checked=childUnion.TestService;
            tbTourNumber.Text = childUnion.ToursNumber.ToString();
            ddlTypeFinance.SelectedIndex = ddlTypeFinance.Items.IndexOf(ddlTypeFinance.Items.FindByValue(childUnion.TypeFinanceId.ToString()));
            ddlTypeValueService.SelectedIndex = ddlTypeValueService.Items.IndexOf(ddlTypeValueService.Items.FindByValue(childUnion.TypeValueServiceId.ToString()));
            ddlIsDopProgram.SelectedIndex = ddlIsDopProgram.Items.IndexOf(ddlIsDopProgram.Items.FindByValue(Convert.ToInt32(childUnion.isDopService).ToString()));
            tbFinansing.Text=childUnion.Finansing.ToString(CultureInfo.GetCultureInfo("en-us"));
            tbsubFinance.Text = childUnion.SubFinance.ToString(CultureInfo.GetCultureInfo("en-us"));
            if (childUnion.DateStart != new DateTime())
            {
                ddlSex.SelectedIndex = ddlSex.Items.IndexOf(ddlSex.Items.FindByValue(childUnion.SexId.ToString()));
                ddlStatus.SelectedIndex =
                    ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(childUnion.StatusId.ToString()));
                ddlForm.SelectedIndex = ddlForm.Items.IndexOf(ddlForm.Items.FindByValue(childUnion.FormId.ToString()));
            }
            else
            {
                ddlSex.SelectedIndex = ddlSex.Items.IndexOf(ddlSex.Items.FindByValue("3"));
            }
            if (childUnion.DateStart==new DateTime())
            {
                var year = DateTime.Now.Year;
                if (DateTime.Now < Convert.ToDateTime("01.09." + year))
                {
                    year--;
                }
                childUnion.DateStart = Convert.ToDateTime("01.09." + year);
            }
            tbStartDate.Text=childUnion.DateStart.ToShortDateString();
            if (childUnion.DateEnd == new DateTime())
            {
                var year = DateTime.Now.Year;
                if (DateTime.Now > Convert.ToDateTime("01.09." + year))
                {
                    year++;
                }
                childUnion.DateEnd = Convert.ToDateTime("01.09." + year);
            }
            tbEndDate.Text= childUnion.DateEnd.ToShortDateString();

            ddlIsPublicPortal.SelectedIndex = childUnion.isPublicPortal ? 0 : 1;
            ddlIsReception.SelectedIndex = childUnion.isReception ? 0 : 1;

            #endregion
            #region Schedule

            using(ScheduleDb db = new ScheduleDb())
            {
                HaveSchedule = db.GetShedules(ChildUnionId, null, null, null, null).Count > 0;
            }
            if (!HaveSchedule)
            {
                using (ChildUnionDb db = new ChildUnionDb())
                {
                    var list = db.GetChildUnionSimpleSchedule(ChildUnionId);
                    cbMonday.Checked = list.Count(i => i.DayOfWeek == 1) > 0;
                    cbTuesday.Checked = list.Count(i => i.DayOfWeek == 2) > 0;
                    cbWednesday.Checked = list.Count(i => i.DayOfWeek == 3) > 0;
                    cbThursday.Checked = list.Count(i => i.DayOfWeek == 4) > 0;
                    cbFriday.Checked = list.Count(i => i.DayOfWeek == 5) > 0;
                    cbSaturday.Checked = list.Count(i => i.DayOfWeek == 6) > 0;
                    cbSunday.Checked = list.Count(i => i.DayOfWeek == 7) > 0;

                    if (cbMonday.Checked)
                        ddlMonday.SelectedIndex =
                            ddlMonday.Items.IndexOf(
                                ddlMonday.Items.FindByValue(
                                    list.Where(i => i.DayOfWeek == 1).FirstOrDefault().TimeDayId.ToString()));
                    else ddlMonday.SelectedIndex = 0;
                    if (cbTuesday.Checked)
                        ddlTuesday.SelectedIndex =
                            ddlMonday.Items.IndexOf(
                                ddlMonday.Items.FindByValue(
                                    list.Where(i => i.DayOfWeek == 2).FirstOrDefault().TimeDayId.ToString()));
                    else ddlThursday.SelectedIndex = 0;
                    if (cbWednesday.Checked)
                        ddlWednesday.SelectedIndex =
                            ddlMonday.Items.IndexOf(
                                ddlMonday.Items.FindByValue(
                                    list.Where(i => i.DayOfWeek == 3).FirstOrDefault().TimeDayId.ToString()));
                    else ddlWednesday.SelectedIndex = 0;
                    if (cbThursday.Checked)
                        ddlThursday.SelectedIndex =
                            ddlMonday.Items.IndexOf(
                                ddlMonday.Items.FindByValue(
                                    list.Where(i => i.DayOfWeek == 4).FirstOrDefault().TimeDayId.ToString()));
                    else ddlThursday.SelectedIndex = 0;
                    if (cbFriday.Checked)
                        ddlFriday.SelectedIndex =
                            ddlMonday.Items.IndexOf(
                                ddlMonday.Items.FindByValue(
                                    list.Where(i => i.DayOfWeek == 5).FirstOrDefault().TimeDayId.ToString()));
                    else ddlFriday.SelectedIndex = 0;
                    if (cbSaturday.Checked)
                        ddlSaturday.SelectedIndex =
                            ddlMonday.Items.IndexOf(
                                ddlMonday.Items.FindByValue(
                                    list.Where(i => i.DayOfWeek == 6).FirstOrDefault().TimeDayId.ToString()));
                    else ddlSaturday.SelectedIndex = 0;
                    if (cbSunday.Checked)
                        ddlSunday.SelectedIndex =
                            ddlMonday.Items.IndexOf(
                                ddlMonday.Items.FindByValue(
                                    list.Where(i => i.DayOfWeek == 7).FirstOrDefault().TimeDayId.ToString()));
                    else ddlSunday.SelectedIndex = 0;
                }
            }
            else
            {
                trSchedule.Visible = false;
                CustomValidator4.Enabled = false;
            }

            #endregion
        }
        else
        {
            //lnkAddAgeGroup.Visible = false;
            //gvLessons.Columns[gvLessons.Columns.Count - 1].Visible = false;
            ddlNumYears.Visible = false;
            var typeBudgets = (childUnion as ExtendedChildUnion).BudgetTypes;
            StringBuilder sb = new StringBuilder();
            foreach (var tb in typeBudgets.Select(i => i.TypeBudgetName).Distinct().ToList())
            {
                sb.Append(tb + "</br>");
            }
            if (sb.ToString() != "")
            {
                sb.Remove(sb.Length - 5, 5);
                lblTypeBudget.Text = sb.ToString();
            }
            else
                lblTypeBudget.Text = "Не задано";

            var educTypes = (childUnion as ExtendedChildUnion).EducationTypes;
            sb = new StringBuilder();
            foreach (var et in educTypes.Select(i => i.EducationTypeName).Distinct().ToList())
            {
                sb.Append(et + "</br>");
            }
            if (sb.ToString() != "")
            {
                sb.Remove(sb.Length - 5, 5);
                lblEducationType.Text = sb.ToString();
            }
            else
                lblEducationType.Text = "Не задано";

            lblProgrCateg.Text = (childUnion as ExtendedChildUnion).ProgramCategory.ProgramCategoryName;

            var addresses = (childUnion as ExtendedChildUnion).Addresses;
            sb = new StringBuilder();
            foreach (var addr in addresses.Select(i => i.AddressStr).Distinct().ToList())
            {
                sb.Append(addr + "</br>");
            }
            if (sb.ToString() != "")
            {
                sb.Remove(sb.Length - 5, 5);
                lblAddress.Text = sb.ToString();
            }
            else
                lblAddress.Text = "Не задано";

            using (ChildUnionDb db = new ChildUnionDb())
            {
                var list = db.GetTeacher(ChildUnionId);
                sb = new StringBuilder();
                foreach (var teac in list.Select(i => i.TeacherName).Distinct().ToList())
                {
                    sb.Append(teac + "</br>");
                }
                if (sb.ToString() != "")
                {
                    sb.Remove(sb.Length - 5, 5);
                    lblTeacherFioNoEdit.Text = sb.ToString();
                }
            }

            lblName.Text = childUnion.Name;
            lblComment.Text = childUnion.Comment;
            lblSection.Text = childUnion.Section.Name;
            lblProfile.Text = childUnion.Profile.Name;
            lblProgram.Text = childUnion.Program.Name;
            lblNumYears.Text = childUnion.NumYears.ToString();
            lblAge.Text = "от " + childUnion.AgeStart.ToString();
            lblAge2.Text = " до " + childUnion.AgeEnd.ToString();
            lblMinCountPupil.Text = childUnion.MinCountPupil.ToString();
            //gvLessons.Columns[0].Visible = false;
            //gvLessons.DataSource = childUnion.AgeGroups;
            //gvLessons.DataBind();
            lblIsDopProgram.Visible =
                lblIsPublicPortal.Visible=
                lblIsReception.Visible=
                lblTypeValueService.Visible=
            lblIsActive.Visible = true;
            lblIsActive.Text = !childUnion.IsActive ? "Да" : "Нет";
            cbxIsActive.Visible = false;

            lblIsPublicPortal.Text = childUnion.isPublicPortal ? "Да" : "Нет";
            lblIsDopProgram.Text = childUnion.isDopService ? "Да" : "Нет";
            lblIsReception.Text = childUnion.isReception ? "Да" : "Нет";
            lblTypeValueService.Text = ddlTypeValueService.SelectedItem.Text;
            tbName.Visible = 
            tbComment.Visible = 
            ddlProfile.Visible = 
            ddlProgram.Visible = 
            ddlSection.Visible = 
            cbxAddress.Visible = 
            tbAgeStart.Visible = 
            tbAgeEnd.Visible = 
            lnkChangeTeacher.Visible = 
            cbxEducationType.Visible =
            cbxTypeBudget.Visible = false;
            lblName.Visible = 
            lblComment.Visible = 
            lblProfile.Visible = 
            lblProgram.Visible = 
            lblSection.Visible = 
            lblAddress.Visible = 
            lblTeacherFioNoEdit.Visible = 
            lblEducationType.Visible = 
            lblTypeBudget.Visible = true;

            if ((UserContext.Roles.RoleId == (int)EnumRoles.Department)
                || (UserContext.Roles.RoleId == (int)EnumRoles.Administrator)
                || (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee))
            {
                lblProgrCateg.Visible = true;
                ddlProgramCategory.Visible = false;
            }
        }
    }

    protected void ProgramIndex_Changed(object sender, EventArgs e)
    {
        int programId = Convert.ToInt32(ddlProgram.SelectedValue);
        using (UdodDb db = new UdodDb())
        {
            var list = db.GetUdodSections(UserContext.UdodId.Value,programId,null);

            ddlProfile.DataSource = list.Profiles;
            ddlProfile.DataBind();

            ddlSection.DataSource = list.Sections;
            ddlSection.DataBind();
        }
    }

    protected void ProfileIndex_Changed(object sender, EventArgs e)
    {
        int profileId = Convert.ToInt32(ddlProfile.SelectedValue);
        int programId = Convert.ToInt32(ddlProgram.SelectedValue);

        using (UdodDb db = new UdodDb())
        {
            var list = db.GetUdodSections(UserContext.UdodId.Value, programId, profileId);

            ddlSection.DataSource = list.Sections;
            ddlSection.DataBind();
        }
    }

    protected void SectionIndex_Changed(object sender, EventArgs e)
    {
        /*int profileId = Convert.ToInt32(ddlProfile.SelectedValue);
        using (DictSectionDb db = new DictSectionDb())
        {
            List<ExtendedSection> list = db.GetSections(profileId, null, null);
            ddlSection.DataSource = list;
            ddlSection.DataBind();
        }*/
    }

    public void addOrUpdateChildUnion(ref ExtendedChildUnion ecu, bool isAddDO, bool addEmpty)
    {
        //ExtendedChildUnion ecu = new ExtendedChildUnion();

        ecu.ChildUnionId = ChildUnionId;
        ecu.Name = addEmpty ? "Новое ДО" : tbName.Text.Replace("\r\n", " ").Replace("\t", " ");
        ecu.Comment = tbComment.Text;
        ecu.AgeStart = Convert.ToInt32(tbAgeStart.Text);
        ecu.AgeEnd = Convert.ToInt32(tbAgeEnd.Text);

        if (ecu.AgeStart < 3)
        {
            ecu.AgeStart = 3;
            tbAgeStart.Text = "3";
        }

        if (ecu.AgeEnd < 3)
        {
            ecu.AgeEnd = 3;
            tbAgeEnd.Text = "3";
        }

        if (ecu.AgeStart > 21)
        {
            ecu.AgeStart = 21;
            tbAgeStart.Text = "21";
        }

        if (ecu.AgeEnd > 21)
        {
            ecu.AgeEnd = 21;
            tbAgeEnd.Text = "21";
        }

        ecu.NumYears = Convert.ToInt32(ddlNumYears.SelectedValue);
        ecu.IsActive = !cbxIsActive.Checked;
        ecu.MinCountPupil = Convert.ToInt32(tbMinCountPupil.Text);
        ecu.ProgramCategory = new ExtendedProgramCategory
        {
            ProgramCategoryId = Convert.ToInt32(ddlProgramCategory.SelectedValue)
        };

        using (SectionDb db2 = new SectionDb())
        {
            ecu.UdodSectionId = db2.GetUdodSection(UserContext.UdodId.Value, Convert.ToInt32(ddlSection.SelectedValue)).UdodSectionId;
        }

        ecu.Section = new ExtendedSection
        {
            SectionId = Convert.ToInt32(ddlSection.SelectedValue)
        };

        ecu.Profile = new ExtendedProfile
        {
            ProfileId = Convert.ToInt32(ddlProfile.SelectedValue)
        };

        ecu.Program = new ExtendedProgram
        {
            ProgramId = Convert.ToInt32(ddlProgram.SelectedValue)
        };
        
        ecu.ProgramService = tbProgramService.Text;
        ecu.RuleService = tbRuleService.Text;
        ecu.DocsDeadline = 7;// Convert.ToInt32(tbDocDeadLine.Text);
        ecu.TestService = cbTestService.Checked;
        ecu.ToursNumber = tbTourNumber.Enabled ? Convert.ToInt32(tbTourNumber.Text) : 0;
        ecu.TypeFinanceId = Convert.ToInt32(ddlTypeFinance.SelectedValue);
        ecu.TypeValueServiceId = Convert.ToInt32(ddlTypeValueService.SelectedValue);
        ecu.isDopService = ddlIsDopProgram.SelectedValue=="1";
        ecu.Finansing = string.IsNullOrEmpty(tbFinansing.Text)?0:Convert.ToDecimal(tbFinansing.Text, (IFormatProvider)CultureInfo.GetCultureInfo("en-us"));
        ecu.SubFinance = string.IsNullOrEmpty(tbsubFinance.Text) ? 0 : Convert.ToDecimal(tbsubFinance.Text, (IFormatProvider)CultureInfo.GetCultureInfo("en-us"));

        if (string.IsNullOrEmpty(tbStartDate.Text))
        {
            var year = DateTime.Now.Year;
            if (DateTime.Now < Convert.ToDateTime("01.09." + year))
            {
                year--;
            }
            ecu.DateStart = Convert.ToDateTime("01.09." + year);
        }
        else ecu.DateStart = Convert.ToDateTime(tbStartDate.Text);
        if (string.IsNullOrEmpty(tbEndDate.Text))
        {
            var year = DateTime.Now.Year;
            if (DateTime.Now > Convert.ToDateTime("01.09." + year))
            {
                year++;
            }
            ecu.DateEnd = Convert.ToDateTime("01.09." + year);
        }
        else ecu.DateEnd = Convert.ToDateTime(tbEndDate.Text);

        //ecu.DateStart = string.IsNullOrEmpty() Convert.ToDateTime(tbStartDate.Text);
        //ecu.DateEnd = Convert.ToDateTime(tbEndDate.Text);
        ecu.SexId = Convert.ToInt32(ddlSex.SelectedValue);
        ecu.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
        ecu.FormId = Convert.ToInt32(ddlForm.SelectedValue);
        ecu.isPublicPortal = ddlIsPublicPortal.SelectedValue == "1";
        ecu.isReception = ddlIsReception.SelectedValue == "1";
        using (ChildUnionDb db = new ChildUnionDb())
        {
            if (isAddDO)
                ChildUnionId = db.AddChildUnion(ecu);
            else
            {
                using (AgeGroupDb db2 = new AgeGroupDb())
                {
                    db2.OpenCloseChildUnion(null, ChildUnionId, true);
                }

                db.UpdateChildUnion(ecu);
            }
            ecu.ChildUnionId = ChildUnionId;
            db.UpdateChildUnionDescription(ecu);
            db.UpdateChildUnionIsPortal(ecu);
            db.UpdateChildUnionIsReception(ecu);

            using(ScheduleDb db1 = new ScheduleDb())
            {
                HaveSchedule = db1.GetShedules(ChildUnionId, null, null, null, null).Count > 0;
            }
            if (!HaveSchedule)
            {
                #region Schedule

                db.UpdateChildUnionSimpleSchedule(ChildUnionId, 1, Convert.ToInt32(ddlMonday.SelectedValue),
                                                  Convert.ToInt32(cbMonday.Checked));
                db.UpdateChildUnionSimpleSchedule(ChildUnionId, 2, Convert.ToInt32(ddlTuesday.SelectedValue),
                                                  Convert.ToInt32(cbTuesday.Checked));
                db.UpdateChildUnionSimpleSchedule(ChildUnionId, 3, Convert.ToInt32(ddlWednesday.SelectedValue),
                                                  Convert.ToInt32(cbWednesday.Checked));
                db.UpdateChildUnionSimpleSchedule(ChildUnionId, 4, Convert.ToInt32(ddlThursday.SelectedValue),
                                                  Convert.ToInt32(cbThursday.Checked));
                db.UpdateChildUnionSimpleSchedule(ChildUnionId, 5, Convert.ToInt32(ddlFriday.SelectedValue),
                                                  Convert.ToInt32(cbFriday.Checked));
                db.UpdateChildUnionSimpleSchedule(ChildUnionId, 6, Convert.ToInt32(ddlSaturday.SelectedValue),
                                                  Convert.ToInt32(cbSaturday.Checked));
                db.UpdateChildUnionSimpleSchedule(ChildUnionId, 7, Convert.ToInt32(ddlSunday.SelectedValue),
                                                  Convert.ToInt32(cbSunday.Checked));

                #endregion
            }

        }
    }
    
    public bool IsNameExist(bool isAddDO)
    {
        // если мы обновляем существующий то проверка не нужна
        if (isAddDO)
        {
            using (ChildUnionDb db = new ChildUnionDb())
            {
                // проверка на существование удод с таким же именем
                List<ExtendedChildUnion> list = db.GetChildUnionsShort(null, UserContext.UdodId.Value, null, null, null, null, null, null,null,null,null, null, null, null, null, null, false);
                if (list.Where(i => i.Name == tbName.Text).ToList().Count > 0)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public bool IsAgeGroupsGood()
    {
        using (AgeGroupDb db = new AgeGroupDb())
        {
            int AgeStart = Convert.ToInt32(tbAgeStart.Text);
            int AgeEnd = Convert.ToInt32(tbAgeEnd.Text);
            int NumYearStart = cbxIsActive.Checked ? 2 : 1;
            int NumYearEnd = Convert.ToInt32(ddlNumYears.SelectedValue);

            int extraPupils = db.PupilsCount(ChildUnionId, NumYearEnd);
            if (extraPupils != 0)
            {
                // это не про удаление записей в таблице продолжительностей, а изменение ddl с предполагаемым годом обучения
                errorPopup.ShowError("Внимание", "Невозможно удалить некоторые года обучения. В них есть обучающиейся.", "", "ОК", false, true);
                return false;
            }

            if (cbxIsActive.Checked)
            {
                int firstYearPupils = db.PupilsCountInFirstYear(ChildUnionId);
                
                var groups = (new ChildUnionDb()).GetCountGroups(ChildUnionId, 1);
                if (firstYearPupils != 0 || groups[0]>0)
                {
                    // это если поставят галочку запись ведется только на второй год, а у них есть обучающиейся в первом
                    errorPopup.ShowError("Внимание", "Невозможно вести приём только на 2й год обучения, так как в данном ДО существуют группы, предполагающие обучение по программе 1-го года.", "", "ОК", false, true);
                    return false;
                }
            }

            var list = db.GetGroupsPaging(null, null, ChildUnionId, null, null, null, "", 1,50000, null);

            if (list.Count > 0)
            {
                int min = list.Min(i => i.AgeStart);
                int max = list.Max(i => i.AgeEnd);

                if (AgeStart > min || AgeEnd < max)
                {
                    errorPopup.ShowError("Внимание",
                                         "Указанные границы возрастов не могут быть сохранены, так как у данного ДО существуют группы, границы возрастов которых выходят за рамки указанных значений. Откорректируйте параметры групп перед редактирование ДО",
                                         "", "ОК", false, true);
                    return false;
                }
            }
            /*
            foreach (var l in list)
            {
                if ((l.NumYearStart < NumYearStart) || (l.NumYearEnd > NumYearEnd) || (l.AgeStart < AgeStart) || (l.AgeEnd > AgeEnd))
                {
                    errorPopup.ShowError("Внимание", "Существуют возрастные группы выходящие за рамки годов обучения или возраста обучающихся. Удалите их.", "", "ОК", false, true);
                    return false;
                }

                if (l.IsActive && ((l.LessonLength == 0) || (l.LessonsInWeek == 0)))
                {
                    errorPopup.ShowError("Внимание", "Существуют активные возрастные группы с некорректными данными о продолжительности занятий или количества занятий в неделю.", "", "ОК", false, true);
                    return false;
                }
            }*/
            /*
            List<List<int>> matrix = new List<List<int>>();
            for (int i = 0; i <= NumYearEnd - NumYearStart; i++)
            {
                matrix.Add(new List<int>());
                for (int j = 0; j <= AgeEnd - AgeStart; j++)
                    matrix[i].Add(0);
            }

            foreach (var l in list)
            {
                for (int i = l.NumYearStart-NumYearStart; i <= l.NumYearEnd - NumYearStart; i++)
                    for (int j = l.AgeStart-AgeStart; j <= l.AgeEnd - AgeStart; j++)
                        matrix[i][j]++;
            }

            int count=0;
            for (int i = 0; i < matrix.Count; i++)
                for (int j = 0; j < matrix[i].Count; j++)
                    if (matrix[i][j] == 1)
                        count++;

            if (count == (NumYearEnd - NumYearStart + 1) * (AgeEnd - AgeStart + 1))
            {
                // вот это хорошо!!!!
                return true;
            }

            errorPopup.ShowError("Внимание", "Список возрастных групп не покрывает некоторые возраста или года обучения, или наоборот покрывает их несколько раз. Внесите коррективы.", "", "ОК", false, true);
            return false;*/
        }
        return true;
    }

    public void GetDOData(bool isAddDO)
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {
            // здесь добавляем ДО стопудово, false вторым параметром означает что берем реальные значения из всех текстбоксов
            ExtendedChildUnion ecuOld = null;
            if (!isAddDO)
            {
                ecuOld = db.GetChildUnion(ChildUnionId, null);
            }
            ExtendedChildUnion ecu = new ExtendedChildUnion();
            addOrUpdateChildUnion(ref ecu, isAddDO, false);
            var udod = (new UdodDb()).GetUdod(UserContext.UdodId.Value);
            

            // сохраняем чекбоксные списки прямо здесь не возвращая их в UdodEdit
            foreach (ListItem item in cbxAddress.Items)
            {
                if (item.Selected)
                {
                    db.AddAddress(ChildUnionId, Convert.ToInt32(item.Value));
                }
                else
                {
                    db.DeleteAddress(ChildUnionId, Convert.ToInt32(item.Value));
                }
            }

            foreach (ListItem item in cbxTypeBudget.Items)
            {
                if (item.Selected)
                {
                    db.AddTypeBudget(ChildUnionId, Convert.ToInt32(item.Value));
                }
                else
                {
                    db.DeleteTypeBudget(ChildUnionId, Convert.ToInt32(item.Value));
                }
            }

            foreach (ListItem item in cbxEducationType.Items)
            {
                if (item.Selected)
                {
                    db.AddEducationType(ChildUnionId, Convert.ToInt32(item.Value));
                }
                else
                {
                    db.DeleteEducationType(ChildUnionId, Convert.ToInt32(item.Value));
                }
            }
            /*
            #region Отправка в ЕСЗ
            SiteUtility.SendBatchESZ(UserContext.CityId, UserContext.UdodId, true, false, ecu.ChildUnionId);
            int checkPriem = 3;
            if (ecuOld!=null && ecuOld.ChildUnionId!=0)
            {
                if (ecuOld.isReception!=ecu.isReception)
                {
                    checkPriem = ecuOld.isReception && !ecu.isReception
                                     ? 0
                                     : !ecuOld.isReception && ecu.isReception ? 1 : 3;
                }
            }
            if (!ecu.isReception) checkPriem = 0; else checkPriem = 1;
            using (AgeGroupDb db2 = new AgeGroupDb())
            {
                var list = db2.GetGroupsPaging(UserContext.UdodId, "", ecu.ChildUnionId, null, 1, null, "", 1, 500000,
                                               null);

                SiteUtility.SendBatchScheduleESZ(ecu.ChildUnionId, checkPriem, list);
            }
            
            #endregion
            */
        }
    }

    public void ClearData()
    {
        ChildUnionId = -1;
        tbName.Text = "";
        tbComment.Text = tbProgramService.Text = "";
        tbFinansing.Text = tbsubFinance.Text = "0";
        ddlTypeValueService.SelectedIndex = 0;
        tbAgeStart.Text = tbAgeEnd.Text = "3";
        ddlNumYears.SelectedIndex = 0;
        cbxIsActive.Checked = false;
        using (DictProgramCategoryDb db = new DictProgramCategoryDb())
        {
            ddlProgramCategory.DataSource = db.GetProgramCategories();
            ddlProgramCategory.DataBind();
            ddlProgramCategory.SelectedIndex = 0;
        }

        foreach (ListItem cbx in cbxTypeBudget.Items)
        {
            cbx.Selected = false;
        }
        foreach (ListItem cbx in cbxEducationType.Items)
        {
            cbx.Selected = false;
        }
        foreach (ListItem cbx in cbxAddress.Items)
        {
            cbx.Selected = false;
        }

        cbMonday.Checked =
            cbWednesday.Checked =
            cbTuesday.Checked = 
            cbSunday.Checked = 
            cbFriday.Checked = 
            cbSaturday.Checked = 
            cbThursday.Checked = false;


        repTeacher.DataSource = new List<ExtendedChildUnionTeacher>();
        repTeacher.DataBind();

        //gvLessons.DataSource = new List<ExtendedAgeGroup>();
        //gvLessons.DataBind();

        using (UdodDb db = new UdodDb())
        {
            var list = db.GetUdodSections(UserContext.UdodId.Value, null, null);
            ddlProgram.DataSource = list.Programs;
            ddlProgram.DataBind();

            ddlProfile.DataSource = list.Profiles;
            ddlProfile.DataBind();

            ddlSection.DataSource = list.Sections;
            ddlSection.DataBind();

            ExtendedUdod udod = db.GetUdod(UserContext.UdodId.Value);
            cbxAddress.DataSource = udod.Addresses;
            cbxAddress.DataBind();
        }

        using (DictBudgetTypeDb db = new DictBudgetTypeDb())
        {
            cbxTypeBudget.DataSource = db.GetBudgetTypes();
            cbxTypeBudget.DataBind();
        }

        using (DictEducationTypeDb db = new DictEducationTypeDb())
        {
            cbxEducationType.DataSource = db.GetEducationTypes();
            cbxEducationType.DataBind();
        }
    }

    /*protected string GetAgeGroupAges(object agegroup)
    {
        if (agegroup != null)
        {
            var ageGroup = (ExtendedAgeGroup)agegroup;
            int AS = ageGroup.AgeStart;
            int AE = ageGroup.AgeEnd;
            if (ageGroup.EducationType.EducationTypeId == 2)
            {
                return "Индивидуальное";
            }
            else if (AS == AE)
            {
                return ageGroup.AgeStart.ToString() + " год(а)";
            }
            else
            {
                return AS.ToString() + " - " + AE.ToString();
            }
        }
        return string.Empty;
    }*/

    protected void Page_Load(object sender, EventArgs e)
    {
        lblProgramCategory.Visible = ddlProgramCategory.Visible = lblProgrCateg.Visible = (UserContext.Roles.RoleId == (int)EnumRoles.Department) 
                                                            || (UserContext.Roles.RoleId == (int)EnumRoles.Administrator)
                                                            || (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee);

        if (!Page.IsPostBack)
        {
            
        }
    }

    protected void lnkChangeTeacher_OnClick(object sender, EventArgs e)
    {
        using (TeacherDb db = new TeacherDb())
        {
            var list = db.GetTeacher(null, UserContext.UdodId.Value, null);
            ddlTeacher.DataSource = list;
            ddlTeacher.DataBind();
        }
        gvTeacherdatabind();

        popupSelectTeacherExtender.Show();
    }
    private void gvTeacherdatabind()
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var list = db.GetTeacher(ChildUnionId);
            gvChildUnionTeacher.DataSource = list;
            gvChildUnionTeacher.DataBind();
        }
        upSelectTeacher.Update();
    }

    protected void lnkAddTeacher_OnClick(object sender, EventArgs e)
    {
        try
        {
            if (ChildUnionId == -1)
            {
                // если ДО еще нет (ChildUnionId = -1), то добавляем фейковый, AddEmpty = true - левые значения
                ExtendedChildUnion ecu=new ExtendedChildUnion();
                addOrUpdateChildUnion(ref ecu, true, true);
                // и поставим флаг не равный -1, что добавили фейк с таким то ChildUnionId
                FakeChildUnionId = ChildUnionId;
            }

            using (ChildUnionDb db = new ChildUnionDb())
            {
                db.AddTeacher(ChildUnionId, new Guid(ddlTeacher.SelectedValue));
            }
            gvTeacherdatabind();
        }
        catch
        {
            errorPopup.ShowError("Внимание", "Невозможно добавить педагога. Сначала заполните Направленность, Профиль и Основной вид деятлеьности",
                "", "OK", false, true);
        }
    }

    protected void btnCancelTeacher_OnClick(object sender, EventArgs e)
    {
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var list = db.GetTeacher(ChildUnionId);
            repTeacher.DataSource = list;
            repTeacher.DataBind();
        }
        upTeacher.Update();
        popupSelectTeacherExtender.Hide();
    }

    protected void lnkDeleteTeacher_OnClick(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        long id = Convert.ToInt64(btn.Attributes["ChildUnionTeacherId"]);

        using (AgeGroupDb db = new AgeGroupDb())
        {
            var count = db.GetGroupsCountForTeacher(ChildUnionId, id);
            if (count!=0)
            {
                errorPopup.ShowError("Ошибка", "Невозможно удалить преподавателя из ДО, т.к. данный преподаватель выбран в группе относящейся к ДО", "", "Ок", false, true);
                return;
            }
        }
        using (ChildUnionDb db = new ChildUnionDb())
        {
            db.DeleteTeacher(id);
        }
        gvTeacherdatabind();
        
        //upSelectTeacher.Update();
    }

    protected void gvChildUnionTeacher_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton btn = (LinkButton)e.Row.FindControl("lnkDeleteTeacher");
            var tmp = (ExtendedChildUnionTeacher)e.Row.DataItem;
            btn.Attributes.Add("ChildUnionTeacherId", tmp.ChildUnionTeacherId.ToString());
        }
    }

    protected void gvLessons_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType==DataControlRowType.DataRow)
        {
            ExtendedAgeGroup item = (ExtendedAgeGroup) e.Row.DataItem;
            e.Row.Attributes.Add("AgeGroupId", item.UdodAgeGroupId.ToString());
        }
    }

    
    protected void gvLessons_OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        /*GridViewRow row = gvLessons.Rows[e.RowIndex];
        
        //RequiredFieldValidator validator = (RequiredFieldValidator)Row.FindControl("val1");
        //validator.ControlToValidate = tb.ID;

        bool isActive = ((CheckBox)row.FindControl("cbIsActive")).Checked;

        TextBox tb = (TextBox)row.FindControl("tbNumYearStart");
        int NumYearStart = 0;
        try
        {
            NumYearStart = Convert.ToInt32(tb.Text);
        }
        catch (Exception)
        {
            NumYearStart = 0;
        }

        tb = (TextBox)row.FindControl("tbNumYearEnd");
        int NumYearEnd = 0;
        try
        {
            NumYearEnd = Convert.ToInt32(tb.Text);
        }
        catch (Exception)
        {
            NumYearEnd = 0;
        }

        tb = (TextBox)row.FindControl("tbAgeStart");
        int AgeStart = 0;
        try
        {
            AgeStart = Convert.ToInt32(tb.Text);
        }
        catch (Exception)
        {
            AgeStart = 0;
        }

        tb = (TextBox)row.FindControl("tbAgeEnd");
        int AgeEnd = 0;
        try
        {
            AgeEnd = Convert.ToInt32(tb.Text);
        }
        catch (Exception)
        {
            AgeEnd = 0;
        }

        tb = (TextBox)row.FindControl("tbLessonLength");
        int LessonLength = 0;
        try
        {
            LessonLength = Convert.ToInt32(tb.Text);
        }
        catch (Exception)
        {
            errorPopup.ShowError("Ошибка", "Продолжительность занятия задана неверно.", "", "OK", false, true);
            return;
        }
        if ((isActive) && (LessonLength <= 0))
        {
            errorPopup.ShowError("Ошибка", "Продолжительность занятия должна быть больше 0.", "", "OK", false, true);
            return;
        }

        tb = (TextBox)row.FindControl("tbBreakLength");
        int BreakLength = 0;
        try
        {
            BreakLength = Convert.ToInt32(tb.Text);
        }
        catch (Exception)
        {
            errorPopup.ShowError("Ошибка", "Продолжительность перемены задана неверно.", "", "OK", false, true);
            return;
        }

        tb = (TextBox)row.FindControl("tbLessonsInWeek");
        int LessonsInWeek = 0;
        try
        {
            LessonsInWeek = Convert.ToInt32(tb.Text);
        }
        catch (Exception)
        {
            errorPopup.ShowError("Ошибка", "Количество занятий в неделю задано неверно.", "", "OK", false, true);
            return;
        }
        if ((isActive) && (LessonsInWeek <= 0))
        {
            errorPopup.ShowError("Ошибка", "Количество занятий в неделю должно быть больше 0.", "", "OK", false, true);
            return;
        }

        if ((isActive) && (LessonsInWeek * LessonLength > 4 * 7 * 60))
        {
            errorPopup.ShowError("Ошибка", "Суммарное количество часов в неделю должно быть не больше 28.", "", "OK", false, true);
            return;
        }
        

        long id = Convert.ToInt64(row.Attributes["AgeGroupId"]);
        using (AgeGroupDb db = new AgeGroupDb())
        {
            gvLessons.EditIndex = -1;
            db.UpdateAgeGroup(id, NumYearStart, NumYearEnd, AgeStart, AgeEnd, LessonLength, BreakLength, LessonsInWeek, isActive);
            db.OpenCloseChildUnion(id, null, false);
            lblAgeGroupWarning.Visible = true;

            var list = db.GetAgeGroups(null, null, null, ChildUnionId);
            gvLessons.DataSource = list;
            gvLessons.DataBind();
            upGridView.Update();
        }*/
    }

    protected void gvLessons_OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        /*gvLessons.EditIndex = e.NewEditIndex;

        using (AgeGroupDb db = new AgeGroupDb())
        {
            var list = db.GetAgeGroups(null, null, null, ChildUnionId);
            gvLessons.DataSource = list;
            gvLessons.DataBind();
            upGridView.Update();
        }
        */
    }

    protected void gvLessons_OnRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        /*gvLessons.EditIndex = -1;
        using (AgeGroupDb db = new AgeGroupDb())
        {
            var list = db.GetAgeGroups(null, null, null, ChildUnionId);
            gvLessons.DataSource = list;
            gvLessons.DataBind();
            upGridView.Update();
        }*/
    }

    protected void lnkAddAgeGroup_OnClick(object sender, EventArgs e)
    {
        if (ChildUnionId == -1)
        {
            // если ДО еще нет (ChildUnionId = -1), то добавляем фейковый, AddEmpty = true - левые значения
            ExtendedChildUnion ecu = new ExtendedChildUnion();
            addOrUpdateChildUnion(ref ecu, true, true);
            // и поставим флаг не равный -1, что добавили фейк с таким то ChildUnionId
            FakeChildUnionId = ChildUnionId;
        }

        using (AgeGroupDb db = new AgeGroupDb())
        {
            db.AddAgeGroup(UserContext.UdodId.Value, ChildUnionId, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            db.OpenCloseChildUnion(null, ChildUnionId, false);
            lblAgeGroupWarning.Visible = true;
            /*
            List<ExtendedAgeGroup> list = db.GetAgeGroups(null, null, null, ChildUnionId);
            list.Add(list[0]);
            list.RemoveAt(0);
            gvLessons.DataSource = list;
            gvLessons.EditIndex = list.Count() - 1;

            gvLessons.DataBind();
            upGridView.Update();*/
        }
    }

    protected void AgeChanged(object sender, EventArgs e)
    {
        try
        {
            int ageStart = Convert.ToInt32(tbAgeStart.Text);
            int ageEnd = Convert.ToInt32(tbAgeEnd.Text);

            if (ageStart < 3)
            {
                ageStart = 3;
                tbAgeStart.Text = "3";
            }

            if (ageEnd < 3)
            {
                ageEnd = 3;
                tbAgeEnd.Text = "3";
            }

            if (ageStart > 21)
            {
                ageStart = 21;
                tbAgeStart.Text = "21";
            }
            
            if (ageEnd > 21)
            {
                ageEnd = 21;
                tbAgeEnd.Text = "21";
            }

            /*if (ageStart <= ageEnd)
            {
                if (ChildUnionId == -1)
                {
                    // если ДО еще нет (ChildUnionId = -1), то добавляем фейковый, AddEmpty = true - левые значения
                    addOrUpdateChildUnion(true, true);
                    // и поставим флаг не равный -1, что добавили фейк с таким то ChildUnionId
                    FakeChildUnionId = ChildUnionId;
                }

                // создаем ageGroup'ы от сих до сих
                using (AgeGroupDb db = new AgeGroupDb())
                {
                    db.DeleteAgeGroups(ChildUnionId);
                    for (int i = ageStart; i <= ageEnd; i++)
                    {
                        db.AddAgeGroup(UserContext.UdodId.Value, ChildUnionId, i, 0, 0, 0, 0);
                    }

                    gvLessons.DataSource = db.GetAgeGroups(null, null, null, ChildUnionId);
                    gvLessons.DataBind();
                    upGridView.Update();
                }
            }*/

            if ((sender as TextBox).ID == "tbAgeStart") tbAgeEnd.Focus();
            if ((sender as TextBox).ID == "tbAgeEnd") tbComment.Focus(); 
        }
        catch (Exception ex)
        {

        }
    }

    protected void gvLessons_RowCreated(object sender, GridViewRowEventArgs e)
    {
       /* if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            TableCell HeaderCell = new TableCell();
            HeaderCell.Text = "";
            HeaderCell.ColumnSpan = 1;
            HeaderCell.BackColor = System.Drawing.Color.Gray;
            HeaderCell.Font.Bold = true;
            HeaderCell.Font.Size = FontUnit.Point(12);
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "Год обучения";
            HeaderCell.ColumnSpan = 2;
            HeaderCell.BackColor = System.Drawing.Color.Gray;
            HeaderCell.Font.Bold = true;
            HeaderCell.Font.Size = FontUnit.Point(12);
            HeaderGridRow.Cells.Add(HeaderCell);
 
            HeaderCell = new TableCell();
            HeaderCell.Text = "Возраст";
            HeaderCell.ColumnSpan = 2;
            HeaderCell.BackColor = System.Drawing.Color.Gray;
            HeaderCell.Font.Bold = true;
            HeaderCell.Font.Size = FontUnit.Point(12);
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "Продолжительность";
            HeaderCell.ColumnSpan = 2;
            HeaderCell.BackColor = System.Drawing.Color.Gray;
            HeaderCell.Font.Bold = true;
            HeaderCell.Font.Size = FontUnit.Point(12);
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "";
            HeaderCell.ColumnSpan = 2;
            HeaderCell.BackColor = System.Drawing.Color.Gray;
            HeaderCell.Font.Bold = true;
            HeaderCell.Font.Size = FontUnit.Point(12);
            HeaderGridRow.Cells.Add(HeaderCell);
 
            gvLessons.Controls[0].Controls.AddAt(0, HeaderGridRow);
 
        }*/
    }

    protected void gvLessons_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
       /* e.Cancel = true;

        if (gvLessons.Rows.Count == 1)
        {
            errorPopup.ShowError("Внимание", "Это последняя оставшаяся запись в таблице продолжительности занятий. Её нельзя удалить.", "", "ОК", false, true);
            return;
        }

        GridViewRow row = gvLessons.Rows[e.RowIndex];
        long id = Convert.ToInt64(row.Attributes["AgeGroupId"]);

        using (AgeGroupDb db = new AgeGroupDb())
        {
            db.DeleteAgeGroup(id);
            db.OpenCloseChildUnion(id, null, false);
            lblAgeGroupWarning.Visible = true;

            gvLessons.DataSource = db.GetAgeGroups(null, null, null, ChildUnionId);
            gvLessons.DataBind();
            upGridView.Update();
        }*/
    }

    /*
    protected void valChechBoxList_OnServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;
        foreach (ListItem item in cbxTypeBudget.Items)
        {
            if (item.Selected) args.IsValid = true;
        }
    }

    protected void CustomValidator1_OnServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;
        foreach (ListItem item in cbxEducationType.Items)
        {
            if (item.Selected) args.IsValid = true;
        }
    }

    protected void CustomValidator2_OnServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;
        foreach (ListItem item in cbxAddress.Items)
        {
            if (item.Selected) args.IsValid = true;
        }
        if (cbxAddress.Items.Count == 0) args.IsValid = true;
    }*/

    protected void ddlStatus_OnDataBound(object sender, EventArgs e)
    {
        ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue("2"));
    }

    protected void ddlForm_OnDataBound(object sender, EventArgs e)
    {
        ddlForm.SelectedIndex = ddlForm.Items.IndexOf(ddlForm.Items.FindByValue("1"));
    }
}
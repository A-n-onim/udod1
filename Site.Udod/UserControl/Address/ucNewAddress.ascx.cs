﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class UserControl_Address_ucNewAddress : BasePageControl
{
    public long AddressId
    {
        get
        {
            return (long)ViewState["AddressId"];
        }
        set
        {
            ViewState["AddressId"] = value;
        }
    }

    public string AddressStr
    {
        get
        {
            using (KladrDb db = new KladrDb())
            {
                int? typeAddressCode = null;
                if (cbRegion.SelectedValue.Substring(0,2) == "77")
                    typeAddressCode = 0;
                


                return db.GetAddress(Code, tbNumberHouse.Text, Fraction, Housing, Building, tbFlat.Text, typeAddressCode).AddressStr;
            }
        }
    }
    public int? typeAddressCode { get { if (cbRegion.SelectedValue.Substring(0,2) == "77") {return 0;} else {return null;} } }

    private string _code;
    public string Code
    {
        get
        {
            string returnvalue = (string) ViewState["CodeStreet"];
            return returnvalue; /*cbStreet.SelectedValue; */
        }
        set
        {
            _code = value;
            using (KladrDb db = new KladrDb())
            {
                tbStreet.Text = db.GetStreetName(_code, IsBti);
            }
            aceHouse.ContextKey = _code;
            ViewState["CodeStreet"] = value;
            /*
            string coderegion = _code.Substring(0, 2) + "00000000000";
            string coderayon = _code.Substring(0, 5) + "00000000";
            string codecity = _code.Substring(0, 11) + "00";
            cbRegion.DataBind();
            cbRegion.SelectedIndex = cbRegion.Items.IndexOf(cbRegion.Items.FindByValue(coderegion));
            cbRayon.DataBind();
            if (cbRayon.Items.Count > 0)
                cbRayon.SelectedIndex = cbRayon.Items.IndexOf(cbRayon.Items.FindByValue(coderayon));
            else cbRayon.DataBind();
            cbCity.SelectedIndex = cbCity.Items.IndexOf(cbCity.Items.FindByValue(codecity));
            cbStreet.DataBind();
            cbStreet.SelectedIndex =  cbStreet.Items.IndexOf(cbStreet.Items.FindByValue(_code));
            */
        }
    }
    public bool IsBti { set { ViewState["IsBti"] = value; } get { if (ViewState["IsBti"] == null) return false; return (bool)ViewState["IsBti"]; } }
    public bool Enabled { set { cbRegion.Enabled = cbRayon.Enabled = cbCity.Enabled = tbStreet.Enabled = tbNumberHouse.Enabled = tbFlat.Enabled= value; } }
    public bool VisibleFlat { set { trFlat.Visible = value; } get { return trFlat.Visible; } }
    public void Databind()
    {
        using (KladrDb db = new KladrDb())
        {
            var list = db.GetRegion();
            cbRegion.DataSource = list;
            cbRegion.DataBind();
        }
    }
    public string Index { get { return (string)ViewState["Index"];} set { ViewState["Index"] = value; } }
    public string NumberHouse
    {
        get
        {
            string[] values = tbNumberHouse.Text.Split(',');
            if (values.Count() > 0)
            {
                string[] tmp = values[0].Split('/');
                return tmp[0];
            }
            return tbNumberHouse.Text;
        } 
        set
        {
            ViewState["NumberHouse"] = value;
            SettbnmberHouse();
        }
    }
    public string Fraction { get
    {
        string[] values = tbNumberHouse.Text.Split(',');
        if (values.Count()>0)
        {
            string[] tmp = values[0].Split('/');
            if (tmp.Count() > 1) return tmp[1];
        }
        return "";
    }
        set
        {
            ViewState["Fraction"] = value;
            SettbnmberHouse();
        }
    }
    public string Housing 
    { 
        get
        {
            string[] values = tbNumberHouse.Text.Split(',');
            foreach (string value in values)
            {
                if (value.Contains("корп."))
                {
                    return value.Replace("корп.", "").Trim();
                }
            }
            return "";
        } 
        set
        {
            ViewState["Housing"] = value;
            SettbnmberHouse();
        } 
    }
    public string Building
    {
        get
        {
            string[] values = tbNumberHouse.Text.Split(',');
            foreach (string value in values)
            {
                if (value.Contains("стр."))
                {
                    return value.Replace("стр.", "").Trim();
                }
            }
            return "";
        } 
        set
        {
            ViewState["Building"] = value;
            SettbnmberHouse();
        }
    }
    public string Flat { get { return tbFlat.Text; } set { tbFlat.Text = value; } }

    private void SettbnmberHouse()
    {
        string value = "";
        if (ViewState["NumberHouse"]!=null)
        {
            value += (string) ViewState["NumberHouse"];
        }
        if (ViewState["Fraction"] != null && !string.IsNullOrEmpty((string)ViewState["Fraction"]))
        {
            value +="/" + (string)ViewState["Fraction"];
        }
        if (ViewState["Housing"] != null && !string.IsNullOrEmpty((string)ViewState["Housing"]))
        {
            value += ", корп." + (string)ViewState["Housing"];
        }
        if (ViewState["Building"] != null && !string.IsNullOrEmpty((string)ViewState["Building"]))
        {
            value += ", стр." + (string)ViewState["Building"];
        }
        tbNumberHouse.Text = value;
    }

    public bool IsValidateEdodEdit
    {
        get { return val24.Enabled; }
        set { val24.Enabled = val34.Enabled = value; }
    }

    public int AddressTypeId
    {
        get
        {
            if (!ddlAddresstype.Visible)
            {
                return (int)ViewState["AddressTypeId"];
            }
            return Convert.ToInt32(ddlAddresstype.SelectedValue);
        }
        set
        {
            divTypeAddress.Visible = ddlAddresstype.Visible = false;
            lblTypeAdress.Visible = true;
            lblTypeAdress.Text = value == 5 ? "Адрес регистрации" : value == 6 ? "Адрес проживания" : "";
            ViewState["AddressTypeId"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        cbStreet.ServicePath = SiteUtility.GetUrl("~/Webservices/Kladr.asmx");
        aceHouse.ServicePath = SiteUtility.GetUrl("~/Webservices/Kladr.asmx");
    }
    protected void cbRegion_DataBound(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(_code) || IsBti)
            cbRegion.SelectedIndex = cbRegion.Items.IndexOf(cbRegion.Items.FindByValue("7700000000000"));
        else
        {
            string coderegion = _code.Substring(0, 2) + "00000000000";
            cbRegion.SelectedIndex = cbRegion.Items.IndexOf(cbRegion.Items.FindByValue(coderegion));
            if (cbRegion.SelectedValue != "7700000000000") val24.Enabled = val34.Enabled = false;
        }
        using (KladrDb db = new KladrDb())
        {
            var list = db.GetRayon(cbRegion.SelectedValue);
            cbRayon.Items.Clear();
            cbRayon.SelectedIndex = -1;
            cbRayon.ClearSelection();
            cbRayon.DataSource = list;
            if (list.Count!=0)
            cbRayon.DataBind();
            else cbRayon_DataBound(new object(), new EventArgs());
        }



    }
    protected void cbCity_DataBound(object sender, EventArgs e)
    {
        if (cbRayon.Items.Count==0)
        {
            ListItem item= cbCity.Items.FindByValue(cbRegion.SelectedValue);
            cbCity.Items.Remove(cbCity.Items.FindByValue(cbRegion.SelectedValue));
            item.Text = "Нет";
            cbCity.Items.Insert(0, item);
        }
        if (!string.IsNullOrEmpty(_code) && !IsBti)
        {
            string codecity = _code.Substring(0, 11) + "00";
            cbCity.SelectedIndex = cbCity.Items.IndexOf(cbCity.Items.FindByValue(codecity));
        }
        else
        {
            cbCity.SelectedIndex = cbCity.Items.IndexOf(cbCity.Items.FindByValue("7700000000000"));
        }

        cbStreet.ContextKey = cbCity.SelectedValue;
        /*using (KladrDb db = new KladrDb())
        {
            var list = db.GetStreet(cbRegion.SelectedValue);
            cbStreet.DataSource = list;
            cbStreet.DataBind();
        }*/

    }
    protected void cbStreet_DataBound(object sender, EventArgs e)
    {
        /*if (!string.IsNullOrEmpty(_code))
        {
            cbStreet.SelectedIndex = cbStreet.Items.IndexOf(cbStreet.Items.FindByValue(_code));
        }*/
    }
    protected void cbRayon_DataBound(object sender, EventArgs e)
    {

        if (cbRayon.Items.Count == 0)
        {
            //cbRayon.Enabled = false;
            cbRayon.Text = "";
            trrayon.Visible = false;
            //dsCity.SelectParameters.Clear();
            //dsCity.SelectParameters.Add("code", cbRegion.SelectedValue);
            using (KladrDb db = new KladrDb())
            {
                var list = db.GetCity(cbRegion.SelectedValue);
                cbCity.DataSource = list;
                cbCity.DataBind();
            }
        }
        else
        {
            //cbRayon.Enabled = true;
            trrayon.Visible = true;
            if (!string.IsNullOrEmpty(_code))
            {
                string coderayon = _code.Substring(0, 5) + "00000000";
                cbRayon.SelectedIndex = cbRayon.Items.IndexOf(cbRayon.Items.FindByValue(coderayon));
            }
            using (KladrDb db = new KladrDb())
            {
                var list = db.GetCity(cbRayon.SelectedValue);
                cbCity.DataSource = list;
                cbCity.DataBind();
            }
        }

    }

    protected void cbRegion_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        using (KladrDb db = new KladrDb())
        {
            val24.Enabled = val34.Enabled = cbRegion.SelectedValue == "7700000000000";
            trrayon.Visible = true;
            var list = db.GetRayon(cbRegion.SelectedValue);
            cbRayon.SelectedIndex = -1;
            cbRayon.Items.Clear();
            cbRayon.DataSource = list;
            cbRayon.DataBind();
        }
        
    }

    protected void cbRayon_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        using (KladrDb db = new KladrDb())
        {
            var list = db.GetCity(cbRayon.SelectedValue);
            cbCity.DataSource = list;
            cbCity.DataBind();
        }
    }

    protected void cbCity_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        cbStreet.ContextKey = cbCity.SelectedValue;
        /*
        using (KladrDb db = new KladrDb())
        {
            var list = db.GetStreet(cbCity.SelectedValue);
            cbStreet.DataSource = list;
            cbStreet.DataBind();
        }   
        */
    }

    protected void tbStreet_OnTextChanged(object sender, EventArgs e)
    {
        string text = ((TextBox) sender).Text;
        int i = text.IndexOf("   ");
        using (KladrDb db = new KladrDb())
        {
            if (i > 0 || cbRegion.SelectedValue == "7700000000000")
            {
                aceHouse.ContextKey = db.GetStreetCode(cbCity.SelectedValue, i>0?text.Substring(0, i).Trim():text);
                if (aceHouse.ContextKey == "") lblStreetAlert.Visible = true;
                else lblStreetAlert.Visible = false;
                ViewState["CodeStreet"] = aceHouse.ContextKey;
                tbNumberHouse.Attributes.Add("setfocus", "true");
            }
        }
        
    }

    protected void tbNumberHouse_OnTextChanged(object sender, EventArgs e)
    {
        
    }

    protected void ServerValidation(object source, ServerValidateEventArgs args)
    {
        string text = tbStreet.Text;
        int i = text.IndexOf("   ");
        args.IsValid = false;
        using (KladrDb db = new KladrDb())
        {
            if (i > 0 || cbRegion.SelectedValue == "7700000000000")
            {
                string tmp = db.GetStreetCode(cbCity.SelectedValue, i > 0 ? text.Substring(0, i).Trim() : text);
                args.IsValid = !string.IsNullOrEmpty(tmp);
            }
        }
        ;
    }

    protected void cusVal1_OnServerValidate(object source, ServerValidateEventArgs args)
    {
        string text = args.Value;
        int i = text.IndexOf("   ");
        args.IsValid = false;
        using (KladrDb db = new KladrDb())
        {
            if (i > 0 || cbRegion.SelectedValue == "7700000000000")
            {
                string tmp = db.GetStreetCode(cbCity.SelectedValue, i > 0 ? text.Substring(0, i).Trim() : text);
                args.IsValid = !string.IsNullOrEmpty(tmp);
            }
        }
        upAddress.Update();
    }
}
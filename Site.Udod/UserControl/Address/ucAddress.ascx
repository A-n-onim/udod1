﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAddress.ascx.cs" Inherits="UserControl_Address_Address" %>
<link href="../../Css/StyleSheet.css" rel="stylesheet" type="text/css" />
<asp:UpdatePanel runat="server" ID="upAddress" UpdateMode="Conditional">
<ContentTemplate>
<table>
    <tr>
        <td class="tdxaddress">
            Тип адреса
        </td>
        <td>
            <div class="inputShort"><asp:DropDownList runat="server" ID="ddlAddresstype" DataTextField="Name" DataValueField="AddressTypeId" DataSourceID="dsAddressType"/>
            <asp:Label runat="server" ID="lblTypeAdress" Visible="false"></asp:Label></div>
        </td>
    </tr>
    <tr>
        <td class="tdxaddress">
            Регион
        </td>
        <td>
            <div class="inputShort"><ajaxToolkit:ComboBox runat="server" ID="cbRegion" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="cbRegion_OnSelectedIndexChanged"  OnDataBound="cbRegion_DataBound"
            DropDownStyle="DropDownList" AutoCompleteMode="Suggest" CaseSensitive="false" CssClass="WindowsStyle"  /></div>
            
        </td>

    </tr>
    <tr>
        <td class="tdxaddress">
            Район
        </td>
        <td>
            <div class="inputShort"><ajaxToolkit:ComboBox runat="server" ID="cbRayon" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnDataBound="cbRayon_DataBound" AutoCompleteMode="Suggest" CaseSensitive="false" OnSelectedIndexChanged="cbRayon_OnSelectedIndexChanged" /></div>
            
        </td>

    </tr>
    <tr>
        <td class="tdxaddress">
            Индекс
        </td>
        <td>
            <div class="inputShort"><asp:TextBox runat="server" ID="tbIndex" CssClass="inputShort" /></div>
        </td>

    </tr>
    <tr>
        <td class="tdx">
            Город
        </td>
        <td>
            <div class="inputShort"><ajaxToolkit:ComboBox runat="server" ID="cbCity" DataTextField="Name" DataValueField="Code" AutoPostBack="true"  OnDataBound="cbCity_DataBound" AutoCompleteMode="Suggest" CaseSensitive="false" OnSelectedIndexChanged="cbCity_OnSelectedIndexChanged" /></div>
            
        </td>

    </tr>
    <tr>
        <td class="tdx">
            Улица
        </td>
        <td>
            <div class="inputShort"><ajaxToolkit:ComboBox runat="server" ID="cbStreet" DataTextField="Name" DataValueField="Code" AutoPostBack="true" AutoCompleteMode="Suggest" CaseSensitive="false" ItemInsertLocation="Append" CssClass="WindowsStyle" OnDataBound="cbStreet_DataBound" /></div>
            
        </td>

    </tr>
    <tr>
        <td class="tdx">
            Номер дома
        </td>
        <td>
            <div class="inputShort"><asp:TextBox runat="server" ID="tbNumberHouse" CssClass="inputShort" /></div>
        </td>

    </tr>
    <tr>
        <td class="tdx">
            Дробь
        </td>
        <td>
            <div class="inputShort"><asp:TextBox runat="server" ID="tbFraction" CssClass="inputShort" /></div>
        </td>

    </tr>
    <tr>
        <td class="tdx">
            Корпус
        </td>
        <td>
            <div class="inputShort"><asp:TextBox runat="server" ID="tbHousing" CssClass="inputShort" /></div>
        </td>

    </tr>
    <tr>
        <td class="tdx">
            Строение
        </td>
        <td>
            <div class="inputShort"><asp:TextBox runat="server" ID="tbBuilding" CssClass="inputShort" />
        </td>

    </tr>
    <tr>
        <td td="tdx">
            Номер квартиры
        </td>
        <td>
            <div class="inputShort"><asp:TextBox runat="server" ID="tbFlat" CssClass="inputShort" /></div>
        </td>

    </tr>
    
</table>
</ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger runat="server" ControlID="cbRegion"/>
</Triggers>
</asp:UpdatePanel>


<asp:ObjectDataSource runat="server" ID="dsAddressType" TypeName="Udod.Dal.KladrDb" SelectMethod="GetAddressTypes"></asp:ObjectDataSource>
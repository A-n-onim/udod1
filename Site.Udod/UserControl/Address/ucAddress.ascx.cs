﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class UserControl_Address_Address : BasePageControl
{
    public long AddressId
    {
        get
        {
            return (long)ViewState["AddressId"];
        }
        set
        {
            ViewState["AddressId"] = value;
        }
    }

    public string AddressStr {get
        {
            using (KladrDb db = new KladrDb())
            {
                int? typeAddressCode = null;
                if (cbRegion.SelectedValue.PadLeft(2)=="77")
                    typeAddressCode = 0;
                return
                    db.GetAddress(cbStreet.SelectedValue, tbNumberHouse.Text, tbFraction.Text, tbHousing.Text, tbBuilding.Text,
                                  tbFlat.Text, typeAddressCode).AddressStr;
            }
        }
    }

    private string _code;
    public string Code { get { return cbStreet.SelectedValue; } 
        set 
        { 
            _code = value;
            /*
            string coderegion = _code.Substring(0, 2) + "00000000000";
            string coderayon = _code.Substring(0, 5) + "00000000";
            string codecity = _code.Substring(0, 11) + "00";
            cbRegion.DataBind();
            cbRegion.SelectedIndex = cbRegion.Items.IndexOf(cbRegion.Items.FindByValue(coderegion));
            cbRayon.DataBind();
            if (cbRayon.Items.Count > 0)
                cbRayon.SelectedIndex = cbRayon.Items.IndexOf(cbRayon.Items.FindByValue(coderayon));
            else cbRayon.DataBind();
            cbCity.SelectedIndex = cbCity.Items.IndexOf(cbCity.Items.FindByValue(codecity));
            cbStreet.DataBind();
            cbStreet.SelectedIndex =  cbStreet.Items.IndexOf(cbStreet.Items.FindByValue(_code));
            */
        } 
    }

    public bool Enabled { set { cbRegion.Enabled =cbRayon.Enabled = cbCity.Enabled = cbStreet.Enabled = value; } }

    public void DataBind()
    {
        using (KladrDb db = new KladrDb())
        {
            var list = db.GetRegion();
            cbRegion.DataSource = list;
            cbRegion.DataBind();
        }
    }
    public string Index { get { return tbIndex.Text; } set { tbIndex.Text = value; } }
    public string NumberHouse { get { return tbNumberHouse.Text; } set { tbNumberHouse.Text = value; } }
    public string Fraction { get { return tbFraction.Text; } set { tbFraction.Text = value; } }
    public string Housing { get { return tbHousing.Text; } set { tbHousing.Text = value; } }
    public string Building { get { return tbBuilding.Text; } set { tbBuilding.Text = value; } }
    public string Flat { get { return tbFlat.Text; } set { tbFlat.Text = value; } }
    public int AddressTypeId 
    { 
        get
        {
            if (!ddlAddresstype.Visible)
            {
                return (int) ViewState["AddressTypeId"];
            }
            return Convert.ToInt32(ddlAddresstype.SelectedValue);
        }
        set 
        { 
            ddlAddresstype.Visible = false;
            lblTypeAdress.Visible = true;
            lblTypeAdress.Text = value == 5 ? "Адрес регистрации" : value == 6 ? "Адрес проживания" : "";
            ViewState["AddressTypeId"] = value;

        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void cbRegion_DataBound(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(_code))
        cbRegion.SelectedIndex=cbRegion.Items.IndexOf(cbRegion.Items.FindByValue("7700000000000"));
        else
        {
            string coderegion = _code.Substring(0, 2) + "00000000000";
            cbRegion.SelectedIndex = cbRegion.Items.IndexOf(cbRegion.Items.FindByValue(coderegion));
        }
        using (KladrDb db= new KladrDb())
        {
            var list = db.GetRayon(cbRegion.SelectedValue);
            cbRayon.SelectedIndex = -1;
            cbRayon.DataSource = list;
            cbRayon.DataBind();
        }
        

        
    }
    protected void cbCity_DataBound(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(_code))
        {
            string codecity = _code.Substring(0, 11) + "00";
            cbCity.SelectedIndex = cbCity.Items.IndexOf(cbCity.Items.FindByValue(codecity));
        }
        else
        {
            cbCity.SelectedIndex = cbCity.Items.IndexOf(cbCity.Items.FindByValue("7700000000000"));
        }
        using (KladrDb db = new KladrDb())
        {
            var list = db.GetStreet(cbRegion.SelectedValue);
            cbStreet.DataSource = list;
            cbStreet.DataBind();
        }
        
    }
    protected void cbStreet_DataBound(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(_code))
        {
            cbStreet.SelectedIndex = cbStreet.Items.IndexOf(cbStreet.Items.FindByValue(_code));
        }
    }
    protected void cbRayon_DataBound(object sender, EventArgs e)
    {
        
        if (cbRayon.Items.Count == 0)
        {
            //cbRayon.Enabled = false;
            cbRayon.Text = "";
            //dsCity.SelectParameters.Clear();
            //dsCity.SelectParameters.Add("code", cbRegion.SelectedValue);
            using (KladrDb db = new KladrDb())
            {
                var list = db.GetCity(cbRegion.SelectedValue);
                cbCity.DataSource = list;
                cbCity.DataBind();
            }
        }
        else
        {
            //cbRayon.Enabled = true;
            if (!string.IsNullOrEmpty(_code))
            {
                string coderayon = _code.Substring(0, 5) + "00000000";
                cbRayon.SelectedIndex = cbRayon.Items.IndexOf(cbRayon.Items.FindByValue(coderayon));
            }
            using (KladrDb db = new KladrDb())
            {
                var list = db.GetCity(cbRayon.SelectedValue);
                cbCity.DataSource = list;
                cbCity.DataBind();
            }
        }
        
    }

    protected void cbRegion_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        using (KladrDb db = new KladrDb())
        {
            var list = db.GetRayon(cbRegion.SelectedValue);
            cbRayon.SelectedIndex = -1;
            cbRayon.DataSource = list;
            cbRayon.DataBind();
        }
        
    }

    protected void cbRayon_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        using (KladrDb db = new KladrDb())
        {
            var list = db.GetCity(cbRayon.SelectedValue);
            cbCity.DataSource = list;
            cbCity.DataBind();
        }
    }

    protected void cbCity_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        
        using (KladrDb db = new KladrDb())
        {
            var list = db.GetStreet(cbCity.SelectedValue);
            cbStreet.DataSource = list;
            cbStreet.DataBind();
        }   
        
    }
}
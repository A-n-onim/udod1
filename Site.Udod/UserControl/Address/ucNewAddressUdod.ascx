﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucNewAddressUdod.ascx.cs" Inherits="UserControl_Address_ucNewAddressUdod" %>
<link href="../../Css/StyleSheet.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
    var eng = new Array("Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "{", "}", "A", "S", "D", "F", "G", "H", "J", "K", "L", ":", "\"", "Z", "X", "C", "V", "B", "N", "M", "<", ">", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "\'", "z", "x", "c", "v", "b", "n", "m", ",", ".", "@");
    var rus = new Array("\u0419", "\u0426", "\u0423", "\u041A", "\u0415", "\u041D", "\u0413", "\u0428", "\u0429", "\u0417", "\u0425", "\u042A", "\u0424", "\u042B", "\u0412", "\u0410", "\u041F", "\u0420", "\u041E", "\u041B", "\u0414", "\u0416", "\u042D", "\u042F", "\u0427", "\u0421", "\u041C", "\u0418", "\u0422", "\u042C", "\u0411", "\u042E", "\u0439", "\u0446", "\u0443", "\u043A", "\u0435", "\u043D", "\u0433", "\u0448", "\u0449", "\u0437", "\u0445", "\u044A", "\u0444", "\u044B", "\u0432", "\u0430", "\u043F", "\u0440", "\u043E", "\u043B", "\u0434", "\u0436", "\u044D", "\u044F", "\u0447", "\u0441", "\u043C", "\u0438", "\u0442", "\u044C", "\u0431", "\u044E", '"');

    function bindtextbox(sender, args) {
        

    }
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(bindtextbox);

    function changeRegistr(event) {
        if (event.keyCode > 46) {
            var str = $(this).val();
            if (str.length > 0) {
                str = getEng2RusString(str);
                $(this).val(str);
            }
        }
    }

    function getEng2RusString(target) {
        var res = "";
        for (var j = 0; j < target.length; j++) {
            var test = target.charAt(j);
            for (var i = 0; i < rus.length; i++) {
                if (test == eng[i]) if (test != "\'") test = rus[i];
            }
            res += test;
        }
        return res;
    }

    function pageLoad() {
        $('.addresslist').each(function () {
            $(this).addClass('addresslist1');
        });
    }
</script>

<asp:UpdatePanel runat="server" ID="upAddress" UpdateMode="Conditional">
<ContentTemplate>
<table>
    <tr>
        <td class="tdxaddress">
            Тип адреса
        </td>
        <td>
            <div class="inputShort" runat="server" ID="divTypeAddress"><asp:DropDownList runat="server" ID="ddlAddresstype" DataTextField="Name" DataValueField="AddressTypeId" DataSourceID="dsAddressType"/>
            </div>
            <asp:Label runat="server" ID="lblTypeAdress" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="tdxaddress">
            Регион
        </td>
        <td>
            <div class="inputShort"><asp:DropDownList runat="server" ID="cbRegion" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="cbRegion_OnSelectedIndexChanged"  OnDataBound="cbRegion_DataBound"
            DropDownStyle="DropDownList" AutoCompleteMode="Suggest" CaseSensitive="false" CssClass="inputShort"  /></div>
            
        </td>

    </tr>
    <tr runat="server" ID="trrayon">
        <td class="tdxaddress" >
            Район
        </td>
        <td>
            <div class="inputShort"><asp:DropDownList runat="server" CssClass="inputShort" ID="cbRayon" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnDataBound="cbRayon_DataBound" AutoCompleteMode="Suggest" CaseSensitive="false" OnSelectedIndexChanged="cbRayon_OnSelectedIndexChanged" /></div>
            
        </td>

    </tr>
    <tr>
        <td class="tdxaddress" >
            Город или населенный пункт*
        </td>
        <td>
            <div class="inputShort"><asp:DropDownList runat="server" CssClass="inputShort" ID="cbCity" DataTextField="Name" DataValueField="Code" AutoPostBack="true"  OnDataBound="cbCity_DataBound" AutoCompleteMode="Suggest" CaseSensitive="false" OnSelectedIndexChanged="cbCity_OnSelectedIndexChanged" /></div>
            
        </td>

    </tr>
    <tr>
        <td class="tdxaddress">
            Улица*
        </td>
        <td>
            <ajaxToolkit:ComboBox runat="server" ID="cbStreet" DropDownStyle="DropDownList" DataTextField="Name" DataValueField="Code" AutoPostBack="true" AutoCompleteMode="Suggest" OnSelectedIndexChanged="cbStreet_OnSelectedIndexChanged"/>
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="val21" ErrorMessage="Поле 'Улица' не заполнено" ControlToValidate="cbStreet" SetFocusOnError="true" ValidationGroup="71"></asp:RequiredFieldValidator>
            <%--<asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="val22" ErrorMessage="Поле 'Улица' не заполнено" ControlToValidate="tbStreet" SetFocusOnError="true" ValidationGroup="50"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="val23" ErrorMessage="Поле 'Улица' не заполнено" ControlToValidate="tbStreet" SetFocusOnError="true" ValidationGroup="80"></asp:RequiredFieldValidator>
             <asp:CustomValidator  ID="cusVal1" ControlToValidate="tbStreet" Display="Dynamic" ErrorMessage="Неверная улица" OnServerValidate="ServerValidation" runat="server" ValidationGroup="71" SetFocusOnError="true"></asp:CustomValidator>--%>
        </td>

    </tr>
    <tr>
        <td colspan=2>
            <asp:Label ID="lblStreetAlert" runat="server" ForeColor="Red" Visible="false">Выбранная улица не существует. Проверьте правильность написания.</asp:Label>
        </td>
    </tr>
    <tr>
        <td class="tdxaddress" >
            Номер дома*
        </td>
        <td>
            <ajaxToolkit:ComboBox runat="server" ID="cbHouse" DataTextField="Name" DataValueField="Code" AutoCompleteMode="Suggest"/>
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="val31" ErrorMessage="Поле 'Номер дома' не заполнено" ControlToValidate="cbHouse" SetFocusOnError="true" ValidationGroup="71"></asp:RequiredFieldValidator>
            <%--<asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="val32" ErrorMessage="Поле 'Номер дома' не заполнено" ControlToValidate="tbNumberHouse" SetFocusOnError="true" ValidationGroup="50"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="val33" ErrorMessage="Поле 'Номер дома' не заполнено" ControlToValidate="tbNumberHouse" SetFocusOnError="true" ValidationGroup="80"></asp:RequiredFieldValidator>
            --%>
        </td>

    </tr>
    <tr runat="server" ID="trFlat">
        <td class="tdxaddress" >
            Номер квартиры
        </td>
        <td>
            <div class="inputShort"><asp:TextBox runat="server" ID="tbFlat" CssClass="inputShort" /></div>
        </td>

    </tr>
    
</table>
</ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger runat="server" ControlID="cbRegion"/>
    <asp:AsyncPostBackTrigger runat="server" ControlID="cbRayon"/>
</Triggers>
</asp:UpdatePanel>


<asp:ObjectDataSource runat="server" ID="dsAddressType" TypeName="Udod.Dal.KladrDb" SelectMethod="GetAddressTypes"></asp:ObjectDataSource>
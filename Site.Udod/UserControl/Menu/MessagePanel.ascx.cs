﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class UserControl_Menu_MessagePanel : BasePageControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lnkAddmessage.Visible = tbMessage.Visible = UserContext.Roles.IsAdministrator;
        if (!Page.IsPostBack)
        {
            using (MessageDb db = new MessageDb())
            {
                var list = db.GetUserMessage(UserContext.Profile.UserId, false);
                if (list.Count > 0)
                {
                    pMessages.Visible = true;
                    gvMessages.DataSource = list;
                    gvMessages.DataBind();
                }
                else pMessages.Visible = false;
            }
            upError.Update();
        }
        
    }

    protected void gvMessages_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        uErrorPopup.ShowError("Подтверждение", "Считать выбранное сообщение прочитанным?", "Да", "Нет", true, true);
    }

    

    protected void lnkAddmessage_OnClick(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(tbMessage.Text))
        {
            using (MessageDb db = new MessageDb())
            {
                db.AddAdminMessage(tbMessage.Text);
                var list = db.GetUserMessage(UserContext.Profile.UserId, false);
                if (list.Count > 0)
                {
                    pMessages.Visible = true;
                    gvMessages.DataSource = list;
                    gvMessages.DataBind();
                }
                else pMessages.Visible = false;
            }

            upError.Update();
        }
    }

    protected void uErrorPopup_OnErrorOkClick(object sender, EventArgs e)
    {
        var messageAdminId = Convert.ToInt32(gvMessages.SelectedDataKey.Value);
        using (MessageDb db = new MessageDb())
        {
            db.MessageRead(UserContext.Profile.UserId, messageAdminId);
            var list = db.GetUserMessage(UserContext.Profile.UserId, false);
            if (list.Count > 0)
            {
                pMessages.Visible = true;
                gvMessages.DataSource = list;
                gvMessages.DataBind();
            }
            else pMessages.Visible = false;
            upError.Update();
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class UserControl_Menu_ucTimer : BasePageControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lnkEditTimer.Visible = UserContext.Roles.RoleId == (int)EnumRoles.Administrator;
    }

    public long GetDeadline(int num)
    {
        List<DateTime> list = new List<DateTime>();
        using (CommonDb db = new CommonDb())
        {
            list = db.GetTimerUpdateDatetimes();
        }
        if (UserContext.Roles.RoleId == (int)EnumRoles.Administrator)
        {
            TimeSpan elapsedSpan = new TimeSpan((list[0] - DateTime.Now).Ticks);
            return (long) elapsedSpan.TotalMilliseconds;
        }
        else
        {
            TimeSpan elapsedSpan = new TimeSpan((list[num] - DateTime.Now).Ticks);
            return (long)elapsedSpan.TotalMilliseconds;
        }
        
    }

    public void lnkEditTimer_OnClick(object sender, EventArgs e)
    {
        tbTimerStart.Text = "";
        tbTimerEnd.Text = "";
        timerModalPopupExtender.Show();
        upTimer.Update();
    }

    public void lnkTimerOkOnClick(object sender, EventArgs e)
    {
        DateTime start;
        DateTime end;
        try
        {
            start = Convert.ToDateTime(tbTimerStart.Text);
            end = Convert.ToDateTime(tbTimerEnd.Text);
        }
        catch
        {
            return;
        }
        if (start > end) return;
        if (start < DateTime.Now) return;
        if (end < DateTime.Now) return;

        using (CommonDb db = new CommonDb())
        {
            db.UpdateTimerDatetimes(start, end);
        }

        timerModalPopupExtender.Hide();
    }

    public void lnkTimerCancelOnClick(object sender, EventArgs e)
    {
        timerModalPopupExtender.Hide();
    }
}
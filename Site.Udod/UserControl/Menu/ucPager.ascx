﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucPager.ascx.cs" Inherits="UserControl_Menu_ucPager" %>

<asp:UpdatePanel ID="upRep" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <asp:Repeater runat="server" ID="rptPages" OnItemDataBound="rptPages_OnItemDataBound" >
            <HeaderTemplate>
                <div style="background-color:RGB(40,84,147);"><table><tr>
            </HeaderTemplate>
            <ItemTemplate>
                <td style="padding-left: 3px;">
                    <asp:LinkButton runat="server" ID="lnkPage" OnClick="lnkPage_OnClick" CssClass="newgridpager"><%# Eval("Text") %></asp:LinkButton>
                </td>
            </ItemTemplate>
            <FooterTemplate>
                </tr></table></div>
            </FooterTemplate>
        </asp:Repeater>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Выводить по " />
                </td>
                <td>
                    <div class="inputXShort" style="margin-top: 6px;">
                        <asp:DropDownList runat="server" ID="ddlNumItemsOnPage" CssClass="inputXShort" OnSelectedIndexChanged="ddlNumItemsOnPage_SelectedIndexChanged" AutoPostBack="true"/>
                    </div>
                </td>
                <td>
                    <asp:Label ID="Label2" runat="server" Text=" элементов на странице." />
                </td>
                <td>
                    <asp:UpdatePanel ID="upNumItemsInGv" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="lblNumItemsInGv" runat="server" />
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
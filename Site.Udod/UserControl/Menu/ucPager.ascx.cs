﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class UserControl_Menu_ucPager : BasePageControl
{
    public int SelectedPageNum
    {
        get
        {
            return (int)ViewState["SelectedPageNum"];
        }
        set
        {
            ViewState["SelectedPageNum"] = value;
        }
    }
    public int pagesCount
    {
        get
        {
            return (int)ViewState["pagesCount"];
        }
        set
        {
            ViewState["pagesCount"] = value;
        }
    }
    public int? numElements 
    {
        get
        {
            return (int?)ViewState["numElements"];
        }
        set
        {
            ViewState["numElements"] = value;
            Databind();
        }
    }
    public int pageStart
    {
        get
        {
            return (int)ViewState["pageStart"];
        }
        set
        {
            ViewState["pageStart"] = value;
        }
    }

    public event EventHandler Change;

    public int PagerIndex
    {
        get
        {
            return (int)ViewState["PagerIndex"];
        }
        set
        {
            ViewState["PagerIndex"] = value;
        }
    }
    public int PagerLength
    {
        get
        {
            return (int)ViewState["PagerLength"];
        }
        set
        {
            ViewState["PagerLength"] = value;
        }
    }

    public bool needStartDots
    {
        get
        {
            return (bool)ViewState["needStartDots"];
        }
        set
        {
            ViewState["needStartDots"] = value;
        }
    }

    public bool needEndDots
    {
        get
        {
            return (bool)ViewState["needEndDots"];
        }
        set
        {
            ViewState["needEndDots"] = value;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //if (!Page.IsPostBack)
        {
            Init();
        }
        //Databind();
    }

    protected void rptPages_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
        {
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkPage");
            lnk.Attributes.Add("Number", ((ExtendedMenuItem)e.Item.DataItem).Text);
            
            if (((ExtendedMenuItem)e.Item.DataItem).Text == SelectedPageNum.ToString())
                lnk.Style.Add("background-color", "Gray");
        }
    }

    protected void Databind()
    {
        if (numElements.HasValue)
        {
            List<ExtendedMenuItem> list = new List<ExtendedMenuItem>();
            list.Clear();

            if (needStartDots) list.Add(new ExtendedMenuItem { Text = "<-" });

            int end = numElements.Value / PagerLength;
            if (numElements.Value % PagerLength != 0) end += 1;

            needEndDots = false;
            if (end - pageStart + 1 > pagesCount)
            {
                end = pageStart - 1 + pagesCount;
                needEndDots = true;
            }

            for (int i = pageStart; i <= end; i++)
            {
                list.Add(new ExtendedMenuItem { Text = i.ToString() });
            }

            if (needEndDots) list.Add(new ExtendedMenuItem { Text = "->" });

            rptPages.DataSource = list;
            rptPages.DataBind();

            lblNumItemsInGv.Text = "Всего " + numElements.Value + " элемент" +
                ((((numElements.Value - 2) % 10 == 0) || ((numElements.Value - 3) % 10 == 0) || ((numElements.Value - 4) % 10 == 0)) ? "а" :
                ((numElements.Value - 1) % 10 == 0) ? "" : "ов");
        }
    }

    protected void lnkPage_OnClick(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        if (lnk.Attributes["Number"] == "->")
        {
            pageStart += pagesCount;
            PagerIndex = (pageStart-1)*PagerLength+1;
            needStartDots = true;
            SelectedPageNum = pageStart;
        }
        else if (lnk.Attributes["Number"] == "<-")
        {
            PagerIndex = (pageStart - 2)*PagerLength+1;
            pageStart -= pagesCount;
            if (pageStart < 1) pageStart = 1;
            if (pageStart == 1) needStartDots = false;
            SelectedPageNum = pageStart + pagesCount - 1;
        }
        else
        {
            SelectedPageNum = Convert.ToInt32(lnk.Attributes["Number"]);
            PagerIndex = PagerLength * (Convert.ToInt32(lnk.Attributes["Number"])-1) + 1;
        }

        if (Change != null) Change(this, new EventArgs());
    }

    protected void ddlNumItemsOnPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        Clear();

        Databind();

        if (Change != null) Change(this, new EventArgs());
    }

    public void Clear()
    {
        SelectedPageNum = 1;
        PagerIndex = 1;
        PagerLength = Convert.ToInt32(ddlNumItemsOnPage.SelectedValue);
        pageStart = 1;
        needStartDots = false;
    }

    public void Init()
    {
        ddlNumItemsOnPage.Items.Clear();
        //ddlNumItemsOnPage.Items.Add(new ListItem() { Text = "2", Value = "2" });
        ddlNumItemsOnPage.Items.Add(new ListItem() { Text = "15", Value = "15" });
        ddlNumItemsOnPage.Items.Add(new ListItem() { Text = "30", Value = "30" });
        ddlNumItemsOnPage.Items.Add(new ListItem() { Text = "50", Value = "50" });
        ddlNumItemsOnPage.Items.Add(new ListItem() { Text = "100", Value = "100" });
        ddlNumItemsOnPage.SelectedIndex = 0;

        Clear();
    }
}
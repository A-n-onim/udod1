﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class ucLeftMenu : BasePageControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            aFind.HRef = SiteUtility.GetUrl("~/default.aspx");
            //aClaim.HRef = SiteUtility.GetUrl("~/Claim/NewClaim.aspx");
            aClaimList.HRef = SiteUtility.GetUrl("~/Udod/ClaimList.aspx");
            audodOsip.HRef = SiteUtility.GetUrl("~/Udod/UdodList.aspx");
            //aPupils.HRef = SiteUtility.GetUrl("~/Udod/PupilList.aspx");
            aImportedPupils.HRef = SiteUtility.GetUrl("~/Udod/ImportedPupilList.aspx");
            aSchools.HRef = SiteUtility.GetUrl("~/Udod/SchoolPupilList.aspx");
            List<ExtendedMenuItem> list = new List<ExtendedMenuItem>();
            #region Справочники

            if (UserContext.Roles.RoleId == (int)EnumRoles.Administrator)
            {
                accSchedule.Visible= accFind.Visible = accUdod.Visible = accDict.Visible = accReport.Visible = accNewClaim.Visible = accUser.Visible = accSchools.Visible = accPupils.Visible = true;
                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Udod/UdodList.aspx", Tooltip = "Список учреждений", Text = "Список УДО" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/TeacherList.aspx", Tooltip = "Список педагогов", Text = "Список педагогов" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ChildUnionList.aspx", Tooltip = "Список детских объединений", Text = "Список ДО" });
                //list.Add(new ExtendedMenuItem { Url = "~/Udod/AgeGroupList.aspx", Tooltip = "Список возрастных групп", Text = "Список групп" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ClaimList.aspx", Tooltip = "Список заявлений", Text = "Список заявлений" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ImportedPupilList.aspx", Tooltip = "Загрузка 2 года", Text = "Загрузка 2 года" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/AgeGroupList.aspx", Tooltip = "Список групп", Text = "Группы" });
                rptUdod.DataSource = list;
                rptUdod.DataBind();

                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Udod/PupilList.aspx", Tooltip = "Список обучающихся", Text = "Список обучающихся" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/TransferPupilList.aspx", Tooltip = "Перевод на следующий год обучения", Text = "Перевод на следующий год обучения" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/GroupPupils.aspx", Tooltip = "Распределение обучающихся по группам", Text = "Распределение" });
                rptPupils.DataSource = list;
                rptPupils.DataBind();

                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Report/ReportClaimCount.aspx", Tooltip = "Список по регистрации в УДО", Text = "Статистика по регистрации" });
                list.Add(new ExtendedMenuItem { Url = "~/Report/ClaimNumber.aspx", Tooltip = "Кол-во заявок по округам/районам", Text = "Кол-во заявок по округам/районам" });
                list.Add(new ExtendedMenuItem { Url = "~/Report/NoClaimList.aspx", Tooltip = "Список ДО, в которые никто не записался", Text = "Список ДО, в которые никто не записался" });
                list.Add(new ExtendedMenuItem { Url = "~/Report/TopClaimList.aspx", Tooltip = "Список востребованных ДО", Text = "Список востребованных ДО" });
                list.Add(new ExtendedMenuItem { Url = "~/Report/ReportForOsip.aspx", Tooltip = "Отчет по годам обучения", Text = "По годам обучения" });
                list.Add(new ExtendedMenuItem { Url = "~/Report/ReportAge.aspx", Tooltip = "Отчет по возрастам", Text = "Отчет по возрастам" });
                list.Add(new ExtendedMenuItem { Url = "~/Report/ReportAgeSex.aspx", Tooltip = "Отчет по полу и направленностям", Text = "Отчет по полу и направленностям" });
                //list.Add(new ExtendedMenuItem { Url = "~/Report/TransferReportForOsip.aspx", Tooltip = "Отчет по переводу на следующий год", Text = "Отчет по переводу на следующий год" });
                repReport.DataSource = list;
                repReport.DataBind();

                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Udod/SectionList.aspx", Tooltip = "Реализуемые виды деятельности (предметы)", Text = "Реализуемые виды деятельности" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ProfileList.aspx", Tooltip = "Список профилей", Text = "Список профилей" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ProgramList.aspx", Tooltip = "Список направлений", Text = "Список направлений" });
                repDict.DataSource = list;
                repDict.DataBind();

                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/User/UserList.aspx", Tooltip = "Список пользователей", Text = "Список пользователей" });
                list.Add(new ExtendedMenuItem { Url = "~/User/Options.aspx", Tooltip = "Обновление БТИ", Text = "Обновление БТИ" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/AddressList.aspx", Tooltip = "Адреса учреждений", Text = "Адреса учреждений" });
                list.Add(new ExtendedMenuItem { Url = "~/Events/EventsList.aspx", Tooltip = "Список событий", Text = "Список событий" });
                repUser.DataSource = list;
                repUser.DataBind();

                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Udod/Schedule.aspx", Tooltip = "Расписание занятий", Text = "Расписание" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ScheduleHistory.aspx", Tooltip = "История занятий", Text = "История занятий" });
                rptSchedule.DataSource = list;
                rptSchedule.DataBind();

                accInfo.Visible = true;
                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Udod/DocumentsList.aspx", Tooltip = "Документы", Text = "Документы" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ContactsOsip.aspx", Tooltip = "Контакты ОСИП", Text = "Контакты ОСИП" });
                list.Add(new ExtendedMenuItem { Url = "~/Report/MessagesInfo.aspx", Tooltip = "Сообщения системы", Text = "Сообщения" });
                repInfo.DataSource = list;
                repInfo.DataBind();
            }

            if (UserContext.Roles.RoleId == (int)EnumRoles.Department)
            {
                aClaimList.Visible = true;

                accClaimList.Visible = accFind.Visible = accUdod.Visible = accReport.Visible = true;
                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Udod/UdodList.aspx", Tooltip = "Список учреждений", Text = "Список УДО" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ChildUnionList.aspx", Tooltip = "Список детских объединений", Text = "Список ДО" });
                rptUdod.DataSource = list;
                rptUdod.DataBind();

                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Report/ReportForOsip.aspx", Tooltip = "Отчет по годам обучения", Text = "По годам обучения" });
                repReport.DataSource = list;
                repReport.DataBind();

                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Udod/SectionList.aspx", Tooltip = "Список секций", Text = "Список секций" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ProfileList.aspx", Tooltip = "Список профилей", Text = "Список профилей" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ProgramList.aspx", Tooltip = "Список направлений", Text = "Список направлений" });
                repDict.DataSource = list;
                repDict.DataBind();

                accInfo.Visible = true;
                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Udod/DocumentsList.aspx", Tooltip = "Документы", Text = "Документы" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ContactsOsip.aspx", Tooltip = "Контакты ОСИП", Text = "Контакты ОСИП" });
                list.Add(new ExtendedMenuItem { Url = "~/Report/MessagesInfo.aspx", Tooltip = "Сообщения системы", Text = "Сообщения" });
                repInfo.DataSource = list;
                repInfo.DataBind();

                accPupils.Visible = true;
                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Udod/PupilList.aspx", Tooltip = "Список обучающихся", Text = "Список обучающихся" });
                rptPupils.DataSource = list;
                rptPupils.DataBind();
            }

            if (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee)
            {
                if (UserContext.Profile.UserName.ToUpper() == "TESTUSER")
                {
                    accUdod.Visible = true;
                    accInfo.Visible= accNewClaim.Visible = false;
                    list.Clear();
                    list.Add(new ExtendedMenuItem { Url = "~/Udod/ChildUnionList.aspx", Tooltip = "Список детских объединений", Text = "Список ДО" });
                    rptUdod.DataSource = list;
                    rptUdod.DataBind();

                    list.Clear();
                    list.Add(new ExtendedMenuItem { Url = "~/Udod/DocumentsList.aspx", Tooltip = "Документы", Text = "Документы" });
                    list.Add(new ExtendedMenuItem { Url = "~/Udod/ContactsOsip.aspx", Tooltip = "Контакты ОСИП", Text = "Контакты ОСИП" });
                    list.Add(new ExtendedMenuItem { Url = "~/Report/MessagesInfo.aspx", Tooltip = "Сообщения системы", Text = "Сообщения" });
                    repInfo.DataSource = list;
                    repInfo.DataBind();
                }
                else
                {

                    accInfo.Visible = true;
                    accSchedule.Visible = accPupils.Visible = accClaimList.Visible = accUdod.Visible = accDict.Visible = accImportedPupils.Visible = true;
                    accReport.Visible = true;
                    string values = ConfigurationManager.AppSettings["SecondYear"];
                    var splits = values.Split(',');
                    accImportedPupils.Visible = UserContext.UdodId.HasValue && splits.Contains(UserContext.UdodId.Value.ToString());

                    //values = ConfigurationManager.AppSettings["Schools"];
                    //var schoolSplits = values.Split(',');
                    ExtendedUdod udod;
                    if (UserContext.UdodId.HasValue)
                        using (UdodDb db = new UdodDb())
                        {
                            udod = db.GetUdod(UserContext.UdodId.Value);
                        }
                    else
                    {
                        udod = new ExtendedUdod() { IsContingentEnabled = false };
                    }
                    accSchools.Visible = UserContext.UdodId.HasValue && udod.IsContingentEnabled;

                    accNewClaim.Visible = true;
                    list.Clear();
                    list.Add(new ExtendedMenuItem { Url = "~/Udod/UdodEdit.aspx", Tooltip = "Данные учреждения", Text = "Учреждение" });
                    list.Add(new ExtendedMenuItem { Url = "~/Udod/TeacherList.aspx", Tooltip = "Список педагогов", Text = "Педагоги" });
                    //list.Add(new ExtendedMenuItem { Url = "~/Udod/AgeGroupList.aspx", Tooltip = "Список возрастных групп", Text = "Список групп" });
                    list.Add(new ExtendedMenuItem { Url = "~/Udod/ChildUnionList.aspx", Tooltip = "Список детских объединений", Text = "Список ДО" });
                    //list.Add(new ExtendedMenuItem { Url = "~/Udod/ClaimList.aspx", Tooltip = "Список заявлений", Text = "Список заявлений" });
                    //list.Add(new ExtendedMenuItem { Url = "~/Udod/PupilList.aspx", Tooltip = "Список обучающихся", Text = "Список обучающихся" });
                    list.Add(new ExtendedMenuItem { Url = "~/Udod/AgeGroupList.aspx", Tooltip = "Список групп", Text = "Группы" });
                    rptUdod.DataSource = list;
                    rptUdod.DataBind();

                    list.Clear();
                    list.Add(new ExtendedMenuItem { Url = "~/Udod/PupilList.aspx", Tooltip = "Список обучающихся", Text = "Список обучающихся" });
                    //list.Add(new ExtendedMenuItem { Url = "~/Udod/TransferPupilList.aspx", Tooltip = "Перевод на следующий год обучения", Text = "Перевод на следующий год обучения" });
                    list.Add(new ExtendedMenuItem { Url = "~/Udod/GroupPupils.aspx", Tooltip = "Распределение обучающихся по группам", Text = "Распределение" });
                    rptPupils.DataSource = list;
                    rptPupils.DataBind();

                    list.Clear();
                    list.Add(new ExtendedMenuItem { Url = "~/Udod/SectionList.aspx", Tooltip = "Список реализуемых видов деятельности", Text = "Виды деятельности (предметы)" });
                    repDict.DataSource = list;
                    repDict.DataBind();

                    list.Clear();
                    //list.Add(new ExtendedMenuItem { Url = "~/Udod/ClaimList.aspx", Tooltip = "Реестр заявлений", Text = "Реестр заявлений" });
                    list.Add(new ExtendedMenuItem { Url = "~/Report/ReportForOsip.aspx", Tooltip = "Отчет по годам обучения", Text = "По годам обучения" });
                    list.Add(new ExtendedMenuItem { Url = "~/Report/ReportAge.aspx", Tooltip = "Отчет по возрастам", Text = "Отчет по возрастам" });
                    list.Add(new ExtendedMenuItem { Url = "~/Report/ReportAgeSex.aspx", Tooltip = "Отчет по полу и направленностям", Text = "Отчет по полу и направленностям" });
                    repReport.DataSource = list;
                    repReport.DataBind();

                    list.Clear();
                    list.Add(new ExtendedMenuItem { Url = "~/Udod/DocumentsList.aspx", Tooltip = "Документы", Text = "Документы" });
                    list.Add(new ExtendedMenuItem { Url = "~/Udod/ContactsOsip.aspx", Tooltip = "Контакты ОСИП", Text = "Контакты ОСИП" });
                    list.Add(new ExtendedMenuItem { Url = "~/Report/MessagesInfo.aspx", Tooltip = "Сообщения системы", Text = "Сообщения" });
                    repInfo.DataSource = list;
                    repInfo.DataBind();

                    list.Clear();
                    
                    
                    list.Add(new ExtendedMenuItem { Url = "~/Udod/Schedule.aspx", Tooltip = "Расписание занятий", Text = "Расписание" });
                    list.Add(new ExtendedMenuItem { Url = "~/Udod/ScheduleHistory.aspx", Tooltip = "История занятий", Text = "История занятий" });
                    rptSchedule.DataSource = list;
                    rptSchedule.DataBind();
                }
            }
            if (UserContext.Roles.RoleId == (int)EnumRoles.Osip)
            {
                accFind.Visible = 
                accNewClaim.Visible = 
                accUdod.Visible = 
                //accUdodOsip.Visible = true;
                //accDict.Visible = true;
                accClaimList.Visible = 
                accReport.Visible = accSchedule.Visible=
                accPupils.Visible = true;
                list.Clear();
                //list.Add(new ExtendedMenuItem { Url = "~/Udod/ClaimList.aspx", Tooltip = "Реестр заявлений", Text = "Реестр заявлений" });
                list.Add(new ExtendedMenuItem { Url = "~/Report/ReportForOsip.aspx", Tooltip = "Отчет по годам обучения", Text = "По годам обучения" });
                //list.Add(new ExtendedMenuItem { Url = "~/Report/TransferReportForOsip.aspx", Tooltip = "Отчет по переводу на следующий год", Text = "Отчет по переводу на следующий год" });
                repReport.DataSource = list;
                repReport.DataBind();

                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Udod/PupilList.aspx", Tooltip = "Список обучающихся", Text = "Список обучающихся" });
                rptPupils.DataSource = list;
                rptPupils.DataBind();

                list.Clear();
                //list.Add(new ExtendedMenuItem { Url = "~/Udod/ClaimList.aspx", Tooltip = "Реестр заявлений", Text = "Реестр заявлений" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/UdodList.aspx", Tooltip = "Список учреждений", Text = "Список УДО" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ChildUnionList.aspx", Tooltip = "Список детских объединений", Text = "Список ДО" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/AgeGroupList.aspx", Tooltip = "Список групп", Text = "Группы" });
                rptUdod.DataSource = list;
                rptUdod.DataBind();
                
                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Udod/SectionList.aspx", Tooltip = "Список секций", Text = "Список секций" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ProfileList.aspx", Tooltip = "Список профилей", Text = "Список профилей" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ProgramList.aspx", Tooltip = "Список направлений", Text = "Список направлений" });
                repDict.DataSource = list;
                repDict.DataBind();

                accInfo.Visible = true;
                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Udod/DocumentsList.aspx", Tooltip = "Документы", Text = "Документы" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ContactsOsip.aspx", Tooltip = "Контакты ОСИП", Text = "Контакты ОСИП" });
                list.Add(new ExtendedMenuItem { Url = "~/Report/MessagesInfo.aspx", Tooltip = "Сообщения системы", Text = "Сообщения" });
                repInfo.DataSource = list;
                repInfo.DataBind();

                list.Clear();


                list.Add(new ExtendedMenuItem { Url = "~/Udod/Schedule.aspx", Tooltip = "Расписание занятий", Text = "Расписание" });
                //list.Add(new ExtendedMenuItem { Url = "~/Udod/ScheduleHistory.aspx", Tooltip = "История занятий", Text = "История занятий" });
                rptSchedule.DataSource = list;
                rptSchedule.DataBind();
            }

            if (UserContext.Roles.RoleId == (int)EnumRoles.User)
            {
                accDict.Visible = false;
                //lblDict.Visible = false;
                //lblUser.Visible = false;
                accUser.Visible = false;
                //PopupMenu.Visible = false;
                accInfo.Visible = true;
                list.Clear();
                list.Add(new ExtendedMenuItem { Url = "~/Udod/DocumentsList.aspx", Tooltip = "Документы", Text = "Документы" });
                list.Add(new ExtendedMenuItem { Url = "~/Udod/ContactsOsip.aspx", Tooltip = "Контакты ОСИП", Text = "Контакты ОСИП" });
                repInfo.DataSource = list;
                repInfo.DataBind();
            }
            //if (UserContext.Roles.RoleId == (int)EnumRoles.Osip) Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
            #endregion
            #region Списки пользователей
            //UserList.Visible=lblUser.Visible = false;
            //accUser.Visible = false;
            /*list.Clear();
            if (UserContext.Roles.IsAdministrator)
            {
                list.Add(new ExtendedMenuItem { Url = "~/User/UserList.aspx", Tooltip = "Пользователи", Text = "Пользователи" });
                list.Add(new ExtendedMenuItem { Url = "~/User/PupilList.aspx", Tooltip = "Воспитанники", Text = "Воспитанники" });
                list.Add(new ExtendedMenuItem { Url = "~/User/TeacherList.aspx", Tooltip = "Список педагогов", Text = "Педагоги" });
                list.Add(new ExtendedMenuItem { Url = "~/User/RelativeList.aspx", Tooltip = "Список заявителей", Text = "Заявители" });
            }
            
            repUser.DataSource = list;
            repUser.DataBind();
            */

            #endregion

            if (Page.Request.Url.AbsoluteUri.ToLower() == SiteUtility.GetUrl("~/Claim/NewClaim.aspx").ToLower())
                lnkNewClaim.CssClass = "selected";
            else if (Page.Request.Url.AbsoluteUri.ToLower() == aFind.HRef.ToLower())
                aFind.Attributes.Add("class", "selected");
            else if (Page.Request.Url.AbsoluteUri.ToLower() == aClaimList.HRef.ToLower())
                aClaimList.Attributes.Add("class", "selected");
            else if (Page.Request.Url.AbsoluteUri.ToLower() == audodOsip.HRef.ToLower())
                audodOsip.Attributes.Add("class", "selected");
            //else if (Page.Request.Url.AbsoluteUri.ToLower() == aPupils.HRef.ToLower())
            //    aPupils.Attributes.Add("class", "selected");
            //else if (Page.Request.Url.AbsoluteUri.ToLower() == aDocs.HRef.ToLower())
            //    aDocs.Attributes.Add("class", "selected");
        }
    }

    protected void lnkDefault_OnClick(object sender, EventArgs e)
    {
        Page.Response.Redirect(SiteUtility.GetUrl("~/default.aspx"));
    }

    protected void lnkClaim_OnClick(object sender, EventArgs e)
    {
        Page.Response.Redirect(SiteUtility.GetUrl("~/Udod/ClaimList.aspx"));
    }

    protected void Logout_OnClick(object sender, EventArgs e)
    {
        Session.Abandon();
        FormsAuthentication.SignOut();
        Page.Response.Redirect(SiteUtility.GetUrl("~/default.aspx"));
    }

    protected void lnkLogout_OnClick(object sender, EventArgs e)
    {
        Session.Abandon();
        FormsAuthentication.SignOut();
        Page.Response.Redirect(SiteUtility.GetUrl("~/default.aspx"));
    }

    protected void lnkNewClaim_OnClick(object sender, EventArgs e)
    {
        UserContext.ClaimId = null;
        Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
    }
}
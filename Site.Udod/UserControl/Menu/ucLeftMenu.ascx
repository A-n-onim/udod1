﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucLeftMenu.ascx.cs" Inherits="ucLeftMenu" %>

<style type="text/css">
 /*Выпадающее меню*/
.slidemenu  
{
    font-size:8.5pt;
    font-family:Tahoma;
    background-color: transparent;
    width: 180px;
    overflow: hidden !important;
}
.slidemenu .content
{
    overflow: hidden !important;
}
.slidemenu .header
{
    cursor:pointer;
    background-color: transparent;
    font-size: 11pt;
    vertical-align: middle;
}
.slidemenu .header div
{
    cursor:pointer;
    height:50px;
    /*padding-left:40px;*/
    background-color:Transparent;
    background-position:center left;
    background-repeat:no-repeat;
}
.slidemenu .header span, .slidemenu .header a
{
    cursor:pointer;
    font-weight:bold;
    display:block;
    color: RGB(42,71,146);
    text-decoration: none;
    padding: 15px 0px 12px 15px;
}
.slidemenu .header:hover
{
    cursor:pointer;
    height:50px;
    background-color:transparent;
    border-bottom:none;
}                
.slidemenu ul
{
    margin:0;
    padding:2px;
    list-style-type:none;
}
.slidemenu li
{
    background-color:Transparent;
    background-repeat:no-repeat;
    background-position:left center;
    vertical-align:middle;
    padding:0;
}
.slidemenu li div
{
    padding-left:5px;
    color:#666666;
}

.slidemenu li a {color: #0095E6; font: bold 12px Sans-Serif; display: block; text-decoration: none;}
.slidemenu li a {background: no-repeat; padding: 12px 0px 0px 30px; height: 28px}
.slidemenu li a:hover {color: #e95400;}

.slidemenu li:hover
{
    /*text-decoration:underline;*/
}

</style>
<script type="text/javascript">

    $(document).ready(function () {
        if ($('.slidemenu a.selected').length) return;

        $('.slidemenu .dn').each(function (i, o) {

            $('a[href]', o).each(function (j, a) {
                if (a.href == window.location.href) {
                    $get('<%= accordion.ClientID %>').AccordionBehavior.set_SelectedIndex(i);
                    //$find('<%=accordion.ClientID %>').set_SelectedIndex(i);
                    $(a).addClass('selected');
                }

            });

        });
    });

</script>

<ajaxToolkit:Accordion ID="accordion" runat="server"
    FadeTransitions="false" FramesPerSecond="100" TransitionDuration="180" 
    CssClass="slidemenu" HeaderCssClass="header" ContentCssClass="content" 
    RequireOpenedPane="False" AutoSize="None" >
    <Panes>
        <ajaxToolkit:AccordionPane ID="accFind" runat="server" ContentCssClass="dn" Visible="false">
            <Header>
                <div>
                    <a href="" runat="server" ID="aFind">ГЛАВНАЯ</a>
                </div>
            </Header>
            <Content></Content>
        </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="accNewClaim" runat="server" ContentCssClass="dn" Visible="false">
            <Header>
                <div >
                    <asp:LinkButton runat="server" ID="lnkNewClaim" OnClick="lnkNewClaim_OnClick">НОВОЕ ЗАЯВЛЕНИЕ</asp:LinkButton>
                </div>
            </Header>
            <Content></Content>
        </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="accClaimList" runat="server" ContentCssClass="dn" Visible="false">
            <Header>
                <div>
                    <a href="" runat="server" ID="aClaimList">РЕЕСТР ЗАЯВЛЕНИЙ</a>
                </div>
            </Header>
            <Content></Content>
        </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="accUdodOsip" runat="server" ContentCssClass="dn" Visible="false">
            <Header>
                <div>
                    <a href="" runat="server" ID="audodOsip">УЧРЕЖДЕНИЕ</a>
                </div>
            </Header>
            <Content></Content>
        </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="accPupils" runat="server" ContentCssClass="dn" Visible="false">
            <Header>
                <div>
                    <span>ОБУЧАЮЩИЕСЯ</span>
                </div>
            </Header>
            <Content>
                <asp:Repeater runat="server" ID="rptPupils">
                    <HeaderTemplate>
                    <ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <li >   
                            <a href="<%# SiteUtility.GetUrl((string)Eval("Url")) %>" title="<%# Eval("Tooltip") %>"><%# Eval("Text") %> </a>
                            </li>
                    </ItemTemplate>
                    <FooterTemplate>
                    </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </Content>
        </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="accSchedule" runat="server" ContentCssClass="dn" Visible="false">
            <Header>
                <div>
                    <span>РАСПИСАНИЕ</span>
                </div>
            </Header>
            <Content>
                <asp:Repeater runat="server" ID="rptSchedule">
                    <HeaderTemplate>
                    <ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <li >   
                            <a href="<%# SiteUtility.GetUrl((string)Eval("Url")) %>" title="<%# Eval("Tooltip") %>"><%# Eval("Text") %> </a>
                            </li>
                    </ItemTemplate>
                    <FooterTemplate>
                    </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </Content>
        </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="accImportedPupils" runat="server" ContentCssClass="dn" Visible="false">
            <Header>
                <div>
                    <a href="" runat="server" ID="aImportedPupils">ЗАГРУЗКА 2 ГОДА</a>
                </div>
            </Header>
            <Content></Content>
        </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="accSchools" runat="server" ContentCssClass="dn" Visible="false">
            <Header>
                <div>
                    <a href="" runat="server" ID="aSchools">КОНТИНГЕНТ ОУ</a>
                </div>
            </Header>
            <Content></Content>
        </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="accUdod" runat="server" ContentCssClass="dn" Visible="false">
            <Header>
                <div>
                    <span>УЧРЕЖДЕНИЕ</span>
                </div>
            </Header>
            <Content>
                <asp:Repeater runat="server" ID="rptUdod">
                    <HeaderTemplate>
                    <ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <li >   
                            <a href="<%# SiteUtility.GetUrl((string)Eval("Url")) %>" title="<%# Eval("Tooltip") %>"><%# Eval("Text") %> </a>
                            </li>
                    </ItemTemplate>
                    <FooterTemplate>
                    </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </Content>
        </ajaxToolkit:AccordionPane>

        <ajaxToolkit:AccordionPane ID="accDict" runat="server" ContentCssClass="dn" Visible="false">
            <Header>
                <div>
                    <span>СПРАВОЧНИКИ</span>
                </div>
            </Header>
            <Content>
                <asp:Repeater runat="server" ID="repDict">
                    <HeaderTemplate>
                    <ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li >
                            <a href="<%# SiteUtility.GetUrl((string)Eval("Url")) %>" title="<%# Eval("Tooltip") %>"><%# Eval("Text") %> </a>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                    </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </Content>
        </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="accInfo" runat="server" ContentCssClass="dn" Visible="true">
            <Header>
                <div>
                    <span>ИНФОРМАЦИЯ</span>
                </div>
            </Header>
            <Content>
                <asp:Repeater runat="server" ID="repInfo">
                    <HeaderTemplate>
                    <ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li >
                            <a href="<%# SiteUtility.GetUrl((string)Eval("Url")) %>" title="<%# Eval("Tooltip") %>"><%# Eval("Text") %> </a>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                    </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </Content>
        </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="accUser" runat="server" ContentCssClass="dn" Visible="false">
            <Header>
                <div>
                    <span>АДМИНИСТРИРОВАНИЕ</span>
                </div>
            </Header>
            <Content>
                <asp:Repeater runat="server" ID="repUser">
                <HeaderTemplate>
                    <ul>
                </HeaderTemplate>
                <ItemTemplate>
                    <li>
                        <a href="<%# SiteUtility.GetUrl((string)Eval("Url")) %>" title="<%# Eval("Tooltip") %>"><%# Eval("Text") %> </a>
                    </li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
                </asp:Repeater>
            </Content>
        </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="accReport" runat="server" ContentCssClass="dn" Visible="false">
            <Header>
                <div>
                    <span>ОТЧЕТЫ</span>
                </div>
            </Header>
            <Content>
                <asp:Repeater runat="server" ID="repReport">
                <HeaderTemplate>
                    <ul>
                </HeaderTemplate>
                <ItemTemplate>
                    <li>
                        <a href="<%# SiteUtility.GetUrl((string)Eval("Url")) %>" title="<%# Eval("Tooltip") %>"><%# Eval("Text") %> </a>
                    </li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
                </asp:Repeater>
            </Content>
        </ajaxToolkit:AccordionPane>
        <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server" ContentCssClass="dn" >
            <Header>
                <div>
                    <asp:LinkButton runat="server" ID="lnkLogout" OnClick="lnkLogout_OnClick">ВЫХОД</asp:LinkButton>
                </div>
            </Header>
            <Content></Content>
        </ajaxToolkit:AccordionPane>
         
    </Panes>
</ajaxToolkit:Accordion>

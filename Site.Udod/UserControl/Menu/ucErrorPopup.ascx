﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucErrorPopup.ascx.cs" Inherits="UserControl_Menu_ucErrorPopup" %>

<asp:Panel runat="server" ID="errorPopup" style="display: none; padding: 5px; border: 2px solid black" BackColor="White">
    <asp:UpdatePanel ID="upError" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pHeaderError" runat="server" >
                <asp:Label ID="lblErrorHeader" Text="<span class='headerCaptions'>Ошибка</span>" runat="server"/>
                <asp:LinkButton ID="LinkButton3" runat="server" 
                    OnClientClick="$find('PopupError').hide(); return false;" />
            </asp:Panel>
            <asp:Label ID="lblErrorText" runat="server" />
            <table>
                <tr>
                    <td>
                        <asp:LinkButton runat="server" Text="<div class='btnBlue'>OK</div>" ID="lnkErrorOk" OnClick="lnkErrorOkOnClick"/>
                    </td>
                    <td>
                        <asp:LinkButton runat="server" Text="<div class='btnBlue'>Отмена</div>" ID="lnkErrorCancel" OnClick="lnkErrorCancelOnClick"/>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
    
<ajaxToolkit:ModalPopupExtender runat="server" ID="errorModalPopupExtender" 
    PopupControlID="errorPopup"
    TargetControlID="btnShowErrorModalPopup" RepositionMode="None" PopupDragHandleControlID="pHeaderError"
    CancelControlID="lnkErrorCancel"
    BehaviorID="PopupError"/>
    
<asp:Button ID="btnShowErrorModalPopup" runat="server" style="display:none" />
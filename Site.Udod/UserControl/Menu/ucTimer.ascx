﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucTimer.ascx.cs" Inherits="UserControl_Menu_ucTimer" %>

<!-- JavaScript includes -->
<script src="http://code.jquery.com/jquery-1.7.1.min.js" type="text/javascript"></script>
<script  type="text/javascript">
    (function ($) {

        // Number of seconds in every time division
        var days = 24 * 60 * 60,
		hours = 60 * 60,
		minutes = 60;
        var datetick;
        // Creating the plugin
        $.fn.countdown = function (prop) {

            var options = $.extend({
                callback: function () { },
                timestamp: 0
            }, prop);

            var left, d, h, m, s, positions;
            datetick = new Date().getTime();
            // Initialize the plugin
            init(this, options);

            positions = this.find('.position');

            (function tick() {

                // Time left
                left = Math.floor((options.timestamp/1000 + datetick/1000 - (new Date().getTime()/1000)));
                //alert(datetick);
                if (left < 0) {
                    left = 0;
                }

                // Number of days left
                d = Math.floor(left / days);
                updateDuo(0, 1, d);
                left -= d * days;

                // Number of hours left
                h = Math.floor(left / hours);
                updateDuo(2, 3, h);
                left -= h * hours;

                // Number of minutes left
                m = Math.floor(left / minutes);
                updateDuo(4, 5, m);
                left -= m * minutes;

                // Number of seconds left
                s = left;
                updateDuo(6, 7, s);

                // Calling an optional user supplied callback
                options.callback(d, h, m, s);

                // Scheduling another call of this function in 1s
                setTimeout(tick, 1000);
            })();

            // This function updates two digit positions at once
            function updateDuo(minor, major, value) {
                switchDigit(positions.eq(minor), Math.floor(value / 10) % 10);
                switchDigit(positions.eq(major), value % 10);
            }

            return this;
        };


        function init(elem, options) {
            elem.addClass('countdownHolder');

            // Creating the markup inside the container
            $.each(['Days', 'Hours', 'Minutes', 'Seconds'], function (i) {
                $('<span class="count' + this + '">').html(
				'<span class="position">\
					<span class="digit static">0</span>\
				</span>\
				<span class="position">\
					<span class="digit static">0</span>\
				</span>'
			).appendTo(elem);

                if (this != "Seconds") {
                    elem.append('<span class="countDiv countDiv' + i + '"></span>');
                }
            });

        }

        // Creates an animated transition between the two numbers
        function switchDigit(position, number) {

            var digit = position.find('.digit')

            if (digit.is(':animated')) {
                return false;
            }

            if (position.data('digit') == number) {
                // We are already showing this number
                return false;
            }

            position.data('digit', number);

            var replacement = $('<span>', {
                'class': 'digit',
                css: {
                    top: '-2.1em',
                    opacity: 0
                },
                html: number
            });

            // The .static class is added when the animation
            // completes. This makes it run smoother.

            digit
                .before(replacement)
                .removeClass('static')
                .animate({ top: '2.5em', opacity: 0 }, 'fast', function () {
                    digit.remove();
                });

            replacement
			.delay(100)
			.animate({ top: 0, opacity: 1 }, 'fast', function () {
			    replacement.addClass('static');
			});
        }
    })(jQuery);
</script>
<script type="text/javascript">
    $(function () {

        var note = $('#note'),
		startUpdate = <%= GetDeadline(0) %>,
        endUpdate = <%= GetDeadline(1) %>,
        ts;

        if (endUpdate < 0)
        {
            ts = endUpdate;
            note.hide();
        }
        else if (startUpdate < 0)
        {
            ts = endUpdate;
            if (window.location != '<%= SiteUtility.GetUrl("~/Maintenance.aspx") %>')
                window.location = '<%= SiteUtility.GetUrl("~/Maintenance.aspx") %>';
        }
        else
        {
            ts = startUpdate;
            //alert(ts);
            note.show();
        }

        $('#countdown').countdown({
            timestamp: ts,
            callback: function (days, hours, minutes, seconds) {

                /*if (endUpdate < new Date().getTime())
                {
                    alert("hide")
                    note.hide();
                    if (window.location == '<%= SiteUtility.GetUrl("~/Maintenance.aspx") %>')
                        window.location = '<%= SiteUtility.GetUrl("~/default.aspx") %>';
                }
                else if (startUpdate < new Date().getTime())
                {
                    if (window.location != '<%= SiteUtility.GetUrl("~/Maintenance.aspx") %>')
                        window.location = '<%= SiteUtility.GetUrl("~/Maintenance.aspx") %>';
                }
                else
                {
                    note.show();
                }*/
                //alert(ts);
                var message = "ВНИМАНИЕ! Система будет закрыта на обновление через <br />";
                if (ts != startUpdate) message = "До окончания обновления осталось приблизительно:<br />";
                message += hours + " : " + minutes + " : " + seconds + " <br />"; ;
                note.html(message);
            }
        });

    });
</script>

<p id="note"></p>

<asp:LinkButton ID="lnkEditTimer" runat="server" Text="<div class='btnBlue'>Изменить время</div>" OnClick="lnkEditTimer_OnClick"/>

<asp:Panel runat="server" ID="timerPopup" style="display: none; padding: 5px; border: 2px solid black" BackColor="White">
    <asp:UpdatePanel ID="upTimer" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pHeaderTimer" runat="server" >
                <asp:Label ID="lblTimerHeader" Text="<span class='headerCaptions'>Таймер</span>" runat="server"/>
                <asp:LinkButton ID="LinkButton3" runat="server" 
                    OnClientClick="$find('PopupTimer').hide(); return false;" />
            </asp:Panel>
            
            <table>
                <tr>
                    <td>
                        Время начала обновления:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="tbTimerStart" CssClass="inputXShort" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Время окончания обновления:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="tbTimerEnd" CssClass="inputXShort" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton runat="server" Text="<div class='btnBlue'>Обновить</div>" ID="lnkTimerOk" OnClick="lnkTimerOkOnClick"/>
                    </td>
                    <td>
                        <asp:LinkButton runat="server" Text="<div class='btnBlue'>Отмена</div>" ID="lnkTimerCancel" OnClick="lnkTimerCancelOnClick"/>
                    </td>
                </tr>
            </table>
            </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
    
<ajaxToolkit:ModalPopupExtender runat="server" ID="timerModalPopupExtender" 
    PopupControlID="timerPopup"
    TargetControlID="btnShowTimerModalPopup" RepositionMode="None" PopupDragHandleControlID="pHeaderTimer"
    CancelControlID="lnkTimerCancel"
    BehaviorID="PopupTimer"/>
    
<asp:Button ID="btnShowTimerModalPopup" runat="server" style="display:none" />
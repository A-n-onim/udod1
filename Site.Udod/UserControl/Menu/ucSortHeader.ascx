﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSortHeader.ascx.cs" Inherits="UserControl_Menu_ucSortHeader" %>

<asp:UpdatePanel ID="upSH" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <table id="mainTable" runat="server">
            <tr>
                <td style="background:Gray">
                    <asp:LinkButton runat="server" Font-Size="12pt" ForeColor="Black" ID="lnkHeaderCaption" Text="Header" OnClick="lnkHeaderCaption_OnClick" />
                </td>
                <td style="background:Gray">
                    <table>
                        <tr>
                            <td style="background:Gray">
                                <asp:LinkButton runat="server" ID="lnkDesc" OnClick="lnkDesc_OnClick"><div class="btnDesc"></div></asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td style="background:Gray">
                                <asp:LinkButton runat="server" ID="lnkAsc" OnClick="lnkAsc_OnClick"><div class="btnAsc"></div></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>

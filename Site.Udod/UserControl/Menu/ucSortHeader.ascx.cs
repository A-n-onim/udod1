﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public delegate void SortingEventHandler(object sender, SortHeaderEventArgs e);

public partial class UserControl_Menu_ucSortHeader : BasePageControl
{
    public event SortingEventHandler SortingChanged;

    public string HeaderCaption
    {
        get
        {
            return lnkHeaderCaption.Text;
        }
        set
        {
            lnkHeaderCaption.Text = value;
        }
    }

    public string SortExpression 
    {
        get
        {
            return (string)ViewState["SortExpression"];
        }
        set
        {
            ViewState["SortExpression"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lnkDesc.Visible = true;
            lnkAsc.Visible = true;
        }
    }

    public void Refresh(string sortExpression, string sortDirection)
    {
        if (sortExpression != this.SortExpression)
        {
            lnkAsc.Visible = true;
            lnkDesc.Visible = true;
        }
        else
        {
            if (sortDirection == "ASC")
            {
                lnkAsc.Visible = false;
                lnkDesc.Visible = true;
            }
            else
            {
                lnkAsc.Visible = true;
                lnkDesc.Visible = false;
            }
        }
        upSH.Update();
    }

    protected void lnkAsc_OnClick(object sender, EventArgs e)
    {
        AscClick();
    }

    protected void lnkDesc_OnClick(object sender, EventArgs e)
    {
        DescClick();
    }

    protected void lnkHeaderCaption_OnClick(object sender, EventArgs e)
    {
        if (lnkAsc.Visible)
        {
            AscClick();
        }
        else
        {
            DescClick();
        }
    }

    protected void DescClick()
    {
        lnkDesc.Visible = false;

        if (SortingChanged != null)
        {
            SortHeaderEventArgs args = new SortHeaderEventArgs();
            args.SortExpression = SortExpression;
            args.SortDirection = "DESC";
            SortingChanged(this, args);
        }
    }

    protected void AscClick()
    {
        lnkAsc.Visible = false;

        if (SortingChanged != null)
        {
            SortHeaderEventArgs args = new SortHeaderEventArgs();
            args.SortExpression = SortExpression;
            args.SortDirection = "ASC";
            SortingChanged(this, args);
        }
    }
}
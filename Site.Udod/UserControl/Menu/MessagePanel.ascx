﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MessagePanel.ascx.cs" Inherits="UserControl_Menu_MessagePanel" %>
<%@ Register TagPrefix="uct" TagName="errorPopup" Src="~/UserControl/Menu/ucErrorPopup.ascx" %>

<asp:UpdatePanel ID="upError" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <asp:Panel style="height: 100px; overflow-y: auto;" runat="server" ID="pMessages" Visible="false">
                <asp:GridView runat="server" ID="gvMessages" CssClass="dgcMessage" AutoGenerateColumns="false" DataKeyNames="MessageAdminId"  
                    OnSelectedIndexChanged="gvMessages_OnSelectedIndexChanged" >
                    <Columns>
                        <asp:TemplateField HeaderText="№">
                            <ItemTemplate><%# gvMessages.Rows.Count+1%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Дата сообщения">
                            <ItemTemplate>
                                <%# ((DateTime)Eval("CreatedDate")).ToShortDateString() + " " + ((DateTime)Eval("CreatedDate")).ToShortTimeString()%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Сообщение">
                            <ItemTemplate>
                                <%# Eval("Message")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Link" ShowSelectButton="true" HeaderText="" SelectText="<div class='btnBlue'>Прочитано</div>"/>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <table>
                <tr>
                    <td>
                    <asp:TextBox runat="server" ID="tbMessage" CssClass="inputLong"></asp:TextBox>
                    </td>
                    <td>
                    <asp:LinkButton runat="server" ID="lnkAddmessage" OnClick="lnkAddmessage_OnClick"><div class="btnBlue">Добавить сообщение</div></asp:LinkButton>
                    </td>
                </tr>
            </table>
            <uct:errorPopup ID="uErrorPopup" runat="server" OnErrorOkClick="uErrorPopup_OnErrorOkClick"></uct:errorPopup> 
            
        </ContentTemplate>
    </asp:UpdatePanel>

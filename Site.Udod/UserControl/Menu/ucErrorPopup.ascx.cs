﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_Menu_ucErrorPopup : BasePageControl
{
    public event EventHandler ErrorOkClick;

    public Unit Width {
        get { return errorPopup.Width; }
        set { errorPopup.Width = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void ShowError(string headerText, string errorText, string okText, string cancelText, bool okVisible, bool cancelVisible)
    {
        lblErrorHeader.Text = "<span class='headerCaptions'>" + headerText + "</span>";
        lblErrorText.Text = errorText;
        lnkErrorOk.Text = "<div class='btnBlue'>" + okText + "</div>";
        lnkErrorCancel.Text = "<div class='btnBlue'>" + cancelText + "</div>";

        lnkErrorOk.Visible = okVisible;
        lnkErrorCancel.Visible = cancelVisible;

        errorModalPopupExtender.X = 300;
        errorModalPopupExtender.Y = 300;
        errorModalPopupExtender.Show();
        upError.Update();
    }

    public void HideError()
    {
        errorModalPopupExtender.Hide();
        upError.Update();
    }

    protected void lnkErrorOkOnClick(object sender, EventArgs e)
    {
        if (ErrorOkClick != null) ErrorOkClick(this, new EventArgs());
    }

    protected void lnkErrorCancelOnClick(object sender, EventArgs e)
    {
        HideError();
    }

    
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucParent.ascx.cs" Inherits="UserControl_Pupil_Parent" %>
<%@ Register TagPrefix="uct" TagName="FIO" Src="~/UserControl/User/ucFIO.ascx" %>
<%@ Register TagPrefix="uct" TagName="Document" Src="~/UserControl/User/ucDocument.ascx" %>

<script type="text/javascript">
    var eng = new Array("Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "{", "}", "A", "S", "D", "F", "G", "H", "J", "K", "L", ":", "\"", "Z", "X", "C", "V", "B", "N", "M", "<", ">", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "\'", "z", "x", "c", "v", "b", "n", "m", ",", ".", "@", ".");
    var rus = new Array("\u0419", "\u0426", "\u0423", "\u041A", "\u0415", "\u041D", "\u0413", "\u0428", "\u0429", "\u0417", "\u0425", "\u042A", "\u0424", "\u042B", "\u0412", "\u0410", "\u041F", "\u0420", "\u041E", "\u041B", "\u0414", "\u0416", "\u042D", "\u042F", "\u0427", "\u0421", "\u041C", "\u0418", "\u0422", "\u042C", "\u0411", "\u042E", "\u0439", "\u0446", "\u0443", "\u043A", "\u0435", "\u043D", "\u0433", "\u0448", "\u0449", "\u0437", "\u0445", "\u044A", "\u0444", "\u044B", "\u0432", "\u0430", "\u043F", "\u0440", "\u043E", "\u043B", "\u0434", "\u0436", "\u044D", "\u044F", "\u0447", "\u0441", "\u043C", "\u0438", "\u0442", "\u044C", "\u0431", "\u044E", '"', ".");

    $(document).ready(function() {
        var tbEmail = '#' + '<%= tbEmail.ClientID %>';
        $(tbEmail).bind("keyup", ChangeToEg);
    });
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(bindemail);

    function bindemail() {
        var tbEmail = '#' + '<%= tbEmail.ClientID %>';
        $(tbEmail).bind("keyup", ChangeToEg);
    }
    
    function ChangeToEg(event) {
        if (event.keyCode > 46) {
            var str = $(this).val();
            str = getRus2EngString(str);
            $(this).val(str);
        }
    }
    
    function getRus2EngString(target) {
        var res = "";
        for (var j = 0; j < target.length; j++) {
            var test = target.charAt(j);
            for (var i = 0; i < eng.length; i++) {
                if (test == rus[i]) if (test != "\'") test = eng[i];
            }
            res += test;
        }
        return res;
    }
</script>

<table border=0>
    <tr>
        <td >
            <uct:FIO runat="server" ID="ucFio"/>
        </td>
    </tr>
    <tr>
        <td>
            <table border=0>
                <tr>
                    <td class="tdx">
                        Заявитель:
                    </td>
                    <td >
                        <asp:RadioButtonList runat="server" ID="cbRelative" RepeatDirection="Horizontal" DataTextField="UserTypeName" DataValueField="UserTypeId"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <uct:Document runat="server" ID="ucPassport"/>
        </td>
    </tr>
    <tr>
        <td>
            <table border=0>
                <tr>
                    <td class="tdx" style="width: 90px; ">
                        Телефон 1:
                    </td>
                    <td style="vertical-align:top;">
                        <div class="inputShort"><asp:TextBox ID="tbPhoneNumber" runat="server" CssClass="inputShort" /></div>
                        <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbPhoneNumber" Mask="8(999)999-99-99" MaskType="Number" />
                    </td>
                    <td class="tdx" style="width: 90px; ">
                        Телефон 2:
                    </td>
                    <td style="vertical-align:top;">
                        <div class="inputShort"><asp:TextBox ID="tbExpressPhoneNumber" runat="server" CssClass="inputShort" /></div>
                        <ajaxToolkit:MaskedEditExtender ID="mee2" runat="server" TargetControlID="tbExpressPhoneNumber" Mask="8(999)999-99-99" MaskType="Number" />
                    </td>
                    <td class="tdx" style="width: 90px; ">
                        Эл. почта:
                    </td>
                    <td>
                        <div class="inputShort"><asp:TextBox ID="tbEmail" runat="server" CssClass="inputShort" /></div>
                    </td>
                </tr>
                <tr>
                    <td colspan=2 align="right">
                    </td>
                    <td colspan=2 align="right">
                        <%-- <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="val1" ErrorMessage="Поле 'Эл. почта' не заполнено" ControlToValidate="tbEmail" SetFocusOnError="true" ValidationGroup="71"></asp:RequiredFieldValidator>--%>
                        <asp:RegularExpressionValidator SetFocusOnError="true" ID="RegularExpressionValidator1" ErrorMessage="Некорректный адрес электронной почты"
                            Display="Dynamic" ValidationGroup="71" ControlToValidate="tbEmail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                    </td>
                </tr>
            </table>
            
        </td>
    </tr>
</table>

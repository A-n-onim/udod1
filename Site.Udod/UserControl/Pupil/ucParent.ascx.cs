﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class UserControl_Pupil_Parent : BasePageControl
{
    public string LastName { get { return ucFio.LastName; } set { ucFio.LastName = value; } }
    public string FirstName { get { return ucFio.FirstName; } set { ucFio.FirstName = value; } }
    public string MiddleName { get { return ucFio.MiddleName; } set { ucFio.MiddleName = value; } }
    public string PhoneNumber { get { return tbPhoneNumber.Text; } set { tbPhoneNumber.Text = value; } }
    public string ExpressPhoneNumber { get { return tbExpressPhoneNumber.Text; } set { tbExpressPhoneNumber.Text = value; } }
    public string EMail { get { return tbEmail.Text; } set { tbEmail.Text = value; } }
    //public bool IsPupil { get { return cbIsPupil.Checked; } set { cbIsPupil.Checked = value; } }
    public bool RelativeEnabled { set { cbRelative.Enabled = value; } }
    public bool reestrValidatorsEnabled { set { ucFio.reestrValidatorsEnabled = value; ucPassport.reestrValidatorsEnabled = value; } }

    public int Relativies 
    {
        get 
        { 
            return Convert.ToInt32(cbRelative.SelectedValue);
        } 
        set 
        { 
            cbRelative.SelectedIndex = cbRelative.Items.IndexOf(cbRelative.Items.FindByValue(value.ToString()));
        } 
    }

    public int PassportType { get { return ucPassport.PassportType; } set { ucPassport.PassportType = value;  } }
    public string Series { get { return ucPassport.Series; } set { ucPassport.Series = value; } }
    public string Number { get { return ucPassport.Number; } set { ucPassport.Number = value; } }
    public DateTime? IssueDate { get { return ucPassport.IssueDate; } set { ucPassport.IssueDate = value; } }
    public bool FIOEnabled { set { ucFio.Enabled = value; } }
    public bool Enabled { set { ucFio.Enabled = ucPassport.Enabled = tbPhoneNumber.Enabled = tbExpressPhoneNumber.Enabled = tbEmail.Enabled = cbRelative.Enabled = value; } }

    public bool IsReestr
    {
        set
        {
            if (value)
            {
                ucFio.Enabled = ucPassport.Enabled = false;
            }
            else
            {
                ucPassport.Enabled = false;
                ucFio.Enabled = false;
                ucFio.EnabledMiddleName = true;
            }
        }
    }

    public event EventHandler Change;

    protected void Page_Init(object sender, EventArgs e)
    {
        using (UserDb db = new UserDb())
        {
            cbRelative.DataSource = db.GetUserTypes();
            cbRelative.DataBind();
            cbRelative.SelectedIndex = 0;
        }
    }

    public void Clear()
    {
        ucFio.Clear();
        ucPassport.Clear();
        cbRelative.SelectedIndex = 0;
        tbPhoneNumber.Text = "";
        tbExpressPhoneNumber.Text = "";
        tbEmail.Text = "";
        //cbIsPupil.Checked = false;
    }

    //protected void cbIsPupil_OnCheckedChanged(object sender, EventArgs e)
    //{
    //    ucFio.Enabled = !cbIsPupil.Checked;
    //}
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucPupilEdit.ascx.cs" Inherits="UserControl_Pupil_PupilEdit" %>
<%@ Register TagPrefix="uct" TagName="FIO" Src="~/UserControl/User/ucFIO.ascx" %>
<%@ Register TagPrefix="uct" TagName="Document" Src="../User/ucDocument.ascx" %>

<script type="text/javascript">
    var eng = new Array("Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "{", "}", "A", "S", "D", "F", "G", "H", "J", "K", "L", ":", "\"", "Z", "X", "C", "V", "B", "N", "M", "<", ">", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "\'", "z", "x", "c", "v", "b", "n", "m", ",", ".", "@",".");
    var rus = new Array("\u0419", "\u0426", "\u0423", "\u041A", "\u0415", "\u041D", "\u0413", "\u0428", "\u0429", "\u0417", "\u0425", "\u042A", "\u0424", "\u042B", "\u0412", "\u0410", "\u041F", "\u0420", "\u041E", "\u041B", "\u0414", "\u0416", "\u042D", "\u042F", "\u0427", "\u0421", "\u041C", "\u0418", "\u0422", "\u042C", "\u0411", "\u042E", "\u0439", "\u0446", "\u0443", "\u043A", "\u0435", "\u043D", "\u0433", "\u0448", "\u0449", "\u0437", "\u0445", "\u044A", "\u0444", "\u044B", "\u0432", "\u0430", "\u043F", "\u0440", "\u043E", "\u043B", "\u0434", "\u0436", "\u044D", "\u044F", "\u0447", "\u0441", "\u043C", "\u0438", "\u0442", "\u044C", "\u0431", "\u044E", '"', ".");

    function bindtextbox(sender, args) {
        var tbSchool = '#' + '<%= tbPupilSchool.ClientID %>';
        $(tbSchool).bind("keyup", changeRegistr);
        
    }
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(bindtextbox);

    function changeRegistr(event) {
        if (event.keyCode > 46) {
            var str = $(this).val();
            if (str.length > 0) {
                str = getEng2RusString(str);
                $(this).val(str);
            }
        }
    }

    function getEng2RusString(target) {
        var res = "";
        for (var j = 0; j < target.length; j++) {
            var test = target.charAt(j);
            for (var i = 0; i < rus.length; i++) {
                if (test == eng[i]) if (test != "\'") test = rus[i];
            }
            res += test;
        }
        return res;
    }
</script>

<table border=0>
    <tr>
        <td colspan=8>
            <uct:FIO runat="server" ID="ucFio"></uct:FIO>
        </td>
    </tr>
    <tr>
        <td colspan=8>
            <uct:Document runat="server" ID="ucPassport"/>
        </td>
    </tr>
    <tr>
        <td class="tdx"  style="vertical-align:top;">
            Дата рождения*:
        </td>
        <td>
            <div class="inputShort"  style="vertical-align:top;">
                <asp:TextBox runat="server" ID="tbBirthday" CssClass="inputShort" />
            </div>
            <ajaxToolkit:CalendarExtender runat="server" ID="calendar" TargetControlID="tbBirthday" Animated="false" Format="dd.MM.yyyy"/>
            <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbBirthday" Mask="99/99/9999" MaskType="Date" />
        </td>
        <td class="tdx">
            Пол:
        </td>
        <td style="width:132px">
            <asp:RadioButtonList runat="server" ID="cbSex" RepeatDirection="Horizontal">
                <Items>
                    <asp:ListItem Selected="true" Value="0">М</asp:ListItem>
                    <asp:ListItem Value="1">Ж</asp:ListItem>
                </Items>
            </asp:RadioButtonList>
        </td>
        <td class="tdx">
            Школа:
        </td>
        <td>
            <asp:TextBox runat="server" ID="tbPupilSchool" CssClass="input"></asp:TextBox>
            <ajaxToolkit:AutoCompleteExtender runat="server" ID="acSchool" TargetControlID="tbPupilSchool" ServiceMethod="GetSchool" EnableCaching="true" CompletionSetCount="50" MinimumPrefixLength="1" UseContextKey="true" BehaviorID="AutoCompleteEx" CompletionListCssClass="addresslist"/>
        </td>
        <td class="tdx">
            Класс:
        </td>
        <td>
            <asp:TextBox runat="server" ID="tbPupilClass" CssClass="input"></asp:TextBox>
            <asp:HiddenField runat="server" ID="hfEkisCode"/>
            <asp:HiddenField runat="server" ID="hfReestrGuid"/>
        </td>
    </tr>
    <tr>
        <td colspan=2>
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="RequiredFieldValidator2" ErrorMessage="Поле 'Дата рождения' не заполнено" ControlToValidate="tbBirthday" SetFocusOnError="true" ValidationGroup="71"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="valrDate" runat="server" ControlToValidate="tbBirthday" MinimumValue="01.08.1812" MaximumValue="01.08.3012" Type="Date" text="Возраст обучающегося должен быть<br>в пределах от 3 лет до 21 года<br>" Display="Dynamic" ValidationGroup="71"/>
            <asp:RangeValidator ID="valrDate2" runat="server" ControlToValidate="tbBirthday" MinimumValue="01.08.1812" MaximumValue="01.08.3012" Type="Date" text="Дата рождения не может быть<br>позднее Сегодня" Display="Dynamic" ValidationGroup="71"/>
            <asp:RangeValidator ID="valrDate3" runat="server" ControlToValidate="tbBirthday" MinimumValue="01.08.1812" MaximumValue="01.08.3012" Type="Date" text="Заявителю должно быть больше 14 лет" Display="Dynamic" ValidationGroup="71"/>
        </td>
    </tr>
</table>

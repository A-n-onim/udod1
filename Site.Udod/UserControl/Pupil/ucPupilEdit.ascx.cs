﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_Pupil_PupilEdit : BasePageControl
{
    public bool Enable14YearsValidation { set { valrDate3.Enabled = value; } }

    public string LastName { get { return ucFio.LastName; } set { ucFio.LastName = value; } }
    public string FirstName { get { return ucFio.FirstName; } set { ucFio.FirstName = value; } }
    public string MiddleName { get { return ucFio.MiddleName; } set { ucFio.MiddleName = value; } }
    public string SchoolName { get { return tbPupilSchool.Text; } set { tbPupilSchool.Text = value; } }
    public string ClassName { get { return tbPupilClass.Text; } set { tbPupilClass.Text = value; } }
    public string CodeEkis { get { return hfEkisCode.Value; } set { hfEkisCode.Value = value; } }
    public string ReestrGuid { get { return hfReestrGuid.Value; } set { hfReestrGuid.Value = value; } }

    public DateTime BirthDay { get { if (string.IsNullOrEmpty(tbBirthday.Text)) return DateTime.Now; return Convert.ToDateTime(tbBirthday.Text); } set { tbBirthday.Text = value.ToShortDateString(); } }
    public int Sex { get { return Convert.ToInt32(cbSex.SelectedValue); } set { cbSex.SelectedIndex = cbSex.Items.IndexOf(cbSex.Items.FindByValue(value.ToString())); } }
    public bool FioEnabled { get { return ucFio.Enabled; } set { ucFio.Enabled = value; } }
    public bool PassportEnabled { set { ucPassport.Enabled = value; } }
    public bool Enabled { set { ucFio.Enabled = ucPassport.Enabled = tbBirthday.Enabled = cbSex.Enabled = tbPupilSchool.Enabled = tbPupilClass.Enabled = value; } get { return tbBirthday.Enabled; } }

    public int PassportType { get { return ucPassport.PassportType; } set { ucPassport.PassportType = value; } }
    public string Series { get { return ucPassport.Series; } set { ucPassport.Series = value; } }
    public string Number { get { return ucPassport.Number; } set { ucPassport.Number = value; } }
    public DateTime? IssueDate { get { return ucPassport.IssueDate; } set { ucPassport.IssueDate = value; } }

    public bool SetDefault { set { tbBirthday.Enabled = cbSex.Enabled = tbPupilSchool.Enabled = tbPupilClass.Enabled = !value; } }

    public bool IsReestr { 
        get { return !tbBirthday.Enabled; } 
        set 
        { 
            if (value)
            {
                tbBirthday.Enabled = ucFio.Enabled = ucPassport.Enabled = 
                    cbSex.Enabled = tbPupilSchool.Enabled = false;
            }
            else
            {
                ucPassport.Enabled = false;
                ucFio.Enabled = false;
                ucFio.EnabledMiddleName = true;
                tbPupilClass.Enabled = tbPupilSchool.Enabled = tbBirthday.Enabled =
                cbSex.Enabled = true;
            }
        } 
    }

    public bool reestrValidatorsEnabled { set { ucFio.reestrValidatorsEnabled = value; ucPassport.reestrValidatorsEnabled = value; } }


    public void Clear()
    {
        ucFio.Clear();
        ucPassport.Clear();
        tbBirthday.Text = "";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Enable14YearsValidation = false;
        if (!Page.IsPostBack)
        {
            //tbPupilClass.Enabled = tbPupilSchool.Enabled=tbBirthday.Enabled = 
            //cbSex.Enabled = false;
            
        }

        valrDate.MinimumValue = DateTime.Today.AddYears(-22).ToString().Substring(0, 10);
        valrDate.MaximumValue = DateTime.Today.AddYears(-3).ToString().Substring(0, 10);

        valrDate2.MaximumValue = DateTime.Today.ToString().Substring(0, 10);

        valrDate3.MaximumValue = DateTime.Today.AddYears(-14).ToString().Substring(0, 10);
        acSchool.ServicePath = SiteUtility.GetUrl("~/Webservices/Dict.asmx");
    }

}
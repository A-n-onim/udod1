﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucFindFilter.ascx.cs" Inherits="UserControl_Filter_ucFindFilter" %>

<table border=0>
    <tr>
        <td>
            Название
        </td>
        <td>
            Направленность
        </td>
        <td>
            Профиль
        </td>
        <td>
            Основной вид деятельности
        </td>
        <td>
            <asp:Label ID="lblBudgetType" runat="server">Вид услуги</asp:Label>
        </td>
    </tr>
    <tr>
        <%--<td >
            <div class="inputShort">
            <asp:DropDownList runat="server" DataSourceID="dsProgram" ID="ddlProgram" DataTextField="Name" DataValueField="ProgramId" AutoPostBack="true" OnSelectedIndexChanged="ddlProgram_OnSelectedIndexChanged" CssClass="inputShort" OnDataBound="ddlProgram_OnDataBound"/>
            </div>
        </td>
        <td >
            <div class="inputShort">
            <asp:DropDownList runat="server" DataSourceID="dsProfile" ID="ddlProfile" DataTextField="Name" DataValueField="ProfileId" AutoPostBack="true" OnSelectedIndexChanged="ddlProfile_OnSelectedIndexChanged" CssClass="inputShort" OnDataBound="ddlProfile_OnDataBound"/>
            </div>
        </td>
         <td>
            <div class="inputShort">
            <asp:DropDownList runat="server" ID="ddlSection" DataSourceID="dsSection" DataTextField="Name" DataValueField="Name" AutoPostBack="true" OnSelectedIndexChanged="ddlSection_OnSelectedIndexChanged" CssClass="inputShort" OnDataBound="ddlSection_OnDataBound"/>
            </div>
        </td>--%>
        <td>
            <asp:TextBox ID="tbFindByName" runat="server" CssClass="inputShort" OnTextChanged="tbFindByName_OnTextChanged" AutoPostBack="true"/>
        </td>
        <td style="width:150px">
            <div style="margin-bottom:10px">
                <ajaxToolkit:ComboBox runat="server" ID="cbxProgram" 
                    DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend" CaseSensitive="false" ItemInsertLocation="Append"
                    AutoPostBack="true" OnSelectedIndexChanged="cbxProgram_OnSelectedIndexChanged"
                    DataTextField="Name" DataValueField="ProgramId" OnDataBound="cbxProgram_OnDataBound"/>
            </div>
        </td>
        <td style="width:150px; ">
            <div style="margin-bottom:10px">
                <ajaxToolkit:ComboBox runat="server" ID="cbxProfile"
                    DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend" CaseSensitive="false" ItemInsertLocation="Append"
                    AutoPostBack="true" OnSelectedIndexChanged="cbxProfile_OnSelectedIndexChanged" 
                    DataTextField="Name" DataValueField="ProfileId" OnDataBound="cbxProfile_OnDataBound"/>
            </div>
        </td>
        <td style="width:150px">
            <div style="margin-bottom:10px">
                <ajaxToolkit:ComboBox runat="server" ID="cbxSection"
                    DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend" CaseSensitive="false" ItemInsertLocation="Append"
                    AutoPostBack="true" OnSelectedIndexChanged="cbxSection_OnSelectedIndexChanged" 
                    DataTextField="Name" DataValueField="SectionId" OnDataBound="cbxSection_OnDataBound"/>
            </div>
        </td>
        <td>
            <asp:DropDownList runat="server" ID="ddlTypeBudget" DataSourceID="dsTypebudget" DataTextField="TypeBudgetName" DataValueField="DictTypeBudgetId" AutoPostBack="true" OnSelectedIndexChanged="ddlTypeBudget_OnSelectedIndexChanged" CssClass="inputShort" OnDataBound="ddlTypeBudget_OnDataBound"/>
        </td>
    </tr>
</table>

<asp:ObjectDataSource runat="server" ID="dsTypebudget" TypeName="Udod.Dal.DictBudgetTypeDb" SelectMethod="GetBudgetTypes">
</asp:ObjectDataSource>
<%-- <asp:ObjectDataSource runat="server" ID="dsProgram" TypeName="Udod.Dal.DictProgramDb" SelectMethod="GetPrograms">
</asp:ObjectDataSource>

<asp:ObjectDataSource runat="server" ID="dsProfile" TypeName="Udod.Dal.DictProfileDb" SelectMethod="GetProfiles">
<SelectParameters>
    <asp:ControlParameter Name="programId" ControlID="ddlProgram" PropertyName="Selectedvalue" ConvertEmptyStringToNull="true"/>
</SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource runat="server" ID="dsSection" TypeName="Udod.Dal.DictSectionDb" SelectMethod="GetSectionsFind">
<SelectParameters>
    <asp:ControlParameter Name="profileId" ControlID="ddlProfile" PropertyName="SelectedValue" ConvertEmptyStringToNull="true"/>
    <asp:Parameter Name="udodId"/>
    <asp:ControlParameter ControlID="ddlProgram" PropertyName="SelectedValue" Name="programId" ConvertEmptyStringToNull="true"/>
</SelectParameters>
</asp:ObjectDataSource>--%>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSectionFilter.ascx.cs" Inherits="UserControl_Filter_ucSectionFilter" %>
<table>
    <tr>
        <td>
            <span>Направленность:</span>
            </td><td>
            <span>Профиль:</span>
        </td>
    </tr>
    <tr>
        <td><div class="input">
            <asp:DropDownList runat="server" ID="ddlProgram" DataTextField="Name" DataValueField="ProgramId" AutoPostBack="true" OnSelectedIndexChanged="ProgramSelectedIndex_Change" CssClass="input" />
            </div>
            </td><td>
            <div class="input">
            <asp:DropDownList runat="server" ID="ddlProfile" DataTextField="Name" DataValueField="ProfileId" AutoPostBack="true" OnSelectedIndexChanged="ProfileSelectedIndex_Change" CssClass="input" />
            </div>
        </td>
    </tr>

</table>

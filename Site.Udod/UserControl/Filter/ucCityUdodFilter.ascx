﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCityUdodFilter.ascx.cs" Inherits="UserControl_ucCityUdodFilter" %>
<table border=0>
    <tr>
        <td>
            <asp:Label runat="server" ID="lblAdmOkr" >Административный округ:</asp:Label>
        </td>
        <td>
            <div class="inputShort" runat="server" ID="ddlCityDiv"><asp:DropDownList runat="server" ID="ddlCity" AutoPostBack="true" 
                DataValueField="CityId" DataTextField="Name" 
                onselectedindexchanged="ddlCity_SelectedIndexChanged"
                CssClass="input" />
            </div>
        </td>
        <td>
            <asp:Label runat="server" ID="lblUd" >УДО:</asp:Label>
        </td>
        <td >
            <div class="inputShort" runat="server" ID="ddlUdodDiv">
                <asp:DropDownList runat="server" ID="ddlUdods" AutoPostBack="true" DataValueField="UdodId" DataTextField="ShortName"
                    onselectedindexchanged="ddlUdods_SelectedIndexChanged" CssClass="input" />
            </div>
        </td>
    </tr>
</table>
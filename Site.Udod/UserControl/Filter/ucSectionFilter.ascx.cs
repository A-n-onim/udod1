﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class UserControl_Filter_ucSectionFilter : BasePageControl
{
    public event EventHandler ProfileChange;
    public event EventHandler ProgramChange;

    public int? SelectedProfileValue
    {
        get
        {
            if (string.IsNullOrEmpty(ddlProfile.SelectedValue)) return null;
            return Convert.ToInt32(ddlProfile.SelectedValue);
        }
    }

    public int? SelectedProgramValue
    {
        get
        {
            if (string.IsNullOrEmpty(ddlProgram.SelectedValue)) return null;
            return Convert.ToInt32(ddlProgram.SelectedValue);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ProgramDatabind();
            ProfileDatabind();
        }
    }

    public void Refresh()
    {
        ProgramDatabind();
        ProfileDatabind();
    }

    protected void ProgramDatabind()
    {
        List<ExtendedProgram> list;
        using (DictProgramDb db = new DictProgramDb())
        {
            list = db.GetPrograms(UserContext.UdodId);
            ddlProgram.DataSource = list;
            ddlProgram.DataBind();
            ddlProgram.Items.Insert(0, new ListItem("Все", ""));
        }
    }

    protected void ProfileDatabind()
    {
        List<ExtendedProfile> list;
        using (DictProfileDb db = new DictProfileDb())
        {
            if (!string.IsNullOrEmpty(ddlProgram.SelectedValue))
                list = db.GetProfiles(Convert.ToInt32(ddlProgram.SelectedValue), UserContext.UdodId);
            else
                list = db.GetProfiles(null, UserContext.UdodId);
            ddlProfile.DataSource = list;
            ddlProfile.DataBind();
            ddlProfile.Items.Insert(0, new ListItem("Все", ""));
        }
    }

    protected void ProgramSelectedIndex_Change(object sender, EventArgs e)
    {
        ProfileDatabind();

        if (ProgramChange != null) ProgramChange(this, new EventArgs());
    }

    protected void ProfileSelectedIndex_Change(object sender, EventArgs e)
    {
        if (ProfileChange != null) ProfileChange(sender, new EventArgs());
    }
}
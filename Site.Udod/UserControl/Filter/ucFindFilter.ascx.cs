﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class UserControl_Filter_ucFindFilter : BasePageControl
{
    public event EventHandler Change;
    public bool BudgetTypeVisible { set { lblBudgetType.Visible = value; ddlTypeBudget.Visible = value; } }
    //public int? ProgramId { get { if (string.IsNullOrEmpty(ddlProgram.SelectedValue)) return null; return Convert.ToInt32(ddlProgram.SelectedValue); } }
    //public int? ProfileId { get { if (string.IsNullOrEmpty(ddlProfile.SelectedValue)) return null; return Convert.ToInt32(ddlProfile.SelectedValue); } }
    //public int? SectionId { get { if (string.IsNullOrEmpty(ddlSection.SelectedValue)) return null; return Convert.ToInt32(ddlSection.SelectedValue); } }
    public string ProgramName { get { if (cbxProgram.SelectedItem != null) return cbxProgram.SelectedItem.Text; else return ""; } }
    public string ProfileName { get { if (cbxProfile.SelectedItem != null) return cbxProfile.SelectedItem.Text; else return ""; } }
    public string SectionName { get { if (cbxSection.SelectedItem != null) return cbxSection.SelectedItem.Text; else return ""; } }
    public string ChildUnionName { get { return tbFindByName.Text; } }
    public int? TypeBudgetId { get { if (string.IsNullOrEmpty(ddlTypeBudget.SelectedValue)) return null; return Convert.ToInt32(ddlTypeBudget.SelectedValue); } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ProgramDatabind();
            ProfileDatabind();
            SectionDatabind();
        }
    }

    public void Clear()
    {
        tbFindByName.Text = "";
        cbxProgram.SelectedIndex = 0;
        cbxProfile.SelectedIndex = 0;
        cbxSection.SelectedIndex = 0;
    }

    protected void ddlTypeBudget_OnDataBound(object sender, EventArgs e)
    {
        ddlTypeBudget.Items.Insert(0, new ListItem("Все", ""));
    }

    protected void ddlTypeBudget_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (Change != null) Change(this, new EventArgs());
    }

    protected void tbFindByName_OnTextChanged(object sender, EventArgs e)
    {
        //if (Change != null) Change(this, new EventArgs());
    }

    protected void cbxProgram_OnDataBound(object sender, EventArgs e)
    {
        cbxProgram.Items.Insert(0, new ListItem("", ""));
    }

    protected void cbxProfile_OnDataBound(object sender, EventArgs e)
    {
        cbxProfile.Items.Insert(0, new ListItem("", ""));
    }

    protected void cbxSection_OnDataBound(object sender, EventArgs e)
    {
        cbxSection.Items.Insert(0, new ListItem("", ""));
    }

    protected void cbxProgram_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ProfileDatabind();
        SectionDatabind();
        //if (Change != null) Change(this, new EventArgs());
    }

    protected void cbxProfile_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        SectionDatabind();
        //if (Change != null) Change(this, new EventArgs());
    }

    protected void cbxSection_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        //if (Change != null) Change(this, new EventArgs());
    }

    protected void ProgramDatabind()
    {
        using (DictProgramDb db = new DictProgramDb())
        {
            cbxProgram.DataSource = db.GetPrograms(null);
            cbxProgram.DataBind();
        }
    }

    protected void ProfileDatabind()
    {
        using (DictProfileDb db = new DictProfileDb())
        {
            if (!string.IsNullOrEmpty(cbxProgram.Text))
                cbxProfile.DataSource = db.GetProfiles(Convert.ToInt32(cbxProgram.Text), null);
            else
                cbxProfile.DataSource = db.GetProfiles(null, null);
            cbxProfile.DataBind();
        }
    }

    protected void SectionDatabind()
    {
        using (DictSectionDb db = new DictSectionDb())
        {
            if (!string.IsNullOrEmpty(cbxProfile.Text))
                cbxSection.DataSource = db.GetSections(Convert.ToInt32(cbxProfile.Text), null, null, null);
            else if (!string.IsNullOrEmpty(cbxProgram.Text))
                cbxSection.DataSource = db.GetSections(null, Convert.ToInt32(cbxProgram.Text), null, null);
            else
                cbxSection.DataSource = db.GetSections(null, null, null,null);

            cbxSection.DataBind();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;

public partial class UserControl_Filter_ucProfileFilter : BasePageControl
{
    public event EventHandler Changed;

    public int? SelectedValue
    {
        get
        {
            if (string.IsNullOrEmpty(ddlProgram.SelectedValue)) return null;
            return Convert.ToInt32(ddlProgram.SelectedValue);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
            using (DictProgramDb db = new DictProgramDb())
            {
                List<ExtendedProgram> list = db.GetPrograms(null);
                ddlProgram.DataSource = list;
                ddlProgram.DataBind();
                ddlProgram.Items.Insert(0, new ListItem("Все", ""));
            }
        }
    }

    protected void SelectedIndex_Change(object sender, EventArgs e)
    {
        if (Changed != null)
            Changed(sender, new EventArgs());
    }
}
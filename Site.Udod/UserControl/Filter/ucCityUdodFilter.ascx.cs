﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class UserControl_ucCityUdodFilter : BasePageControl
{
    public bool UdodDdlEnabled { get; set; }
    public bool AddAllProperty { get; set; }
    public bool RemoveAllCityProperty { get; set; }

    public event EventHandler loadComplete;
    public event EventHandler UdodChange;
    public event EventHandler CityChange;

    public long? SelectedUdod
    {
        get
        {
            return UserContext.UdodId;
        }
        set
        {
            //UserContext.UdodId = value;
            ddlUdods.SelectedIndex = ddlUdods.Items.IndexOf(ddlUdods.Items.FindByValue(value.ToString()));
            // вызываем события что УДО был изменен
            PageEvents.UdodChangedEvent();
            if (UdodChange != null) UdodChange(this, new EventArgs());
        }
    }

    public int? SelectedCity
    {
        get
        {
            return UserContext.CityId;
        }
        set
        {
            //UserContext.CityId = value;
            ddlCity.SelectedIndex = ddlCity.Items.IndexOf(ddlCity.Items.FindByValue(value.ToString()));
            // вызываем события что сити был изменен
            PageEvents.CityChangedEvent();
            if (CityChange != null) CityChange(this, new EventArgs());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ddlUdods.Visible = lblUd.Visible = UdodDdlEnabled;
            if (!UdodDdlEnabled) UserContext.UdodId = null;   

            // проверка на роль пользователя и изменение контролов
            if (UserContext.Roles.RoleId == (int)EnumRoles.UdodEmployee)
            {
                // Сотрудник УДО - ddl убраны, берем только один УДО, Сити не важен
                ddlUdodDiv.Visible = ddlCityDiv.Visible = lblAdmOkr.Visible = lblUd.Visible = false;
                //lblUdod.Visible = true;

                //using (UdodDb db = new UdodDb())
                //{
                //    var Udod = db.GetUdod(UserContext.UdodId.Value);
                //    lblUdod.Text = Udod.Name;
                //}
            }
            else if (UserContext.Roles.RoleId == (int)EnumRoles.Osip)
            {
                // сотрудник ОСИП - загружаются оба ddl
                ddlCityDiv.Visible = ddlUdodDiv.Visible = true;
                //lblUdod.Visible = false;

                // загружаем Округа
                CityDatabind();
                // загружаем УДОы 
                UdodDatabind();
            }
            else if (UserContext.Roles.RoleId == (int)EnumRoles.Administrator)
            {
                // администратор - загружаем оба ddl
                ddlCityDiv.Visible = ddlUdodDiv.Visible = true;
                //lblUdod.Visible = false;

                // загружаем Округа
                CityDatabind();
                // загружаем УДОы 
                UdodDatabind();
            }
            else if (UserContext.Roles.RoleId == (int)EnumRoles.Department)
            {
                // сотрудник департамента - загружаем оба ddl
                ddlCityDiv.Visible = ddlUdodDiv.Visible = true;
                //lblUdod.Visible = false;

                // загружаем Округа
                CityDatabind();
                // загружаем УДОы 
                UdodDatabind();
            }

            if (loadComplete != null) loadComplete(this, new EventArgs());
        }
    }

    protected void CityDatabind()
    {
        // загрузка списка Округов
        List<ExtendedCity> list;
        using (DictCityDb db = new DictCityDb())
        {
            list = db.GetCities();
        }
        ddlCity.DataSource = list;
        ddlCity.DataBind();
        if (AddAllProperty) ddlCity.Items.Insert(0, new ListItem("Все", ""));
        if (RemoveAllCityProperty) ddlCity.Items.Remove(new ListItem("Все", ""));

        // если в UserContext уже есть номер Округа (например из профиля сотрудника ОСИП или с предыдущей страницы)
        // то в ddl выставляется этот УДО, если нет то наоборот выставляем в UserContext первый УДО из списка
        if (UserContext.CityId.HasValue)
            ddlCity.SelectedIndex = ddlCity.Items.IndexOf(ddlCity.Items.FindByValue(UserContext.CityId.Value.ToString()));
        else
            if (!string.IsNullOrEmpty(ddlCity.SelectedValue))
                UserContext.CityId = Convert.ToInt32(ddlCity.SelectedValue);
            else
                UserContext.CityId = null;
    }

    protected void UdodDatabind()
    {
        // загрузка списка УДО выбранного cityId
        List<ExtendedUdod> list;
        using (UdodDb db = new UdodDb())
        {
            list = db.GetUdods(UserContext.CityId, null, null, null, null, null, null, null, null, null, null, null, null, null,null);
        }
        ddlUdods.DataSource = list.OrderBy(i => i.ShortName);
        ddlUdods.DataBind();
        if (AddAllProperty) ddlUdods.Items.Insert(0, new ListItem("Все", ""));

        // если в UserContext уже есть номер УДОа (например из профиля сотрудника УДО или с предыдущей страницы)
        // то в ddl выставляется этот УДО, если нет то наоборот выставляем в UserContext первый УДО из списка
        if (UserContext.UdodId.HasValue)
            ddlUdods.SelectedIndex = ddlUdods.Items.IndexOf(ddlUdods.Items.FindByValue(UserContext.UdodId.Value.ToString()));
        else
            if (!string.IsNullOrEmpty(ddlUdods.SelectedValue))
                UserContext.UdodId = Convert.ToInt32(ddlUdods.SelectedValue);
            else
                UserContext.UdodId = null;
    }

    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        // изменяется City - устанавливаем новый cityId в UserContext и обновляем список УДО
        if (!string.IsNullOrEmpty(ddlCity.SelectedValue))
            UserContext.CityId = Convert.ToInt32(ddlCity.SelectedValue);
        else
            UserContext.CityId = null;

        UdodDatabind();

        if (!string.IsNullOrEmpty(ddlUdods.SelectedValue))
            UserContext.UdodId = Convert.ToInt32(ddlUdods.SelectedValue);
        else
            UserContext.UdodId = null;

        // вызываем события что сити был изменен
        PageEvents.CityChangedEvent();
        if (CityChange != null) CityChange(this, new EventArgs());
    }

    protected void ddlUdods_SelectedIndexChanged(object sender, EventArgs e)
    {
        // изменяется Udod - устанавливаем новый UdodId в UserContext
        if (!string.IsNullOrEmpty(ddlUdods.SelectedValue))
            UserContext.UdodId = Convert.ToInt32(ddlUdods.SelectedValue);
        else
            UserContext.UdodId = null;

        // вызываем события что УДО был изменен
        PageEvents.UdodChangedEvent();
        if (UdodChange != null) UdodChange(this, new EventArgs());
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucDocument.ascx.cs" Inherits="UserControl_User_Document" %>

<script type="text/javascript">
    var eng = new Array("I", "D", "L", "X", "C", "V", "M", "i", "d", "l", "x", "c", "v", "m");
    var rus = new Array("\u0428", "\u0412", "\u0414", "\u0427", "\u0421", "\u041C", "\u042C", "\u0448", "\u0432", "\u0434", "\u0447", "\u0441", "\u043C", "\u044C");
    
    
    function bindtextboxDoc(sender, args) {
        /*$('.upperDoc3').each(function () {
            $(this).bind("keyup", changeRegistrDoc);
        });*/
        var ddlTypeDocument = '#' + '<%= ddlTypeDocument.ClientID %>';
        var tbSeries = '#' + '<%= tbSeries.ClientID %>';
        if ($(ddlTypeDocument).val() == "3") {
            $(tbSeries).each(function () {
                $(this).bind("keyup", changeLangDoc);
            });
        }
    }
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(bindtextboxDoc);

    function changeLangDoc(event) {
        if (event.keyCode > 46) {
            var str = $(this).val();
            if (str.length > 0) {

                var k = str.indexOf('-');
                //alert(k);
                if (k < 0) {
                    str = getRus2EngString(str);
                }
                else {
                    var str1 = str.substring(0, k);
                    str1 = getRus2EngString(str1);
                    str = str1 + str.substring(k, str.length);
                }
                str = str.toUpperCase();
                $(this).val(str);
            }
        }
    }

    function getRus2EngString(target) {
        var res = "";
        for (var j = 0; j < target.length; j++) {
            var test = target.charAt(j);
            for (var i = 0; i < eng.length; i++) {
                if (test == rus[i]) if (test != "\'") test = eng[i];
            }
            res += test;
        }
        return res;
    }
    
    function changeRegistrDoc(event) {
        if (event.keyCode > 46) {
            var str = $(this).val();
            if (str.length > 0) {
                str = str.toUpperCase();
                $(this).val(str);
            }
        }
    }
</script>
<asp:UpdatePanel ID="upPassport" runat="server" UpdateMode="Always">
<ContentTemplate>
<table>
    <tr>
        <td class="tdx">
            Тип документа:
        </td>
        <td>
            <div class="inputShort">
                <asp:DropDownList runat="server" ID="ddlTypeDocument" AutoPostBack="true" DataTextField="PassportTypeName" DataValueField="PassportTypeId" DataSourceID="dspassportType" CssClass="inputShort" OnSelectedIndexChanged="ddlTypeDocument_OnSelectedIndexChanged" OnDataBound="ddlTypeDocument_OnDataBound"/>
            </div>
        </td>
        <td class="tdx">
            Серия*:
        </td>
        <td>
            <div class="inputShort"><asp:TextBox runat="server" ID="tbSeries" CssClass="inputShort upperDoc3 changeDoc3" /></div>
            <ajaxToolkit:MaskedEditExtender ID="mee3" runat="server" TargetControlID="tbSeries" Mask="9999" MaskType="Number" />
            <asp:RegularExpressionValidator runat="server" ID="valBirthday" Display="Dynamic" ControlToValidate="tbSeries" ValidationGroup="71" ErrorMessage="Неправильный номер серии" SetFocusOnError="true"
            ValidationExpression="[IVXLCDMivxlcd]{1,3}-[А-Яа-я][А-Яа-я]" Enabled="false"></asp:RegularExpressionValidator>
        </td>
        <td class="tdx">
            Номер*:
        </td>
        <td>
            <div class="inputShort"><asp:TextBox runat="server" ID="tbNumber" CssClass="inputShort" /></div>
            <ajaxToolkit:MaskedEditExtender ID="mee2" runat="server" TargetControlID="tbNumber" Mask="999999" MaskType="Number" />
            
        </td>
        <td class="tdx">
            Дата выдачи*:
        </td>
        <td>
        <div class="inputShort">
            <asp:TextBox runat="server" ID="tbIssueDate" CssClass="inputShort" />
            </div>
            <ajaxToolkit:CalendarExtender  runat="server" ID="calendar" TargetControlID="tbIssueDate" Animated="false" Format="dd.MM.yyyy"/>
            <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbIssueDate" Mask="99/99/9999" MaskType="Date" />
        </td>
    </tr>
    <tr>
        <td colspan=2 align="right">
        </td>
        <td colspan=2 align="right">
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="val1" ControlToValidate="tbSeries" ErrorMessage="Поле 'Серия' не заполнено" SetFocusOnError="true" ValidationGroup="71"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator Display="Dynamic" runat="server" ID="regVal" ControlToValidate="tbSeries" ErrorMessage="Неверный формат серии" ValidationExpression="\d{4}" ValidationGroup="71" SetFocusOnError="true"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator runat="server" ID="valReqSeries" Display="Dynamic" ControlToValidate="tbSeries" ValidationGroup="10" ErrorMessage="Заполните необходимое поле" SetFocusOnError="true"></asp:RequiredFieldValidator>
        </td>
        <td colspan=2 align="right">
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="val2" ErrorMessage="Поле 'Номер' не заполнено" ControlToValidate="tbNumber" SetFocusOnError="true" ValidationGroup="71"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator Display="Dynamic" runat="server" ID="regVal1" ControlToValidate="tbNumber" ErrorMessage="Неверный формат номера" ValidationExpression="\d{6}" ValidationGroup="71" SetFocusOnError="true"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator runat="server" ID="valReqNumber" Display="Dynamic" ControlToValidate="tbNumber" ValidationGroup="10" ErrorMessage="Заполните необходимое поле" SetFocusOnError="true"></asp:RequiredFieldValidator>
        </td>
        <td colspan=2 align="right" style="vertical-align:top;">
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="val3" ErrorMessage="Поле 'Дата выдачи' не заполнено" ControlToValidate="tbIssueDate" SetFocusOnError="true" ValidationGroup="71"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="valrDate" runat="server" ControlToValidate="tbIssueDate" MinimumValue="01.08.1812" MaximumValue="01.08.3012" Type="Date" text="Дата выдачи не может быть<br>позднее Сегодня" Display="Dynamic" ValidationGroup="71"/>
        </td>
    </tr>
</table>
</ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="ddlTypeDocument" />
</Triggers>
</asp:UpdatePanel>

<asp:ObjectDataSource runat="server" ID="dspassportType" TypeName="Udod.Dal.CommonDb" SelectMethod="GetPasportType"></asp:ObjectDataSource>

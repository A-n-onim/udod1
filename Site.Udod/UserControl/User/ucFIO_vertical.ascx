﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucFIO_vertical.ascx.cs" Inherits="UserControl_User_ucFIO_vertical" %>
<script type="text/javascript">
    var eng = new Array("Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "{", "}", "A", "S", "D", "F", "G", "H", "J", "K", "L", ":", "\"", "Z", "X", "C", "V", "B", "N", "M", "<", ">", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "\'", "z", "x", "c", "v", "b", "n", "m", ",", ".", "@");
    var rus = new Array("\u0419", "\u0426", "\u0423", "\u041A", "\u0415", "\u041D", "\u0413", "\u0428", "\u0429", "\u0417", "\u0425", "\u042A", "\u0424", "\u042B", "\u0412", "\u0410", "\u041F", "\u0420", "\u041E", "\u041B", "\u0414", "\u0416", "\u042D", "\u042F", "\u0427", "\u0421", "\u041C", "\u0418", "\u0422", "\u042C", "\u0411", "\u042E", "\u0439", "\u0446", "\u0443", "\u043A", "\u0435", "\u043D", "\u0433", "\u0448", "\u0449", "\u0437", "\u0445", "\u044A", "\u0444", "\u044B", "\u0432", "\u0430", "\u043F", "\u0440", "\u043E", "\u043B", "\u0434", "\u0436", "\u044D", "\u044F", "\u0447", "\u0441", "\u043C", "\u0438", "\u0442", "\u044C", "\u0431", "\u044E", '"');
    /*
    $(document).ready(function () {
        $('.upperFio').each(function () {
            $(this).bind("keyup", changeRegistrFio1);
        });
    });*/

    function bindtextboxFio1(sender, args) {
        $('.upperFio').each(function () {
            $(this).bind("keyup", changeRegistrFio1);
        });
    }
    
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(bindtextboxFio1);
    
    function changeRegistrFio1() {
        var str = $(this).val();
        if (str.length > 0) {
            var myArray = str.match(/[A-Za-zА-Яа-яЁё]+[\s\-]*/g);
            var resultstr = '';
            if (myArray != null) {
                for (var i = 0; i < myArray.length; i++) {
                    //var result = "myArray[" + i + "] = " + myArray[i];
                    var firstliter = myArray[i].substring(0, 1);
                    firstliter = firstliter.toUpperCase();
                    myArray[i] = myArray[i].substring(1, myArray[i].length);
                    myArray[i] = myArray[i].toLowerCase();
                    myArray[i] = firstliter + myArray[i];
                    resultstr += myArray[i];
                }
            }
            str = resultstr;
            str = getEng2RusString(str);
            $(this).val(str);
        }
    }
    function getEng2RusString(target) {
        var res = "";
        for (var j = 0; j < target.length; j++) {
            var test = target.charAt(j);
            for (var i = 0; i < rus.length; i++) {
                if (test == eng[i]) if (test != "\'") test = rus[i];
            }
            res += test;
        }
        return res;
    }
</script>
<table>
    <tr>
        <td class="tdx">
            <asp:Label ID="lblLastName" runat="server">Фамилия:</asp:Label>
        </td>
        <td>
             <div class="inputShort"><asp:TextBox runat="server" ID="tbLastName" CssClass="inputShort upperFio" /></div>
        </td>
        <td>
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="val1" ErrorMessage="Поле 'Фамилия' не заполнено" ControlToValidate="tbLastName" SetFocusOnError="true" ValidationGroup="50"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator runat="server" ID="valReqNumber" Display="Dynamic" ControlToValidate="tbLastName" ValidationGroup="10" ErrorMessage="Заполните необходимое поле" SetFocusOnError="true"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="tdx">
            <asp:Label ID="lblFirstName" runat="server">Имя:</asp:Label>
        </td>
        <td>
            <div class="inputShort"><asp:TextBox runat="server" ID="tbFirstName" CssClass="inputShort upperFio" /></div>
        </td>
        <td>
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="val2" ErrorMessage="Поле 'Имя' не заполнено" ControlToValidate="tbFirstName" SetFocusOnError="true" ValidationGroup="50"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" Display="Dynamic" ControlToValidate="tbFirstName" ValidationGroup="10" ErrorMessage="Заполните необходимое поле" SetFocusOnError="true"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="tdx">
            Отчество:
        </td>
        <td>
            <div class="inputShort"><asp:TextBox runat="server" ID="tbMiddleName" CssClass="inputShort upperFio" /></div>
        </td>
    </tr>
</table>
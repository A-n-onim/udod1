﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_User_ucFIO : BasePageControl
{
    public string FIO { get { return LastName+' '+FirstName+ ' ' + MiddleName; } }
    public string LastName { get { if (string.IsNullOrEmpty(tbLastName.Text)) return ""; return tbLastName.Text/*.Substring(0, 1).ToUpper() + tbLastName.Text.Substring(1, tbLastName.Text.Length - 1)*/; } set { tbLastName.Text = value; } }
    public string FirstName { get { if (string.IsNullOrEmpty(tbFirstName.Text)) return ""; return tbFirstName.Text/*.Substring(0, 1).ToUpper() + tbFirstName.Text.Substring(1, tbFirstName.Text.Length - 1)*/; } set { tbFirstName.Text = value; } }
    public string MiddleName { get { if (string.IsNullOrEmpty(tbMiddleName.Text)) return "";  return tbMiddleName.Text/*.Substring(0, 1).ToUpper() + tbMiddleName.Text.Substring(1, tbMiddleName.Text.Length - 1)*/; } set { tbMiddleName.Text = value; } }
    public bool Enabled {set { tbLastName.Enabled = tbFirstName.Enabled = tbMiddleName.Enabled = val1.Enabled = val2.Enabled = value; }
        get { return tbLastName.Enabled && tbFirstName.Enabled && tbMiddleName.Enabled; }}
    public bool EnabledMiddleName { get { return tbMiddleName.Enabled; } set { tbMiddleName.Enabled = value; } }
    public bool reestrValidatorsEnabled { set {
        valReqLastName.Enabled = valReqFirstName.Enabled = value;
    } }

    public void Clear()
    {
        tbLastName.Text = tbFirstName.Text=tbMiddleName.Text="";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

public partial class UserControl_User_Document : BasePageControl
{
    public int PassportType { get { return Convert.ToInt32(ddlTypeDocument.SelectedValue); } set { if (ddlTypeDocument.Items.Count==0) ddlTypeDocument.DataBind(); ddlTypeDocument.SelectedIndex = ddlTypeDocument.Items.IndexOf(ddlTypeDocument.Items.FindByValue(value.ToString())); } }
    public string Series { get { return tbSeries.Text; } set { tbSeries.Text = value; } }
    public string Number { get { return tbNumber.Text; } set { tbNumber.Text = value; } }
    public DateTime? IssueDate { get { if (!string.IsNullOrEmpty(tbIssueDate.Text))
        return Convert.ToDateTime(tbIssueDate.Text); return DateTime.Now; } set { tbIssueDate.Text = value.HasValue? value.Value.ToShortDateString():""; } }
    public bool Enabled { set { 
        tbSeries.Enabled = tbNumber.Enabled = tbIssueDate.Enabled = ddlTypeDocument.Enabled = mee2.Enabled = mee3.Enabled = valrDate.Enabled = regVal.Enabled = regVal1.Enabled = val1.Enabled = val2.Enabled = val3.Enabled = value;
        regVal.Enabled = regVal1.Enabled = mee2.Enabled = mee3.Enabled = ddlTypeDocument.SelectedValue == "2";
    } }
    public bool reestrValidatorsEnabled { set {
        valReqSeries.Enabled = valReqNumber.Enabled = value;
    } }

    public void Clear()
    {
        tbSeries.Text = "";
        tbNumber.Text = "";
        tbIssueDate.Text = "";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            regVal.Enabled = regVal1.Enabled = mee2.Enabled = mee3.Enabled = ddlTypeDocument.SelectedValue == "2";

            valrDate.MaximumValue = DateTime.Today.ToString().Substring(0, 10);
        }
    }

    protected void ddlTypeDocument_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        regVal.Enabled = regVal1.Enabled = mee2.Enabled = mee3.Enabled = ddlTypeDocument.SelectedValue == "2";
        mee2.Enabled = ddlTypeDocument.SelectedValue == "3" || ddlTypeDocument.SelectedValue == "2";
        valBirthday.Enabled = ddlTypeDocument.SelectedValue == "3";
        tbSeries.ToolTip = ddlTypeDocument.SelectedValue == "3" ? "Формат серии свидетельства от 1 до 3 римских цифр 'IVXLCDM', затем тире и 2 буквы русского алфавита. Пример: IV-МЮ" : "";
        //tbSeries.Focus();
    }

    protected void ddlTypeDocument_OnDataBound(object sender, EventArgs e)
    {
        //ddlTypeDocument.SelectedIndex = 1;

        regVal.Enabled = regVal1.Enabled = mee2.Enabled = mee3.Enabled = ddlTypeDocument.SelectedValue == "2";
        mee2.Enabled = ddlTypeDocument.SelectedValue == "3" || ddlTypeDocument.SelectedValue == "2";
        valBirthday.Enabled = ddlTypeDocument.SelectedValue == "3" ;
        tbSeries.ToolTip = ddlTypeDocument.SelectedValue == "3" ? "Формат серии свидетельства от 1 до 3 римских цифр 'IVXLCDM', затем тире и 2 буквы русского алфавита. Пример: IV-МЮ" : "";
    }
}
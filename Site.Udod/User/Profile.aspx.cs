﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Profile : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(false);
    }
    protected void AddChild_Click(object sender, EventArgs e)
    {
        popupAddExtender.Show();
    }
}
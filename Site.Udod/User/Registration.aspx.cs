﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Registration : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(false);
    }
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect("~/user/Profile.aspx");
    }
}
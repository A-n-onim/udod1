﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ErrorMessageESZ.aspx.cs" Inherits="User_ErrorMessageESZ" MasterPageFile="../Master/MasterPage.master" %>

<asp:Content runat="server" ID="content" ContentPlaceHolderID="body">
    <asp:UpdatePanel runat="server" ID="upUserList" UpdateMode="Conditional">
        <ContentTemplate>
            <table>
            <tr>
                <td>Начальная дата</td>
                <td>Конечная дата</td>
                <td>Код заявления в ЕСЗ</td>
            </tr>
            <tr>
                <td><asp:TextBox runat="server" ID="tbDateStart" CssClass="input"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender runat="server" ID="calendar" TargetControlID="tbDateStart" Animated="false" Format="dd.MM.yyyy"/>
                    <ajaxToolkit:MaskedEditExtender ID="mee1" runat="server" TargetControlID="tbDateStart" Mask="99/99/9999" MaskType="Date" />
                                                
                </td>
                <td><asp:TextBox runat="server" ID="tbDateEnd" CssClass="input"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="tbDateEnd" Animated="false" Format="dd.MM.yyyy"/>
                    <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="tbDateEnd" Mask="99/99/9999" MaskType="Date" />
                    
                </td>
                <td><asp:TextBox runat="server" ID="tbESZClaimId" CssClass="input"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="3"><asp:LinkButton runat="server" ID="lnkFind" OnClick="lnkFind_OnClick"><div class="btnBlue">Поиск</div></asp:LinkButton></td>
            </tr>
         </table>
            
            <asp:GridView runat="server" ID="gvErrors" CssClass="dgc" DataSourceID="dsErrorList" AutoGenerateColumns="false">
                <EmptyDataTemplate>
                    <asp:Label ID="lblEmptyGvErrors" runat="server" style="font-size:16px">Нет списка</asp:Label>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="№" >
                        <ItemTemplate><%# gvErrors.Rows.Count%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ошибка">
                        <ItemTemplate>
                            <%# Eval("Error")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Код заявления в ЕСЗ">
                        <ItemTemplate>
                            <%# Eval("ESZClaimId")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Дата">
                        <ItemTemplate>
                            <%# ((DateTime)Eval("CreatedDate")).ToShortDateString()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ФИО заявителя">
                        <ItemTemplate>
                            <%# Eval("ParentFio")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ФИО обучаеющегося">
                        <ItemTemplate>
                            <%# Eval("PupilFio")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ДР обучаеющегося">
                        <ItemTemplate>
                            <%# ((DateTime)Eval("PupilBirthday")).ToShortDateString()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Серия документа">
                        <ItemTemplate>
                            <%# Eval("PupilPassportSeries")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Номер документа">
                        <ItemTemplate>
                            <%# Eval("PupilPassportNumber")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Дата выдачи документа">
                        <ItemTemplate>
                            <%# ((DateTime)Eval("PupilPassportIssueDate")).ToShortDateString()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="EMail">
                        <ItemTemplate>
                            <%# Eval("ParentEMail")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Номер телефона">
                        <ItemTemplate>
                            <%# Eval("ParentPhoneNumber")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Код ДО">
                        <ItemTemplate>
                            <%# Eval("ChildUnionId")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:ObjectDataSource runat="server" ID="dsErrorList" TypeName="Udod.Dal.MessageDb" SelectMethod="GetErrorsESZ">
        <SelectParameters>
            <asp:ControlParameter Name="dateStart" ControlID="tbDateStart" DbType="DateTime" ConvertEmptyStringToNull="true" />
            <asp:ControlParameter Name="dateEnd" ControlID="tbDateEnd" DbType="DateTime" ConvertEmptyStringToNull="true" />
            <asp:ControlParameter Name="eszClaimId" ControlID="tbESZClaimId" DbType="Int64" ConvertEmptyStringToNull="true" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_ErrorMessageESZ : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator);
        if (string.IsNullOrEmpty(tbDateStart.Text)) tbDateStart.Text = DateTime.Now.AddDays(-7).ToShortDateString();
        if (string.IsNullOrEmpty(tbDateEnd.Text)) tbDateEnd.Text = DateTime.Now.ToShortDateString();
    }

    protected void lnkFind_OnClick(object sender, EventArgs e)
    {
        gvErrors.DataBind();
        upUserList.Update();
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="Options.aspx.cs" Inherits="User_Options" %>
<%@ Register src="../UserControl/Menu/ucErrorPopup.ascx" tagName="ErrorPopup" tagPrefix="uct" %>

<asp:Content runat="server" ID="content" ContentPlaceHolderID="body">
    
        
    <asp:FileUpload runat="server" ID="ulfBTI"/> 
    <asp:DropDownList runat="server" ID="ddlTypeEvent">
        <Items>
            <asp:ListItem runat="server" Value="0">Дома</asp:ListItem>
            <asp:ListItem runat="server" Value="1">Улицы</asp:ListItem>
            <asp:ListItem runat="server" Value="2">Районы</asp:ListItem>
            <asp:ListItem runat="server" Value="3">Округа</asp:ListItem>
        </Items>
    </asp:DropDownList>
    <asp:LinkButton runat="server" ID="lnkSaveBTI" OnClick="lnkSaveBTI_OnClick"><div class="btnBlue">Обновить</div></asp:LinkButton>
    <uct:ErrorPopup runat="server" ID="ucErrorPopup" />

    <asp:FileUpload runat="server" ID="fuplReestr"/>
    <asp:LinkButton runat="server" ID="lnkUpdateReestrCode" OnClick="lnkUpdateReestrCode_OnClick"><div class="btnBlueLong">Обновить GUID реестра</div></asp:LinkButton>
    <asp:LinkButton runat="server" ID="lnkSendClaimsinESZ" OnClick="lnkSendClaimsinESZ_OnClick"><div class="btnBlueLong">Послать заявления в esz</div></asp:LinkButton>
    <asp:LinkButton runat="server" ID="lnkDataSet1" OnClick="lnkDataSet1_OnClick"><div class="btnBlue">2г-1</div></asp:LinkButton>
    <asp:LinkButton runat="server" ID="lnkDataSet2" OnClick="lnkDataSet1_OnClick"><div class="btnBlue">2г-2</div></asp:LinkButton>
    <asp:LinkButton runat="server" ID="lnkDataSet3" OnClick="lnkDataSet1_OnClick"><div class="btnBlue">2г-3</div></asp:LinkButton>
    <asp:LinkButton runat="server" ID="lnkDataSet4" OnClick="lnkDataSet1_OnClick"><div class="btnBlue">2г-4</div></asp:LinkButton>
    <asp:LinkButton runat="server" ID="lnkDataSet5" OnClick="lnkDataSet1_OnClick"><div class="btnBlue">2г-5</div></asp:LinkButton>
    <asp:LinkButton runat="server" ID="lnkDataSet6" OnClick="lnkDataSet1_OnClick"><div class="btnBlue">2г-6</div></asp:LinkButton>
    <asp:LinkButton runat="server" ID="lnkDataSet7" OnClick="lnkDataSet1_OnClick"><div class="btnBlue">2г-7</div></asp:LinkButton>
    <asp:LinkButton runat="server" ID="lnkDataSet2V" OnClick="lnkDataSet2V_OnClick"><div class="btnBlue">2В</div></asp:LinkButton>
    <asp:LinkButton runat="server" ID="lnkRestuct" OnClick="lnkRestuct_OnClick"><div class="btnBlue">Обновление ОУ</div></asp:LinkButton>
    <asp:LinkButton runat="server" ID="lnkRestuct1" OnClick="lnkRestuct1_OnClick"><div class="btnBlue">Подчиненность ОУ</div></asp:LinkButton>
    <asp:LinkButton runat="server" ID="lnkTmp1" OnClick="lnkTmp1_OnClick"><div class="btnBlue">Кнопка на 1 раз</div></asp:LinkButton>
    
</asp:Content>
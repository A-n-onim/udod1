﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using Udod.Dal;
using Udod.Dal.Enum;
using Aspose.Cells;

public partial class User_UserList : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator);
    }

    protected void lnkAdduser_OnClick(object sender, EventArgs e)
    {
        tbUserName.Enabled = true;
        ucFio.FirstName = "";
        ucFio.MiddleName = "";
        ucFio.LastName = "";
        tbUserName.Text = "";
        tbEmail.Text = "";
        lblWarning.Visible = false;
        cblRole.ClearSelection();
        ddlCity.Visible = lblCity.Visible = false;
        ddlUdod.Visible = lblUdod.Visible = false;
        up.Update();

        litTitle.Text = "<span class='headerCaptions'>Добавление пользователя</span>";
        upTitle.Update();
        
        popupAddExtender.Show();
    }
    protected string GetUserRoles(object user)
    {
        var roles = (user as ExtendedUser).Roles;
        StringBuilder sb = new StringBuilder();

        foreach (var extendedRole in roles)
        {
            sb.Append(extendedRole.Name + "<br>");
        }
        return sb.ToString();
    }

    protected void lnkDeleteUser_OnClick(object sender, EventArgs e)
    {
        Guid id = new Guid(((LinkButton) sender).Attributes["userId"]);
        using (UserDb db = new UserDb())
        {
            db.DeleteUser(id);
        }
        gvUserList.DataBind();
        upUserList.Update();
    }

    protected void gvUserList_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType==DataControlRowType.DataRow)
        {
            var data = (ExtendedUser)e.Row.DataItem;
            var btn = (LinkButton) e.Row.FindControl("lnkDeleteUser");
            btn.Attributes.Add("userId", data.UserId.ToString());

            btn = (LinkButton)e.Row.FindControl("lnkUpdateUser");
            btn.Attributes.Add("userId", data.UserId.ToString());
        }
    }

    protected void dsUserList_OnLoad(object sender, EventArgs e)
    {
        if (UserContext.UdodId.HasValue)
        dsUserList.SelectParameters["UdodId"].DefaultValue = UserContext.UdodId.Value.ToString();
    }

    protected void btnCancel_OnClick(object sender, EventArgs e)
    {
        popupAddExtender.Hide();
    }

    protected void btnAddUser_OnClick(object sender, EventArgs e)
    {
        // Добавление пользователя
        using (UserDb db = new UserDb())
        {
            if ((tbUserName.Enabled) && (db.CheckLoginIsExist(tbUserName.Text)))
            {
                lblWarning.Visible = true;
                up.Update();
                return;
            }

            Guid userId = db.CreateUser(tbUserName.Text, tbPassword.Text, tbEmail.Text);
            Membership.Provider.ResetPassword(tbUserName.Text, tbPassword.Text);

            db.SaveProfile(userId, tbUserName.Text, ucFio.LastName, ucFio.FirstName, ucFio.MiddleName, DateTime.Now, tbEmail.Text, "", "", "", "");

            foreach (ListItem cbx in cblRole.Items)
            {
                if (cbx.Selected)
                {
                    if (cbx.Value == "2")
                        db.AddUserRole(userId, 2, Convert.ToInt32(ddlCity.SelectedValue), null);
                    else if (cbx.Value == "3")
                        db.AddUserRole(userId, 3, Convert.ToInt32(ddlCity.SelectedValue), Convert.ToInt32(ddlUdod.SelectedValue));
                    else
                        db.AddUserRole(userId, Convert.ToInt32(cbx.Value), null, null);
                }
                else
                {
                    db.DeleteUserRole(userId, Convert.ToInt32(cbx.Value));
                }
            }
        }

        popupAddExtender.Hide();
        gvUserList.DataBind();
        upUserList.Update();
    }

    protected void cblRoles_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlCity.Visible = lblCity.Visible = (cblRole.Items.FindByValue("3").Selected || cblRole.Items.FindByValue("2").Selected);
        ddlUdod.Visible = lblUdod.Visible = (cblRole.Items.FindByValue("3").Selected);
    }

    protected void lnkUpdateUser_OnClick(object sender, EventArgs e)
    {
        tbUserName.Enabled = false;
        up.Update();
        litTitle.Text = "<span class='headerCaptions'>Редактирование пользователя</span>";
        upTitle.Update();
        Guid id = new Guid(((LinkButton)sender).Attributes["userId"]);
        using (UserDb db = new UserDb())
        {
            var user = db.GetUserToUpdate(id);
            if (user != null)
            {
                tbUserName.Text = user.UserName;
                ucFio.LastName = user.LastName;
                ucFio.FirstName = user.FirstName;
                ucFio.MiddleName = user.MiddleName;
                tbPassword.Text = "";
                tbConfirmPassword.Text = "";
                tbEmail.Text = user.EMail;
                cblRole.ClearSelection();
                lblWarning.Visible = false;
                foreach (ExtendedRole role in user.Roles)
                {
                    cblRole.Items.FindByValue(role.RoleId.ToString()).Selected = true;
                }

                ddlCity.Visible = lblCity.Visible = (cblRole.Items.FindByValue("3").Selected || cblRole.Items.FindByValue("2").Selected);
                ddlUdod.Visible = lblUdod.Visible = (cblRole.Items.FindByValue("3").Selected);

                if ((cblRole.Items.FindByValue("2").Selected) || (cblRole.Items.FindByValue("3").Selected))
                    ddlCity.SelectedIndex = ddlCity.Items.IndexOf(ddlCity.Items.FindByValue(user.CityId.ToString()));

                if (cblRole.Items.FindByValue("3").Selected)
                    ddlUdod.SelectedIndex = ddlUdod.Items.IndexOf(ddlUdod.Items.FindByValue(user.UdodId.ToString()));
            }
        }
        up.Update();

        popupAddExtender.Show();
    }

    protected void genUsers_OnClick(object sender, EventArgs e)
    {
        popupLoadFileExtender.Show();

        
        /*using (UdodDb db = new UdodDb())
        {
            list = db.GetUdods(null, null, null, null, null, null, null, null, null,null, null);
            list = list.Where(i => i.CityId != 10).ToList();
            list = list.Where(i => i.CityId != 11).ToList();
            list = list.Where(i => i.UdodId > 105 && !string.IsNullOrEmpty(i.FioDirector)).ToList();
        }
        */
        /*list.Clear();
        list.Add(new ExtendedUdod() { FioDirector = "Гарбина Александра Евгеньевна", Name = "", CityId = 4});
        list.Add(new ExtendedUdod() { FioDirector = "Анисимов Вадим Андреевич", Name = "", CityId = 4 });
        list.Add(new ExtendedUdod() { FioDirector = "Орловская Юлия Алексеевна", Name = "", CityId = 4 });
        list.Add(new ExtendedUdod() { FioDirector = "Михеева Людмила Анатольевна", Name = "", CityId = 1 });
        list.Add(new ExtendedUdod() { FioDirector = "Крутякова Евгения Никитична", Name = "", CityId = 1 });
        list.Add(new ExtendedUdod() { FioDirector = "Бондарева Татьяна Владиславовна", Name = "", CityId = 1 });
        list.Add(new ExtendedUdod() { FioDirector = "Мелихова Елена Николаевна", Name = "", CityId = 1 });
        list.Add(new ExtendedUdod() { FioDirector = "Астапенко Ирина Геннадьевна", Name = "", CityId = 7 });
        list.Add(new ExtendedUdod() { FioDirector = "Бушуева Марина Юрьевна", Name = "", CityId = 7 });
        list.Add(new ExtendedUdod() { FioDirector = "Золотухин Дмитрий Сергеевич", Name = "", CityId = 7 });
        list.Add(new ExtendedUdod() { FioDirector = "Щербакова Людмила Рудольфовна", Name = "", CityId = 7 });
        list.Add(new ExtendedUdod() { FioDirector = "Бабенко Любовь Львовна", Name = "", CityId = 8 });
        list.Add(new ExtendedUdod() { FioDirector = "Головинова Галина Николаевна ", Name = "", CityId = 8 });
        list.Add(new ExtendedUdod() { FioDirector = "Александрова Анжела Михайловна", Name = "", CityId = 9 });
        list.Add(new ExtendedUdod() { FioDirector = "Процко Валерий Игоревич", Name = "", CityId = 9 });
        list.Add(new ExtendedUdod() { FioDirector = "Макарова Ольга Владимировна", Name = "", CityId = 9 });
        list.Add(new ExtendedUdod() { FioDirector = "Шебанов Алексей Олегович", Name = "", CityId = 9 });
        list.Add(new ExtendedUdod() { FioDirector = "Чекалниа Ольга Павловна", Name = "", CityId = 3 });
        list.Add(new ExtendedUdod() { FioDirector = "Чепенко Татьяна Геннадьевна", Name = "", CityId = 3 });
        list.Add(new ExtendedUdod() { FioDirector = "Зайцев Сергей Александрович", Name = "", CityId = 3 });
        list.Add(new ExtendedUdod() { FioDirector = "Минаждинова Лилия Анясовна", Name = "", CityId = 3 });
        list.Add(new ExtendedUdod() { FioDirector = "Свердлова Ирина Юрьевна", Name = "", CityId = 3 });
        list.Add(new ExtendedUdod() { FioDirector = "Малкова Людмила Викторовна", Name = "", CityId = 3 });
        list.Add(new ExtendedUdod() { FioDirector = "Тупиков Владимир Юрьевич", Name = "", CityId = 3 });
        list.Add(new ExtendedUdod() { FioDirector = "Тимошенко Евгения Викторовна", Name = "", CityId = 3 });
        list.Add(new ExtendedUdod() { FioDirector = "Чернова Нина Николаевна", Name = "", CityId = 3 });
        list.Add(new ExtendedUdod() { FioDirector = "Помысухина Светлана Анатольевна", Name = "", CityId = 3 });
        */
        //list.Add(new ExtendedUdod() { FioDirector = "osip", Name = "" });
        //list.Add(new ExtendedUdod() { FioDirector = "Максименкова Ирина Васильевна", Name = "" });
        //list.Add(new ExtendedUdod() { FioDirector = "Киселева Людмила Геннадьевна", Name = "" });
        //list.Add(new ExtendedUdod() { FioDirector = "Березина Вера Александровна", Name = "" });
        

        
        //Membership.Provider.ResetPassword("fyvaprovjya", "guest");

    }

    protected void lnkLoadFile_OnClick(object sender, EventArgs e)
    {
        // загрзка самих данных и их проверка
        //Загрузка файлов включить

        FileFormatType format;
        if (fileUpload.HasFile)
        {

            if (fileUpload.FileName.EndsWith(".xlsx"))
            {
                format = FileFormatType.Excel2007Xlsx;
            }
            else
            {
                format = FileFormatType.Excel2003;
            }

            ImportFromExcel(fileUpload.FileContent, format);

        }
        upLoadFile.Update();
        gvuserspassword.DataBind();
        upUserList.Update();
        popupLoadFileExtender.Hide();
    }

    protected void ImportFromExcel(Stream data, FileFormatType format)
    {
        List<ExtendedUdod> list = new List<ExtendedUdod>();
        Workbook workbook = new Workbook();
        try
        {
            workbook.Open(data, format);
            //workbook.LoadData(data);
        }
        catch (Exception)
        {
            throw new Exception("Данный формат файлов не поддерживается.");
        }

        Worksheet sheet = workbook.Worksheets[0];
        // загрузка в список и вывод ошибок если что
        int row = 0;
        int d = sheet.Cells.Rows.Count;
        for (; row < d; row++)
        {
            string director = sheet.Cells[row, 0].StringValue;
            int udod = sheet.Cells[row, 1].IntValue;
            int city = sheet.Cells[row, 2].IntValue;
            int role = sheet.Cells[row, 3].IntValue;

            // в ExtendedUdod нет roleId, поэтому запишу роль в UdodTypeId
            list.Add(new ExtendedUdod() { FioDirector = director, CityId = city, UdodId = udod, UdodTypeId = role });
        }

        List<ExtendedPassword> list1 = new List<ExtendedPassword>();
        using (UserDb db = new UserDb())
        {
            foreach (var extendedUdod in list)
            {
                if (string.IsNullOrEmpty(extendedUdod.FioDirector)) continue;
                string fio = extendedUdod.FioDirector.Trim();
                string[] values = fio.Split(' ');
                fio = values[0];
                for (int i = 1; i < values.Count(); i++)
                {
                    fio += values[i].Substring(0, 1);
                }

                string login = SiteUtility.Transliteration.Front(fio);
                
                //if (db.CheckLoginIsExist(login)) continue; // если пользователь с таким логином уже есть, игнорируем его

                //string login = extendedUdod.FioDirector;
                string str = "qwertyuiopasdfghjklzxcvbnm1234567890";
                string pass = "";
                Random r = new Random();

                for (int j = 0; j < 8; j++)
                {
                    int tmp = r.Next(36);
                    pass += str[tmp];
                }
                if (db.GetUser(login).UserId == Guid.Empty)
                {
                    Guid id = db.CreateUser(login, pass, "");
                    db.SaveProfile(id, login, values[0], values[1], values.Count() > 2 ? values[2] : "", DateTime.Now, "", "", "", "", "");
                    
                    if (extendedUdod.UdodTypeId == 2)
                        db.AddUserRole(id, 2, extendedUdod.CityId, null);
                    else if (extendedUdod.UdodTypeId == 3)
                        db.AddUserRole(id, 3, extendedUdod.CityId, extendedUdod.UdodId);
                    else
                        db.AddUserRole(id, extendedUdod.UdodTypeId, null, null);

                    Membership.Provider.ResetPassword(login, pass);
                    list1.Add(new ExtendedPassword() { Login = login, Password = pass, UdodName = extendedUdod.FioDirector, UdodId = extendedUdod.UdodId });
                }
                else
                {
                    string login_r = login + str[r.Next(36)];
                    while (db.GetUser(login_r).UserId != Guid.Empty)
                    {
                        login_r = login + str[r.Next(36)];
                    }

                    Guid id = db.CreateUser(login_r, pass, "");
                    db.SaveProfile(id, login_r, values[0], values[1], values.Count() > 2 ? values[2] : "", DateTime.Now, "", "", "", "", "");

                    if (extendedUdod.UdodTypeId == 2)
                        db.AddUserRole(id, 2, extendedUdod.CityId, null);
                    else if (extendedUdod.UdodTypeId == 3)
                        db.AddUserRole(id, 3, extendedUdod.CityId, extendedUdod.UdodId);
                    else
                        db.AddUserRole(id, extendedUdod.UdodTypeId, null, null);

                    Membership.Provider.ResetPassword(login_r, pass);
                    list1.Add(new ExtendedPassword() { Login = login_r, Password = pass, UdodName = extendedUdod.FioDirector, UdodId=extendedUdod.UdodId });
                }
            }
        }
        gvuserspassword.DataSource = list1;
        gvuserspassword.DataBind();

        gvUserList.DataBind();
        upUserList.Update();
    }

    public void pgrUserList_OnChange(object sender, EventArgs e)
    {
        gvUserList.DataBind();
        upUserList.Update();
    }

    public void ds_Selected(object sender, EventArgs e)
    {
        using (UserDb db = new UserDb())
        {
            pgrUserList.numElements = db.GetUsersCount(null, tbLoginFind.Text, tbLastName.Text, tbFirstName.Text, tbMiddleName.Text);
        }
    }

    public void lnkFind_OnClick(object sender, EventArgs e)
    {
        pgrUserList.Clear();
        gvUserList.DataBind();
        upUserList.Update();
    }

    public void lnkClear_OnClick(object sender, EventArgs e)
    {
        tbFirstName.Text = "";
        tbLastName.Text = "";
        tbMiddleName.Text = "";
        pgrUserList.Clear();
        gvUserList.DataBind();
        upUserList.Update();
    }

    protected void lnkGeneratePassword_OnClick(object sender, EventArgs e)
    {
        string str = "qwertyuiopasdfghjklzxcvbnm1234567890";
        string pass = "";
        Random r = new Random();

        for (int j = 0; j < 8; j++)
        {
            int tmp = r.Next(36);
            pass += str[tmp];
        }
        lblPassword.Text = pass;
        var data = new DataObject();
        data.SetData(DataFormats.UnicodeText, true, pass);
        var thread = new Thread(() => Clipboard.SetDataObject(data, true));
        thread.SetApartmentState(ApartmentState.STA);
        thread.Start();
        thread.Join();
        up.Update();
    }
}
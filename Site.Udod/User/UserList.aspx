﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="UserList.aspx.cs" Inherits="User_UserList" %>
<%@ Register TagPrefix="uct" TagName="FIO" Src="~/UserControl/User/ucFIO_vertical.ascx" %>
<%@ Register src="../UserControl/Menu/ucPager.ascx" tagName="Pager" tagPrefix="uc" %>

<asp:Content runat="server" ID="contentUserList" ContentPlaceHolderID="body">

    <asp:Panel DefaultButton="lnkFind" runat="server" ID="pFind" GroupingText="Поиск по ФИО">
        <table>
            <tr>
                <td>Логин</td>
                <td>Фамилия</td>
                <td>Имя</td>
                <td>Отчество</td>
            </tr>
            <tr>
                <td><asp:TextBox ID="tbLoginFind" runat="server"></asp:TextBox></td>
                <td><asp:TextBox ID="tbLastName" runat="server"></asp:TextBox></td>
                <td><asp:TextBox ID="tbFirstName" runat="server"></asp:TextBox></td>
                <td><asp:TextBox ID="tbMiddleName" runat="server"></asp:TextBox></td>
            </tr>
         </table>
         <table>
            <tr>
                <td>
                    <asp:LinkButton runat="server" ID="lnkFind" OnClick="lnkFind_OnClick"><div class="btnBlueLong">Поиск</div></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton runat="server" ID="lnkClear" OnClick="lnkClear_OnClick"><div class="btnBlueLong">Сбросить фильтры</div></asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upUserList" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView runat="server" ID="gvuserspassword" AutoGenerateColumns="true">
            </asp:GridView>

            <asp:LinkButton runat="server" ID="genUsers" OnClick="genUsers_OnClick" Visible="true"><div class="btnBlueLong">Сгенерировать пользователей</div></asp:LinkButton>
            <br />
            <asp:LinkButton runat="server" ID="lnkAddUser" OnClick="lnkAdduser_OnClick" ><div class="btnBlueLong">Добавить пользователя</div></asp:LinkButton>
            <br />
            <asp:GridView runat="server" ID="gvUserList" CssClass="dgc" DataSourceID="dsUserList" AutoGenerateColumns="false" OnRowDataBound="gvUserList_OnRowDataBound" DataKeyNames="UserId" >
                <EmptyDataTemplate>
                    <asp:Label ID="lblEmptyGvUserList" runat="server" style="font-size:16px">Нет пользователей</asp:Label>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="№" >
                        <ItemTemplate><%# gvUserList.Rows.Count + pgrUserList.PagerIndex%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Логин">
                        <ItemTemplate>
                            <%# Eval("UserName") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ФИО">
                        <ItemTemplate>
                            <%# Eval("Fio") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Роли">
                        <ItemTemplate>
                            <%# GetUserRoles(Container.DataItem)%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Редактирование">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lnkUpdateUser" OnClick="lnkUpdateUser_OnClick"><div class="btnBlue">Изменить</div> </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Удаление">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lnkDeleteUser" OnClick="lnkDeleteUser_OnClick" OnClientClick="javascript:return confirm('У пользователя удалятся все данные! Продолжить?');"><div class="btnBlue">Удалить</div> </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger runat="server" ControlID="lnkFind"/>
        </Triggers>
    </asp:UpdatePanel>
    <uc:Pager runat="server" ID="pgrUserList" needStartDots="false" needEndDots="false" pagesCount="10" OnChange="pgrUserList_OnChange"/>

    <asp:ObjectDataSource runat="server" ID="dsUserList" TypeName="Udod.Dal.UserDb" SelectMethod="GetUsersPaging" OnLoad="dsUserList_OnLoad" OnSelected="ds_Selected">
        <SelectParameters>
            <asp:Parameter runat="server" Name="udodId" ConvertEmptyStringToNull="true"/>
            <asp:ControlParameter Name="Login" ControlID="tbLoginFind" ConvertEmptyStringToNull="true" />
            <asp:ControlParameter Name="LastName" ControlID="tbLastName" ConvertEmptyStringToNull="true" />
            <asp:ControlParameter Name="FirstName" ControlID="tbFirstName" ConvertEmptyStringToNull="true" />
            <asp:ControlParameter Name="MiddleName" ControlID="tbMiddleName" ConvertEmptyStringToNull="true" />
            <asp:ControlParameter Name="pagerIndex" ControlID="pgrUserList" ConvertEmptyStringToNull="true" PropertyName="PagerIndex" />
            <asp:ControlParameter Name="pagerLength" ControlID="pgrUserList" ConvertEmptyStringToNull="true" PropertyName="PagerLength" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:Panel runat="server" ID="popupAddUser" CssClass="modalPopup" style="border: 2px solid black;" BackColor="White">
        <asp:Panel ID="pnlHeader" runat="server" >
            <asp:UpdatePanel runat="server" ID="upTitle" UpdateMode="Conditional">
            <ContentTemplate>
            <asp:Label ID="litTitle" Text="<span class='headerCaptions'>Редактирование пользователя</span>" runat="server"/>
            </ContentTemplate>
            </asp:UpdatePanel>
            <asp:LinkButton ID="lnkClose" runat="server" 
                OnClientClick="$find('AddUserPopup').hide(); return false;" />
        </asp:Panel>
        <div>
            <asp:UpdatePanel runat="server" ID="up" UpdateMode="Conditional">
            <ContentTemplate>
            <table border=0>
                <tr>
                    <td colspan="2">
                        <uct:FIO runat="server" ID="ucFio"/>
                    </td>
                    <td class="tdx" rowspan=3>
                        Роль:
                    </td>
                    <td rowspan=3 valign="top">
                        <div style="text-align:left">
                            <asp:CheckBoxList runat="server" ID="cblRole" DataSourceID="dsRoles" DataTextField="Name" DataValueField="RoleId" AutoPostBack="true" OnSelectedIndexChanged="cblRoles_SelectedIndexChanged"/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Электронный адрес
                    </td>
                    <td>
                        <div class="inputShort">
                            <asp:TextBox runat="server" ID="tbEmail" CssClass="inputShort"></asp:TextBox>
                            <asp:RegularExpressionValidator runat="server" ID="valEmail" ErrorMessage="Ошибка" ValidationGroup="77" ControlToValidate="tbEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><asp:LinkButton runat="server" ID="lnkGeneratePassword" OnClick="lnkGeneratePassword_OnClick"><div class="btnBlue">Сгенерировать пароль</div></asp:LinkButton></td>
                    <td>
                        <asp:Label runat="server" ID="lblPassword"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Логин
                    </td>
                    <td >
                        <div class="inputShort">
                            <asp:TextBox runat="server" ID="tbUserName" CssClass="inputShort"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="val2" ValidationGroup="77" ErrorMessage="*" ControlToValidate="tbUserName"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblWarning" runat="server" Text="Такой логин уже есть" Visible="false" ForeColor="Red"/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Пароль
                    </td>
                    <td>
                        <div class="inputShort">
                            <asp:TextBox runat="server" ID="tbPassword" TextMode="Password" CssClass="inputShort"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="77" ErrorMessage="*" ControlToValidate="tbPassword"></asp:RequiredFieldValidator>
                        </div>
                    </td>
                    <td class="tdx">
                        <asp:Label ID="lblCity" runat="server" Text="ОКРУГ:" />
                    </td>
                    <td>
                        <div class="inputShort">
                            <asp:DropDownList runat="server" CssClass="inputShort" ID="ddlCity" DataSourceID="dsCity" DataTextField="Name" DataValueField="CityId" AutoPostBack="true"/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="tdx">
                        Подтверждение пароля
                    </td>
                    <td>
                        <div class="inputShort">
                            <asp:TextBox runat="server" ID="tbConfirmPassword" TextMode="Password" CssClass="inputShort"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="77" ErrorMessage="*" ControlToValidate="tbConfirmPassword"></asp:RequiredFieldValidator>
                        </div>
                    </td>
                    <td class="tdx">
                        <asp:Label ID="lblUdod" runat="server" Text="УДО:" />
                    </td>
                    <td>
                        <div class="inputShort">
                            <asp:DropDownList runat="server" CssClass="inputShort" ID="ddlUdod" DataSourceID="dsUdod" DataTextField="ShortName" DataValueField="UdodId"/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CompareValidator runat="server" ID="val" ControlToValidate="tbPassword"  ControlToCompare="tbConfirmPassword" ErrorMessage="Пароли не совпадают" Display="Dynamic"></asp:CompareValidator>
                    </td>
                </tr>
            </table>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger runat="server" ControlID="ddlCity" />
            </Triggers>
            </asp:UpdatePanel>
        </div>
        <div >
            <asp:UpdatePanel ID="upAddUserButtons" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:LinkButton ID="btnAddUser" runat="server" CausesValidation="true" ValidationGroup="77" OnClick="btnAddUser_OnClick"><div class="btnBlue">Сохранить</div></asp:LinkButton>
                            </td>
                            <td>
                                <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false" OnClick="btnCancel_OnClick"><div class="btnRed">Отменить</div></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel> 
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupAddExtender" PopupDragHandleControlID="pnlHeader"
        PopupControlID="popupAddUser" CancelControlID="btnCancel"
        TargetControlID="btnShowPopup" RepositionMode="None"
        BehaviorID="AddUserPopup"/>

    <asp:Button ID="btnShowPopup" runat="server" style="display:none" />

    <asp:Panel runat="server" ID="popupLoadFile" style="padding: 5px; display: none; border: 2px solid black" BackColor="White">
    <asp:Panel ID="pLoadFileHeader" runat="server" >
        <asp:Label ID="Label5" Text="<span class='headerCaptions'>Загрузка данных</span>" runat="server"/>
        <asp:LinkButton ID="LinkButton3" runat="server" 
            OnClientClick="$find('LoadFile').hide(); return false;" />
    </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upLoadFile" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:FileUpload runat="server" ID="fileUpload" />
                <table>
                    <tr>
                        <td>
                            <asp:LinkButton runat="server" ID="lnkLoadFile" OnClick="lnkLoadFile_OnClick"><div class="btnBlue">Загрузить</div></asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="btnCancelLoadFile" runat="server" CausesValidation="false"><div class="btnBlue">Закрыть</div></asp:LinkButton>
                        </td>
                    </tr>
                </table>
        </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger runat="server" ControlID="lnkLoadFile"/>
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupLoadFileExtender" PopupDragHandleControlID="pLoadFileHeader"
        PopupControlID="popupLoadFile" CancelControlID="btnCancelLoadFile"
        TargetControlID="btnShowLoadFile" RepositionMode="None"
        BehaviorID="LoadFile"/>
    <asp:Button ID="btnShowLoadFile" runat="server" style="display:none" />

    <asp:ObjectDataSource runat="server" ID="dsRoles" TypeName="Udod.Dal.RoleDb" SelectMethod="GetRoles"></asp:ObjectDataSource>
    <asp:ObjectDataSource runat="server" ID="dsCity" TypeName="Udod.Dal.DictCityDb" SelectMethod="GetCities"></asp:ObjectDataSource>
    <asp:ObjectDataSource runat="server" ID="dsUdod" TypeName="Udod.Dal.UdodDb" SelectMethod="GetUdods">
        <SelectParameters>
            <asp:ControlParameter Name="cityId" ConvertEmptyStringToNull="true" ControlID="ddlCity" PropertyName="SelectedValue"/>
            <asp:Parameter Name="cityCode" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="rayonCode" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="metroId" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="programId" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="profileId" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="sectionId" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="udodName" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="typeBudgetId" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="sectionName" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="profileName" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="programName" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="childUnionName" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="rayonId" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="ShowIsDOEnabled" ConvertEmptyStringToNull="true" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using Udod.Dal;

public partial class User_Options : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckPermissions(UserContext.Roles.IsAdministrator);

    }

    protected void lnkSaveBTI_OnClick(object sender, EventArgs e)
    {
        LoadFile(Convert.ToInt32(ddlTypeEvent.SelectedValue));
    }

    private void LoadFile(int fileType)
    {
        FileFormatType format;
        if (ulfBTI.HasFile)
        {
            if (ulfBTI.FileName.EndsWith(".xlsx"))
            {
                format = FileFormatType.Excel2007Xlsx;
            }
            else
            {
                format = FileFormatType.Excel2003;
            }

            Workbook workbook = new Workbook();
            try
            {
                workbook.Open(ulfBTI.FileContent, format);
                //workbook.LoadData(data);
            }
            catch (Exception)
            {
                throw new Exception("Данный формат файлов не поддерживается.");
            }
            Worksheet sheet = workbook.Worksheets[0];
            int countColums = sheet.Cells.End.Column+1;
            int numcolums = 0;
            if (fileType == 0) numcolums = 1;
            if ((countColums == 4 && (fileType == 2 || fileType == 3)) || (countColums == 26 && fileType == 1) || (countColums == 21 && fileType == 0))
            {
                try
                {

                    using (KladrDb db = new KladrDb())
                    {

                        int j = 1;
                        while (!string.IsNullOrEmpty(sheet.Cells[j, numcolums].StringValue))
                        {

                            if (!string.IsNullOrEmpty(sheet.Cells[j, numcolums].StringValue))
                            {
                                long code = Convert.ToInt64(sheet.Cells[j, numcolums].Value);
                                List<string> updateParams = new List<string>();
                                for (int i = 1 + numcolums; i < countColums; i++)
                                {
                                    updateParams.Add(sheet.Cells[j, i].StringValue);
                                }
                                db.UpdateDict(code, fileType, updateParams);
                            }
                            j++;
                        }
                    }
                }
                catch (Exception)
                {
                    ucErrorPopup.ShowError("Ошибка", "Файл не совпадает с необходимым форматом данных", "", "Ок", false, true);
                }
                
            }
            else
            {
                // ошибка
                ucErrorPopup.ShowError("Ошибка", "Файл не совпадает с необходимым форматом данных", "", "Ок", false,true);
            }
        }
    }

    protected void lnkUpdateReestrCode_OnClick(object sender, EventArgs e)
    {
        FileFormatType format;
        if (fuplReestr.HasFile)
        {
            if (fuplReestr.FileName.EndsWith(".xlsx"))
            {
                format = FileFormatType.Excel2007Xlsx;
            }
            else
            {
                format = FileFormatType.Excel2003;
            }

            Workbook workbook = new Workbook();
            try
            {
                workbook.Open(fuplReestr.FileContent, format);
                //workbook.LoadData(data);
            }
            catch (Exception)
            {
                throw new Exception("Данный формат файлов не поддерживается.");
            }
            Worksheet sheet = workbook.Worksheets[0];

            int i = 1;
            using (PupilsDb db = new PupilsDb())
            {
                while (!string.IsNullOrEmpty(sheet.Cells[i, 2].StringValue))
                {
                    try
                    {
                        //db.UpdatePupilReestrCode(new Guid(sheet.Cells[i, 3].StringValue), Convert.ToInt64(sheet.Cells[i, 1].StringValue), null, new Guid(sheet.Cells[i, 2].StringValue), "");
                    }
                    catch (Exception)
                    {
                    }
                    
                    i++;
                }
            }
        }
    }

    protected void lnkSendClaimsinESZ_OnClick(object sender, EventArgs e)
    {
        //using (ClaimDb db = new ClaimDb())
        //{
        //    var list = db.GetClaimsNotinESZ();
        //    list = list.Where(i => i == 893671).ToList();
        //    //list.Add(691486);

        //    foreach (long l in list)
        //    {
        //        ExtendedClaim claim1 = db.GetClaim(l);
        //        if (claim1.ClaimId != 0)
        //        {
        //            try
        //            {
        //                SiteUtility.SendClaimInESZ(new ExtendedPupil()
        //                                               {
        //                                                   LastName = claim1.Pupil.LastName,
        //                                                   FirstName = claim1.Pupil.FirstName,
        //                                                   MiddleName = claim1.Pupil.MiddleName,
        //                                                   Birthday = claim1.Pupil.Birthday,
        //                                                   Sex = claim1.Pupil.Sex,
        //                                                   Passport = new ExtendedPassport()
        //                                                                  {
        //                                                                      Series = claim1.Pupil.Passport.Series,
        //                                                                      Number = claim1.Pupil.Passport.Number,
        //                                                                      IssueDate = claim1.Pupil.Passport.IssueDate,
        //                                                                      PassportType = new ExtendedPassportType()
        //                                                                                         {
        //                                                                                             PassportTypeId =
        //                                                                                                 claim1.Pupil.
        //                                                                                                 Passport.
        //                                                                                                 PassportType.
        //                                                                                                 PassportTypeId
        //                                                                                         }
        //                                                                  }
        //                                               }, new ExtendedPupil()
        //                                                      {
        //                                                          LastName = claim1.Parent.LastName,
        //                                                          FirstName = claim1.Parent.FirstName,
        //                                                          MiddleName = claim1.Parent.MiddleName,
        //                                                          UserId = claim1.Parent.UserId
        //                                                      }, claim1.ClaimId, claim1.ChildUnion.ChildUnionId,
        //                                           claim1.ClaimId.ToString());

        //               // ExportInAsGuf tmp = new ExportInAsGuf();
        //                //tmp.UpdateStatus(claim1.ClaimStatus.ClaimStatusId, l, "");
        //            }
        //            catch (Exception)
        //            {
        //            }
        //        }
        //    }
        //}
    }

    protected void lnkDataSet1_OnClick(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)(sender);
        int type = -1;
        try
        {
            type = Convert.ToInt32(lnk.ID.Replace("lnkDataSet", ""));
        }
        catch (Exception)
        {
            
        }
        if (type != -1)
        {
            HttpContext.Current.Response.Clear();

            string filename = string.Format("2G-" + type + ".txt");

            HttpContext.Current.Response.AddHeader("content-disposition",
                                                   string.Format("attachment; filename={0}", filename));
            HttpContext.Current.Response.ContentType = "text/plain";
            string header = "";
            switch (type)
            {
                case 1:
                    header = "AREA,EDU_NAME,EDU_NUMBER,DATE,PUPILS_QUANTITY\n";
                break;
                case 2:
                header = "AREA,COURSE_YEAR,EDU_NAME,EDU_NUMBER,DATE,PUPILS_QUANTITY\n";
                break;
                case 3:
                header = "BIRTH_YEAR,BIRTH_MONTH,PUPILS_QUANTITY,DATE\n";
                break;
                case 4:
                header = "AREA,DISTRICT,PUPILS_QUANTITY,DATE\n";
                break;
                case 5:
                header = "ID_EKIS,AREA,DISTRICT,EDU_NUMBER,EDU_NAME,PUPILS_QUANTITY,DATE\n";
                break;
                case 6:
                header = "AREA,COURSE_YEAR,PUPILS_QUANTITY,BOYS_QUANTITY,GIRLS_QUANTITY,DATE\n";
                break;
                case 7:
                header = "AREA,EDU_FORM,PUPILS_QUANTITY,DATE\n";
                break;
            }
            HttpContext.Current.Response.Write(header);
            using (ReportDb db = new ReportDb())
            {
                var list = db.GetDataSet(type);
                MyStringBuilder sb = new MyStringBuilder(",");
                foreach (var dataSetValue in list)
                {
                    foreach (var value in dataSetValue)
                    {
                        if (value.IsInt)
                        {
                            sb.Append(value.StrValue=="0"?"":value.StrValue);
                        }
                        else
                        {
                            sb.Append("\"" + value.StrValue.Trim().Replace("\"", "'") + "\"");
                        }
                    }
                    HttpContext.Current.Response.Write(sb.ToString());
                    HttpContext.Current.Response.Write("\n");
                    sb=new MyStringBuilder(",");
                }

            }


            HttpContext.Current.Response.End();
        }
    }


    protected void lnkDataSet2V_OnClick(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();

        string filename = string.Format("ChildUnionList.xls");

        HttpContext.Current.Response.AddHeader("content-disposition",
                                                string.Format("attachment; filename={0}", filename));
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
        Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];
        #region Стили
        Aspose.Cells.Style style_area = workbook.Styles[0];
        Aspose.Cells.Style style_header = workbook.Styles[1];
        style_area.ShrinkToFit = true;

        style_area.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_area.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_area.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
        style_header.Borders[Aspose.Cells.BorderType.TopBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.BottomBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.LeftBorder].Color =
            style_header.Borders[Aspose.Cells.BorderType.RightBorder].Color = System.Drawing.Color.Black;
        style_header.Font.IsBold = true;
        style_header.Font.Size = 12;
        style_header.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
        style_header.BackgroundColor = System.Drawing.Color.Gray;
        style_header.Pattern = Aspose.Cells.BackgroundType.Solid;
        #endregion
        using (ChildUnionDb db = new ChildUnionDb())
        {
            var childunions = db.GetChildUnionsForExport(null);

            sheet.Cells[0, 0].PutValue("UNIQUE_ID");
            sheet.Cells[0, 1].PutValue("ID_EKIS");
            sheet.Cells[0, 2].PutValue("FULL_NAME");
            sheet.Cells[0, 3].PutValue("SHORT_NAME");
            sheet.Cells[0, 4].PutValue("NUMBER");
            sheet.Cells[0, 5].PutValue("TYPE");
            sheet.Cells[0, 6].PutValue("CLASS");
            sheet.Cells[0, 7].PutValue("DIRECTION");
            sheet.Cells[0, 8].PutValue("PROFILE");
            sheet.Cells[0, 9].PutValue("ACTIVITY_TYPE");
            sheet.Cells[0, 10].PutValue("PROGRAM_CATEGORY");
            sheet.Cells[0, 11].PutValue("GROUP_NAME");
            sheet.Cells[0, 12].PutValue("ACCESSORY");
            sheet.Cells[0, 13].PutValue("AGE_FROM");
            sheet.Cells[0, 14].PutValue("AGE_TO");
            sheet.Cells[0, 15].PutValue("FREEBIE");
            sheet.Cells[0, 16].PutValue("TEACHER");
            sheet.Cells[0, 17].PutValue("TRAINING_PERIOD");
            sheet.Cells[0, 18].PutValue("ACTIVITY_FORM");
            sheet.Cells[0, 19].PutValue("DURATION");
            int i = 1;
            foreach (var childunion in childunions)
            {
                sheet.Cells[i, 0].PutValue(childunion.ChildUnionId);
                /*sheet.Cells[i, 1].PutValue(childunion.ChildUnionId);
                sheet.Cells[i, 2].PutValue(childunion.ChildUnionId);
                sheet.Cells[i, 3].PutValue(childunion.ChildUnionId);
                sheet.Cells[i, 4].PutValue(childunion.ChildUnionId);
                sheet.Cells[i, 5].PutValue(childunion.ChildUnionId);
                sheet.Cells[i, 6].PutValue(childunion.ChildUnionId);*/
                sheet.Cells[i, 7].PutValue("\"" + childunion.Program.Name.Trim().Replace("\"", "'") + "\"");
                sheet.Cells[i, 8].PutValue("\"" + childunion.Profile.Name.Trim().Replace("\"", "'") + "\"");
                sheet.Cells[i, 9].PutValue("\"" + childunion.Section.Name.Trim().Replace("\"", "'") + "\"");
                sheet.Cells[i, 10].PutValue("\"" + childunion.ProgramCategory.ProgramCategoryName.Trim().Replace("\"", "'") + "\"");
                sheet.Cells[i, 11].PutValue("\"" + childunion.Name.Trim().Replace("\"", "'") + "\"");
                //sheet.Cells[i, 12].PutValue("\"" + childunion.Name.Trim().Replace("\"", "'") + "\"");
                sheet.Cells[i, 13].PutValue(childunion.AgeStart);
                sheet.Cells[i, 14].PutValue(childunion.AgeEnd);
                string tmp = childunion.BudgetTypes.Count(i1=>i1.DictTypeBudgetId==2)>0?"Да":"Нет";
                
                /*foreach (var budgettype in childunion.BudgetTypes)
                {
                    tmp = tmp + budgettype.TypeBudgetName + ";";
                }*/
                sheet.Cells[i, 15].PutValue("\"" + tmp.Trim().Replace("\"", "'") + "\"");
                tmp = "";
                foreach (var teacher in childunion.Teachers)
                {
                    tmp = tmp + teacher.Fio + ";";
                }
                sheet.Cells[i, 16].PutValue("\"" + tmp.Trim().Replace("\"", "'") + "\"");
                sheet.Cells[i, 17].PutValue(childunion.NumYears);
                tmp = "";

                foreach (var edutype in childunion.EducationTypes)
                {
                    tmp = tmp + edutype.EducationTypeName + ";";
                }

                sheet.Cells[i, 18].PutValue("\"" + tmp.Trim().Replace("\"", "'") + "\"");
                tmp = "";

                foreach (var agegroup in childunion.AgeGroups)
                {
                    tmp = tmp + string.Format("Года:{0}-{1} Возраст:{2}-{3} Пр-сть и кол-во:{4},{5};", agegroup.NumYearStart, agegroup.NumYearEnd, agegroup.AgeStart, agegroup.AgeEnd, agegroup.LessonLength, agegroup.LessonsInWeek);
                }
                sheet.Cells[i, 19].PutValue("\"" + tmp.Trim().Replace("\"", "'") + "\"");
                sheet.Cells[i, 20].PutValue(childunion.UdodId);
                
                
                
                i++;
            }
            sheet.AutoFitColumns();
            sheet.Cells.HideColumn(20);
            sheet.Cells.CreateRange(1, 0, i, 21).ApplyStyle(style_area, new Aspose.Cells.StyleFlag() { All = true });
            sheet.Cells.CreateRange(0, 0, 1, 21).ApplyStyle(style_header, new Aspose.Cells.StyleFlag() { All = true });


        }


        workbook.Save("ChildUnionList.xls", Aspose.Cells.FileFormatType.Excel2003, Aspose.Cells.SaveType.OpenInExcel, Response);
        HttpContext.Current.Response.End();
    }

    protected void lnkRestuct_OnClick(object sender, EventArgs e)
    {
        using (CommonDb db = new CommonDb())
        {
            var list = db.GetHistoryEvents();
            list = list.OrderBy(i => i.Number).ToList();
            //list = list.Where(i => i.FirstEkisId == 12494 && i.FirstEkisId!=i.SecondEkisId).ToList();
            using (UdodDb db1 = new UdodDb())
            {
                foreach (ExtendedHistoryEvent historyEvent in list)
                {
                    var firstudod = db1.GetUdodByEkis(historyEvent.FirstEkisId);
                    if (firstudod ==null) // Создаем ОУ
                    {
                        try
                        {

                        
                        string _url = Page.MapPath("../Templates/getUdodInfo.xml");
                        int? ReestrId = null;
                        //var reestrData = SiteUtility.GetUdodData(_url, historyEvent.FirstEkisId, ReestrId);

                        //using (DictCityDb db2 = new DictCityDb())
                        //{
                        //    var cities = db2.GetCities();
                        //    foreach (var city in cities)
                        //    {
                        //        if (reestrData.CityName.ToUpper().Contains(city.Name.Trim().ToUpper()) || reestrData.CityName.ToUpper().Equals(city.Name.Trim().ToUpper()))
                        //        {
                        //            reestrData.CityId = city.CityId;
                        //        }
                        //    }
                        //}
                        
                        //var udodid = db1.AddUdod(
                        //    string.IsNullOrEmpty(reestrData.Name)?"":reestrData.Name, 
                        //    string.IsNullOrEmpty(reestrData.ShortName)?"":reestrData.ShortName, 
                        //    string.IsNullOrEmpty(reestrData.JurAddress)?"":reestrData.JurAddress, reestrData.CityId,
                        //    string.IsNullOrEmpty(reestrData.FioDirector)?"":reestrData.FioDirector, 
                        //    string.IsNullOrEmpty(reestrData.PhoneNumber)?"":reestrData.PhoneNumber, 
                        //    string.IsNullOrEmpty(reestrData.FaxNumber)?"":reestrData.FaxNumber, 
                        //    String.IsNullOrEmpty(reestrData.EMail)?"":reestrData.EMail,
                        //    string.IsNullOrEmpty(reestrData.SiteUrl)?"":reestrData.SiteUrl, 
                        //    reestrData.UdodTypeId, 
                        //    string.IsNullOrEmpty(reestrData.UdodNumber)?"":reestrData.UdodNumber);
                        //db1.ChangeEkisCode(udodid, historyEvent.FirstEkisId);
                        //firstudod=new ExtendedUdod();
                        //firstudod.UdodId = udodid;
                        //firstudod.UdodStatusId = 1;
                        }
                        catch (Exception e11)
                        {
                            firstudod = null;
                        }
                    }

                    var secondudod = db1.GetUdodByEkis(historyEvent.SecondEkisId);
                    if (secondudod==null || firstudod==null || historyEvent.FirstEkisId==historyEvent.SecondEkisId)
                    {
                        continue;
                    }
                    if (secondudod.UdodStatusId == 1 )
                    {
                        db1.MergeOrAttachUdods(firstudod.UdodId, secondudod.UdodId, "", "");
                    }
                }
            }
        }
    }

    protected void lnkTmp1_OnClick(object sender, EventArgs e)
    {
        //using (CommonDb db = new CommonDb())
        //{
        //    var list = db.GetSchoolsTmp();
        //    using (PupilsDb db1 =new PupilsDb())
        //    {
        //        foreach (ExtendedTmp extendedTmp in list)
        //        {
        //            string _url = Page.MapPath("../Templates/getUdodInfo.xml");
        //            ExtendedPupil pupil = new ExtendedPupil();
        //            var pupils = SiteUtility.GetPupilData(Page.MapPath("../Templates/search_by_ReestrGuid_rq.xml"),
        //                                                extendedTmp.SchoolGuid.ToString());
        //            if (pupils.Count(i => !i.IsReestrDelete) > 0)
        //            {
        //                pupil = pupils.FirstOrDefault(i=>!i.IsReestrDelete);
        //                if (!string.IsNullOrEmpty(pupil.ReestrCode))
        //                {
        //                    // Обновление
        //                    pupil.DictSchoolTypeId = 1;
        //                    if (string.IsNullOrEmpty(pupil.ExpressPhoneNumber)) pupil.ExpressPhoneNumber = "";
        //                    if (string.IsNullOrEmpty(pupil.PhoneNumber)) pupil.PhoneNumber = "";
        //                    db1.UpdatePupil(extendedTmp.UserId, pupil);
        //                }
        //            }

        //        }
        //    }
        //}
    }

    protected void lnkRestuct1_OnClick(object sender, EventArgs e)
    {
        //using (UdodDb db = new UdodDb())
        //{
        //    var udods= db.GetUdods(null, "","", null, null, null, null, "", null, "", "", "", "", null,null);

        //    foreach (var udod1 in udods)
        //    {
        //        var udod = db.GetUdod(udod1.UdodId);
        //        ExtendedUdod info = SiteUtility.GetUdodData("", udod.EkisId, null);
        //        db.ChangeParent(udod.UdodId, info.ParentName);
        //    }
        //}
    }
}
﻿<%@ Page Language="C#" MasterPageFile="../Master/MasterPage.master" AutoEventWireup="true" CodeFile="Registration.aspx.cs" Inherits="User_Registration" %>
<%@ Register TagPrefix="uct" TagName="FIO" Src="~/UserControl/User/ucFIO.ascx" %>

<asp:Content runat="server" ID="contentRegistration" ContentPlaceHolderID="body">
    <table>
        <tr>
            <td>
                Логин:
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbLogin"></asp:TextBox>
            </td>
            
        </tr>
        <tr>
            <td>
                Пароль:
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbPassword"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Потверждение пароля:
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbPasswordConfirm"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <uct:FIO runat="server" ID="ucFIO"/>
            </td>
        </tr>
        <tr>
            <td>
                Дата рождения
            </td>
            <td>
            
                <asp:TextBox runat="server" ID="txtBirthDate" MaxLength="50"
                EnableTheming="false" autocomplete="off"
                AutoPostBack="true"  />

                <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtBirthDate" 
                    Animated="false" Format="dd.MM.yyyy"  /> 
            </td>
        </tr>
        <tr>
            <td>
                Email:
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbEMail"></asp:TextBox>
                <asp:RegularExpressionValidator runat="server" ID="dmEMail" ControlToValidate="tbEMail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                Телефон:
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbPhoneNumber"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Телефон для экстренной связи:
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbExpressPhoneNumber"></asp:TextBox>
            </td>
        </tr>
        
    </table>
    <asp:LinkButton runat="server" ID="lnkSave" onclick="lnkSave_Click">Сохранить</asp:LinkButton>
</asp:Content>
﻿<%@ Page Language="C#" MasterPageFile="../Master/MasterPage.master" AutoEventWireup="true" CodeFile="Profile.aspx.cs" Inherits="User_Profile" %>
<%@ Register Src="~/UserControl/User/ucFIO.ascx" TagPrefix="uct" TagName="FIO" %>
<%@ Register TagPrefix="uct" TagName="Address" Src="~/UserControl/Address/ucNewAddress.ascx" %>

<asp:Content runat="server" ID="ContentProfile" ContentPlaceHolderID="body">
    <br/>
    <asp:Label runat="server" ID="lblInfo">Профиль пользователя</asp:Label>
    <table>
        <tr>
            <td>
                Логин:
            </td>
            <td>
                <asp:Label runat="server" ID="lblLogin">Sergey</asp:Label>
            </td>
            
        </tr>
        <tr>
            <td>
                Пароль:
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbPassword"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Потверждение пароля:
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbPasswordConfirm"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <uct:FIO runat="server" ID="ucFIO"/>
            </td>
        </tr>
        <tr>
            <td>
                Дата рождения
            </td>
            <td>
            
                <asp:TextBox runat="server" ID="tbBirthDate" MaxLength="50"
                EnableTheming="false" autocomplete="off"
                AutoPostBack="true"  />

                <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="tbBirthDate" 
                    Animated="false" Format="dd.MM.yyyy"  /> 
            </td>
        </tr>
        <tr>
            <td>
                <uct:Address runat="server" ID="ParentAddress"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:LinkButton runat="server" ID="AddChild" onclick="AddChild_Click">Добавить ребенка</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView runat="server" ID="gvChildren" CssClass="dgc"  >
                    <EmptyDataTemplate>
                        <asp:Label ID="lblEmptyGvChildren" runat="server" style="font-size:16px">Нет детей</asp:Label>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="№" >
                        <ItemTemplate><%# (gvChildren.Rows.Count + 1)%></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ФИО">
                            <ItemTemplate>
                                <%# Eval("FIO") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>

    <asp:Panel runat="server" ID="popupAddPupil" style="display: none; border: 2px solid black" >
        <asp:Panel ID="pnlHeader" runat="server" >
            <asp:Label ID="litTitle" Text="<span class='headerCaptions'>Добавление ребенка</span>" runat="server"/>
            <asp:LinkButton ID="lnkClose" runat="server" 
                OnClientClick="$find('AddPupilPopup').hide(); return false;" />
        </asp:Panel>
        <div >

            <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                Фамилия:
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="tbLastName"></asp:TextBox>
                        
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Имя:
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="tbFirstName"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Отчетство:
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="tbMiddleName"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <asp:CheckBox runat="server" ID="cbEgualParent" Text="Адреса совпадают с родительским адресом"/> 
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Адрес проживания:
                            </td>
                            <td>
                                <uct:Address runat="server" ID="Address"/>
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <asp:CheckBox runat="server" ID="cbEqual" Text="Адрес совпадает с адресом проживания"/> 
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Адрес регистрации:
                            </td>
                            <td>
                                <uct:Address runat="server" ID="RegAddress"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Дата рождения
                            </td>
                            <td>
            
                                <asp:TextBox runat="server" ID="tbChildBirthday" MaxLength="50"
                                EnableTheming="false" autocomplete="off"
                                AutoPostBack="true"  />

                                <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="tbChildBirthday" 
                                    Animated="false" Format="dd.MM.yyyy"  /> 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Пол:
                            </td>
                            <td>
                                <asp:RadioButtonList runat="server" ID="rbSex" RepeatColumns="2"> 
                                    <Items>
                                        <asp:ListItem>Мужской</asp:ListItem>
                                        <asp:ListItem>Женский</asp:ListItem>
                                    </Items>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>
            
        
    </div>
        <div >

        <asp:Button ID="btnAddPulil" runat="server" Text="Сохранить" CausesValidation="false"/>
        <asp:Button ID="btnCancel" runat="server" Text="Отменить" CausesValidation="false" />
    </div>
    </asp:Panel>
    
    <ajaxToolkit:ModalPopupExtender runat="server" ID="popupAddExtender" PopupDragHandleControlID="pnlHeader"
        PopupControlID="popupAddPupil" CancelControlID="btnCancel"
        TargetControlID="btnShowPopup" RepositionMode="None"
        BehaviorID="AddPupilPopup"/>

    <asp:Button ID="btnShowPopup" runat="server" style="display:none" />
</asp:Content>
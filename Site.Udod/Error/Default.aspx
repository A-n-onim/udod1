﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../Master/MasterPage.master" CodeFile="Default.aspx.cs" Inherits="Error_Default" %>

<asp:Content runat="server" ID="content" ContentPlaceHolderID="body">
    <div id="wrapper">
        <div id="Div1" style="padding-left: 20px;">
            <p>
                В ходе выполнения запрошенной Вами операции было обнаружено, что в одном или нескольких элементах Системы, участвующих в выполнении операции, существуют ошибки, допущенные Вами ранее. 
                В связи с этим запрошенная Вами операция выполнена быть НЕ МОЖЕТ. 
                Проверьте корректность каждого из элементов, участвующих в выполнении операции. 
                <br />
                Вы можете вернуться на <a style="color: rgb(31, 64, 112); text-decoration: underline;" href="javascript:history.back()">предыдущую</a> или <a style="color: rgb(31, 64, 112); text-decoration: underline;" href="../">главную</a> страницу
            </p>

        </div>
    </div>
</asp:Content>

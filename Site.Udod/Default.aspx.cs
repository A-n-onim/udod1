﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Udod.Dal;
using Udod.Dal.Enum;

public partial class _Default : BasePage 
{
    private const string SessionKey = "Udod_ClaimList_";

    protected void Page_Load(object sender, EventArgs e)
    {
        //throw new Exception("Просто так");
        if (!Page.IsPostBack)
        {
            if (UserContext.Roles.RoleId==(int)EnumRoles.Osip)
            {
                pOsip.Visible = true;
                pOsip.DefaultButton = lnkFind.ID;
            }
            if (UserContext.Roles.RoleId==(int)EnumRoles.UdodEmployee)
            {
                if (UserContext.Profile.UserName.ToUpper() == "TESTUSER")
                    Page.Response.Redirect(SiteUtility.GetUrl("~/Udod/ChildUnionList.aspx"));
                else
                    Page.Response.Redirect(SiteUtility.GetUrl("~/Udod/ClaimList.aspx"));

            }
            if (UserContext.Roles.RoleId == (int)EnumRoles.Department)
            {
                Page.Response.Redirect(SiteUtility.GetUrl("~/Udod/UdodList.aspx"));
            }
            pFind.Visible = true;
        }
    }

    protected void lnkFind_OnClick(object sender, EventArgs e)
    {
        /*string param = "Number=" + tbNumber.Text + "&LastName=" + ucFio.LastName + "&FirstName=" + ucFio.FirstName + "&MiddleName=" + ucFio.MiddleName;
        if (rblBirthday.SelectedIndex==0)
        {
            param += "&BirthdayBegin=" + tbBirthdayBegin.Text + "&BirthdayEnd=" + tbBirthdayEnd.Text;
        }
        else
        {
            param += "&BirthdayBegin=" + tbBirthdayBegin.Text;
        }
        param += "&birthdayFindType=" + rblBirthday.SelectedIndex.ToString();*/
        if (UserContext.Roles.RoleId == (int)EnumRoles.Osip)
            StoreFilterValues();
        Page.Response.Redirect(SiteUtility.GetUrl("~/Udod/ClaimList.aspx"));
    }

    protected void ucFindUdod_OnChange(object sender, EventArgs e)
    {
        Session["SelectAgeGroupUdodId"] = ucFindUdod.UdodAgeGroupId;
        Page.Response.Redirect(SiteUtility.GetUrl("~/Claim/NewClaim.aspx"));
    }

    protected void popupSelectDO_OnLoad(object sender, EventArgs e)
    {
        
    }

    protected void popupSelectDOExtender_OnResolveControlID(object sender, ResolveControlEventArgs e)
    {
        
    }

    protected void rblBirthday_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        td6.Visible = td7.Visible = rblBirthday.SelectedIndex == 0;
        td3.InnerText = rblBirthday.SelectedIndex == 0 ? "Дата рождения от:" : "Дата рождения:";
    }

    private void StoreFilterValues()
    {
        Session[SessionKey + "Number"] = String.IsNullOrEmpty(tbNumber.Text) ? null : tbNumber.Text;

        Session[SessionKey + "LastName"] = String.IsNullOrEmpty(ucFio.LastName) ? null : ucFio.LastName;
        Session[SessionKey + "FirstName"] = String.IsNullOrEmpty(ucFio.FirstName) ? null : ucFio.FirstName;
        Session[SessionKey + "MiddleName"] = String.IsNullOrEmpty(ucFio.MiddleName) ? null : ucFio.MiddleName;

        Session[SessionKey + "birthdayFindType"] = rblBirthday.SelectedIndex;
        Session[SessionKey + "BirthdayBegin"] = String.IsNullOrEmpty(tbBirthdayBegin.Text) ? null : tbBirthdayBegin.Text;
        Session[SessionKey + "BirthdayEnd"] = String.IsNullOrEmpty(tbBirthdayEnd.Text) ? null : tbBirthdayEnd.Text;

        Session[SessionKey + "RegDateStart"] = "";
        Session[SessionKey + "RegDateEnd"] = "";
    }
}